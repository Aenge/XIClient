#pragma once
#include "CTkMsgBase.h"
#include "CTkMsgWinData.h"

namespace FFXI {
	namespace CTk {
		class CTkNode;
		class CTkSpoolMsgBase : public CTkMsgBase {
		public:
			virtual ~CTkSpoolMsgBase() = default;
			virtual void OnInitialUpdatePrimitive() override final;
			virtual void OnDestroyPrimitive() override final;
			virtual void OnDrawPrimitive() override final;
			virtual void OnDrawCalc(bool) override final;
			virtual void CreateSpoolMsg(char*, TK_LOGDATAHEADER) = 0;
			virtual void DestroyWind() = 0;
			CTkSpoolMsgBase(CTkDrawMessageWindow*);
			void StoreMsg(char*, TK_LOGDATAHEADER);
			void EnableChatDraw(int);
			bool IsActive();
			CTkNode* field_68;
			short field_6C;
			short field_6E;
		};
	}
}