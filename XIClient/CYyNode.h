#pragma once
#include "MemoryManagedObject.h"
namespace FFXI {
	namespace CYy {
		class CYyNode : public MemoryManagedObject {
		public:
			static const BaseGameObject::ClassInfo CYyNodeClass;
			virtual const BaseGameObject::ClassInfo* GetRuntimeClass() override;
			CYyNode();
			~CYyNode();
			void Append(CYyNode**);
			void Delete(CYyNode**);
			CYyNode* GetTail();
			CYyNode* field_4;
			CYyNode* field_8;
		};
	}
}