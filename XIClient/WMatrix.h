#pragma once
#define WIN32_LEAN_AND_MEAN
#include "d3dx9math.h"
namespace FFXI {
	namespace Math {
		class WMatrix : public D3DXMATRIX {
		public:
			WMatrix() = default;
			WMatrix(bool);
			void Identity();
			bool CheckMatrix();
			void Vec3TransformDrop4(D3DXVECTOR3*, const D3DXVECTOR3*);
			void Vec3TransformDrop4Self(D3DXVECTOR3*);
			void Vec3TransformNormal(D3DXVECTOR3*, D3DXVECTOR3*);
			void Vec3TransformNormalSelf(D3DXVECTOR3*);
			void Vec3CharCollisionHelper(D3DXVECTOR3*, D3DXVECTOR3*, D3DXVECTOR3*);
			void Vec4TransformSelf(D3DXVECTOR4*);
			void TransformVectorInPlace(D3DXVECTOR4*);
			void TransformVector(D3DXVECTOR4*, const D3DXVECTOR4*);
			void Vec4MultiplyAndScale(D3DXVECTOR4*, D3DXVECTOR4*);
			void Vec4MultiplySelfAndScale(D3DXVECTOR4*);
			void RotateX(float);
			void RotateY(float);
			void RotateZ(float);
			void Scale3(const D3DXVECTOR3*);
			void MatrixMultiply(const WMatrix*);
			void MatrixReverseMultiply(const WMatrix*);
			void MatrixInvert();
			void SomeCombo(WMatrix*);
			void DoSomething();
			bool IsBoxOutsideFrustum(D3DXVECTOR3*);
			bool IsBoxOutsideFrustumExcludingFarPlane(D3DXVECTOR3*);
			void CreateScaling(D3DXVECTOR4*);
			void AddTranslation4(D3DXVECTOR4*);
			void AddTranslation3(const D3DXVECTOR3*);
			void __fastcall Vec3TransNorm(D3DXVECTOR3*, float, float, float);
			void RHPerspective(float, float, float, float);
		};
	}
}