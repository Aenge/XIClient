#include "FsPhraseConstructor.h"
#include "TextUtils.h"
#include <string>

FFXI::Input::FsPhraseConstructor::~FsPhraseConstructor()
{
}

FFXI::Input::FsPhraseConstructor::FsPhraseConstructor()
{
	//nullsub
}

int FFXI::Input::FsPhraseConstructor::convertPhrase(char* a2, unsigned int a3, char* a4, unsigned int a5, bool a6, bool a7)
{
	if (a4 == nullptr) {
		return -1;
	}

	unsigned int a2_pos{};
	unsigned int a4_pos{};
	unsigned int v17{};
	while (a4_pos < a5 && a4[a4_pos] != 0) {
		if (FsPhraseConstructor::isWordIndex(a4, a4_pos) == true) {
			char v19[452];
			int len = this->convertToString(a4 + a4_pos, a3 - a2_pos, v19, 450, &v17, a6, a7);
			if (len < 0) {
				break;
			}
			if (a2_pos + len >= a3) {
				break;
			}
			strncpy_s(a2 + a2_pos, a3 - a2_pos, v19, len);
			a2_pos += len;
			a4_pos = v17;
		}
		else {
			int codelen = Util::Text::GetCodeLen(a4[a4_pos]);
			if (codelen + a2_pos >= a3) {
				break;
			}
			strncpy_s(a2 + a2_pos, a3 - a2_pos, a4 + a4_pos, codelen);
			a2_pos += codelen;
			a4_pos += codelen;
			a2[a2_pos] = 0;
		}
	}

	a2[a2_pos] = 0;
	return a2_pos;
}

int FFXI::Input::FsPhraseConstructor::convertToString(char* a2, unsigned int a3, char* a4, unsigned int a5, unsigned int* a6, bool a7, bool a8)
{
	if (a3 == 0) {
		return 0;
	}

	if (FsPhraseConstructor::isWordIndex(a2, 0u) == false) {
		return 0;
	}

	if (a3 < 2) {
		return -1;
	}

	bool v19{ true };
	unsigned int a2_pos{};
	char v27[452];
	char sublen = a2[5];
	if (a2[1] == 6) {
		if (sublen < 0) {
			return -1;
		}
		v19 = false;
		memcpy_s(v27, sizeof(v27), a2 + 6, (unsigned char)sublen);
		a2_pos = 7u + (unsigned char)sublen;
	}
	else {
		exit(0x8338);
		int v22 = 0;// this->field_3C->lookUp();
		if (v22 < 0) {
			return -1;
		}
		a2_pos = 6;
	}

	*a6 += a2_pos;
	char v26[452];
	unsigned int v26_pos{};
	if (v19 == true) {
		if (a7 == true) {
			v26[0] = 0xEFi8;
			v26[1] = 0x27i8;
			v26_pos = 2;
		}
		if (a8 == true) {
			v26[v26_pos] = 0x7Fi8;
			v26[v26_pos + 1] = 0x69i8;
			v26_pos += 2;
		}
	}

	char v27_pos{};
	while (v27_pos < sublen) {
		if (FsPhraseConstructor::isWordIndex(v27, (unsigned char)v27_pos) == true) {
			unsigned int v25{};
			int v16 = this->convertToString(a2 + a2_pos, a3 - a2_pos, v26 + v26_pos, 450 - v26_pos, &v25, false, false);
			if (v16 < 0) {
				return -1;
			}
			exit(0x8339);
			v16 = 0;//this->field_3C->lookUp();
			v26_pos += (unsigned int)v16;
			v27_pos += 6u;
			a2_pos += v25;
			*a6 += v25;
		}
		else {
			v26[v26_pos] = v27[v26_pos];
			v26_pos += 1;
			v27_pos += 1;
		}
	}

	if (v19 == true) {
		if (a8 == true) {
			v26[v26_pos] = 0x7Fi8;
			v26[v26_pos + 1] = 0x64i8;
			v26_pos += 2;
		}
		if (a7 == true) {
			v26[v26_pos] = 0xEFi8;
			v26[v26_pos + 1] = 0x28i8;
			v26_pos += 2;
		}
	}

	memcpy_s(a4, a5, v26, v26_pos);
	a4[v26_pos] = 0;
	return v26_pos;
}

bool FFXI::Input::FsPhraseConstructor::isWordIndex(char* a1, unsigned int a2)
{
	return a1[a2] == 0xFDi8;
}
