#pragma once
#include "CMoTask.h"
namespace FFXI {
	namespace CYy {
		class CYyVolumeChangeTask : public CMoTask {
		public:
			enum class VolumeChangeType {
				SYSTEM = 1,
				EFFECT = 2,
				ZONE   = 4,
				UNK    = 8
			};
			CYyVolumeChangeTask(float, int, CYyVolumeChangeTask::VolumeChangeType);
			virtual ~CYyVolumeChangeTask();
			virtual char OnMove() override final;
			VolumeChangeType Type;
			float field_38;
			float field_3C;
			float field_40;
			float Volume;
		};
	}
}