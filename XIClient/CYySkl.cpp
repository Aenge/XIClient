#include "CYySkl.h"
#include "CMoSk2.h"
#include "WMatrix.h"
#include "CMoResourceMng.h"
#include "KzFQuat.h"
#include "CYyMotionMan.h"
#include "BoneTransformState.h"
#include "Values.h"
#include "MemoryPoolManager.h"

using namespace FFXI::CYy;

const BaseGameObject::ClassInfo CYySkl::CYySklClass{	"CYySkl", sizeof(CYySkl), &BaseGameObject::BaseGameObjectClass };

//These five globals are related to the current skeleton being animated
CMoSk2* CYySkl::gSkeletonResource{ nullptr };
FFXI::Math::WMatrix* CYySkl::gBoneMatrices{ nullptr };
unsigned char* CYySkl::gBoneFlags{ nullptr };
int CYySkl::gBoneCount{ 0 };
int CYySkl::g_arr[256] = { 0 };

const BaseGameObject::ClassInfo* FFXI::CYy::CYySkl::GetRuntimeClass()
{
	return &CYySkl::CYySklClass;
}

FFXI::CYy::CYySkl::CYySkl()
{
	this->Resource = nullptr;
	this->pMatrixMemoryBlock = nullptr;
	this->pBoneMatrices = nullptr;
	this->field_10 = 0;
	this->pBoneFlags = nullptr;
}

FFXI::CYy::CYySkl::~CYySkl()
{
	if (this->pMatrixMemoryBlock)
		MemoryPoolManager::Unwrap(this->pMatrixMemoryBlock);

	if (this->pBoneFlags)
		MemoryPoolManager::Unwrap(this->pBoneFlags);
}

void CYySkl::Init() 
{
	if (this->Resource != nullptr) {
		unsigned short boneCount = (*this->Resource)->GetBoneCount();
		
		//Allocate a matrix for each bone plus one extra to make room for alignment
		this->pMatrixMemoryBlock = MemoryPoolManager::Wrap((boneCount + 1) * sizeof(FFXI::Math::WMatrix), MemoryPoolManager::MemoryPoolType::Elem);
		
		if (this->pMatrixMemoryBlock != nullptr) {
			//Align the matrix addresses to 16 bytes, which is good for SSE instructions.
			int alignedAddress = (int)this->pMatrixMemoryBlock + 15;
			alignedAddress &= 0xFFFFFFF0;
			this->pBoneMatrices = (FFXI::Math::WMatrix*)alignedAddress;
			
			//Initialize each bone matrix to identity
			for (unsigned short i = 0; i < boneCount; ++i) {
					this->pBoneMatrices[i].Identity();
			}

			//Allocate one byte for each bone in the skeleton for flags
			this->pBoneFlags = (unsigned char*)MemoryPoolManager::Wrap(boneCount, MemoryPoolManager::MemoryPoolType::Elem);
			if (this->pBoneFlags != nullptr)
				return;
		}
	}

	if (this->pMatrixMemoryBlock != nullptr)
		MemoryPoolManager::Unwrap(this->pMatrixMemoryBlock);

	if (this->pBoneFlags != nullptr)
		MemoryPoolManager::Unwrap(this->pBoneFlags);
}

bool FFXI::CYy::CYySkl::ResetBoneFlags()
{
	if (this->Resource == nullptr || this->pBoneFlags == nullptr)
		return false;

	unsigned short boneCount = (*this->Resource)->GetBoneCount();
	for (int i = 0; i < boneCount; ++i) {
		this->pBoneFlags[i] = BONE_STATE_INITIAL;
	}

	return true;
}

CMoSk2::BoneTransform* FFXI::CYy::CYySkl::GetStaticBoneTransform(unsigned short transformIndex)
{
	if (this->Resource == nullptr)
		return nullptr;
	if (CMoResourceMng::IsResourceReady((CMoResource***)&this->Resource) == false)
		return nullptr;

	return (*this->Resource)->GetBoneTransform(transformIndex);
}

FFXI::Math::BoundingBox3D* FFXI::CYy::CYySkl::GetSkeletonBoundingBox(unsigned char boxIndex)
{
	if (this->Resource == nullptr)
		return nullptr;
	if (CMoResourceMng::IsResourceReady((CMoResource***)&this->Resource) == false)
		return nullptr;

	return (*this->Resource)->GetBoundingBox(boxIndex);
}

void FFXI::CYy::CYySkl::MarkBoneChainActive(int boneIndex)
{
	CMoSk2::BoneData* boneData = (*this->Resource)->GetBoneData(boneIndex);

	this->pBoneFlags[boneData->ParentIndex] = CYySkl::BoneProcessingFlags::BONE_ACTIVE;
	
	//Traverse the bone heirachy until we reach the root
	if (boneData->ParentIndex != boneIndex)
		this->MarkBoneChainActive(boneData->ParentIndex);
}

void FFXI::CYy::CYySkl::ComputeBoneMatrices(FFXI::Math::WMatrix* a2, int a3, int a4, char* a5)
{
	if (this->Resource == nullptr)
		return;

	unsigned short boneCount = CYySkl::gSkeletonResource->GetBoneCount();
	if (a3 == 0)
		a3 = boneCount - 1;

	if (a4 == 0) {
		if (a5 != nullptr) {
			for (int i = 0; i < 2; ++i) {
				unsigned char index = a5[i + 2];
				if (index != 0xFFui8) {
					CYySkl::g_arr[index] = 0;
					BoneTransformState* boneTransform1 = CYyMotionMan::gBoneTransforms + a5[i];
					BoneTransformState* boneTransform2 = CYyMotionMan::gBoneTransforms + index;
					boneTransform1->Scaling.x *= boneTransform2->Scaling.x;
					boneTransform1->Scaling.y *= boneTransform2->Scaling.y;
					boneTransform1->Scaling.z *= boneTransform2->Scaling.z;
				}
			}
		}
		
		int i = 0;
		if (a3 > 0) {
			CYyMotionMan::gBoneTransforms[0].AccumulatedScaling = CYyMotionMan::gBoneTransforms[0].Scaling;
			CYyMotionMan::gBoneTransforms[0].Rotation.GetMatrix(CYySkl::gBoneMatrices);
			CYySkl::gBoneMatrices->AddTranslation3(&CYyMotionMan::gBoneTransforms[0].Translation);
			CYySkl::gBoneMatrices->MatrixMultiply(a2);
			for (i = 1; i <= a3; i++) {
				CMoSk2::BoneData* boneData = CYySkl::gSkeletonResource->GetBoneData(i);
				if (CYySkl::g_arr[boneData->ParentIndex] != 0) {
					CYyMotionMan::gBoneTransforms[i].AccumulatedScaling.x = CYyMotionMan::gBoneTransforms[boneData->ParentIndex].AccumulatedScaling.x * CYyMotionMan::gBoneTransforms[i].Scaling.x;
					CYyMotionMan::gBoneTransforms[i].AccumulatedScaling.y = CYyMotionMan::gBoneTransforms[boneData->ParentIndex].AccumulatedScaling.y * CYyMotionMan::gBoneTransforms[i].Scaling.y;
					CYyMotionMan::gBoneTransforms[i].AccumulatedScaling.z = CYyMotionMan::gBoneTransforms[boneData->ParentIndex].AccumulatedScaling.z * CYyMotionMan::gBoneTransforms[i].Scaling.z;
					CYyMotionMan::gBoneTransforms[i].Rotation.GetMatrix(CYySkl::gBoneMatrices + i);
					CYySkl::gBoneMatrices[i]._41 = CYyMotionMan::gBoneTransforms[boneData->ParentIndex].AccumulatedScaling.x * CYyMotionMan::gBoneTransforms[i].Translation.x;
					CYySkl::gBoneMatrices[i]._42 = CYyMotionMan::gBoneTransforms[boneData->ParentIndex].AccumulatedScaling.y * CYyMotionMan::gBoneTransforms[i].Translation.y;
					CYySkl::gBoneMatrices[i]._43 = CYyMotionMan::gBoneTransforms[boneData->ParentIndex].AccumulatedScaling.z * CYyMotionMan::gBoneTransforms[i].Translation.z;
					CYySkl::gBoneMatrices[i].MatrixMultiply(CYySkl::gBoneMatrices + boneData->ParentIndex);
					
				}
				else {
					CYySkl::g_arr[i] = 0;
				}
			}
		}

		for (int j = i; j < boneCount; j++) {
			CYySkl::g_arr[j] = 0;
		}
	}
	else if (a4 == 1) {
		exit(0x10033AB3);
	}
	else if (a4 == 2) {
		for (int i = 0; i <= a3; ++i) {
			CMoSk2::BoneData* boneData = CYySkl::gSkeletonResource->GetBoneData(i);

			if (i == 0) {
				CYyMotionMan::gBoneTransforms[0].AccumulatedScaling = CYyMotionMan::gBoneTransforms[0].Scaling;
			}
			else {
				CYyMotionMan::gBoneTransforms[i].AccumulatedScaling.x = CYyMotionMan::gBoneTransforms[i].Scaling.x * CYyMotionMan::gBoneTransforms[boneData->ParentIndex].AccumulatedScaling.x;
				CYyMotionMan::gBoneTransforms[i].AccumulatedScaling.y = CYyMotionMan::gBoneTransforms[i].Scaling.y * CYyMotionMan::gBoneTransforms[boneData->ParentIndex].AccumulatedScaling.y;
				CYyMotionMan::gBoneTransforms[i].AccumulatedScaling.z = CYyMotionMan::gBoneTransforms[i].Scaling.z * CYyMotionMan::gBoneTransforms[boneData->ParentIndex].AccumulatedScaling.z;
			}
			FFXI::Math::WMatrix v19{};
			v19.Scale3(&CYyMotionMan::gBoneTransforms[i].AccumulatedScaling);
			CYySkl::gBoneMatrices[i].MatrixReverseMultiply(&v19);
		}
	}
}

bool FFXI::CYy::CYySkl::BeginAnimating()
{
	if (this->Resource == nullptr)
		return false;

	if (this->pMatrixMemoryBlock == nullptr)
		return false;

	if (this->pBoneFlags == nullptr)
		return false;

	if (this->Resource == nullptr)
		CYySkl::gSkeletonResource = nullptr;
	else
		CYySkl::gSkeletonResource = *this->Resource;

	CYySkl::gBoneCount = CYySkl::gSkeletonResource->GetBoneCount();
	CYySkl::gBoneMatrices = this->pBoneMatrices;
	CYySkl::gBoneFlags = this->pBoneFlags;

	for (int i = 0; i < CYySkl::gBoneCount; ++i) {
		CYySkl::gBoneFlags[i] &= BONE_ACTIVE;
		CYySkl::g_arr[i] = 1;
	}

	return true;
}

void FFXI::CYy::CYySkl::SetStatics2()
{
	if (this->Resource == nullptr)
		return;

	unsigned short boneCount = CYySkl::gSkeletonResource->GetBoneCount();
	for (unsigned short i = 0; i < boneCount; ++i) {
		CMoSk2::BoneData* boneData = CYySkl::gSkeletonResource->GetBoneData(i);
		BoneTransformState* mmss = CYyMotionMan::gBoneTransforms + i;
		
		mmss->Rotation.Multiply(&mmss->Rotation, &boneData->Rotation);
		mmss->Rotation.Normalize();

		CYyMotionMan::gBoneTransforms[i].Translation += boneData->Position;
	}

	FFXI::Math::WMatrix v12{};
	v12.Identity();
	v12.RotateY(FFXI::Constants::Values::ANGLE_3PI_OVER_2);
	v12.Vec3TransformDrop4Self(&CYyMotionMan::gBoneTransforms[0].Translation);
}

void FFXI::CYy::CYySkl::ApplyGlobalTranslation(D3DXVECTOR4* translationVector, unsigned short boneCount)
{
	if (this->Resource == nullptr) {
		return;
	}

	if (this->pBoneMatrices == nullptr) {
		return;
	}

	CYySkl::gBoneMatrices = this->pBoneMatrices;
	CYySkl::gSkeletonResource = *this->Resource;

	if (boneCount == 0) 
		boneCount = (*this->Resource)->GetBoneCount();

	for (int i = 0; i < boneCount; i++) {
		CYySkl::gBoneMatrices[i]._41 += translationVector->x;
		CYySkl::gBoneMatrices[i]._42 += translationVector->y;
		CYySkl::gBoneMatrices[i]._43 += translationVector->z;
	}
}
