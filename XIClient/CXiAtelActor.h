#pragma once
#include "CXiActor.h"
namespace FFXI {
	namespace CYy {
		class CXiAtelActor : public CXiActor {
		public:
			static const BaseGameObject::ClassInfo CXiAtelActorClass;
			virtual const BaseGameObject::ClassInfo* GetRuntimeClass() override;
			virtual char OnMove() override;
			D3DXVECTOR4 field_C4;
		};
	}
}