#include "CMoMo2.h"
#include "CMoResourceMng.h"
#include "BoneTransformState.h"
#include "CYySkl.h"
#include "Globals.h"
using namespace FFXI::CYy;

FFXI::CYy::CMoMo2::CMoMo2()
{
	//nullsub
}

void FFXI::CYy::CMoMo2::Open()
{
	if (this->GetVersion() != 0) {
		//version error
	}

	if (this->HasBeenOpened() == true) 
		return;

	this->Data[1] = 1;

	if (*(short*)(this->Data + 4) != 0)
		*(short*)(this->Data + 4) -= 1;

	int resfileindex = CMoResourceMng::GetNumFileIndex();
	unsigned short trackCount = this->GetTrackCount();
	if (trackCount != 0) {
		if (resfileindex == 0xC122 || resfileindex == 0xC40D || resfileindex == 0xC129) {
			//throw "not implemented";
			exit(0x10018900);
		}
	}

	if (resfileindex >= CMoResourceMng::SomeFileMappingFunc(0, 8) && resfileindex <= CMoResourceMng::SomeFileMappingFunc(0, 15)
		|| resfileindex >= CMoResourceMng::SomeFileMappingFunc(0, 436) && resfileindex <= CMoResourceMng::SomeFileMappingFunc(0, 443)
		|| resfileindex >= CMoResourceMng::SomeFileMappingFunc(1, 8) && resfileindex <= CMoResourceMng::SomeFileMappingFunc(1, 15)) {
		int fourcc = this->ResourceID & 0xFFFF0000;
		if (fourcc == 0x316C0000 || fourcc == 0x316D0000 || fourcc == 0x31700000) {
			exit(0x10018901);
		}
	}
}

void FFXI::CYy::CMoMo2::Close()
{
	//nullsub
}

void FFXI::CYy::CMoMo2::CalcMotionData(int a2, float a3, float a4, BoneTransformState* a5)
{
	if (a4 < 0.0001f)
		return;

	unsigned short trackCount = this->GetTrackCount();
	for (int i = 0; i < trackCount; ++i) {
		CMoMo2::BoneTrack* boneTrack = this->GetBoneTrack(i);
		unsigned char flags = CYySkl::gBoneFlags[boneTrack->BoneIndex];

		if ((flags & CYySkl::BoneProcessingFlags::BONE_ACTIVE) == 0)
			continue;

		if ((flags & CYySkl::BoneProcessingFlags::BONE_SKIP_PROCESSING) != 0)
			continue;

		if ((flags & CYySkl::BoneProcessingFlags::BONE_STATE_MASK) == CYySkl::BoneProcessingFlags::BONE_STATE_INITIAL)
		{
			if (this->ApplyBaseAnimation(a2, i, boneTrack->BoneIndex, a3, a5)) {
				CYySkl::gBoneFlags[boneTrack->BoneIndex] |= CYySkl::BoneProcessingFlags::BONE_STATE_PROCESSED;
			}
		}
		else if ((flags & CYySkl::BoneProcessingFlags::BONE_STATE_MASK) == CYySkl::BoneProcessingFlags::BONE_STATE_PROCESSED) 
		{
			this->BlendOne(a2, i, boneTrack->BoneIndex, a3, a4, a5);
		}
		
	}
}

bool FFXI::CYy::CMoMo2::ApplyBaseAnimation(int a2, int a3, unsigned int a4, float a5, BoneTransformState* a6)
{
	FFXI::Math::KzFQuat quatlerp{}, quatsetup{};
	int* intchunk = (int*)(this->Data + 14 + 84 * a3);
	//CMoMo2::BoneTrack* boneTrack = this->GetBoneTrack(a3);
	//leftoff here needs cleaning
	float* floatchunk = (float*)intchunk;
	for (int i = 0; i < 4; ++i) {
		if (intchunk[i] == 0) {
			*(&a6[a4].Rotation.x + i) = floatchunk[4 + i];
			*(&quatsetup.x + i) = floatchunk[4 + i];
		}
		else if (intchunk[i] < 0 && a2 != 0) {
			if (a2 < 5) {
				FFXI::Math::KzFQuat quat{};
				a6[a4].Rotation = quat;
				a6[a4].Translation = { 0.0, 0.0, 0.0 };
				a6[a4].Scaling = { 1.0, 1.0, 1.0 };
			}

			return false;
		} 
		else {
			int v11 = intchunk[i] & 0x7FFFFFFF;
			if (v11 != 0) {
				float* pos = (float*)(this->Data + 10) + (int)a5 + v11;
				*(&a6[a4].Rotation.x + i) = pos[0];
				*(&quatsetup.x + i) = pos[1];
			}
			else {
				*(&a6[a4].Rotation.x + i) = floatchunk[4 + i];
				*(&quatsetup.x + i) = floatchunk[4 + i];
			}
		}
	}

	int cut = (int)a5;
	float v44 = a5 - (float)cut;
	a6[a4].Rotation.lerp(&quatlerp, &quatsetup, v44);
	a6[a4].Rotation = quatlerp;

	bool retval{ false };
	D3DXVECTOR3 vecsetup{};
	if (intchunk[8] < 0 && a2 != 0) {
		for (int i = 0; i < 3; ++i) {
			*(&a6[a4].Translation.x + i) = 0.0;
			int offset = intchunk[14 + i];
			if (offset == 0) {
				*(&a6[a4].Scaling.x + i) = floatchunk[17 + i];
				*(&vecsetup.x + i) = 0.0;
			}
			else {
				float* pos = (float*)(this->Data + 10) + (int)a5 + offset;
				*(&a6[a4].Scaling.x + i) = pos[0];
				*(&vecsetup.x + i) = pos[1] - pos[0];
			}
		}

		a6[a4].Scaling.x += v44 * vecsetup.x;
		a6[a4].Scaling.y += v44 * vecsetup.y;
		a6[a4].Scaling.z += v44 * vecsetup.z;
	}
	else {
		for (int i = 0; i < 3; ++i) {
			int index = intchunk[8 + i] & 0x7FFFFFFF;
			if (index == 0) {
				*(&quatlerp.x + i) = 0.0;
				*(&a6[a4].Translation.x + i) = floatchunk[11 + i];
			}
			else {
				float* pos = (float*)(this->Data + 10) + (int)a5 + index;
				*(&a6[a4].Translation.x + i) = pos[0];
				*(&quatlerp.x + i) = pos[1] - pos[0];
			}

			int offset = intchunk[14 + i];
			if (offset == 0) {
				*(&vecsetup.x + i) = 0.0;
				*(&a6[a4].Scaling.x + i) = floatchunk[17 + i];
			}
			else {
				float* pos = (float*)(this->Data + 10) + (int)a5 + index;
				*(&a6[a4].Scaling.x + i) = pos[0];
				*(&vecsetup.x + i) = pos[1] - pos[0];
			}
		}

		a6[a4].Translation.x += v44 * quatlerp.x;
		a6[a4].Translation.y += v44 * quatlerp.y;
		a6[a4].Translation.z += v44 * quatlerp.z;

		a6[a4].Scaling.x += v44 * vecsetup.x;
		a6[a4].Scaling.y += v44 * vecsetup.y;
		a6[a4].Scaling.z += v44 * vecsetup.z;

		retval = true;
	}

	if (a4 == 0) {
		a6->Scaling.x = 1.0;
		a6->Scaling.y = 1.0;
		a6->Scaling.z = 1.0;
	}
	
	return retval;
}

bool FFXI::CYy::CMoMo2::BlendOne(int a2, int a3, unsigned int a4, float a5, float a6, BoneTransformState* a7)
{
	//sub //TODO
	exit(0x100190B0);
	return false;
}

unsigned char FFXI::CYy::CMoMo2::GetVersion()
{
	return this->Data[0];
}

bool FFXI::CYy::CMoMo2::HasBeenOpened()
{
	return this->Data[1] != 0;
}

unsigned short FFXI::CYy::CMoMo2::GetTrackCount()
{
	return *(unsigned short*)(this->Data + 2);
}

unsigned short FFXI::CYy::CMoMo2::GetFrameCount()
{
	return *(unsigned short*)(this->Data + 4);
}

float FFXI::CYy::CMoMo2::GetTiming()
{
	return *(float*)(this->Data + 6);
}

CMoMo2::BoneTrack* FFXI::CYy::CMoMo2::GetBoneTrack(unsigned short trackIndex)
{
	return (BoneTrack*)(this->Data + 10 + trackIndex * sizeof(BoneTrack));
}
