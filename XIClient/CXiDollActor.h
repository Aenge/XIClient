#pragma once
#include "CXiActor.h"
namespace FFXI {
	namespace CYy {
		class CXiDollActor : public CXiActor {
		public:
			static const BaseGameObject::ClassInfo CXiDollActorClass;
			virtual const BaseGameObject::ClassInfo* GetRuntimeClass() override final;
		};
	}
}