#pragma once
#include "BaseGameObject.h"

namespace FFXI {
	namespace CYy {
		/**
		* @class MemoryManagedObject
		* @brief Base class for objects managed by the game's memory pool system.
		*
		* MemoryManagedObject extends BaseGameObject by providing custom memory
		* management operations through the memory pool system rather than using
		* standard C++ heap operations. It introduces a virtual DestroyObject() method
		* that handles both object destruction and memory deallocation.

		* Objects that need pool-based memory management should inherit from
		* this class instead of directly from BaseGameObject.
		*/
		class MemoryManagedObject : public BaseGameObject {
		public:
            MemoryManagedObject() = default;
            virtual ~MemoryManagedObject() = default;

			/**
			* @brief Properly destroys the object and optionally frees its memory
			*
			* This method handles the complete cleanup of an object by:
			* 1. Calling the appropriate destructor (which will cascade through the inheritance chain)
			* 2. Optionally returning the memory to the memory pool manager
			*
			* @param freeMemory If true (default), returns memory to the pool manager.
			*                   Set to false when memory management is handled separately.
			*/
            virtual void DestroyObject(bool freeMemory = true);
		};
	}
}