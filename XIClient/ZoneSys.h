#pragma once
#include "GC_ZONE.h"
#include "huffman.h"
#include "GP_SERV_COMMAND.h"

namespace FFXI {
	namespace Network { namespace UDP { class GP_GAME_PACKET_HEAD; namespace GP_SERV_PACKETS { struct GP_SERV_BASE; } } }
	namespace Zone {
		class ZoneSys {
		public:
			typedef void(*COMMAND_FUNC)(FFXI::Zone::GC_ZONE*, FFXI::Network::UDP::GP_GAME_PACKET_HEAD*, FFXI::Network::UDP::GP_SERV_PACKETS::GP_SERV_BASE*);
			GC_ZONE* pNowZone;
			GC_ZONE nowZone;
			int field_3B084;
			int field_3B088;
			FFXI::Network::Huffman::HuffmanTree huffmanTree;
			FFXI::Network::Huffman::HuffmanTable huffmanTable;
			int field_3E07C[(int)FFXI::Constants::Enums::GP_CLI_COMMAND::MAX];
			COMMAND_FUNC field_3E4F4[(int)FFXI::Constants::Enums::GP_SERV_COMMAND::MAX];
			COMMAND_FUNC IncomingPacketHandlers[(int)FFXI::Constants::Enums::GP_SERV_COMMAND::MAX];
			int field_3EDEC;
			int field_3EDF0;
			int field_3EDF4;
			int field_3EDF8;
			int field_3EDFC;
		};
	}
}