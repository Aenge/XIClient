#pragma once
#include "MemoryBlockDescriptor.h"

namespace FFXI {
	namespace CYy {
		class CYyLowerMemory : public MemoryBlockDescriptor {
		public:
			const static BaseGameObject::ClassInfo CYyLowerMemoryClass;
			virtual const BaseGameObject::ClassInfo* GetRuntimeClass() override final;
		};
	}
}