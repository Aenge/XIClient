#pragma once
#define WIN32_LEAN_AND_MEAN
#include "d3d8to9/d3d8types.hpp"
#include "CTkMenuPrimitive.h"
#include "CTkMsgWinData.h"
#include "TkRect.h"
namespace FFXI {
	namespace CTk {
		class _49SubList;
		class CTkMenuCtrlData;
		class CTkSpoolMsgBase;
		class CTkEventMsgBase;
		class CTkDrawMessageWindow : public CTkMenuPrimitive {
			static void kaipageDraw(DWORD, DWORD);
			void LogDraw(TKRECT, short, short, short, _49SubList*);
		protected:
			static CTkMenuCtrlData* kaipage;
		public:
			static D3DVIEWPORT8 LogViewport1;
			static D3DVIEWPORT8 LogViewport2;
			static void LogViewportSetup(TKRECT);
			static void TkDrawStringWithCtrl(short*, short, DWORD, DWORD, DWORD, unsigned char, char, TKRECT*);
			CTkDrawMessageWindow() = delete;
			CTkDrawMessageWindow(CTkMsgWinData*);
			virtual ~CTkDrawMessageWindow();
			virtual void OnInitialUpdatePrimitive() override;
			virtual void OnDrawPrimitive() override;
			virtual void OnDrawCalc(bool) override;
			virtual void PrimVirt16(int) override;
			virtual void MsgWinVirt1(char);
			virtual char MsgWinVirt2();
			virtual bool MsgWinVirt3() = 0;
			virtual char MsgWinVirt4() = 0;
			virtual const char* MsgWinGetResName() = 0;
			virtual int MsgWinVirt6() = 0;
			virtual void MsgWinVirt7(short) = 0;
			void AddLine(bool);
			void Resize();
			void Adds();
			int field_14;
			CTkMsgWinData* field_18;
			CTkSpoolMsgBase* field_1C;
			CTkEventMsgBase* field_20;
			TKRECT field_24;
			short field_2C;
			short field_2E;
			char field_30;
			char field_31;
			short field_32;
			int field_34;
			int field_38;
			int field_3C;
			TKRECT field_40;
			float field_48;
			char field_4C;
			char field_4D;
			short field_4E;
			short field_50;
			short field_52;
			short field_54;
			short field_56;
			char field_58;
			char field_59;
			char field_5A;
			char field_5B;
		};
	}
}