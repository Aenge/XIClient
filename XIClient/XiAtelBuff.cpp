#include "XiAtelBuff.h"
#include "MemoryPoolManager.h"
#include "CXiControlActor.h"
#include "Strings.h"
#include <string>
#include "CXiSkeletonActor.h"
#include "Values.h"
#include "Globals.h"
#include "ActorRace.h"
#include "PcActorModelSlots.h"
#include "FsConfig.h"
#include "CYyDb.h"
#include "PlaceHolders.h"
#include "CMoTaskMng.h"
#include "CGcMainSys.h"
#include "CTsZoneMap.h"
using namespace FFXI::CYy;

short XiAtelBuff::LoginActIndex{ 0 };
XiAtelBuff* XiAtelBuff::ActorBuffPtr[0x900];
unsigned int XiAtelBuff::ActorFlags[0x900];

const unsigned int InitActions[8] = 
{
    'tini', '1ini', '2ini', '3ini', 'tini', '1ini', '2ini', '3ini'
};

void FFXI::CYy::XiAtelBuff::InitSkeleActor()
{
    D3DXVECTOR4 pos{};
    if ((this->field_120 & 0x80u) != 0 && CYyDb::EventRdyFlag == true) {
        exit(0x1008E119);
    }
    else {
        pos = this->field_4;
    }

    char* mem = FFXI::MemoryPoolManager::globalInstance->Get(sizeof(CYy::CXiSkeletonActor), MemoryPoolManager::MemoryPoolType::Ex);
    if (mem == nullptr) {
        return;
    }

    CXiSkeletonActor* newactor = nullptr;
    FFXI::CYy::CMoTaskMng::DeleteThisTask = true;
    if (this->GetSomeFlag() == 1) {
        newactor = new (mem) CXiSkeletonActor(&pos, this, this->RaceGender - 1, 0);
    }
    else if ((this->field_12C & 0x800000) != 0) {
        newactor = new (mem) CXiSkeletonActor(&pos, this, 'pops');
    }
    else {
        newactor = new (mem) CXiSkeletonActor(&pos, this, 0);
    }

    this->Actor = newactor;
}

XiAtelBuff* FFXI::CYy::XiAtelBuff::GetLocalPlayer()
{
    if (LoginActIndex == 0) {
        return nullptr;
    }
    
    return XiAtelBuff::ActorBuffPtr[LoginActIndex];
}

void FFXI::CYy::XiAtelBuff::SetPctypeEquip(unsigned short a1, unsigned short* a2, bool a3)
{
    FFXI::CYy::XiAtelBuff* buff = FFXI::CYy::XiAtelBuff::ActorBuffPtr[a1];
    if (buff == nullptr) {
        return;
    }

    if (a3 == false) {
        //monstrosity?
        if (a2[Constants::Enums::PcActorModelSlots::RANGE] == 0xFFFFu) {
            a2[Constants::Enums::PcActorModelSlots::RANGE] = 0;
            exit(0x100997FB);
            return;
        }
        if ((buff->field_120 & 0x20) == 0 && buff->field_20C == 0) {
            buff->ActorType = FFXI::Constants::Enums::ActorType::ZERO;
        }
    }
    
    unsigned char race = (a2[Constants::Enums::PcActorModelSlots::FACE] >> 8) & 0xFF;
    if (buff->RaceGender != race) {
        buff->RaceGender = race;
        buff->field_120 |= 1;
    }

    bool IsFemale{};
    switch (buff->RaceGender) {
    case Constants::Enums::ActorRace::HUME_F:
    case Constants::Enums::ActorRace::ELVAAN_F:
    case Constants::Enums::ActorRace::TARU_F:
    case Constants::Enums::ActorRace::MITHRA:
        IsFemale = true;
        break;
    default:
        IsFemale = false;
        break;
    }

    SetActorFlags4And5(a1, (char)IsFemale);
    SetActorFlag6(a1, (char)IsFemale);
    SetActorFlag7(a1, 0);

    unsigned char face = a2[Constants::Enums::PcActorModelSlots::FACE] & 0xFF;
    if (a1 == LoginActIndex || (buff->ServerID & 0xFF000000) != 0 || buff->ActorType != FFXI::Constants::Enums::ActorType::ZERO || FFXI::Config::FsConfig::GetConfig(Constants::Enums::FsConfigSubjects::Subject165) == 0) {
        if (buff->ModelIDs[Constants::Enums::PcActorModelSlots::FACE] != face) {
            buff->ModelIDs[Constants::Enums::PcActorModelSlots::FACE] = face;
            buff->ModelChangedFlags |= 1 << FFXI::Constants::Enums::PcActorModelSlots::FACE;
        }
        if (buff->ModelIDs[Constants::Enums::PcActorModelSlots::HEAD] != a2[Constants::Enums::PcActorModelSlots::HEAD]) {
            buff->ModelIDs[Constants::Enums::PcActorModelSlots::HEAD] = a2[Constants::Enums::PcActorModelSlots::HEAD];
            buff->ModelChangedFlags |= 1 << FFXI::Constants::Enums::PcActorModelSlots::HEAD;
        }
        if (buff->ModelIDs[Constants::Enums::PcActorModelSlots::BODY] != a2[Constants::Enums::PcActorModelSlots::BODY]) {
            buff->ModelIDs[Constants::Enums::PcActorModelSlots::BODY] = a2[Constants::Enums::PcActorModelSlots::BODY];
            buff->ModelChangedFlags |= 1 << FFXI::Constants::Enums::PcActorModelSlots::BODY;
        }
        if (buff->ModelIDs[Constants::Enums::PcActorModelSlots::HAND] != a2[Constants::Enums::PcActorModelSlots::HAND]) {
            buff->ModelIDs[Constants::Enums::PcActorModelSlots::HAND] = a2[Constants::Enums::PcActorModelSlots::HAND];
            buff->ModelChangedFlags |= 1 << FFXI::Constants::Enums::PcActorModelSlots::HAND;
        }
        if (buff->ModelIDs[Constants::Enums::PcActorModelSlots::LEG] != a2[Constants::Enums::PcActorModelSlots::LEG]) {
            buff->ModelIDs[Constants::Enums::PcActorModelSlots::LEG] = a2[Constants::Enums::PcActorModelSlots::LEG];
            buff->ModelChangedFlags |= 1 << FFXI::Constants::Enums::PcActorModelSlots::LEG;
        }
        if (buff->ModelIDs[Constants::Enums::PcActorModelSlots::FOOT] != a2[Constants::Enums::PcActorModelSlots::FOOT]) {
            buff->ModelIDs[Constants::Enums::PcActorModelSlots::FOOT] = a2[Constants::Enums::PcActorModelSlots::FOOT];
            buff->ModelChangedFlags |= 1 << FFXI::Constants::Enums::PcActorModelSlots::FOOT;
        }
    }
    else {
        if (buff->ModelIDs[Constants::Enums::PcActorModelSlots::FACE] != face) {
            buff->ModelIDs[Constants::Enums::PcActorModelSlots::FACE] = face;
            buff->ModelChangedFlags |= 1 << FFXI::Constants::Enums::PcActorModelSlots::FACE;
        }

        static const unsigned short default_models[11] = {0, 0, 0, 8, 0, 8, 0, 8, 0, 8, 0};
        for (int i = 1; i < 11; ++i) {
            if (buff->ModelIDs[i] != default_models[i]) {
                buff->ModelIDs[i] = default_models[i];
                buff->ModelChangedFlags |= 1 << i;
            }
        }
    }

    if (buff->ModelIDs[Constants::Enums::PcActorModelSlots::MH] != a2[Constants::Enums::PcActorModelSlots::MH]) {
        buff->ModelIDs[Constants::Enums::PcActorModelSlots::MH] = a2[Constants::Enums::PcActorModelSlots::MH];
        buff->ModelChangedFlags |= 1 << FFXI::Constants::Enums::PcActorModelSlots::MH;
    }
    if (buff->ModelIDs[Constants::Enums::PcActorModelSlots::OH] != a2[Constants::Enums::PcActorModelSlots::OH]) {
        buff->ModelIDs[Constants::Enums::PcActorModelSlots::OH] = a2[Constants::Enums::PcActorModelSlots::OH];
        buff->ModelChangedFlags |= 1 << FFXI::Constants::Enums::PcActorModelSlots::OH;
    }
    if (buff->ModelIDs[Constants::Enums::PcActorModelSlots::RANGE] != a2[Constants::Enums::PcActorModelSlots::RANGE]) {
        buff->ModelIDs[Constants::Enums::PcActorModelSlots::RANGE] = a2[Constants::Enums::PcActorModelSlots::RANGE];
        buff->ModelChangedFlags |= 1 << FFXI::Constants::Enums::PcActorModelSlots::RANGE;
    }
}

void FFXI::CYy::XiAtelBuff::SetActorFlags4And5(unsigned short a1, char a2)
{
    ActorFlags[a1] &= 0xFFCF;
    ActorFlags[a1] ^= (a2 & 3) << 5;
}

void FFXI::CYy::XiAtelBuff::SetActorFlag6(unsigned short a1, char a2)
{
    ActorFlags[a1] &= 0xFFBF;
    ActorFlags[a1] ^= (a2 & 1) << 6;
}

void FFXI::CYy::XiAtelBuff::SetActorFlag7(unsigned short a1, char a2)
{
    ActorFlags[a1] &= 0xFF7F;
    ActorFlags[a1] ^= (a2 & 1) << 7;
}

void FFXI::CYy::XiAtelBuff::InitActorFlags()
{
    const int array_size = sizeof(ActorFlags) / sizeof(ActorFlags[0]);
    for (int i = 0; i < array_size; ++i) {
        ActorFlags[i] &= 0xFFFFFF00;
    }
}

bool FFXI::CYy::XiAtelBuff::CanIMove()
{
    FFXI::CYy::XiAtelBuff* localplayer = GetLocalPlayer();
    if (localplayer == nullptr) {
        return false;
    }

    if (LoginActIndex == 0) {
        return false;
    }

    if ((localplayer->ServerID & 0xFF000000) == 0) {
        Constants::Enums::ActorType type = localplayer->ActorType;
        if (type != Constants::Enums::ActorType::DOOR && type != Constants::Enums::ActorType::LIFT && type != Constants::Enums::ActorType::MODEL) {
            switch (localplayer->field_1B8) {
            case 1:
            case 2:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 0xF:
            case 0x10:
            case 0x14:
            case 0x15:
            case 0x16:
            case 0x17:
            case 0x18:
            case 0x19:
            case 0x1A:
                return false;
            default:
                break;
            }
        }
    }

    if ((localplayer->field_12C & 0x20) != 0) {
        return false;
    }

    if (Placeholder::CliEventUcFlag == true) {
        return false;
    }

    if (Placeholder::ClientEventUcFlag2 == true) {
        return false;
    }

    if (Placeholder::EventExecFlag == true) {
        return false;
    }

    if (Placeholder::KaComCheckBuffs() == false) {
        return false;
    }

    if (CYyDb::RecvKingCounterFlag == false) {
        return false;
    }

    if (Placeholder::KaComCheckBuffStatis(14) == true) {
        return false;
    }

    if (XiAtelBuff::IsFishing(localplayer->gameStatus) == true) {
        return false;
    }

    if (localplayer->gameStatus == Constants::Enums::GAME_STATUS::ITEMMAKE) {
        return false;
    }

    if (XiAtelBuff::IsFishing(localplayer->field_168) == true) {
        return false;
    }

    if (localplayer->field_168 == Constants::Enums::GAME_STATUS::ITEMMAKE) {
        return false;
    }

    if ((localplayer->field_128 & 0x8000) != 0) {
        return false;
    }

    if (Placeholder::g_pTkDelivery != nullptr) {
        //TODO
        exit(0x10094EAF);
    }

    if (Placeholder::g_pTkPost != nullptr) {
        //TODO
        exit(0x10094EC6);
    }

    return true;
}

FFXI::CYy::XiAtelBuff::XiAtelBuff()
{
	this->Reset(0);
}

FFXI::CYy::XiAtelBuff::~XiAtelBuff()
{
    this->EventDelete();
    this->ObjectDelete();
}

void FFXI::CYy::XiAtelBuff::Reset(int a2)
{
    this->field_DC = 100;
    this->field_E0 = 100;
    this->field_120 &= 0xFFFFE07F;
    this->field_11C = 0;
    this->field_4 = { 0.0, 0.0, 0.0, 1.0 };
    this->field_14 = { 0.0, 0.0, 0.0, 1.0 };
    this->field_24 = { 0.0, 0.0, 0.0, 1.0 };
    this->ZoneID = 0;
    this->ServerID = 0;
    this->Name[0] = 0;
    this->Actor = nullptr;
    memset(this->ActorPointers, 0, sizeof(this->ActorPointers));
    this->field_E8 = 0;
    this->field_120 &= 0xFFFFFFCF;
    this->ActorType = FFXI::Constants::Enums::ActorType::ZERO;
    this->RaceGender = 0;
    this->field_162 = 0;
    this->field_164 = '4uom';
    memset(&this->ModelIDs, 0, sizeof(this->ModelIDs));
    this->ModelChangedFlags = 0;
    this->field_F6 = 0;
    this->field_D8 = 0;
    this->gameStatus = FFXI::Constants::Enums::GAME_STATUS::N_IDLE;
    this->field_168 = FFXI::Constants::Enums::GAME_STATUS::N_IDLE;
    this->field_174 = 0;
    this->field_178 = 0;
    this->field_158 = 5.0;
    this->field_98 = 5.0;
    this->field_9C = 5.0;
    this->field_F0 = 0;
    this->field_1B4 = 0;
    this->field_184 = 0;
    this->field_180 = 0;
    this->field_17C = 0;
    this->field_160 = 0;
    this->field_15E = 0;
    this->field_15C = 0;
    this->field_E4 = 0;
    this->field_1B8 = 0;
    this->field_1B9 = 0;
    this->field_1D0 = 0;
    this->field_1D4 = 23;
    this->field_1CC = 0;
    this->field_144 = 0;
    this->field_14A = 0;
    this->field_14C = 0;
    this->field_120 &= 0x1FB0;
    this->field_140 = 0;
    this->field_124 = 0x880;
    this->field_142 = 0;
    this->field_146 = 0;
    this->field_148 = 0;
    this->field_130 &= 0xFFFF7F00;
    this->field_12C &= 0x300002;
    this->field_138 &= 0x87FFFFFF;
    this->field_13C &= 0xFEC00800;
    //this and possibly more has changed since this client version
    this->field_13C |= 0x800;
    this->field_1EC = 0;
    this->field_1FB = 0;
    this->field_1FC = 0;
    this->field_1FD = 0;
    this->field_1D6 = 0;
    this->field_1D7 = 0;
    this->field_128 = 0x20001u;
    this->field_141 = 0;
    this->field_1F4 = 0;
    this->field_2A2 = 0;
    this->field_1F6 = 0;
    this->field_2A0 = 0;
    this->field_1FA = 0;
    this->field_1F8 = 0;
    this->field_200 = -1.0f;
    this->field_204 = 0;
    this->field_208 = 0;
    this->field_20C = 0;
    this->field_20E = 0;
    this->field_20F = 0;
    memset(&this->MonsterNumber, 0, 0x2Cu);
    this->field_13C &= 0xFF3FFFFF;
    this->field_188 = 0;
    this->field_130 &= 0xFFFC80FF;
    memset(&this->field_278, 0, 0x24u);
    this->field_134 &= 0xFF0FFC02;
    this->field_130 &= 0x103FFFF;
    this->field_29C = 0;
    this->field_29E = 0;
    this->field_134 &= 0x00F003FD;
    this->field_1FE = 0;
    this->field_138 &= 0xF8000000;
    memcpy(this->field_18C, FFXI::Constants::Strings::DefaultAnimations, sizeof(FFXI::Constants::Strings::DefaultAnimations));
    if (a2 == 0x1000000)
    {
        this->ServerID = 0x1000000;
        this->ZoneID = 0x900;
        this->field_1CC |= 2;
        this->field_120 |= 0x200;
    }
    else
    {
        this->MotionSetChg();
        this->IdleDefMotionReset();
    }
    this->field_D4 = 0;
    this->field_F8 = 0;
    this->field_1BC = -1;
    this->field_1C0 = 0;
    this->field_1C4 = 0;
    this->field_1C8 = 0;
    this->field_1E8 = 0;
    this->field_1F0 = 0;
    this->field_1D8 = 0;
    this->field_1DC = 0;
    this->field_1E0 = 0;
    this->field_1E4 = 0;
    this->field_2A4 = 0;
    memset(&this->field_210, 0, 0x20u);

    //something not quite right here
    this->field_230 = 0;
    memset((char*)&this->field_230 + 1, 0, 0x18u);
    this->field_249 = 0;
}

void FFXI::CYy::XiAtelBuff::MotionSetChg()
{
    if (this->Actor) {
        if (this->Actor->InOwnActorPointers() && this->CheckStatus()) {
            this->field_18C[0] = ' ihc';
        }
    }

    if (this->ActorType == FFXI::Constants::Enums::ActorType::EIGHT)
        this->field_18C[0] = ' ihc';

    if (this->GetSomeFlag() == 2) {
        memcpy(this->field_1A0, FFXI::Constants::Strings::SomeAnimations, sizeof(FFXI::Constants::Strings::SomeAnimations));
    }
    else {
        int v4{};
        if (XiAtelBuff::IsOnChair(this->gameStatus) == true) {
            if (this->IndexOver700()
                || (this->field_120 & 0x40000000) != 0
                || (this->field_12C & 0x80000000) != 0) {
                v4 = this->field_18C[1];
            }
            else
                v4 = this->field_18C[4];
        }
        else if (this->gameStatus == FFXI::Constants::Enums::GAME_STATUS::SIT)
            v4 = this->field_18C[1];
        else {
            v4 = this->field_18C[0];
        }

        this->field_1A0[0] = v4;
        this->field_1A0[1] = v4;
        this->field_1A0[2] = v4;
        this->field_1A0[3] = this->field_18C[2];
        this->field_1A0[4] = this->field_18C[3];

        if (this->ActorType == FFXI::Constants::Enums::ActorType::TWO || this->ActorType == FFXI::Constants::Enums::ActorType::SEVEN) {
            unsigned short v8 = this->ModelIDs[0];
            if (v8 >= 0x3C0u && v8 <= 0x3C9u
                || v8 >= 0x979u && v8 <= 0x97Du
                || v8 >= 0x9BFu && v8 <= 0x9C3u) {
                this->field_1A0[2] = this->field_18C[0];
            }
        }
    }

    if ((this->field_120 & 0x200) != 0) {
        if (this->Actor->IsKindOf(&CXiSkeletonActor::CXiSkeletonActorClass)) {
            CXiSkeletonActor* skele = (CXiSkeletonActor*)this->Actor;
            skele->field_7C8[0] = this->field_1A0[0];
            skele->field_7C8[1] = this->field_1A0[3];
            skele->field_7C8[2] = this->field_1A0[4];
        }
    }
}

void FFXI::CYy::XiAtelBuff::IdleDefMotionReset()
{
    if ((this->field_120 & 0x2000) != 0)
        this->field_1B6 = 0;
    else
        this->field_1B6 = rand() % 600 + 600;

    this->field_1A0[0] = this->field_1A0[1];

    if ((this->field_120 & 0x200) != 0) {
        if (this->Actor->IsKindOf(&CXiSkeletonActor::CXiSkeletonActorClass)) {
            CXiSkeletonActor* skele = (CXiSkeletonActor*)this->Actor;
            if (skele->field_7C8[0] != this->field_1A0[0])
                skele->field_7C8[0] = this->field_1A0[0];
       }
    }
}

unsigned char FFXI::CYy::XiAtelBuff::GetSomeFlag()
{
    if ((this->field_12C & 0x80000) != 0)
        return (this->field_12C >> 20) & 3;
    
    return (this->field_124 >> 13) & 3;
}

bool FFXI::CYy::XiAtelBuff::IsOnChair(FFXI::Constants::Enums::GAME_STATUS a2)
{
    switch (a2) {
    case Constants::Enums::GAME_STATUS::CHAIR00:
    case Constants::Enums::GAME_STATUS::CHAIR01:
    case Constants::Enums::GAME_STATUS::CHAIR02:
    case Constants::Enums::GAME_STATUS::CHAIR03:
    case Constants::Enums::GAME_STATUS::CHAIR04:
    case Constants::Enums::GAME_STATUS::CHAIR05:
    case Constants::Enums::GAME_STATUS::CHAIR06:
    case Constants::Enums::GAME_STATUS::CHAIR07:
    case Constants::Enums::GAME_STATUS::CHAIR08:
    case Constants::Enums::GAME_STATUS::CHAIR09:
    case Constants::Enums::GAME_STATUS::CHAIR10:
    case Constants::Enums::GAME_STATUS::CHAIR11:
    case Constants::Enums::GAME_STATUS::CHAIR12:
    case Constants::Enums::GAME_STATUS::CHAIR13:
    case Constants::Enums::GAME_STATUS::CHAIR14:
    case Constants::Enums::GAME_STATUS::CHAIR15:
    case Constants::Enums::GAME_STATUS::CHAIR16:
    case Constants::Enums::GAME_STATUS::CHAIR17:
    case Constants::Enums::GAME_STATUS::CHAIR18:
    case Constants::Enums::GAME_STATUS::CHAIR19:
    case Constants::Enums::GAME_STATUS::CHAIR20:
        return true;
    default:
        return false;
    }
}

bool FFXI::CYy::XiAtelBuff::IsDead(FFXI::Constants::Enums::GAME_STATUS a2)
{
    return a2 == Constants::Enums::GAME_STATUS::N_DEAD || a2 == Constants::Enums::GAME_STATUS::B_DEAD;
}

bool FFXI::CYy::XiAtelBuff::IsFishing(FFXI::Constants::Enums::GAME_STATUS a2)
{
    return IsFishing12(a2) || IsFishing3(a2);
}

bool FFXI::CYy::XiAtelBuff::IsFishing12(FFXI::Constants::Enums::GAME_STATUS a2)
{
    switch (a2) {
    case Constants::Enums::GAME_STATUS::FISHING:
    case Constants::Enums::GAME_STATUS::FISHING1:
    case Constants::Enums::GAME_STATUS::FISHING2:
    case Constants::Enums::GAME_STATUS::FISHING3:
    case Constants::Enums::GAME_STATUS::FISHING4:
    case Constants::Enums::GAME_STATUS::FISHING5:
    case Constants::Enums::GAME_STATUS::FISHING6:
    case Constants::Enums::GAME_STATUS::FISH_2:
    case Constants::Enums::GAME_STATUS::FISHF:
    case Constants::Enums::GAME_STATUS::FISHR:
    case Constants::Enums::GAME_STATUS::FISHL:
        return true;
    default:
        return false;
    }
}

bool FFXI::CYy::XiAtelBuff::IsFishing3(FFXI::Constants::Enums::GAME_STATUS a2)
{
    switch (a2) {
    case Constants::Enums::GAME_STATUS::FISH_3:
    case Constants::Enums::GAME_STATUS::FISH_31:
    case Constants::Enums::GAME_STATUS::FISH_32:
    case Constants::Enums::GAME_STATUS::FISH_33:
    case Constants::Enums::GAME_STATUS::FISH_34:
    case Constants::Enums::GAME_STATUS::FISH_35:
    case Constants::Enums::GAME_STATUS::FISH_36:
        return true;
    default:
        return false;
    }
}

bool FFXI::CYy::XiAtelBuff::IndexOver700()
{
    return (this->ZoneID & 0xFFFF) >= 0x700;
}

void FFXI::CYy::XiAtelBuff::SetName(char* a2)
{
    if (this->field_20C != 0) return;

    memcpy(this->Name, a2, sizeof(this->Name) - 1);
    this->Name[sizeof(this->Name)-1] = 0;
}

bool FFXI::CYy::XiAtelBuff::CheckStatus()
{
    if (XiAtelBuff::IsOnMount(this->gameStatus) == true) {
        return false;
    }

    char v3 = (this->field_120 >> 24) & 7;
    return v3 == 2 || v3 == 3;
}

bool FFXI::CYy::XiAtelBuff::IsOnChocobo(FFXI::Constants::Enums::GAME_STATUS a2)
{
    return a2 == Constants::Enums::GAME_STATUS::CHOCOBO;
}

bool FFXI::CYy::XiAtelBuff::IsOnMount(FFXI::Constants::Enums::GAME_STATUS a2)
{
    return a2 == Constants::Enums::GAME_STATUS::MOUNT;
}

void FFXI::CYy::XiAtelBuff::CopyAllSPos()
{
    this->field_4 = this->field_24;
    this->field_14 = this->field_34;
    this->CopyAllPos();
}

void FFXI::CYy::XiAtelBuff::CopyAllPos()
{
    if ((this->field_120 & 0x200) == 0) return;

    if (this->ActorType == FFXI::Constants::Enums::ActorType::ZERO || this->ActorType == FFXI::Constants::Enums::ActorType::ONE || this->ActorType == FFXI::Constants::Enums::ActorType::TWO
        || this->ActorType == FFXI::Constants::Enums::ActorType::SIX || this->ActorType == FFXI::Constants::Enums::ActorType::SEVEN || this->ActorType == FFXI::Constants::Enums::ActorType::EIGHT) {

        CXiSkeletonActor* skele = (CXiSkeletonActor*)this->Actor;
        skele->field_34 = this->field_4;
        skele->field_5FC = this->field_4;
        skele->field_5FC.w = 1.0;
        skele->field_5C4 = this->field_4;
        skele->SetPos(&this->field_4);
        skele->field_C4 = this->field_4;
        skele->field_44 = this->field_14;
        skele->field_E4 = this->field_14;

        Globals::RotClamp(&skele->field_44);

        if (skele->IsDirectionLock() == false) {
            skele->field_61C = this->field_14;
            Globals::RotClamp(&skele->field_61C);
        }
    }
}

XiAtelBuff* FFXI::CYy::XiAtelBuff::GetSomeAtelBuff()
{
    if (this->field_29E == 0)
        return nullptr;

    return XiAtelBuff::ActorBuffPtr[this->field_29E];
}

void FFXI::CYy::XiAtelBuff::EventDelete()
{
    if ((this->field_120 & 0x80) == 0) {
        return;
    }

    exit(0x1008D72E);
}

void FFXI::CYy::XiAtelBuff::ObjectNew()
{
    if (XiAtelBuff::IsOnChocobo(this->gameStatus) == true && this->CheckStatus() == true) {
        if (this->MonsterNumber < 32 || this->MonsterNumber >= 37) {
            return;
        }
    }

    if (XiAtelBuff::IsOnMount(this->gameStatus) && this->field_1D7 == 0) 
    {
        return;
    }

    bool IsFishing = XiAtelBuff::IsFishing(this->gameStatus);
    bool IsOnChocobo = XiAtelBuff::IsOnChocobo(this->gameStatus);
    bool IsOnMount = XiAtelBuff::IsOnMount(this->gameStatus);
    bool IsOnChair = XiAtelBuff::IsOnChair(this->gameStatus);
    if ((this->field_120 & 0x200) == 0) {
        this->field_120 |= 0x200;
        this->field_128 &= 0x5FFFFFFF;
        this->gameStatus = this->field_168;

        switch (this->ActorType) {
        case Constants::Enums::ActorType::DOOR:
            exit(0x1008EBD9);
            break;
        case Constants::Enums::ActorType::LIFT:
            exit(0x1008E950);
            break;
        case Constants::Enums::ActorType::MODEL:
            exit(0x1008E6A1);
            break;
        default:
            this->InitSkeleActor();
            break;
        }

        if (this->Actor == nullptr) {
            this->field_120 &= ~0x200;
            return;
        }

        if (this->ServerID == FFXI::Network::CGcMainSys::gcZoneCharID()[1]) {
            if (FFXI::CYy::CXiActor::can_control == true) {
                FFXI::CYy::CXiActor::control_actor.SetActor(this->Actor);
            }
            FFXI::CYy::CXiControlActor::IsAutoRunning(false);
        }

        this->field_E4 = this->field_14.y;
        this->CheckHide();
    }
    else if ((this->field_120 & 0x80) != 0 && CYyDb::EventRdyFlag == true) {
        exit(0x1008EF0F);
    }
    else if ((this->field_120 & 0x80) == 0) {
        if ((this->field_120 & 0x100000) == 0) {
            if (this->Actor->VirtActor242() == 1) {
                float new_y{};
                bool use_new_y{ false };
                if (XiAtelBuff::GetLocalPlayer() == this) {
                    use_new_y = CYyDb::g_pCYyDb->g_pTsZoneMap->VCalibrate(this->field_4.x, this->field_4.y, this->field_4.z, 50.0, &new_y);
                }
                else {
                    use_new_y = CYyDb::g_pCYyDb->g_pTsZoneMap->VCalibrate3(this->field_4.x, this->field_4.y, this->field_4.z, &new_y);
                }
                if (use_new_y == true) {
                    this->field_4.y = new_y;
                    this->field_24.y = new_y;
                }
            }
        }

        this->Actor->field_34 = this->field_4;
        this->Actor->field_44 = this->field_14;
        Globals::RotClamp(&this->Actor->field_44);
    }

    //Label_216
    if ((this->field_120 & 0x200) != 0) {
        Constants::Enums::ActorType type = this->ActorType;
        if (type != Constants::Enums::ActorType::DOOR
            && type != Constants::Enums::ActorType::LIFT
            && type != Constants::Enums::ActorType::MODEL) {
            CXiSkeletonActor* sact = (CXiSkeletonActor*)this->Actor;
            if (sact->IsReadCompleteResList() == true) {
                this->SomeActionThing();
                if (this->field_140 == 3) {
                    sact->SetAction('0pop', sact, nullptr);
                }
                else if (this->field_140 == 6) {
                    sact->SetAction('1pop', sact, nullptr);
                }
                this->field_140 = 0;
            }
        }

        if ((this->field_12C & 0x1000) != 0) {
            this->Actor->VirtActor237(1);
        }

        D3DCOLOR v258 = this->Actor->VirtActor1();
        if ((this->field_12C & 0xC00) != 0 || (v258 & 0xFF000000) == 0) {
            this->Actor->SetShadowAlpha(0);
        }
        else {
            this->Actor->SetShadowAlpha(128);
        }

        if (IsOnChocobo || IsOnMount || IsFishing || IsOnChair
            && (this->field_120 & 0x400) == 0
            && ((CXiSkeletonActor*)this->Actor)->IsReadCompleteResList() == true) {
            exit(0x1008F3D5);
        }
    }

    //label 376
    if ((this->field_120 & 0x400) != 0) {
        if (IsOnChocobo || IsOnMount) {
            if ((this->field_120 & 0x1800) != 0) {
                this->Clean120_0x400();
            }
        }
        else if (IsFishing) {
            if ((this->field_120 & 0x1800) != 0x800) {
                this->Clean120_0x400();
            }
        }
        else if (IsOnChair) {
            if ((this->field_120 & 0x1800) != 0x1000) {
                this->Clean120_0x400();
            }
        }
    }

    //label 386
    if ((this->field_134 & 1) == 0) {
        if ((this->field_134 & 2) != 0) {
            if (this->field_1FE == 0) {
                if (this->ActorPointers[1] != nullptr) {
                    this->ActorPointers[1]->SetAction('daed', this->ActorPointers[1], nullptr);
                    this->field_1FE = 90;
                }
            }
            else {
                this->field_1FE -= (short)FFXI::CYyDb::CheckTick();
                if (this->field_1FE <= 0) {
                    this->Clean134_0x2();
                }
            }
        }
    }
    else {
        int conf = FFXI::Config::FsConfig::GetConfig(FFXI::Constants::Enums::FsConfigSubjects::Subject146);
        int v380 = CXiActor::SomeFunction(this->Actor, this->Actor, conf);
        if ((this->field_134 & 2) != 0 || v380 == 0) {
            if ((this->ActorPointers[1] != nullptr && this->field_1FE != 0) || v380 == 0) {
                this->Clean134_0x2();
            }
        }
        else {
            if (this->ActorPointers[1] != nullptr) {
                this->Clean134_0x2();
            }

            int some_count = ((this->field_130 >> 26) & 0xF) + 37;
            char* mem = FFXI::MemoryPoolManager::globalInstance->Get(sizeof(CYy::CXiSkeletonActor), MemoryPoolManager::MemoryPoolType::Ex);
            FFXI::CYy::CMoTaskMng::DeleteThisTask = true;
            CYy::CXiSkeletonActor* newskeleactor = new (mem) CYy::CXiSkeletonActor(&this->field_4, this, (FFXI::Constants::Enums::SUBACTOR_STATUS)some_count);
            this->ActorPointers[1] = newskeleactor;
            if (newskeleactor == nullptr) {
                return;
            }

            newskeleactor->field_88 |= 8u;
            newskeleactor->field_34 = this->field_4;
            newskeleactor->field_5C4 = this->field_4;
            newskeleactor->field_C4 = this->field_4;

            newskeleactor->field_44 = this->field_14;
            newskeleactor->field_E4 = this->field_14;
            Globals::RotClamp(&newskeleactor->field_44);

            newskeleactor->SetPos(&this->field_4);
            if (newskeleactor->IsDirectionLock() == false) {
                newskeleactor->field_61C = this->field_14;
                Globals::RotClamp(&newskeleactor->field_61C);
            }

            newskeleactor->field_5FC = this->field_4;
            newskeleactor->field_5FC.w = 1.0;

            this->field_134 |= 2;
        }
    }

    //label 444
    for (int i = 0; i < 8; ++i) {
        unsigned char v433 = 1 << i;
        unsigned char v434 = (this->field_134 >> 2) & 0xFFu;
        unsigned char v435 = (this->field_134 >> 10) & 0xFFu;
        if ((v434 & v433) != 0) {
            if ((v435 & v433) == 0) {
                if (this->ActorPointers[2 + i] != nullptr) {
                    this->Clean134_Shifted(i);
                }

                CXiSkeletonActor* skact{};
                char* mem = FFXI::MemoryPoolManager::globalInstance->Get(sizeof(CXiSkeletonActor), MemoryPoolManager::MemoryPoolType::Ex);
                CMoTaskMng::DeleteThisTask = true;
                if (mem != nullptr) {
                    skact = new (mem) CXiSkeletonActor(&this->field_4, this, i + 53);
                }

                this->ActorPointers[2 + i] = skact;
                if (skact == nullptr) {
                    return;
                }

                skact->field_88 |= 8u;
                skact->field_44 = this->field_14;
                Globals::RotClamp(&skact->field_44);

                skact->field_34 = this->field_4;
                skact->field_C4 = this->field_4;
                skact->field_E4 = this->field_14;
                skact->SetPos(&this->field_4);
                skact->field_5C4 = this->field_4;

                if (skact->IsDirectionLock() == false) {
                    skact->field_61C = this->field_14;
                    Globals::RotClamp(&skact->field_61C);
                }

                skact->field_5FC = this->field_4;
                skact->field_5FC.w = 1.0;
            }
            this->field_134 |= v433 << 10;
        }
        else if ((v435 & v433) != 0)
        {
            this->Clean134_Shifted(i);
        }
    }

    if ((this->field_138 & 0x4000) != 0) {
        if ((this->field_134 & 0x40000) == 0) {
            if (this->ActorPointers[10] != nullptr) {
                this->Clean134_0x40000();
            }

            D3DXVECTOR4 v596{ 1000.0f, 1000.0f, 1000.0f, 1.0f };
            CXiSkeletonActor* skact{};
            char* mem = FFXI::MemoryPoolManager::globalInstance->Get(sizeof(CXiSkeletonActor), MemoryPoolManager::MemoryPoolType::Ex);
            CMoTaskMng::DeleteThisTask = true;
            if (mem != nullptr) {
                skact = new (mem) CXiSkeletonActor(&v596, this, 82);
            }

            this->ActorPointers[10] = skact;
            if (skact == nullptr) {
                return;
            }

            skact->field_88 |= 8u;
            skact->field_44 = this->field_14;
            Globals::RotClamp(&skact->field_44);

            skact->field_34 = v596;
            skact->field_C4 = v596;
            skact->field_E4 = this->field_14;
            skact->SetPos(&v596);
            skact->field_5C4 = v596;

            if (skact->IsDirectionLock() == false) {
                skact->field_61C = this->field_14;
                Globals::RotClamp(&skact->field_61C);
            }

            skact->field_5FC = v596;
            skact->field_5FC.w = 1.0;
            this->field_134 |= 0x40000;
        }

        this->ObjectNewSubFunc1(this->ActorPointers[10]);
    }
    else if ((this->field_134 & 0x40000) != 0) {
        this->Clean134_0x40000();
    }

    if ((this->field_138 & 0x80000) != 0) {
        if ((this->field_134 & 0x80000) == 0) {
            if (this->ActorPointers[11] != nullptr) {
                this->Clean134_0x80000();
            }

            D3DXVECTOR4 v596{ 1000.0f, 1000.0f, 1000.0f, 1.0f };
            CXiSkeletonActor* skact{};
            char* mem = FFXI::MemoryPoolManager::globalInstance->Get(sizeof(CXiSkeletonActor), MemoryPoolManager::MemoryPoolType::Ex);
            CMoTaskMng::DeleteThisTask = true;
            if (mem != nullptr) {
                skact = new (mem) CXiSkeletonActor(&v596, this, 83);
            }

            this->ActorPointers[11] = skact;
            if (skact == nullptr) {
                return;
            }

            skact->field_88 |= 8u;
            skact->field_44 = this->field_14;
            Globals::RotClamp(&skact->field_44);

            skact->field_34 = v596;
            skact->field_C4 = v596;
            skact->field_E4 = this->field_14;
            skact->SetPos(&v596);
            skact->field_5C4 = v596;

            if (skact->IsDirectionLock() == false) {
                skact->field_61C = this->field_14;
                Globals::RotClamp(&skact->field_61C);
            }

            skact->field_5FC = v596;
            skact->field_5FC.w = 1.0;
            this->field_134 |= 0x80000;
        }
        
        this->ObjectNewSubFunc1(this->ActorPointers[11]);
    }
    else if ((this->field_134 & 0x80000) != 0) {
        this->Clean134_0x80000();
    }
}

void FFXI::CYy::XiAtelBuff::ObjectNewSubFunc1(CXiActor* a2)
{
    if (a2 == nullptr) {
        return;
    }

    D3DXVECTOR4 pos = *a2->GetPos();
    this->ObjectNewSubFunc2(&pos);
}

void FFXI::CYy::XiAtelBuff::ObjectNewSubFunc2(D3DXVECTOR4* a2)
{
    if (FFXI::Network::CGcMainSys::pGlobalNowZone == nullptr) {
        return;
    }

    if (Placeholder::get_config_153(7) == true) {
        return;
    }

    if (((this->field_134 >> 20) & 0xF) != 2) {
        return;
    }

    exit(0x1008DC7E);
}

void FFXI::CYy::XiAtelBuff::ObjectDelete()
{
    this->Clean120_0x400();
    this->Clean134_0x2();
    for (int i = 0; i < 10; ++i) {
        this->Clean134_Shifted(i);
    }

    this->Clean134_0x40000();
    this->Clean134_0x80000();
    this->Clean120_0x200();
    unsigned int result = this->field_124 ^ ((unsigned __int16)this->field_124 ^ (unsigned __int16)(16 * this->field_124)) & 0xF00;
    result &= ~0xFF;
    result |= this->field_124 & 0xF;
    this->field_124 = result | (8 * (this->field_124 & 0xE));
}

void FFXI::CYy::XiAtelBuff::Clean120_0x400()
{
    if ((this->field_120 & 0x400) == 0) {
        return;
    }

    exit(0x10091120);
}

void FFXI::CYy::XiAtelBuff::Clean134_0x2()
{
    if ((this->field_134 & 0x2) == 0) {
        this->field_1FE = 0;
    }
    else {
        exit(0x100910CC);
    }
}

void FFXI::CYy::XiAtelBuff::Clean134_Shifted(int a2)
{
    unsigned char value = (unsigned char)(this->field_134 >> 10);
    if ((value & (unsigned char)(1 << a2)) != 0) {
        exit(0x1009105F);
    }
}

void FFXI::CYy::XiAtelBuff::Clean134_0x40000()
{
    if ((this->field_134 & 0x40000) == 0) {
        return;
    }

    exit(0x10090FDF);
}

void FFXI::CYy::XiAtelBuff::Clean134_0x80000()
{
    if ((this->field_134 & 0x80000) == 0) {
        return;
    }

    exit(0x10090F8F);
}

void FFXI::CYy::XiAtelBuff::Clean120_0x200()
{
    if ((this->field_120 & 0x200) == 0) {
        return;
    }

    if ((this->field_128 & 4) != 0) {
        CXiSkeletonActor* actor = (CXiSkeletonActor*)this->Actor;
        actor->DeleteResp(3);
    }

    if (this->Actor != nullptr) {
        delete this->Actor;
        this->Actor = nullptr;
    }

    this->field_120 &= ~0x200;
    this->field_128 &= ~0x004;
    if (this->field_1B8 == 1 || this->field_1B8 == 2) {
        this->field_1B8 = 3;
    }
}

void FFXI::CYy::XiAtelBuff::Set120Bits24(unsigned char a1)
{
    this->field_120 &= 0xF8FFFFFF;
    this->field_120 ^= (a1 & 7) << 24;
}

void FFXI::CYy::XiAtelBuff::SetStepCount(unsigned short a1)
{
    this->field_15C = a1;
    unsigned short old15E = this->field_15E;
    this->field_15E = a1;
    this->field_124 &= 0xFFFF7FFF;
    if (old15E > a1) {
        this->field_160 = a1;
        this->IdleDefMotionReset();
    }
    else {
        this->field_160 = a1 - old15E;
    }

    this->field_160 += 8;
    this->field_124 |= 0x10000;
}

void FFXI::CYy::XiAtelBuff::CheckHide()
{
    this->field_120 &= ~0x800000u;
    if ((this->field_120 & 0x200) != 0) {
        if ((this->field_120 & 0x80) != 0 && CYyDb::EventRdyFlag == true) {
            if ((this->field_120 & 0x20000) == 0) {
                this->Actor->AtelDispOn();
                this->field_120 &= ~0x10000u;
            }
            else {
                this->Actor->AtelDispOff();
                this->field_120 |= 0x10000;
            }
        }
        else {
            if ((this->field_120 & 0xC000) == 0) {
                this->Actor->AtelDispOn();
                this->field_120 &= ~0x10000u;
            }
            else {
                if ((this->field_120 & 0x40000) != 0 && (this->field_138 & 0x1000000) == 0) {
                    this->field_120 |= 0x800000u;
                }
                this->Actor->AtelDispOff();
                this->field_120 |= 0x10000;
            }
        }
    }

    if ((this->field_120 & 0x100) != 0) {
        return;
    }

    FFXI::CYy::XiAtelBuff* localplayer = FFXI::CYy::XiAtelBuff::GetLocalPlayer();
    //client does not check localplayer
    if ((localplayer->field_130 & 0x800) != 0) {
        switch (localplayer->field_1FB) {
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 0x28:
        case 0x29:
        case 0x2A:
        case 0x2B:
            if ((this->field_1CC & 1) != 0 && (this->field_128 & 0x3800) < 0x1800) {
                switch (this->field_1FB) {
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 0x28:
                case 0x29:
                case 0x2A:
                case 0x2B:
                    break;
                default:
                    this->field_120 |= 0x800000;
                }
            }
            break;
        default:
            break;
        }
    }

    //grouplookstuff
    //TODO
    Placeholder::gcGroupLookStart();

    if (((this->field_120 & 0x80u) == 0 || CYyDb::EventRdyFlag == false) && (this->field_138 & 0x1000000) == 0) {
        if ((this->field_120 & 0x44000) != 0) {
            this->field_120 |= 0x800000u;
        }
        if ((this->ServerID & 0xFF000000) != 0) {
            bool v9 = (((unsigned char)FFXI::CYyDb::CliEventMode | (unsigned char)FFXI::CYyDb::CliEventModeLocal) & 0x10) == 0;
            if (v9 == false) {
                this->field_120 |= 0x800000u;
            }
        }
        else {
            bool v9 = (((unsigned char)FFXI::CYyDb::CliEventMode | (unsigned char)FFXI::CYyDb::CliEventModeLocal) & 2) == 0;
            if (v9 == false) {
                this->field_120 |= 0x800000u;
            }
        }
    }
}

void FFXI::CYy::XiAtelBuff::SomeActionThing()
{
    int v4 = this->field_124 & 0xF0;
    unsigned char v3 = (this->field_124 >> 1) & 7;
    if (v3 == 4 && v4 == 0 || v3 == 0 && v4 == 64) {
        unsigned int v2 = this->field_124;
        int v6 = v2 ^ ((unsigned __int16)v2 ^ (unsigned __int16)(16 * v2)) & 0xF00;
        this->field_124 = (v6 & 0xFFFFFF0F) | (8 * (v6 & 0xE));
        int v7 = this->field_124 >> 1;
        v7 &= 7;
        this->Actor->VirtActor32(v7);
    }
    else if (v3 >= 4 || this->ActorType == Constants::Enums::ActorType::SIX || this->ActorType == Constants::Enums::ActorType::SEVEN) {
        if (this->Actor != nullptr) {
            if ((this->field_128 & 0x20000000) != 0) {
                if (v3 != this->Actor->VirtActor31()
                    || v3 != (unsigned char)this->field_124 >> 4 && this->Actor->IsMovingAction(InitActions[v3], this->Actor) == 0) {
                    unsigned int v18 = this->field_124 ^ ((unsigned __int16)this->field_124 ^ (unsigned __int16)(16 * this->field_124)) & 0xF00;
                    this->field_124 = v18;
                    v18 >>= 1;
                    v18 &= 7;
                    this->Actor->VirtActor32(v18);
                    int v20 = (this->field_124 >> 4) & 0xF;
                    if (v20 != 8) {
                        this->Actor->KillAction(InitActions[v20], this->Actor);
                    }
                    this->Actor->SetAction(InitActions[v3], this->Actor, nullptr);
                    this->field_124 = (this->field_124 & 0xFFFFFF0F) | (8 * (this->field_124 & 0xE));
                }
            }
        }
    }
    else if (v3 != this->Actor->VirtActor31() || v3 != (unsigned char)this->field_124 >> 4) {
        unsigned int v12 = this->field_124 ^ ((unsigned __int16)this->field_124 ^ (unsigned __int16)(16 * this->field_124)) & 0xF00;
        this->field_124 = v12;
        v12 >>= 1;
        v12 &= 7;
        this->Actor->VirtActor32(v12);
        if ((this->field_120 & 0x200) != 0
            && (this->field_120 & 0x2000) != 0 || (this->field_130 & 4) != 0
            && (this->field_124 & 0xF0) != 0x80
            && this->Actor->IsMovingAction(InitActions[v3], this->Actor) == 0) {
            this->Actor->SetAction(InitActions[v3], this->Actor, nullptr);
        }

        this->field_124 = (this->field_124 & 0xFFFFFF0F) | (8 * (this->field_124 & 0xE));
    }
}
