#pragma once
#include "CMoTask.h"
namespace FFXI {
	namespace CYy {
		class CXiSkeletonActor;
		class CXiSkeletonActorRes : public CMoTask {
		public:
			static CXiSkeletonActorRes* last_task;
			CXiSkeletonActorRes(CXiSkeletonActor*, bool(__cdecl*)(CXiSkeletonActor*, CXiSkeletonActorRes*));
			virtual ~CXiSkeletonActorRes();
			virtual char OnMove() override final;
			CXiSkeletonActor* Actor;
			bool(__cdecl* Callback)(CXiSkeletonActor*, CXiSkeletonActorRes*);
			int state;
			int field_40;
			int field_44;
		};
	}
}