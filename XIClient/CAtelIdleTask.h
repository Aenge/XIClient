#pragma once
#include "CMoTask.h"

namespace FFXI {
	namespace CYy {
		class CAtelIdleTask : public CMoTask {
			const static BaseGameObject::ClassInfo CAtelIdleTaskClass;
			virtual const BaseGameObject::ClassInfo* GetRuntimeClass() override final;
			virtual char OnMove() override final;
		};
	}
}