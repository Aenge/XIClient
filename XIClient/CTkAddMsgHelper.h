#pragma once
namespace FFXI {
	namespace CTk {
		class CTkAddMsgHelper {
		public:
			struct s1 {
				int field_0;
				char field_4[4];
			};
			static void GetLen(unsigned int*, unsigned int*, char*);
			static void Fill(s1*, char*, unsigned int, char*);
			static int DoSomething(char*, unsigned int, char*, s1*);
			CTkAddMsgHelper();
			~CTkAddMsgHelper();
			int IsFinished();
			void Reset();
			void Init(char*, char, int, void*);
			unsigned short CreateMsg(char*, unsigned short, unsigned short);
			int field_0;
			unsigned short field_4;
			unsigned short field_6;
			char field_8[2];
			char field_A;
			char field_B;
			char* field_C;
			s1* field_10;
			unsigned int field_14;
			short field_18;
			unsigned short field_1A;
			int field_1C;
			int field_20;
		};
	}
}