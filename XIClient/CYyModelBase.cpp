#include "CYyModelBase.h"
#include "CMoSk2.h"
#include "CMoOs2.h"
#include "Globals.h"
#include "MemoryPoolManager.h"
#include "CYyBasicDt.h"
#include "CYyAdvancedDt.h"
#include "WMatrix.h"
#include "CXiSkeletonActor.h"
#include "KzCibCollect.h"
#include "CYyModel.h"
#include "CDx.h"
#include "CYyDb.h"
#include "CYyTexMng.h"
#include "CMoResourceMng.h"
#include "CYyOs2VtxBuffer.h"
#include "CYyVbMng.h"
#include "CYyMotionQue.h"
using namespace FFXI::CYy;

const BaseGameObject::ClassInfo CYyModelBase::CYyModelBaseClass{
	"CYyModelBase", sizeof(CYyModelBase), &BaseGameObject::BaseGameObjectClass
};

void FFXI::CYy::CYyModelBase::AddDt(CYyModelDt* a2)
{
	CYyModelDt** next = &this->ModelDt;
	while (*next != nullptr) {
		next = &(*next)->field_4;
	}
	*next = a2;
}

const BaseGameObject::ClassInfo* FFXI::CYy::CYyModelBase::GetRuntimeClass()
{
	return &CYyModelBase::CYyModelBaseClass;
}

FFXI::CYy::CYyModelBase::CYyModelBase()
{
	this->Previous = 0;
	this->ModelDt = nullptr;
	this->field_B0 = { 20.0, 0.0, 0.0, -1.0 };
	this->field_C0 = 0;
	this->field_C4 = (CYyTex*) -1;
}

FFXI::CYy::CYyModelBase::~CYyModelBase()
{
	if (this->Skeleton.Resource != nullptr) {
		(*this->Skeleton.Resource)->DecrementReferenceCount();
	}

	if (this->Previous != nullptr) {
		delete this->Previous;
		this->Previous = nullptr;
	}

	if (this->ModelDt != nullptr) {
		delete this->ModelDt;
		this->ModelDt = nullptr;
	}

	for (int i = 0; i < sizeof(this->field_24.motions) / sizeof(this->field_24.motions[0]); ++i) {
		this->field_24.motions[i].DeleteAll();
	}
}

void FFXI::CYy::CYyModelBase::SetSkeleton(CMoSk2** a2)
{
	this->Skeleton.Resource = a2;
	this->Skeleton.Init();
	this->Init();
}

void FFXI::CYy::CYyModelBase::Init()
{
	if (this->Skeleton.ResetBoneFlags() == false)
		return;

	CYyModelDt** dt = this->GetModelDt();
	while (*dt != nullptr) {
		(*dt)->VirtModelDt4(this->Skeleton.pBoneFlags);
		dt = &(*dt)->field_4;
	}

	int v7 = this->GetStaticBoneTransformBoneIndex(0x7F);
	if (v7)
		this->Skeleton.pBoneFlags[v7] = CYySkl::BoneProcessingFlags::BONE_ACTIVE;

	int v9 = this->GetStaticBoneTransformBoneIndex(0x7E);
	if (v9)
		this->Skeleton.pBoneFlags[v9] = CYySkl::BoneProcessingFlags::BONE_ACTIVE;

	unsigned short boneCount = (*this->Skeleton.Resource)->GetBoneCount();
	for (unsigned short i = boneCount - 1; i > 0; --i) {
		if (this->Skeleton.pBoneFlags[i] != CYySkl::BoneProcessingFlags::BONE_STATE_INITIAL)
			this->Skeleton.MarkBoneChainActive(i);
	}
}

CYyModelDt** FFXI::CYy::CYyModelBase::GetModelDt()
{
	return &this->ModelDt;
}

int FFXI::CYy::CYyModelBase::GetStaticBoneTransformBoneIndex(unsigned int a2)
{
	CMoSk2::BoneTransform* boneTransform = this->Skeleton.GetStaticBoneTransform(a2);
	if (boneTransform == nullptr)
		return 0;

	if (a2 < 0x80)
		return boneTransform->BoneIndex;

	return 0;
}

void FFXI::CYy::CYyModelBase::LinkOs2(CMoOs2** a2)
{
	CMoOs2* os2{ nullptr };
	if (a2)
		os2 = *a2;
	//os2 can be nullptr as per client

	CYyModelDt* dt{ nullptr };
	if ( (os2->Data[2] & 0x7F) == 0) {
		char* mem = MemoryPoolManager::globalInstance->Get(sizeof(CYyBasicDt), MemoryPoolManager::MemoryPoolType::Ex);
		if (mem) {
			dt = new (mem) CYyBasicDt();
		}
	}
	else if ( (os2->Data[2] & 0x7F) == 1) {
		char* mem = MemoryPoolManager::globalInstance->Get(sizeof(CYyAdvancedDt), MemoryPoolManager::MemoryPoolType::Ex);
		if (mem) {
			CYyAdvancedDt* adt = new (mem) CYyAdvancedDt();
			adt->Init(os2);
			dt = adt;
		}
	}

	//dt can be nullptr as per the client
	dt->field_8 = a2;
	this->AddDt(dt);
	this->Init();
}

bool FFXI::CYy::CYyModelBase::DoingSomething(CXiActor* a2, FFXI::Math::WMatrix* a3, float a4, D3DXVECTOR4* a5, int a6)
{
	bool FlagOne = a2->IsConstrain();
	bool FlagTwo{ FlagOne }, FlagThree{ FlagOne };

	int v1920 = a2->VirtActor192(0);
	int v1921 = a2->VirtActor192(1);

	if ((v1920 | v1921) != 0) {
		FlagTwo = v1920;
		FlagThree = v1921;
	}

	char flags[4] = { -1, -1, -1, -1 };
	if (FlagThree || FlagTwo) {
		KzCibCollect* cib = a2->VirtActor236();
		if (cib->field_10 != -1 && FlagTwo == true) {
			flags[0] = this->GetStaticBoneTransformBoneIndex(0x7E);
			if (flags[0] != 0) {
				flags[2] = this->GetStaticBoneTransformBoneIndex(cib->field_10);
				FlagOne = true;
			}
		}

		if (cib->constrain_no != 1 && FlagThree == true) {
			flags[1] = this->GetStaticBoneTransformBoneIndex(0x7F);
			if (flags[1] != 0) {
				flags[3] = this->GetStaticBoneTransformBoneIndex(cib->constrain_no);
				FlagOne = true;
			}
		}
	}

	if (this->Skeleton.BeginAnimating() == false)
		return false;

	if (this->field_24.UpdateAllLayers() == false && (a6 & 0x10) == 0)
		return false;

	int value{ 0 };
	if (a5 != 0) {
		int setter{ 0 };
		value = this->GetStaticBoneTransformBoneIndex(3);
		a6 = value;
		if (value != 0) {
			setter = this->GetStaticBoneTransformBoneIndex(7);
			if (setter != 0) {
				if (value < setter)
					value = setter;
				else
					a6 = setter;
			}
		}
		if (setter != -1)
			value = 0;
	}

	this->Skeleton.SetStatics2();

	if (FlagOne == true)
		this->Skeleton.ComputeBoneMatrices(a3, value, 0, flags);
	else
		this->Skeleton.ComputeBoneMatrices(a3, value, 0, nullptr);

	if (value != 0) {
		exit(0x1002950E);
	}

	if (FlagOne == true)
		this->Skeleton.ComputeBoneMatrices(a3, 0, 1, flags);
	
	this->Skeleton.ComputeBoneMatrices(a3, 0, 2, nullptr);
	return true;
}

void FFXI::CYy::CYyModelBase::AddStaticBoneTransformTranslation(int boneTransformIndex, D3DXVECTOR3* outputVector)
{
	CMoSk2::BoneTransform* boneTransform = this->Skeleton.GetStaticBoneTransform(boneTransformIndex);
	if (boneTransform != nullptr)
		*outputVector += boneTransform->Translation;
}

void FFXI::CYy::CYyModelBase::GetBoneWorldPosition(unsigned int boneTransformIndex, D3DXVECTOR4* out)
{
	// Initialize with default homogeneous position
	*out = { 0.0f, 0.0f, 0.0f, 1.0f };

	CMoSk2::BoneTransform* boneTransform = this->Skeleton.GetStaticBoneTransform(boneTransformIndex);
	if (boneTransform == nullptr)
		return;

	FFXI::Math::WMatrix v6{};
	v6.Identity();
	v6.RotateX(boneTransform->Rotation.x);
	v6.RotateY(boneTransform->Rotation.y);
	v6.RotateZ(boneTransform->Rotation.z);
	v6.AddTranslation3(&boneTransform->Translation);
	v6.MatrixMultiply(this->Skeleton.pBoneMatrices + boneTransform->BoneIndex);
	v6.TransformVectorInPlace(out);
}

void FFXI::CYy::CYyModelBase::Draw(CXiActor* a2, CYyModel* a3, float a4)
{
	CDx* cdx = FFXI::CYy::CDx::instance;
	if (this->field_C4 == (CYyTex*) -1) {
		this->field_C4 = CYyDb::g_pCYyDb->pCYyTexMng->FindD3sTexUnder("cubemap spec    ", (CMoResource*) *CYyDb::g_pCYyDb->pCMoResourceMng->Unknown3);
	}

	cdx->DXDevice->SetRenderState(D3DRS_AMBIENT, CXiSkeletonActor::ActorAmbientLight);

	for (int i = 0; i < 2; ++i) {
		D3DLIGHT8* light = CXiSkeletonActor::g_light_arr + i;
		if (light->Type == NULL) {
			cdx->DXDevice->LightEnable(i, false);
		}
		else {
			cdx->SetLight(i, light);
			cdx->DXDevice->LightEnable(i, true);
		}
	}

	cdx->DXDevice->SetRenderState(D3DRS_LIGHTING, true);
	CYyModelDt* dt = *this->GetModelDt();

	while (dt != nullptr) {
		IDirect3DVertexBuffer8** buffs = dt->field_10.field_C;
		CYyOs2VtxBuffer* vtxbuff = dt->VirtModelDt5();
		if (vtxbuff != nullptr) {
			if (vtxbuff->SomeCount != 0) {
				if (dt->field_10.field_4 == 0) {
					if (buffs[0] == nullptr
						&& CYyDb::g_pCYyDb->pCYyVbMng->InitBuffer(
							4 + 12 * vtxbuff->SomeCount,
							520, 0, Globals::g_VertexBufferD3DPool,
							buffs, 0) < D3D_OK
						|| buffs[2] == nullptr
						&& CYyDb::g_pCYyDb->pCYyVbMng->InitBuffer(
							4 + 12 * vtxbuff->SomeCount,
							520, 0, Globals::g_VertexBufferD3DPool,
							buffs + 2, 0) < D3D_OK
						|| (((*vtxbuff->Os2Resource)->Data[4] & 1) != 0)
						&& (buffs[4] == nullptr
							&& CYyDb::g_pCYyDb->pCYyVbMng->InitBuffer(
								4 + 12 * vtxbuff->SomeCount,
								520, 0, Globals::g_VertexBufferD3DPool,
								buffs + 4, 0) < D3D_OK
							|| buffs[6] == nullptr
							&& CYyDb::g_pCYyDb->pCYyVbMng->InitBuffer(
								4 + 12 * vtxbuff->SomeCount,
								520, 0, Globals::g_VertexBufferD3DPool,
								buffs + 6, 0) < D3D_OK)){
						//one of the above calls failed..
						for (int i = 0; i < 2; ++i) {
							if (buffs[i] != nullptr) {
								CYyDb::g_pCYyDb->pCYyVbMng->DoSomething(buffs + i);
								buffs[i] = nullptr;
							}
							if (buffs[2 + i] != nullptr) {
								CYyDb::g_pCYyDb->pCYyVbMng->DoSomething(buffs + 2 + i);
								buffs[2 + i] = nullptr;
							}
							if (buffs[4 + i] != nullptr) {
								CYyDb::g_pCYyDb->pCYyVbMng->DoSomething(buffs + 4 + i);
								buffs[4 + i] = nullptr;
							}
							if (buffs[6 + i] != nullptr) {
								CYyDb::g_pCYyDb->pCYyVbMng->DoSomething(buffs + 6 + i);
								buffs[6 + i] = nullptr;
							}
						}
					}

					dt->field_10.field_4 = vtxbuff->SomeCount;
				}
			}
		}
		dt = dt->field_4;
	}

	int count1{}, count2{};
	dt = *this->GetModelDt();
	while (dt != nullptr) {
		if (dt->field_C == 0) {
			CYyOs2VtxBuffer* vtxbuff = dt->VirtModelDt5();
			if (vtxbuff != nullptr) {
				vtxbuff->field_44 = dt->field_10.field_C[dt->field_10.field_8];
				vtxbuff->field_48 = dt->field_10.field_C[dt->field_10.field_8 + 2];
				vtxbuff->field_50 = dt->field_10.field_C[dt->field_10.field_8 + 4];
				vtxbuff->field_54 = dt->field_10.field_C[dt->field_10.field_8 + 6];
			}
			dt->VirtModelDt1(a2, a3, this, a4, &count1, &count2);
		}
		dt = dt->field_4;
	}

	for (int i = 0; i < 4; i++) {
		cdx->DXDevice->LightEnable(i, false);
	}
}

void FFXI::CYy::CYyModelBase::MaybeUpdateAnims(CXiSkeletonActor* a2)
{
	const int array_size = sizeof(this->field_24.motions) / sizeof(this->field_24.motions[0]);
	for (int i = array_size - 1; i >= 0; i--) {
		CYyMotionQueList* v3 = this->field_24.motions + i;
		CYyMotionQue* head = v3->Head;
		while (head != nullptr) {
			if (head->UpdateBlendWeights() == true) {
				//unused?
				//TODO
				head->GetMod();
			}
			else if (head->Previous != nullptr || i >= 3) {
				v3->Remove(head);
			}
			else if (a2 != nullptr) {
				a2->SetMotionLock(true);
			}
			head = (CYyMotionQue*)head->Previous;
		}
		//
		v3->field_10 = 0;
		//
		if (a2->GetMotStop() == false) {
			CYyQue* que = v3->Head;
			while (que != nullptr) {
				que->AdvanceAnimation(a2, i);
				que = que->Previous;
			}
		}
	}
}

void FFXI::CYy::CYyModelBase::UpdateDTs(CYyModel* a2, int a3)
{
	CYyModelDt** dt = this->GetModelDt();
	//dt can't be nullptr since it's a pointer to a struct field

	while (*dt != nullptr) {
		(*dt)->VirtModelDt2();
		dt = &(*dt)->field_4;
	}
}
