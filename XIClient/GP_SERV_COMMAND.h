#pragma once
namespace FFXI {
	namespace Constants {
		namespace Enums {
			enum class GP_SERV_COMMAND : unsigned short {
				NONE = 0x000,
				LOGIN = 0x00A,
				EQUIP_CLEAR = 0x04F,
				MAX = 0x11F
			};
		}
	}
}