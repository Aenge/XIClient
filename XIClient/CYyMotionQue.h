#pragma once
#include "CYyQue.h"
namespace FFXI {
	namespace CYy {
		class CMoMo2;
		class CYyMotionQue : public CYyQue {
		public:
			static const BaseGameObject::ClassInfo CYyMotionQueClass;
			virtual const BaseGameObject::ClassInfo* GetRuntimeClass() override final;
			virtual void VirtQue1() override final;
			virtual int GetResID() override final;
			virtual void* GetMod() override final;
			virtual void SetMod(void*) override final;
			virtual void SetSpeed(float) override final;
			virtual float GetFrame() override final;
			virtual void AdvanceAnimation(CXiSkeletonActor*, int) override final;
			virtual void ApplyAnimationToBones(int, int, void*, CYyQue*) override final;
			virtual bool IsFinished() override final;
			CYyMotionQue();
			virtual ~CYyMotionQue();

			bool UpdateBlendWeights();
			void BeginFadeOut();
			float Frame;
			float Speed;
			int LoopCounter;
			int MotionFourCC;
			CMoMo2** MotionResource;
			float CustomDuration;
			int field_3C;

			enum AnimationBlendState : int {
				NORMAL = 0,       // Normal playback with stable blend weight
				FADE_IN = 1,      // Increasing blend weight (animation starting)
				COMPLETED = 2,    // Animation has finished or is stopping
				FADE_OUT = 3      // Decreasing blend weight (animation ending)
			};
		};
	}
}