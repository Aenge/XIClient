#include "CTkDrawMessageWindowOne.h"
#include "MojiDraw.h"
#include "TkManager.h"
#include "CTkMsbDataList.h"
#include "Strings.h"
#include "FsConfig.h"
#include "CTkMenuCtrlData.h"
#include "PrimMng.h"
#include "CTkSubWindow.h"
#include "_49SubList.h"
#include "CTkMenuMng.h"

FFXI::CTk::CTkDrawMessageWindowOne::CTkDrawMessageWindowOne(CTkMsgWinData* a2)
	: CTkDrawMessageWindow(a2)
{
	//nullsub
}

FFXI::CTk::CTkDrawMessageWindowOne::~CTkDrawMessageWindowOne()
{
	//nullsub
}

void FFXI::CTk::CTkDrawMessageWindowOne::OnInitialUpdatePrimitive()
{
	if (FFXI::Text::MojiDraw::g_pFontUsGaiji == nullptr) {
		FFXI::Text::MojiDraw::g_pFontUsGaiji = FFXI::CTk::TkManager::g_CTkMenuDataList.FindMenuShapeFile(Constants::Strings::FontUsGaiji);
	}

	CTkDrawMessageWindow::kaipage = this->MenuCtrlData;

	//sub //todo
	//buffered log here, this->field_18->field_6403C
	//leaving this comment here so when it's changed to buffered log this won't compile
	this->field_18->field_6403C = 0;
	this->CTkDrawMessageWindow::OnInitialUpdatePrimitive();
}

void FFXI::CTk::CTkDrawMessageWindowOne::OnDrawPrimitive()
{
	this->CTkDrawMessageWindow::OnDrawPrimitive();
	if (this->field_31 <= 0) {
		return;
	}

	TKRECT a2{};
	this->MenuCtrlData->GetWindowLocate(&a2);

	_49SubList* list{};
	int chatmode = CTkSubWindow::GetChatMode();
	int v3 = FFXI::Config::FsConfig::GetConfig(Constants::Enums::FsConfigSubjects::Subject180);
	if (v3 == 0) {
		list = PrimMng::g_pTkSubWindow->field_5C[chatmode];
	}
	else {
		list = PrimMng::g_pTkSubWindow->field_80[chatmode];
	}

	if (list == nullptr) {
		return;
	}

	short diff = TkManager::g_CTkMenuMng.UIXRes - TkManager::g_CTkMenuMng.field_7C;

	float v1 = (1.0f - TkManager::g_CTkMenuMng.Get84(false)) * (float)diff - 512.0f;
	short v2 = diff - this->field_32 - 160;
	v1 -= (float)v2;
	a2.Left += (short)v1;
	list->Draw(a2.Left, a2.Top, 0x80808080, 0, 0);
}

void FFXI::CTk::CTkDrawMessageWindowOne::OnDrawCalc(bool a2)
{
	this->MsgWinVirt1(a2);
	this->CTkDrawMessageWindow::OnDrawCalc(a2);
}

bool FFXI::CTk::CTkDrawMessageWindowOne::MsgWinVirt3()
{
	return true;
}

char FFXI::CTk::CTkDrawMessageWindowOne::MsgWinVirt4()
{
	return PrimMng::g_pTkDrawMessageWindow != nullptr && PrimMng::g_pTkDrawMessageWindow->MenuCtrlData != nullptr;
}

const char* FFXI::CTk::CTkDrawMessageWindowOne::MsgWinGetResName()
{
	return Constants::Strings::MenuLogWindow;
}

int FFXI::CTk::CTkDrawMessageWindowOne::MsgWinVirt6()
{
	return 0;
}

void FFXI::CTk::CTkDrawMessageWindowOne::MsgWinVirt7(short a2)
{
	int v2 = (CTk::TkManager::g_CTkMenuMng.UIXRes - CTk::TkManager::g_CTkMenuMng.field_7C - 512) / 100 + 352;
	v2 *= a2;
	int v3 = (CTk::TkManager::g_CTkMenuMng.UIXRes - CTk::TkManager::g_CTkMenuMng.field_7C) - 160;
	if (v2 > v3) {
		v2 = v3;
	}
	
	if (v2 < 352) {
		v2 = 352;
	}

	this->field_52 = v2;
}
