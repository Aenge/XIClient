#pragma once
#include "CMoAttachmentsSubStruct.h"
namespace FFXI {
	namespace File {
		class ReferenceReadBase {
		public:
			ReferenceReadBase() = default;
			~ReferenceReadBase() = default;
			int ActorIndex;
			CYy::CMoAttachmentsSubStruct field_4;
		};
	}
}