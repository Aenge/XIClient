#include "Globals.h"
#include "CEnv.h"
#include "RegistryConfig.h"
#include <iostream>
#include <intrin.h>
using namespace FFXI::CYy;

const BaseGameObject::ClassInfo CEnv::CEnvClass{
	"CEnv", sizeof(CEnv), &BaseGameObject::BaseGameObjectClass
};

const BaseGameObject::ClassInfo* CEnv::GetRuntimeClass()
{
	return &CEnvClass;
}

CEnv::~CEnv() {
	//nullsub
}

CEnv::CEnv() {
	this->CPU_MMX_Support = 0;
	this->CPU_SSE_Support = 0;
	this->CPU_SSE3_Support = 0;
	this->CPU_3DNOW_Support = 0;
	this->field_C = 0;
	this->CPU_Threads_Per_Core = 1;
	this->field_18 = 0;
	this->field_14 = 0;
	this->Init();
}

void FFXI::CYy::CEnv::Init()
{
	this->CheckCPU();
	this->field_18 = 0;
	this->field_1C = 0;
	this->field_1E = 1;
	this->field_20 = 0;
	this->field_14 = 1;
	this->DoSomething(0.0);
}

void FFXI::CYy::CEnv::CheckCPU()
{
	this->CPUType = FFXI::Constants::Enums::CPUVendorID::Unknown;

	unsigned int v1 = __readeflags();
	//Flag 0x200000 determines if cpuid instruction may be used
	__writeeflags(v1 ^ 0x200000);
		
	unsigned int v2 = __readeflags();
	//Check if cpuid flag was allowed to be set. If not, exit.
	if (v2 == v1)
		return;

	//When called, cpuid sets EAX, EBX, ECX, and EDX registers.
	//In C, those registers are stored (in that order) into the array passed to cpuid
	//The second parameter passed to cpuid, also known as the leaf, determines which data is returned
	int data[4] = { 0 };
	char cpuStr[13] = { 0 };

	//Request CPU manufacturer ID string (Leaf 0)
	__cpuid(data, 0);
	*((int*)cpuStr + 0) = data[1];
	*((int*)cpuStr + 1) = data[3];
	*((int*)cpuStr + 2) = data[2];

	if (strcmp(cpuStr, "GenuineTMx86\0") == 0) {
		this->CPUType = FFXI::Constants::Enums::CPUVendorID::Intel;
	}
	else if (strcmp(cpuStr, "AuthenticAMD\0") == 0) {
		this->CPUType = FFXI::Constants::Enums::CPUVendorID::AMD;
	}
	else {
		for (int i = 0; i < 8; ++i) {
			if (cpuStr[i] == 'I') {
				if (memcmp(cpuStr + i, "Intel", 5) == 0) {
					this->CPUType = FFXI::Constants::Enums::CPUVendorID::Intel;
					break;
				}
			}
		}
	}

	//Leaf 1
	__cpuid(data, 1);
	this->CPU_MMX_Support = (data[3] & 0x800000) != 0;
	if (this->CPUType == FFXI::Constants::Enums::CPUVendorID::Intel) {
		this->CPU_SSE_Support = (data[3] & 0x2000000) != 0;

		//If family ID is 15 or LOWBYTE(Extended Family ID) is nonzero
		if ((data[0] & 0xF00) == 0xF00 || (data[0] & 0xF00000) != 0) {

			//If hyper threading is supported
			if ((data[3] & 0x10000000) == 0xF00000) {
				this->CPU_Threads_Per_Core = (data[1] >> 16) & 0xFF;
			}
		}

		if ((data[2] & 1) == 1) {
			this->CPU_SSE3_Support = 1;
		}

		//Leaf 2
		__cpuid(data, 2);
		//25H 	Cache 	3rd-level cache: 2 MBytes, 8-way set associative, 64 byte line size, 2 lines per sector.
		//29H 	Cache 	3rd - level cache : 4 MBytes, 8 - way set associative, 64 byte line size, 2 lines per sector.
		if ((unsigned char)data[3] == 0x25 || (unsigned char)data[3] == 0x29) {
			this->CPU_SSE3_Support = 1;
		}
	}
	else if (this->CPUType == FFXI::Constants::Enums::CPUVendorID::AMD) {
		this->CPU_3DNOW_Support = 0;

		__cpuid(data, (int)0x80000000);
		//If the highest calling parameter is not 0x80000000
		if (data[0] != (int)0x80000000) {
			__cpuid(data, (int)0x80000001);
			
			if ((data[3] & 0x80000000) != 0) {
				this->CPU_3DNOW_Support = 1;
			}
		}
	}
	//Don't see where cputype can be set to 3, but this is in the client
	else if ((int)this->CPUType == 3) {
		this->CPU_SSE_Support = (data[3] & 0x2000000) != 0;
	}
}

void FFXI::CYy::CEnv::DoSomething(float)
{
	this->field_18 = Config::MainRegistryConfig::dword104458E4;
	if (this->CheckField18()) {
		short v3 = this->field_1C + 1;
		if (v3 >= 4)
			v3 = 0;
		this->field_1C = v3;
	}
}

bool FFXI::CYy::CEnv::CheckField18()
{
	return this->field_18 != 0;
}
