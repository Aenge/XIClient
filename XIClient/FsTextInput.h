#pragma once
#include "FsWordCompletion.h"
#include "FsPhraseConstructor.h"
#include "FsTextBuffer.h"
#include "FsTextInputSetting.h"
#include "Class1.h"
namespace FFXI {
	namespace Input {
		class FsTextInput {
		public:
			static FsTextInput* g_fsTextInput;
			static void SystemInit();
			static void SystemClean();
			virtual ~FsTextInput();
			FsTextInput();
			void Setup();
			int convertToString(char*, unsigned int, char*, unsigned int, bool, bool);
			FsWordCompletion field_4;
			FsPhraseConstructor field_7E30;
			int field_84F4[365];
			FsTextBuffer field_8AAC;
			FsWordEntry field_8C88[0x3FC];
			int field_EC28;
			Class1 field_EC2C;
			int field_EC30[302];
			FsTextInputSetting field_F0E8;
		};
	}
}