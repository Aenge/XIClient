#pragma once
#include "CMoD3mElem.h"

namespace FFXI {
	namespace CYy {
		class CMoD3mSpecularElem : public CMoD3mElem {
		public:
			static const BaseGameObject::ClassInfo CMoD3mSpecularElemClass;
			CMoD3mSpecularElem();
			virtual const BaseGameObject::ClassInfo* GetRuntimeClass() override;
			virtual ~CMoD3mSpecularElem();
			virtual bool VirtElem1(FFXI::Constants::Enums::ElemType) override final;
			virtual void OnDraw() override final;
			virtual void OnCalc() override final;
			int field_198;
			int field_19C;
			int field_1A0;
			int field_1A4;
			int field_1A8;
			int field_1AC;
			int field_1B0;
			int field_1B4;
			int field_1B8;
			unsigned char field_1BC;
			unsigned char field_1BD;
			unsigned char field_1BE;
			unsigned char field_1BF;
		};
	}
}