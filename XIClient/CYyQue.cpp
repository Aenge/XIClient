#include "CYyQue.h"
using namespace FFXI::CYy;

const BaseGameObject::ClassInfo CYyQue::CYyQueClass{
	"CYyQue", sizeof(CYyQue), &BaseGameObject::BaseGameObjectClass
};

const BaseGameObject::ClassInfo* FFXI::CYy::CYyQue::GetRuntimeClass()
{
	return &CYyQue::CYyQueClass;
}

FFXI::CYy::CYyQue::CYyQue()
{
	this->Previous = nullptr;
}

FFXI::CYy::CYyQue::~CYyQue()
{
	//nullsub
}

void FFXI::CYy::CYyQue::Kill(float a2)
{
	switch (this->State) {
	case 0:
		this->State = 2;
		this->RemainingLifetime = a2;
		break;
	case 1:
		this->State = 3;
		this->BlendDuration = a2;
		this->RemainingLifetime = a2;
		break;
	case 2:
	case 3:
		if (a2 <= 1.0) {
			this->RemainingLifetime = 0.0;
			this->State = 2;
		}
		break;
	default:
		break;
	}
}

void FFXI::CYy::CYyQue::ControlBlendWeight()
{
	//nullsub
}
