#pragma once
namespace FFXI {
	namespace Constants {
		namespace Enums {
			enum class ActorType : unsigned char {
				ZERO  = 0,
				ONE   = 1,
				TWO   = 2,
				DOOR  = 3,
				LIFT  = 4,
				MODEL = 5,
				SIX   = 6,
				SEVEN = 7,
				EIGHT = 8
			};
		}
	}
}