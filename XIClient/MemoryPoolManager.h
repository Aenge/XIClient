#pragma once
namespace FFXI {
	namespace CYy { class MemoryBlockDescriptor; class BaseGameObject; }
	class MemoryPoolManager {
	public:
		//Forward declaration
		enum class MemoryPoolType;

		static MemoryPoolManager* globalInstance;
		static int CurrentZoneId;
		static bool Initialize();
		static void Uninitialize();
		virtual ~MemoryPoolManager();
		static void Clean123();
		static void Clean456();
		static CYy::MemoryBlockDescriptor* g_pCYyDataHead;
		static CYy::MemoryBlockDescriptor* g_pCYyDataTail;
		static CYy::MemoryBlockDescriptor* g_pCYyElemHead;
		static CYy::MemoryBlockDescriptor* g_pCYyElemTail;
		static CYy::MemoryBlockDescriptor* g_pCYyLoadHead;
		static CYy::MemoryBlockDescriptor* g_pCYyLoadTail;
		static CYy::MemoryBlockDescriptor* g_pCYyMenuHead;
		static CYy::MemoryBlockDescriptor* g_pCYyMenuTail;
		static CYy::MemoryBlockDescriptor* g_pCYyExHead;
		static CYy::MemoryBlockDescriptor* g_pCYyExTail;
		static void* Wrap(int, MemoryPoolManager::MemoryPoolType);
		static void Unwrap(void*);
		static void* WrapPlus12(int, MemoryPoolManager::MemoryPoolType);
		static void UnwrapPlus12(void*);

		bool Init();
		char* Get(int, MemoryPoolType);
		char* GetOrUpper(int, MemoryPoolType);
		int FindLargestOpenSlot(MemoryPoolType);
		void Delete(CYy::BaseGameObject*);
		void RunClean(int*);

		int SystemMemoryMB{ 0 };
		char* AlignedMemoryStart{ nullptr };
		char* AllocatedMemoryBlock{ nullptr };

		enum class MemoryPoolType {
			Upper = 0,
			Lower = 1,
			Elem = 2,
			Work = 3,
			Ex = 4,
			Load = 5,
			Menu = 6,
		};
	};
}