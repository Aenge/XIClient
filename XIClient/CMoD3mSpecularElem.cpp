#include "CMoD3mSpecularElem.h"
#include "CYyDb.h"
#include "CMoDx.h"

const FFXI::CYy::BaseGameObject::ClassInfo FFXI::CYy::CMoD3mSpecularElem::CMoD3mSpecularElemClass = {
	"CMoD3mSpecularElem", sizeof(CMoD3mSpecularElem), &FFXI::CYy::CMoElem::CMoElemClass
};

FFXI::CYy::CMoD3mSpecularElem::CMoD3mSpecularElem()
{
	this->field_176 = 448;
	this->field_1A8 = 0;
	this->field_198 &= 0xF7;
	if (FFXI::CYyDb::g_pCYyDb->g_pCMoDx->field_DCC.MaxSimultaneousTextures <= 2) {
		this->field_1B8 = 0;
	}
	else if ((FFXI::CYyDb::g_pCYyDb->g_pCMoDx->field_DCC.PrimitiveMiscCaps & D3DPMISCCAPS_TSSARGTEMP) != 0)
	{
		this->field_1B8 = 3;
	}
	else {
		this->field_1B8 = 2;
	}

	switch (this->field_1B8) {
	case 0:
		this->field_1BD = 1;
		this->field_1BC = 0;
		break;
	case 1:
		this->field_1BD = 0;
		this->field_1BC = 1;
		break;
	case 2:
	[[fallthrough]];
	case 3:
		this->field_1BD = 2;
		this->field_1BC = 0;
		break;
	default:
		break;
	}
 }

const FFXI::CYy::BaseGameObject::ClassInfo* FFXI::CYy::CMoD3mSpecularElem::GetRuntimeClass()
{
	return &CMoD3mSpecularElem::CMoD3mSpecularElemClass;
}

FFXI::CYy::CMoD3mSpecularElem::~CMoD3mSpecularElem()
{
	//nullsub
}

bool FFXI::CYy::CMoD3mSpecularElem::VirtElem1(FFXI::Constants::Enums::ElemType a1)
{
	return a1 == FFXI::Constants::Enums::ElemType::Specular;
}

void FFXI::CYy::CMoD3mSpecularElem::OnDraw()
{
	if (this->res == nullptr) {
		return;
	}

	if (this->MMBFlag == true) {
		this->DoMMBDraw();
	}
	else {
		exit(0x1004049B);
	}
}

void FFXI::CYy::CMoD3mSpecularElem::OnCalc()
{
	this->field_10C = this->field_10C & 0xFDFEFFFF | 0x2000000;
	if (this->MMBFlag == true) {
		return;
	}

	exit(0x1004034A);
}
