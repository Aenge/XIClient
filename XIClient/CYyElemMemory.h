#pragma once
#include "MemoryBlockDescriptor.h"

namespace FFXI {
	namespace CYy {
		class CYyElemMemory : public MemoryBlockDescriptor {
		public:
			const static BaseGameObject::ClassInfo CYyElemMemoryClass;
			virtual const BaseGameObject::ClassInfo* GetRuntimeClass() override final;
		};
	}
}