#pragma once
#include "BaseGameObject.h"
#include "FVF144Vertex.h"

namespace FFXI {
	namespace CYy {
		class CYyTex;
		class CYyIcon : public BaseGameObject {
		public:
			const static BaseGameObject::ClassInfo CYyIconClass;
			virtual const BaseGameObject::ClassInfo* GetRuntimeClass() override final;
			CYyIcon(int, unsigned char*);
			~CYyIcon();
			int field_4;
			CYyTex* field_8;
			FVF144Vertex Vertices[4];
		};
	}
}