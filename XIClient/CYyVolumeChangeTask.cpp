#include "CYyVolumeChangeTask.h"
#include "CYySoundElem.h"
#include "CYyDb.h"

FFXI::CYy::CYyVolumeChangeTask::CYyVolumeChangeTask(float a2, int a3, CYyVolumeChangeTask::VolumeChangeType a4)
{
	this->Type = a4;
	switch (this->Type) {
	case VolumeChangeType::SYSTEM:
		if (CYySoundElem::system_vol_change_task != nullptr) {
			delete CYySoundElem::system_vol_change_task;
		}
		CYySoundElem::system_vol_change_task = this;
		this->field_40 = CYySoundElem::system_volume;
		break;
	case VolumeChangeType::EFFECT:
		if (CYySoundElem::effect_vol_change_task != nullptr) {
			delete CYySoundElem::effect_vol_change_task;
		}
		CYySoundElem::effect_vol_change_task = this;
		this->field_40 = CYySoundElem::effect_volume;
		break;
	case VolumeChangeType::ZONE:
		if (CYySoundElem::zone_vol_change_task != nullptr) {
			delete CYySoundElem::zone_vol_change_task;
		}
		CYySoundElem::zone_vol_change_task = this;
		this->field_40 = CYySoundElem::zone_volume;
		break;
	case VolumeChangeType::UNK:
		if (CYySoundElem::unknown_vol_change_task != nullptr) {
			delete CYySoundElem::zone_vol_change_task;
		}
		CYySoundElem::zone_vol_change_task = this;
		this->field_40 = 1.0;
		break;
	default:
		break;
	}

	this->Volume = a2;
	this->field_38 = (float)a3;
	this->field_3C = (float)a3;
}

FFXI::CYy::CYyVolumeChangeTask::~CYyVolumeChangeTask()
{
	switch (this->Type) {
	case VolumeChangeType::SYSTEM:
		CYySoundElem::SetSystemVolume(this->Volume);
		CYySoundElem::system_vol_change_task = nullptr;
		break;
	case VolumeChangeType::EFFECT:
		CYySoundElem::SetEffectVolume(this->Volume);
		CYySoundElem::effect_vol_change_task = nullptr;
		break;
	case VolumeChangeType::ZONE:
		CYySoundElem::SetZoneVolume(this->Volume);
		CYySoundElem::zone_vol_change_task = nullptr;
		break;
	case VolumeChangeType::UNK:
		CYySoundElem::SetUnknownVolume(this->Volume);
		CYySoundElem::unknown_vol_change_task = nullptr;
		//statics set here //TODO
		exit(0x10034D66);
		break;
	default:
		break;
	}
}

char FFXI::CYy::CYyVolumeChangeTask::OnMove()
{
	this->field_38 -= FFXI::CYyDb::g_pCYyDb->CheckTick();
	if (this->field_38 <= 0) {
		delete this;
		return 1;
	}

	float new_volume = (1.0 - this->field_38 / this->field_3C) * (this->Volume - this->field_40) + this->field_40;
	switch (this->Type) {
	case VolumeChangeType::SYSTEM:
		CYySoundElem::SetSystemVolume(new_volume);
		break;
	case VolumeChangeType::EFFECT:
		CYySoundElem::SetEffectVolume(new_volume);
		break;
	case VolumeChangeType::ZONE:
		CYySoundElem::SetZoneVolume(new_volume);
		break;
	case VolumeChangeType::UNK:
		CYySoundElem::SetUnknownVolume(new_volume);
		break;
	default:
		break;
	}

	return 0;
}
