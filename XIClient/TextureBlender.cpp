#include "TextureBlender.h"
#include "Globals.h"
#include "CDx.h"
#include "CEnv.h"
#include "FVF144Vertex.h"

using namespace FFXI::CYy;

bool TextureBlender::some_flag{ false };
D3DCOLOR TextureBlender::some_color{};
void FFXI::CYy::TextureBlender::SetStatics(D3DCOLOR a2, bool a3)
{
	some_color = a2;
	some_flag = a3;
}
FFXI::CYy::TextureBlender::TextureBlender()
{
	this->field_8[0] = Globals::g_pSomeTexture1;
	if (this->field_8[0] != nullptr)
		this->field_8[0]->AddRef();

	this->field_8[1] = Globals::g_pSomeTexture3;
	if (this->field_8[1] != nullptr)
		this->field_8[1]->AddRef();

	this->field_8[2] = Globals::g_pSomeTexture2;
	if (this->field_8[2] != nullptr)
		this->field_8[2]->AddRef();

	this->field_8[3] = Globals::g_pSomeTexture4;
	if (this->field_8[3] != nullptr)
		this->field_8[3]->AddRef();

	this->field_4 = false;
	TextureBlender::some_flag = false;
}

FFXI::CYy::TextureBlender::~TextureBlender()
{
	for (int i = 0; i < sizeof(this->field_8) / sizeof(this->field_8[0]); ++i) {
		if (this->field_8[i] != nullptr) {
			this->field_8[i]->Release();
			this->field_8[i] = nullptr;
		}
	}
}

void FFXI::CYy::TextureBlender::PrepareViewport()
{
	if (this->field_8[0] == nullptr)
		return;

	if (TextureBlender::some_flag == false) {
		if ((TextureBlender::some_color & 0xFF000000) == 0x80000000)
			return;
	}

	if (this->field_4 == true) {
		if (FFXI::CYy::CDx::instance->ViewportIndex == 0) {
			FFXI::CYy::CDx::instance->AddViewportAtOrigin(this->field_8[0], this->field_8[2], FFXI::CYy::CDx::instance->StencilDepthSurface);
		}
		else {
			FFXI::CYy::CDx::instance->AddViewportAtOrigin(this->field_8[0], this->field_8[2], FFXI::CYy::CDx::instance->Stencils[FFXI::CYy::CDx::instance->ViewportIndex]);
		}
	}
	else {
		if (Globals::g_pCenv->CheckField18() == true) {
			FFXI::CYy::CDx::instance->AddViewportAtOrigin(this->field_8[1], this->field_8[3], nullptr);
			FFXI::CYy::CDx::instance->ClearViewport(0, nullptr, 1, 0, 1.0, 0);
			FFXI::CYy::CDx::instance->RevertStage();
		}
		if (FFXI::CYy::CDx::instance->ViewportIndex == 0) {
			FFXI::CYy::CDx::instance->AddViewportAtOrigin(this->field_8[0], this->field_8[2], FFXI::CYy::CDx::instance->StencilDepthSurface);
		}
		else {
			FFXI::CYy::CDx::instance->AddViewportAtOrigin(this->field_8[0], this->field_8[2], FFXI::CYy::CDx::instance->Stencils[FFXI::CYy::CDx::instance->ViewportIndex]);
		}
		FFXI::CYy::CDx::instance->ClearViewport(0, nullptr, 1, 0, 1.0, 0);
		this->field_4 = true;
	}
}

void FFXI::CYy::TextureBlender::CleanViewport()
{
	if (this->field_8[0] == nullptr)
		return;

	if (TextureBlender::some_flag == false) {
		if ((TextureBlender::some_color & 0xFF000000) == 0x80000000)
			return;
	}

	FFXI::CYy::CDx::instance->RevertStage();
}

void FFXI::CYy::TextureBlender::Render()
{
	if (this->field_8[0] == nullptr)
		return;

	if (this->field_4 == false)
		return;

	if (TextureBlender::some_flag == false) {
		if ((TextureBlender::some_color & 0xFF000000) == 0x80000000) {
			return;
		}
	}

	CDx* cdx = CYy::CDx::instance;
	D3DVIEWPORT8* top_viewport = cdx->GetTopViewport();
	FVF144Vertex verts[4]{};
	verts[0].X = 0.0;
	verts[0].Y = 0.0;
	verts[0].Z = 0.0;
	verts[0].RHW = 1.0;
	verts[0].DiffuseColor = TextureBlender::some_color;
	verts[0].TexVertX = 0.0;
	verts[0].TexVertY = 0.0;

	verts[1].X = (float)top_viewport->Width;
	verts[1].Y = 0.0;
	verts[1].Z = 0.0;
	verts[1].RHW = 1.0;
	verts[1].DiffuseColor = TextureBlender::some_color;
	verts[1].TexVertX = 1.0;
	verts[1].TexVertY = 0.0;
	
	verts[2].X = 0.0;
	verts[2].Y = (float)top_viewport->Height;
	verts[2].Z = 0.0;
	verts[2].RHW = 1.0;
	verts[2].DiffuseColor = TextureBlender::some_color;
	verts[2].TexVertX = 0.0;
	verts[2].TexVertY = 1.0;

	verts[3].X = (float)top_viewport->Width;
	verts[3].Y = (float)top_viewport->Height;
	verts[3].Z = 0.0;
	verts[3].RHW = 1.0;
	verts[3].DiffuseColor = TextureBlender::some_color;
	verts[3].TexVertX = 1.0;
	verts[3].TexVertY = 1.0;

	cdx->DXDevice->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_GREATER);
	cdx->DXDevice->SetRenderState(D3DRS_ALPHAREF, 0);
	cdx->DXDevice->SetRenderState(D3DRS_ZWRITEENABLE, false);
	cdx->DXDevice->SetRenderState(D3DRS_ZENABLE, false);
	cdx->DXDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, true);
	cdx->DXDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	cdx->DXDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
	cdx->DXDevice->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_MODULATE2X);
	cdx->DXDevice->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_DIFFUSE);
	cdx->DXDevice->SetTextureStageState(0, D3DTSS_COLORARG2, D3DTA_TEXTURE);
	cdx->DXDevice->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_MODULATE2X);
	cdx->DXDevice->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_DIFFUSE);
	cdx->DXDevice->SetTextureStageState(0, D3DTSS_ALPHAARG2, D3DTA_TEXTURE);
	cdx->DXDevice->SetTextureStageState(1, D3DTSS_ALPHAOP, D3DTOP_DISABLE);
	cdx->DXDevice->SetTextureStageState(1, D3DTSS_COLOROP, D3DTOP_DISABLE);
	cdx->DXDevice->SetVertexShader(verts->FVF);

	char backup_828 = cdx->field_828;
	cdx->field_828 = 0;
	if (Globals::g_pCenv->CheckField18() == true) {
		cdx->DXDevice->SetTexture(0, this->field_8[0]);
		cdx->DrawVerts(D3DPT_TRIANGLESTRIP, 2, verts, sizeof verts[0]);
		if (this->field_8[1] != nullptr) {
			if (cdx->CAcc2.Texture[1] != nullptr && cdx->CAcc2.Surface2 != nullptr) {
				cdx->DXDevice->SetTexture(0, this->field_8[1]);
				cdx->AddViewportAtOrigin(cdx->CAcc2.Texture[1], cdx->CAcc2.Texture3, cdx->CAcc2.Surface2);
				cdx->DrawVerts(D3DPT_TRIANGLESTRIP, 2, verts, sizeof verts[0]);
				cdx->RevertStage();
			}
		}
	}

	cdx->DXDevice->SetTexture(0, this->field_8[0]);
	cdx->DrawVerts(D3DPT_TRIANGLESTRIP, 2, verts, sizeof verts[0]);
	cdx->field_828 = backup_828;
	cdx->DXDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, false);
	cdx->DXDevice->SetRenderState(D3DRS_ALPHAREF, 0x60);
	cdx->DXDevice->SetRenderState(D3DRS_ZWRITEENABLE, true);
	cdx->DXDevice->SetRenderState(D3DRS_ZENABLE, true);
	cdx->DXDevice->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_ALWAYS);
	this->field_4 = 0;
}
