#pragma once
#include "MemoryManagedObject.h"
#include "WMatrix.h"
#include "ActorPosHistory.h"
namespace FFXI {
	namespace CYy {
		class CXiActor;
		class CMoCameraTask;
		class CYyCamMng2 : public MemoryManagedObject {
		public:
			const static BaseGameObject::ClassInfo CYyCamMng2Class;
			virtual const BaseGameObject::ClassInfo* GetRuntimeClass() override final;
			virtual void VObj2(int*) override final;
			static void InitCameraManager();
			static void SetSomeValues(char, char, int);
			static void ResetCameraInitTimer();
			static void GetDefaultCamera(D3DXVECTOR3*, D3DXVECTOR3*);
			static bool SomeKeyCheck();
			static void ClearSomeInt();
			static int GetSomeInt();
			static char SomeByte1;
			static char SomeByte2;
			static int SomeDword1;
			static int InitTimer;
			static int SomeInt;
			static float TransModeTimer;
			static CMoCameraTask* CurrentCameraTask;
			static float PrevYDelta;
			static float NoticeModeHeight;
			static int IsControled;
			static int SomeCounter;
			static int SomeCounter2;
			static D3DXVECTOR3 ActorPrevPos;
			static D3DXVECTOR3 ActorPrevPos2;
			static bool IsTracking;
			static bool ResetCamera;
			CYyCamMng2();
			~CYyCamMng2();
			void Init();
			void SomeInit();
			void Update();
			void SetPos(D3DXVECTOR3*);
			void SetAt(D3DXVECTOR3*);
			void SetCameraPos(D3DXVECTOR3*, CXiActor*);
			bool Check();
			void Follow(CXiActor*);
			FFXI::Math::WMatrix* MakeMatrix();
			FFXI::Math::WMatrix* GetView();
			void MakeMatrixWithAngle(float);
			FFXI::Math::WMatrix field_4;
			D3DXVECTOR3 Position;
			D3DXVECTOR3 field_50;
			float field_5C;
			D3DXVECTOR3 field_60;
			D3DXVECTOR3 field_6C;
			float field_78;
			float field_7C;
			float field_80;
			float field_84;
			char field_88;
			char field_89;
			char field_8A;
			char field_8B;
			D3DXVECTOR3 field_8C;
			D3DXVECTOR3 field_98;
			float field_A4;
			D3DXVECTOR3 field_A8;
			float field_B4;
			float field_B8;
			ActorPosHistory field_BC;
			char field_F4;
			char field_F5;
			char field_F6;
			char field_F7;
			char field_F8;
			char field_F9;
			char field_FA;
			char field_FB;
			char field_FC;
			char field_FD;
			char field_FE;
			char field_FF;
			char field_100;
			char field_101;
			char field_102;
			char field_103;
			char field_104;
			char field_105;
			char field_106;
			char field_107;
			char field_108;
			char field_109;
			char field_10A;
			char field_10B;
			char field_10C;
			char field_10D;
			char field_10E;
			char field_10F;
			char field_110;
			char field_111;
			char field_112;
			char field_113;
			char field_114;
			char field_115;
			char field_116;
			char field_117;
		};
	}
}