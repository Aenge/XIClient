#include "MotionReferenceRead.h"
#include "CXiSkeletonActor.h"
#include "CMoResourceMng.h"
using namespace FFXI;

void FFXI::File::MotionReferenceRead::ReadCallback(CYy::ResourceContainer** a1, ReferenceReadBase* a2)
{
	if (CYy::CMoResourceMng::IsResourceReady((CYy::CMoResource***) &a1) == true) {
		CYy::CXiSkeletonActor* actor = (CYy::CXiSkeletonActor*)a2->field_4.GetActor();
		if (actor != nullptr) {
			actor->AddMotRes((CYy::CMoResource**)a1, a2->ActorIndex);
		}
	}
	if (a2 != nullptr) {
		MotionReferenceRead* read = (MotionReferenceRead*)a2;
		delete read;
	}
}
