#include "FsTextInput.h"

FFXI::Input::FsTextInput* FFXI::Input::FsTextInput::g_fsTextInput{};

void FFXI::Input::FsTextInput::SystemInit()
{
	g_fsTextInput = new FsTextInput();
}

void FFXI::Input::FsTextInput::SystemClean()
{
	if (g_fsTextInput != nullptr) {
		delete g_fsTextInput;
		g_fsTextInput = nullptr;
	}
}

FFXI::Input::FsTextInput::~FsTextInput()
{
}

FFXI::Input::FsTextInput::FsTextInput()
	: field_EC2C(0, 0, 0, 0)
{
	//nullsub
}

void FFXI::Input::FsTextInput::Setup()
{
}

int FFXI::Input::FsTextInput::convertToString(char* a2, unsigned int a3, char* a4, unsigned int a5, bool a6, bool a7)
{
	return this->field_7E30.convertPhrase(a2, a3, a4, a5, a6, a7);
}
