#include "MapCollisionData.h"
#include "RidListNode.h"
#include "RidStruct.h"
#include "CTsZoneMapSubStruct1.h"
#include <string>
#include "MiscFuncs.h"
#include "PlaceHolders.h"
#include "BaseGameObject.h"
#include "MemoryPoolManager.h"
using namespace FFXI::CYy;

bool CheckPolygonInnner(D3DXVECTOR3* a1, D3DXVECTOR3* a2, D3DXVECTOR3* a3, D3DXVECTOR3* a4, D3DXVECTOR3* a5)
{
	D3DXVECTOR3 v5 = *a2 - *a1;
	D3DXVECTOR3 v8 = *a4 - *a1;
	D3DXVECTOR3 product{};
	D3DXVec3Cross(&product, &v5, &v8);
	product.x *= a5->x;
	product.y *= a5->y;
	product.z *= a5->z;

	if (product.x > 0.0001f) {
		return false;
	}

	if (product.y > 0.0001f) {
		return false;
	}

	if (product.z > 0.0001f) {
		return false;
	}

	v5 = *a3 - *a2;
	v8 = *a4 - *a2;
	D3DXVec3Cross(&product, &v5, &v8);
	product.x *= a5->x;
	product.y *= a5->y;
	product.z *= a5->z;

	if (product.x > 0.0001f) {
		return false;
	}

	if (product.y > 0.0001f) {
		return false;
	}

	if (product.z > 0.0001f) {
		return false;
	}

	v5 = *a1 - *a3;
	v8 = *a4 - *a3;
	D3DXVec3Cross(&product, &v5, &v8);
	product.x *= a5->x;
	product.y *= a5->y;
	product.z *= a5->z;

	if (product.x > 0.0001f) {
		return false;
	}

	if (product.y > 0.0001f) {
		return false;
	}

	if (product.z > 0.0001f) {
		return false;
	}

	return true;
}

int FFXI::CYy::MapCollisionData::KO_CharaCollision(D3DXVECTOR3* a2, D3DXVECTOR3* a3, D3DXVECTOR3* a4)
{
	D3DXVECTOR3 v81 = { 0.0f, -1.0f, 0.0f };
	D3DXVECTOR3 v72 = *a2;
	v72.y -= 0.5f;
	D3DXVECTOR3 va3 = *a3;
	va3.y -= 0.5f;

	if (v72.y == va3.y) {
		va3.y += 0.001f;
	}

	if (v72.x == va3.x && v72.z == va3.z) {
		v81.y = 0.0f;
		v81.z = 1.0f;
	}

	D3DXVECTOR3 v85 = va3 - v72;

	FFXI::Math::WMatrix v83{};
	v83.Vec3CharCollisionHelper(&v72, &v85, &v81);

	this->field_3416C._11 = v83._11;
	this->field_3416C._12 = v83._21;
	this->field_3416C._13 = v83._31;
	this->field_3416C._14 = 0.0f;
	this->field_3416C._21 = v83._12;
	this->field_3416C._22 = v83._22;
	this->field_3416C._23 = v83._32;
	this->field_3416C._24 = 0.0f;
	this->field_3416C._31 = v83._13;
	this->field_3416C._32 = v83._23;
	this->field_3416C._33 = v83._33;
	this->field_3416C._34 = 0.0f;
	this->field_3416C._41 = -(v83._13 * v83._43 + v83._12 * v83._42 + v83._41 * v83._11);
	this->field_3416C._42 = -(v83._23 * v83._43 + v83._22 * v83._42 + v83._21 * v83._41);
	this->field_3416C._43 = -(v83._33 * v83._43 + v83._32 * v83._42 + v83._31 * v83._41);
	this->field_3416C._44 = v83._44;

	float v75 = std::min(v72.y, va3.y);
	float v79 = std::max(v72.y, va3.y);
	v75 -= 0.5f;
	v79 += 0.5f;

	this->field_34128 = 0;
	D3DXVECTOR3 diff3 = v72 - va3;
	D3DXVECTOR3 v82{};
	v82.x = 0.5f;
	v82.y = 0.5f;
	v82.z = sqrt(diff3.x * diff3.x + diff3.y * diff3.y + diff3.z * diff3.z);
	
	int v28 = (int)((v72.x + this->field_40) * 0.25f);
	if (v28 < 0	|| v28 >= this->field_48) {
		return 0;
	}
	int v30 = (int)((v72.z + this->field_44) * 0.25f);
	if (v30 < 0 || v30 >= this->field_4C) {
		return 0;
	}
	
	int v13{}, v31{}, v71{}, v74{};
	if (this->field_34139 != 0) {
		v74 = (v28 < 2) ? 0 : std::min(v28 - 2, this->field_48 - 1);
		v71 = (v28 < -2) ? 0 : std::min(v28 + 2, this->field_48 - 1);
		v13 = (v30 < 2) ? 0 : std::min(v30 - 2, this->field_4C - 1);
		v31 = v30 + 2;
	}
	else {
		v74 = (v28 < 1) ? 0 : std::min(v28 - 1, this->field_48 - 1);
		v71 = (v28 < -1) ? 0 : std::min(v28 + 1, this->field_48 - 1);
		v13 = (v30 < 1) ? 0 : std::min(v30 - 1, this->field_4C - 1);
		v31 = v30 + 1;
	}

	v31 = (v31 < 0) ? 0 : std::min(v31, this->field_4C - 1);
	this->field_34120 = 0;

	for (int d1 = v13; d1 <= v31; ++d1) {
		for (int d2 = v74; d2 <= v71; ++d2) {
			int* v34 = (int*)this->field_60[d2 + d1 * this->field_48];
			while (v34 != nullptr) {
				int* v35 = v34 + 1;
				int v36 = *v34;
				int v37 = v36 & 0x7FF;
				if ((v36 & 0x2000) != 0)
				{
					v35 += 2 * v37;
				}
				else
				{
					int* v84 = v35;
					for (int d3 = v37; d3 > 0; --d3) {
						unsigned int* v38 = (unsigned int*)v35[1];
						if (v38 != nullptr) {
							if ((*(unsigned char*)(v35[0] + 164) & 2) != 2) {
								float* v41 = (float*)v35[0];
								if (v41[45] <= v79
									&& v41[46] >= v75
									&& this->field_4 != *(unsigned int*)(v35[0] + 188))
								{
									int count = *((__int16*)v38 + 6);
									for (int d4 = 0; d4 < count; ++d4)
									{
										unsigned int v45 = v38[2] + 8 * d4;
										if ((*(unsigned char*)(v45 + 3) & 0x40) == 0)
										{
											this->SetThings(&v83, v41, (int*)v38, (unsigned short*)v45, &v82);
										}
									}
								}
							}
						}
						v35 += 2;
					}

					int* v35 = v84;
					for (int d3 = v37; d3 > 0; --d3) {
						unsigned int* v48 = (unsigned int*)v35[1];
						if (v48 != nullptr) {
							float* v49 = (float*)v35[0];
							if ((*(unsigned char*)(*v35 + 164) & 2) != 0)
							{
								if (v49[45] <= v79 && v49[46] >= v75 && this->field_4 != *((unsigned int*)v49 + 47)) {
									int count = *((__int16*)v48 + 6);
									for (int d4 = 0; d4 < count; ++d4)
									{
										unsigned int v55 = v48[2] + 8 * d4;
										if ((*(unsigned char*)(v55 + 3) & 0x40) == 0)
											this->SetThings(&v83, v49, (int*)v48, (unsigned short*)v55, &v82);
									}
								}
							}
						}
						v35 += 2;
					}
				}

				v34 = (int*)v35[0];
			}
		}
	}
	
	if (Placeholder::g_pYkMyroom != nullptr) {
		exit(0x1016850E);
	}

	if (this->KO_ExecSpCorrect(&va3, &v82) == true) {
		a4->x = va3.x;
		a4->y = va3.y + 0.5f;
		a4->z = va3.z;
	}
	else {
		*a4 = *a2;
	}
	return this->field_34128;
}

FFXI::CYy::MapCollisionData::MapCollisionData()
{
	memset(&this->field_8, 0, 0x6C);
	this->field_198FC = 0;
	this->field_1807C = 0;
	this->field_3413C = { 0.0f, 0.0f, 0.0f };
	this->field_34148 = 0;
	this->field_3414C = 0;
	this->field_34150 = 0;
	this->field_34158 = 0;
	this->field_34154 = 0;
	this->field_3415C = 0;
	this->field_3415D = 0;
	this->field_3415E = 0;
	this->field_3415F = 0;

	//set some statics
	//sub //TODO

	this->field_34138 = 0;
	this->field_34139 = 0;
	this->field_3413A = 0;
	this->field_3413B = 0;
}

FFXI::CYy::MapCollisionData::~MapCollisionData()
{
	this->Clean();
}

void FFXI::CYy::MapCollisionData::PrepareData(CTsZoneMapSubStruct1* a2, char* a3, int MzbVersion, char a5)
{
	int v43{ 0 };
	bool v6{ false };
	this->field_34138 = a5 & 1;
	this->field_34139 = (a5 & 4) != 0;
	this->field_3413A = (a5 & 8) != 0;
	this->field_3413B = (a5 & 16) != 0;

	this->field_1807C = 0;
	this->MzbVersion = MzbVersion;
	if (MzbVersion <= 0x19)
		this->field_70 = 128;
	else
		this->field_70 = 192;

	this->field_30 = a2->field_10;
	this->field_34 = a2->field_14;
	this->field_38 = a2->field_18;
	this->field_3C = a2->field_1C;

	int v9 = this->field_30 * this->field_38;
	this->field_40 = (float)v9 * 0.5f;
	this->field_48 = v9 / 4;

	int v10 = this->field_34 * this->field_3C;
	this->field_44 = (float)v10 * 0.5f;
	this->field_4C = v10 / 4;

	if (a2->field_C) {
		this->field_50 = a2->field_C[0];
		this->field_54 = (int*)((char*)a2->data + a2->field_C[1]);
		this->field_58 = a2->field_C[2];
		this->field_5C = (int*)((char*)a2->data + a2->field_C[3]);
		this->field_64 = a2->field_C[6];
		this->field_68 = (int*)((char*)a2->data + a2->field_C[5]);
		if (a2->field_0 == 1)
			this->field_60 = (int*)((char*)a2->data + a2->field_C[4]);

		if (this->field_50 > 0) {
			int* v12 = this->field_54;
			for (int i = 0; i < this->field_50; ++i) {
				v12[0] += (int)a2->data;
				v12[1] += (int)a2->data;
				v12[2] += (int)a2->data;

				short* v17 = (short*)(v12[2]);
				short someCount = *((short*)v12 + 6);
				if (someCount > 0) {
					for (int j = 0; j < someCount; ++j) {
						float* v18 = (float*)(v12[1] + 12 * (v17[3] & 0x7FFF));
						int* nanCheck = (int*)v18;
						if (nanCheck[0] == 0xFFC00000) {
							v18[0] = 0.0;
							v43 = 1;
						}
						if (nanCheck[1] == 0xFFC00000) {
							v18[1] = 0.0;
							v43 = 1;
						}
						if (nanCheck[2] == 0xFFC00000) {
							v18[2] = 0.0;
							v43 = 1;
						}
						
						if (v18[0] == 0.0 && v18[1] == 0.0 && v18[2] == 0.0) {
							v18[1] = 0.000001f;
							v43 = 1;
						}

						float* v19 = (float*)(v12[0] + 12 * (v17[0] & 0x7FFF));
						float* v20 = (float*)(v12[0] + 12 * (v17[1] & 0x3FFF));
						float* v21 = (float*)(v12[0] + 12 * (v17[2] & 0x3FFF));
					
						float va = v19[0] - v20[0];
						float vb = v19[1] - v20[1];
						float vc = v19[2] - v20[1];

						float v45 = v20[0] - v21[0];
						float v44 = v20[1] - v21[1];
						float v22 = v20[2] - v21[2];

						float v48 = v21[0] - v19[0];
						float v47 = v21[1] - v19[1];
						float v24 = v21[2] - v19[2];
						v17[1] &= 0xBFFF;

						if (va * va + vb * vb + vc * vc == 0.0
							|| v22 * v22 + v44 * v44 + v45 * v45 == 0.0
							|| v24 * v24 + v47 * v47 + v48 * v48 == 0.0) 
							v17[1] |= 0x40;
						
						v17 += 4;
					}
				}

				v12 = (int*)(v12[2] + 8 * someCount);
			}
		}

		int* v28 = this->field_5C;
		if (this->field_58 > 0) {
			for (int i = 0; i < this->field_58; ++i) {
				int* v29 = v28 + 1;
				int v30 = *v28 & 0x7FF;
				if (v30 != 0) {
					for (int j = 0; j < v30; ++j) {
						if (*v29)
							*v29 += (int)a2->data;
						else
							v6 = true;
						if (v29[1])
							v29[1] += (int)a2->data;
						else
							v6 = true;
						v29 += 2;
					}
				}
				v28 = v29 + 1;
			}
			if (v6 == true) {
				//"There is an error in the offset of the data" JP note
				throw "MapCollisionData::PrepareData: Data offset error";
			}
		}

		int* v33 = this->field_68;
		unsigned char* v33c = (unsigned char*)v33;
		if (this->field_64 > 0) {
			if (v33[42])
				v33[42] += (int)a2->data;
			if (v33c[172]) {
				if ((v33[41] & 0x10000000) == 0) {
					v33[41] |= 0x10000000;
					v33c[172] = a3[v33c[172] - 1] + 1;
				}
			}
			
			if (v33c[173]) {
				if ((v33[41] & 0x20000000) == 0) {
					v33[41] |= 0x20000000;
					v33c[173] = a3[v33c[173] - 1] + 1;
				}
			}

			if (v33c[174]) {
				if ((v33[41] & 0x40000000) == 0) {
					v33[41] |= 0x40000000;
					v33c[174] = a3[v33c[174] - 1] + 1;
				}
			}

			if (v33c[175]) {
				if ((v33[41] & 0x80000000) == 0) {
					v33[41] |= 0x80000000;
					v33c[175] = a3[v33c[175] - 1] + 1;
				}
			}

			v33 = (int*)(v33c + this->field_70);
		}

		if (a2->field_0 == 1) {
			int* v39 = this->field_60;
			int v40 = this->field_4C * this->field_48;
			for (int j = 0; j < v40; ++j) {
				if (v39[j])
					v39[j] += (int)a2->data;
			}
		}

		this->field_8 = a2->data;
	}
}

void FFXI::CYy::MapCollisionData::Clean()
{
	if (this->field_14 != nullptr) {
		FFXI::MemoryPoolManager::Unwrap(this->field_14);
	}

	if (this->field_1C != nullptr) {
		FFXI::MemoryPoolManager::Unwrap(this->field_1C);
	}

	if (this->field_24 != nullptr) {
		FFXI::MemoryPoolManager::Unwrap(this->field_24);
	}
		
	memset(&this->field_8, 0, 0x6C);
}

void FFXI::CYy::MapCollisionData::InitRid(RidListNode* a2)
{
	for (int i = 0; i < a2->field_10; ++i) {
		RidStruct* rs = a2->field_C + i;

		if (rs->field_D8 != nullptr) {
			for (int j = 0; j < rs->field_D4; ++j) {
				RidUnderscoreStruct* ris = rs->field_D8 + j;
				int v6 = (int)((ris->field_10 + this->field_40) * 0.25);
				int v7 = (int)((ris->field_14 + this->field_44) * 0.25);
				ris->field_0 = (v7 << 23) ^ ris->field_0 & 0x17FF ^ ((v6 & 0x1FF) << 14) | 0x800;
				int** v8 = (int**)(this->field_60 + v6 + v7 * this->field_48);
				for (int* k = *v8; *v8; k = *v8)
					v8 = (int**)(k + 2 * (*k & 0x7FF) + 1);
				*v8 = (int*)ris;
			}
		}
	}
}

int FFXI::CYy::MapCollisionData::CheckCharCollision(D3DXVECTOR3* a2, D3DXVECTOR3* a3, D3DXVECTOR3* a4)
{
	D3DXVECTOR3 diff = *a3 - *a2;
	int v19 = 1 - (int)(sqrtf(diff.z * diff.z + diff.x * diff.x) * -0.25f);
	if (v19 < 2) {
		return this->KO_CharaCollision(a2, a3, a4);
	}

	float v11 = 1.0f / (float)v19;
	D3DXVECTOR3 v13 = *a2;
	D3DXVECTOR3 v14{}, v15{};
	v15 = diff * v11;
	float v16 = 0.0f;
	while (true) {
		v14 = v13 + v15;
		v16 += v11;
		if (v16 >= 1.0f) {
			return KO_CharaCollision(&v13, a3, a4);
		}
		else if (KO_CharaCollision(&v13, &v14, a4)) {
			return 1;
		}
		v13 = v14;
	}
}

bool FFXI::CYy::MapCollisionData::CheckCharCollisionFast(D3DXVECTOR3* a2, D3DXVECTOR3* a3, char a4, float a5, float a6, float a7)
{
	D3DXVECTOR3 v20 = *a2;
	D3DXVECTOR3 v22 = *a2;
	D3DXVECTOR3 v21{};

	v22.y += a6;
	
	float v12{}, v13{};
	if (a4 != 0) {
		v20.y -= a5;
		bool result = this->FastCheck1(&v20, &v22, &v21);
		*a3 = *a2;
		if (result == false) {
			return false;
		}
		v12 = v21.y - (a2->y - a5);
		if (v12 < 0.0f) {
			v12 = -v12;
		}
		v13 = a7 + 2.0f;
	}
	else {
		v20.y -= 0.5f;
		bool result = this->FastCheck2(&v20, &v22, &v21);
		*a3 = *a2;
		if (result == false) {
			return false;
		}
		v12 = v21.y - (a2->y - 0.5f);
		if (v12 < 0.0f) {
			v12 = -v12;
		}
		v13 = a7 + 0.5f;
	}

	if (v12 > v13) {
		this->field_34148 = 0;
	}

	a3->y = v21.y;
	return true;
}

bool FFXI::CYy::MapCollisionData::VCalibrate2(D3DXVECTOR3* a2, D3DXVECTOR3* a3, D3DXVECTOR3* r)
{
	this->field_1807C = 0;

	if (this->field_60 == nullptr) {
		return false;
	}

	int v9 = (int)((this->field_40 + a2->x) * 0.25);
	if (v9 < 0 || v9 >= this->field_48) {
		return false;
	}

	int v11 = (int)((this->field_44 + a2->z) * 0.25);
	if (v11 < 0	|| v11 >= this->field_4C) {
		return false;
	}

	float v69 = std::min(a2->y, a3->y);
	float v70 = std::max(a2->y, a3->y);

	int v13{}, v14{}, v58{}, v59{};
	if (this->field_3413A != 0) {
		v59 = (v9 < 2) ? 0 : std::min(v9 - 2, this->field_48 - 1);
		v58 = (v9 < -2) ? 0 : std::min(v9 + 2, this->field_48 - 1);
		v13 = (v11 < 2) ? 0 : std::min(v11 - 2, this->field_4C - 1);
		v14 = v11 + 2;
	}
	else {
		v59 = (v9 < 1) ? 0 : std::min(v9 - 1, this->field_48 - 1);
		v58 = (v9 < -1) ? 0 : std::min(v9 + 1, this->field_48 - 1);
		v13 = (v11 < 1) ? 0 : std::min(v11 - 1, this->field_4C - 1);
		v14 = v11 + 1;
	}

	int v57 = (v14 < 0) ? 0 : std::min(v14, this->field_4C - 1);

	float v71 = 1000000.0f;
	bool result = false;
	for (int d1 = v13; d1 <= v57; ++d1) {
		for (int d2 = v59; d2 <= v58; ++d2) {
			int* v18 = (int*)this->field_60[d2 + d1 * this->field_48];
			while (v18 != nullptr) {
				int v19 = v18[0];
				int* v20 = v18 + 1;
				short v21 = v19;
				int v22 = v21 & 0x7FF;
				if ((v19 & 0x2000) != 0) {
					v20 += 2 * v22;
				}
				else if (v22 > 0) {
					for (int d3 = v22; d3 > 0; --d3) {
						int* v68 = (int*)v20[1];
						if (v68 != nullptr) {
							float* v24 = (float*)v20[0];
							if (v24[45] <= v70 && v24[46] >= v69 && this->field_4 != *((unsigned int*)v20[0] + 47)) {
								int count = *((short*)v68 + 6);
								float v64 = v24[24] * a2->z + v24[20] * a2->y + a2->x * v24[16] + v24[28];
								float v65 = v24[25] * a2->z + v24[21] * a2->y + v24[17] * a2->x + v24[29];
								float v66 = v24[26] * a2->z + v24[22] * a2->y + v24[18] * a2->x + v24[30];
								float v34 = v24[24] * a3->z + v24[20] * a3->y + a3->x * v24[16] + v24[28];
								float v35 = v24[17] * a3->x + v24[25] * a3->z + v24[21] * a3->y + v24[29];
								float v89 = v24[18] * a3->x + v24[26] * a3->z + v24[22] * a3->y + v24[30];
								float v73 = v34 - v64;
								float v74 = v35 - v65;
								float v75 = v89 - v66;
								for (int d4 = 0; d4 < count; ++d4) {
									unsigned short* v25 = ((unsigned short*)v68[2]) + 4 * d4;
									float* v36 = (float*)(v68[1] + 12 * (v25[3] & 0x7FFF));
									float v83 = v36[0];
									float v84 = v36[1];
									float v85 = v36[2];
									v83 *= -1.0;
									v84 *= -1.0;
									v85 *= -1.0;
									float* v86 = (float*)(v68[0] + 12 * (v25[0] & 0x7FFF));
									float* v87 = (float*)(v68[0] + 12 * (v25[1] & 0x3FFF));
									float* v88 = (float*)(v68[0] + 12 * (v25[2] & 0x3FFF));
									float v40 = v85 * v75 + v84 * v74 + v83 * v73;
									if (v40 != 0.0f) {
										float v60 = (v85 * v66 + v84 * v65 + v83 * v64 - (v85 * v86[2] + v84 * v86[1] + v83 * *v86))
											* (-1.0f / v40);
										if (v60 >= 0.0f) {
											if (v60 < 1.0f) {
												D3DXVECTOR3 v61{};
												v61.x = v60 * v73 + v64;
												v61.y = v60 * v74 + v65;
												v61.z = v60 * v75 + v66;
												float float_params[3] = { v83, v84, v85 };
												float* pfloat_params[3] = { v86, v87, v88 };
												if (FFXI::Math::SomeCollisionTest(&v61, float_params, pfloat_params) == true) {
													D3DXVECTOR3 v77{};
													v77.x = v61.z * v24[8] + v61.y * v24[4] + v61.x * *v24 + v24[12];
													v77.y = v61.z * v24[9] + v61.y * v24[5] + v61.x * v24[1] + v24[13];
													v77.z = v61.z * v24[10] + v61.y * v24[6] + v61.x * v24[2] + v24[14];
													D3DXVECTOR3 v45 = *a2 - v77;
													float sqmag = v77.x * v77.x + v77.y * v77.y + v77.z * v77.z;
													if (v71 > sqmag) {
														v71 = sqmag;
														*r = v77;
														this->field_3413C.x = v85 * v24[38] + v84 * v24[35] + v83 * v24[32];
														this->field_3413C.y = v85 * v24[39] + v84 * v24[36] + v83 * v24[33];
														this->field_3413C.z = v85 * v24[40] + v84 * v24[37] + v83 * v24[34];
														this->field_34148 = (*v25 >> 15) | (2 * ((v25[1] >> 15)
															| (2 * ((v25[2] >> 15) | (2 * (v25[3] >> 15))))));
														unsigned int* v24uint = (unsigned int*)v24;
														this->field_3414C = (int)(v24uint[41] << 6) >> 12;
														this->field_34150 = (v24uint[41] >> 2) & 1;
														this->field_34154 = (v24uint[41] >> 3) & 7 | (8 * ((v24uint[41] >> 26) & 3));
														this->field_34158 = v24uint[44];
														this->field_3415C = *((unsigned char*)v24 + 172);
														this->field_3415D = *((unsigned char*)v24 + 173);
														this->field_3415E = *((unsigned char*)v24 + 174);
														this->field_3415F = *((unsigned char*)v24 + 175);
														this->field_1807C = v24uint[42];
														result = true;
													}
												}
											}
										}
									}
								}
							}
						}
						v20 += 2;
					}
				}
				v18 = (int*)v20[0];
			}
		}
	}

	return result;
}

bool FFXI::CYy::MapCollisionData::FastCheck1(D3DXVECTOR3* a2, D3DXVECTOR3* a3, D3DXVECTOR3* a4)
{
	this->field_1807C = 0;
	if (this->field_60 == nullptr) {
		return false;
	}

	float v76 = std::max(a2->y, a3->y);
	float v82 = std::min(a2->y, a3->y);

	int v9 = (int)((this->field_40 + a2->x) * 0.25);
	if (v9 < 0 || v9 >= this->field_48) {
		return false;
	}
	int v11 = (int)((this->field_44 + a2->z) * 0.25);
	if (v11 < 0	|| v11 >= this->field_4C) {
		return false;
	}
	
	int v13{}, v14{}, v60{}, v61{};
	if (this->field_3413A != 0) {
		v61 = (v9 < 2) ? 0 : std::min(v9 - 2, this->field_48 - 1);
		v60 = (v9 < -2) ? 0 : std::min(v9 + 2, this->field_48 - 1);
		v13 = (v11 < 2) ? 0 : std::min(v11 - 2, this->field_4C - 1);
		v14 = v11 + 2;
	}
	else {
		v61 = (v9 < 1) ? 0 : std::min(v9 - 1, this->field_48 - 1);
		v60 = (v9 < -1) ? 0 : std::min(v9 + 1, this->field_48 - 1);
		v13 = (v11 < 1) ? 0 : std::min(v11 - 1, this->field_4C - 1);
		v14 = v11 + 1;
	}

	int v62 = (v14 < 0) ? 0 : std::min(v14, this->field_4C - 1);

	bool retval{ false };
	float v73 = 1000000.0f;
	
	for (int d1 = v13; d1 < v62; ++d1) {
		for (int d2 = v61; d2 < v60; ++d2) {
			int* v18 = (int*)this->field_60[d2 + d1 * this->field_48];
			while (v18 != nullptr) {
				int v19 = v18[0];
				int* v20 = v18 + 1;
				short v21 = v19;
				int v22 = v21 & 0x7FF;
				if ((v19 & 0x2000) != 0) {
					v20 += 2 * v22;
				}
				else if (v22 > 0) {
					for (int d3 = v22; d3 > 0; --d3) {
						short* v75 = (short*)v20[1];
						if (v75 != nullptr) {
							int* v23 = (int*)v75;
							float* v24f = (float*)v20[0];
							unsigned int* v24ui = (unsigned int*)v24f;
							
							if (v24f[45] <= v76 && v24f[46] >= v82 && this->field_4 != *((unsigned int*)v24f + 47)) {
								float v68 = v24f[24] * a2->z + v24f[20] * a2->y + a2->x * v24f[16] + v24f[28];
								float v69 = v24f[25] * a2->z + v24f[21] * a2->y + v24f[17] * a2->x + v24f[29];
								float v70 = v24f[26] * a2->z + v24f[22] * a2->y + v24f[18] * a2->x + v24f[30];
								float v31 = v24f[24] * a3->z + v24f[20] * a3->y + a3->x * v24f[16] + v24f[28];
								float v32 = v24f[17] * a3->x + v24f[25] * a3->z + v24f[21] * a3->y + v24f[29];
								float v91 = v24f[18] * a3->x + v24f[26] * a3->z + v24f[22] * a3->y + v24f[30];
								D3DXVECTOR3 v64{};
								v64.x = v31 - v68;
								v64.y = v32 - v69;
								v64.z = v91 - v70;
								float v34 = 1.0f / sqrt(v64.x * v64.x + v64.y * v64.y + v64.z * v64.z);
								D3DXVECTOR3 v90 = v64 * v34;
								short count = v75[6];
								if (count > 0) {
									for (int d4 = 0; d4 < count; ++d4) {
										unsigned short* v25 = (unsigned short*)*((int*)v75 + 2) + 4 * d4;
										float* v36 = (float*)(v23[1] + 12 * (v25[3] & 0x7FFF));
										float v84 = v36[0];
										float v85 = v36[0];
										float v86 = v36[0];

										float* v87 = (float*)(v23[0] + 12 * (*v25 & 0x7FFF));
										float* v88 = (float*)(v23[0] + 12 * (v25[1] & 0x3FFF));
										float* v89 = (float*)(v23[0] + 12 * (v25[2] & 0x3FFF));

										v84 = v84 * -1.0f;
										v85 = v85 * -1.0f;
										v86 = v86 * -1.0f;
										
										if (v86 * v90.z + v85 * v90.y + v84 * v90.x > 0.0f) {
											float v40 = v86 * v64.z + v85 * v64.y + v84 * v64.x;
											if (v40 != 0.0f) {
												float v63 = (v86 * v70 + v85 * v69 + v84 * v68 - (v86 * v87[2] + v85 * v87[1] + v84 * *v87))
													* (-1.0f	/ v40);
												if (v63 >= 0.0f && v63 < 1.0f) {
													D3DXVECTOR3 v65{};
													v65.x = v63 * v64.x + v68;
													v65.y = v63 * v64.y + v69;
													v65.z = v63 * v64.z + v70;
													float float_params[3] = { v84, v85, v86 };
													float* pfloat_params[3] = { v87, v88, v89 };
													if (FFXI::Math::SomeCollisionTest(&v65, float_params, pfloat_params) == true) {
														float v77 = v65.z * v24f[8] + v65.y * v24f[4] + v65.x * v24f[0] + v24f[12];
														float v78 = v65.z * v24f[9] + v65.y * v24f[5] + v65.x * v24f[1] + v24f[13];
														float v79 = v65.z * v24f[10] + v65.y * v24f[6] + v65.x * v24f[2] + v24f[14];
														float v45 = a2->z - v79;
														float v46 = a2->y - v78;
														float v48 = a2->x - v77;
														float v49 = v45 * v45 + v46 * v46 + v48 * v48;
														if (v73 > v49) {
															v73 = v49;
															a4->x = v77;
															a4->y = v78;
															a4->z = v79;
															this->field_3413C.x = v86 * v24f[38] + v85 * v24f[35] + v84 * v24f[32];
															this->field_3413C.y = v86 * v24f[39] + v85 * v24f[36] + v84 * v24f[33];
															this->field_3413C.z = v86 * v24f[40] + v85 * v24f[37] + v84 * v24f[34];
															this->field_1A114.x = v84;
															this->field_1A114.y = v85;
															this->field_1A114.z = v86;
															this->field_34148 = (v25[0] >> 15) | (2 * ((v25[1] >> 15) | (2 * ((v25[2] >> 15) | (2 * (v25[3] >> 15))))));
															this->field_3414C = (int)(v24ui[41] << 6) >> 12;
															this->field_34150 = (v24ui[41] >> 2) & 1;
															this->field_34154 = (v24ui[41] >> 3) & 7 | (8 * ((v24ui[41] >> 26) & 3));
															this->field_34158 = v24ui[44];
															this->field_3415C = *((unsigned char*)v24f + 172);
															this->field_3415D = *((unsigned char*)v24f + 173);
															this->field_3415E = *((unsigned char*)v24f + 174);
															this->field_3415F = *((unsigned char*)v24f + 175);
															this->field_1807C = v24ui[42];
															retval = true;
														}
													}
												}
											}
										}
									}
								}
							}
						}
						v20 += 2;
					}
				}
				v18 = (int*)*v20;
			}
		}
	}
	return retval;
}

bool FFXI::CYy::MapCollisionData::FastCheck2(D3DXVECTOR3* a2, D3DXVECTOR3* a3, D3DXVECTOR3* a4)
{
	this->field_1807C = 0;

	if (this->field_60 == 0) {
		return false;
	}

	float v69 = std::min(a2->y, a3->y);
	float v70 = std::max(a2->y, a3->y);

	int v10 = (int)((this->field_40 + a2->x) * 0.25);
	if (v10 < 0 || v10 >= this->field_48) {
		return false;
	}
	int v11 = (int)((this->field_44 + a2->z) * 0.25);

	if (v11 < 0 || v11 >= this->field_4C) {
		return false;
	}

	int v13{}, v14{}, v58{}, v59{};
	if (this->field_34139 != 0) {
		v59 = (v10 < 2) ? 0 : std::min(v10 - 2, this->field_48 - 1);
		v58 = (v10 < -2) ? 0 : std::min(v10 + 2, this->field_48 - 1);
		v13 = (v11 < 2) ? 0 : std::min(v11 - 2, this->field_4C - 1);
		v14 = v11 + 2;
	}
	else {
		v59 = (v10 < 1) ? 0 : std::min(v10 - 1, this->field_48 - 1);
		v58 = (v10 < -1) ? 0 : std::min(v10 + 1, this->field_48 - 1);
		v13 = (v11 < 1) ? 0 : std::min(v11 - 1, this->field_4C - 1);
		v14 = v11 + 1;
	}

	int v57 = std::max(0, std::min(v14, this->field_4C - 1));

	float v71 = 1000000.0f;
	bool result = false;

	for (int d1 = v13; d1 <= v57; ++d1) {
		for (int d2 = v59; d2 <= v58; ++d2) {
			unsigned int* v18 = (unsigned int*)this->field_60[d2 + d1 * this->field_48];
			while (v18 != nullptr) {
				int v19 = v18[0];
				unsigned int* v20 = v18 + 1;
				short v21 = v19;
				int v22 = v21 & 0x7FF;
				if ((v19 & 0x2000) != 0)
				{
					v20 += 2 * v22;
				}
				else {
					for (int d3 = 0; d3 < v22; ++d3) {
						unsigned int* v68 = (unsigned int*)v20[1];
						if (v68 != nullptr) {
							float* v24 = (float*)v20[0];
							if (v24[45] <= v70 && v24[46] >= v69 && this->field_4 != *((unsigned int*)v24 + 47)) {
								float v64 = v24[24] * a2->z + v24[20] * a2->y + a2->x * v24[16] + v24[28];
								float v65 = v24[25] * a2->z + v24[21] * a2->y + v24[17] * a2->x + v24[29];
								float v66 = v24[26] * a2->z + v24[22] * a2->y + v24[18] * a2->x + v24[30];
								float v34 = v24[24] * a3->z + v24[20] * a3->y + a3->x * v24[16] + v24[28];
								float v35 = v24[17] * a3->x + v24[25] * a3->z + v24[21] * a3->y + v24[29];
								float v89 = v24[18] * a3->x + v24[26] * a3->z + v24[22] * a3->y + v24[30];
								float v73 = v34 - v64;
								float v74 = v35 - v65;
								float v75 = v89 - v66;
								short count = *((short*)v68 + 6);
								for (int d4 = 0; d4 < count; d4++) {
									unsigned short* v25 = (unsigned short*)v68[2] + 4 * d4;
									float* v36 = (float*)(v68[1] + 12 * (v25[3] & 0x7FFF));
									float* v86 = (float*)(v68[0] + 12 * (v25[0] & 0x7FFF));
									float* v87 = (float*)(v68[0] + 12 * (v25[1] & 0x3FFF));
									float* v88 = (float*)(v68[0] + 12 * (v25[2] & 0x3FFF));

									float v83 = -1.0f * v36[0];
									float v84 = -1.0f * v36[1];
									float v85 = -1.0f * v36[2];
									float v40 = v85 * v75 + v84 * v74 + v83 * v73;
									if (v40 != 0.0f) {
										float v60 = (v85 * v66 + v84 * v65 + v83 * v64 - (v85 * v86[2] + v84 * v86[1] + v83 * *v86))
											* (-1.0f / v40);
										if (v60 >= 0.0f && v60 < 1.0f) {
											D3DXVECTOR3 v61{};
											v61.x = v60 * v73 + v64;
											v61.y = v60 * v74 + v65;
											v61.z = v60 * v75 + v66;
											float float_params[3] = { v83, v84, v85 };
											float* pfloat_params[3] = { v86, v87, v88 };
											if (FFXI::Math::SomeCollisionTest(&v61, float_params, pfloat_params) == true) {
												float v77 = v61.z * v24[8] + v61.y * v24[4] + v61.x * *v24 + v24[12];
												float v78 = v61.z * v24[9] + v61.y * v24[5] + v61.x * v24[1] + v24[13];
												float v79 = v61.z * v24[10] + v61.y * v24[6] + v61.x * v24[2] + v24[14];
												float v45 = a2->z - v79;
												float v46 = a2->y - v78;
												float v48 = a2->x - v77;
												float v49 = v45 * v45 + v46 * v46 + v48 * v48;
												if (v71 > v49) {
													v71 = v49;
													a4->x = v77;
													a4->y = v78;
													a4->z = v79;
													this->field_3413C.x = v85 * v24[38] + v84 * v24[35] + v83 * v24[32];
													this->field_3413C.y = v85 * v24[39] + v84 * v24[36] + v83 * v24[33];
													this->field_3413C.z = v85 * v24[40] + v84 * v24[37] + v83 * v24[34];
													this->field_34148 = (*v25 >> 15) | (2 * ((v25[1] >> 15) | (2 * ((v25[2] >> 15) | (2 * (v25[3] >> 15))))));
													unsigned int* v24_ui = (unsigned int*)v24;
													this->field_3414C = (int)(v24_ui[41] << 6) >> 12;
													this->field_34150 = (v24_ui[41] >> 2) & 1;
													this->field_34154 = (v24_ui[41] >> 3) & 7 | (8 * ((v24_ui[41] >> 26) & 3));
													this->field_34158 = v24_ui[44];
													this->field_3415C = *((unsigned char*)v24 + 172);
													this->field_3415D = *((unsigned char*)v24 + 173);
													this->field_3415E = *((unsigned char*)v24 + 174);
													this->field_3415F = *((unsigned char*)v24 + 175);
													this->field_1807C = v24_ui[42];
													result = true;
												}
											}
											
										}
									}
								}
							}
						}
						v20 += 2;
					}
				}
				v18 = (unsigned int*)v20[0];
			}
		}
	}

	return result;
}

void FFXI::CYy::MapCollisionData::GetGroundStatus(GroundStatus* a2)
{
	a2->field_8.x = -this->field_3413C.x;
	a2->field_8.y = -this->field_3413C.y;
	a2->field_8.z = -this->field_3413C.z;
	a2->field_0 = this->field_34148;
	a2->field_4 = (float)this->field_3414C / 256.0f;
	a2->field_18 = this->field_34150;
	a2->field_1C = this->field_34154;
	a2->field_20 = this->field_3415C;
	a2->field_21 = this->field_3415D;
	a2->field_22 = this->field_3415E;
	a2->field_23 = this->field_3415F;
	a2->fourcc = this->field_34158;
}

void FFXI::CYy::MapCollisionData::SetThings(D3DXMATRIX* a2, float* a3, int* a4, unsigned short* a5, D3DXVECTOR3* a6)
{
	if (this->field_34120 >= 1020) {
		return;
	}

	float* v10 = (float*)a4[0];
	float* v11 = (float*)(a4[0] + 12 * (*a5 & 0x7FFF));

	float v112{}, v113{}, v114{}, v115{}, v116{}, v117{}, v118{}, v119{};

	float v18{}, v19{};
	if (((unsigned char)a3[41] & 1) != 0)
	{
		v112 = *a3 * v10[3 * (*a5 & 0x7FFF)] + a3[8] * v10[3 * (*a5 & 0x7FFF) + 2] + a3[4] * v11[1] + a3[12];
		v113 = a3[1] * *v11 + a3[9] * v11[2] + a3[5] * v11[1] + a3[13];
		float v20 = a3[2] * *v11 + v11[2] * a3[10];
		float v21 = v11[1] * a3[6];
		float* v22 = &v10[3 * (a5[2] & 0x3FFF)];
		v114 = v20 + v21 + a3[14];
		v115 = v22[2] * a3[8] + *a3 * *v22 + v22[1] * a3[4] + a3[12];
		v116 = v22[2] * a3[9] + v22[1] * a3[5] + a3[1] * *v22 + a3[13];
		float v23 = a3[2] * *v22 + v22[2] * a3[10];
		float v24 = v22[1] * a3[6];
		float* v25 = &v10[3 * (a5[1] & 0x3FFF)];
		v117 = v23 + v24 + a3[14];
		v118 = a3[8] * v25[2] + *a3 * *v25 + a3[4] * v25[1] + a3[12];
		v119 = a3[9] * v25[2] + a3[5] * v25[1] + a3[1] * *v25 + a3[13];
		v18 = *v25 * a3[2] + v25[2] * a3[10];
		v19 = v25[1] * a3[6];
	}
	else
	{
		v112 = a3[4] * v10[3 * (*a5 & 0x7FFF) + 1] + *a3 * v10[3 * (*a5 & 0x7FFF)] + a3[8] * v11[2] + a3[12];
		v113 = a3[5] * v11[1] + a3[1] * *v11 + a3[9] * v11[2] + a3[13];
		float* v14 = &v10[3 * (a5[1] & 0x3FFF)];
		v114 = a3[2] * *v11 + v11[2] * a3[10] + v11[1] * a3[6] + a3[14];
		v115 = v14[1] * a3[4] + *a3 * *v14 + a3[8] * v14[2] + a3[12];
		v116 = a3[1] * *v14 + a3[9] * v14[2] + a3[5] * v14[1] + a3[13];
		float v15 = v14[2] * a3[10] + v14[1] * a3[6];
		float v16 = *v14 * a3[2];
		float* v17 = &v10[3 * (a5[2] & 0x3FFF)];
		v117 = v15 + v16 + a3[14];
		v118 = v17[2] * a3[8] + v17[1] * a3[4] + *v17 * *a3 + a3[12];
		v119 = v17[2] * a3[9] + v17[1] * a3[5] + a3[1] * *v17 + a3[13];
		v18 = v17[2] * a3[10] + v17[1] * a3[6];
		v19 = *v17 * a3[2];
	}

	float v120 = v18 + v19 + a3[14];
	float v106 = v112 * a2->_11 + v113 * a2->_21 + v114 * a2->_31 + a2->_41;
	float v107 = v113 * a2->_22 + v114 * a2->_32 + v112 * a2->_12 + a2->_42;
	float v108 = v114 * a2->_33 + v112 * a2->_13 + v113 * a2->_23 + a2->_43;
	float v109 = v115 * a2->_11 + v117 * a2->_31 + v116 * a2->_21 + a2->_41;
	float v110 = v117 * a2->_32 + v116 * a2->_22 + v115 * a2->_12 + a2->_42;
	float v111 = v117 * a2->_33 + v116 * a2->_23 + v115 * a2->_13 + a2->_43;
	float v103 = v118 * a2->_11 + v120 * a2->_31 + v119 * a2->_21 + a2->_41;
	float v104 = v120 * a2->_32 + v119 * a2->_22 + v118 * a2->_12 + a2->_42;
	float v105 = v120 * a2->_33 + v119 * a2->_23 + v118 * a2->_13 + a2->_43;

	float v49{};
	
	float v31 = std::min(std::min(v106, v109), v103);
	float v40 = std::min(std::min(v107, v110), v104);

	if (v108 >= v111)
	{
		if (v111 <= v105) {
			v49 = v117 * a2->_33 + v116 * a2->_23 + v115 * a2->_13 + a2->_43;
		}
		else {
			v49 = v120 * a2->_33 + v119 * a2->_23 + v118 * a2->_13 + a2->_43;
		}	
	}
	else
	{
		if (v108 <= v105) {
			v49 = v114 * a2->_33 + v112 * a2->_13 + v113 * a2->_23 + a2->_43;
		}
		else {
			v49 = v120 * a2->_33 + v119 * a2->_23 + v118 * a2->_13 + a2->_43;
		}
	}

	float v62 = std::max(std::max(v106, v109), v103);
	float v75 = std::max(std::max(v107, v110), v104);
	float v88 = std::max(std::max(v108, v111), v105);
	
	float v124 = (v62 + v31) * 0.5;
	float v121 = (v75 + v40) * 0.5;
	float v101 = (v49 + v88) * 0.5;
	float v100 = (v88 - v49) * 0.5;
	float v123 = (v62 - v31) * 0.5 + a6->x;
	float v122 = (v75 - v40) * 0.5 + a6->y;
	if (v124 - -v123 > 0.0
		&& v121 - -v122 > 0.0
		&& v101 - (-a6->x - v100) > 0.0
		&& v123 - v124 > 0.0
		&& v122 - v121 > 0.0
		&& v100 + a6->z + a6->x - v101 > 0.0)
	{
		CollSubStruct* v93 = &this->field_1A120[this->field_34120];
		this->field_34120 += 1;
		v93->field_0.x = v106;
		v93->field_0.y = v107;
		v93->field_0.z = v108;
		v93->field_C.x = v109;
		v93->field_C.y = v110;
		v93->field_C.z = v111;
		v93->field_18.x = v103;
		v93->field_18.y = v104;
		v93->field_18.z = v105;
		v93->field_30.x = v112;
		v93->field_30.y = v113;
		v93->field_30.z = v114;
		v93->field_3C.x = v115;
		v93->field_3C.y = v116;
		v93->field_3C.z = v117;
		v93->field_48.x = v118;
		v93->field_48.y = v119;
		v93->field_48.z = v120;
		char v94 = ((*a5 & 0x8000) != 0) | (2
			* (((a5[1] & 0x8000) != 0) | (2
				* (((a5[2] & 0x8000) != 0) | (2 * ((a5[3] & 0x8000) != 0))))));
		v93->field_60 = (int)a3;
		v93->field_64 = v94;
		float* v95 = (float*)a4[1];
		int v96 = a5[3] & 0x7FFF;
		float* v99 = &v95[3 * v96];
		v93->field_54.x = a3[38] * v95[3 * v96 + 2] + a3[35] * v95[3 * v96 + 1] + a3[32] * *v99;
		v93->field_54.y = a3[39] * v99[2] + a3[33] * *v99 + a3[36] * v99[1];
		v93->field_54.z = a3[40] * v99[2] + a3[34] * *v99 + a3[37] * v99[1];
		v93->field_24.x = a2->_11 * v93->field_54.x + a2->_31 * v93->field_54.z + a2->_21 * v93->field_54.y;
		v93->field_24.y = a2->_32 * v93->field_54.z + a2->_22 * v93->field_54.y + a2->_12 * v93->field_54.x;
		v93->field_24.z = v93->field_54.z * a2->_33 + v93->field_54.y * a2->_23 + a2->_13 * v93->field_54.x;
	}
}

bool FFXI::CYy::MapCollisionData::KO_ExecSpCorrect(D3DXVECTOR3* a2, D3DXVECTOR3* a3)
{
	float z = 0.0;
	float v28 = a3->x * 0.3f;
	if (a3->z > 0.0) {
		z = a3->z;
	}
	
	this->field_34124 = 1;
	this->field_34125 = 0;
	this->field_34126 = 0;
	this->field_3412C = 0.25f;

	D3DXVECTOR3 v30{};
	while (true) {
		v30.x = z * this->field_3416C._31 + this->field_3416C._41;
		v30.y = z * this->field_3416C._32 + this->field_3416C._42;
		v30.z = z * this->field_3416C._33 + this->field_3416C._43;
		this->field_34134 = 9999999.0f;
		this->field_34130 = 9999999.0f;
		this->field_341AC.x = v30.x;
		this->field_341AC.z = v30.z;

		if (this->field_34126 == 0) {
			this->field_341AC.y = v30.y;
		}

		if (this->field_34120 > 0) {
			for (int i = 0; i < this->field_34120; ++i) {
				this->InterpolatePosition(this->field_1A120 + i, &v30);
			}
		}

		if (z >= a3->z || this->field_34125 != 0) {
			break;
		}

		z += v28;
		if (z > a3->z) {
			z = a3->z;
		}
	}

	this->field_34124 = 0;
	
	for (int i=0; i<3; ++i)
	{
		this->field_34125 = 0;
		this->field_34126 = 0;
		this->field_34130 = 9999999.0;
		this->field_34134 = 9999999.0;

		if (this->field_34120 <= 0) {
			*a2 = this->field_341AC;
			return true;
		}

		for (int j = 0; j < this->field_34120; ++j) {
			this->InterpolatePosition(this->field_1A120 + j, &v30);
		}

		if (this->field_34125 == 0 && this->field_34126 == 0) {
			*a2 = this->field_341AC;
			return true;
		}
	}

	return false;
}

void FFXI::CYy::MapCollisionData::InterpolatePosition(CollSubStruct* a2, D3DXVECTOR3* a3)
{
	if (this->field_34124 != 0 && a2->field_24.z > 0.0) {
			return;
	}

	float v10 = a2->field_54.x * a2->field_54.x + a2->field_54.y * a2->field_54.y + a2->field_54.z * a2->field_54.z;
	if (v10 == 0.0f) {
		return;
	}

	D3DXVECTOR3 v51 = a2->field_30;
	D3DXVECTOR3 v53 = a2->field_3C;
	D3DXVECTOR3 v54 = a2->field_48;

	float v12 = ((v51.x - a3->x) * a2->field_54.x + (v51.z - a3->z) * a2->field_54.z + (v51.y - a3->y) * a2->field_54.y)
		/ v10;
	
	D3DXVECTOR3 v52{};
	v52.x = v12 * a2->field_54.x + a3->x;
	v52.y = v12 * a2->field_54.y + a3->y;
	v52.z = v12 * a2->field_54.z + a3->z;

	if (CheckPolygonInnner(&v51, &v53, &v54, &v52, &a2->field_54) == false
	&& this->DoesShpereIntersect(&v51, &v53, a3) == false
	&& this->DoesShpereIntersect(&v53, &v54, a3) == false
	&& this->DoesShpereIntersect(&v54, &v51, a3) == false)
	{
		return;
	}

	float v15 = v52.x - a3->x;
	float v13 = v52.y - a3->y;
	float v14 = v52.z - a3->z;
	float v55 = v14 * v14 + v13 * v13 + v15 * v15;
	if (this->field_3412C - 0.001f < v55) {
		return;
	}

	if (a2->field_54.y <= 0.708f && a2->field_54.y >= -0.708f) {
		if (this->field_34130 > v55) {
			float v56 = a3->y - 0.002f;
			if (v51.y < v56 || v53.y < v56 || v54.y < v56) {
				float v26 = a3->x - v52.x;
				v51.y = a3->y - v52.y;
				float v27 = a3->z - v52.z;
				float v28 = v27 * v27 + v51.y * v51.y + v26 * v26;
				if (v28 != 0.0f) {
					float v29 = 1.0f / sqrt(v28);
					v51.x = v29 * v26;
					v51.y = v51.y * v29;
					float v30 = v29 * v27;
					if (v30 * a2->field_54.z + v51.y * a2->field_54.y + v51.x * a2->field_54.x >= 0.0f)
					{
						float v31 = sqrt(this->field_3412C - v55);
						this->field_341AC.x = v51.x * v31 + a3->x;
						this->field_341AC.y = v51.y * v31 * 0.2f + a3->y;
						float v32 = v31 * v30 + a3->z;
						this->field_34125 = 1;
						this->field_34130 = v55;
						this->field_341AC.z = v32;
					}
				}
			}
		}
	}
	else if (this->field_34134 > v55)
	{
		float v57 = a3->y - 0.3f;
		if (v51.y > v57 || v53.y > v57 || v54.y > v57)
		{
			if (a2->field_54.y < 0.0f)
			{
				this->field_34128 = 1;
				this->field_34126 = 1;
				this->field_341AC.y = v52.y - 0.5f;
			}
			this->field_34134 = v55;
		}
	}
}

bool FFXI::CYy::MapCollisionData::DoesShpereIntersect(D3DXVECTOR3* a2, D3DXVECTOR3* a3, D3DXVECTOR3* a4)
{
	D3DXVECTOR3 v14 = *a4 - *a2;
	D3DXVECTOR3 v4 = *a3 - *a2;

	float v17 = sqrt(v4.x * v4.x + v4.y * v4.y + v4.z * v4.z);
	float v7 = 1.0f / v17;

	v4 *= v7;

	float v8 = v4.x * v14.x + v4.y * v14.y + v4.z * v14.z;
	if (v8 < 0.0f) {
		return false;
	}

	if (v8 > v17) {
		return false;
	}

	float a = (v14.z * v14.z + v14.y * v14.y + v14.x * v14.x - v8 * v8);
	if (this->field_3412C < a) {
		return false;
	}

	return true;
}

bool FFXI::CYy::MapCollisionData::SomeCollisionCheck(D3DXVECTOR3* a2, D3DXVECTOR3* a3, D3DXVECTOR3* a4)
{
	D3DXVECTOR3 diff = *a3 - *a2;
	float v5 = sqrt(diff.x * diff.x + diff.z * diff.z);
	int v23 = 1 - (int)(v5 * -0.25);
	if (v23 < 2) {
		return this->KO_LineCollision(a2, a3, a4);
	}

	D3DXVECTOR3 v13 = *a2;

	float v12 = 0.0;
	float v18 = (1.0f / (float)v23);
	D3DXVECTOR3 v15 = diff * v18;

	while (true) {
		D3DXVECTOR3 v14 = v15 + v13;
		v12 += v18;
		if (v12 >= 1.0) {
			return this->KO_LineCollision(&v13, a3, a4);
		}
		if (this->KO_LineCollision(&v13, &v14, a4) == true) {
			return true;
		}
		v13 = v14;
	}
}

bool FFXI::CYy::MapCollisionData::KO_LineCollision(D3DXVECTOR3* a2, D3DXVECTOR3* a3, D3DXVECTOR3* a4)
{
	this->field_1807C = 0;
	if (this->field_60 == nullptr) {
		return false;
	}

	int v6 = (int)((this->field_40 + a2->x) * 0.25);
	if (v6 < 0) {
		return false;
	}

	int v8 = this->field_48;
	if (v6 >= v8) {
		return false;
	}

	int v9 = (int)((this->field_44 + a2->z) * 0.25);
	if (v9 < 0) {
		return false;
	}

	int v10 = this->field_4C;
	if (v9 >= v10) {
		return false;
	}

	bool result = false;

	int v104 = (v6 < 2) ? 0 : std::min(v6 - 2, v8 - 1);
	int v92 = (v6 < -2) ? 0 : std::min(v6 + 2, v8 - 1);
	int v11 = (v6 < 2) ? 0 : std::min(v6 - 2, v10 - 1);
	int v91 = (v9 < -2) ? 0 : std::min(v9 + 2, v10 - 1);

	float v14 = std::min(a2->y, a3->y);
	float v15 = std::max(a2->y, a3->y);

	float v83 = 1000000.0f;
	int v16 = v11;
	for (int i = v11; i <= v91; ++i) {
		for (int j = v104; j <= v92; ++j) {
			unsigned int* v18 = (unsigned int*)this->field_60[j + i * this->field_48];
			while (v18 != nullptr) {
				int v19 = v18[0];
				unsigned int* v20 = v18 + 1;
				short v21 = v19;
				int v22 = v21 & 0x7FF;
				if ((v19 & 0x2000) != 0)
				{
					v20 += 2 * v22;
				}
				else {
					for (int d3 = 0; d3 < v22; ++d3) {
						unsigned int* v68 = (unsigned int*)v20[1];
						if (v68 != nullptr) {
							float* v24 = (float*)v20[0];
							if (v24[45] <= v15 && v24[46] >= v14 && this->field_4 != *((unsigned int*)v24 + 47)) {
								float v64 = v24[24] * a2->z + v24[20] * a2->y + a2->x * v24[16] + v24[28];
								float v65 = v24[25] * a2->z + v24[21] * a2->y + v24[17] * a2->x + v24[29];
								float v66 = v24[26] * a2->z + v24[22] * a2->y + v24[18] * a2->x + v24[30];
								float v34 = v24[24] * a3->z + v24[20] * a3->y + a3->x * v24[16] + v24[28];
								float v35 = v24[17] * a3->x + v24[25] * a3->z + v24[21] * a3->y + v24[29];
								float v89 = v24[18] * a3->x + v24[26] * a3->z + v24[22] * a3->y + v24[30];
								float v73 = v34 - v64;
								float v74 = v35 - v65;
								float v75 = v89 - v66;
								short count = *((short*)v68 + 6);
								bool flag = *((short*)v68 + 7) == 0;

								for (int d4 = 0; d4 < count; d4++) {
									unsigned short* v25 = (unsigned short*)v68[2] + 4 * d4;

									if (flag = true && (*((char*)v25 + 5) & 0x40) == 0) {
										continue;
									}
									float* v36 = (float*)(v68[1] + 12 * (v25[3] & 0x7FFF));
									float* v86 = (float*)(v68[0] + 12 * (v25[0] & 0x7FFF));
									float* v87 = (float*)(v68[0] + 12 * (v25[1] & 0x3FFF));
									float* v88 = (float*)(v68[0] + 12 * (v25[2] & 0x3FFF));

									float v83 = -1.0f * v36[0];
									float v84 = -1.0f * v36[1];
									float v85 = -1.0f * v36[2];
									float v40 = v85 * v75 + v84 * v74 + v83 * v73;
									if (v40 != 0.0f) {
										float v60 = (v85 * v66 + v84 * v65 + v83 * v64 - (v85 * v86[2] + v84 * v86[1] + v83 * *v86))
											* (-1.0f / v40);
										if (v60 >= 0.0f && v60 < 1.0f) {
											D3DXVECTOR3 v61{};
											v61.x = v60 * v73 + v64;
											v61.y = v60 * v74 + v65;
											v61.z = v60 * v75 + v66;
											float float_params[3] = { v83, v84, v85 };
											float* pfloat_params[3] = { v86, v87, v88 };
											if (FFXI::Math::SomeCollisionTest(&v61, float_params, pfloat_params) == true) {
												float v77 = v61.z * v24[8] + v61.y * v24[4] + v61.x * *v24 + v24[12];
												float v78 = v61.z * v24[9] + v61.y * v24[5] + v61.x * v24[1] + v24[13];
												float v79 = v61.z * v24[10] + v61.y * v24[6] + v61.x * v24[2] + v24[14];
												float v45 = a2->z - v79;
												float v46 = a2->y - v78;
												float v48 = a2->x - v77;
												float v49 = v45 * v45 + v46 * v46 + v48 * v48;
												if (v83 > v49) {
													v83 = v49;
													a4->x = v77;
													a4->y = v78;
													a4->z = v79;
													this->field_3413C.x = v85 * v24[38] + v84 * v24[35] + v83 * v24[32];
													this->field_3413C.y = v85 * v24[39] + v84 * v24[36] + v83 * v24[33];
													this->field_3413C.z = v85 * v24[40] + v84 * v24[37] + v83 * v24[34];
													this->field_34148 = (*v25 >> 15) | (2 * ((v25[1] >> 15) | (2 * ((v25[2] >> 15) | (2 * (v25[3] >> 15))))));
													unsigned int* v24_ui = (unsigned int*)v24;
													this->field_3414C = (int)(v24_ui[41] << 6) >> 12;
													this->field_34150 = (v24_ui[41] >> 2) & 1;
													this->field_34154 = (v24_ui[41] >> 3) & 7 | (8 * ((v24_ui[41] >> 26) & 3));
													this->field_34158 = v24_ui[44];
													this->field_3415C = *((unsigned char*)v24 + 172);
													this->field_3415D = *((unsigned char*)v24 + 173);
													this->field_3415E = *((unsigned char*)v24 + 174);
													this->field_3415F = *((unsigned char*)v24 + 175);
													this->field_1807C = v24_ui[42];
													result = true;
												}
											}

										}
									}
								}
								
							}
						}
						v20 += 2;
					}
				}
				v18 = (unsigned int*)v20[0];
			}
		}
	}
	
	//TODO
	if (Placeholder::g_pYkMyroom) {
		exit(010165313);
	}
	return result;
}
