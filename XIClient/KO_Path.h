#pragma once
#define WIN32_LEAN_AND_MEAN
#include "d3dx9math.h"
namespace FFXI {
	namespace Math {
		/**
		* @brief Represents a 3D path with direction and radius information
		*/
		class KO_Path {
		public:
			struct SegmentDefinition;
			struct SegmentPoint;
			/**
			 * @brief Calculates the direction and distance to the nearest point on the path
			 *
			 * @param outDirection Output vector containing direction to the path
			 * @param currentPosition Current position to calculate from
			 */
			void GetDirection(D3DXVECTOR3* outDirection, D3DXVECTOR3* currentPosition);

			//Track segment functions
			unsigned int GetSegmentCount();
			SegmentDefinition GetSegmentDefinition(unsigned int segmentIndex);

			//Segment point functions
			SegmentPoint* GetSegmentPoint(unsigned int pointIndex);

			//Haven't seen these used yet
			int field_0;    
			int field_4;
			int field_8;   
			int field_C;

			// Array of SegmentPoints (16 byte header, first value is point count)
			unsigned int* pointsArray;

			// Array of SegmentDefinitions (16 byte header, first value is segment count)
			unsigned int* segmentsArray;

			//Haven't seen these used yet
			int field_18;
			int field_1C;

			#pragma pack(push, 1)
			struct SegmentDefinition {
				unsigned int StartIndex;
				unsigned int EndIndex;
			};

			struct SegmentPoint {
				D3DXVECTOR3 Position;
				float Radius;
			};
			#pragma pack(pop)
		};
	}
}

/*
* SVG Image to visualize the path
* 
<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 800 600">
  <!-- Background -->
  <rect width="800" height="600" fill="#f8f9fa" />
  
  <!-- Title -->
  <text x="400" y="40" font-family="Arial" font-size="24" text-anchor="middle" font-weight="bold">Spherical Path with Variable Radius</text>
  
  <!-- Coordinate system hint -->
  <g transform="translate(700, 100)">
    <line x1="0" y1="0" x2="50" y2="0" stroke="#e74c3c" stroke-width="2" />
    <line x1="0" y1="0" x2="0" y2="50" stroke="#2ecc71" stroke-width="2" />
    <line x1="0" y1="0" x2="-25" y2="25" stroke="#3498db" stroke-width="2" />
    <text x="55" y="5" font-family="Arial" font-size="14">X</text>
    <text x="5" y="65" font-family="Arial" font-size="14">Y</text>
    <text x="-35" y="35" font-family="Arial" font-size="14">Z</text>
  </g>
  
  <!-- Floor grid -->
  <g transform="translate(100, 450)">
    <path d="M0,0 L600,0 L500,-200 L-100,-200 Z" fill="#eee" stroke="#ccc" stroke-width="1" />
    
    <!-- Grid lines -->
    <g stroke="#ddd" stroke-width="0.5">
      <path d="M0,0 L-100,-200" />
      <path d="M100,0 L0,-200" />
      <path d="M200,0 L100,-200" />
      <path d="M300,0 L200,-200" />
      <path d="M400,0 L300,-200" />
      <path d="M500,0 L400,-200" />
      <path d="M600,0 L500,-200" />
      
      <path d="M0,0 L-100,-200" />
      <path d="M0,-40 L-100,-240" />
      <path d="M0,-80 L-100,-280" />
      <path d="M0,-120 L-100,-320" />
      <path d="M0,-160 L-100,-360" />
      <path d="M0,-200 L-100,-400" />
    </g>
  </g>
  
  <!-- Path segments with varying radius -->
  <!-- Segment 1 with small radius -->
  <g opacity="0.7">
    <path d="M100,400 C150,350 200,320 250,320" fill="none" stroke="#3498db" stroke-width="4" />
    
    <!-- Radius visualization for segment 1 -->
    <circle cx="100" cy="400" r="15" fill="#3498db" opacity="0.3" />
    <circle cx="140" cy="365" r="15" fill="#3498db" opacity="0.3" />
    <circle cx="180" cy="335" r="15" fill="#3498db" opacity="0.3" />
    <circle cx="220" cy="323" r="15" fill="#3498db" opacity="0.3" />
    <circle cx="250" cy="320" r="15" fill="#3498db" opacity="0.3" />
    
    <!-- Upper tube boundary -->
    <path d="M100,385 C150,335 200,305 250,305" fill="none" stroke="#3498db" stroke-width="1" stroke-dasharray="4" />
    <!-- Lower tube boundary -->
    <path d="M100,415 C150,365 200,335 250,335" fill="none" stroke="#3498db" stroke-width="1" stroke-dasharray="4" />
  </g>
  
  <!-- Segment 2 with medium radius -->
  <g opacity="0.7">
    <path d="M250,320 C300,320 350,360 400,360" fill="none" stroke="#3498db" stroke-width="4" />
    
    <!-- Radius visualization for segment 2 (increasing) -->
    <circle cx="250" cy="320" r="15" fill="#3498db" opacity="0.3" />
    <circle cx="280" cy="325" r="20" fill="#3498db" opacity="0.3" />
    <circle cx="310" cy="335" r="25" fill="#3498db" opacity="0.3" />
    <circle cx="340" cy="345" r="30" fill="#3498db" opacity="0.3" />
    <circle cx="370" cy="355" r="35" fill="#3498db" opacity="0.3" />
    <circle cx="400" cy="360" r="40" fill="#3498db" opacity="0.3" />
    
    <!-- Upper tube boundary -->
    <path d="M250,305 C300,300 350,330 400,320" fill="none" stroke="#3498db" stroke-width="1" stroke-dasharray="4" />
    <!-- Lower tube boundary -->
    <path d="M250,335 C300,340 350,390 400,400" fill="none" stroke="#3498db" stroke-width="1" stroke-dasharray="4" />
  </g>
  
  <!-- Segment 3 with large radius -->
  <g opacity="0.7">
    <path d="M400,360 C450,360 500,330 550,280" fill="none" stroke="#3498db" stroke-width="4" />
    
    <!-- Radius visualization for segment 3 -->
    <circle cx="400" cy="360" r="40" fill="#3498db" opacity="0.3" />
    <circle cx="430" cy="355" r="40" fill="#3498db" opacity="0.3" />
    <circle cx="460" cy="340" r="40" fill="#3498db" opacity="0.3" />
    <circle cx="490" cy="320" r="30" fill="#3498db" opacity="0.3" />
    <circle cx="520" cy="300" r="25" fill="#3498db" opacity="0.3" />
    <circle cx="550" cy="280" r="20" fill="#3498db" opacity="0.3" />
    
    <!-- Upper tube boundary -->
    <path d="M400,320 C450,320 500,290 550,260" fill="none" stroke="#3498db" stroke-width="1" stroke-dasharray="4" />
    <!-- Lower tube boundary -->
    <path d="M400,400 C450,400 500,370 550,300" fill="none" stroke="#3498db" stroke-width="1" stroke-dasharray="4" />
  </g>
  
  <!-- Path points -->
  <circle cx="100" cy="400" r="6" fill="#e74c3c" />
  <circle cx="250" cy="320" r="6" fill="#e74c3c" />
  <circle cx="400" cy="360" r="6" fill="#e74c3c" />
  <circle cx="550" cy="280" r="6" fill="#e74c3c" />
  
  <!-- Radius indicators -->
  <line x1="250" y1="320" x2="250" y2="305" stroke="#e74c3c" stroke-width="2" />
  <line x1="400" y1="360" x2="400" y2="320" stroke="#e74c3c" stroke-width="2" />
  <line x1="550" y1="280" x2="550" y2="260" stroke="#e74c3c" stroke-width="2" />
  
  <!-- Labels -->
  <text x="250" y="290" font-family="Arial" font-size="14" text-anchor="middle">r = 15</text>
  <text x="400" y="310" font-family="Arial" font-size="14" text-anchor="middle">r = 40</text>
  <text x="550" y="240" font-family="Arial" font-size="14" text-anchor="middle">r = 20</text>
  
  <!-- Axis labels -->
  <text x="650" y="450" font-family="Arial" font-size="16" text-anchor="middle">Path Direction</text>
  
  <!-- Annotations -->
  <text x="400" y="520" font-family="Arial" font-size="16" text-anchor="middle">
    The radius defines a "tube" of influence around the path
  </text>
  <text x="400" y="550" font-family="Arial" font-size="16" text-anchor="middle">
    Radius can vary between points, creating wider or narrower sections
  </text>
  
  <!-- Example "current position" -->
  <circle cx="360" cy="390" r="5" fill="#9b59b6" />
  <text x="380" y="400" font-family="Arial" font-size="14">Current Position</text>
  <line x1="360" y1="390" x2="360" y2="360" stroke="#9b59b6" stroke-width="1" stroke-dasharray="4" />
  <text x="320" y="375" font-family="Arial" font-size="12" text-anchor="end">Distance</text>
</svg>
*/