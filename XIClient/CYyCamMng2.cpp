#include "CYyCamMng2.h"
#include "MemoryPoolManager.h"
#include "Globals.h"
#include "CYyDb.h"
#include "CMoCameraTask.h"
#include "CMoResourceMng.h"
#include "CXiSkeletonActor.h"
#include "CXiModelActor.h"
#include "InputMng.h"
#include "CTkInputCtrl.h"
#include "TkManager.h"
#include "CFsConf6Win.h"
#include "FsConfig.h"
#include "CTsZoneMap.h"
using namespace FFXI::CYy;

const BaseGameObject::ClassInfo CYyCamMng2::CYyCamMng2Class{
	"CYyCamMng2", sizeof(CYyCamMng2), &BaseGameObject::BaseGameObjectClass
};

char CYyCamMng2::SomeByte1{ 0 };
char CYyCamMng2::SomeByte2{ 0 };
int CYyCamMng2::SomeDword1{ 0 };
int CYyCamMng2::InitTimer{ 0 };
int CYyCamMng2::SomeInt{ 0 };
float CYyCamMng2::TransModeTimer{ 0.0 };
CMoCameraTask* CYyCamMng2::CurrentCameraTask{ nullptr };
float CYyCamMng2::PrevYDelta{ 0.0 };
float CYyCamMng2::NoticeModeHeight{ 0.0 };
int CYyCamMng2::IsControled{ 0 };
int CYyCamMng2::SomeCounter{ 0 };
int CYyCamMng2::SomeCounter2{ 0 };
D3DXVECTOR3 CYyCamMng2::ActorPrevPos{};
D3DXVECTOR3 CYyCamMng2::ActorPrevPos2{};
bool CYyCamMng2::IsTracking{ false };
bool CYyCamMng2::ResetCamera{ false };
const BaseGameObject::ClassInfo* FFXI::CYy::CYyCamMng2::GetRuntimeClass()
{
	return &CYyCamMng2Class;
}

void FFXI::CYy::CYyCamMng2::VObj2(int* a2)
{
	if (a2[0] != 2) {
		if (a2[0] != 5) {
			return;
		}
	}

	if (Globals::g_pCYyCamMng2 == CYyDb::g_pCYyDb->CameraManager)
		CYyDb::g_pCYyDb->CameraManager = nullptr;
	
	if (Globals::g_pCYyCamMng2) {
		delete Globals::g_pCYyCamMng2;
		Globals::g_pCYyCamMng2 = nullptr;
	}
}

void FFXI::CYy::CYyCamMng2::InitCameraManager()
{
	char* mem = MemoryPoolManager::globalInstance->Get(sizeof(CYyCamMng2), MemoryPoolManager::MemoryPoolType::Ex);
	if (mem) {
		Globals::g_pCYyCamMng2 = new (mem) CYyCamMng2();
		Globals::g_pCYyCamMng2->Init();
		CYyDb::g_pCYyDb->LinkCameraManager(Globals::g_pCYyCamMng2);
	}
}

void FFXI::CYy::CYyCamMng2::SetSomeValues(char a1, char a2, int a3)
{
	if (CYyCamMng2::SomeByte1 == 0 && a1 != 0 || a2 != 0) {
		CYyCamMng2::SomeByte2 = 0;
	}
	CYyCamMng2::SomeByte1 = a1;
	CYyCamMng2::SomeDword1 = a3;
}

void FFXI::CYy::CYyCamMng2::ResetCameraInitTimer()
{
	InitTimer = 60;
}

void FFXI::CYy::CYyCamMng2::GetDefaultCamera(D3DXVECTOR3* a1, D3DXVECTOR3* a2)
{
	CXiActor* actor = CYy::CXiActor::control_actor.GetSearchActor();
	if (actor == nullptr) {
		*a1 = CYyDb::g_pCYyDb->CameraManager->Position;
		*a2 = CYyDb::g_pCYyDb->CameraManager->field_50;
		return;
	}
	
	if (CXiControlActor::is_first_person_view == true) {
		exit(0x100204B0);
		return;
	}

	D3DXVECTOR3 v10 = { -3.0, 0.0, 0.0 };

	D3DXVECTOR4* v6 = actor->VirtActor102();
	D3DXVECTOR4* v7 = actor->GetPos();
	D3DXVECTOR4 v12{};
	actor->GetElemLocal(2, &v12);

	Math::WMatrix v14{};
	v14.Identity();
	v14.RotateY(v6->y);
	v14.Vec3TransformDrop4Self(&v10);

	v10.x += v7->x;
	v10.y += v7->y;
	v10.z += v7->z;

	*a1 = v10;
	Globals::CopyVec3(a2, v7);

	Math::BoundingBox3D* boundingBox = actor->GetBoundingBoxByState(true);
	if (boundingBox == nullptr) {
		a2->y -= 1.2;
	}
	else {
		a2->y -= (boundingBox->Bottom - boundingBox->Top) * 0.6f;
	}
}

bool FFXI::CYy::CYyCamMng2::SomeKeyCheck()
{
	bool check1 = FFXI::Input::InputMng::GetKey(63, (FFXI::Constants::Enums::KEYTYPE)118, FFXI::Constants::Enums::TRGTYPE_4, -1);
	bool check2 = FFXI::Input::InputMng::GetKey(63, (FFXI::Constants::Enums::KEYTYPE)119, FFXI::Constants::Enums::TRGTYPE_4, -1);
	return check1 || check2;
}

void FFXI::CYy::CYyCamMng2::ClearSomeInt()
{
	SomeInt = 0;
}

int FFXI::CYy::CYyCamMng2::GetSomeInt()
{
	return SomeInt;
}

void FFXI::CYy::CYyCamMng2::Init()
{
	this->SomeInit();
	this->field_B4 = 3.0;
	this->field_B8 = 6.0;
}

void FFXI::CYy::CYyCamMng2::SomeInit()
{
	this->Position = {0.0, -1.7, 0.0};
	CYyDb::g_pCYyDb->SetProjection(350.0);
	this->field_50 = this->Position;
	this->field_50.z += 1;
	this->field_5C = 0.0;
	this->field_A8 = {0.0, -1.0, 0.0};
	this->field_6C = this->field_50;
	this->field_60 = this->Position;
	this->field_84 = 0;
	this->field_80 = 0;
	this->field_7C = 0;
	this->field_88 = 0;
	this->field_89 = 0;
	this->field_8A = 0;
	this->field_F4 = 0;
}

void FFXI::CYy::CYyCamMng2::Update()
{
	if (this->field_88) {
		this->SetPos(&this->field_8C);
		this->field_88 = 0;
	}
	if (this->field_89) {
		this->SetAt(&this->field_98);
		this->field_89 = 0;
	}
	if (this->field_8A) {
		this->field_5C = this->field_A4;
		this->field_8A = 0;
	}
	float v2 = CYyDb::g_pCYyDb->CheckTick();
	if (v2 >= 8.0)
		v2 = 8.0;
	if (this->field_7C == 0.0 && this->field_80 == 0.0)
		return;

	double v4 = (double)v2 * this->field_80;

	if (v4 <= 0.0) {
		this->field_7C += v4;
		if (this->field_7C <= 0.0)
			return;
	}
	else {
		this->field_7C -= v4;
		if (this->field_7C >= 0.0)
			return;
	}

	double v12 = this->field_7C;
	if (v12 < 0.0)
		v12 = -v12;

	if (v12 < 0.000099999997) {
		double v16 = this->field_80;
		if (v16 < 0.0)
			v16 = -v16;
		if (v16 < 0.000099999997)
		{
			this->field_80 = 0.0;
			this->field_7C = 0.0;
			return;
		}
	}

	double v20 = (double)this->field_7C * 0.86000001;
	this->field_7C = v20;
	this->field_80 = v20 * 0.125;
	if (this->field_80 < 0.0)
		this->field_80 = -this->field_80;

	this->field_84 = CYyDb::g_pCYyDb->pCMoResourceMng->rng.frand(10.0) * 0.1 + 1.0;
}

void FFXI::CYy::CYyCamMng2::SetPos(D3DXVECTOR3* a2)
{
	this->Position = *a2;
}

void FFXI::CYy::CYyCamMng2::SetAt(D3DXVECTOR3* a2)
{
	D3DXVECTOR3 a1 = *a2 - this->Position;
	float magsq = a1.x * a1.x + a1.y * a1.y + a1.z * a1.z;
	if (magsq != 0)
		this->field_50 = *a2;
}

void FFXI::CYy::CYyCamMng2::SetCameraPos(D3DXVECTOR3* a1, CXiActor* a2)
{
	FFXI::Math::WMatrix v19{};

	if (FFXI::CYy::CXiControlActor::is_first_person_view == true) {
		exit(0x100209A3);
	}
	else {
		if (a1->x == 0.0 && a1->y == 0.0 && a1->z == 0.0) {
			a1->y = -2.0;
			a1->x = -4.0;
		}

		v19.Identity();
		D3DXVECTOR4* rotate = a2->VirtActor102();
		v19.RotateX(rotate->x);
		v19.RotateY(rotate->y);
		v19.RotateZ(rotate->z);

		D3DXVECTOR4* translate = a2->GetPos();
		v19.AddTranslation3((D3DXVECTOR3*)translate);

		D3DXVECTOR3 out{};
		v19.Vec3TransformDrop4(&out, a1);

		this->field_8C = out;
		this->field_88 = 1;
		this->SetPos(&out);

		this->field_98.x = translate->x;
		this->field_98.y = translate->y - 0.5;
		this->field_98.z = translate->z;
		
		this->field_89 = 1;
		this->SetAt(&this->field_98);
		this->field_F4 = 0;
	}

	double v16 = this->Position.y - this->field_50.y;
	if (v16 < 0.0) {
		v16 = -v16;
	}

	PrevYDelta = v16;
	this->GetView();
	this->field_F4 = 1;
}

bool FFXI::CYy::CYyCamMng2::Check()
{
	if (this->field_7C != 0.0)
		return true;
	if (this->field_60.x != this->Position.x)
		return true;
	if (this->field_60.y != this->Position.y)
		return true;
	if (this->field_60.z != this->Position.z)
		return true;
	if (this->field_6C.x != this->field_50.x)
		return true;
	if (this->field_6C.y != this->field_50.y)
		return true;
	if (this->field_6C.z != this->field_50.z)
		return true;
	return this->field_78 != this->field_5C;
}

void FFXI::CYy::CYyCamMng2::Follow(CXiActor* a2)
{
	if (a2 == nullptr) {
		return;
	}

	if (CYyDb::g_pCYyDb->field_9 == 0 && a2->IsKindOf(&CYy::CXiSkeletonActor::CXiSkeletonActorClass) == false) {
		return;
	}

	if (a2->IsKindOf(&CYy::CXiModelActor::CXiModelActorClass) == true) {
		return;
	}

	if (CXiControlActor::enable_user_control_camera == false) {
		return;
	}

	IsControled -= (int)CYyDb::CheckTick();
	if (IsControled < 0) {
		IsControled = 0;
	}

	D3DXVECTOR3 var290 = this->Position;
	D3DXVECTOR3 var4C = this->field_50;
	float AnalogKey1 = 0.0;
	float AnalogKey2 = 0.0;
	if (CYyCamMng2::SomeKeyCheck() == false || FFXI::CTk::TkManager::g_pCTkInputCtrl->field_0.field_22 == 0) {
		AnalogKey1 = FFXI::Input::InputMng::GetAnalogKey(63, (FFXI::Constants::Enums::KEYTYPE)6);
		AnalogKey2 = FFXI::Input::InputMng::GetAnalogKey(63, (FFXI::Constants::Enums::KEYTYPE)7);
	}

	if (FFXI::Config::FsConfig::GetConfig(FFXI::Constants::Enums::FsConfigSubjects::Subject139)) {
		AnalogKey1 = -AnalogKey1;
	}

	if (FFXI::Config::FsConfig::GetConfig(FFXI::Constants::Enums::FsConfigSubjects::Subject140)) {
		AnalogKey2 = -AnalogKey2;
	}

	float v270 = 0.0;
	if (FFXI::CTk::CFsConf6Win::Check() == true) {
		exit(0x1001E186);
	}

	if (a2->IsParallelMove() == 0) {
		float angle = CYyDb::CheckTick() * AnalogKey1 * 0.027924445;
		if (a2->IsFreeRun() == 0) {
			D3DXVECTOR3 v282 = this->Position - this->field_50;
			long double v14 = sqrt(v282.x * v282.x + v282.y * v282.y + v282.z * v282.z);
			if (v14 < 0.01) {
				v14 = 0.01;
			}
			angle = 6.0f / v14 * angle;
		}

		if (a2->IsFreeRun() == false || TransModeTimer <= 0.0) {
			if (angle != 0.0) {
				IsControled = 10;
			}
		}
		else {
			angle = 0.0;
		}

		this->MakeMatrixWithAngle(angle);

		float offset = CYyDb::CheckTick() * AnalogKey2 * 0.10666667;
		if (a2->IsFreeRun() == false || TransModeTimer <= 0.0) {
			if (offset != 0.0) {
				IsControled = 10;
			}
		}
		else {
			offset = 0.0;
		}
		this->Position.y += offset;
	}

	if (FFXI::CTk::CFsConf6Win::Check() == true
		&& a2->IsFreeRun() == true
		&& SomeByte1 != 0
		&& v270 != 0.0) {
		exit(0x1001E364);
	}

	FFXI::CYy::CXiActor* actor = FFXI::CTk::TkManager::g_pCTkInputCtrl->GetTargetACTORWithCheck();
	if (actor == nullptr) {
		actor = CXiControlActor::user_control_target.TWOGetSearchActor();
	}

	if (a2->IsOnLift() == true) {
		float v29 = a2->GetPos()->y - ActorPrevPos.y;
		this->Position.y += v29;
		if (v29 != 0.0) {
			IsControled = 10;
		}
	}

	D3DXVECTOR4* v30 = a2->GetPos();
	Globals::CopyVec3(&ActorPrevPos, v30);
	char v288 = a2->IsFreeRun();
	if (actor == nullptr || a2 == actor) {
		a2->IsFreeRun(1);
	}
	
	D3DXVECTOR3 v283{};
	if (a2->IsFreeRun() != 0) {
		D3DXVECTOR4* v31 = a2->GetPos();
		Globals::CopyVec3(&v283, v31);
		unsigned char v38 = a2->VirtActor39();
		if (v38 == 0x80) {
			float v256 = 0.0;
			if (a2->IsOnChocobo() == true || a2->IsOnMount() == true) {
				exit(0x1001E5BD);
				CXiSkeletonActor* sactor = (CXiSkeletonActor*)a2;
				Math::BoundingBox3D* boundingBox = sactor->GetBoundingBoxByState(true);
				if (boundingBox != nullptr) {

				}
				else {

				}
				if (sactor->field_768[0] != nullptr) {

				}
			}
			else {
				Math::BoundingBox3D* boundingBox = a2->GetBoundingBoxByState(true);
				if (boundingBox == nullptr) {
					v283.y = this->field_50.y;
				}
				else {
					v256 = boundingBox->Bottom - boundingBox->Top;
					if (a2->VirtActor73() == true) {
						if (v256 >= 2.5) {
							v256 = 2.5;
						}
					}
				}
			}
			v283.y -= v256 * 0.6f;
		}
		else {
			D3DXVECTOR4 elem{};
			a2->GetElemLocal(v38, &elem);
			v283.y += elem.y;
		}
	}
	else {		
		D3DXVECTOR4* v42 = actor->GetPos();
		D3DXVECTOR4* v45 = a2->GetPos();

		Globals::CopyVec3(&v283, v42);
		v283.y += NoticeModeHeight;
		
		D3DXVECTOR4 a3{};
		a2->GetElemLocal(13, &a3);

		D3DXVECTOR3 var1B8{};
		var1B8.x = v45->x;
		var1B8.y = v45->y + a3.y;
		var1B8.z = v45->z;

		D3DXVec3Lerp(&v283, &var1B8, &v283, 0.5);
	}

	bool v47 = false;
	FFXI::Constants::Enums::GAME_STATUS status = a2->GetGameStatus();
	if (status == FFXI::Constants::Enums::GAME_STATUS::SIT
		|| status >= FFXI::Constants::Enums::GAME_STATUS::CHAIR00 && status <= FFXI::Constants::Enums::GAME_STATUS::CHAIR20) {
		Math::BoundingBox3D* boundingBox = a2->GetBoundingBoxByState(false);
			if (boundingBox != nullptr) {
				v47 = true;
				v283.y = a2->GetPos()->y - (boundingBox->Bottom - boundingBox->Top * 0.5) * 0.6f;
			}
	}
	else {
		v47 = true;
	}

	D3DXVECTOR3 var194 = this->field_50;
	if (v47 == true) {
		int tick = (int)CYyDb::CheckTick();
		while (tick > 0) {
			D3DXVECTOR3 v282 = v283 - var194;
			v282 *= 0.25;
			var194 += v282;
			tick -= 1;
		}
	}
	
	D3DXVECTOR3 v289{};
	this->SetAt(&var194);
	if (a2->IsFreeRun() != 0) {
		if (IsTracking == true) {
			v289 = var194;
			if (IsControled != 0) {
				this->field_BC.field_34 = 0;
			}

			if (this->field_BC.field_34 < 4) {
				this->field_BC.field_34 = 0;
				IsTracking = false;
			}
			else {
				int tick = CYyDb::CheckTick();
				D3DXVECTOR3 v282{};
				for (int i = 0; i < tick; ++i) {
					this->field_BC.GetOldestPos(&v282);
					D3DXVECTOR3 var1C8 = v282 - this->Position;
					float v54 = sqrt(var1C8.x * var1C8.x + var1C8.y * var1C8.y + var1C8.z * var1C8.z);
					if (v54 <= 0.05f) {
						this->field_BC.field_34 = 0;
						IsTracking = false;
					}
					else {
						var1C8.x *= 0.05f;
						var1C8 += this->Position;
						this->SetPos(&var1C8);
					}
				}
				D3DXVECTOR3 var1B8{};
				this->field_BC.GetOldestPos(&var1B8);
				D3DXVECTOR3 diff = var1B8 - var194;
				float v59 = sqrt(diff.x * diff.x + diff.y * diff.y + diff.z * diff.z);
				if (v59 > 10.0) {
					this->field_BC.field_34 = 0;
					IsTracking = false;
				}
			}
		}
		else {
			double v258{};
			if (ResetCamera == true) {
				v258 = (350.0 - CYyDb::g_pCYyDb->field_2F4) * 0.25;
				if (v258 <= -1.0 || v258 >= 1.0) {
					v258 += CYyDb::g_pCYyDb->field_2F4;
				}
				else {
					v258 = 350.0;
					ResetCamera = false;
				}
				CYyDb::g_pCYyDb->SetProjection(v258);
				FFXI::Config::FsConfig::SetConfig(Constants::Enums::FsConfigSubjects::Subject73, v258);
			}
			else if (FFXI::Input::InputMng::GetKey(63, (Constants::Enums::KEYTYPE)79, Constants::Enums::TRGTYPE_4, -1) && FFXI::Input::InputMng::GetKey(63, (Constants::Enums::KEYTYPE)80, Constants::Enums::TRGTYPE_4, -1)) {
				ResetCamera = true;
			} 
			else if (FFXI::Input::InputMng::GetKey(63, (Constants::Enums::KEYTYPE)79, Constants::Enums::TRGTYPE_4, -1)) {
				v258 = CYyDb::CheckTick() * 6.0 + CYyDb::g_pCYyDb->field_2F4;
				if (v258 > 900.0) {
					v258 = 900.0;
				}
				ClearSomeInt();
				CYyDb::g_pCYyDb->SetProjection(v258);
				FFXI::Config::FsConfig::SetConfig(Constants::Enums::FsConfigSubjects::Subject73, v258);
			}
			else if (FFXI::Input::InputMng::GetKey(63, (Constants::Enums::KEYTYPE)80, Constants::Enums::TRGTYPE_4, -1)) {
				v258 = CYyDb::g_pCYyDb->field_2F4 - CYyDb::CheckTick() * 6.0;
				if (v258 < 242.0) {
					v258 = 242.0;
				}
				ClearSomeInt();
				CYyDb::g_pCYyDb->SetProjection(v258);
				FFXI::Config::FsConfig::SetConfig(Constants::Enums::FsConfigSubjects::Subject73, v258);
			}
			else if (FFXI::CTk::CFsConf6Win::Check() == true) {
				if (GetSomeInt() > 0) {
					v258 = CYyDb::CheckTick() * 6.0 + CYyDb::g_pCYyDb->field_2F4;
					if (v258 > 900.0) {
						v258 = 900.0;
						ClearSomeInt();
					}
					CYyDb::g_pCYyDb->SetProjection(v258);
					FFXI::Config::FsConfig::SetConfig(Constants::Enums::FsConfigSubjects::Subject73, v258);
				} 
				else if (GetSomeInt() < 0) {
					v258 = CYyDb::g_pCYyDb->field_2F4 - CYyDb::CheckTick() * 6.0;
					if (v258 < 242.0) {
						v258 = 242.0;
						ClearSomeInt();
					}
					CYyDb::g_pCYyDb->SetProjection(v258);
					FFXI::Config::FsConfig::SetConfig(Constants::Enums::FsConfigSubjects::Subject73, v258);
				}
			}
			//Label 127
			bool v269 = true;
			if (a2->IsParallelMove() == 0) {
				if (FFXI::Config::FsConfig::GetConfig(Constants::Enums::FsConfigSubjects::Subject145) != 0) {
					D3DXVECTOR3 var1C8 = this->Position - var194;
					var1C8.y = 0;
					long double v77 = sqrt(var1C8.x * var1C8.x + var1C8.z * var1C8.z);
					if (v77 < 3.0) {
						if (v77 > 1.0) {
							Globals::Vec3Normalize(&var1C8);
							var1C8 *= 3.1f;
							var1C8 += var194;
							D3DXVECTOR3 v282{};
							if (CYyDb::g_pCYyDb->g_pTsZoneMap->SomeCollisionCheck(&var194, &var1C8, &v282) == true) {
								v269 = false;
							}
						}
					}
				}

				float v88 = abs(AnalogKey1);
				float v95 = abs(AnalogKey2);

				int tick = (int)CYyDb::CheckTick();
				for (int i = 0; i < tick; ++i) {
					TransModeTimer -= 1.0;
					if (TransModeTimer <= 0.0) {
						TransModeTimer = 0.0;
						D3DXVECTOR3 var1C8 = var194 - this->Position;
						float v263 = sqrt(var1C8.x * var1C8.x + var1C8.y * var1C8.y + var1C8.z * var1C8.z);
						if (FFXI::Config::FsConfig::GetConfig(Constants::Enums::FsConfigSubjects::Subject145) != 0) {
							if (v88 >= 0.01 && v95 >= 0.01) {
								D3DXVECTOR4* v101 = a2->GetPos();
								Globals::CopyVec3(&ActorPrevPos2, v101);
								SomeCounter = 4;
							}
							
							if (SomeCounter != 0) {
								D3DXVECTOR3 var184 = ActorPrevPos2;
								D3DXVECTOR4* v102 = a2->GetPos();
								D3DXVECTOR3 v285{};
								Globals::CopyVec3(&v285, v102);
								var184.y = 0.0;
								v285.y = 0.0;
								D3DXVECTOR3 diff = var184 - v285;
								float v103 = sqrt(diff.x * diff.x + diff.z * diff.z);
								if (v103 > 0.5f) {
									SomeCounter -= 1;
									if (SomeCounter < 0) {
										SomeCounter = 0;
									}
								}
							}

							if (SomeCounter == 0) {
								D3DXVECTOR3 v278 = { -v263, 0.0, 0.0 };
								Math::WMatrix v293{};
								v293.Identity();

								D3DXVECTOR4* v108 = a2->VirtActor102();
								v293.RotateY(v108->y);
								v293.Vec3TransformDrop4Self(&v278);
								D3DXVECTOR3 var1B8 = v278;
								Globals::Vec3Normalize(&var1B8);

								D3DXVECTOR3 v282{};
								v282.x = this->Position.x - var194.x;
								v282.z = this->Position.z - var194.z;
								Globals::Vec3Normalize(&v282);
								float v110 = var1B8.x * v282.x + var1B8.z * v282.z;
								if (v110 > 0.5f && v110 < 0.99f) {
									v278 += var194;
									v278 -= this->Position;
									float scale = (float)50 * 0.01f * 0.05f;
									v278 *= scale;
									this->Position.x += v278.x;
									this->Position.z += v278.z;
								}
							}
						}
						
						if (v263 > 100.0f) {
							Globals::Vec3Normalize(&var1C8);
							float v119 = (v263 - 100.0f) * 0.5f;
							var1C8 *= v119;
							var1C8 += this->Position;
							this->SetPos(&var1C8);
						}
						else if (v263 > 6.0f) {
							float v119 = (v263 - 6.0f) * 0.012f;
							var1C8 *= v119;
							var1C8 += this->Position;
							this->SetPos(&var1C8);
						}

						if (v269 == true) {
							if (v263 < 3.0f) {
								float v241 = (3.0f - v263) * -0.125f;
								var1C8 *= v241;
								var1C8 += this->Position;
								this->SetPos(&var1C8);
								IsControled = 10;
							}
							var1C8 = this->Position - var194;
							var1C8.y = 0;
							long double v124 = sqrt(var1C8.x * var1C8.x + var1C8.z * var1C8.z);
							if (v124 < 3.0) {
								Globals::Vec3Normalize(&var1C8);
								var1C8.x *= (3.0 - v124) * 0.125;
								var1C8.z *= (3.0 - v124) * 0.125;
								var1C8 += this->Position;
								var1C8.y = this->Position.y;
								this->SetPos(&var1C8);
								IsControled = 10;
							}
						}
					}
				}
			}
			
			if (IsControled == 0 && TransModeTimer <= 0.0) {
				float v126 = abs(this->Position.y - this->field_50.y);
				if (v126 < PrevYDelta && this->Position.y < this->field_50.y) {
					this->Position.y -= PrevYDelta - v126;
				}
			}
			v289 = var194;
			D3DXVECTOR3 v282{};
			if (TransModeTimer <= 0.0 && CYyDb::g_pCYyDb->g_pTsZoneMap->SomeCollisionCheck(&var194, &this->Position, &v282) == true) {
				if (IsControled != 0 || this->field_BC.field_34 < 4) {
					D3DXVECTOR3 var1C8 = v282 - this->Position;
					float v127 = sqrt(var1C8.x * var1C8.x + var1C8.y * var1C8.y + var1C8.z * var1C8.z);
					if (v127 > 0.0) {
						float v242 = 1.0f / v127;
						var1C8 *= v242;
						float v243 = v127 + 0.5f;
						var1C8 *= v243;
					}
					var1C8 += this->Position;
					this->SetPos(&var1C8);
				}
				else {
					IsTracking = true;
					IsControled = 0;
				}
			}
			else {
				this->field_BC.Add(&var194);
			}
		}

		//label 179?
		if (SomeCounter2 <= 0) {
			if (FFXI::Input::InputMng::GetKey(63, (FFXI::Constants::Enums::KEYTYPE)81, FFXI::Constants::Enums::TRGTYPE_4, -1)) {
				SomeCounter2 = 8;
			}
		}
		else {
			Math::WMatrix v292{};
			v292.Identity();
			D3DXVECTOR4* v132 = a2->VirtActor102();
			v292.RotateY(v132->y);

			D3DXVECTOR4 var184{};
			a2->GetElemLocal(2, &var184);
			
			D3DXVECTOR4 var1B8 = { -3.0, var184.y, 0.0, 1.0 };
			v292.Vec3TransformDrop4Self((D3DXVECTOR3*)& var1B8);

			D3DXVECTOR4* v133 = a2->GetPos();
			var1B8.x += v133->x;
			var1B8.y += v133->y;
			var1B8.z += v133->z;

			var184.x = this->Position.x;
			var184.y = this->Position.y;
			var184.z = this->Position.z;

			float v134 = atan2(-this->field_4._33, this->field_4._31);
			float v276 = v134 - Constants::Values::ANGLE_PI;

			int attempts = 0;
			float v138 = v276 - v132->y;
			while (v138 < Constants::Values::ANGLE_MINUS_PI) {
				if (attempts >= 100) {
					break;
				}
				attempts += 1;
				v138 += Constants::Values::ANGLE_2PI;
			}
			while (v138 >= Constants::Values::ANGLE_PI) {
				if (attempts >= 100) {
					break;
				}
				attempts += 1;
				v138 -= Constants::Values::ANGLE_2PI;
			}

			if (fabs(v138) >= 0.05f) {
				if (SomeCounter2 != 0) {
					int tick = (int)CYyDb::CheckTick();
					while (tick > 0) {
						D3DXVECTOR3 v284{};
						v284.x = var1B8.x - var184.x;
						v284.y = var1B8.y - var184.y;
						v284.z = var1B8.z - var184.z;
						v284 *= 0.125;
						var184.x += v284.x;
						var184.y += v284.y;
						var184.z += v284.z;
						tick -= 1;
					}
					this->SetPos((D3DXVECTOR3*)& var184);
					SomeCounter2 -= 1;
				}
			}
			else {
				SomeCounter2 = 0;
			}
		}
	}
	else {
		exit(0x12123434);
	}

	//Label 234
	a2->IsFreeRun(v288);
	D3DXVECTOR3 diff = var290 - this->Position;
	float v220 = sqrt(diff.x * diff.x + diff.y * diff.y + diff.z * diff.z);
	if (v220 > 0.01f || InitTimer > 0) {
		float var1D8{};
		bool check = CYyDb::g_pCYyDb->g_pTsZoneMap->VCalibrate2(this->Position.x, this->Position.y, this->Position.z, 10.0f, &var1D8);
		float v225 = var1D8 - 0.17f;
		if (check == true && v225 < this->Position.y) {
			this->Position.y = v225;
			this->SetPos(&this->Position);
		}
		else {
			bool check = CYyDb::g_pCYyDb->g_pTsZoneMap->VCalibrateUpsideDown(this->Position.x, this->Position.y, this->Position.z, 10.0f, &var1D8);
			float v225 = var1D8 + 0.17f;
			if (check == true && v225 > this->Position.y) {
				this->Position.y = v225;
				this->SetPos(&this->Position);
			}
		}

		D3DXVECTOR3 var138 = { -0.17f, 0.0f, 0.0f };
		D3DXVECTOR3 var128 = { 0.17f, 0.0f, 0.0f };
		
		Math::WMatrix* m = this->MakeMatrix();
		Math::WMatrix v290 = *m;
		Math::WMatrix v293 = v290;
		
		v293.DoSomething();
		v293.Vec3TransformDrop4Self(&var138);
		v293.Vec3TransformDrop4Self(&var128);

		bool flag1 = false;
		bool flag2 = false;
		D3DXVECTOR3 var1B8{}, var184{};
		if (CYyDb::g_pCYyDb->g_pTsZoneMap->SomeCollisionCheck(&var128, &var138, &var1B8) == true) {
			flag1 = true;
		}
		if (CYyDb::g_pCYyDb->g_pTsZoneMap->SomeCollisionCheck(&var138, &var128, &var184) == true) {
			flag2 = true;
		}

		if (flag1 == false) {
			if (flag2 == true) {
				D3DXVECTOR3 v276{ 0.0f ,0.0f ,0.0f };
				v290.Vec3TransformDrop4Self(&var184);
				v276.x -= 0.17 - var184.x;
				v293.Vec3TransformDrop4Self(&v276);
				this->SetPos(&v276);
			}
		}
		else if (flag2 == false || var1B8 != var184) {
			D3DXVECTOR3 v276{ 0.0f ,0.0f ,0.0f };
			v290.Vec3TransformDrop4Self(&var184);
			v276.x += 0.17 + var1B8.x;
			v293.Vec3TransformDrop4Self(&v276);
			this->SetPos(&v276);
			if (flag2 == true) {
				D3DXVECTOR3 v276{ 0.0f ,0.0f ,0.0f };
				v290.Vec3TransformDrop4Self(&var184);
				v276.x -= 0.17 - var184.x;
				v293.Vec3TransformDrop4Self(&v276);
				this->SetPos(&v276);
			}
		}

		this->MakeMatrix();
	}

	//254
	D3DXVECTOR3 v291{};
	if (TransModeTimer <= 0.0 && CYyDb::g_pCYyDb->g_pTsZoneMap->SomeCollisionCheck(&v289, &this->Position, &v291) == true) {
		D3DXVECTOR3 var1B8 = v291 - this->Position;
		float v232 = sqrt(var1B8.x * var1B8.x + var1B8.y * var1B8.y + var1B8.z * var1B8.z);
		if (v232 > 0.0f) {
			float v251 = 1.0f / v232;
			var1B8 *= v251;
			float v252 = v232 + 0.2f;
			var1B8 *= v252;
		}

		var1B8 += this->Position;
		this->SetPos(&var1B8);
	}

	diff = var290 - this->Position;
	float diffmag = sqrt(diff.x * diff.x + diff.y * diff.y + diff.z * diff.z);
	if (diffmag < 0.0001f) {
		this->SetPos(&var290);
		this->MakeMatrix();
	}
	if (InitTimer > 0) {
		InitTimer -= 1;
	}

	PrevYDelta = abs(this->Position.y - this->field_50.y);
}

FFXI::Math::WMatrix* FFXI::CYy::CYyCamMng2::MakeMatrix()
{
	D3DXVECTOR3 a1{}, v7{}, v8{};
	FFXI::Math::WMatrix v9{}, psrc{};
	float angle{};

	v8 = this->field_50;
	D3DXVec3Subtract(&a1, &this->field_50, &this->Position);
	v9.Identity();
	angle = atan2(-a1.x, -a1.z);
	v9.RotateZ(this->field_5C);
	v9.RotateY(angle);
	v9.RotateX(0.0f);
	v7 = this->field_A8;
	v9.Vec3TransformNormalSelf(&v7);
	Globals::Vec3Normalize(&a1);
	if (this->field_7C != 0.0f) {
		a1.y += this->field_84 * this->field_7C;
		Globals::Vec3Normalize(&a1);
	}
	D3DXVec3Add(&v8, &this->Position, &a1);
	D3DXMatrixLookAtRH(&psrc, &this->Position, &v8, &v7);
	if (psrc.CheckMatrix())
		this->field_4 = psrc;
	return &this->field_4;
}

FFXI::Math::WMatrix* FFXI::CYy::CYyCamMng2::GetView()
{
	this->MakeMatrix();
	this->field_60 = this->Position;
	this->field_6C = this->field_50;
	return &this->field_4;
}

void FFXI::CYy::CYyCamMng2::MakeMatrixWithAngle(float a2)
{
	float v11 = this->field_50.x - this->Position.x;
	float v3 = this->field_50.z - this->Position.z;
	float v4 = atan2(v3, v11) + a2;
	if (v4 > FFXI::Constants::Values::ANGLE_PI) {
		v4 -= FFXI::Constants::Values::ANGLE_2PI;
	}
	else if (v4 < FFXI::Constants::Values::ANGLE_MINUS_PI) {
		v4 += FFXI::Constants::Values::ANGLE_2PI;
	}

	float v9 = sqrt(v11 * v11 + v3 * v3);
	if (v9 < 0.0f) {
		v9 = 1.0f;
	}
	this->Position.x = this->field_50.x - cos(v4) * v9;
	this->Position.z = this->field_50.z - sin(v4) * v9;
	this->MakeMatrix();
}

FFXI::CYy::CYyCamMng2::CYyCamMng2()
{
	this->Init();
	//todo stuff here
	IsTracking = false;
	IsControled = 0;
	PrevYDelta = 0.0;
	TransModeTimer = 0.0;
	InitTimer = 0;
	ResetCamera = false;
	SomeCounter = 0;
	SomeCounter2 = 0;
	ActorPrevPos = { 0.0f, 0.0f, 0.0f };
	ActorPrevPos2 = { 0.0f, 0.0f, 0.0f };
	NoticeModeHeight = 0.0f;
	CYyCamMng2::SetSomeValues(0, 1, 0);
}

FFXI::CYy::CYyCamMng2::~CYyCamMng2()
{
	//nullsub
}
