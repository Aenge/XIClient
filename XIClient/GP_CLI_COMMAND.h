#pragma once
namespace FFXI {
	namespace Constants {
		namespace Enums {
			enum class GP_CLI_COMMAND : unsigned short {
				NONE		 = 0x000,
				LOGIN		 = 0x00A,
				GAMEOK		 = 0x00C,
				LOGINOK      = 0x011,
				LOCK_CMD     = 0x053,
				REQ_STATUS   = 0x061,
				SUBMAPCHANGE = 0x0F2,
				MAX          = 0x11E
			};
		}
	}
}