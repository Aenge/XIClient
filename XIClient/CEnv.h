#pragma once
#include "MemoryManagedObject.h"
#include "Enums.h"
namespace FFXI {
	namespace CYy {
		class CEnv : public MemoryManagedObject {
		public:
			const static BaseGameObject::ClassInfo CEnvClass;
			virtual const BaseGameObject::ClassInfo* GetRuntimeClass() override final;
			virtual ~CEnv();
			CEnv();
			void Init();
			void CheckCPU();
			void DoSomething(float);
			bool CheckField18();
			FFXI::Constants::Enums::CPUVendorID CPUType;
			char CPU_MMX_Support;
			char CPU_SSE_Support;
			char CPU_SSE3_Support;
			char CPU_3DNOW_Support;
			char field_C;
			char field_D;
			char field_E;
			char field_F;
			int CPU_Threads_Per_Core;
			char field_14;
			char field_15;
			char field_16;
			char field_17;
			int field_18;
			short field_1C;
			char field_1E;
			char field_1F;
			char field_20;
			char field_21;
			char field_22;
			char field_23;
		};
	}
}