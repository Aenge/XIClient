#pragma once
#define WIN32_LEAN_AND_MEAN
#include "d3d8to9/d3d8.hpp"
#include "WMatrix.h"
namespace FFXI {
	namespace CYy {
		class CXiOpening;  class CYyCamMng2; class CMoResource; class CMoResourceMng; class ResourceContainer;
		class CMoTaskMng; class CYyTexMng; class CYyVbMng; class CYyVb; class SomeTaskType; class CXITimerLow; 
		class CTsZoneMap; class CMoDx; class BaseProcessor; class CXiActorDraw; class CAtelIdleTask;
	}
	namespace CTk {
		class FsMenuDraw; class CTkMsgWinData;
	}
	namespace Config { class FsConfig; }
	class UnknownClass;
	class CYyDb {
		void FsGameLobbyStart();
		void HandleState();
		void OnMove();
		void TryMove();
		void ZoneProc();
		bool ZoneUpdate();
		static void Logout();
		static bool ZoneSetup();
		static void InitLogin();
		static void LoadMonNames(unsigned short, unsigned short);
		static DWORD SqBaseTime;
		static DWORD SqTimerDifference;
		static DWORD GetOffsetTime();
	public:
		static CYyDb* g_pCYyDb;
		static IDirect3DTexture8* SomeTexture1;
		static IDirect3DTexture8* SomeTexture2;
		static IDirect3DTexture8* SomeTexturesKindaLikeCApp2[2];
		static IDirect3DTexture8* TextureDisplayTaskArr[4];
		static int TextureDisplayState;
		static int TextureDisplaySubState[2];
		static Config::FsConfig* g_pFsConfig;
		static CYy::CMoTaskMng* pCMoTaskMng;
		static CYy::CMoResourceMng* pCMoResourceMng;
		static CYy::SomeTaskType* pSomeTaskType;
		static CYy::CYyTexMng* pCYyTexMng;
		static CYy::CYyVbMng* pCYyVbMng;
		static CYy::CYyVb* pCYyVb;
		static CYy::BaseProcessor* pMoProcessor;
		static CYy::ResourceContainer** ResFile_MENUMissionQuest;
		static CYy::ResourceContainer** ResFile_MENUUnk1;
		static CYy::ResourceContainer** ResFile_TEXGeneral;
		static CYy::ResourceContainer** ResFile_TEXIcons1;
		static CYy::ResourceContainer** ResFile_TEXIcons2;
		static void* SomeStatic;
		static UnknownClass* UnknownClass;
		static CYy::CAtelIdleTask* AtelIdleTask;
		static CYy::CTsZoneMap* g_pTsZoneMap;
		static CYy::CXiActorDraw* pCXiActorDraw;
		static int SomeState;
		static int SomeState2;
		static int SomeState3;
		static int SomeState4;
		static unsigned char SomeByte;
		static int SomeFlag;
		static float StaticFloat;
		static float FloatArray[4];
		static int FloatArrayIndex;
		static WORD TimerSeconds;
		static CYy::CXiOpening* XiOpening;
		static bool PatchingComplete;
		static CYy::ResourceContainer** RES_1;
		static int PhaseInitCountdown;
		static CYy::CMoDx* g_pCMoDx;
		static bool CamInitialized;
		static D3DXVECTOR4 CamPos;

		//InitLogin statics
		static int RecvKingCounter;
		static int RecvKingCounter2;
		static int CliZoneFadeInFlag;
		static int CliZoneFadeOutFlag;
		static int CliZoneFadeInCount;
		static int CliZoneFadeOutCount;
		static int MonNameCount;
		static int ZoneChgReqFlag;

		static unsigned int WeatherStartTime;
		static unsigned short WeatherNumber;
		static unsigned short WeatherOffsetTime;

		static short MusicBuff[8];
		static unsigned short MusicParams[8];
		static char MusicBuffPos;
		static short CurrentMusicNum;
		static char MusicBuffOldPos;
		
		static char* MonName;

		static bool ZoneSetUpFlag;
		static bool ZoneStartOkFlag;
		static bool RecvKingCounterFlag;
		static bool CommandRdyFlag;
		static bool IsMyroom;
		static bool IsZone724;
		static bool OpeningFlag;
		static bool LiftReadFlag;
		static bool ServerReqEventCancel;
		static bool EffectFadeFlag;

		static unsigned short CliEventMode;
		static unsigned short CliEventModeLocal;
		static bool EventRdyFlag;

		static float CheckTick();
		static void SetField2FC(float);
		static void SetField300(float);
		static void SetField314(float);
		static void SetField318(float);
		static unsigned short GetBackgroundYRes();
		static unsigned short GetBackgroundXRes();
		static void AtelIdle();
		static void CliLocalTask();
		static bool ZoneStartOk();
		static void NormalMusicPlay();
		static void sqTimerInit();
		static DWORD sqGetCalenderTime();
		static void Clean();
		virtual ~CYyDb();
		CYyDb();
		void InitSubState(short);
		void CountdownTillClose();
		void IncrementSubState();
		char Init(char*);
		
		void SomeCalc1();
		char MainFlow();
		bool PhaseInit();
		bool OpeningMovieIsFinished();
		void SetFarColor(unsigned char, unsigned char, unsigned char);
		void SetBackColor(unsigned char, unsigned char, unsigned char);
		void Set324(int);
		void SetProjection(float);
		void SetField328(int);
		void SetField334(int);
		void SetField338(int);
		void SetView(FFXI::Math::WMatrix*);
		void LinkCameraManager(CYy::CYyCamMng2*);
		bool MapStart1();
		short SubState;
		short field_6;
		char field_8;
		char field_9;
		char field_A;
		char UseMIPMapping;
		char field_C;
		char field_D;
		char field_E;
		char field_F;
		unsigned short AppWidth;
		unsigned short AppHeight;
		unsigned short UIXRes;
		unsigned short UIYRes;
		unsigned short BackgroundXRes;
		unsigned short BackgroundYRes;
		CYy::CXITimerLow* Timer1;
		CYy::CXITimerLow* Timer2;
		int CloseTimer;
		float field_28;
		float field_2C;
		int FPSDivisor;
		int field_34;
		int field_38;
		int field_3C;
		int field_40;
		int MainState;
		int field_48;
		int CurrentZoneId;
		CYy::CYyCamMng2* CameraManager;
		FFXI::Math::WMatrix field_54;
		FFXI::Math::WMatrix field_94;
		FFXI::Math::WMatrix field_D4;
		FFXI::Math::WMatrix field_114;
		FFXI::Math::WMatrix field_154;
		FFXI::Math::WMatrix field_194;
		FFXI::Math::WMatrix field_1D4;
		FFXI::Math::WMatrix field_214;
		FFXI::Math::WMatrix field_254;
		FFXI::Math::WMatrix field_294;
		float field_2D4;
		float field_2D8;
		float field_2DC;
		float field_2E0;
		float field_2E4;
		float field_2E8;
		float field_2EC;
		float field_2F0;
		float field_2F4;
		float field_2F8;
		float field_2FC;
		float field_300;
		D3DCOLOR FarColor;
		D3DCOLOR BackColor;
		char field_30C;
		char field_30D;
		char field_30E;
		char field_30F;
		char field_310;
		char field_311;
		char field_312;
		char field_313;
		float field_314;
		float field_318;
		D3DCOLOR field_31C;
		D3DCOLOR field_320;
		int field_324;
		int field_328;
		unsigned char field_32C;
		unsigned char field_32D;
		unsigned char field_32E;
		unsigned char field_32F;
		float field_330;
		D3DCOLOR field_334;
		int field_338;
	};
}