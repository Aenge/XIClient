#pragma once
#include "CXiControlActor.h"

namespace FFXI {
	namespace CYy {
		class CXiModelActor : public CXiControlActor {
		public:
			static const BaseGameObject::ClassInfo CXiModelActorClass;
			virtual const BaseGameObject::ClassInfo* GetRuntimeClass() override final;
		};
	}
}