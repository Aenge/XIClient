#include "CMoResourceMng.h"
#include "MemoryPoolManager.h"
#include "CTkMsbDataList.h"
#include "CYyDb.h"
#include "XIFileManager.h"
#include "TkManager.h"
#include "ResourceContainer.h"
#include "CYyBmp2.h"
#include "CYyMsb.h"
#include "a32or27thing.h"
#include "_49SubList.h"
#include "Globals.h"
#include "RegistryConfig.h"
#include "Enums.h"
#include "CYyScheduler.h"
#include "CMoTaskMng.h"
#include "CYyFileLoadTask.h"
#include "CXiActor.h"
#include <iostream>
using namespace FFXI::CYy;

const BaseGameObject::ClassInfo CMoResourceMng::CMoResourceMngClass{
	"CMoResourceMng", sizeof(CMoResourceMng), &BaseGameObject::BaseGameObjectClass
};

CMoResource** CMoResourceMng::LastResourceCreatedByType[Constants::Values::COUNT_RESOURCE_TYPES] = { nullptr };
CMoResource** CMoResourceMng::MostRecentLoadedResourceReference{ nullptr };
int CMoResourceMng::ActiveFileLoadTaskCount{ 0 };
int CMoResourceMng::NumFileIndex{ 0 };
ISAACRandom CMoResourceMng::rng{};
char CMoResourceMng::SomeByte{ 0 };

ResourceContainer** CMoResourceMng::file27{ nullptr };
CYyScheduler** CMoResourceMng::damv{ nullptr };
CYyScheduler** CMoResourceMng::misv{ nullptr };
CYyScheduler** CMoResourceMng::curv{ nullptr };

CYyOs2VtxBuffer* CMoResourceMng::os2vtxbuffers[16] = { nullptr };
const BaseGameObject::ClassInfo* FFXI::CYy::CMoResourceMng::GetRuntimeClass()
{
	return &CMoResourceMngClass;
}

FFXI::CYy::CMoResourceMng::~CMoResourceMng()
{
	if (CMoResourceMng::file27 != nullptr) {
		(*CMoResourceMng::file27)->DecrementReferenceCount();
		CMoResource::UnlinkFromManager((CMoResource***) &CMoResourceMng::file27);
		CMoResourceMng::file27 = nullptr;
	}

	if (this->Unknown3) {
		(*this->Unknown3)->DecrementReferenceCount();
		CMoResource::UnlinkFromManager((CMoResource***)&this->Unknown3);
		this->Unknown3 = nullptr;
	}
	if (!CMoResourceMng::MostRecentLoadedResourceReference) return;

	CMoResource* last = *CMoResourceMng::MostRecentLoadedResourceReference;
	if (last) {
		CMoResource* v7{ nullptr };
		do {
			CMoResource** prev = last->PreviousLoadedResourceReference;
			if (prev)
				v7 = *prev;
			else
				v7 = nullptr;
			last->Metadata.StateFlags |= CMoResource::ResourceStateFlags::RESOURCE_CLONED;
			if (!FFXI::Config::RegistryConfig::g_pOptions->Other.field_28)
				last->Metadata.MemoryReferenceCount &= 0x8000u;
			CMoResource::UnlinkFromManager(&last->Metadata.SelfReference);
			last = v7;
		} while (v7);
	}
}

FFXI::CYy::CMoResourceMng::CMoResourceMng()
{
	int v1 = sizeof(this->ResourceSlotPool) / sizeof(this->ResourceSlotPool[0]) - 1;
	void** Resources = this->ResourceSlotPool;
	
	for (int i = 0; i < v1; ++i) {
		Resources[i] = Resources + i + 1;
	}

	this->ResourceSlotPool[v1] = nullptr;
	this->NextAvailableSlot = this->ResourceSlotPool;
	this->Unknown3 = nullptr;
	this->ResourceCount = 0;

}

int FFXI::CYy::CMoResourceMng::SomeFileMappingFunc(int a1, int a2)
{
	switch (a1) {
	case 1:
		return a2 + 0x996B;
	case 2:
		if (a2 < 0x100)
			return a2 + 0x809F;

		if (a2 < 0x200)
			return a2 + 0xC7EF;

		if (a2 < 0xE00)
			return 0;

		if (a2 < 0xF00 || a2 < 0x1000)
			return a2 + 0xE75B;

		return 0x809F;
	case 3:
		return a2 + 0x1889B;
	case 4:
		return a2 + 0x19131;
	default:
		if (a2 >= 0xDAC)
			return a2 - 0xDAC + 0x18D6B;

		if (a2 >= 0xBB8)
			return a2 - 0xBB8 + 0x18643;

		if (a2 >= 0x5DC)
			return a2 - 0x5DC + 0xCA53;

		return a2 + 0x514;
	}
}

bool FFXI::CYy::CMoResourceMng::IsResourceReady(CMoResource*** a1)
{
	bool result = CYyDb::g_pCYyDb->pCMoResourceMng->InList(*a1);
	if (result == false)
		return false;

	return *a1 && **a1 && (**a1)->GetDependencyCount() == 0;
}

ResourceContainer** FFXI::CYy::CMoResourceMng::GetOrLoadDatByIndex(int a2)
{
	CMoResource** prmp = CMoResourceMng::LastResourceCreatedByType[FFXI::Constants::Enums::ResourceType::Rmp];
	ResourceContainer* rmp{ nullptr };
	if (prmp)
		rmp = (ResourceContainer*)*prmp;

	CMoResource* head{ nullptr };
	while (rmp) {
		head = rmp->GetParent();
		if (head == rmp && rmp->FileIndex == a2) {
			rmp->Metadata.MemoryReferenceCount |= 0x8000;
			return (ResourceContainer**)rmp->Metadata.SelfReference;
		}
		if (rmp->Metadata.PreviousResourceOfSameType)
			rmp = (ResourceContainer*)(*rmp->Metadata.PreviousResourceOfSameType);
		else
			rmp = nullptr;
	}

	CMoTask* loadtask = CYyDb::g_pCYyDb->pCMoTaskMng->FindInQueue(a2, 'Mrt\0');
	CYyFileLoadTask* fileloadtask{ nullptr };
	if (loadtask) {
		fileloadtask = (CYyFileLoadTask*)loadtask;
		return fileloadtask->ResourceFile;
	}
	else {
		char* mem = MemoryPoolManager::globalInstance->Get(sizeof(CYyFileLoadTask), MemoryPoolManager::MemoryPoolType::Ex);
		CMoTaskMng::DeleteThisTask = true;
		if (mem) {
			fileloadtask = new (mem) CYyFileLoadTask();
			fileloadtask->FileDataBuffer = nullptr;
			fileloadtask->FileStat = nullptr;
		}

		//fileloadtask could be null, as per the client
		CYyDb::g_pCYyDb->pCMoTaskMng->DoSomething(fileloadtask);
		fileloadtask->Init(a2);
		return fileloadtask->ResourceFile;
	}

	return nullptr;
}

CMoResource*** FFXI::CYy::CMoResourceMng::FindNextUnder(CMoResource*** a1, CMoResource* a2, Constants::Enums::ResourceType a3, int a4)
{
	int v5 = ((a2->TypeSizeFlags & 0x7F) != 1) - 1;
	CMoResource* v4 = a2;
	while (v4) {
		int type = v4->TypeSizeFlags & 0x7F;
		if (type == FFXI::Constants::Enums::ResourceType::Rmp)
			++v5;
		if (type == FFXI::Constants::Enums::ResourceType::Terminate && --v5 < 0) {
			*a1 = nullptr;
			return a1;
		}
		if (a3) {
			if (type == a3) {
				if (!a4 || a4 == v4->ResourceID) {
					*a1 = v4->Metadata.SelfReference;
					return a1;
				}
			}
		}
		else if (v4->ResourceID == a4) {
			*a1 = v4->Metadata.SelfReference;
			return a1;
		}

		CMoResource** Next = v4->Metadata.NextResource;
		if (Next)
			v4 = *Next;
		else
			v4 = nullptr;
	}
	*a1 = nullptr;
	return a1;
}

void FFXI::CYy::CMoResourceMng::RemoveRes(CMoResource** a2)
{
	if (a2 == nullptr) {
		return;
	}

	void* v2 = *a2;
	int lastIndex = sizeof(this->ResourceSlotPool) / sizeof(this->ResourceSlotPool[0]) - 1;

	if (v2 < this->ResourceSlotPool || v2 > (this->ResourceSlotPool + lastIndex)) {
		*a2 = (CMoResource*)this->NextAvailableSlot;
		this->NextAvailableSlot = a2;
		this->ResourceCount -= 1;
		if (this->ResourceCount < 0)
			this->ResourceCount = 0;
	}
}

void FFXI::CYy::CMoResourceMng::Unlink(CMoResource** a2)
{
	CMoResource* v2 = *a2;
	if ((v2->Metadata.MemoryReferenceCount & 0x7FFF) == 0) {
		if ((v2->Metadata.StateFlags & CMoResource::ResourceStateFlags::RESOURCE_CLONED) == 0)
		{
			CMoResource* v3 = v2->GetRoot();
			if ((v3->Metadata.MemoryReferenceCount & 0x7FFF) != 0)
				return;

			CMoResource* v7{ nullptr };
			if (v3->Metadata.NextResource)
				v7 = *v3->Metadata.NextResource;

			if (v7) {
				CMoResource* v9{ nullptr };
				do {
					if (v7->Metadata.NextResource)
						v9 = *v7->Metadata.NextResource;
					else
						v9 = nullptr;
					v7->CMoResource::Close();
					v7 = v9;
				} while (v9);
			}

			v2 = v3;
		}
		v2->CMoResource::Close();
	}
}

ResourceContainer*** FFXI::CYy::CMoResourceMng::LoadNumFile(ResourceContainer*** a2, int a3)
{
	ResourceContainer** piVar6{ nullptr };
	this->SetNumFileIndex(a3);
	ResourceContainer** Last = (ResourceContainer**)CMoResourceMng::LastResourceCreatedByType[Constants::Enums::ResourceType::Rmp];
	if (Last) {
		piVar6 = (ResourceContainer**)(*Last)->Metadata.SelfReference;
		while (piVar6 != nullptr) {
			ResourceContainer* piVar4 = (ResourceContainer*)*piVar6;
			if (piVar4->GetParent() == piVar4 && piVar4->FileIndex == a3) {
				(*piVar6)->Metadata.MemoryReferenceCount |= 0x8000;
				*a2 = piVar6;
				return a2;
			}
			piVar6 = (ResourceContainer**)piVar4->Metadata.PreviousResourceOfSameType;
		}
	}
	unsigned int filesize = FFXI::File::XIFileManager::g_pXIFileManager->GetFileSizeByNumfile(a3);
	if (filesize > 0) {
		char* buffer = new char[filesize];
		if (buffer) {
			int iVar3 = FFXI::File::XIFileManager::g_pXIFileManager->ReadNumfileNow(a3, buffer, filesize, 0);
			if (iVar3 != 0) {
				ResourceContainer** v13{ nullptr };
				this->Parse(buffer, filesize, &v13, nullptr);
				if (v13 && *v13) {
					(*v13)->FileIndex = a3;
					delete[] buffer;
					*a2 = v13;
					return a2;
				}
			}
			delete[] buffer;
		}
	}
	*a2 = nullptr;
	return a2;
}

ResourceContainer*** FFXI::CYy::CMoResourceMng::LoadNumFile2(ResourceContainer*** a2, int a3)
{
	ResourceContainer** piVar6{ nullptr };
	this->SetNumFileIndex(a3);
	ResourceContainer** Last = (ResourceContainer**)CMoResourceMng::LastResourceCreatedByType[Constants::Enums::ResourceType::Rmp];
	if (Last) {
		piVar6 = (ResourceContainer**)(*Last)->Metadata.SelfReference;
		while (piVar6 != nullptr) {
			ResourceContainer* piVar4 = reinterpret_cast<ResourceContainer*>(*piVar6);
			if (piVar4->GetParent() == piVar4 && piVar4->FileIndex == a3) {
				(*piVar6)->Metadata.MemoryReferenceCount |= 0x8000;
				*a2 = piVar6;
				return a2;
			}
			piVar6 = (ResourceContainer**)piVar4->Metadata.PreviousResourceOfSameType;
		}
	}
	unsigned int filesize = FFXI::File::XIFileManager::g_pXIFileManager->GetFileSizeByNumfile(a3);
	if (filesize > 0) {
		char* buffer = new char[filesize];
		if (buffer) {
			int iVar3 = FFXI::File::XIFileManager::g_pXIFileManager->ReadNumfileNow(a3, buffer, filesize, 0);
			if (iVar3 != -1) {
				ResourceContainer** v13{ nullptr };
				this->Parse(buffer, filesize, &v13, nullptr);
				if (v13 && *v13) {
					ResourceContainer* v15{ nullptr };
					(*v13)->FileIndex = a3;
					delete[] buffer;
					*a2 = v13;
					return a2;
				}
			}
			delete[] buffer;
		}
	}
	*a2 = nullptr;
	return a2;
}

ResourceContainer*** FFXI::CYy::CMoResourceMng::FindFile(ResourceContainer*** a2, int a3, bool a4)
{
	CMoResource* v4 = nullptr;
	if (MostRecentLoadedResourceReference != nullptr) {
		v4 = *MostRecentLoadedResourceReference;
	}

	while (v4 != nullptr) {
		int resourceType = v4->TypeSizeFlags & 0x7F;
		if (resourceType == FFXI::Constants::Enums::ResourceType::Rmp || resourceType == FFXI::Constants::Enums::ResourceType::Rmw) {
			ResourceContainer* file = (ResourceContainer*)v4;
			if (file->FileIndex == a3) {
				*a2 = (ResourceContainer**)file->Metadata.SelfReference;
				return a2;
			}
		}

		if (v4->PreviousLoadedResourceReference != nullptr) {
			v4 = *v4->PreviousLoadedResourceReference;
		}
		else {
			v4 = nullptr;
		}
	}
		
	if (a4 == true) {
		//file load task
		exit(0x10072E36);
	}

	*a2 = nullptr;
	return a2;
}

ResourceContainer*** FFXI::CYy::CMoResourceMng::Parse(char* p_buf, int p_buflen, ResourceContainer*** a2, CMoResource** a4)
{
	ResourceContainer** headPtr{ nullptr };
	if ((p_buf[4] & 0x7F) == Constants::Enums::ResourceType::Rmp) {
		ResourceContainer* head = (ResourceContainer*)CMoResource::OpenResource(p_buf, nullptr);
		if (!head) {
			*a2 = nullptr;
			return a2;
		}
		if (a4) {
			*a4 = head;
			this->RemoveRes(head->Metadata.SelfReference);
			head->Metadata.SelfReference = a4;
		}

		head->RegisterWithManager();
		head->ParentResourceReference = head->Metadata.SelfReference;
		head->GetRoot()->Metadata.ActiveDependencyCount = 0;

		headPtr = (ResourceContainer**)head->Metadata.SelfReference;
		ResourceContainer** headList[0x100];

		int pos = 16 * ((*((int*)p_buf + 1) >> 7) & 0x7FFFF);

		int headIndex = 0;
		headList[headIndex] = headPtr;

		CMoResource* lastRes = head;
		p_buf += pos;
		while (p_buf) {
			CMoResource* newRes = CMoResource::OpenResource(p_buf, head);
			this->Link(newRes);

			int newType = newRes->TypeSizeFlags & 0x7F;
			if (newType == Constants::Enums::ResourceType::Rmp) {
				ResourceContainer* newHead = (ResourceContainer*)newRes;
				head = newHead;
				headList[++headIndex] = (ResourceContainer**)newHead->Metadata.SelfReference;
			}
			if (newType == Constants::Enums::ResourceType::Bmp2 ||
				newType == Constants::Enums::ResourceType::Msb) {
				char* mem = MemoryPoolManager::globalInstance->GetOrUpper(sizeof(a32or27thing), MemoryPoolManager::MemoryPoolType::Lower);
				CMoResourceMng::SomeByte = 1;
				a32or27thing* newGraphic = new (mem) a32or27thing();
				CYyBmp2* casted = (CYyBmp2*)newRes;

				newGraphic->ResourceID = casted->ResourceID;
				newGraphic->TypeSizeFlags = 0x20A;
				newGraphic->ParentResourceReference = nullptr;
				newGraphic->PreviousLoadedResourceReference = nullptr;
				newGraphic->Init(casted->Data + 1);
				newGraphic->ParentResourceReference = head->Metadata.SelfReference;
				newGraphic->Metadata.StateFlags = CMoResource::ResourceStateFlags::RESOURCE_ACTIVE;

				this->Link(newGraphic);
				lastRes->Metadata.LinkToNext(newGraphic);
				newGraphic->Metadata.LinkToPrevious(lastRes);
				newGraphic->Metadata.LinkToNext(casted);
				casted->Metadata.LinkToPrevious(newGraphic);
			}
			else {
				lastRes->Metadata.LinkToNext(newRes);
				newRes->Metadata.LinkToPrevious(lastRes);
			}

			newRes->Metadata.StateFlags &= ~CMoResource::ResourceStateFlags::RESOURCE_CLOSING;

			if ((newRes->TypeSizeFlags & 0x7F) == Constants::Enums::ResourceType::Terminate) {
				
				if (head->ParentResourceReference)
					head = (ResourceContainer*)*head->ParentResourceReference;
				else
					head = nullptr;

				ResourceContainer** popHead = headList[headIndex];
				if (popHead && *popHead)
					(*popHead)->TerminateNode = newRes->Metadata.SelfReference;

				if (headIndex <= 0)
					break;
				--headIndex;
			}

			lastRes = newRes;
			int resSize = 16 * ((newRes->TypeSizeFlags >> 7) & 0x7FFFF);
			p_buf += resSize;
		}

		CMoResource* res{ nullptr };

		if (head->Metadata.NextResource) {
			int value = 1;
			for (int i = 0; i < 3; ++i) {
				res = *head->Metadata.NextResource;

				while (res) {
					res->InitializeByType(value);
					if (res->Metadata.NextResource)
						res = *res->Metadata.NextResource;
					else
						res = nullptr;
				}
				value *= 2;
			}
		}
	}
	else {
		throw "does this ever happen";
	}

	if (this->Unknown3 != nullptr)
		(*headPtr)->CheckGenerators();

	*a2 = headPtr;
	return a2;
}

FFXI::CTk::_49SubList*** FFXI::CYy::CMoResourceMng::LoadNumFileGet49(int a2, ResourceContainer*** a3)
{
	ResourceContainer** a0{ nullptr };
	ResourceContainer*** NumFile = this->LoadNumFile(&a0, a2);
	*a3 = *NumFile;
	(**NumFile)->IncrementReferenceCount();

	CYyMsb** result{ nullptr };
	
	(**NumFile)->FindResourceUnder((CMoResource***)&result, Constants::Enums::ResourceType::Msb, 0);

	if (result)
		return CTk::TkManager::g_CTkMenuDataList.FindMenuShapeFile((char*)&(*result)[1]);
	
	return nullptr;
}

CMoResource** FFXI::CYy::CMoResourceMng::Link(CMoResource* a3)
{
	CMoResource** res = a3->Metadata.SelfReference;
	if (!res) {
		res = (CMoResource**)(this->NextAvailableSlot);
		if (!res) {
			return nullptr;
		}
		this->NextAvailableSlot = *res;
		*res = a3;
		a3->Metadata.SelfReference = res;
		this->ResourceCount += 1;
	}
	
	return res;
}

bool FFXI::CYy::CMoResourceMng::InList(CMoResource** a2)
{
	if (a2 == nullptr)
		return false;

	const int lastIndex = sizeof(this->ResourceSlotPool) / sizeof(this->ResourceSlotPool[0]) - 1;

	if ((int)a2 < (int)this->ResourceSlotPool) 
		return false;
	if ((int)a2 > (int)(this->ResourceSlotPool + lastIndex))
		return false;

	if ((int)(*a2) < (int)this->ResourceSlotPool)
		return true;
	if ((int)(*a2) > (int)(this->ResourceSlotPool + lastIndex))
		return true;

	return false;
}

void FFXI::CYy::CMoResourceMng::SetNumFileIndex(int p_index)
{
	CMoResourceMng::NumFileIndex = p_index;
}

void FFXI::CYy::CMoResourceMng::LoadStartScheduler(int a1, int a2, CXiActor* a3, CXiActor* a4, unsigned int a5)
{
	ResourceContainer** file = nullptr;
	FFXI::CYyDb::g_pCYyDb->pCMoResourceMng->LoadNumFile(&file, a1);
	
	(*file)->Metadata.StateFlags |= CMoResource::ResourceStateFlags::RESOURCE_SCHEDULED;
	
	CYyScheduler** scheduler = nullptr;
	(*file)->FindResourceUnder((CMoResource***) & scheduler, Constants::Enums::ResourceType::Scheduler, a2);
	
	if (scheduler != nullptr) {
		(*scheduler)->CreateStartTask(a3, a4, a5, nullptr);
	}
}

void FFXI::CYy::CMoResourceMng::KillScheduler(int a1, int a2, CXiActor* a3, CXiActor* a4)
{
	ResourceContainer** file = nullptr;
	FFXI::CYyDb::g_pCYyDb->pCMoResourceMng->FindFile(&file, a1, true);

	if (file != nullptr) {
		CYyScheduler** scheduler = nullptr;
		(*file)->FindResourceUnder((CMoResource***)&scheduler, Constants::Enums::ResourceType::Scheduler, a2);

		if (scheduler != nullptr) {
			(*scheduler)->Kill(a3, a4);
		}
	}	
}

int FFXI::CYy::CMoResourceMng::GetNumFileIndex()
{
	return CMoResourceMng::NumFileIndex;
}

void FFXI::CYy::CMoResourceMng::InitUnk3(int a1)
{
	this->LoadNumFile2(&this->Unknown3, a1);
	if (this->Unknown3 != nullptr) {
		(*this->Unknown3)->IncrementReferenceCount();
		(*this->Unknown3)->CheckGenerators();
	}

	this->LoadNumFile(&CMoResourceMng::file27, 27);
	if (file27 != nullptr) {
		CMoResource** unused{ nullptr };
		(*file27)->IncrementReferenceCount();
		CMoResourceMng::damv = *(CYyScheduler***)(*file27)->FindResourceUnder(&unused, FFXI::Constants::Enums::ResourceType::Scheduler, 'vmad');
		CMoResourceMng::misv = *(CYyScheduler***)(*file27)->FindResourceUnder(&unused, FFXI::Constants::Enums::ResourceType::Scheduler, 'vsim');
		CMoResourceMng::curv = *(CYyScheduler***)(*file27)->FindResourceUnder(&unused, FFXI::Constants::Enums::ResourceType::Scheduler, 'vruc');
	}
	//sub //TODO
	//doesn't seem like this is used anywhere
}

void** FFXI::CYy::CMoResourceMng::ReserveSlot()
{
	void** nextfree = (void**)this->NextAvailableSlot;
	if (nextfree) {
		this->NextAvailableSlot = *nextfree;
		*nextfree = nullptr;
		return nextfree;
	}

	return nullptr;
}
