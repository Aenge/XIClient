#pragma once
#define WIN32_LEAN_AND_MEAN
#include "Enums.h"
#include <wtypes.h>

namespace FFXI {
	class GlobalStruct {
	public:
		static GlobalStruct g_GlobalStruct;
		int field_0{ 0 };
		HKEY phkResult{ 0 };
		int RegionCode{ -1 };
		int field_C{ 0 };
		int ClientExpansions{ 1 };
		int ServerExpansions{ 1 };
		unsigned int field_18{ 0x80 };
		unsigned int field_1C{ 0xCDF23B55 };
		unsigned int field_20{ 0xFF1600AF };

		int GetRegionCode();
		bool Check10Flag(int);
		bool Check14Flag(int);
	};
}