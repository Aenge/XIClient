#include "CYyLowerMemory.h"

using namespace FFXI::CYy;

const BaseGameObject::ClassInfo CYyLowerMemory::CYyLowerMemoryClass{
	"CYyLowerMemory", sizeof(CYyLowerMemory), &BaseGameObject::BaseGameObjectClass
};

const BaseGameObject::ClassInfo* CYyLowerMemory::GetRuntimeClass() {
	return &CYyLowerMemoryClass;
}

