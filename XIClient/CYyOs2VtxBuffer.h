#pragma once
#include "CMoTask.h"
#include "d3d8to9/d3d8.hpp"
namespace FFXI {
	namespace CYy {
		class CMoOs2;
		class CYyOs2VtxBuffer : public CMoTask {
		public:
			const static BaseGameObject::ClassInfo CYyOs2VtxBufferClass;
			static unsigned short* DataPointer1;
			static unsigned short* DataPointer2;
			virtual const BaseGameObject::ClassInfo* GetRuntimeClass() override final;
			virtual char OnMove() override final;
			CYyOs2VtxBuffer(CMoOs2**);
			~CYyOs2VtxBuffer();
			bool CheckOs2Data(unsigned char*, int*);
			bool CreateBuffers();
			bool FillBuffers(unsigned char*);
			bool ParseFloats();
			CMoOs2** Os2Resource;
			int State;
			int Substate;
			IDirect3DIndexBuffer8* IndexBuffer;
			IDirect3DVertexBuffer8* field_44;
			IDirect3DVertexBuffer8* field_48;
			IDirect3DVertexBuffer8* VertexBuffer;
			IDirect3DVertexBuffer8* field_50;
			IDirect3DVertexBuffer8* field_54;
			int SomeCount;
			void* field_5C;
			int field_60;
			DWORD ShaderHandle;
			char field_68;
			char field_69;
			void* field_6A;
			void* field_6E;
			void* field_72;
			int field_76;
			void* field_7A;
			unsigned char* field_7E;
			int field_82;
			int field_86;
			int StateCounter;
			//vertex buffer data pointers
			void* field_8E;
			void* field_92;
			void* field_96;
			void* field_9A;
			char field_9E;
			char field_9F;
		};
	}
}