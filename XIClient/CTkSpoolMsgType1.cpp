#include "CTkSpoolMsgType1.h"
#include "TkManager.h"
#include "CTkMenuMng.h"
#include "PrimMng.h"
#include "Strings.h"
FFXI::CTk::CTkSpoolMsgType1::CTkSpoolMsgType1(CTkDrawMessageWindow* a2) 
	: CTkSpoolMsgBase(a2)
{
}

void FFXI::CTk::CTkSpoolMsgType1::CreateSpoolMsg(char* a2, TK_LOGDATAHEADER a3)
{
	if (PrimMng::g_pTkSpoolMsg->IsDrawWindow() == false) {
		TkManager::g_CTkMenuMng.CreateDrawMenu(Constants::Strings::MenuSpoolMsg, true, 0);
	}

	PrimMng::g_pTkSpoolMsg->StoreMsg(a2, a3);
}

void FFXI::CTk::CTkSpoolMsgType1::DestroyWind()
{
	TkManager::g_CTkMenuMng.DestroyDrawMenu(Constants::Strings::MenuSpoolMsg);
}
