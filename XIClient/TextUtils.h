#pragma once
namespace FFXI {
	namespace Util {
		class Text {
		public:
			static unsigned int FsTrimStr(char*);
			static unsigned int SomeCopy(char*, unsigned int, char*, unsigned int);

			static unsigned char FsGetLetterByte(const char, const char);
			static unsigned int GetCodeLen(const char);
			static unsigned int GetLongCodeLen(const char, const char);
			static unsigned int GetCode(const char*, short*);
			static unsigned int GetSomeLen(char*, unsigned int);

			static bool IsOneByteCode(const char);
			static bool IsTwoByteCode(const char);

			static bool ShouldUnIndent(const char*);
			static bool IsHyphenatedWord(const char*);
			
			static bool CharIsNewline(const char);
			static int FindToken(const char*, unsigned int, const char[][4], const unsigned char*);
			static int GetNACKPos(const char*, unsigned int);
		};
	}
}