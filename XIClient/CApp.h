#pragma once
#define WIN32_LEAN_AND_MEAN
#include "MemoryManagedObject.h"
#include <wtypes.h>
namespace FFXI {
	namespace Input { class InputMng; }
	namespace Network { class NT_SYS; }
	namespace CYy {
		class CApp : public MemoryManagedObject {
		public:
			static CApp* g_pCApp;
			static bool Initialize(HWND);
			static void DestroyApp();
			const static BaseGameObject::ClassInfo CAppClass;
			virtual const BaseGameObject::ClassInfo* GetRuntimeClass() override final;
			CApp(char*, HINSTANCE, HWND, char*, char*);
			~CApp();
			static void WinMsgHandler(MSG* msg);
			static bool InitInput(HINSTANCE);
			static int GetAppState();
			static void SetAppState(int);
			static Input::InputMng* g_pInputMng;
			static int AppState;
			static Network::NT_SYS* g_pNT_SYS;
			static Network::NT_SYS g_NT_SYS;
			static UINT period;
			bool ClientTick();
			int Execute();
			void TryInitialize();
			void RunMainLoop();
			void SetupWindowRefreshThread(HWND);
			void CleanupWindowRefreshThread();
			char* SomeDateString{nullptr};
			char* SomeTimeString{nullptr};
			char* VersionString{nullptr};
			char* FFXIDirectory{nullptr};
			HINSTANCE hmod{};
			HWND hWnd{};
			char DXdeviceReady{0};
			char isInitialized{0};
			//1E, 1F usage not found yet
			char field_1E{0};
			char field_1F{0};
			RECT windowPosition{};
			//30-37 usage not found yet
			char field_30{0};
			char field_31{0};
			char field_32{0};
			char field_33{0};
			char field_34{0};
			char field_35{0};
			char field_36{0};
			char field_37{0};
			HANDLE threadHandle{nullptr};
			char threadRunning{0};
			char threadShouldExit{0};
			//3E, 3F usage not found yet
			char field_3E{0};
			char field_3F{0};
			HWND threadTargetWindow{};
		};
	}
}