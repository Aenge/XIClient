#pragma once

namespace FFXI {
	namespace Network {
		class enQue;
		class enQueBuff {
		public:
			unsigned short field_0;
			unsigned short field_2;
			char data[0x100];
			int field_104;
			int field_108;
			enQue* field_10C;
		};
	}
}