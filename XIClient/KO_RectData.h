#pragma once
#define WIN32_LEAN_AND_MEAN
#include "d3dx9math.h"

namespace FFXI {
	namespace Math {
		struct KO_RectData {
			const static KO_RectData DataTbl[6];
			static bool HitCheck(const D3DXVECTOR3*, const D3DXVECTOR3*, const KO_RectData*);
			int field_0;
			int field_4;
			int field_8;
			int field_C;
			D3DXVECTOR3 field_10;
		};
	}
}