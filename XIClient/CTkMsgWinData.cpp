#include "CTkMsgWinData.h"
#include "TextRenderer.h"
#include "TextUtils.h"
#include "PrimMng.h"
#include "TkManager.h"
#include "CTkDrawMessageWindowOne.h"
#include "CTkDrawMessageWindowTwo.h"
#include "CTkSpoolMsgType1.h"
#include "CTkSpoolMsgType2.h"
#include <string>
#include "CYyDb.h"
#include "FsTextInput.h"
#include "CTkAddMsgHelper.h"

int FFXI::CTk::CTkMsgWinData::lg_currentcolor{};
char FFXI::CTk::CTkMsgWinData::LogStringBuffer[300][2048]{};

FFXI::CTk::CTkMsgWinData::CTkMsgWinData() {
	this->field_6403C = 0;
	this->InitAll();
}

unsigned int FFXI::CTk::CTkMsgWinData::fsGetFontWidth(char* a1)
{
	if (Util::Text::IsOneByteCode(*a1)) {
		short jis = (short)*a1;
		return Text::TextRenderer::SJisFontWidthGet(*a1);
	}
		
	unsigned short doublefont = a1[0];
	doublefont <<= 8;
	doublefont |= a1[1];
	return Text::TextRenderer::SJisFontWidthGet(doublefont);
}

unsigned int FFXI::CTk::CTkMsgWinData::fsGetStrWidth(char* a1)
{
	unsigned int width{};
	unsigned int pos{};

	while (a1[pos] != 0) {
		switch (a1[pos]) {
		case 0x7Fi8:
			switch (a1[pos + 1])
			{
			case 0xFBi8:
			case 0xFCi8:
			case 0x31i8:
			case 0x32i8:
			case 0x33i8:
			case 0x37i8:
				pos += 2;
				break;
			case 0x34i8:
			case 0x35i8:
			case 0x36i8:
				pos += 3;
				break;
			case 0x38i8:
				pos += 4;
				break;
			default:
				pos += 1;
				break;
			}
			break;
		case 0x1Ei8:
			pos += 2;
			break;
		case 0x1Fi8:
			pos += 2;
			break;
		default:
			pos += Util::Text::GetCodeLen(a1[pos]);
			break;
		}

		width += 1;
	}

	return width;
}

void FFXI::CTk::CTkMsgWinData::ClearCurrentColor()
{
	lg_currentcolor = 0;
}

unsigned int FFXI::CTk::CTkMsgWinData::GetColorData(short a1)
{
	const int array_size = sizeof(colorTable) / sizeof(colorTable[0]);
	for (int i = 0; i < array_size; ++i) {
		ColorTableEntry* entry = colorTable + i;
		if (entry->ID == 0) {
			return 0x80808080;
		}

		if (entry->ID == a1) {
			return entry->Color;
		}
	}

	return 0x80808080;
}

unsigned char FFXI::CTk::CTkMsgWinData::GetTextColorNo(int a1)
{
	switch (a1) {
	case 0x1B:
		return 0x54;
	case 0x1C:
		return 0x55;
	case 0x1D:
		return 0x56;
	case 0x1E:
		return 0x57;
	case 0x1F:
		return 0x58;
	case 0x20:
		return 0x59;
	case 0x21:
		return 0x5A;
	case 0x22:
		return 0x5B;
	case 0x23:
		return 0x5C;
	case 0x24:
		return 0x5D;
	case 0x25:
		return 0x5E;
	case 0x26:
		return 0x5F;
	case 0x27:
		return 0x60;
	case 0x28:
		return 0x61;
	case 0x29:
		return 0x62;
	case 0x2A:
		return 0x63;
	case 0x2B:
		return 0x64;
	case 0x2C:
		return 0x65;
	case 0x2D:
		return 0x66;
	case 0x2E:
		return 0x67;
	case 0x2F:
		return 0x68;
	case 0x30:
		return 0x69;
	case 0x31:
		return 0x6A;
	case 0x9B:
		return 0x6C;
	case 0xB0:
		return 0x6D;
	case 0xB3:
		return 0x6E;
	default:
		return 1;
	}
}

unsigned int FFXI::CTk::CTkMsgWinData::GetLineColorData(short a1)
{
	MsgKindTableEntry* entry = GetLineKindData(a1);
	if (entry == nullptr) {
		return 0x80808080;
	}
	
	return GetColorData(entry->field_2);
}

FFXI::CTk::MsgKindTableEntry* FFXI::CTk::CTkMsgWinData::GetLineKindData(short a2)
{
	const int array_size = sizeof(msgKindTable) / sizeof(msgKindTable[0]);
	for (int i = 0; i < array_size; ++i) {
		MsgKindTableEntry* entry = msgKindTable + i;
		if (entry->ID == 0) {
			return nullptr;
		}
		
		if (entry->ID == a2) {
			return entry;
		}
	}
	return nullptr;
}

bool FFXI::CTk::CTkMsgWinData::IsInMsgKindTable(unsigned char a1)
{
	const int array_size = sizeof(msgKindTable) / sizeof(msgKindTable[0]);
	for (int i = 0; i < array_size; ++i) {
		MsgKindTableEntry* entry = msgKindTable + i;
		if (entry->ID == 0) {
			return false;
		}

		if (entry->ID == a1) {
			if (entry->field_3 == 1) {
				return true;
			}

			return false;
		}
	}

	return false;
}

unsigned int FFXI::CTk::CTkMsgWinData::SplitParse(char* a1, short* a2, unsigned int a3, char* a4, unsigned int a5)
{
	if (a2 == nullptr || a3 == 0) {
		return 0;
	}

	int a1pos = 0;
	int a2pos = 0;
	int a4pos = 0;
	bool v8{ false };
	unsigned int i{};
	for (i = 0; i < a3 - 1; ++i) {
		if (a1[a1pos] == 0) {
			break;
		}
		switch (a1[a1pos]) {
		case 0x7Fi8:
		{
			a2[a2pos] = -(unsigned char)a1[a1pos + 1];
			switch (a1[a1pos + 1]) {
			case 0xFBi8:
				a1pos += 2;
				v8 = false;
				break;
			case 0xFCi8:
				a1pos += 2;
				v8 = true;
				break;
			case 0x31i8:
			case 0x32i8:
			case 0x33i8:
			case 0x37i8:
				a1pos += 2;
				break;
			case 0x34i8:
			case 0x35i8:
			case 0x36i8:
				a1pos += 3;
				break;
			case 0x38i8:
				a1pos += 4;
				break;
			default:
				a1pos += 1;
				break;
			}
		}
			break;
		case 0x1Ei8:
			a2[a2pos] = -256i16 - (short)(unsigned char)a1[a1pos + 1];
			a1pos += 2;
			break;
		case 0x1Fi8:
			a2[a2pos] = -512i16 - (short)(unsigned char)a1[a1pos + 1];
			a1pos += 2;
			break;
		default:
			int codelen = Util::Text::GetCodeLen(a1[a1pos]);
			if (codelen == 1) {
				a2[a2pos] = a1[a1pos] - 32;
			}
			else {
				a2[a2pos] = (short)Text::TextRenderer::SJis2Sq(a1 + a1pos);
			}
			if (v8 == true) {
				if (a4 != nullptr && a4pos < a5) {
					a4[a4pos] = a1[a1pos];
					a4pos += 1;
				}
			}

			a1pos += codelen;
			break;
		}

		a2pos += 1;
	}

	if (a4 != nullptr) {
		a4[0] = 0;
	}

	a2[a2pos] = 0;
	return i;
}

void FFXI::CTk::CTkMsgWinData::InitAll()
{
	this->Init();
	this->field_0 = 0;
	this->field_4 = 0;
	this->field_8 = 0;
	this->field_C = 0;
	this->field_10 = 0;
	this->field_64020 = 0;
	this->field_64024 = 0;
	this->field_64048 = 0;
	this->field_6404A = CTkMsgWinData::fsGetFontWidth(CTkMsgWinData::SomeValues);
	this->field_6404C = CTkMsgWinData::fsGetStrWidth(CTkMsgWinData::SomeValues);
}

void FFXI::CTk::CTkMsgWinData::Init()
{
	memset(this->field_20, 0, sizeof(this->field_20));
	this->field_14 = 0;
	this->field_16 = 0;
	this->field_1A = 0;
	this->field_18 = 8;
}

short* FFXI::CTk::CTkMsgWinData::GetPrevString(short* a2, TK_LOGDATAHEADER** a3)
{
	short index = *a2;
	if (index == -1) {
		return nullptr;
	}

	*a3 = &this->field_20[index].data.header;

	if (index == this->field_16) {
		*a2 = -1;
	}
	else {	
		index -= 1;
		if (index < 0) {
			index = 99;
		}
		*a2 = index;
	}

	return (short*)this->field_20[index].data.field_3C;
}

bool FFXI::CTk::CTkMsgWinData::AddString(char* a2, TK_DATAHEADER* a3, char a4, char a5, CTkDrawMessageWindow* a6)
{	
	if (a3->field_0 >= 200u && a3->field_0 <= 201u) {
		//todo
		//sub_101C94
		exit(0x101243AF);
	}

	if (this->field_64048 == 1) {
		return true;
	}

	ClearCurrentColor();

	if (a2 == nullptr) {
		return false;
	}

	TK_LOGDATAHEADER v23{};
	this->logDataHeaderCalc(a2, &v23, a3);
	//TODO
	//GetCallNum(a2,a3);
	char string[0x800];
	if (a5 == 0) {
		strncpy_s(string, a2, 0x800);
	}
	else {
		Input::FsTextInput::g_fsTextInput->convertToString(string, sizeof(string), a2, strlen(a2), true, false);
	}

	if (v23.field_0.field_0 >= 0x96u && v23.field_0.field_0 <= 0x97u) {
		//ctkeventctrl
		exit(0x101244C1);
		return true;
	}

	if ((v23.field_0.field_1 & 0x3F) == 0) {
		Text::TextRenderer::FrenchCopy(string, strlen(string), sizeof(string));
	}

	if (v23.field_12 == 2) {
		TK_LOGDATAHEADER v20{ v23 };
		PrimMng::g_pTkSpoolMsg->CreateSpoolMsg(string, v20);
		PrimMng::g_pTkSpoolMsg2->CreateSpoolMsg(string, v20);
	}

	bool v17 = IsInMsgKindTable(v23.field_0.field_0);
	if (a6 == nullptr || a6 == PrimMng::g_pTkDrawMessageWindow) {
		this->AddLogString(PrimMng::g_pTkDrawMessageWindow, string, &v23, a4, v17);
		this->field_C += 1;
	}
	if (a6 == nullptr || a6 == PrimMng::g_pTkDrawMessageWindow2) {
		TkManager::MsgWinData2->AddLogString(PrimMng::g_pTkDrawMessageWindow2, string, &v23, a4, v17);
		TkManager::MsgWinData2->field_C += 1;
	}

	return true;
}

void FFXI::CTk::CTkMsgWinData::AddLogString(CTkDrawMessageWindow* a2, char* a3, TK_LOGDATAHEADER* a4, char a5, bool a6)
{
	char* v33 = a3;
	char* time_buff{};
	if (a2->field_56 != 0 && a4->field_38 != 0) {
		size_t len = strlen(a3) + 64u;
		time_buff = new char[len];
		v33 = time_buff;
		if (v33 != nullptr) {
			char time_color[3]{ 0x1E, GetTextColorNo(0x31), 0x00 };
			char text_color[3]{ 0x1E, 0x01, 0x00 };
			const char* time = "TODO";
			sprintf_s(v33, len, "%s[%s]%s %s", time_color, time, text_color, a3);
		}
	}

	size_t len = strlen(v33) + 1u;
	char* v38 = new char[len];
	unsigned int v31{}, v32{};
	CTkAddMsgHelper::GetLen(&v32, &v31, v33);

	size_t len2 = v31 + 1;
	char* v12 = new char[len2];
	
	CTkAddMsgHelper::s1* v29{};
	if (v32 != 0) {
		v29 = new CTkAddMsgHelper::s1[v32];
	}
	
	CTkAddMsgHelper::Fill(v29, v12, len2, v33);
	unsigned int v11{};
	TK_LOGDATA v40;
	CTkAddMsgHelper v39{};
	v39.Init(v33, a6, a5, nullptr);
	while (v39.IsFinished() == false) {
		v40.field_168[v11] = v39.CreateMsg(LogStringBuffer[v11], sizeof(LogStringBuffer[0]) - 1u, a2->field_32);
		if (v40.field_168[v11] != 0) {
			v40.field_3C[v11] = v39.field_8[1];
			v11 += 1;
		}
	}

	int v37 = a2->MsgWinVirt6();
	this->AddLogModeCnt(a4->field_0.field_0, v11 & 0xFFFF);
	
	for (unsigned int i = 0; i < v11; ++i) {
		char* buff = LogStringBuffer[i];
		unsigned int v16 = CTkMsgWinData::fsGetStrWidth(buff) + 1;
		bool v32{ false };

		TK_LOGDATA* v18{ &v40 };
		if (a2->IsDrawWindow() == true) {
			TK_LOGDATAHEADER v25{ *a4 };
			//sub
			v18 = &this->GetLogEntry(v16)->data;
			SplitParse(buff, (short*)v18->field_3C, v16, nullptr, 0);
			unsigned int index = 2 * (v16 - 1);
			v18->field_3C[index] = 0;
			v18->field_3C[index + 1] = 0;
			v32 = true;
		}

		memcpy(&v18->header, a4, sizeof(v18->header));
		if (i != 0) {
			v18->header.field_38 = 0;
			v18->header.field_15 &= 0xB0;
		}

		v18->header.field_10 = v16;
		v18->header.field_C = this->field_10;
		this->field_10 += 1;
		//fsbufferedlog::addstring
		a2->AddLine(v32);
	}
	delete[] v12;
	delete[] v29;
	delete[] v38;
	delete[] time_buff;
}

void FFXI::CTk::CTkMsgWinData::logDataHeaderCalc(char* a2, TK_LOGDATAHEADER* a3, TK_DATAHEADER* a4)
{
	a3->field_8 = this->field_C;
	a3->field_0.field_0 = a4->field_0;
	a3->field_0.field_1 = a4->field_1;
	a3->field_0.field_2 = a4->field_2;
	a3->field_13 = 1;
	a3->field_38 = 0;
	a3->field_15 = 0;

	bool v7{ false };
	switch (a3->field_0.field_0) {
	case 1u:
		[[fallthrough]];
	case 2u:
	case 3u:
	case 4u:
	case 5u:
	case 6u:
	case 7u:
	case 206u:
	case 211u:
	case 213u:
		v7 = true;
		break;
	default:
		if ((a3->field_0.field_1 & 0x40) != 0) {
			v7 = true;
		}
		break;
	}

	MsgKindTableEntry* entry = GetLineKindData(a4->field_0);
	if (entry == nullptr) {
		a3->field_14 = 0;
		a3->field_4 = GetColorData(0);
		a3->field_12 = 0;
	}
	else {
		a3->field_14 = entry->field_3;
		a3->field_4 = GetColorData(entry->field_2);
		a3->field_12 = 0;

		switch (a3->field_0.field_0) {
		case 1:
		case 9:
			if ((a3->field_0.field_1 & 0x3F) == 0) {
				a3->field_38 = CYyDb::sqGetCalenderTime();
				if (v7 == false) {
					a3->field_15 = a3->field_15 & 0xF0 | 1;
				}
			}
			if ((this->field_64044 & 1) != 0) {
				a3->field_12 = 2;
			}
			break;
		case 2:
		case 3:
		case 10:
		case 11:
			if ((a3->field_0.field_1 & 0x3F) == 0) {
				a3->field_38 = CYyDb::sqGetCalenderTime();
				if (v7 == false) {
					a3->field_15 = a3->field_15 & 0xF0 | 1;
				}
			}
			if ((this->field_64044 & 2) != 0) {
				a3->field_12 = 2;
			}
			break;
		case 4:
		case 12:
			if ((a3->field_0.field_1 & 0x3F) == 0) {
				a3->field_38 = CYyDb::sqGetCalenderTime();
				if (v7 == false) {
					a3->field_15 = a3->field_15 & 0xF0 | 1;
				}
			}
			if ((this->field_64044 & 4) != 0) {
				a3->field_12 = 2;
			}
			break;
		case 5:
		case 13:
			if ((a3->field_0.field_1 & 0x3F) == 0) {
				a3->field_38 = CYyDb::sqGetCalenderTime();
				if (v7 == false) {
					a3->field_15 = a3->field_15 & 0xF0 | 1;
				}
			}
			if ((this->field_64044 & 8) != 0) {
				a3->field_12 = 2;
			}
			break;
		case 6:
		case 14:
			if ((a3->field_0.field_1 & 0x3F) == 0) {
				a3->field_38 = CYyDb::sqGetCalenderTime();
				if (v7 == false) {
					a3->field_15 = a3->field_15 & 0xF0 | 1;
				}
			}
			if ((this->field_64044 & 0x10) != 0) {
				a3->field_12 = 2;
			}
			break;
		case 7:
		case 15:
			if ((a3->field_0.field_1 & 0x3F) == 0) {
				a3->field_38 = CYyDb::sqGetCalenderTime();
				if (v7 == false) {
					a3->field_15 = a3->field_15 & 0xF0 | 1;
				}
			}
			if ((this->field_64044 & 0x20) != 0) {
				a3->field_12 = 2;
			}
			break;
		case 135:
			if (v7 == false) {
				a3->field_38 = CYyDb::sqGetCalenderTime();
				a3->field_15 = a3->field_15 & 0xF0 | 1;
			}
			break;
		case 152:
		case 190:
			a3->field_12 = 1;
			break;
		case 206:
			if ((a3->field_0.field_1 & 0x3F) == 0) {
				a3->field_38 = CYyDb::sqGetCalenderTime();
				if (v7 == false) {
					a3->field_15 = a3->field_15 & 0xF0 | 1;
				}
			}
			break;
		case 208:
			if ((a3->field_0.field_1 & 0x3F) == 0) {
				a3->field_38 = CYyDb::sqGetCalenderTime();
				if (v7 == false) {
					a3->field_15 = a3->field_15 & 0xF0 | 1;
				}
			}
			if ((this->field_64044 & 0x20) != 0) {
				a3->field_12 = 2;
			}
			break;
		case 210:
			if ((this->field_64044 & 0x40) != 0) {
				a3->field_12 = 2;
			}
			break;
		case 211:
		case 212:
			if ((a3->field_0.field_1 & 0x3F) == 0) {
				a3->field_38 = CYyDb::sqGetCalenderTime();
				if (v7 == false) {
					a3->field_15 = a3->field_15 & 0xF0 | 1;
				}
			}
			if ((this->field_64044 & 0x80) != 0) {
				a3->field_12 = 2;
			}
			break;
		case 213:
		case 214:
			if ((a3->field_0.field_1 & 0x3F) == 0) {
				a3->field_38 = CYyDb::sqGetCalenderTime();
				if (v7 == false) {
					a3->field_15 = a3->field_15 & 0xF0 | 1;
				}
			}
			if ((this->field_64044 & 0x100) != 0) {
				a3->field_12 = 2;
			}
			break;
		default:
			break;
		}
	}

	short v11[128];
	char v10[32];
	SplitParse(a2, v11, sizeof(v11), v10, sizeof(v10));
	memcpy(a3->field_18, v10, sizeof(a3->field_18));
	a3->field_18[31] = 0;
}

short FFXI::CTk::CTkMsgWinData::GetTailBufferLineNo()
{
	return this->field_14;
}

short FFXI::CTk::CTkMsgWinData::GetNextLineNo(short a2)
{
	if (a2 == -1) {
		return -1;
	}

	short result = a2 + 1;
	if (result >= 100) {
		result = 0;
	}

	return result;
}

void FFXI::CTk::CTkMsgWinData::RemoveEventFlg(int a2)
{
	TK_LOGDATAHEADER* v5{};

	short v3 = this->GetTailBufferLineNo();
	while (v3 != -1) {
		this->GetPrevString(&v3, &v5);
		if (a2 <= v5->field_C) {
			v5->field_12 = 0;
		}
	}
}

void FFXI::CTk::CTkMsgWinData::AddLogModeCnt(unsigned char a2, unsigned short a3)
{
	switch (a2) {
	case 1ui8:
		[[fallthrough]];
	case 9ui8:
		this->field_64020 += (unsigned char)(a3 & 0xFF);
		if (this->field_64020 > 0x64ui8) {
			this->field_64020 = 0x64ui8;
		}
		this->field_64030 += a3;
		break;
	case 2ui8:
		[[fallthrough]];
	case 3ui8:
		[[fallthrough]];
	case 10ui8:
		[[fallthrough]];
	case 11ui8:
		this->field_64021 += (unsigned char)(a3 & 0xFF);
		if (this->field_64021 > 0x64ui8) {
			this->field_64021 = 0x64ui8;
		}
		this->field_64034 += a3;
		break;
	case 4ui8:
		[[fallthrough]];
	case 12ui8:
		this->field_64022 += (unsigned char)(a3 & 0xFF);
		if (this->field_64022 > 0x64ui8) {
			this->field_64022 = 0x64ui8;
		}
		this->field_64032 += a3;
		break;
	case 5ui8:
		[[fallthrough]];
	case 13ui8:
		[[fallthrough]];
	case 0xD2ui8:
		this->field_64023 += (unsigned char)(a3 & 0xFF);
		if (this->field_64023 > 0x64ui8) {
			this->field_64023 = 0x64ui8;
		}
		this->field_6402C += a3;
		break;
	case 6ui8:
		[[fallthrough]];
	case 14ui8:
		this->field_64024 += (unsigned char)(a3 & 0xFF);
		if (this->field_64024 > 0x64ui8) {
			this->field_64024 = 0x64ui8;
		}
		this->field_6402E += a3;
		break;
	case 0xD3ui8:
		[[fallthrough]];
	case 0xD4ui8:
		this->field_64027 += (unsigned char)(a3 & 0xFF);
		if (this->field_64027 > 0x64ui8) {
			this->field_64027 = 0x64ui8;
		}
		this->field_64036 += a3;
		break;
	case 0xD5ui8:
		[[fallthrough]];
	case 0xD6ui8:
		this->field_64025 += (unsigned char)(a3 & 0xFF);
		if (this->field_64025 > 0x64ui8) {
			this->field_64025 = 0x64ui8;
		}
		this->field_64038 += a3;
		break;
	default:
		break;
	}

	this->field_6402A += a3;
	this->field_6403A += a3;
}

FFXI::CTk::TK_LOGENTRY* FFXI::CTk::CTkMsgWinData::GetLogEntry(unsigned int)
{
	int v2 = this->field_14 + 1;
	if (v2 >= 100) {
		v2 = 0;
	}
	int v3 = v2;
	if (this->field_16 == v2 && this->field_1A > 50) {
		do {
			this->field_1A -= 1;
			v3 += 1;
			if (v3 >= 100) {
				v3 = 0;
			}
		} while (this->field_20[v2].data.header.field_C == this->field_20[v3].data.header.field_C);
		this->field_16 = v3;
		this->field_0 = this->field_20[v3].data.header.field_C;
	}

	this->field_14 = v2;

	if (this->field_1A == 0) {
		this->field_16 = this->field_14;
	}
	this->field_1A += 1;

	memset(this->field_20 + this->field_14, 0, sizeof(this->field_20[v2]));
	return this->field_20 + this->field_14;
}

FFXI::CTk::ColorTableEntry FFXI::CTk::CTkMsgWinData::colorTable[50] =
{
	{ 0x0042, 0x60808080 },
	{ 0x004A, 0x80808080 },
	{ 0x004B, 0x80A0A0A0 },
	{ 0x0041, 0xFF101010 },
	{ 0x0043, 0x60404040 },
	{ 0x0044, 0x60A02C34 },
	{ 0x004C, 0x30FF2020 },
	{ 0x004D, 0x60A08080 },
	{ 0x0048, 0xA0701090 },
	{ 0x0045, 0x60808010 },
	{ 0x004E, 0x60686840 },
	{ 0x0046, 0x60405080 },
	{ 0x0052, 0x5020C0A0 },
	{ 0x0047, 0x603050F0 },
	{ 0x0049, 0x50A040A0 },
	{ 0x004F, 0x4000C000 },
	{ 0x0053, 0x5010C040 },
	{ 0x0050, 0x4070F070 },
	{ 0x006F, 0x80802060 },
	{ 0x0051, 0xFF5020FF },
	{ 0x0054, 0x60808080 },
	{ 0x0055, 0x60A07060 },
	{ 0x006C, 0x60A06050 },
	{ 0x0056, 0x50A040A0 },
	{ 0x0057, 0x5020C0A0 },
	{ 0x0058, 0x5050FF60 },
	{ 0x0059, 0x606050A0 },
	{ 0x005A, 0x50A0D0D0 },
	{ 0x005B, 0x60808080 },
	{ 0x005C, 0x606090C0 },
	{ 0x005D, 0x70A0302C },
	{ 0x005E, 0x60808080 },
	{ 0x005F, 0x60808080 },
	{ 0x0060, 0x60808050 },
	{ 0x0061, 0x60808080 },
	{ 0x0062, 0x5090C0F0 },
	{ 0x0063, 0x58C08080 },
	{ 0x0064, 0x60808080 },
	{ 0x0065, 0x60808080 },
	{ 0x0066, 0x60A08040 },
	{ 0x0067, 0x60707070 },
	{ 0x0068, 0x60808010 },
	{ 0x0069, 0x50C060D0 },
	{ 0x006A, 0x50F0F050 },
	{ 0x006B, 0x50F0F050 },
	{ 0x006C, 0x50F0F050 },
	{ 0x006D, 0x5080571F },
	{ 0x006E, 0x5000CC00 },
	{ 0x006F, 0x5000CC00 },
	{ 0x0000, 0x00000000 }
};

FFXI::CTk::MsgKindTableEntry FFXI::CTk::CTkMsgWinData::msgKindTable[194] = 
{
	{ 0x0001, 0x54, 0x1 },
	{ 0x0002, 0x55, 0x1 },
	{ 0x0003, 0x6C, 0x1 },
	{ 0x0004, 0x56, 0x1 },
	{ 0x0005, 0x57, 0x1 },
	{ 0x0006, 0x58, 0x1 },
	{ 0x0007, 0x59, 0x1 },
	{ 0x0008, 0x69, 0x1 },
	{ 0x0009, 0x54, 0x1 },
	{ 0x000A, 0x55, 0x1 },
	{ 0x000B, 0x6C, 0x1 },
	{ 0x000C, 0x56, 0x1 },
	{ 0x000D, 0x57, 0x1 },
	{ 0x000E, 0x58, 0x1 },
	{ 0x000F, 0x59, 0x1 },
	{ 0x0010, 0x69, 0x1 },
	{ 0x0011, 0x5A, 0x1 },
	{ 0x0012, 0x5A, 0x1 },
	{ 0x0014, 0x63, 0x2 },
	{ 0x0015, 0x67, 0x2 },
	{ 0x001C, 0x5D, 0x2 },
	{ 0x001D, 0x61, 0x2 },
	{ 0x0024, 0x45, 0x2 },
	{ 0x0026, 0x44, 0x2 },
	{ 0x0014, 0x63, 0x2 },
	{ 0x0015, 0x67, 0x2 },
	{ 0x0016, 0x62, 0x2 },
	{ 0x0017, 0x62, 0x2 },
	{ 0x0018, 0x62, 0x2 },
	{ 0x0019, 0x63, 0x2 },
	{ 0x001A, 0x67, 0x2 },
	{ 0x001B, 0x62, 0x2 },
	{ 0x001C, 0x5D, 0x2 },
	{ 0x001D, 0x61, 0x2 },
	{ 0x001E, 0x5C, 0x2 },
	{ 0x001F, 0x5C, 0x2 },
	{ 0x0020, 0x63, 0x2 },
	{ 0x0021, 0x67, 0x2 },
	{ 0x0022, 0x62, 0x2 },
	{ 0x0023, 0x62, 0x2 },
	{ 0x0024, 0x45, 0x2 },
	{ 0x0025, 0x4E, 0x2 },
	{ 0x0026, 0x44, 0x2 },
	{ 0x0027, 0x4C, 0x2 },
	{ 0x0028, 0x63, 0x2 },
	{ 0x0029, 0x67, 0x2 },
	{ 0x002A, 0x62, 0x2 },
	{ 0x002B, 0x62, 0x2 },
	{ 0x002C, 0x42, 0x2 },
	{ 0x0032, 0x68, 0x2 },
	{ 0x0033, 0x68, 0x2 },
	{ 0x0034, 0x68, 0x2 },
	{ 0x0035, 0x4E, 0x2 },
	{ 0x0036, 0x42, 0x2 },
	{ 0x0037, 0x68, 0x2 },
	{ 0x0038, 0x5E, 0x2 },
	{ 0x0039, 0x5F, 0x2 },
	{ 0x003A, 0x68, 0x2 },
	{ 0x003B, 0x60, 0x2 },
	{ 0x003C, 0x64, 0x2 },
	{ 0x003D, 0x65, 0x2 },
	{ 0x003E, 0x68, 0x2 },
	{ 0x003F, 0x66, 0x2 },
	{ 0x0040, 0x64, 0x2 },
	{ 0x0041, 0x65, 0x2 },
	{ 0x0042, 0x68, 0x2 },
	{ 0x0043, 0x66, 0x2 },
	{ 0x0044, 0x66, 0x2 },
	{ 0x0045, 0x66, 0x2 },
	{ 0x0046, 0x61, 0x2 },
	{ 0x0047, 0x67, 0x2 },
	{ 0x0048, 0x67, 0x2 },
	{ 0x0050, 0x68, 0x2 },
	{ 0x0051, 0x5E, 0x2 },
	{ 0x0052, 0x5F, 0x2 },
	{ 0x0053, 0x68, 0x2 },
	{ 0x0054, 0x61, 0x2 },
	{ 0x0055, 0x68, 0x2 },
	{ 0x0056, 0x64, 0x2 },
	{ 0x0057, 0x65, 0x2 },
	{ 0x0058, 0x68, 0x2 },
	{ 0x0059, 0x67, 0x2 },
	{ 0x005A, 0x68, 0x2 },
	{ 0x005B, 0x64, 0x2 },
	{ 0x005C, 0x65, 0x2 },
	{ 0x005D, 0x68, 0x2 },
	{ 0x005E, 0x67, 0x2 },
	{ 0x0064, 0x68, 0x2 },
	{ 0x0065, 0x5E, 0x2 },
	{ 0x0066, 0x5F, 0x2 },
	{ 0x0067, 0x68, 0x2 },
	{ 0x0068, 0x61, 0x2 },
	{ 0x0069, 0x68, 0x2 },
	{ 0x006A, 0x64, 0x2 },
	{ 0x006B, 0x65, 0x2 },
	{ 0x006C, 0x68, 0x2 },
	{ 0x006D, 0x67, 0x2 },
	{ 0x006E, 0x68, 0x2 },
	{ 0x006F, 0x64, 0x2 },
	{ 0x0070, 0x65, 0x2 },
	{ 0x0071, 0x68, 0x2 },
	{ 0x0072, 0x67, 0x2 },
	{ 0x00BF, 0x42, 0x0 },
	{ 0x0079, 0x6A, 0x2 },
	{ 0x007A, 0x68, 0x2 },
	{ 0x007B, 0x44, 0x0 },
	{ 0x007C, 0x44, 0x0 },
	{ 0x007D, 0x44, 0x0 },
	{ 0x007E, 0x44, 0x0 },
	{ 0x007F, 0x6A, 0x2 },
	{ 0x0080, 0x6A, 0x2 },
	{ 0x0081, 0x6A, 0x2 },
	{ 0x0082, 0x6A, 0x2 },
	{ 0x0083, 0x6A, 0x2 },
	{ 0x0084, 0x6A, 0x2 },
	{ 0x0085, 0x6A, 0x2 },
	{ 0x0086, 0x6A, 0x2 },
	{ 0x0087, 0x6A, 0x2 },
	{ 0x0088, 0x6A, 0x2 },
	{ 0x0089, 0x6A, 0x2 },
	{ 0x008A, 0x6A, 0x2 },
	{ 0x008B, 0x6A, 0x2 },
	{ 0x008C, 0x6A, 0x2 },
	{ 0x008D, 0x45, 0x2 },
	{ 0x008E, 0x5B, 0x1 },
	{ 0x008F, 0x5B, 0x1 },
	{ 0x0090, 0x5B, 0x1 },
	{ 0x0091, 0x5B, 0x1 },
	{ 0x0092, 0x5B, 0x0 },
	{ 0x0093, 0x5B, 0x0 },
	{ 0x0094, 0x5B, 0x0 },
	{ 0x0095, 0x5B, 0x0 },
	{ 0x0096, 0x5B, 0x1 },
	{ 0x0097, 0x5B, 0x0 },
	{ 0x0098, 0x5B, 0x1 },
	{ 0x00BE, 0x5B, 0x0 },
	{ 0x0099, 0x5B, 0x0 },
	{ 0x009A, 0x45, 0x0 },
	{ 0x009B, 0x45, 0x0 },
	{ 0x009C, 0x45, 0x0 },
	{ 0x009D, 0x42, 0x0 },
	{ 0x009E, 0x4F, 0x0 },
	{ 0x009F, 0x45, 0x0 },
	{ 0x00A0, 0x43, 0x0 },
	{ 0x00A1, 0x43, 0x0 },
	{ 0x00A2, 0x62, 0x2 },
	{ 0x00A3, 0x63, 0x2 },
	{ 0x00A4, 0x67, 0x2 },
	{ 0x00A5, 0x62, 0x2 },
	{ 0x00A6, 0x4E, 0x2 },
	{ 0x00A7, 0x4C, 0x2 },
	{ 0x00A8, 0x68, 0x2 },
	{ 0x00A9, 0x68, 0x2 },
	{ 0x00AA, 0x66, 0x2 },
	{ 0x00AB, 0x68, 0x2 },
	{ 0x00AC, 0x68, 0x2 },
	{ 0x00AD, 0x67, 0x2 },
	{ 0x00AE, 0x65, 0x2 },
	{ 0x00AF, 0x64, 0x2 },
	{ 0x00B0, 0x65, 0x2 },
	{ 0x00B1, 0x68, 0x2 },
	{ 0x00B2, 0x64, 0x2 },
	{ 0x00B3, 0x68, 0x2 },
	{ 0x00B4, 0x68, 0x2 },
	{ 0x00B5, 0x67, 0x2 },
	{ 0x00B6, 0x65, 0x2 },
	{ 0x00B7, 0x64, 0x2 },
	{ 0x00B8, 0x67, 0x2 },
	{ 0x00B9, 0x63, 0x2 },
	{ 0x00BA, 0x67, 0x2 },
	{ 0x00BB, 0x62, 0x2 },
	{ 0x00BC, 0x62, 0x2 },
	{ 0x00BD, 0x66, 0x2 },
	{ 0x00C8, 0x51, 0x0 },
	{ 0x00C9, 0x51, 0x0 },
	{ 0x00C8, 0x51, 0x0 },
	{ 0x00C9, 0x51, 0x0 },
	{ 0x00CC, 0x4F, 0x0 },
	{ 0x00CD, 0x58, 0x0 },
	{ 0x00CE, 0x6B, 0x0 },
	{ 0x00CF, 0x46, 0x0 },
	{ 0x00D0, 0x59, 0x0 },
	{ 0x00D1, 0x51, 0x0 },
	{ 0x00D2, 0x57, 0x1 },
	{ 0x00D3, 0x6D, 0x1 },
	{ 0x00D4, 0x6D, 0x1 },
	{ 0x00D5, 0x6E, 0x1 },
	{ 0x00D6, 0x6E, 0x1 },
	{ 0x00D7, 0x6F, 0x1 },
	{ 0x00D8, 0x6F, 0x1 },
	{ 0x00D9, 0x6E, 0x0 },
	{ 0x00DA, 0x6F, 0x0 },
	{ 0x0000, 0x00, 0x0 },
	{ 0x0000, 0x00, 0x0 }
};

char FFXI::CTk::CTkMsgWinData::SomeValues[8] = {
	0x81i8, 0x40i8, 0, 0, 0, 0, 0, 0
};

unsigned int FFXI::CTk::CTkMsgWinData::SomeColors[18] = {
	0, 0, 0x4040C000, 0x604040A0, 0x604040A0, 0xFFA02080, 0x60108080, 0x60C09060, 0x80804020, 0, 0, 0, 0, 0, 0, 0, 0, 0
};
