#pragma once
#include "CTkSpoolMsgBase.h"

namespace FFXI {
	namespace CTk {
		class CTkSpoolMsgType1 : public CTkSpoolMsgBase
		{
		public:
			virtual void CreateSpoolMsg(char*, TK_LOGDATAHEADER) override final;
			virtual void DestroyWind() override final;
			CTkSpoolMsgType1(CTkDrawMessageWindow*);
		};
	}
}