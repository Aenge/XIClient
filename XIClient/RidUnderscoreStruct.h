#pragma once
#include "WMatrix.h"
namespace FFXI {
	namespace CYy {
		struct RidUnderscoreStruct {
			int field_0;
			FFXI::Math::WMatrix* field_4;
			D3DXVECTOR3* field_8;
			int field_C;
			float field_10;
			float field_14;
			FFXI::Math::WMatrix translation;
			FFXI::Math::WMatrix inverseTranslation;
			D3DXVECTOR3 field_98;
			D3DXVECTOR3 field_A4;
			D3DXVECTOR3 field_B0;
			int field_BC;
			int field_C0;
			int field_C4;
			int field_C8;
			float field_CC;
			int field_D0;
			int field_D4;
			D3DXVECTOR3* field_D8;
			D3DXVECTOR3* field_DC;
			unsigned short* field_E0;
			int field_E4;
			D3DXVECTOR3 field_E8[8];
			D3DXVECTOR3 field_148[6];
			unsigned short field_190[48];
		};
	}
}