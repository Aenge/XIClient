#include "CMoProcessor.h"
using namespace FFXI::CYy;

const BaseGameObject::ClassInfo CMoProcessor::CMoProcessorClass{
	"CMoProcessor", sizeof(CMoProcessor), &BaseGameObject::BaseGameObjectClass
};

const BaseGameObject::ClassInfo* FFXI::CYy::CMoProcessor::GetRuntimeClass()
{
	return &CMoProcessorClass;
}
