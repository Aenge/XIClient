#include "CTkSpoolMsgBase.h"
#include "CTkMenuCtrlData.h"
#include "PrimMng.h"
#include "CTkDrawMessageWindowOne.h"
#include "TkManager.h"
#include "CTkMenuMng.h"

void FFXI::CTk::CTkMsgBase::OnInitialUpdatePrimitive()
{
	this->MenuCtrlData->GetOriginalRect(&this->field_38);
	this->MenuCtrlData->ResizeWindow(
		this->field_38.Left, 
		this->field_38.Top,
		PrimMng::g_pTkDrawMessageWindow->field_32 + 14,
		this->field_38.Bottom - this->field_38.Top,
		1, 0, 0);
	this->field_54 = 0;
	if (this->field_48 != 0) {
		tagPOINT v5{};
		v5.x = (LONG)this->field_4A;
		v5.y = (LONG)this->field_4C;
		TkManager::g_CTkMenuMng.GetSomePosition(&v5, 0, false);
		this->MenuCtrlData->RePosition(v5.x, v5.y);
		this->MenuCtrlData->field_86 = 0;
		this->MenuCtrlData->field_84 = 0;
	}

	this->DisableDraw();
	this->MenuCtrlData->GetWindowLocate(&this->field_30);
}

void FFXI::CTk::CTkMsgBase::OnDestroyPrimitive()
{
	this->field_60 = 0;
}

void FFXI::CTk::CTkMsgBase::UpdateData()
{
	//nullsub
}

FFXI::CTk::CTkMsgBase::CTkMsgBase(CTkDrawMessageWindow* a2)
{
	this->field_64 = a2;
}

void FFXI::CTk::CTkMsgBase::EnableDraw()
{
	if (this->MenuCtrlData == nullptr) {
		return;
	}

	if (this->field_48 == 0) {
		if (this->field_64 != nullptr) {
			if (this->field_64->MenuCtrlData != nullptr) {
				this->MenuCtrlData->RePosition(
					this->field_64->MenuCtrlData->field_3A.Left,
					this->field_64->MenuCtrlData->field_3A.Top
				);
			}
		}
	}

	if (this->MenuCtrlData->field_6A == 0) {
		if (this->field_64 != nullptr) {
			this->field_64->field_18->RemoveEventFlg(this->field_50);
			if (this->field_48 == 0) {
				if (this->field_64->MenuCtrlData != nullptr) {
					this->MenuCtrlData->EnableDrawButton(1);
					this->MenuCtrlData->field_62 = 1;
					this->MenuCtrlData->field_69 = 1;
					this->MenuCtrlData->field_6A = 1;
					TKRECT v7{};
					this->field_64->MenuCtrlData->GetWindowLocate(&v7);
					this->MenuCtrlData->ResizeWindow(v7.Left, v7.Top, v7.Right - v7.Left, 16 * this->field_4E + 4, 1, 0, 0);
				}
			}
		}
	}

	this->MenuCtrlData->GetWindowLocate(&this->field_30);
	TkManager::g_CTkMenuMng.ActiveDrawMenu(this->MenuCtrlData, 0);

	this->field_5C = this->field_4E;
	if (this->field_5C > 4) {
		this->field_5C = 4;
	}

	this->field_58 = this->field_14.GetHeadPosition();
}

void FFXI::CTk::CTkMsgBase::DisableDraw()
{
	//client doesn't check for menuctrldata nullptr
	if (this->MenuCtrlData->field_6A == 0) {
		return;
	}

	this->MenuCtrlData->DisableDrawButton(1, false);
	this->MenuCtrlData->field_62 = 0;
	this->MenuCtrlData->field_69 = 0;

	if (this->field_48 == 0) {
		this->MenuCtrlData->field_6A = 0;
	}
}

void FFXI::CTk::CTkMsgBase::RemoveAllMsg()
{
	this->field_64->field_18->RemoveEventFlg(this->field_50);

	CTkNode* node = this->field_14.GetHeadPosition();
	while (node != nullptr) {
		CTkNode* nodecpy = node;
		CTkObject* obj = this->field_14.GetNext(&node);
		this->field_14.RemoveAt(nodecpy);
		if (obj != nullptr) {
			delete obj;
		}
	}

	this->field_14.RemoveAll();
}

void FFXI::CTk::CTkMsgBase::PresetEventMessageMode(char a2, short a3, short a4)
{
	this->field_48 = a2;
	this->field_4A = a3;
	if (this->field_4A < 0) {
		this->field_4A = -176 - a3 * (TkManager::g_CTkMenuMng.UIXRes - TkManager::g_CTkMenuMng.field_7C) / 100;
	}

	this->field_4C = a4;
	if (this->field_4C < 0) {
		this->field_4C = -32 - a4 * (TkManager::g_CTkMenuMng.UIYRes - TkManager::g_CTkMenuMng.field_7E) / 100;
	}
}

void FFXI::CTk::CTkMsgBase::SpoolMsg(char* a2, char a3, char a4)
{
	exit(0x10132195);
}
