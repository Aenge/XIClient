#pragma once
#include "CMoTask.h"
namespace FFXI {
	namespace CYy {
		class CXiActorDraw : public CMoTask {
		public:
			static const BaseGameObject::ClassInfo CXiActorDrawClass;
			virtual const BaseGameObject::ClassInfo* GetRuntimeClass() override;
			virtual char OnMove() override final;
		};
	}
}