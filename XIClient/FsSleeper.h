#pragma once
#define WIN32_LEAN_AND_MEAN
#include <wtypes.h>
namespace FFXI {
	namespace File {
		class FsSleeper {
		public:
			virtual ~FsSleeper();
			FsSleeper();
			int sleep(DWORD);
			int alarm();
			HANDLE Handle;
		};
	}
}