#include "ShadowRenderer.h"
#include "Globals.h"
#include "CDx.h"
#include "CYyDb.h"
#include "CYyModel.h"
#include "CYyTex.h"
#include "CTsZoneMap.h"

using namespace FFXI::CYy;

float ShadowRenderer::g_mss1_float{};
D3DCOLOR ShadowRenderer::s_color{ 0 };
FFXI::CYy::CYyTex* ShadowRenderer::s_texture1{ nullptr };
FFXI::CYy::CYyTex* ShadowRenderer::s_texture2{ nullptr };
IDirect3DTexture8* FFXI::CYy::ShadowRenderer::g_pSomeTexture1{ nullptr };
IDirect3DTexture8* FFXI::CYy::ShadowRenderer::g_pSomeTexture2{ nullptr };

void FFXI::CYy::ShadowRenderer::clean_texs()
{
	if (ShadowRenderer::g_pSomeTexture1 != nullptr) {
		ShadowRenderer::g_pSomeTexture1->Release();
		ShadowRenderer::g_pSomeTexture1 = nullptr;
	}
	if (ShadowRenderer::g_pSomeTexture2 != nullptr) {
		ShadowRenderer::g_pSomeTexture2->Release();
		ShadowRenderer::g_pSomeTexture2 = nullptr;
	}
}

FFXI::CYy::ShadowRenderer::ShadowRenderer()
{
	if (ShadowRenderer::g_pSomeTexture1 == nullptr) {
		ShadowRenderer::g_pSomeTexture1 = FFXI::CYy::CDx::instance->CreateTexture(
			CYyDb::g_pCYyDb->GetBackgroundXRes(),
			CYyDb::g_pCYyDb->GetBackgroundYRes(),
			&ShadowRenderer::g_pSomeTexture2,
			1,
			D3DPOOL_DEFAULT
		);
	}

	if (ShadowRenderer::g_pSomeTexture1 != nullptr) {
		this->field_8 = ShadowRenderer::g_pSomeTexture1;
		this->field_8->AddRef();
	}
	
	this->field_8 = ShadowRenderer::g_pSomeTexture1;

	if (ShadowRenderer::g_pSomeTexture2 != nullptr) {
		this->field_C = ShadowRenderer::g_pSomeTexture2;
		this->field_C->AddRef();
	}
	
	this->field_C = ShadowRenderer::g_pSomeTexture2;

	ShadowRenderer::g_mss1_float = 1.0;
	this->field_14 = { 0.0, 0.0, 0.0 };
	this->field_20 = { 0.0, 0.0, 0.0 };
	this->field_4 = 0;
}

FFXI::CYy::ShadowRenderer::~ShadowRenderer()
{
	if (this->field_8 != nullptr) {
		this->field_8->Release();
		this->field_8 = nullptr;
	}

	if (this->field_C != nullptr) {
		this->field_C->Release();
		this->field_C = nullptr;
	}
}

void FFXI::CYy::ShadowRenderer::Init(D3DXVECTOR3* a2)
{
	if (a2->y > 0.0) {
		a2->x = 0.44999999;
		a2->y = -0.89999998;
		a2->z = 0.0;
	}

	double v9 = CYyDb::g_pCYyDb->CheckTick() * 0.039999999;
	float v33 = v9;
	if (v9 > 1.0)
		v33 = 1.0f;

	
	*a2 -= this->field_20;
	*a2 *= v33;
	this->field_20 += *a2;
	D3DXVECTOR3 a1 = this->field_20;
	Globals::Vec3Normalize(&a1);
	long double mag = sqrt(a1.z * a1.z + a1.x * a1.x);
	float magf = mag;
	if (mag <= 0.000099999997) {
		a1 = { 0.0, -1.0, 0.0 };
	}
	else {
		if (a1.y >= 0.0 
			|| -atan2(a1.y, magf) < FFXI::Constants::Values::ANGLE_PI_OVER_3) {
			a1.y = -(sin(FFXI::Constants::Values::ANGLE_PI_OVER_3) * magf);
		}
		Globals::Vec3Normalize(&a1);
	}

	double v19 = CYyDb::g_pCYyDb->CheckTick() * 0.039999999;
	float v35 = v19;
	if (v19 > 1.0)
		v35 = 1.0f;

	a1 -= this->field_14;
	a1 *= v35;
	this->field_14 += a1;
	if (abs(this->field_14.x) < 0.0099999998) {
		if (abs(this->field_14.z) < 0.0099999998) {
			this->field_14.x = 0.0099999998;
		}
	}
	Globals::Vec3Normalize(&this->field_14);
}

void FFXI::CYy::ShadowRenderer::Render(D3DXVECTOR4* a2)
{
	return;
	if (Globals::g_some_actordraw_short == 0) {
		if (CYyModel::g_some_short != 1) {
			float v8 = ShadowRenderer::g_mss1_float * 0.33333334;
			//sub //TODO is this correct?
			if (Globals::g_some_actordraw_float <= 20.0) {
				exit(0x10033056);
			}
			else {
				exit(0x1003306A);
			}
		}
	}
	else if (Globals::g_some_actordraw_short == 1) {
		if (this->field_4 != 0) {
			if (this->field_8 != nullptr) {
				float v8 = ShadowRenderer::g_mss1_float * 2.4000001;
				CYyDb::g_pTsZoneMap->DrawShadow(a2, &this->field_14, this->field_8, v8);
				this->field_4 = 0;
			}
		}
	}
	else if (Globals::g_some_actordraw_short == 2) {
		if (CYyModel::g_some_short != 1) {
			float v8 = ShadowRenderer::g_mss1_float * 0.33333334;
			exit(0x10032FC6);
		}
	}
}

bool FFXI::CYy::ShadowRenderer::PrepareToRender(CYyTex** a2, D3DCOLOR a3)
{
	FFXI::Math::WMatrix v6{}, pOut{};
	if (Globals::g_some_actordraw_short == 1 && Globals::g_some_actordraw_float < 4.0 && this->field_8 != nullptr) {
		FFXI::CYy::ShadowRenderer::s_texture1 = a2[0];
		FFXI::CYy::ShadowRenderer::s_texture2 = a2[1];
		FFXI::CYy::ShadowRenderer::s_color = a3;

		CDx::instance->AddViewportAtOrigin(this->field_8, this->field_C, NULL);
		if (this->field_4 == 0) {
			CDx::instance->ClearViewport(0, NULL, 1, 0, 0.0, 0);
			this->field_4 = 1;
		}

		CDx::instance->DXDevice->SetRenderState(D3DRS_TEXTUREFACTOR, 0x80FFFFFF);
		CDx::instance->DXDevice->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_SELECTARG2);
		CDx::instance->DXDevice->SetTextureStageState(0, D3DTSS_COLORARG2, D3DTA_TFACTOR);
		CDx::instance->DXDevice->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG2);
		CDx::instance->DXDevice->SetTextureStageState(0, D3DTSS_ALPHAARG2, D3DTA_TFACTOR);
		CDx::instance->DXDevice->SetTextureStageState(1, D3DTSS_COLOROP, D3DTOP_DISABLE);
		CDx::instance->DXDevice->SetTextureStageState(1, D3DTSS_ALPHAOP, D3DTOP_DISABLE);
		CDx::instance->DXDevice->SetRenderState(D3DRS_LIGHTING, false);
		CDx::instance->DXDevice->SetRenderState(D3DRS_FOGENABLE, false);
		CDx::instance->DXDevice->SetRenderState(D3DRS_ZENABLE, false);
		CDx::instance->DXDevice->SetRenderState(D3DRS_ZWRITEENABLE, false);
		CDx::instance->DXDevice->SetRenderState(D3DRS_SHADEMODE, D3DSHADE_FLAT);

		D3DXMatrixPerspectiveFovRH(&pOut, 0.19634955, 1.0, 0.1, 65535.0);

		CDx::instance->SetTransform(D3DTS_PROJECTION, &pOut);
		CDx::instance->SetTransform(D3DTS_VIEW, &CYyModel::view_transform);
		CDx::instance->SetTransform(D3DTS_WORLD, &Globals::g_TransformBackup);
		return true;
	}

	return false;
}

bool FFXI::CYy::ShadowRenderer::FinishRender(char type)
{
	if (Globals::g_some_actordraw_short != 1) {
		return false;
	}

	CDx::instance->DXDevice->SetTextureStageState(0, D3DTSS_COLOROP, 5);
	CDx::instance->DXDevice->SetTextureStageState(0, D3DTSS_COLORARG1, 2);
	CDx::instance->DXDevice->SetTextureStageState(0, D3DTSS_ALPHAOP, 5);
	CDx::instance->DXDevice->SetTextureStageState(0, D3DTSS_ALPHAARG1, 2);
	CDx::instance->DXDevice->SetTextureStageState(0, D3DTSS_COLORARG2, 1);

	if (type <= 0 || type > 2) {
		CDx::instance->DXDevice->SetTextureStageState(1, D3DTSS_COLOROP, 5u);
		CDx::instance->DXDevice->SetTextureStageState(1, D3DTSS_COLORARG1, 1);
		CDx::instance->DXDevice->SetTextureStageState(1, D3DTSS_COLORARG2, 3);
		CDx::instance->DXDevice->SetTextureStageState(1, D3DTSS_ALPHAOP, 6);
		CDx::instance->DXDevice->SetTextureStageState(1, D3DTSS_ALPHAARG1, 1);
	}
	else {
		CDx::instance->DXDevice->SetTextureStageState(1, D3DTSS_COLOROP, 0x12u);
		CDx::instance->DXDevice->SetTextureStageState(1, D3DTSS_ALPHAOP, 7);
		CDx::instance->DXDevice->SetTextureStageState(1, D3DTSS_COLORARG1, 1);
		CDx::instance->DXDevice->SetTextureStageState(1, D3DTSS_COLORARG2, 2);
		CDx::instance->DXDevice->SetTextureStageState(1, D3DTSS_ALPHAARG1, 1);
	}

	CDx::instance->DXDevice->SetRenderState(D3DRS_TEXTUREFACTOR, ShadowRenderer::s_color);

	if (Globals::g_some_actordraw_float < 4.0) {
		if (this->field_8 == nullptr) {
			return false;
		}

		CDx::instance->RevertStage();
		CDx::instance->SetTransform(D3DTS_PROJECTION, &CYyModel::temp_proj_mtx);
		CDx::instance->SetTransform(D3DTS_WORLD, &CYyModel::temp_wrld_mtx);
		CDx::instance->SetTransform(D3DTS_VIEW, &CYyModel::temp_view_mtx);
		CDx::instance->DXDevice->SetRenderState(D3DRS_ZENABLE, 1);
		CDx::instance->DXDevice->SetRenderState(D3DRS_ZWRITEENABLE, 1);
		CDx::instance->DXDevice->SetRenderState(D3DRS_SHADEMODE, 2);
		CDx::instance->DXDevice->SetRenderState(D3DRS_FOGENABLE, 1);
		CDx::instance->DXDevice->SetRenderState(D3DRS_LIGHTING, 1);
	}

	if (ShadowRenderer::s_texture1 != nullptr) {
		if (ShadowRenderer::s_texture1->RegTex != nullptr) {
			CDx::instance->DXDevice->SetTexture(0, ShadowRenderer::s_texture1->RegTex);
		}
		else {
			CDx::instance->DXDevice->SetTexture(0, ShadowRenderer::s_texture1->CubeTex);
		}
	}
	else {
		CDx::instance->DXDevice->SetTexture(0, nullptr);
	}

	if (ShadowRenderer::s_texture2 != nullptr) {
		if (ShadowRenderer::s_texture2->RegTex != nullptr) {
			CDx::instance->DXDevice->SetTexture(1, ShadowRenderer::s_texture2->RegTex);
		}
		else {
			CDx::instance->DXDevice->SetTexture(1, ShadowRenderer::s_texture2->CubeTex);
		}
	}
	else {
		CDx::instance->DXDevice->SetTexture(1, nullptr);
	}

	return true;
}
