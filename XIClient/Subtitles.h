#pragma once
#define WIN32_LEAN_AND_MEAN
#include "streams.h"
namespace FFXI {
	namespace CTk { class _49SubList; }
	namespace CYy {
		class CMoResource;
		class ResourceContainer;
		class Subtitles {
		public:
			Subtitles(int);
			virtual ~Subtitles();
			void Init();
			void Update();
			ResourceContainer** field_4;
			CTk::_49SubList*** SubLists;
			int field_C;
			float field_10;
		};
	}
}