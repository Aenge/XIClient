#pragma once
#include "CMoResource.h"
#include "KzFQuat.h"
#include "BoundingBox3D.h"

namespace FFXI {
	namespace CYy {
		class CMoSk2 : public CMoResource {
		private:
			void* GetTransformTable();
			char Data[0x24];
		public:
			struct BoneData; struct BoneTransform;

			CMoSk2();
			virtual ~CMoSk2() = default;
			virtual void Open() override final;
			virtual void Close() override final;

			unsigned char GetVersion();
			unsigned short GetBoneCount();
			BoneData* GetBoneData(unsigned short boneIndex);
			unsigned short GetBoneTransformCount();
			BoneTransform* GetBoneTransform(unsigned short transformId);
			Math::BoundingBox3D* GetBoundingBox(unsigned char boundingBoxId);

			#pragma pack(push, 1)
			struct BoneData {
				unsigned char ParentIndex;
				unsigned char NotUsed;
				Math::KzFQuat Rotation;
				D3DXVECTOR3 Position;
			};
			
			struct BoneTransform {
				unsigned short BoneIndex;
				D3DXVECTOR3 Rotation;
				D3DXVECTOR3 Translation;
			};
			#pragma pack(pop)
		};
	}
}