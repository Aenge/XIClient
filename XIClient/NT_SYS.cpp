#include "NT_SYS.h"
#include "CApp.h"
#include "Globals.h"
#include "CYyDb.h"
using namespace FFXI::Network;

int NT_SYS::timer{ 0 };
int NT_SYS::SomeValue{ 0 };
char NT_SYS::versionstring[16]{};

void NT_SYS::SetVersionString(const char* a1)
{
    strcpy_s(versionstring, a1);
	versionstring[sizeof(versionstring) - 1] = 0;
}

int NT_SYS::getTotalModePhase(int a1)
{
	switch (a1)
	{
	case 1:
		return 21;
	case 2:
	case 17:
	case 19:
		return 4;
	case 3:
		return 22;
	case 4:
	case 20:
	case 22:
		return 10;
	case 5:
	case 18:
	case 23:
		return 2;
	default:
		return 1;
	}
}

void NT_SYS::ResetTimer()
{
    timer = 0;
}

bool NT_SYS::UpdateTimer()
{
    ++timer;
    return (timer >= 3600);
}

char* NT_SYS::getChrName(int a2)
{
    return g_GcMainSys.IwWorkPacket.ContentIDList[a2].Name;
}

char* FFXI::Network::NT_SYS::getWorldName(int a2)
{
    return g_GcMainSys.IwWorkPacket.ContentIDList[a2].WorldName;
}

int FFXI::Network::NT_SYS::getWorld(int a2)
{
    return g_GcMainSys.IwWorkPacket.ContentIDList[a2].World;
}

int FFXI::Network::NT_SYS::getArea(int a2)
{
    int base = g_GcMainSys.IwWorkPacket.ContentIDList[a2].Zone;
    int top = g_GcMainSys.IwWorkPacket.ContentIDList[a2].field_4F & 1;
    return (top << 8) + base;
}

short* FFXI::Network::NT_SYS::getEquip(int a2)
{
    return g_GcMainSys.IwWorkPacket.ContentIDList[a2].Equip;
}

int FFXI::Network::NT_SYS::getRaceGender(int a2)
{
    return g_GcMainSys.IwWorkPacket.ContentIDList[a2].RaceGender;
}

int FFXI::Network::NT_SYS::getJob(int a2)
{
    return g_GcMainSys.IwWorkPacket.ContentIDList[a2].MainJob;
}

int FFXI::Network::NT_SYS::getFaceNo(int a2)
{
    return g_GcMainSys.IwWorkPacket.ContentIDList[a2].Face;
}

int FFXI::Network::NT_SYS::getTownNo(int a2)
{
    return g_GcMainSys.IwWorkPacket.ContentIDList[a2].Town;
}

int FFXI::Network::NT_SYS::getHair(int a2)
{
    return g_GcMainSys.IwWorkPacket.ContentIDList[a2].Hair;
}

int FFXI::Network::NT_SYS::getSize(int a2)
{
    return g_GcMainSys.IwWorkPacket.ContentIDList[a2].Size;
}

int FFXI::Network::NT_SYS::getLv(int a2)
{
    return g_GcMainSys.IwWorkPacket.ContentIDList[a2].MainJobLevel;
}

int FFXI::Network::NT_SYS::getFFXIID(int a2)
{
    return g_GcMainSys.IwWorkPacket.ContentIDList[a2].field_0;
}

int FFXI::Network::NT_SYS::getTotalCharacters()
{
    return g_GcMainSys.IwWorkPacket.ContentIDCount;
}

int FFXI::Network::NT_SYS::getChrStatus(int a2)
{
    return g_GcMainSys.IwWorkPacket.ContentIDList[a2].Status;
}

bool NT_SYS::Init()
{
	if (NT_SYS_BASE::Init() != 1)
		return false;

	this->field_8 = 0;
	this->field_C = 0;
	this->field_80 = 0;
	this->field_90 = 101;
	this->field_8C = -1;
	this->field_94 = 200;
	this->field_14 = 0;

    this->ClientMode = pGame ? pGame->CliMode : 0;
    this->ZoneMode = 0;
    this->POLCON = pGame ? pGame->Polcon : 0;

	this->field_A0 = 0;
	this->field_B4 = 0;
	if (this->ClientMode == 3)
		this->field_88 = 0;
	else
		this->field_88 = 3;
	this->field_18 = 0;
	this->field_B8 = 0;
	this->field_A8 = 2;

	return true;
}

void NT_SYS::Proc()
{
    switch (this->field_8) {
    case 0:
        this->ProcBase();
        if (this->field_14 == 12 || this->field_14 == 21
            || this->field_14 == 11 || this->field_14 == 14)
            return;
        ++this->field_8;
        FFXI::CYyDb::CliZoneFadeOutFlag = 1;
        return;
    case 1:
        this->ProcBase();
        if (FFXI::CYyDb::CliZoneFadeOutFlag == 0)
            ++this->field_8;
        FFXI::CYyDb::CliLocalTask();
        return;
    case 2:
        this->ProcBase();
        FFXI::CYyDb::CliLocalTask();
        return;
    default:
        exit(0x100D87E1);
        return;
    }
}

void NT_SYS::ProcBase()
{
    if (this->field_8C >= 0)
        this->field_8C = -1;

    if (this->ClientMode) {
        int v3 = this->field_88;
        if (v3 != 12 && v3 != 21 && v3 != 202 && v3 != 203 && v3 != 201)
        {
            if (FFXI::Network::CGcMainSys::gcLoginGetIsZoneChange() == true) {
                if (this->field_88 != 9) {
                    this->modeSet(203, 0);
                }

                this->modeSet(10, 0);
            }
            if (FFXI::Network::CGcMainSys::gcLoginGetIsLogout() == true) {
                if (this->field_88 != 9) {
                    this->modeSet(203, 0);
                }
                if (this->field_A0 != 0) {
                    this->modeSet(11, 0);
                }
                else {
                    this->modeSet(14, 0);
                }
                this->field_A0 = 0;
            }
        }

        ntGameProc2();

        switch (this->field_88) {
        case 0: 
            //XIInfo start
            //sub //TODO
            if (this->field_90 == 100)
                this->field_8C = this->field_88;
            this->field_90 = 101;
            break;
        case 1:
            if (this->field_90 == 100) {
                int v8 = FFXI::Network::CGcMainSys::gcLoginProc(&this->field_C);
                if (v8 == -1) {
                    if (this->field_88 == 1) {
                        this->field_94 = 203;
                        this->modeSet(203, 0);
                    }
                }
                else if (v8 == 1) {
                    this->field_90 = 101;
                    this->field_8C = this->field_88;
                }
            }
            break;
        case 2:
            if (this->field_90 == 100) {
                //gcLoginChrListProc
                exit(0x100D81CF);
                int v13 = 0;
                if (v3 == -1) {
                    if (this->field_88 == 2) {
                        this->field_94 = 203;
                        this->modeSet(203, 0);
                    } 
                }
                else if (v13 == 1) {
                    this->field_90 = 101;
                    this->field_8C = this->field_88;
                }
            }
            break;
        case 3:
            if (this->field_90 == 100) {
                int v13 = FFXI::Network::CGcMainSys::gcLoginChrSelectProc(&this->field_C, this->field_80);
                if (v13 == -1) {
                    if (this->field_88 == 3) {
                        this->field_94 = 203;
                        this->modeSet(203, 0);
                    }
                }
                else if (v13 == 1) {
                    FFXI::Network::CGcMainSys::gcChrBindProc();
                    this->field_90 = 101;
                    this->field_8C = this->field_88;
                }
            }
            break;
        case 4:
            if (this->field_90 == 100) {
                //gcLoginChrCreateProc
                exit(0x100D82E3);
            }
            break;
        case 5:
            if (this->field_90 == 100) {
                //gcLoginChrDeleteProc
                exit(0x100D8392);
            }
            break;
        case 6:
        {
            //infowrite
            //sub //TODO
            int v18 = FFXI::Network::CGcMainSys::gcLoginCliinitProc(&this->field_C);
            if (v18 == -1) {
                if (this->field_88 == 6) {
                    this->field_94 = 203;
                    this->modeSet(203, 0);
                }
            }
            else if (v18 == 1) {
                if (this->field_90 == 100) {
                    this->field_90 = 101;
                    this->field_8C = this->field_88;
                }
            }
        }
            break;
        case 7:
        {
            //infowrite
            //sub //TODO
            int v19 = FFXI::Network::CGcMainSys::gcDataLoadProc(&this->field_C);
            if (v19 == -1) {
                if (this->field_88 == 7) {
                    this->field_94 = 203;
                    this->modeSet(203, 0);
                }
            }
            else if (v19 == 1) {
                if (this->field_90 == 100) {
                    this->field_90 = 101;
                    this->field_8C = this->field_88;
                }
            }
        }
            break;
        case 9:
        {
            int v18 = FFXI::Network::CGcMainSys::gcLoginZoneProc();
            if (v18 == -1) {
                if (this->field_88 == 9) {
                    this->field_94 = 203;
                    this->modeSet(203, 0);
                }
            }
            else if (v18 == 1) {
                if (this->field_90 == 100) {
                    this->field_90 = 101;
                    this->field_8C = this->field_88;
                }
            }
        }
            break;
        case 201:
        case 202:
        case 203:
            this->field_88 = 0;
            break;
        default:
            break;
        }
        switch (this->field_88) {
        case 201:
        case 202:
        case 203:
            this->field_8C = this->field_88;
            this->field_90 = 101;
            break;
        default:
            break;
        }
    }
    else if (this->field_90 == 100) {
        this->field_90 = 101;
        this->field_8C = this->field_88;
    }
}

bool NT_SYS::IsConnect()
{
    return FFXI::Network::CGcMainSys::gcLoginIsConnect();
}

void NT_SYS::terminateTCP() {
    FFXI::Network::CGcMainSys::gcLoginProcBreak();
}

int NT_SYS::modeGet()
{
	if (this->field_90 == 100)
		return 100;

	if (this->field_8C < 0)
		return 101;

	this->field_14 = this->field_8C;

	return this->field_14;
}

void NT_SYS::modeSet(int a2, int a3)
{
    int v5 = this->field_88;
    if (this->ClientMode == 0) {
        this->field_88 = a2;
        this->field_90 = 100;
        this->field_C = 0;
        return;
    }

    switch (a2)
    {
    case 0:
        this->field_88 = 0;
        this->field_C = 0;
        this->field_90 = 100;
        return;
    case 1:
        this->field_C = 0;
        this->field_88 = 1;
        this->field_90 = 100;
        return;
    case 2:
        if (v5 != 1 && v5 != 19 && v5 != 3 && v5 != 4 && v5 != 20 && v5 != 22 && v5 != 23 && v5 != 5) {
            this->field_94 = 201;
            this->modeSet(201, 0);
            return;
        }
        this->field_C = 0;
        this->field_88 = 2;
        this->field_90 = 100;
        return;
    case 3:
        if (v5 != 2 && v5 != 1) {
            this->field_94 = 201;
            this->modeSet(201, 0);
            return;
        }
        this->field_C = 0;
        this->field_80 = a3;
        this->field_88 = 3;
        this->field_90 = 100;
        return;
    case 4:
        if (v5 != 1 && v5 != 20) {
            this->field_94 = 201;
            this->modeSet(201, 0);
            return;
        }
        this->field_C = 0;
        this->field_80 = a3;
        this->field_88 = 4;
        this->field_90 = 100;
        return;
    case 5:
        if (v5 != 2 && v5 != 1) {
            this->field_94 = 201;
            this->modeSet(201, 0);
            return;
        }
        this->field_C = 0;
        this->field_80 = a3;
        this->field_88 = 5;
        this->field_90 = 100;
        return;
    case 6:
        if (this->field_10)
            ;// POLSTAT_Reset();
        if (v5 != 10 && v5 != 3 && v5 != 11 && v5 != 14 && v5 != 12 && v5 != 21) {
            this->field_94 = 201;
            this->modeSet(201, 0);
            return;
        }

        this->field_88 = 6;
        this->field_90 = 100;
        if (this->ClientMode == 3) {
            this->field_C = 0;
            return;
        }
        this->field_C = 15;
        return;
    case 7:
        if (this->field_88 != 6) {
            this->field_94 = 201;
            this->modeSet(201, 0);
            return;
        }

        this->field_C = 0;
        this->field_88 = 7;
        this->field_90 = 100;
        return;
    case 8:
        if (this->field_88 != 6) {
            this->field_94 = 201;
            this->modeSet(201, 0);
            return;
        }
        this->field_C = 0;
        this->field_88 = 8;
        this->field_90 = 100;
        return;
    case 9:
        if (v5 != 7 && v5 != 8) {
            this->field_94 = 201;
            this->modeSet(201, 0);
            return;
        }
        this->field_8C = 9;
        this->field_88 = 9;
        this->field_90 = 100;
        this->field_C = 0;
        return;
    case 10:
        if (this->field_88 != 9) {
            this->field_94 = 201;
            this->modeSet(201, 0);
            return;
        }
        this->field_C = 0;
        this->field_88 = 10;
        this->field_90 = 100;
        return;
    case 11:
    case 14:
        if (this->field_88 == 9) {
            this->field_88 = a2;
            this->field_90 = 100;
            this->field_C = 0;
            return;
        }
        this->field_94 = 201;
        this->modeSet(201, 0);
        return;
    case 12:
    case 21:
    case 201:
    case 202:
    case 203:
        this->field_88 = a2;
        this->field_90 = 100;
        this->field_C = 0;
        return;
    case 17:
        this->field_C = 0;
        this->field_84 = a3;
        this->field_88 = 17;
        this->field_90 = 100;
        return;
    case 18:
        this->field_C = 0;
        this->field_84 = a3;
        this->field_88 = 18;
        this->field_90 = 100;
        return;
    case 19:
        this->field_C = 0;
        this->field_88 = 19;
        this->field_90 = 100;
        return;
    case 20:
        if (v5 != 2 && v5 != 1 && v5 != 20) {
            this->field_94 = 201;
            this->modeSet(201, 0);
            return;
        }
        this->field_C = 0;
        this->field_80 = a3;
        this->field_88 = 20;
        this->field_90 = 100;
        return;
    case 22:
        if (v5 != 2 && v5 != 1 && v5 != 22) {
            this->field_94 = 201;
            this->modeSet(201, 0);
            return;
        }

        this->field_C = 0;
        this->field_80 = a3;
        this->field_88 = 22;
        this->field_90 = 100;
        return;
    case 23:
        if (v5 != 2 && v5 != 1 && v5 != 23) {
            this->field_94 = 201;
            this->modeSet(201, 0);
            return;
        }
        this->field_C = 0;
        this->field_80 = a3;
        this->field_88 = 23;
        this->field_90 = 100;
        return;
    default:
        this->field_94 = 201;
        this->modeSet(201, 0);
        return;
    }
}

int FFXI::Network::NT_SYS::getLastError()
{
    return FFXI::Network::CGcMainSys::gcLoginGetXiError();
}
