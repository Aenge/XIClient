#include "CYyMotionMan.h"
#include "BoneTransformState.h"
#include "CXiActor.h"
#include "KzFQuat.h"
#include "CYySkl.h"
#include "CMoMo2.h"
#include "Globals.h"

using namespace FFXI::CYy;

BoneTransformState CYyMotionMan::gBoneTransforms[256]{};
BoneTransformState tempBoneTransforms[256]{};

void FFXI::CYy::CYyMotionMan::AppendSync(CXiActor* a2, CMoMo2** a3, float a4, float a5, int a6, float a7, float a8, int a9, float a10, float a11)
{
	if (a9 >= 0 && a9 < sizeof(this->motions) / sizeof(this->motions[0])) {
		if (a2->GetMotStop() == false || a2->IsMotionLock() == true) {
			this->motions[a9].AppendSync(a3, a4, a5, a6, a7, a8, a10, a11);
			if (a9 == 0 && a2 != nullptr)
				a2->SetMotionLock(false);
		}
	}
}

void FFXI::CYy::CYyMotionMan::Append(CXiActor* a2, CMoMo2** a3, float a4, float a5, int a6, float a7, float a8, int a9, float a10, float a11)
{
	if (a9 < 0 || a9 >= (sizeof(this->motions) / sizeof(this->motions[0])))
		return;

	if (a2->GetMotStop() == false
		|| a2->IsMotionLock() == true) {
		this->motions[a9].Append(a3, a4, a5, a6, a7, a8, a10, a11);
		if (a9 == 0) {
			if (a2 != nullptr) {
				a2->SetMotionLock(false);
			}
		}
	}
}

bool FFXI::CYy::CYyMotionMan::UpdateAllLayers()
{
	static FFXI::Math::KzFQuat quat{};

	for (int i = 0; i < CYySkl::gBoneCount; ++i) {
		BoneTransformState* boneTransform = CYyMotionMan::gBoneTransforms + i;
		boneTransform->Rotation = quat;
		boneTransform->Translation = { 0.0, 0.0, 0.0 };
		boneTransform->Scaling = { 1.0, 1.0, 1.0 };
	}

	//Skip updating if there are no animations in the que
	int sum = 0;
	for (int i = 4; i >= 0; --i) {
		sum += this->motions[i].GetNbQue();
	}

	if (sum == 0)
		return false;

	//Update the first 5 layers in reverse order
	for (int i = 4; i >= 0; --i) {
		if (this->motions[i].Head == nullptr)
			continue;

		this->motions[i].UpdateLayer(i, CYyMotionMan::gBoneTransforms);

		for (int j = 0; j < CYySkl::gBoneCount; ++j) {
			if ((CYySkl::gBoneFlags[j] & CYySkl::BoneProcessingFlags::BONE_STATE_MASK) != CYySkl::BoneProcessingFlags::BONE_STATE_INITIAL) {
				CYySkl::gBoneFlags[j] |= CYySkl::BoneProcessingFlags::BONE_SKIP_PROCESSING;
			}
		}
	}

	//Update the last two layers
	for (int i = 5; i < sizeof(this->motions) / sizeof(this->motions[0]); ++i) {
		CYyMotionQueList* list = this->motions + i;
		if (list->Head == nullptr)
			continue;

		for (int j = 0; j < CYySkl::gBoneCount; ++j) {
			CYySkl::gBoneFlags[j] &= CYySkl::BoneProcessingFlags::BONE_ACTIVE;
		}

		list->UpdateLayer(i, tempBoneTransforms);

		for (int j = 0; j < CYySkl::gBoneCount; ++j) {
			if ((CYySkl::gBoneFlags[j] & CYySkl::BoneProcessingFlags::BONE_ACTIVE) != 0) {
				if ((CYySkl::gBoneFlags[j] & CYySkl::BoneProcessingFlags::BONE_STATE_MASK) != CYySkl::BoneProcessingFlags::BONE_STATE_INITIAL) {
					BoneTransformState* boneTransform = CYyMotionMan::gBoneTransforms + j;
					FFXI::Math::KzFQuat v25{};
					boneTransform->Rotation.lerp(&v25, &tempBoneTransforms[j].Rotation, list->blendAccumulator);
					gBoneTransforms[j].Rotation = v25;

					Globals::Vec3Lerp(
						&CYyMotionMan::gBoneTransforms[j].Translation,
						&tempBoneTransforms[j].Translation,
						&CYyMotionMan::gBoneTransforms[j].Translation,
						list->blendAccumulator);
					
					Globals::Vec3Lerp(
						&CYyMotionMan::gBoneTransforms[j].Scaling,
						&tempBoneTransforms[j].Scaling,
						&CYyMotionMan::gBoneTransforms[j].Scaling,
						list->blendAccumulator);
				}
			}
		}
	}
	return true;
}
