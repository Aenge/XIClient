#include "CIwYesNoMenu.h"
#include "CTkMenuMng.h"
#include "CTkMenuCtrlData.h"
#include "TkManager.h"
#include "GlobalStruct.h"
#include "IwManager.h"
#include "StringTables.h"
#include "TextRenderer.h"
#include "Globals.h"
#include "XIString.h"
#include "Strings.h"
#include <cstdio>
#include "SoundMng.h"

FFXI::CTk::CIwYesNoMenu::CIwYesNoMenu()
{
	this->field_14 = 1;
	this->field_30 = 0;
	this->field_A5E1 = 0;
	this->field_18 = 0;
	this->field_20[0] = 0;
	this->field_A5E0 = 0;
	this->field_A640 = -1;
	this->field_A644 = -1;
	this->field_A754 = '*-0\0';
}

void FFXI::CTk::CIwYesNoMenu::OnInitialUpdatePrimitive()
{
	this->MenuCtrlData->field_5D = 0;
	this->MenuCtrlData->field_85 = 0;
	this->MenuCtrlData->field_86 = 0;
	this->MenuCtrlData->field_84 = 0;

	int xfactor = ((short)(TkManager::g_CTkMenuMng.UIXRes - TkManager::g_CTkMenuMng.field_7C) - 512) / 2;
	int yfactor = ((short)(TkManager::g_CTkMenuMng.UIYRes - TkManager::g_CTkMenuMng.field_7E) - 448) / 2;

	TKRECT a2{};
	this->MenuCtrlData->GetOriginalRect(&a2);
	this->MenuCtrlData->ResizeWindow(
		a2.Left + xfactor,
		a2.Top + yfactor,
		a2.Right - a2.Left,
		a2.Bottom - a2.Top,
		1,
		0,
		0
	);

	this->field_A5E1 = 0;
	if (this->field_A5E0 == 1) {
		this->MenuCtrlData->DisableDrawButton(3, false);
		this->field_A5E1 = 1;
	}
	if (this->field_A5E0 == 3) {
		this->MenuCtrlData->DisableDrawButton(1, false);
		this->MenuCtrlData->DisableDrawButton(2, false);
		this->MenuCtrlData->DisableDrawButton(3, false);
	}
}

void FFXI::CTk::CIwYesNoMenu::OnDrawPrimitive()
{
	//This mess is building multi - lined strings. Each line is an element in the array
	char* mes0[1] = { nullptr };
	
	//Cancel update?
	char* mes1[2] = { FFXI::Text::XiStrGet(8, 0xA4) , nullptr };

	//Cancel file check?
	char* mes2[2] = { FFXI::Text::XiStrGet(8, 0xA5) , nullptr };

	char* mes3[2] = { &Globals::NullString, nullptr };

	//Want to log in on "Zilart Uninstalled Mode"?
	char* mes4[3] = { FFXI::Text::XiStrGet(8, 0xD1) ,FFXI::Text::XiStrGet(8, 0xD2) , nullptr };

	//Want to log in on "Virtual JP Mode"?
	char* mes5[3] = { FFXI::Text::XiStrGet(8, 0xCD) ,FFXI::Text::XiStrGet(8, 0xCE) , nullptr };

	//Want to log in on "Virtual US Mode"?
	char* mes6[3] = { FFXI::Text::XiStrGet(8, 0xCF) ,FFXI::Text::XiStrGet(8, 0xD0) , nullptr };

	//An error occurred. Retry?
	char* mes7[3] = { FFXI::Text::XiStrGet(8, 0xB4) ,FFXI::Text::XiStrGet(8, 0xB5) , nullptr };

	//nullstr + The data will be lost forever.Proceed ? (Character name gets inserted in nullstr. Used in delete char
	char* mes8[3] = { &Globals::NullString ,FFXI::Text::XiStrGet(8, 0xAE) , nullptr };

	//Want to log in on "Virtual PS2 Mode"?
	char* mes9[3] = { FFXI::Text::XiStrGet(8, 0xB8) ,FFXI::Text::XiStrGet(8, 0xB9) , nullptr };

	//Want to log in on "Promathia Uninstalled Mode"?
	char* mes10[3] = { FFXI::Text::XiStrGet(8, 0xD3) ,FFXI::Text::XiStrGet(8, 0xD4) , nullptr };

	//Want to log in on "Virtual Windows Mode"?
	char* mes11[3] = { FFXI::Text::XiStrGet(8, 0xB6) ,FFXI::Text::XiStrGet(8, 0xB7) , nullptr };

	//Want to log in on "Aht Urghan Uninstalled Mode"?
	char* mes12[3] = { FFXI::Text::XiStrGet(8, 0xD5) ,FFXI::Text::XiStrGet(8, 0xD6) , nullptr };

	//nullstr + and begin play? (Character name gets inserted in nullstr. Used when char is selected
	char* mes13[3] = { &Globals::NullString ,FFXI::Text::XiStrGet(8, 0xB0) , nullptr };

	//Want to log in on "Virtual XBox Mode"?
	char* mes14[3] = { FFXI::Text::XiStrGet(8, 0xBA) ,FFXI::Text::XiStrGet(8, 0xBB) , nullptr };

	//***DEBUG*** Delete unnecessary config files? Choose "Yes" to delete from HDD. QA Staff should select "Yes"!
	char* mes15[4] = { FFXI::Text::XiStrGet(8, 0xB1) ,FFXI::Text::XiStrGet(8, 0xB2) ,FFXI::Text::XiStrGet(8, 0xB3), nullptr };

	char* mes16[4] = { &Globals::NullString ,&Globals::NullString ,&Globals::NullString, nullptr };
	char* mes17[5] = { &Globals::NullString ,&Globals::NullString ,&Globals::NullString, &Globals::NullString, nullptr };
	char* mes18[5] = { &Globals::NullString ,&Globals::NullString ,&Globals::NullString, &Globals::NullString, nullptr };

	//***DEBUG*** Skip the handle menu? Choose "Yes" to skip. The handle menu will not work unless FFXI is installed on the HDD and you boot from POL.
	char* mes19[5] = { FFXI::Text::XiStrGet(8, 0xAA) ,FFXI::Text::XiStrGet(8, 0xAB) ,FFXI::Text::XiStrGet(8, 0xAC), FFXI::Text::XiStrGet(8, 0xAD), nullptr };
	
	//***DEBUG*** Skip the update patch? Choose "Yes" to skip. The patch will not work unless FFXI is installed on the HDD and you boot from POL.
	char* mes20[5] = { FFXI::Text::XiStrGet(8, 0xA6) ,FFXI::Text::XiStrGet(8, 0xA7) ,FFXI::Text::XiStrGet(8, 0xA8), FFXI::Text::XiStrGet(8, 0xA9), nullptr };
	
	char** messages[27] = { mes1, mes1, mes20, mes19, mes8, mes13, mes15, mes7, mes11, mes9, mes16, mes18, mes17, mes3, mes5, mes6, mes4,
							mes10, mes12, mes0, mes0, mes0, mes0, mes0, mes0, mes0, mes14};
	TKRECT a2{};
	a2.Left = 0;
	a2.Top = 0;
	a2.Right = 0;
	a2.Bottom = 0;

	this->MenuCtrlData->GetWindowLocateCompen(&a2);
	FFXI::CTk::CTkMenuMng::MarginComp(&a2);
	if (FFXI::GlobalStruct::g_GlobalStruct.GetRegionCode() != FFXI::Constants::Enums::LanguageCode::Japanese) {
		a2.Left += 48;
	}

	switch (this->field_A5E0) {
	case 0: 
	{
		char message[1024];
		char* v8{};
		switch (this->field_18) {
		case 4:
			v8 = FFXI::Text::XiStrGet(10, 0x29);
			break;
		case 5:
			v8 = FFXI::Text::XiStrGet(10, 0xAF);
			break;
		case 10:
			v8 = FFXI::Text::XiStrGet(27, 6);
			sprintf_s(message, v8, &this->field_20);
			FFXI::Text::TextRenderer::RenderInButtonMenu(message, &a2);
			return;
		case 13:
			v8 = FFXI::Text::XiStrGet(10, 0x52);
			break;
		default:
			IwManager::IwPutMes(messages[this->field_18], &a2);
			return;
		}

		sprintf_s(message, v8, &this->field_20);
		messages[this->field_18][0] = message;
		IwManager::IwPutMes(messages[this->field_18], &a2);
	}
		return;
	case 1: 
	{
		char message[1024];
		char v56[256];
		char v57[256];
		char v59[256];
		if (this->field_A5D8 == 0) {
			if (this->field_18 == 11) {
				char* v17 = FFXI::Text::XiStrGet(8, 0xC7);
				sprintf_s(message, v17, &this->field_20);
				v17 = FFXI::Text::XiStrGet(8, 0xEC);
				sprintf_s(v57, v17);
			}
			else if (this->field_18 == 12) {
				char* v17 = FFXI::Text::XiStrGet(8, 0xC9);
				sprintf_s(message, v17, &this->field_20);
				v17 = FFXI::Text::XiStrGet(8, 0xED);
				sprintf_s(v57, v17);
			}

			char* v14 = FFXI::Text::XiStrGet(8, 0xEE);
			sprintf_s(v59, v14, &this->field_A5E8, &this->field_A600);
		}
		else {
			int value = (this->field_A5D8 & 0xFF) + 64;
			if (this->field_18 == 11) {
				char* v17 = FFXI::Text::XiStrGet(8, 0xC7);
				sprintf_s(message, v17, &this->field_20);
				v17 = FFXI::Text::XiStrGet(8, 0xC8);
				sprintf_s(v57, v17, value);
			}
			else if (this->field_18 == 12) {
				char* v17 = FFXI::Text::XiStrGet(8, 0xC9);
				sprintf_s(message, v17, &this->field_20);
				v17 = FFXI::Text::XiStrGet(8, 0xCA);
				sprintf_s(v57, v17, value);
			}

			char* v14 = FFXI::Text::XiStrGet(8, 0xCB);
			sprintf_s(v59, v14, value, &this->field_A5E8, &this->field_A600);
		}

		v56[0] = 0;
		messages[this->field_18][0] = message;
		messages[this->field_18][1] = v57;
		messages[this->field_18][2] = v56;
		messages[this->field_18][3] = v59;
		if (FFXI::GlobalStruct::g_GlobalStruct.GetRegionCode() == FFXI::Constants::Enums::LanguageCode::Japanese) {
			IwManager::IwPutMes(messages[this->field_18], &a2);
		}
		else {
			FFXI::Text::XIString v8{};
			int pos = 0;
			while (messages[this->field_18][pos] != nullptr) {
				v8.AppendStr(messages[this->field_18][pos]);
				pos += 1;

				if (messages[this->field_18][pos] == nullptr) {
					v8.AppendStr(FFXI::Constants::Strings::DoubleNewLineStr);
				}
				else {
					v8.AppendStr(FFXI::Constants::Strings::SpaceString);
				}
			}

			TKRECT v7 = a2;
			v7.Left -= 28;
			const char* v6 = v8.c_str();
			FFXI::Text::TextRenderer::SjisFontDrawInRectHSpace(v6, &v7, 0);
		}
	}
		return;
	case 2:
		exit(0x0200022);
		return;
	case 3:
		exit(0x0300033);
		return;
	default:
		return;
	}
}

void FFXI::CTk::CIwYesNoMenu::OnUpdatePrimitive()
{
	this->MenuCtrlData->field_4C = (this->field_14 & 0xFFFF) + 1;
	this->MenuCtrlData->RepositionCursol(this->MenuCtrlData->field_4C);
}

void FFXI::CTk::CIwYesNoMenu::OnKeyDown(short a2, short a3)
{
	if (this->field_30 == 1) {
		return;
	}

	switch (a2) {
	case 1:
		if (this->field_A5E0 == 2 || this->field_A5E0 == 3)
		{
			if (this->field_A5D4 + this->field_A5D8 != this->field_A5DC - 1)
			{
				if (this->field_A5D8 == 8)
					this->field_A5D4 = this->field_A5D4 + 1;
				else
					this->field_A5D8 = this->field_A5D8 + 1;
			}
		}
		return;
	case 2:
		if ((this->field_A5E0 == 2 || this->field_A5E0 == 3) && this->field_A5D8 != 0) {
			this->field_A5D8 -= 1;
		}
		else if (this->field_A5D4 > 0) {
			this->field_A5D4 -= 1;
		}
		return;
	case 3:
	case 4:
	case 9:
		FFXI::SoundMng::CYySePlayCursor();
		return;
	case 5:
		this->field_30 = 1;
		this->MenuCtrlData->field_5D = 1;
		this->field_A640 = -1;
		this->field_14 = a3 - 1;
		this->field_A644 = -1;
		this->field_A754 = '*-0\0';
		FFXI::SoundMng::CYySePlayClick();
		return;
	case 6:
		if (this->field_A5E1 != 0) {
			this->OnKeyDown(5, 2);
		}
		return;
	default:
		return;
	}
}

void FFXI::CTk::CIwYesNoMenu::OnDrawCalc(bool)
{
	if (this->field_A5E0 != 1 || this->field_30 == 1) {
		return;
	}

	bool v4 = false;
	//this block gets values from pol
	if (this->field_A644 == -1) {
		if (this->field_A640 >= 0) {

		}
		else {
			
		}
	}

	//this block gets values from a file
	if (this->field_A5E4 != this->field_A5D8 || this->field_A5D8 == 0 && v4 == true) {
		if (this->field_A5D8 != 0) {

		}
		else {

		}
	}
}
