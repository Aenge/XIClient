#pragma once
#define WIN32_LEAN_AND_MEAN
#include "d3dx9math.h"
namespace FFXI {
	namespace CYy {
		class RidListNode;
		class Zoneline;
		class Submodel;
		class RidStruct;
		class RidManager {
		public:
			RidManager();
			virtual ~RidManager();
			static int ListSize;
			static RidListNode* ListHead;
			static RidListNode* ListTail;
			static RidListNode* GetHead();
			static void InitUnderscoreRid(RidStruct*, char*);
			void Add(char*);
			void Clean();
			void Init();
			void InitZonelines();
			void InitSubModels();
			RidStruct* LiftRectHitCheck(D3DXVECTOR3*, D3DXVECTOR3*);
			Submodel* Submodels;
			int SubmodelCount;
			Zoneline* Zonelines;
			int ZonelineCount;
		};
	}
}