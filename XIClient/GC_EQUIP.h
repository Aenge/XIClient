#pragma once
#include "EquipmentSlots.h"
namespace FFXI {
	namespace Zone {
		class GC_EQUIP {
		public:
			struct slot {
				int field_0;
				unsigned char field_4;
				unsigned char field_5;
				unsigned char field_6;
				unsigned char field_7;
			};
			struct something {
				int field_0;
				int field_4;
				int field_8;
				unsigned short field_C;
				unsigned short field_E;
				int field_10;
				int field_14;
				int field_18;
				int field_1C;
				int field_20;
			};
			slot slots[FFXI::Constants::Enums::EquipmentSlot::MAX];
			something field_80[FFXI::Constants::Enums::EquipmentSlot::MAX];
			int field_2C0;
			int field_2C4;
			int field_2C8;
			int field_2CC;
			int field_2D0;
			int field_2D4;
			int field_2D8;
			int field_2DC;
			int field_2E0;
			int field_2E4;
			int field_2E8;
			int field_2EC;
			int field_2F0;
			int field_2F4;
			int field_2F8;
			int field_2FC;
			int field_300;
			int field_304;
			int field_308;
			int field_30C;
			int field_310;
			int field_314;
			int field_318;
			int field_31C;
			int field_320;
			int field_324;
			int field_328;
			int field_32C;
			int field_330;
			int field_334;
			int field_338;
			int field_33C;
			int field_340;
			int field_344;
			int field_348;
			int field_34C;
			int field_350;
			int field_354;
			int field_358;
			int field_35C;
			int field_360;
			int field_364;
			int field_368;
			int field_36C;
			int field_370;
			int field_374;
			int field_378;
		};
	}
}