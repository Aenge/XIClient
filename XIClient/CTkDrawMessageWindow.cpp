#include "CTkDrawMessageWindow.h"
#include "CTkDrawMessageWindowOne.h"
#include "CTkDrawMessageWindowTwo.h"
#include "MojiDraw.h"
#include "CTkMenuMng.h"
#include "TkManager.h"
#include "CTkInputCtrl.h"
#include "CTkMenuCtrlData.h"
#include "CTkMsgWinData.h"
#include "CYyDb.h"
#include "PrimMng.h"
#include "TextRenderer.h"
#include "CDx.h"
#include "PlaceHolders.h"
#include "CTkMouse.h"
#include "CTkDrawCtrlFrame.h"
#include "InputMng.h"
#include "_49SubList.h"
#include "CTkSpoolMsgBase.h"
#include "CTkEventMsgBase.h"
D3DVIEWPORT8 FFXI::CTk::CTkDrawMessageWindow::LogViewport1{};
D3DVIEWPORT8 FFXI::CTk::CTkDrawMessageWindow::LogViewport2{};
FFXI::CTk::CTkMenuCtrlData* FFXI::CTk::CTkDrawMessageWindow::kaipage{};
void MessageWindowControl() {
	exit(0x23423);
	int a = 1;
}

void FFXI::CTk::CTkDrawMessageWindow::LogViewportSetup(TKRECT a1)
{
	memcpy(&LogViewport1, FFXI::CYy::CDx::instance->GetTopViewport(), sizeof(LogViewport1));
	memcpy(&LogViewport2, &LogViewport1, sizeof(LogViewport2));
	DWORD Top = (DWORD)a1.Top;
	DWORD Bottom = (DWORD)a1.Bottom;
	DWORD Left = (DWORD)a1.Left;
	DWORD Right = (DWORD)a1.Right;

	if (Top < 0u) {
		Top = 0u;
	}
	if (Left < 0u) {
		Left = 0u;
	}
	if (Bottom > LogViewport1.Height) {
		Bottom = LogViewport1.Height;
	}
	if (Right > LogViewport1.Width) {
		Right = LogViewport1.Width;
	}

	LogViewport2.Y = Top;
	if (Bottom == Top) {
		LogViewport2.Height = 1;
	}
	else {
		LogViewport2.Height = Bottom - Top;
	}
	LogViewport2.X = Left;
	LogViewport2.Width = Right - Left;
	FFXI::CYy::CDx::instance->AddSetViewport(&LogViewport2);

}

void FFXI::CTk::CTkDrawMessageWindow::TkDrawStringWithCtrl(short* a1, short a2, DWORD a3, DWORD a4, DWORD a5, unsigned char a6, char a7, TKRECT* a8)
{
	if (a6 == 0) {
		a6 = 128;
	}

	DWORD color = a3 & 0x00FFFFFF | (a6 << 24);
	DWORD x = a4;
	DWORD y = a5;
	for (short i = 0; i < a2; i++) {
		short v9 = a1[i];
		if (v9 >= 0) {
			Text::MojiDraw::TextPos.x = x;
			Text::MojiDraw::TextPos.y = y;
			Text::TextRenderer::g_MojiDraw->MojiIndexDraw(v9, color, 68, 1.0, 1.0);
			Text::TextRenderer::g_MojiDraw->SetIndent(v9, (int*) & x);
		}
		else if (v9 <= -512) {
			color = CTk::CTkMsgWinData::GetLineColorData(-512 - v9);
			color = color & 0x00FFFFFF | (a6 << 24);
		}
		else if (v9 <= -256) {
			int index = abs(v9 + 256);
			if (index >= 17) {
				color = CTk::CTkMsgWinData::GetColorData(index);
			}
			else {
				color = CTk::CTkMsgWinData::SomeColors[index];
			}

			if (color == 0) {
				color = a3;
			}

			color = color & 0x00FFFFFF | (a6 << 24);
		}
	}

	if (a7 != 0) {
		if (a8 != nullptr && a8->Right - 12 < x) {
			x = a8->Right - 12;
		}
		else {
			x -= 9;
		}
		y += 4;
		CTk::CTkDrawMessageWindow::kaipageDraw(x, y);
	}
	
	FFXI::Text::TextRenderer::PrepareDeviceForText2();
	FFXI::Text::TextRenderer::g_MojiDraw->SetMojiTexture();
}

void FFXI::CTk::CTkDrawMessageWindow::LogDraw(TKRECT a2, short a3, short a4, short a5, _49SubList* a6)
{
	if (CYyDb::g_pCYyDb->field_8 == 0) {
		return;
	}

	if (a4 <= 0) {
		return;
	}

	if (a2.Top == a2.Bottom) {
		return;
	}

	if (a5 < a2.Top - a2.Bottom) {
		return;
	}

	if (a6 != nullptr) {
		exit(0x1015BACD);
	}

	FFXI::Text::TextRenderer::PrepareDeviceForText2();
	FFXI::Text::TextRenderer::g_MojiDraw->SetMojiTexture();
	LogViewportSetup(a2);
	TK_LOGDATAHEADER* header{ nullptr };
	short v6 = a3;
	int va4 = a5 - 16;
	bool v10{ false };
	int v14{};
	TKRECT* v15{ nullptr };
	for (short i = 0; i < a4; ++i) {
		short* prev = this->field_18->GetPrevString(&v6, &header);
		if (prev == nullptr) {
			break;
		}

		if (header->field_12 != 1 || v10 == true) {
			v14 = 0;
			v15 = nullptr;
		}
		else {
			v10 = true;
			v14 = 1;
			v15 = &a2;
		}
		unsigned int color = CTkMsgWinData::GetLineColorData(header->field_0.field_0);
		TkDrawStringWithCtrl(prev, header->field_10, color, a2.Left + 4, va4 + a2.Bottom, 0x7Fu, v14, v15);
		va4 -= 16;
	}

	CYy::CDx::instance->AddSetViewport(&LogViewport1);
}

void FFXI::CTk::CTkDrawMessageWindow::kaipageDraw(DWORD x, DWORD y)
{
	if (kaipage == nullptr) {
		return;
	}

	_49SubList* shape = kaipage->GetCursolShape();
	if (shape == nullptr) {
		return;
	}

	shape->Draw(x, y, 0x80808080, 0, 0);
}

FFXI::CTk::CTkDrawMessageWindow::CTkDrawMessageWindow(CTkMsgWinData* a2)
{
	this->field_E = -1;
	this->MenuCtrlData = 0;
	this->field_C = 1;
	this->field_18 = a2;
	this->field_24.Top = 0;
	this->field_24.Left = 0;
	this->field_24.Bottom = 0;
	this->field_24.Right = 0;
	this->field_40.Top = 0;
	this->field_40.Left = 0;
	this->field_40.Bottom = 0;
	this->field_40.Right = 0;
	this->field_1C = nullptr;
	this->field_20 = nullptr;
	this->field_38 = 0;
	this->field_2C = 0;
	this->field_2E = 0;
	this->field_30 = 0;
	this->field_48 = 0.0f;
	this->field_58 = 1;
	this->field_54 = 20;
	this->field_50 = 0;
	this->field_4E = 8;
	this->field_4C = 0;
	this->field_4D = 1;
	this->field_5A = 0;
	this->field_56 = 0;
	this->field_31 = 4;

	FFXI::CTk::CTkMenuMng* mmng = &FFXI::CTk::TkManager::g_CTkMenuMng;
	this->field_32 = mmng->UIXRes - mmng->field_7C - 160;
	this->field_52 = mmng->UIXRes - mmng->field_7C - 160;
}

FFXI::CTk::CTkDrawMessageWindow::~CTkDrawMessageWindow()
{
	FFXI::Text::MojiDraw::g_pFontUsGaiji = nullptr;
	CTkDrawMessageWindow::kaipage = nullptr;
}

void FFXI::CTk::CTkDrawMessageWindow::OnInitialUpdatePrimitive()
{
	this->Resize();
	this->MenuCtrlData->DisableDrawButton(1, false);
	this->field_3C = 0;
	this->field_2C = this->field_18->field_14;
}

void FFXI::CTk::CTkDrawMessageWindow::OnDrawPrimitive()
{
	if (this->field_31 <= 0) {
		return;
	}

	this->LogDraw(this->field_40, this->field_2C, this->field_30, this->field_2E, 0);
}

void FFXI::CTk::CTkDrawMessageWindow::OnDrawCalc(bool a2)
{
	if (CYyDb::g_pCYyDb->field_8 == 0) {
		return;
	}

	if (this->MsgWinVirt3() == true) {
		MessageWindowControl();
	}

	if (TkManager::g_CTkMenuMng.IsCurrentWindow(this->MenuCtrlData) == true) {
		if (TkManager::g_pCTkInputCtrl->field_68 != 1) {
			TkManager::g_CTkMenuMng.ActiveCurrentWindow();
		}
	}

	int type1count{ 0 };
	int type2count{ 0 };
	short v59 = this->field_2C;
	TK_LOGDATAHEADER* header{};
	short* prev { nullptr };
	for (char i = 0; i < this->field_31; ++i) {
		prev = this->field_18->GetPrevString(&v59, &header);
		if (prev == nullptr) {
			break;
		}

		if (header->field_12 == 1) {
			type1count += 1;
		}
		else if (header->field_12 == 2) {
			type2count += 1;
		}
	}

	bool v55{ false };
	if (CTkMenuMng::IsPropertyWindow(0x1000u) == true) {
		v55 = true;
	}
	else {
		if (this->field_20 != nullptr) {
			if (this->field_20->MenuCtrlData != nullptr && this->field_20->MenuCtrlData->field_69 != 0) {
				v55 = true;
			}
			else if (this->field_20->field_14.field_10 >= type1count && prev != nullptr && header->field_12 == 1) {
				v55 = true;
			}
		}
		if (v55 == false && this->field_1C != nullptr) {
			if (this->field_1C->IsActive() == true) {
				v55 = true;
			}
			else if (this->field_1C->field_14.field_10 >= type2count && prev != nullptr && header->field_12 == 2) {
				v55 = true;
			}
		}
	}

	if (Placeholder::g_fsTextInputDisplay != nullptr) {
		//TODO
		exit(0x1015BF48);
	}
	else if (v55 == false && CTk::TkManager::g_CTkMenuMng.HaveActiveWindow() == false) {
		if (FFXI::Input::InputMng::GetKey(59, (Constants::Enums::KEYTYPE)149, Constants::Enums::TRGTYPE_1, -1) == true) {
			if (CTk::TkManager::g_CTkMenuMng.IsNoWindow() == false) {
				this->Adds();
			}
		}
	}
	else if (this->field_59 != 0) {
		if ((this->field_20 == nullptr || this->field_20->MenuCtrlData == nullptr || this->field_20->MenuCtrlData->field_69 == 0 || this->field_20->field_14.field_10 < type1count || prev == nullptr || header->field_12 != 1)
			&& (this->field_1C == nullptr || this->field_1C->IsActive() == false || this->field_1C->field_14.field_10 < type2count || prev == nullptr || header->field_12 != 2))
		{
			if (CTk::TkManager::g_CTkMenuMng.IsNoWindow() == false) {
				this->Adds();
			}
		}
	}
	
	//label 51
	this->field_59 = 0;
	if (a2 == true) {
		this->field_34 = this->field_4E * this->field_54;
	}

	int tick = (int)CYyDb::CheckTick();
	if (this->field_34 > this->field_54 * this->field_50 && v55 == false) {
		this->field_34 -= tick;
		int v21 = this->field_54 * this->field_50;
		if (this->field_34 < v21) {
			this->field_34 = v21;
		}
	}
	
	if (this->field_38 > 0) {
		this->field_38 -= tick;
		int v25 = 20 * this->field_4E;
		if (this->field_38 > v25) {
			this->field_38 = v25;
		}
		if (this->field_38 < 0) {
			this->field_38 = 0;
		}
	}
	if (this->field_3C > 0) {
		this->field_3C -= tick * this->field_48;
		if (this->field_3C < 0) {
			this->field_3C = 0;
		}
	}

	this->field_31 = this->field_34 / this->field_54;
	this->field_30 = this->field_31 + 1;
	if (this->field_34 % this->field_54 > 0) {
		this->field_31 += 1;
		this->field_30 += 1;
	}
	if (this->MsgWinVirt3() == true && (this->field_4C != 0 || Placeholder::g_fsTextInputDisplay)) {
		this->field_30 -= 1;
		if (this->field_30 < 0) {
			this->field_30 = 0;
		}
	}
	
	TKRECT v60{};
	this->MenuCtrlData->GetWindowLocate(&v60);

	short v31 = 16 * (this->field_31 - 8) - this->field_24.Top;
	short v33 = this->field_24.Bottom + v31;
	short height = v60.Bottom - v60.Top;

	//set v35
	short v35{};
	if (height == v33) {
		v35 = height;
	}
	else {
		int v3 = 1;
		if ((v33 - height) < 0) {
			v3 = -1;
		}
		int v5{};
		if (abs(v33 - height) <= 16u) {
			v5 = 16 * v3 * (10 - (this->field_38 % 10)) / 60;
		}
		else {
			v5 = 16 * v3 / 2;
		}
		if (abs(v5) <= abs(height - v33)) {
			v35 = v5 + height;
		}
		else {
			v35 = v33;
		}
	}

	if (height != v35) {
		this->MenuCtrlData->ResizeWindow(
			this->MenuCtrlData->field_3A.Left,
			this->MenuCtrlData->field_3A.Bottom - v35,
			this->field_32 + 14,
			v35, 1, 0, 0);

		tagPOINT vt{};
		if (FFXI::CTk::TkManager::g_pCTkMouse != nullptr) {
			vt = FFXI::CTk::TkManager::g_pCTkMouse->field_4;
		}
		if (v55 != false) {
			CTkMenuCtrlData* mcd = CTk::TkManager::g_CTkMenuMng.GetCurMCD();
			if (mcd != nullptr) {
				if ((mcd->field_34 & 0x1000) != 0) {
					if (mcd->IsOnWindow(vt) == true) {
						vt.y += height - v35;
					}
				}
			}
		}
	}

	this->MenuCtrlData->GetWindowLocate(&this->field_40);
	this->field_40.Bottom -= 4;
	this->field_40.Top += 3;
	this->field_2E = 0;
	if (this->MsgWinVirt3() == true && (this->field_4C != 0 || Placeholder::g_fsTextInputDisplay)) {
		this->field_40.Bottom -= 16;
		this->field_2E -= 16;
	}

	short v4{ 0 };
	if (this->field_20->MenuCtrlData != nullptr) {
		if (this->field_20->IsDrawWindow() == true) {
			if (this->field_20->MenuCtrlData->field_69 != 0) {
				v4 = this->field_20->field_4E;
			}
		}
	}

	if (this->field_1C->MenuCtrlData != nullptr) {
		if (this->field_1C->IsDrawWindow() == true) {
			if (this->field_1C->MenuCtrlData->field_69 != 0) {
				if (v4 < this->field_1C->field_4E) {
					v4 = this->field_1C->field_4E;
				}
			}
		}
	}

	if (v4 > 4) {
		v4 = 4;
	}

	this->field_40.Top += 16 * v4;
	this->field_30 -= v4;
	if (this->field_3C > 0) {
		this->field_2E += 16 * (this->field_3C % 20) / 20;
	}

	short v48 = this->field_18->GetTailBufferLineNo();
	short v49 = this->field_2C;
	if (v49 == v48) {
		this->field_3C = this->field_3C % 20;
	}
	else {
		int i{};
		for (i = 0; v49 != v49; ++i) {
			v49 = this->field_18->GetNextLineNo(v49);
		}
		if (i <= this->field_3C / 20) {
			this->field_3C = this->field_3C % 20 + 20 * i;
		}
		else {
			this->field_2C = this->field_18->GetNextLineNo(this->field_2C);
		}

		if (i > 64) {
			this->field_3C = 0;
			this->field_2C = this->field_18->field_14;
		}
		else if (i > 4) {
			this->field_48 = 20.0f;
		}
		else {
			this->field_48 = 1.0f;
		}
	}

	CTk::CTkDrawCtrlFrame* frame = this->MenuCtrlData->GetDrawCtrlFrame();
	if (frame != nullptr) {
		if (this->field_31 != 0) {
			if (frame->field_44 == 1 || frame->field_44 == 2) {
				frame->field_44 = 3;
				frame->field_48 = 20;
			}
		}
		else if (frame->field_44 == 3 || frame->field_44 == 0) {
			frame->field_44 = 2;
			frame->field_48 = 20;
		}
	}
}

void FFXI::CTk::CTkDrawMessageWindow::PrimVirt16(int a2)
{
	if (a2 == 512) {
		this->field_18->Init();
	}
}

void FFXI::CTk::CTkDrawMessageWindow::MsgWinVirt1(char a2)
{
	this->field_5A = a2;
}

char FFXI::CTk::CTkDrawMessageWindow::MsgWinVirt2()
{
	return this->field_5A;
}

void FFXI::CTk::CTkDrawMessageWindow::AddLine(bool a2)
{
	if (a2 == true) {
		if (this->field_34 >= 0) {
			this->field_34 += this->field_54;
		}
		else {
			this->field_34 = this->field_54;
		}
		this->field_3C += 20;
		int v8 = this->field_4E * this->field_54;
		if (this->field_34 > v8) {
			this->field_34 = v8;
		}
		int v2 = this->field_34 % this->field_54;
		if (v2 != 0) {
			this->field_34 += this->field_54 - v2;
		}
	}
	
	TK_LOGDATAHEADER* v10{};
	short v20[2] = { this->field_18->field_14, 0 };
	for (short i = 0; i < this->field_4E; ++i) {
		if (i >= (short)this->field_30 || this->field_18->GetPrevString(v20, &v10) == nullptr) {
			if (v10 != nullptr && v10->field_12 == 1) {
				if (this->field_30 > 0 && this->field_31 >= this->field_4E) {
					this->field_20->EnableDraw();
				}
			}
			break;
		}

		if (v10 == nullptr) {
			break;
		}
	}

	v20[0] = this->field_18->field_14;
	v20[1] = 0;
	short v3 = this->field_31 - this->field_1C->field_5C;

	short i = 0;
	while (i < this->field_4E + 7) {
		if (this->field_18->GetPrevString(v20, &v10) == nullptr) {
			break;
		}
		if (v10 == nullptr) {
			break;
		}
		if (i >= v3 && this->field_30 > 0 && v10->field_12 == 2) {
			if (this->field_31 >= this->field_4E - this->field_1C->field_4E) {
				this->field_1C->EnableChatDraw(v10->field_C);
				v10->field_12 = 0;
				if (v3 > this->field_4E - 4) {
					v3 -= 1;
				}
				v20[0] = this->field_18->field_14;
				v20[1] = 0;
				i = 0;
				continue;
			}
		}

		i += 1;
	}
}

void FFXI::CTk::CTkDrawMessageWindow::Resize()
{
	FFXI::CTk::CTkMenuMng* mmng = &FFXI::CTk::TkManager::g_CTkMenuMng;
	this->field_32 = mmng->UIXRes - mmng->field_7C - 160;
	if (this->field_32 > this->field_52) {
		this->field_32 = this->field_52;
	}

	if (this->MenuCtrlData == nullptr) {
		return;
	}

	this->MenuCtrlData->GetOriginalRect(&this->field_24);
	this->MenuCtrlData->ResizeWindow(
		this->field_24.Left, this->field_24.Top, 
		this->field_32 + 14, 
		this->field_24.Bottom - this->field_24.Top, 
		1, 0, 0
	);
}

void FFXI::CTk::CTkDrawMessageWindow::Adds()
{
	this->field_34 -= this->field_54;
	int v4 = this->field_50 * this->field_54;
	if (this->field_34 < v4) {
		this->field_34 = v4;
	}
	int v5 = this->field_34 % this->field_54;
	if (v5 != 0) {
		this->field_34 += this->field_54 + -v5;
	}
	this->field_3C -= 20;
	if (this->field_3C < 0) {
		this->field_3C = 0;
	}
}
