#include "CTkSpoolMsgData.h"
#include "CTkDrawMessageWindow.h"
#include "CYyDb.h"
#include <cstring>

FFXI::CTk::CTkSpoolMsgData::CTkSpoolMsgData(CTkDrawMessageWindow* a2)
{
	this->field_14C = a2;
	memset(this->field_4, 0, sizeof(this->field_4));
	memset(&this->field_104, 0, sizeof(this->field_104));
	this->field_144 = 0x7F7F7F7F;
	this->field_14A = 0;
	this->field_140 = a2->field_54;
}

bool FFXI::CTk::CTkSpoolMsgData::NeckCalc()
{
	if (this->field_140 <= 0) {
		return false;
	}

	short tick = (short)CYyDb::CheckTick();
	this->field_140 -= (int)tick;
	if (this->field_140 <= 0) {
		this->field_140 = 0;
		this->field_14A = 2;
	}

	return true;
}
