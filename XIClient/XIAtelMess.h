#pragma once
namespace FFXI {
	namespace Text {
		class XIAtelMess {
			static char* StatusMess;
			static char* BtlMess;
			static char* EmotionMess;
			static char* SkillName;
			static char* BitName;
			static char* SystemMess;
			static char* WeatherHead;
			static char* WeatherHead2;
			static char* MonWazaMess;
			static char* UnityMess;
			static char* SevMess;
		public:
			static void ItemNameDataRead();
			static void SevDataRead(unsigned short, unsigned short);
			static void ReadMess(char**, unsigned int, bool);
			static void MesConv(unsigned char*, int);
			static void Clean();
		};
	}
}