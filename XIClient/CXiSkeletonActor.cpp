#include "CXiSkeletonActor.h"
#include "CXiSkeletonActorRes.h"
#include "CYyMotionQue.h"
#include "CYyModelDt.h"
#include "XiAtelBuff.h"
#include "Globals.h"
#include "RegistryConfig.h"
#include "CDx.h"
#include "FsConfig.h"
#include "Strings.h"
#include "DatIndices.h"
#include "MemoryPoolManager.h"
#include "CYyDb.h"
#include "CYyCamMng2.h"
#include "CMoTaskMng.h"
#include "CMoResourceMng.h"
#include "ResourceContainer.h"
#include "CMoSk2.h"
#include "CMoMo2.h"
#include "CMoOs2.h"
#include "CYyModelBase.h"
#include "CMoCib.h"
#include "CMoDx.h"
#include "CMoOT.h"
#include "RaceModelTables.h"
#include "EquipReferenceRead.h"
#include "MotionReferenceRead.h"
#include "CYyModel.h"
#include "ResourceList.h"
#include "XiZone.h"
#include "WMatrix.h"
#include "KzFQuat.h"
#include "PlaceHolders.h"
#include "CYyResfList.h"
#include "CYyTexMng.h"
#include "CMoAttachments.h"
#include "CYyGeneratorClone.h"
#include "ZoneNames.h"
#include <iostream>
using namespace FFXI::CYy;

const BaseGameObject::ClassInfo CXiSkeletonActor::CXiSkeletonActorClass{
	"CXiSkeletonActor", sizeof(CXiSkeletonActor), &CXiCollisionActor::CXiCollisionActorClass
};

D3DLIGHT8* CXiSkeletonActor::g_light{ nullptr };
D3DLIGHT8 CXiSkeletonActor::g_light_arr[4]{};
FFXI::Math::BoundingBox3D CXiSkeletonActor::skeletonActor_emulate_boundingbox[3]{};
D3DCOLOR CXiSkeletonActor::ActorAmbientLight{};
CYyTex* CXiSkeletonActor::s_cubemap_spec{ nullptr };
CMoResource** CXiSkeletonActor::s_veve_texinfo{ nullptr };
char CXiSkeletonActor::is_motion_append{ 0 };

float CXiSkeletonActor::s_float1{ 0.0 };
float CXiSkeletonActor::s_float2{ 0.0 };
float CXiSkeletonActor::s_float3{ 0.0 };
float CXiSkeletonActor::s_float4{ 0.0 };
int CXiSkeletonActor::s_int1{ 0 };
int CXiSkeletonActor::s_int2{ 0 };
D3DCOLOR CXiSkeletonActor::s_color1{ 0 };
char CXiSkeletonActor::s_char1{ 0 };

void __cdecl FFXI::CYy::CXiSkeletonActor::OTCallback(FFXI::CYy::CMoTask* a1)
{
	CXiSkeletonActor* actor = (CXiSkeletonActor*)a1;
	if (actor == nullptr) {
		return;
	}

	if (actor->field_B2 != 0) {
		return;
	}

	if (actor->field_8C < 0.0f) {
		actor->field_B0 = 1;
		actor->field_B2 |= 1;
	}
	else {
		actor->Draw();
	}
}

const BaseGameObject::ClassInfo* FFXI::CYy::CXiSkeletonActor::GetRuntimeClass()
{
	return &CXiSkeletonActor::CXiSkeletonActorClass;
}

char FFXI::CYy::CXiSkeletonActor::OnMove()
{
	bool virt_72_result = this->VirtActor72() != 0;
	this->Model.field_30 &= 0x10u;
	this->field_B2 &= ~1u;
	this->field_60C.x = 0.0; this->field_60C.y = 0.0; this->field_60C.z = 0.0;
	this->field_62C.x = 0.0; this->field_62C.y = 0.0; this->field_62C.z = 0.0;
	if (this->InOwnActorPointers() == false) {
		if (this->CheckAtel13CBit20() == true) {
			if (this->field_8BC < 0.0) {
				this->field_8BC = 32.0;
				this->field_8B6 = 1;
			}
			this->SetAtel13CBit20(false);
		}

		if (this->field_8BC >= 0.0) {
			this->field_8BC -= CYyDb::g_pCYyDb->CheckTick();
		}
	}

	D3DXVECTOR4 v287 = *this->GetPos();
	D3DXVECTOR4 v290 = *this->VirtActor102();

	char collisionResult = this->CXiCollisionActor::OnMove();
	if (collisionResult == 0) {
		this->ReadMotionPackPolling();

		D3DXVECTOR4 v277{};
		if (this->field_103 != 0
			|| this->field_104 != 0
			|| this->IsOnLift() != 0
			|| this->field_102 != 0
			|| this->field_101 != 0) {
			v277 = this->field_5C4 - v287;
			v277.w = 0.0;
		}
		else if ((this->IsOnChocobo() == true || this->IsOnMount() == true) 
			&& this->field_768[0] == nullptr) {
			v277 = { 0.0, 0.0, 0.0, 1.0 };
		}
		else {
			float v16 = CYyDb::g_pCYyDb->CheckTick();
			D3DXVECTOR4 a1 = v287;
			while (v16 > 0.0) {
				v277 = this->field_5C4 - a1;
				v277.x *= 0.125;
				v277.y *= 0.125;
				v277.z *= 0.125;
				a1.x += v277.x;
				a1.y += v277.y;
				a1.z += v277.z;
				v16 -= 1.0;
			}
			v277 = a1 - v287;
			v277.w = 0.0;
		}

		D3DXVECTOR4 a2{};
		bool v28 = false;
		if (this->IsDirectionLock() == false
		&& this->field_102 == 0
		&& this->field_7A4 == -1) {
			if (this->field_86C != 0) {
				if (this->field_870 > 0.0f) {
					this->field_61C.y += this->field_874;
					if (this->field_61C.y > FFXI::Constants::Values::ANGLE_PI) {
						this->field_61C.y -= FFXI::Constants::Values::ANGLE_2PI;
					}
					if (this->field_61C.y < FFXI::Constants::Values::ANGLE_MINUS_PI) {
						this->field_61C.y += FFXI::Constants::Values::ANGLE_2PI;
					}

					float v37 = fabs(this->field_874);
					float v40 = this->field_870 - v37;
					if (v40 >= 0.0f) {
						this->field_870 = v40;
					}
					else {
						this->field_870 = 0.0;

						if (this->field_874 <= 0.0) {
							v40 *= -1;
						}
						this->field_61C.y += v40;
						if (this->field_61C.y > FFXI::Constants::Values::ANGLE_PI) {
							this->field_61C.y -= FFXI::Constants::Values::ANGLE_2PI;
						}
						if (this->field_61C.y < FFXI::Constants::Values::ANGLE_MINUS_PI) {
							this->field_61C.y += FFXI::Constants::Values::ANGLE_2PI;
						}
					}
				}
			}
			else if (this->AmIControlActor() == true && this->IsParallelMove() != 0	&& (this->IsOnChocobo() == true	|| this->IsOnMount() == true)
			|| this->IsFreeRun() == 0) 
			{
				a2.x = this->field_E4.x;
				a2.y = this->field_E4.y;
				a2.z = this->field_E4.z;
				float arg0b = 0.125;
				if (Placeholder::IsEvent(255) == true) {
					int something = this->VirtActor69();
					if (something != 0) {
						arg0b *= (float)something / 100.0f;
					}
				}
				double v49 = CYyDb::g_pCYyDb->CheckTick();
				D3DXVECTOR4 v278{};
				while (v49 > 0.0) {
					v278.x = a2.x - this->field_61C.x;
					v278.y = a2.y - this->field_61C.y;
					v278.z = a2.z - this->field_61C.z;
					Globals::RotClamp(&v278);
					v278.x *= arg0b;
					v278.y *= arg0b;
					v278.z *= arg0b;
					this->field_61C += v278;
					v49 -= 1.0;
				}

				Globals::RotClamp(&this->field_61C);
			}
			else {
				D3DXVECTOR3 v286 = { this->field_5B4.x, this->field_5B4.y, this->field_5B4.z };
				D3DXVECTOR3 v287 = { v277.x, v277.y, v277.z };
				Globals::Vec3Normalize(&v286);
				Globals::Vec3Normalize(&v287);
				v286.y = 0.0;
				v287.y = 0.0;
				if (this->AmIControlActor() == true	&& Globals::Vec3Inner(&v286, &v287) < 0.0) {
					a2.y = this->field_E4.x;
					a2.z = this->field_E4.y;
					a2.w = this->field_E4.z;
					v28 = true;
					a2.y = -atan2(v286.z, v286.x);
				}
				else {
					float arg0b = v277.z * v277.z + v277.x * v277.x;
					if (v277.y < 0.0) {
						arg0b += v277.y * v277.y;
					}

					arg0b = sqrt(arg0b);
					a2.x = this->field_E4.x;
					a2.y = this->field_E4.y;
					a2.z = this->field_E4.z;
					if (this->AtelBuffer == nullptr	|| this->AtelBuffer->GetSomeAtelBuff() == nullptr) 
					{
						if (arg0b >= 0.01f) {
							float w = fabs(v277.z);
							float v102 = fabs(v277.x);
							if (w > 0.01f || v102 > 0.01f) {
								v28 = true;
								a2.y = -atan2(v277.z, v277.x);
							}
						}
					}
				}

				float arg0b = 0.125;
				if (Placeholder::IsEvent(255) == true) {
					int something = this->VirtActor69();
					if (something != 0)
						arg0b *= something / 100.f;
				}
				double v108 = CYyDb::g_pCYyDb->CheckTick();
				D3DXVECTOR4 v278{};
				while (v108 > 0.0) {
					v278.x = a2.x - this->field_61C.x;
					v278.y = a2.y - this->field_61C.y;
					v278.z = a2.z - this->field_61C.z;
					Globals::RotClamp(&v278);
					v278.x *= arg0b;
					v278.y *= arg0b;
					v278.z *= arg0b;
					this->field_61C += v278;
					v108 -= 1.0;
				}

				Globals::RotClamp(&this->field_61C);
				if (v28 == true && CXiControlActor::is_first_person_view == false) {
					this->field_44 = this->field_61C;
					Globals::RotClamp(&this->field_44);
				}
			}
		}

		//137
		int v163 = -1;
		if (this->field_7A4 != -1) {
			//sub //TODO this block
			exit(0x100C4FF1);
			CXiActor* act = this->field_7A8.GetSearchActor();
			if (this == act || act == nullptr) {
				this->field_7A4 = -1;
			}
			else {
				CXiSkeletonActor* skac = (CXiSkeletonActor*)act;
				if (this->field_7A4 < 48
					|| this->field_7A4 >= 61) {
					D3DXVECTOR4 a1{};
					skac->VirtActor103(this->field_7A4, &a1);
				}
				else {
					//sub //TODO
				}
			}
		}

		float v180 = sqrt(v277.x * v277.x + v277.y * v277.y + v277.z * v277.z);
		if (v180 < 0.01f) {
			D3DXVECTOR4* v181 = this->GetPos();
			v181->x += v277.x;
			v181->y += v277.y;
			v181->z += v277.z;
			//sub //TODO need to look into this
			v277.x = 0.0;
			v277.y = 0.0;
			v277.z = 0.0;
		}

		D3DXVECTOR4* v182 = this->GetPos();
		v182->x += v277.x;
		v182->y += v277.y;
		v182->z += v277.z;

		if (this->field_B4 != nullptr
		&& CMoResourceMng::IsResourceReady((CMoResource***) &this->field_B4) == true) 
		{
			CXiSkeletonActor* some_actor = nullptr;
			if (this->field_80 != 0) {
				if (this->field_103 != 0
				|| this->field_104 != 0
				|| this->field_101 != 0
				|| this->VirtActor209() == true
				|| this->VirtActor219() == true
				|| this->VirtActor223() == true) {
					this->DefaultMove((D3DXVECTOR3*)& v277);
				}
				else if (this->field_102 == 0) {
					if (this->VirtActor201() == false
					&& this->VirtActor204() == false
					&& this->IsFishingRod() == false
					&& this->VirtActor209() == false
					&& this->VirtActor212() == false
					&& this->VirtActor215() == false
					&& this->VirtActor219() == false
					&& this->VirtActor223() == false) {
						if (this->IsOnChocobo() == true) {
							//ChocoboProc
							exit(0x100C5335);
						}
						else if (this->IsOnMount() == true) {
							//Probably MountProc
							exit(0x100C5359);
						}
						else {
							FFXI::Constants::Enums::GAME_STATUS status = this->GetGameStatus();
							if (
								status == FFXI::Constants::Enums::GAME_STATUS::FISHING
							|| status == FFXI::Constants::Enums::GAME_STATUS::FISHING1
							|| status == FFXI::Constants::Enums::GAME_STATUS::FISHING2
							|| status == FFXI::Constants::Enums::GAME_STATUS::FISHING3
							|| status == FFXI::Constants::Enums::GAME_STATUS::FISHING4
							|| status == FFXI::Constants::Enums::GAME_STATUS::FISHING5
							|| status == FFXI::Constants::Enums::GAME_STATUS::FISHING6
							|| status == FFXI::Constants::Enums::GAME_STATUS::FISH_2
							|| status == FFXI::Constants::Enums::GAME_STATUS::FISHF
							|| status == FFXI::Constants::Enums::GAME_STATUS::FISHR
							|| status == FFXI::Constants::Enums::GAME_STATUS::FISHL
							|| status == FFXI::Constants::Enums::GAME_STATUS::FISH_3
							|| status == FFXI::Constants::Enums::GAME_STATUS::FISH_31
							|| status == FFXI::Constants::Enums::GAME_STATUS::FISH_32
							|| status == FFXI::Constants::Enums::GAME_STATUS::FISH_33
							|| status == FFXI::Constants::Enums::GAME_STATUS::FISH_34
							|| status == FFXI::Constants::Enums::GAME_STATUS::FISH_35
							|| status == FFXI::Constants::Enums::GAME_STATUS::FISH_36) 
							{
								//Fishing Proc
								//v183 = (int *)XiSkeletonActor::FishingProc((char *)this, 0, (int)this);
								exit(0x100C548A);
							}
							else {
								if (this->IsOnChair() == true) {
									//chairproc?
									exit(0x100C547C);
								}
							}
						}
					}
				}
				else {
					exit(0x100C554C);
					//sub_100C8950
					//BlowBackProc
				}
			}
			else if (this->VirtActor67() != 0) {
				if (this->field_102 != 0) {
					exit(0x100C554C);
				}
				else if (this->field_101 != 0
					|| this->VirtActor209() == true
					|| this->VirtActor219() == true) {
					this->DefaultMove((D3DXVECTOR3*)&v277);
				}
			}
			else if (this->field_102 == 0) 
			{
				if ( this->field_101 == 0
				&& this->VirtActor201() == false
				&& this->VirtActor204() == false
				&& this->IsFishingRod() == false
				&& this->VirtActor209() == false
				&& this->VirtActor212() == false
				&& this->VirtActor215() == false
				&& this->VirtActor219() == false
				&& this->VirtActor223() == false
				&& this->VirtActor75() == false) {
					this->DefaultMove((D3DXVECTOR3*)&v277);
					this->Thing1((D3DXVECTOR3*)&v277);
					this->Thing2((D3DXVECTOR3*)&v277);
					this->Thing3((D3DXVECTOR3*)&v277);
					this->Thing4((D3DXVECTOR3*)&v277);
					FFXI::Constants::Enums::GAME_STATUS game_status = this->GetGameStatus();
					if (this->IsOnChocobo() == true) {
						//ChocoboProc
						//v183 = sub_100C6F60(v186, 0, (int)this, (int)&v277.y);
						exit(0x100C5330);
					}
					else if (this->IsOnMount() == true) {
						//Probably MountProc
						//v183 = (int*)sub_100C73C0((char*)this, 0, (int)this, (int)&v277.y);
						exit(0x100C5354);
					}
					else if (
						game_status == FFXI::Constants::Enums::GAME_STATUS::FISHING
						|| game_status == FFXI::Constants::Enums::GAME_STATUS::FISHING1
						|| game_status == FFXI::Constants::Enums::GAME_STATUS::FISHING2
						|| game_status == FFXI::Constants::Enums::GAME_STATUS::FISHING3
						|| game_status == FFXI::Constants::Enums::GAME_STATUS::FISHING4
						|| game_status == FFXI::Constants::Enums::GAME_STATUS::FISHING5
						|| game_status == FFXI::Constants::Enums::GAME_STATUS::FISHING6
						|| game_status == FFXI::Constants::Enums::GAME_STATUS::FISH_2
						|| game_status == FFXI::Constants::Enums::GAME_STATUS::FISHF
						|| game_status == FFXI::Constants::Enums::GAME_STATUS::FISHR
						|| game_status == FFXI::Constants::Enums::GAME_STATUS::FISHL
						|| game_status == FFXI::Constants::Enums::GAME_STATUS::FISH_3
						|| game_status == FFXI::Constants::Enums::GAME_STATUS::FISH_31
						|| game_status == FFXI::Constants::Enums::GAME_STATUS::FISH_32
						|| game_status == FFXI::Constants::Enums::GAME_STATUS::FISH_33
						|| game_status == FFXI::Constants::Enums::GAME_STATUS::FISH_34
						|| game_status == FFXI::Constants::Enums::GAME_STATUS::FISH_35
						|| game_status == FFXI::Constants::Enums::GAME_STATUS::FISH_36)
					{
						//Fishing Proc
						//v183 = (int*)sub_100C8AF0((char*)this, 0, (int)this);
						exit(0x100C548F);
					}
					else if (this->IsOnChair() == true) {
						//v183 = sub_100C9020((char*)this, 0, (int)this);
						exit(0x100C5481);
					}
				}
				else if (this->field_102 != 0) {
					//seems like a mistake, this can never be entered
					//sub_100C8950
					//BlowBackProc
					exit(0x100C554C);
				}
				else if (this->field_101 != 0
					|| this->VirtActor209() == true
					|| this->VirtActor219() == true)
				{
					this->DefaultMove((D3DXVECTOR3*)&v277);
				}
			}
			else {
				//sub_100C8950
				//BlowBackProc
				exit(0x100C554C);
			}

			if (this->field_8B4 != 0) 
			{
				FFXI::Constants::Enums::GAME_STATUS game_status = this->GetGameStatus();
				switch (game_status) 
				{
				case FFXI::Constants::Enums::GAME_STATUS::N_IDLE:
				case FFXI::Constants::Enums::GAME_STATUS::B_IDLE:
				case FFXI::Constants::Enums::GAME_STATUS::EVT:
				case FFXI::Constants::Enums::GAME_STATUS::FISHING:
				case FFXI::Constants::Enums::GAME_STATUS::ES2:
				case FFXI::Constants::Enums::GAME_STATUS::ES5:
				case FFXI::Constants::Enums::GAME_STATUS::FISHING1:
				case FFXI::Constants::Enums::GAME_STATUS::FISHING2:
				case FFXI::Constants::Enums::GAME_STATUS::FISHING3:
				case FFXI::Constants::Enums::GAME_STATUS::FISHING4:
				case FFXI::Constants::Enums::GAME_STATUS::FISHING5:
				case FFXI::Constants::Enums::GAME_STATUS::FISHING6:
				case FFXI::Constants::Enums::GAME_STATUS::SIT:
				case FFXI::Constants::Enums::GAME_STATUS::ICRYSTALL:
				case FFXI::Constants::Enums::GAME_STATUS::FISH_2:
				case FFXI::Constants::Enums::GAME_STATUS::FISHF:
				case FFXI::Constants::Enums::GAME_STATUS::FISHR:
				case FFXI::Constants::Enums::GAME_STATUS::FISHL:
				case FFXI::Constants::Enums::GAME_STATUS::FISH_3:
				case FFXI::Constants::Enums::GAME_STATUS::FISH_31:
				case FFXI::Constants::Enums::GAME_STATUS::FISH_32:
				case FFXI::Constants::Enums::GAME_STATUS::FISH_33:
				case FFXI::Constants::Enums::GAME_STATUS::FISH_34:
				case FFXI::Constants::Enums::GAME_STATUS::FISH_35:
				case FFXI::Constants::Enums::GAME_STATUS::FISH_36:
				case FFXI::Constants::Enums::GAME_STATUS::CHAIR00:
				case FFXI::Constants::Enums::GAME_STATUS::CHAIR01:
				case FFXI::Constants::Enums::GAME_STATUS::CHAIR02:
				case FFXI::Constants::Enums::GAME_STATUS::CHAIR03:
				case FFXI::Constants::Enums::GAME_STATUS::CHAIR04:
				case FFXI::Constants::Enums::GAME_STATUS::CHAIR05:
				case FFXI::Constants::Enums::GAME_STATUS::CHAIR06:
				case FFXI::Constants::Enums::GAME_STATUS::CHAIR07:
				case FFXI::Constants::Enums::GAME_STATUS::CHAIR08:
				case FFXI::Constants::Enums::GAME_STATUS::CHAIR09:
				case FFXI::Constants::Enums::GAME_STATUS::CHAIR10:
				case FFXI::Constants::Enums::GAME_STATUS::CHAIR11:
				case FFXI::Constants::Enums::GAME_STATUS::CHAIR12:
				case FFXI::Constants::Enums::GAME_STATUS::CHAIR13:
				case FFXI::Constants::Enums::GAME_STATUS::CHAIR14:
				case FFXI::Constants::Enums::GAME_STATUS::CHAIR15:
				case FFXI::Constants::Enums::GAME_STATUS::CHAIR16:
				case FFXI::Constants::Enums::GAME_STATUS::CHAIR17:
				case FFXI::Constants::Enums::GAME_STATUS::CHAIR18:
				case FFXI::Constants::Enums::GAME_STATUS::CHAIR19:
				case FFXI::Constants::Enums::GAME_STATUS::CHAIR20:
					this->BlinkEyeProc();
					break;
				default:
					if (this->IsOnChocobo() == true
						|| this->IsOnMount() == true)
					{
						this->BlinkEyeProc();
					}
					break;
				}
			}

			this->WithMotion(some_actor);
			this->WithMotion2(this->field_798);
		}

		this->Model.field_34.x = v277.x;
		this->Model.field_34.y = v277.y;
		this->Model.field_34.z = v277.z;

		if (this->field_B4 != nullptr && CMoResourceMng::IsResourceReady((CMoResource***) &this->field_B4) == true) {
			FFXI::Constants::Enums::GAME_STATUS game_status = this->GetGameStatus();
			if (game_status == FFXI::Constants::Enums::GAME_STATUS::ES2
			|| game_status == FFXI::Constants::Enums::GAME_STATUS::ES5
			|| game_status == FFXI::Constants::Enums::GAME_STATUS::N_IDLE
			|| game_status == FFXI::Constants::Enums::GAME_STATUS::B_IDLE
			|| game_status == FFXI::Constants::Enums::GAME_STATUS::EVT
			|| this->IsOnChocobo() == true
			|| this->IsOnMount() == true) {
				this->PlayFootsteps();
			}
		}

		if (this->IsReadComplete() == true) {
			if ((this->IsOnChocobo() == false && this->IsOnMount() == false)
			|| (this->field_768[0] != nullptr && this->field_768[0]->IsReadComplete() == true)) {
				float some_time = this->field_59C + CYyDb::g_pCYyDb->CheckTick() / 32.0f;
				if (some_time > 1.0) {
					some_time = 1.0;
				}
				this->field_59C = some_time;
			}
		}

		FFXI::CYy::CDx::instance->SetTransform(D3DTS_WORLD, &Globals::g_D3DTransformMatrix);
		float cyydb_field_2DC = CYyDb::g_pCYyDb->field_2DC;
		
		float some_float = this->VirtActor58();
		D3DXVECTOR3 scale_vector = { 0.0f, 0.0f, 0.0f };
		if (some_float >= 0.0f) {
			scale_vector.x = some_float;
		}
		else if (this->field_760 >= 0.0f) {
			scale_vector.x = this->field_760;
		}
		else {
			scale_vector.x = this->field_754;
		}

		if (some_float >= 0.0f) {
			scale_vector.y = some_float;
		}
		else if (this->field_75C >= 0.0f) {
			scale_vector.y = this->field_75C;
		}
		else {
			scale_vector.y = this->field_754;
		}

		if (some_float >= 0.0f) {
			scale_vector.z = some_float;
		}
		else if (this->field_758 >= 0.0f) {
			scale_vector.z = this->field_758;
		}
		else {
			scale_vector.z = this->field_754;
		}

		FFXI::Math::WMatrix v293{};
		v293.Scale3(&scale_vector);

		float rotate_Y = this->VirtActor102()->y + FFXI::Constants::Values::ANGLE_3PI_OVER_2;
		if (rotate_Y > FFXI::Constants::Values::ANGLE_PI) {
			rotate_Y -= FFXI::Constants::Values::ANGLE_2PI;
		}
		if (rotate_Y < FFXI::Constants::Values::ANGLE_MINUS_PI) {
			rotate_Y += FFXI::Constants::Values::ANGLE_2PI;
		}
		v293.RotateY(rotate_Y);

		D3DXVECTOR4* translation = this->GetPos();
		v293.AddTranslation3((D3DXVECTOR3*)translation);

		D3DXVECTOR3 v288{};
		Math::BoundingBox3D* boundingBox = this->GetBoundingBoxByState(false);
		if (boundingBox == nullptr) {
			this->field_B0 = 1;
			this->field_B2 |= 1;
			FFXI::Math::WMatrix v290{};
			CYy::CDx::instance->GetTransform(D3DTS_VIEW, &v290);
			v290.Vec3TransformDrop4(&v288, (D3DXVECTOR3*)&this->field_5FC);
		}
		else {
			float v219{}, v220{}, v221{}, v222{};
			v219 = boundingBox->Top;
			v220 = boundingBox->Bottom;
			v221 = boundingBox->Right;
			v222 = boundingBox->Left;
			if (v219 - v220 == 0.0) {
				v219 -= 1.0;
				v220 += 1.0;
			}
			if (v221 - v222 == 0.0) {
				v221 -= 1.0;
				v222 += 1.0;
			}
			
			this->field_918[0].x = v221;
			this->field_918[0].y = v220;
			this->field_918[0].z = boundingBox->Front;
			this->field_918[1].x = v222;
			this->field_918[1].y = v220;
			this->field_918[1].z = boundingBox->Front;
			this->field_918[2].z = boundingBox->Back;
			this->field_918[2].x = -v221;
			this->field_918[2].y = v220;
			this->field_918[3].x = -v222;
			this->field_918[3].y = v220;
			this->field_918[3].z = boundingBox->Back;
			this->field_918[4].x = -v221;
			this->field_918[4].y = v219;
			this->field_918[4].z = boundingBox->Front;
			this->field_918[5].x = -v222;
			this->field_918[5].y = v219;
			this->field_918[5].z = boundingBox->Front;
			this->field_918[6].x = v221;
			this->field_918[6].y = v219;
			this->field_918[6].z = boundingBox->Back;
			this->field_918[7].x = v222;
			this->field_918[7].y = v219;
			this->field_918[7].z = boundingBox->Back;

			this->field_8F8 = { 1000.0, 1000.0, 1000.0, 1.0 };
			this->field_908 = { -1000.0, -1000.0, -1000.0, 1.0 };

			FFXI::Math::WMatrix temp_view{}, temp_proj{};
			FFXI::CYy::CDx::instance->GetTransform(D3DTS_VIEW, &temp_view);
			FFXI::CYy::CDx::instance->GetTransform(D3DTS_PROJECTION, &temp_proj);

			float v273 = 1.0;
			const int array_size = sizeof(this->field_918) / sizeof(this->field_918[0]);
			D3DXVECTOR4 v294[array_size], v295[array_size];
			for (int i = 0; i < array_size; ++i) {
				v293.Vec3TransformDrop4Self((D3DXVECTOR3*)(this->field_918 + i));
				this->field_918[i].w = 1.0;
				temp_view.TransformVector(v294 + i, this->field_918 + i);
				if (v273 > v294[i].z) {
					v273 = v294[i].z;
				}
			}
			int counter = 0;
			bool v231 = false;
			for (int i = 0; i < array_size; ++i) {
				if (v294[i].z > cyydb_field_2DC) {
					counter += 1;
				}
				if (-CYyDb::g_pCYyDb->field_154._22 < v294[i].z) {
					v294[i].z = -CYyDb::g_pCYyDb->field_154._22;
					v231 = true;
				}
				temp_proj.Vec4MultiplyAndScale(v295 + i, v294 + i);
				CYy::CDx::instance->AdjustVec4ByViewport(v295 + i);
				float* v238 = &v295[i].x;
				float* v239 = &this->field_908.x;
				float* v240 = &this->field_8F8.x;
				for (int j = 0; j < 4; ++j) {
					if (v240[j] > v238[j]) {
						v240[j] = v238[j];
					}
					if (v239[j] < v238[j]) {
						v239[j] = v238[j];
					}
				}
			}
			if (counter != 8) {
				if (v231 == true) {
					this->field_8F8.z = 0.01f;
				}
				v288.z = v273;
			}
			else if (virt_72_result == false) {
				this->field_B0 = 1;
				this->field_B2 |= 1;
				FFXI::Math::WMatrix v290{};
				CYy::CDx::instance->GetTransform(D3DTS_VIEW, &v290);
				v290.Vec3TransformDrop4(&v288, (D3DXVECTOR3*)&this->field_5FC);
			}
		}

		this->field_8C = -v288.z;
		this->field_BC = -v288.z;

		if (this->AmIControlActor() == true
		&& CXiControlActor::is_first_person_view == true
		|| virt_72_result == true) {
			this->field_B2 &= ~1u;
			this->field_8C = 0.1f;
			this->field_BC = 0.1f;
		}
		else if (this->field_B2 == 0) {
			//why and zero
			this->field_B2 &= ~1u;
			if (FFXI::CYy::XiZone::zone->WorldEnvironment != nullptr) {
				if (XiZone::zone->GetDrawDistance(true) < -v288.z) {
					this->field_B0 = 1;
					this->field_B2 |= 1;
				}
			}
		}
		
		if ((this->AmIControlActor() == true && CXiControlActor::is_first_person_view == false)
		|| (CXiControlActor::is_first_person_view == false
		|| (CXiActor::control_actor.GetSearchActor() != this->field_768[0]
		|| this->VirtActor201() == false && this->VirtActor204() == false)
		&& (CXiActor::control_actor.GetSearchActor() != this->field_768[0]
		|| this->VirtActor209() == false)))
		{
			this->field_664 = this->CalcAlphaByDistance();
			if (this->field_59C == 0.0)
			{
				this->field_B0 = 1;
				this->field_B2 |= 1;
			}
		}
		else {
			float v257 = this->field_59C - CYyDb::g_pCYyDb->CheckTick() / 16.0f;
			if (v257 < 0.0f) {
				v257 = 0.0f;
			}
			this->field_59C = v257;
		}

		if (this->AtelBuffer != nullptr && (this->AtelBuffer->field_12C & 0x4000000) != 0) {
			this->field_664 = 1.0;
		}
		
		if (this->field_B2 != 0) {
			this->field_8C8.field_C = 0;
			this->field_8E8 = 1;
		}
		else {
			CMoResource** model_res = this->Model.SubStruct5.GetResource();
			if (this->field_59C * this->field_664 >= 1.0
				&& this->MaybeIsDistortion() == false
				&& (model_res == nullptr || *model_res == nullptr))
			{
				this->field_8C8.field_C = 0;
				this->field_8E8 = 1;
			}
			else {
				D3DXVECTOR4 v284{};
				CYy::CDx::instance->GetTransVec4(&v284, &this->field_5FC);
				this->field_8C8.field_0 = v284.z * 512.0f + this->field_8EC;
				if (this->field_8C8.field_0 < 0.0) {
					this->field_8C8.field_0 = 0.0;
				}
				this->field_8C8.field_C = this;
				this->field_8C8.Callback = CXiSkeletonActor::OTCallback;
				if (Globals::CheckFloat(this->field_8C8.field_0) == true) {
					CMoDx::OT->Insert(&this->field_8C8);
				}
				else {
					this->field_8C8.field_C = nullptr;
					this->field_8E8 = 1;
				}
			}
		}

		if (this->AtelBuffer != nullptr
			&& (this->AtelBuffer->IndexOver700() == true && (this->AtelBuffer->field_12C & 0x40000000) == 0
				|| this->CheckAtel120Bit5() == true
				|| (this->AtelBuffer->field_12C & 0x400000) != 0))
		{
			this->SetConstrain(1, 0);
		}

		this->Model.MaybeUpdateAnims(this);
		
		if (this->AmIControlActor() == true) {
			if (this->IsOnChocobo() == true || this->IsOnMount() == true) {
				exit(0x100C633F);
			}
		}
		
		v287 = *this->GetPos() + this->field_60C;
		v287.w = 1.0;
		this->Model.field_4 = v287;

		v290 = *this->VirtActor102() + this->field_62C;
		v290.w = 1.0;
		this->Model.field_14 = v290;

		float virt58 = this->VirtActor58();
		if (virt58 >= 0.0) {
			this->Model.field_24.x = virt58;
			this->Model.field_24.y = virt58;
			this->Model.field_24.z = virt58;
		}
		else {
			if (this->field_758 >= 0.0) {
				this->Model.field_24.x = this->field_758;
			}
			else {
				this->Model.field_24.x = this->field_754;
			}
			if (this->field_760 >= 0.0) {
				this->Model.field_24.y = this->field_760;
			}
			else {
				this->Model.field_24.y = this->field_754;
			}
			if (this->field_75C >= 0.0) {
				this->Model.field_24.z = this->field_75C;
			}
			else {
				this->Model.field_24.z = this->field_754;
			}
		}

		if (this->field_B2 != 0) {
			this->Model.UpdateBaseDTs();
			CYyModelBase** base = this->Model.GetBase();
			if (*base != nullptr) {
				D3DXVECTOR4 diff4 = v287 - this->field_9D8;
				(*base)->Skeleton.ApplyGlobalTranslation(&diff4, 0);
			}
		}

		if (CYyDb::g_pCYyDb->field_8 != 0 && this->AmIControlActor() == true) {
			CXiControlActor::UserControlCamera(this);
		}
	}

	this->field_9D8 = v287;
	this->field_9D8.w = 1.0;

	return collisionResult;
}

bool FFXI::CYy::CXiSkeletonActor::VirtSkeletonActor247()
{
	return false;
}

bool SetModelCallback(FFXI::CYy::CXiSkeletonActor* a1, FFXI::CYy::CXiSkeletonActorRes* a2) {
	return a1->SetModels(a2->field_44);
}

const unsigned short SomeEquipTable[8] = {
	0x0, 0x3F, 0x75, 0x8F, 0x1D7, 0x1FF, 0x280, 0x2BF
};

void LoadRaceActor(CXiSkeletonActor* actor, int a2) {
	int race = actor->GetRace();
	if (race >= 9 && race < 29 || race >= 37) {
		//race number is wrong
		race = 1;
	}

	ResourceContainer** numfile{ nullptr };
	int index = FFXI::Constants::DatIndices::base_skeleton_no_tab[race];
	FFXI::CYyDb::g_pCYyDb->pCMoResourceMng->LoadNumFile(&numfile, index);
	if (CMoResourceMng::IsResourceReady((CMoResource***)&numfile)) {
		actor->SetFile(numfile, actor->field_9EC);
	}
	else {
		exit(0x100D1C14);
		//sub //TODO
	}

	if (race < sizeof(RaceModelTable) / sizeof(RaceModelTable[0])) {
		const RaceModelTableEntry* racetable = RaceModelTable[race];

		for (int category = 0; category < 9; ++category) {
			if (race >= 0x1D && category > 5)
				break;

			int equipnum = actor->GetEquipNum(category);
			int tableindex = equipnum >> 12;
			if (tableindex != category) {
				tableindex = category;
				equipnum = (category << 12) | (equipnum & 0xFFF);
			}

			//racetable is incomplete
			const RaceModelTableEntry* equiptable = racetable + tableindex;
			if (equiptable->field_0) {
				int total = equiptable->field_24 + equiptable->field_4 + equiptable->field_C
					+ equiptable->field_14 + equiptable->field_1C + equiptable->field_2C;
				if ((equipnum & 0xFFF) >= total) {
					//wrong GRP number
					equipnum = tableindex << 12;
				}

				int equipnum12 = equipnum & 0xFFF;
				if (tableindex == 7) {
					actor->field_8B5 = 1;
					for (int v84 = 0; v84 < 8; v84 += 2) {
						if (equipnum12 < SomeEquipTable[v84] || equipnum12 > SomeEquipTable[v84 + 1]) {
							actor->field_8B5 = 0;
							break;
						}
					}
				}

				if (racetable) {
					int offset = 0;
					int sum = 0;
					if (equipnum12 < equiptable->field_4) {
						offset = equiptable->field_0;
					}
					else if (sum += equiptable->field_4, equipnum12 < sum + equiptable->field_C) {
						offset = equiptable->field_8 - sum;
					}
					else if (sum += equiptable->field_C, equipnum12 < sum + equiptable->field_14) {
						offset = equiptable->field_10 - sum;
					}
					else if (sum += equiptable->field_14, equipnum12 < sum + equiptable->field_1C) {
						offset = equiptable->field_18 - sum;
					}
					else if (sum += equiptable->field_1C, equipnum12 < sum + equiptable->field_24) {
						offset = equiptable->field_20 - sum;
					}
					else if (sum += equiptable->field_24, equipnum12 < sum + equiptable->field_2C) {
						offset = equiptable->field_28 - sum;
					}
					else
						continue;

					int DATIndex = equipnum12 + offset;
					ResourceContainer** equipfile = FFXI::CYyDb::g_pCYyDb->pCMoResourceMng->GetOrLoadDatByIndex(DATIndex);
					if (CMoResourceMng::IsResourceReady((CMoResource***)&equipfile) == true) {
						actor->SetEquipModel(equipfile, equipnum, actor->field_9EC, true);
					}
					else {
						FFXI::File::EquipReferenceRead* refread = new FFXI::File::EquipReferenceRead();
						if (refread) {
							refread->ActorIndex = actor->field_9EC;
							refread->field_14 = 0;
							refread->field_4.SetActor(actor);
							refread->field_10 = 0;
							refread->EquipModelID = equipnum;
							refread->DATIndex = DATIndex;
							ResourceContainer::ReferenceRead(&equipfile, FFXI::File::EquipReferenceRead::ReadCallback, refread, actor, nullptr);
						}
					}
				}
			}
		}
	}

	if (race < 9) {
		if (a2 >= 0) {
			actor->field_8A0 = a2;
			actor->field_8A4 = 4;
		}
		else {
			actor->Negativea2();
		}
	}
}

FFXI::CYy::CXiSkeletonActor::CXiSkeletonActor(int a2)
{
	this->field_734 = 0;
	this->field_750 = 0;
	this->field_744 = 0.1f;
	this->field_740 = 0;
	this->field_73C = 0.0f;
	this->field_74C = 0;
	this->field_730 = 72;
	this->field_738 = 0;

	//sub //TODO

	this->Init();
	this->field_8C4 = a2;

	this->field_5FC = *(D3DXVECTOR4*)&CYyDb::g_pCYyDb->CameraManager->field_50;
	this->field_5FC.w = 1.0;

	this->field_9D8 = this->field_5FC;
	this->Model.field_4 = this->field_5FC;

	//inputctrl stuff //SUB //TODO

	if (this->GetGameStatus() == FFXI::Constants::Enums::GAME_STATUS::B_IDLE) {
		if (this->GetType() == FFXI::Constants::Enums::ActorType::ZERO || this->GetType() == FFXI::Constants::Enums::ActorType::SIX)
			this->field_83C[0] = 1;
	}

	if (this->GetMonsterFlag() == 1) {
		if (this->GetType() == FFXI::Constants::Enums::ActorType::TWO || this->GetType() == FFXI::Constants::Enums::ActorType::SEVEN)
			this->field_83C[0] = 1;
	}

	this->TaskMngShift4(4);
	FFXI::CYyDb::ZoneStartOk();
}

FFXI::CYy::CXiSkeletonActor::CXiSkeletonActor(D3DXVECTOR4* a2, CYy::XiAtelBuff* a3, Constants::Enums::SUBACTOR_STATUS a4)
{
	this->field_734 = 0;
	this->field_750 = 0;
	this->field_744 = 0.1f;
	this->field_740 = 0;
	this->field_73C = 0.0f;
	this->field_74C = 0;
	this->field_730 = 72;
	this->field_738 = 0;

	this->field_88 |= 8;
	this->Init();
	this->SubActorStatus = a4;
	
	if (a3->Actor == nullptr) {
		return;
	}

	exit(0x100C3BCF);
}

FFXI::CYy::CXiSkeletonActor::CXiSkeletonActor(D3DXVECTOR4* a2, CYy::XiAtelBuff* a3, unsigned int a4)
{
	this->field_734 = 0;
	this->field_750 = 0;
	this->field_744 = 0.1f;
	this->field_740 = 0;
	this->field_73C = 0.0f;
	this->field_74C = 0;
	this->field_730 = 72;
	this->field_738 = 0;

	this->Init();
	if (a4 == 'pops' && a3->ActorType != FFXI::Constants::Enums::ActorType::ZERO) {
		this->field_660 = 0x808080;
	}

	this->field_8C4 = a4;
	this->field_5FC = *a2;
	this->field_5FC.w = 1.0;
	this->field_9D8 = this->field_5FC;
	this->Model.field_4 = this->field_5FC;
	this->AtelBuffer = a3;
	this->SetUp(-1);

	//keyeventrecvcallback
	//TODO
	if (this->GetGameStatus() == FFXI::Constants::Enums::GAME_STATUS::B_IDLE && (this->GetType() == FFXI::Constants::Enums::ActorType::ZERO || this->GetType() == FFXI::Constants::Enums::ActorType::SIX)
		|| this->GetMonsterFlag() && (this->GetType() == FFXI::Constants::Enums::ActorType::TWO || this->GetType() == FFXI::Constants::Enums::ActorType::SEVEN)) {
		this->field_83C[0] = 1;
	}
	this->TaskMngShift4(4);
	FFXI::CYyDb::ZoneStartOk();
}

FFXI::CYy::CXiSkeletonActor::CXiSkeletonActor(D3DXVECTOR4* a2, CYy::XiAtelBuff* a3, int a4, unsigned int a5)
{
	this->field_734 = 0;
	this->field_750 = 0;
	this->field_744 = 0.1f;
	this->field_740 = 0;
	this->field_73C = 0.0f;
	this->field_74C = 0;
	this->field_730 = 72;
	this->field_738 = 0;
	this->Init();

	this->field_8C4 = a5;
	this->field_5FC = *a2;
	this->field_5FC.w = 1.0;
	this->field_9D8 = this->field_5FC;
	this->Model.field_4 = this->field_5FC;
	this->AtelBuffer = a3;
	this->SetUp(a4);

	//keyeventrecvcallback
	//TODO
	this->TaskMngShift4(4);
	FFXI::CYyDb::ZoneStartOk();

}

FFXI::CYy::CXiSkeletonActor::~CXiSkeletonActor()
{
	if (this->field_764 != 0) {
		exit(0x100C3CA7);
	}

	if (this->field_814 != 0) {
		exit(0x100C3CD7);
	}

	this->DeleteAttachments();
	this->DeleteAllResp();

	if (this->field_864 != nullptr && *this->field_864 != nullptr) {
		(*this->field_864)->DecrementReferenceCount();
		FFXI::CYyDb::pCMoResourceMng->Unlink((CMoResource**)this->field_864);
	}

	if (Placeholder::g_pTkInputCtrl != nullptr) {
		exit(0x100C3D50);
	}

	if (CXiActor::some_count1[this->field_9F0] != 0) {
		CXiActor::some_count1[this->field_9F0] -= 1;
	}

	if (CXiActor::some_count2[this->field_9FC] != 0) {
		CXiActor::some_count2[this->field_9FC] -= 1;
	}

	if (this->field_A08 != nullptr) {
		//leftoff here
		delete this->field_A08;
		this->field_A08 = nullptr;
	}
}

void FFXI::CYy::CXiSkeletonActor::SetUp(int a2)
{
	bool v3{ false };
	if (this->AtelBuffer != nullptr) {
		if ((this->AtelBuffer->field_120 & 0x80) != 0
			|| (this->AtelBuffer->field_120 & 0x100) != 0) {
			v3 = true;
		}
	}

	if (this->AmIControlActor() == true || v3 == true) {
		this->SetModels(a2);
	}
	else {
		char* mem = MemoryPoolManager::globalInstance->Get(sizeof(CXiSkeletonActorRes), MemoryPoolManager::MemoryPoolType::Ex);
		if (mem) {
			CMoTaskMng::DeleteThisTask = true;
			this->field_A08 = new (mem) CXiSkeletonActorRes(this, SetModelCallback);
			this->field_A08->field_44 = a2;
		}
		else {
			this->field_A08 = nullptr;
			this->SetModels(a2);
		}
	}
}

void FFXI::CYy::CXiSkeletonActor::Init()
{
	this->field_5F8 = 0;
	this->field_764 = 0;
	memset(this->field_768, 0, sizeof(this->field_768));
	this->field_798 = nullptr;
	this->SetMotionLock(1);
	this->field_840 &= ~0x800000u;
	this->field_838 = 0;

	this->SetConstrain(0, 0);
	this->SetConstrain(0, 1);
	this->VirtActor191(0, 0);
	this->VirtActor191(0, 1);

	this->field_7F8 = 0x20202020;
	this->field_7FC = 0;
	this->field_800 = 0;
	this->field_804 = 0x20202020;
	this->field_80C = 0x20202020;
	this->field_810 = 0;
	this->field_808 = 0;
	this->field_814 = 0;
	this->field_818 = 0;

	this->field_840 &= 0xFC;
	this->field_848 = { 20.0, 0.0, 0.0, -1.0 };
	this->field_82C = { 0.0, -1.0, 0.0 };
	this->field_828 = 0;

	this->field_61C = { 0.0, 0.0, 0.0, 1.0 };
	this->field_5FC = { 0.0, 0.0, 0.0, 1.0 };
	this->field_44 = this->field_5FC;
	Globals::RotClamp(&this->field_44);

	this->field_9D8 = { 0.0, 0.0, 0.0, 1.0 };
	this->field_60C = { 0.0, 0.0, 0.0, 1.0 };
	this->field_62C = { 0.0, 0.0, 0.0, 1.0 };

	this->field_840 &= 0xFF8FFFFF;

	this->field_758 = -1.0;
	this->field_75C = -1.0;
	this->field_760 = -1.0;
	this->field_65C = 0;
	this->field_754 = 1.0;
	this->field_660 = 0x80808080;
	this->field_664 = 1.0;
	this->field_59C = 0;
	this->field_668 = 128;
	this->field_66C = 128;
	this->field_670 = 0;
	this->field_8C8.field_C = 0;
	this->field_8E8 = 1;
	this->field_8EC = 0;
	this->field_7A0 = 0;
	this->field_7E8 = 0;
	this->field_7EC = 0;
	this->field_7F0 = 0;
	this->field_7F4 = 0;
	this->Blink_Timer = 120.0;
	this->field_9E8 = 0;
	this->field_81C = 0;
	this->field_820 = 0;
	this->field_8B5 = 0;

	this->SetSomeFlag(0);
	this->field_824 = 0;

	strcpy_s((char*)this->field_7C8, sizeof(this->field_7C8), FFXI::Constants::Strings::SkeletonAnimations);
	this->field_7C8[4] = 0;
	this->field_7DC = 1.0;
	this->field_7E0 = 1.0;
	this->field_8C4 = 0;
	this->field_7C4 = 0;
	this->field_7E4 = nullptr;
	this->field_8A0 = 0;
	this->field_8A4 = 0;
	this->VirtActor32(8u);
	this->VirtActor34(2u);
	this->VirtActor36(2u);
	this->field_8C0 = 0;
	this->VirtActor38(0x80u);
	this->VirtActor40(0x80u);
	this->field_8B0 = 0;
	this->field_8B4 = 1;
	this->field_8AA = 0;
	this->field_8AB = 0;
	this->SetModelWep(0);
	this->field_8AC &= 0xF8;
	this->field_8BC = 0;
	this->field_8B6 = 0;
	this->field_86C = 0;
	this->field_870 = 0;
	this->field_874 = 0;
	this->veve_texinfo = 0;
	this->null_maxresource = 0;
	this->ohno_texinfo = 0;
	this->cubemap_spec = 0;
	this->field_864 = 0;
	this->field_840 = this->field_840 & 0xF9F00003 | 0x7FF00;
	this->field_844 = 0;
	this->field_88 &= 0xFFFFF9FF;
	this->field_B2 = 1;
	this->field_B0 = 1;
	this->field_7A4 = -1;

	this->field_7A8.SetActor(0);
	this->field_7B4 = { 0.0, 0.0, 0.0, 1.0 };

	this->VirtActor11();
	this->field_9EC = CXiActor::skeletonActorIndex++;
	this->field_9F4 = 1;
	const int count1_size = sizeof(CXiActor::some_count1) / sizeof(CXiActor::some_count1[0]);
	const int count2_size = sizeof(CXiActor::some_count1) / sizeof(CXiActor::some_count1[0]);
	int count1_index = 0;
	int count2_index = 0;
	for (int i = 0; i < count1_size; ++i) {
		if (CXiActor::some_count1[count1_index] > CXiActor::some_count1[i]) {
			count1_index = i;
		}
	}
	for (int i = 0; i < count2_size; ++i) {
		if (CXiActor::some_count2[count2_index] > CXiActor::some_count2[i]) {
			count2_index = i;
		}
	}

	this->field_9F0 = count1_index;
	this->field_9FC = count2_index;
	some_count1[count1_index] += 1;
	some_count2[count2_index] += 1;
	this->field_9F8 = 1;
	this->field_A04 = 1;
	this->field_A00 = 0;
	this->field_A08 = nullptr;
}

void FFXI::CYy::CXiSkeletonActor::SetModelWep(int a2)
{
	CYyModelBase* base = *this->Model.GetBase();
	if (base == nullptr) return;

	//throw "not implemented";
	exit(0x100D41C0);
}

void FFXI::CYy::CXiSkeletonActor::SetEquipModel(ResourceContainer** a2, int p_equipModelID, int p_actorIndex, bool a5)
{
	int v5 = (p_equipModelID >> 12) & 0xF;
	if (a5 == false) {
		this->field_8B0 |= 1 << v5;
		return;
	}

	if (a2 == nullptr) return;
	if (p_actorIndex != this->field_9EC) return;

	if (this->GetType() == FFXI::Constants::Enums::ActorType::SIX) {
		if (v5 == 9) {
			if (this->GetRace() == 0) {
				this->VirtActor154(a2);
			}
			else {
				this->field_864 = a2;
				(*a2)->IncrementReferenceCount();
			}
		}
	}
	else if (this->GetType() == FFXI::Constants::Enums::ActorType::SEVEN) {
		if (v5 == 9) {
			if (this->GetEquipNum(0) == 0) {
				this->VirtActor154(a2);
			}
			else {
				this->field_864 = a2;
				(*a2)->IncrementReferenceCount();
			}
		}
	}

	CYyModelBase* base = *this->Model.GetBase();
	if (base == nullptr) {
		base = this->Model.CreateBase();
		if (base == nullptr)
			return;
	}

	this->AddOS2s((CMoResource**)a2, base);

	CMoCib** pcib{ nullptr };
	CYyDb::g_pCYyDb->pCMoResourceMng->FindNextUnder((CMoResource***)&pcib, *a2, FFXI::Constants::Enums::ResourceType::Cib, 0);
	if (pcib && *pcib) {
		CMoCib* cib = *pcib;
		switch (v5) {
		case 2:
			this->CibCollect.waist_type = cib->info.waist_type;
			break;
		case 5:
			this->CibCollect.material_foot = cib->info.material_foot;
			this->CibCollect.size_foot = cib->info.size_foot;
			break;
		case 6:
			this->CibCollect.amot_wep0 = cib->info.amot_wep0;
			this->CibCollect.inout_wep0 = cib->info.inout_wep0;
			this->CibCollect.constrain_no = cib->info.constrain_no;
			this->CibCollect.atksch_wep = cib->info.atksch_wep;
			this->CibCollect.field_13 = cib->info.legs4;
			break;
		case 7:
			this->CibCollect.inout_wep1 = cib->info.inout_wep0;
			this->CibCollect.field_10 = cib->info.constrain_no;
			this->CibCollect.field_11 = cib->info.atksch_wep;
			if (this->field_8B5 != 0) {
				this->CibCollect.is_shield = 0;
				this->CibCollect.field_12 = cib->info.amot_wep0;
			}
			else {
				this->CibCollect.field_12 = 0;
				this->CibCollect.is_shield = cib->info.is_shield;
			}
			break;
		case 8:
			this->CibCollect.field_E = cib->info.field_E;
			break;
		default:
			break;
		}
	}

	this->field_8B0 |= 1 << v5;
}

bool FFXI::CYy::CXiSkeletonActor::SetModels(int a2)
{
	bool v4{ false };
	if (this->AtelBuffer != nullptr) {
		if ((this->AtelBuffer->field_120 & 0x80) != 0
			|| (this->AtelBuffer->field_120 & 0x100) != 0)
			v4 = true;
	}

	if (this->VirtActor203()) {
		//throw "not implemented";

		int race = this->GetRace();
		if ( (race >= 9 && race < 29) || race >= 37)
			race = 32;
		
		exit(1232);
	}
	
	if (this->VirtActor201()) {
		//throw "not implemented";

		exit(1233);
	}
	
	if (this->VirtActor204()) {
		//throw "not implemented";

		exit(1234);
	}
	
	if (this->IsFishingRod()) {
		//throw "not implemented";

		exit(1235);
	}
	
	if (this->VirtActor209()) {
		exit(1236);
	}
	
	if (this->VirtActor223() || this->VirtActor219()) {
		exit(1237);
	}
	
	if (this->VirtActor215()) {
		exit(1238);
	}

	if (this->VirtActor212()) {
		exit(1239);
	}

	switch (this->GetType()) {
	case FFXI::Constants::Enums::ActorType::ZERO:
	case FFXI::Constants::Enums::ActorType::ONE:
		LoadRaceActor(this, a2);
		return true;
	default:
		exit(1240);
	}
}

void FFXI::CYy::CXiSkeletonActor::Set5FC(D3DXVECTOR4* a2)
{
	if (this->AtelBuffer) {
		this->AtelBuffer->field_24.x = a2->x;
		this->AtelBuffer->field_24.y = a2->y;
		this->AtelBuffer->field_24.z = a2->z;
		this->AtelBuffer->CopyAllSPos();
	}
	this->field_5FC = *a2;
	this->field_5FC.w = 1.0;
}

void FFXI::CYy::CXiSkeletonActor::Set61C(D3DXVECTOR4* a2)
{
	if (this->AtelBuffer) {
		this->AtelBuffer->field_34.x = a2->x;
		this->AtelBuffer->field_34.y = a2->y;
		this->AtelBuffer->field_34.z = a2->z;
		this->AtelBuffer->CopyAllSPos();
	}

	if (this->IsDirectionLock() == false) {
		this->field_61C = *a2;
		Globals::RotClamp(&this->field_61C);
	}
}

void FFXI::CYy::CXiSkeletonActor::SetSomeFlag(char a2)
{
	this->field_840 ^= (this->field_840 ^ ((a2 != 0) << 23)) & 0x800000;
}

void FFXI::CYy::CXiSkeletonActor::SetFile(ResourceContainer** a2, int a3)
{
	if (a3 != this->field_9EC) return;
	if (a2 == nullptr) return;

	this->CXiActor::VirtActor154(a2);
	this->InitFromFile(a2);
}

void FFXI::CYy::CXiSkeletonActor::InitFromFile(ResourceContainer** a2)
{
	CMoSk2** skeletonresource{ nullptr };
	CMoCib** cibresource{ nullptr };
	ResourceContainer* file{ nullptr };
	if (a2)
		file = *a2;
	CYyDb::g_pCYyDb->pCMoResourceMng->FindNextUnder((CMoResource***)&skeletonresource, file, FFXI::Constants::Enums::ResourceType::Sk2, 0);
	if (skeletonresource == nullptr) return;

	CYyModelBase* modelbase = *this->Model.GetBase();
	if (modelbase == nullptr) {
		modelbase = this->Model.CreateBase();
		if (modelbase == nullptr)
			return;
	}

	modelbase->SetSkeleton(skeletonresource);
	(*skeletonresource)->IncrementReferenceCount();

	CYyDb::g_pCYyDb->pCMoResourceMng->FindNextUnder((CMoResource***)&cibresource, *skeletonresource, FFXI::Constants::Enums::ResourceType::Cib, 0);
	if (cibresource && *cibresource) {
		this->CibCollect.scale[0] = (*cibresource)->info.scale[0];
		this->CibCollect.scale[1] = (*cibresource)->info.scale[1];
		this->CibCollect.scale[2] = (*cibresource)->info.scale[2];
		this->CibCollect.scale[3] = (*cibresource)->info.scale[3];
	}

	if (this->VirtActor201() == true || this->VirtActor204() == true) {
		if (this->field_768) {
			//throw "not implemented";
			exit(0x100CDA7E);
		}
	} else if (this->IsFishingRod() == true) {
		if (this->field_768) {
			exit(0x100CD8CF);
		}
	}
	else if (this->VirtActor219() == false) {
		if (this->VirtActor223()) {
			this->field_754 = 0.0;
		}
		else if (this->VirtActor209() == true) {
			exit(0x100CD990);
		}
		else if (this->VirtActor212() == true) {
			exit(0x100CDA61);
		}
		else if (this->field_768[1] != nullptr) {
			exit(0x100CDA27);
		}
	}
	else if (this->field_768[10] != nullptr) {
		exit(0x100CD919);
	}
}

void FFXI::CYy::CXiSkeletonActor::AddOS2s(CMoResource** a2, CYyModelBase* a3)
{
	CMoResource* res{ nullptr };
	if (a2)
		res = *a2;
	//res can be nullptr as per client

	ResourceList reslist{};
	reslist.PrepareFromResource(res, FFXI::Constants::Enums::ResourceType::Os2, 0, -1);
	while (reslist.field_14) {
		CMoResource** prpl = reslist.field_14->Metadata.SelfReference;
		a3->LinkOs2((CMoOs2**)prpl);
		(*prpl)->IncrementReferenceCount();
		reslist.GetNext(false);
	}

	if ((res->TypeSizeFlags & 0x7F) == FFXI::Constants::Enums::ResourceType::Rmp)
		this->CXiActor::StartGenerators(a2);
}

void FFXI::CYy::CXiSkeletonActor::Negativea2()
{
	FFXI::Constants::Enums::GAME_STATUS status = this->GetGameStatus();
	FFXI::Constants::Enums::ActorType mytype = this->GetType();
	bool flag = mytype >= FFXI::Constants::Enums::ActorType::ZERO && (mytype <= FFXI::Constants::Enums::ActorType::ONE || mytype == FFXI::Constants::Enums::ActorType::SIX);
	switch (status) {
	case FFXI::Constants::Enums::GAME_STATUS::B_IDLE:
		if (flag == true) {
			this->field_8A4 = 2;
		}
		break;
	case FFXI::Constants::Enums::GAME_STATUS::N_DEAD:
		if (flag == true) {
			this->field_8A4 = 1;
		}
		this->field_7C8[4] = '    ';
		break;
	case FFXI::Constants::Enums::GAME_STATUS::B_DEAD:
		if (flag == true) {
			if (this->Unknown7E4Func(1) == nullptr) {
				this->field_8A4 = 2;
			}
		}
		this->field_7C8[4] = '    ';
		break;
	case FFXI::Constants::Enums::GAME_STATUS::D_OPEN:
	case FFXI::Constants::Enums::GAME_STATUS::D_CLOSE:
	case FFXI::Constants::Enums::GAME_STATUS::ES0:
	case FFXI::Constants::Enums::GAME_STATUS::ES1:
	case FFXI::Constants::Enums::GAME_STATUS::ES3:
	case FFXI::Constants::Enums::GAME_STATUS::ES4:
	case FFXI::Constants::Enums::GAME_STATUS::EFF0:
	case FFXI::Constants::Enums::GAME_STATUS::EFF1:
	case FFXI::Constants::Enums::GAME_STATUS::EFF2:
	case FFXI::Constants::Enums::GAME_STATUS::EFF3:
	case FFXI::Constants::Enums::GAME_STATUS::D_OPEN2:
	case FFXI::Constants::Enums::GAME_STATUS::D_CLOSE2:
		return;
	case FFXI::Constants::Enums::GAME_STATUS::MANNEQUIN:
		break;
	default:
		if (flag == true) {
			this->field_8A4 = 1;
		}
		break;
	}
}

void* FFXI::CYy::CXiSkeletonActor::Unknown7E4Func(int a2)
{
	if (this->field_7E4 == nullptr)
		return nullptr;

	//sub //TODO
	exit(0x100D25AE);
}

void FFXI::CYy::CXiSkeletonActor::Draw()
{
	D3DXVECTOR3 a1{};
	D3DXVECTOR4 v99{}, v101{};
	bool FlagOne{ false }, FlagTwo{ false }, FlagThree{ false }, FlagFour{ false };

	CYyModelBase* modelbase = *this->Model.GetBase();
	if (modelbase == nullptr) {
		this->field_B0 = 1;
		this->field_B2 |= 1;
		this->SomeCameraUpdate();
		return;
	}

	if (this->AtelBuffer != nullptr) {
		FlagOne = (this->AtelBuffer->field_12C >> 17) & 1;
		FlagTwo = this->AtelBuffer->field_130 & 1;

		if ((this->AtelBuffer->field_120 & 0x80) != 0
			|| (this->AtelBuffer->field_120 & 0x100) != 0)
			FlagThree = true;

		if ((this->AtelBuffer->field_12C & 0x8000000) != 0) {
			if (this->CheckSomeFlag1() == true 
				|| this->CheckSomeFlag2() == true)
				FlagFour = true;
		}
	}

	CXiSkeletonActor::somebool = true;
	CMoOs2::DrawBasicFlagOne = false;
	CMoOs2::DrawBasicFlagTwo = false;
	FFXI::CYy::CDx::instance->SetTransform(D3DTS_WORLD, &Globals::g_D3DTransformMatrix);
	Globals::g_TransformBackup = Globals::g_D3DTransformMatrix;

	this->Tick();
	double v11{ 1.0 };
	if (this->field_664 < 0.80000001) {
		v11 = this->field_664 * 1.25;
	}

	//59C appears to be an alpha for model blending into background
	//this->field_59C = 1.0;	//REMOVE THIS. temp to enter logic below

	v11 *= this->field_59C;
	if (v11 > 1.0)
		v11 = 1.0;

	long long v12 = (long long)(128.0 * v11);
	unsigned int v97 = ((unsigned int)v12 << 24) | this->field_660 & 0xFFFFFF;
	if (v11 >= 0.0001) {
		CYyModel::g_some_short = this->VirtActor238();
		int something = this->VirtActor124();
		if (XiZone::zone->ZoneID == FFXI::Constants::Enums::ZoneNames::AlTaieu
			&& (something == 8 || something == 9)) {
			Globals::g_some_actordraw_short = 3;
		}
		else if (this->VirtActor6() == 0) {
			Globals::g_some_actordraw_short = 3;
		}
		else if (Config::MainRegistryConfig::dword104458F4 == 1) {
			Globals::g_some_actordraw_short = 3;
		}
		else {
			D3DXVECTOR4* act = this->GetPos();
			D3DXVECTOR3* cam = &CYyDb::g_pCYyDb->CameraManager->Position;
			a1.x = act->x - cam->x;
			a1.y = act->y - cam->y;
			a1.z = act->z - cam->z;
			if (a1.y < -2.0) {
				Globals::g_some_actordraw_short = 3;
			}
			else if (Config::MainRegistryConfig::dword104458F4 == 2 && Globals::g_some_actordraw_float < 40.0) {
				Globals::g_some_actordraw_short = 1;
				Globals::g_some_actordraw_float = 0.0;
			}
			else {
				Globals::g_some_actordraw_float = this->field_BC;
				if (this->AmIControlActor() == false) {
					if (this->Model.field_B4 <= 1) {
						Globals::g_some_actordraw_short = 3;
					}
					else if (this->InOwnActorPointers() == true) {
						if (this->VirtActor201() == true
							|| this->VirtActor204() == true) {
							if (this->field_768[0] != 0
								&& this->AmIControlActor() == true) {
								Globals::g_some_actordraw_short = 1;
								Globals::g_some_actordraw_float = 0.0;
							}
							else {
								Globals::g_some_actordraw_short = this->field_17C != 0 ? 2 : 0;
							}
						}
						else if (this->IsFishingRod() == true) {
							if (this->field_768[0] == 0) {
								Globals::g_some_actordraw_short = 3;
							}
							else if (this->AmIControlActor() == true) {
								Globals::g_some_actordraw_short = 1;
								Globals::g_some_actordraw_float = 0.0;
							}
							else {
								Globals::g_some_actordraw_short = 3;
							}
						}
						else if (this->VirtActor219() == true) {
							Globals::g_some_actordraw_short = 3;
						}
						else if (this->VirtActor209() == true) {
							if (this->field_768[0] == 0) {
								Globals::g_some_actordraw_short = 3;
							}
							else if (this->AmIControlActor() == true) {
								Globals::g_some_actordraw_short = 1;
								Globals::g_some_actordraw_float = 0.0;
							}
							else {
								Globals::g_some_actordraw_short = 3;
							}
						}
						else if (this->VirtActor215() == true
							|| this->VirtActor212() == true) {
							Globals::g_some_actordraw_short = 3;
						}
						else if (this->VirtActor223() == true) {
							Globals::g_some_actordraw_short = 3;
						}
						else {
							Globals::g_some_actordraw_short = this->field_17C != 0 ? 2 : 0;
						}
					}
					else {
						if (this->field_768[0] == 0) {
							Globals::g_some_actordraw_short = this->field_17C != 0 ? 2 : 0;
						}
						else if (this->VirtActor201() == true) {
							Globals::g_some_actordraw_short = 3;
						}
						else if (this->VirtActor204() == true) {
							Globals::g_some_actordraw_short = 3;
						}
						else {
							Globals::g_some_actordraw_short = this->field_17C != 0 ? 2 : 0;
						}
					}
				}
				else {
					if (this->field_768[0] == 0) {
						Globals::g_some_actordraw_short = 1;
						Globals::g_some_actordraw_float = 0.0;
					}
					else if (this->VirtActor201() == true) {
						Globals::g_some_actordraw_short = 3;
					}
					else if (this->VirtActor204() == true) {
						Globals::g_some_actordraw_short = 3;
					}
					else {
						Globals::g_some_actordraw_short = 1;
						Globals::g_some_actordraw_float = 0.0;
					}
				}
			}
		}

		static D3DXVECTOR3 static_vec3_1{};
		static D3DXVECTOR3 static_vec3_2{};
		static D3DXVECTOR3 static_vec3_3{};
		static D3DXVECTOR3 static_vec3_4{};

		if (this->VirtSkeletonActor247() == true) {
			a1 = static_vec3_4;
		}
		else if (CXiSkeletonActor::g_light->Type == D3DLIGHT_POINT) {
			D3DXVECTOR4* act = this->GetPos();
			a1.x = CXiSkeletonActor::g_light->Position.x - act->x;
			a1.y = CXiSkeletonActor::g_light->Position.y - act->y;
			a1.z = CXiSkeletonActor::g_light->Position.z - act->z;
		}
		else {
			a1.x = -CXiSkeletonActor::g_light->Direction.x;
			a1.y = -CXiSkeletonActor::g_light->Direction.y;
			a1.z = -CXiSkeletonActor::g_light->Direction.z;
		}

		this->Model.shadowRenderer.Init(&a1);
		D3DXVECTOR4* act101 = this->GetPos();
		static_vec3_2 = { act101->x, act101->y, act101->z };
		static_vec3_1 = static_vec3_2 - this->Model.shadowRenderer.field_14;
		D3DXMatrixLookAtRH(&CYyModel::view_transform, &static_vec3_1, &static_vec3_2, &static_vec3_3);
		D3DXVECTOR3 v107{}, v104{};
		CYyModel::view_transform.Vec3TransformNormal(&v107, (D3DXVECTOR3*)& this->field_918);
		v99 = { v107.x, v107.y, v107.z, 0 };
		v101 = v99;
		for (int i = 1; i < sizeof(this->field_918) / sizeof(this->field_918[0]); ++i) {
			CYyModel::view_transform.Vec3TransformNormal(&v104, (D3DXVECTOR3*)(this->field_918 + i));
			if (v99.x >= v104.x)
				v99.x = v104.x;
			if (v99.y >= v104.y)
				v99.y = v104.y;

			if (v101.x <= v104.x)
				v101.x = v104.x;
			if (v101.y <= v104.y)
				v101.y = v104.y;
		}

		float v36 = v101.x - v99.x;
		if (v36 < 0.0f)
			v36 = -v36;

		float v37 = v101.y - v99.y;
		if (v37 < 0.0f)
			v37 = -v37;

		if (v36 > v37)
			v37 = v36;

		if (Globals::g_some_actordraw_short != 1) {
			v37 *= 5.0f / 9.0f;
		}

		if (v37 < 0.01f)
			v37 = 0.01f;

		ShadowRenderer::g_mss1_float = v37;
		static_vec3_1 = this->Model.shadowRenderer.field_14 * v37 * -12.0f;
		static_vec3_1 += static_vec3_2;
		D3DXMatrixLookAtRH(&CYyModel::view_transform, &static_vec3_1, &static_vec3_2, &static_vec3_3);

		char v24{ 0 };
		if (this->AmIControlActor() == true)
			goto LABEL_155;
		if (FlagOne == true || FlagTwo == true)
			goto LABEL_98;
		if (this->field_9F8 == 0 && FlagThree == false
			&& FFXI::Config::RegistryConfig::g_pOptions->Other.field_48 == 0) {
			if (CYyDb::g_pCYyDb->field_34 % 4 != this->field_9F0) {
				if (this->field_9F4 != 0)
					goto LABEL_98;
				goto LABEL_104;
			}
		}
		if (this->VirtActor204() == true)
			goto LABEL_98;

		this->field_9F8 = 0;
		if (this->field_8F8.z < 0.0)
			goto LABEL_98;

		v99.x = this->field_8F8.x;
		v99.y = this->field_8F8.y;
		v99.z = this->field_908.x;
		v99.w = this->field_908.y;
		{
			float v51 = this->field_908.z - this->field_8F8.z;
			v51 *= 0.25f;
			v51 += this->field_8F8.z;

			this->field_9F4 = this->field_8F0.Update(&v99, v51, 
				(float)CYyDb::g_pCYyDb->GetBackgroundXRes(), 
				(float)CYyDb::g_pCYyDb->GetBackgroundYRes(),
				Globals::g_pSomeTexture1,
				Globals::g_pSomeTexture2,
				1, 1, 0);
		}
		if (this->field_9F4 != 0) {
		LABEL_98:
			v24 = this->field_A04 == 0 && FlagThree == false;
			if (this->VirtActor201() == true) {
				if (this->field_768[0] != 0) {
					if (this->AmIControlActor() == true)
						v24 = 0;
				}
			}

			if (this->VirtActor204() == true)
				goto LABEL_155;
			if (this->IsOnMount() == true|| v24 == 0)
				goto LABEL_155;
			switch (Config::MainRegistryConfig::dword10445A28) {
			case 1:
				[[fallthrough]];
			case 3:
				if (CYyDb::g_pCYyDb->field_34 % 4 != this->field_9FC)
					goto LABEL_155;
				break;
			case 2:
				if ((((unsigned char)this->field_9FC ^ (unsigned char)(CYyDb::g_pCYyDb->field_34 % 4)) & 1) == 0)
					goto LABEL_155;
				break;
			default:
				goto LABEL_155;
			}
			if (this->field_A00 != 0) {
				CXiActor::somebool = false;
				this->field_A00 = 0;
				LABEL_156:
				this->BaseDoSomething();
				this->SetDrawBasicFlags();
				float v63 = this->VirtActor58();
				float v66 = v63;
				float v67 = v63;
				if (v63 < 0.0) {
					if (this->field_760 >= 0.0) {
						v63 = this->field_760;
					}
					else {
						v63 = this->field_754;
					}
					if (this->field_75C >= 0.0) {
						v66 = this->field_75C;
					}
					else {
						v66 = this->field_754;
					}
					if (this->field_758 >= 0.0) {
						v67 = this->field_758;
					}
					else {
						v67 = this->field_754;
					}
				}

				D3DXVECTOR4 scalevec{};
				scalevec.x = v67;
				scalevec.y = v66;
				scalevec.z = v63;
				scalevec.z = 1.0;
				this->field_998.CreateScaling(&scalevec);
				if (this->field_5F8 != nullptr) {
					this->field_998 = *this->field_5F8;
				}
				else {
					bool rotateflag = this->CheckSomeFlag3();
					if (rotateflag == false) {
						int legs4 = this->CibCollect.legs4;
						if (legs4 <= 0 || legs4 == 3) {
							if (this->IsDead() == false)
								rotateflag = true;
						}
					}

					if (rotateflag == true) {
						D3DXVECTOR4* pr = this->VirtActor102();
						D3DXVECTOR4 r = *pr + this->field_62C;
						if (this->SomeEventCheck() == true) {
							this->field_998.RotateX(r.z);
							this->field_998.RotateY(r.y);
							this->field_998.RotateZ(r.x);
						}
						else {
							this->field_998.RotateX(r.x);
							this->field_998.RotateY(r.y);
							this->field_998.RotateZ(r.z);
						}
					}
					else {
						this->SomeMatrixMath(&this->field_998, 0.1875);
					}
					D3DXVECTOR4* a = this->GetPos();
					D3DXVECTOR4 b = *a + this->field_60C;
					b.w = 1.0;
					this->field_998.AddTranslation4(&b);
				}

				D3DXVECTOR4 v99 = this->field_848;
				v99.w = modelbase->field_B0.w;
				if (FlagThree == true
					|| this->AmIControlActor() == true) {
					D3DXVECTOR4* v83 = this->VirtActor102();
					D3DXVECTOR4* v77 = this->GetPos();
					this->Model.Draw(this, &this->field_998, &scalevec, v77, v83, &v99, v97);
					CXiActor::maybeActorDrawCount += 1;
					this->field_B0 = 0;
					this->SomeCameraUpdate();
					return;
				}
				CXiActor::maybeActorDrawCount += 1;
				if (CXiActor::maybeActorDrawCount >= CXiActor::config60
					&& FlagFour == false) {
					CXiSkeletonActor* v74 = (CXiSkeletonActor*)this->field_768[0];
					if (v74 != nullptr
						&& v74->Model.field_30 == 4) {
						D3DXVECTOR4* v83 = this->VirtActor102();
						D3DXVECTOR4* v77 = this->GetPos();
						this->Model.Draw(this, &this->field_998, &scalevec, v77, v83, &v99, v97);
						this->field_B0 = 0;
						this->SomeCameraUpdate();
						return;
					}
					v74 = (CXiSkeletonActor*)this->field_768[1];
					if (v74 == nullptr || v74->Model.field_30 != 4) {
						if (this->Model.field_B4 > 1) {
							this->field_B0 = 0;
							this->SomeCameraUpdate();
							return;
						}
					}
				}
				D3DXVECTOR4* v83 = this->VirtActor102();
				D3DXVECTOR4* v77 = this->GetPos();
				this->Model.Draw(this, &this->field_998, &scalevec, v77, v83, &v99, v97);
				this->field_B0 = 0;
				this->SomeCameraUpdate();
				return;
			}

		LABEL_155:
			this->field_A04 = 0;
			CXiActor::somebool = true;
			this->field_A00 = 1;
			goto LABEL_156;
		}
		else {
			LABEL_104:
			this->field_B0 = 1;
			this->field_B2 |= 1;
		}
	}
	
	//LABEL_105:
	if (this->field_5F8 != nullptr) {
		this->field_998 = *this->field_5F8;
	}
	else {
		float zval{}, yval{}, xval{};
		float somefloat = this->VirtActor58();
		if (somefloat < 0.0) {
			if (this->field_760 >= 0.0) {
				zval = this->field_760;
			}
			else {
				zval = this->field_754;
			}

			if (this->field_75C >= 0.0) {
				yval = this->field_75C;
			}
			else {
				yval = this->field_754;
			}

			if (this->field_758 >= 0.0) {
				xval = this->field_758;
			}
			else {
				xval = this->field_754;
			}
		}
		else {
			zval = somefloat;
			yval = somefloat;
			xval = somefloat;
		}		

		D3DXVECTOR4 v105 = { xval, yval, zval, 1.0 };
		this->field_998.CreateScaling(&v105);

		bool rotateflag = this->CheckSomeFlag3();
		if (rotateflag == false) {
			int legs4 = this->CibCollect.legs4;
			if (legs4 <= 0 || legs4 == 3) {
				if (this->IsDead() == false)
					rotateflag = true;
			}
		}

		if (rotateflag == true) {
			D3DXVECTOR4* pr = this->VirtActor102();
			D3DXVECTOR4 r = *pr + this->field_62C;
			this->field_998.RotateX(r.x);
			this->field_998.RotateY(r.y);
			this->field_998.RotateZ(r.z);
		}
		else {
			this->SomeMatrixMath(&this->field_998, 0.1875);
		}
		D3DXVECTOR4* a = this->GetPos();
		D3DXVECTOR4 b = *a + this->field_60C;
		b.w = 1.0;
		this->field_998.AddTranslation4(&b);
	}

	if (modelbase->field_B0.w == 0.0
		|| modelbase->field_B0.w == -1.0) {
		this->Model.DoingSomething(this, &this->field_998, this->field_BC, nullptr);
	}
	else {
		D3DXVECTOR4 v99 = this->field_848;
		v99.w = modelbase->field_B0.w;
		this->Model.DoingSomething(this, &this->field_998, this->field_BC, &v99);
	}

	this->SomeCameraUpdate();
}

void FFXI::CYy::CXiSkeletonActor::SomeCameraUpdate()
{
	if (this->VirtActor75() == false)
		return;

	//sub //TODO
	exit(0x100CAEF8);
}

void FFXI::CYy::CXiSkeletonActor::BaseDoSomething()
{
	CYyModelBase* base = *this->Model.GetBase();
	if (base == nullptr)
		return;

	if (this->CheckAtel120Bit5() == true) {
		this->field_848.w = -1.0;
	}
	else {
		CXiActor* v3{ nullptr };
		if ((this->field_840 & 1) != 1) {
			if (this->AmIControlActor() == true)
				;// sub;//inputfunc
			else
				v3 = this->Get154Actor();

			if (v3 != nullptr && v3 != this) {
				FFXI::Constants::Enums::ActorType v3type = v3->GetType();
				if (v3type == FFXI::Constants::Enums::ActorType::ZERO
					|| v3type == FFXI::Constants::Enums::ActorType::ONE
					|| v3type == FFXI::Constants::Enums::ActorType::TWO
					|| v3type == FFXI::Constants::Enums::ActorType::SIX
					|| v3type == FFXI::Constants::Enums::ActorType::SEVEN
					|| v3type == FFXI::Constants::Enums::ActorType::EIGHT) {
					FFXI::Constants::Enums::GAME_STATUS thisstatus = this->GetGameStatus();
					if (thisstatus == FFXI::Constants::Enums::GAME_STATUS::N_IDLE
						|| thisstatus == FFXI::Constants::Enums::GAME_STATUS::SIT
						|| this->IsOnChair()
						|| thisstatus == FFXI::Constants::Enums::GAME_STATUS::ICRYSTALL
						|| this->IsOnChocobo()
						|| this->IsOnMount()) {
						D3DXVECTOR4 v26{};
						v3->VirtActor103(3, &v26);
						if (v3->field_B2 != 0) {
							v26.y -= 1.2f;
						}
						this->field_848.x = v26.x;
						this->field_848.y = v26.y;
						this->field_848.z = v26.z;
						this->field_848.w = 0.0;
					}
				}
			}
			this->field_848.w = -1.0;
		}
		else if ((this->field_840 & 2) != 0) {
			this->field_848.w = -1.0;
		}
		else {
			this->field_848.w = 1.0;
		}
	}
}

void FFXI::CYy::CXiSkeletonActor::SetDrawBasicFlags()
{
	if ((this->field_840 & 0x80000) == 0)
	{
		ResourceContainer* file{ nullptr };
		if (CYyDb::g_pCYyDb->pCMoResourceMng->Unknown3 != nullptr) {
			file = *CYyDb::g_pCYyDb->pCMoResourceMng->Unknown3;
		}

		this->cubemap_spec = CYyDb::pCYyTexMng->FindD3sTexUnder("cubemap spec    ", file);
		this->field_840 |= 0x80000;
	}
	FFXI::CYy::CXiSkeletonActor::s_cubemap_spec = this->s_cubemap_spec;

	if ((this->field_840 & 8) == 0)
	{
		this->ActorFindResource(&this->veve_texinfo, FFXI::Constants::Enums::ResourceType::Texinfo, 'veve');
		this->field_840 |= 8u;
	}
	FFXI::CYy::CXiSkeletonActor::s_veve_texinfo = this->veve_texinfo;

	if ((this->field_840 & 0x10) == 0)
	{
		this->ActorFindResource(&this->null_maxresource, FFXI::Constants::Enums::ResourceType::Max_0, 0);
		this->field_840 |= 0x10u;
		this->Model.SubStruct5.field_4 = this->null_maxresource;
		if (this->AtelBuffer != nullptr)
		{
			if ((this->AtelBuffer->field_120 & 0x80) != 0 || (this->AtelBuffer->field_120 & 0x100) != 0)
				this->field_8EC = 0x40800000;
		}
	}

	if (this->null_maxresource != nullptr && *this->null_maxresource != nullptr && (this->field_840 & 0x20) == 0)
	{
		this->ActorFindResource(&this->ohno_texinfo, FFXI::Constants::Enums::ResourceType::Texinfo, 'onho');
		this->field_840 |= 0x20u;
	}
	
	if (this->MaybeIsEnvmap()) {
		CXiSkeletonActor::s_float1 = this->VirtSkeletonActor251();
		CXiSkeletonActor::s_float2 = this->VirtSkeletonActor253();
		CXiSkeletonActor::s_int1 = this->VirtSkeletonActor249();
		CXiSkeletonActor::s_int2 = this->VirtSkeletonActor248();
		CMoOs2::DrawBasicFlagOne = CXiSkeletonActor::s_int2 != 0;
	}

	if (this->MaybeIsDistortion()) {
		CXiSkeletonActor::s_float3 = this->VirtSkeletonActor251();
		CXiSkeletonActor::s_float4 = this->VirtSkeletonActor253();
		CMoOs2::DrawBasicFlagTwo = 1;
		D3DCOLOR somecolor = this->VirtActor1();
		CXiSkeletonActor::s_color1 = (0x80000000 - (somecolor & 0xFF000000)) | 0x808080;
	}

	this->MaybeIsMasking();
	char value = this->VirtActor20();
	if (value != 0) {
		CXiSkeletonActor::s_char1 = 1;
	}
}

void FFXI::CYy::CXiSkeletonActor::Tick()
{
	float v2 = CYyDb::g_pCYyDb->CheckTick() * 0.4f;
	if (v2 >= 1.0f)
		v2 = 1.0f;

	D3DLIGHT8* v88[4] = { nullptr };
	float v89[4] = { 10000.0, 10000.0, 10000.0, 10000.0 };
	int v7 = 0;

	//Setup point lights
	for (int i = 0; i < sizeof(this->field_180) / sizeof(this->field_180[0]); ++i) {
		if (v7 >= 4)
			break;

		D3DLIGHT8* light1 = this->field_180 + i;
		D3DLIGHT8* light2 = this->field_388 + i;

		light1->Type = light2->Type;

		if (light2->Type != D3DLIGHT_POINT) {
			continue;
		}

		light1->Diffuse = light2->Diffuse;
		light1->Position = light2->Position;
		light1->Range = light2->Range;
		light1->Attenuation2 = light2->Attenuation2;
		D3DXVECTOR4* v10 = this->GetPos();
		D3DXVECTOR4 v91{}, v92{};
		v92.x = light1->Position.x - v10->x;
		v92.y = light1->Position.y - v10->y;
		v92.z = light1->Position.z - v10->z;

		v91.x = v92.x * v92.x;
		v91.y = v92.y * v92.y;
		v91.z = v92.z * v92.z;

		float v11 = v91.z + v91.y + v91.x;
		float v13 = sqrt(v11);
		if (v13 <= light1->Range) {
			float v17 = v11 * light1->Attenuation2;
			if (v17 <= 0.0001f) {
				v17 = 0.0001f;
			}
			float v19 = 1.0f / v17;
			int j{};
			for (j = 0; j < 4; ++j) {
				if (v88[j] == nullptr)
					break;
				if (v19 > v89[j])
					break;
			}
			if (j != 4) {
				for (int k = 2; k >= j; --k) {
					v88[k + 1] = v88[k];
					v89[k + 1] = v89[k];
				}
				v7 += 1;
				v88[j] = light1;
				v89[j] = v19;
			}
		}
	}
	if (v7 > 1) {
		memset(v89 + 1, 0, 12);
		memset(v88 + 1, 0, 12);
		v7 = 1;
	}

	//Setup directional lights
	for (int i = 0; i < sizeof(this->field_180) / sizeof(this->field_180[0]); ++i) {
		if (v7 >= 4)
			break;

		D3DLIGHT8* light1 = this->field_180 + i;
		D3DLIGHT8* light2 = this->field_388 + i;

		light1->Type = light2->Type;
		if (light2->Type != D3DLIGHT_DIRECTIONAL) {
			continue;
		}

		if (this->field_B0 == 0 || this->field_B2 != 0) {
			light1->Diffuse.r += (light2->Diffuse.r - light1->Diffuse.r) * v2;
			light1->Diffuse.g += (light2->Diffuse.g - light1->Diffuse.g) * v2;
			light1->Diffuse.b += (light2->Diffuse.b - light1->Diffuse.b) * v2;
			light1->Diffuse.a += (light2->Diffuse.a - light1->Diffuse.a) * v2;

			light1->Direction.x += (light2->Direction.x - light1->Direction.x) * v2;
			light1->Direction.y += (light2->Direction.y - light1->Direction.y) * v2;
			light1->Direction.z += (light2->Direction.z - light1->Direction.z) * v2;
		}
		else {
			light1->Diffuse = light2->Diffuse;
			light1->Direction = light2->Direction;
		}
		int j{};
		for (j = 0; j < 4; ++j) {
			if (v88[j] == nullptr)
				break;
			if (v89[j] < 0.001f)
				break;
		}
		if (j != 4) {
			for (int k = 2; k >= j; --k) {
				v88[k + 1] = v88[k];
				v89[k + 1] = v89[k];
			}
			v7 += 1;
			v88[j] = light1;
			v89[j] = 0.001f;
		}
	}

	//Set up g_light_arr and transform point lights into directional lights
	for (int i = 0; i < 4; ++i) {
		D3DLIGHT8* light = CXiSkeletonActor::g_light_arr + i;
		if (v88[i] != nullptr) {
			*light = *v88[i];
			if (light->Type == D3DLIGHT_POINT) {
				light->Type = D3DLIGHT_DIRECTIONAL;
				D3DXVECTOR4* ac = this->GetPos();
				light->Direction.x = ac->x - light->Position.x;
				light->Direction.y = ac->y - light->Position.y;
				light->Direction.z = ac->z - light->Position.z;
				Globals::Vec3Normalize((D3DXVECTOR3*)&light->Direction);
				light->Diffuse.r *= v89[i];
				light->Diffuse.g *= v89[i];
				light->Diffuse.b *= v89[i];
			}
		}
		else {
			light->Type = (D3DLIGHTTYPE)NULL;
		}
	}

	int v34{};
	for (v34 = 0; v34 < 4; v34++) {
		if (this->field_180 + v34 == v88[0])
			break;
	}
	if (v34 == 4)
		v34 = 0;
	CXiSkeletonActor::g_light = this->field_388 + v34;
	int vf130 = this->VirtActor130();
	bool vf130zero = vf130 == 0;
	int f88 = this->VirtActor88();
	CMoResource** v37 = this->Model.SubStruct5.GetResource();

	if (v37 != nullptr && *v37 != nullptr){
		unsigned char byte48 = ((unsigned char*)(*v37))[48];
		if ((byte48 & 2) == 0) {
			for (auto a : CXiSkeletonActor::g_light_arr) {
				a.Type = (D3DLIGHTTYPE)NULL;
			}
		}

		ActorAmbientLight = 0x80707070;
	}
	else {
		XiZone::zone->GetAmbientByFourCC(&ActorAmbientLight, vf130zero, f88);
	}

	D3DCOLOR fogColor{};
	float fogStart{}, fogEnd{};
	XiZone::zone->GetFogByFourCC(&fogColor, &fogEnd, &fogStart, vf130zero, f88);
	Placeholder::g_pTkInputCtrl;
	CXiActor* v42 = nullptr; //sub //TODO //inputctrl func
	if (v42 == nullptr) {
		v42 = CXiControlActor::user_control_target.TWOGetSearchActor();
	}
	
	if (v42 == this
		|| (this->field_88 & 0x200) != 0) {
		if ((this->field_840 & 4) == 0) {
			this->field_844 = 40.0;
			this->field_840 |= 0x6000000;
		}
		if ((this->field_840 & 0x6000000) != 0
			|| (this->field_88 & 0x200) != 0) {
			float v50{};
			if (this->field_844 <= 20.0)
				v50 = this->field_844;
			else
				v50 = 40.0f - this->field_844;

				//Adjust ambient light
				float v84 = v50 * 0.05f;
				float scale = 1.0f - v84;
				float add = v84 * 220.0f;
				D3DXVECTOR3 vecambient{};
				vecambient.x = (unsigned char)ActorAmbientLight;
				vecambient.x *= scale;
				vecambient.x += add;

				vecambient.y = (unsigned char)(ActorAmbientLight >> 8);
				vecambient.y *= scale;
				vecambient.y += add;

				vecambient.z = (unsigned char)(ActorAmbientLight >> 16);
				vecambient.z *= scale;
				vecambient.z += add;

				//Lower bound check
				if (vecambient.x < 0.0)
					vecambient.x = 0;
				if (vecambient.y < 0.0)
					vecambient.y = 0;
				if (vecambient.z < 0.0)
					vecambient.z = 0;

				//Upper bound check
				if (vecambient.x > 255.0)
					vecambient.x = 255.0;
				if (vecambient.y > 255.0)
					vecambient.y = 255.0;
				if (vecambient.z > 255.0)
					vecambient.z = 255.0;

				ActorAmbientLight = (int)vecambient.z;
				ActorAmbientLight <<= 8;
				ActorAmbientLight |= (int)vecambient.y;
				ActorAmbientLight <<= 8;
				ActorAmbientLight |= (int)vecambient.x;

				//Adjust Fog Color
				D3DXVECTOR3 fog{};
				fog.x = (unsigned char)fogColor;
				fog.x *= v84;

				fog.y = (unsigned char)(fogColor >> 8);
				fog.y *= v84;

				fog.z = (unsigned char)(fogColor >> 16);
				fog.z *= v84;

				//Lower bound check
				if (fog.x < 0.0)
					fog.x = 0;
				if (fog.y < 0.0)
					fog.y = 0;
				if (fog.z < 0.0)
					fog.z = 0;

				//Upper bound check
				if (fog.x > 255.0)
					fog.x = 255.0;
				if (fog.y > 255.0)
					fog.y = 255.0;
				if (fog.z > 255.0)
					fog.z = 255.0;

				fogColor = (int)fog.z;
				fogColor <<= 8;
				fogColor |= (int)fog.y;
				fogColor <<= 8;
				fogColor |= (int)fog.x;

				this->field_844 -= CYyDb::g_pCYyDb->CheckTick();
				if (this->field_844 < 0.0) {
					this->field_844 = 40.0;
					int v76 = this->field_840;
					this->field_840 = v76 ^ (v76 ^ ((v76 & 0xFE000000) - 1)) & 0x6000000;
					if ((this->field_840 & 0x6000000) == 0) {
						this->field_88 &= 0xFFFFFDFF;
					}
				}
		}
		this->field_840 |= 4;
	}
	else {
		this->field_840 &= 0xFFFFFFFB;
	}

	FFXI::CYy::CDx::instance->DXDevice->SetRenderState(D3DRS_FOGCOLOR, fogColor);
	FFXI::CYy::CDx::instance->DXDevice->SetRenderState(D3DRS_FOGSTART, *((DWORD*)(&fogStart)));
	FFXI::CYy::CDx::instance->DXDevice->SetRenderState(D3DRS_FOGEND, *((DWORD*)(&fogEnd)));

	CMoResource** something = this->Model.SubStruct5.GetResource();
	if (something == nullptr || *something == nullptr)
		return;

	unsigned char byte48 = ((unsigned char*)(*something))[48];
	if ((byte48 & 2) == 0) {
		for (auto a : CXiSkeletonActor::g_light_arr) {
			a.Type = (D3DLIGHTTYPE)NULL;
		}
	}
}

void FFXI::CYy::CXiSkeletonActor::SomeMatrixMath(FFXI::Math::WMatrix* a2, float a3) {
	FFXI::Math::WMatrix v33{};
	D3DXVECTOR4 v28 = { 0.0, 0.0, 1.0, 1.0 };
	D3DXVECTOR4 v31 = { 0.0, -1.0, 0.0, 1.0 };
	D3DXVECTOR4* v5 = this->GetGroundNormal();
	D3DXVECTOR4 v34{};
	D3DXVECTOR3 v27{}, v30{}, v32{}, v36{};
	double magsq = v31.x * v5->x + v31.y * v5->y + v31.z * v5->z;
	if (magsq >= 0.86602539) {
		v34.x = v5->x;
		v34.y = v5->y;
		v34.z = v5->z;
	}
	else {
		FFXI::Math::KzFQuat v35{};
		v35.SetQuatFromVector_Rad((D3DXVECTOR3*)&v31, (D3DXVECTOR3*)v5, Constants::Values::ANGLE_PI_OVER_6);
		v35.GetMatrix(&v33);
		v33.Vec3TransformDrop4((D3DXVECTOR3*)&v34, (D3DXVECTOR3*)&v31);
		Globals::Vec3Normalize((D3DXVECTOR3*)&v34);
	}
	float v25 = 1.0f - this->field_828;
	v30.x = v34.x;
	v30.y = v34.y;
	v30.z = v34.z;
	
	v30.x = v30.x * this->field_828 + v25 * v31.x;
	v30.y = v30.y * this->field_828 + v25 * v31.y;
	v30.z = v30.z * this->field_828 + v25 * v31.z;
	Globals::Vec3Normalize(&v30);

	float v26 = 1.0f - a3;
	v27 = this->field_82C;
	v27.x = v27.x * v26 + v30.x * a3;
	v27.y = v27.y * v26 + v30.y * a3;
	v27.z = v27.z * v26 + v30.z * a3;
	Globals::Vec3Normalize(&v27);
	this->field_82C = v27;
	v27.x *= -1;
	v27.y *= -1;
	v27.z *= -1;

	D3DXVECTOR4* v8 = this->VirtActor102();
	v33.Identity();
	v33.RotateX(v8->x);
	v33.RotateY(v8->y);
	v33.RotateZ(v8->z);

	v33.Vec3TransformDrop4Self((D3DXVECTOR3*) &v28);
	Globals::Vec3Normalize((D3DXVECTOR3*) &v28);
	Globals::Vec3Outer(&v32, &v27, (D3DXVECTOR3*)&v28);
	Globals::Vec3Normalize(&v32);
	Globals::Vec3Outer((D3DXVECTOR3*)&v28, &v32, &v27);
	Globals::Vec3Normalize((D3DXVECTOR3*)&v28);

	v33._11 = v32.x;
	v33._12 = v32.y;
	v33._13 = v32.z;
	v33._14 = 0.0;

	v33._21 = v27.x;
	v33._22 = v27.y;
	v33._23 = v27.z;
	v33._24 = 0.0;

	v33._31 = v28.x;
	v33._32 = v28.y;
	v33._33 = v28.z;
	v33._34 = 0.0;

	v33._41 = 0.0;
	v33._42 = 0.0;
	v33._43 = 1.0;
	v33._44 = 0.0;
	a2->MatrixMultiply(&v33);

	Globals::Vec3Outer(&v32, (D3DXVECTOR3*) & v31, (D3DXVECTOR3*)&v34);
	Globals::Vec3Normalize(&v32);
	Globals::Vec3Outer(&v36, (D3DXVECTOR3*)&v34, &v32);
	Globals::Vec3Normalize(&v36);

	Math::BoundingBox3D* boundingBox = this->GetBoundingBoxByState(false);
	
	D3DXVECTOR3 v29{};
	v29.x = boundingBox->Back - boundingBox->Front;
	v29.y = 0.0;
	v29.z = boundingBox->Right - boundingBox->Left;

	double v17 = v29.z / v29.x;
	if (v17 < 1.4) {
		if (v17 > 0.6) {
			this->field_828 = 1.0;
			return;
		}
	}

	if (v17 >= 1.0) {
		v29 = { 1.0, 0.0, 0.0 };
	}
	else {
		v29 = { 0.0, 0.0, 1.0 };
	}

	a2->Vec3TransformDrop4Self(&v29);
	this->field_828 = fabs(Globals::Vec3Inner(&v36, &v29));
}

void FFXI::CYy::CXiSkeletonActor::ReadMotionPackPolling()
{
	FFXI::Constants::Enums::ActorType actortype = this->GetType();
	if (actortype == FFXI::Constants::Enums::ActorType::ZERO || actortype == FFXI::Constants::Enums::ActorType::ONE || actortype == FFXI::Constants::Enums::ActorType::SIX) {
		if ((this->field_8B0 & 0x1FF) != 0x1FF)
			return;

		switch (this->field_8A4) {
		case 1:
			this->ReadStdMotionRes();
			this->field_8A4 = 0;
			break;
		case 2:
			//sub //TODO
			exit(0x100CF12A);
			this->field_8A4 = 0;
			break;
		case 3:
			//sub //TODO
			exit(0x100CF146);
			this->field_8A4 = 0;
			break;
		case 4:
			//sub //TODO
			exit(0x100CF14F);
			this->field_8A4 = 0;
			break;
		case 5:
			//sub //TODO
			exit(0x100CF158);
			this->field_8A4 = 0;
			break;
		default:
			break;
		}
	}

	if (this->field_8A4 == 3) {
		if (actortype == FFXI::Constants::Enums::ActorType::TWO || actortype == FFXI::Constants::Enums::ActorType::SEVEN) {
			exit(0x100CF1A3);
			this->field_8A4 = 0;
		}
	}
	
	if (this->IsReadCompleteResList() == true) {
		if (this->field_8C4 != 0) {
			exit(0x100CF1A4);
		}
	}
}

void FFXI::CYy::CXiSkeletonActor::ReadStdMotionRes()
{
	if (this->GetType() == FFXI::Constants::Enums::ActorType::TWO)
		return;

	int race = this->GetRace();
	if (race >= 9)
		race = 1;

	unsigned int waistDatIndex = this->GetWaistDatIndex(race, 0, 0);
	if (waistDatIndex > 0) {
		if (this->FindResfList(waistDatIndex) == nullptr) {
			ResourceContainer** wres = CYyDb::pCMoResourceMng->GetOrLoadDatByIndex(waistDatIndex);
			if (wres != nullptr) {
				this->AppendNewResp(wres, 2, waistDatIndex, 5);
				if (CMoResourceMng::IsResourceReady((CMoResource***) &wres) == true) {
					this->AddMotRes((CMoResource**)wres, this->field_9EC);
				}
				else {
					File::MotionReferenceRead* motread = new File::MotionReferenceRead();
					if (motread != nullptr) {
						if (this == nullptr) {
							motread->ActorIndex = CXiSkeletonActor::skeletonActorIndex;
						}
						else {
							motread->ActorIndex = this->field_9EC;
						}
						motread->field_4.SetActor(this);
						motread->field_10 = 2;
					}
					ResourceContainer::ReferenceRead(&wres, File::MotionReferenceRead::ReadCallback, motread, this, nullptr);
				}
			}
		}
	}

	unsigned int upperBodyDatIndex = this->GetUpperBodyDatIndex(race, 0);
	if (upperBodyDatIndex >= 0) {
		if (this->FindResfList(upperBodyDatIndex) == nullptr) {
			ResourceContainer** ubres = CYyDb::pCMoResourceMng->GetOrLoadDatByIndex(upperBodyDatIndex);
			if (ubres != nullptr) {
				this->AppendNewResp(ubres, 1, upperBodyDatIndex, 0);
				if (CMoResourceMng::IsResourceReady((CMoResource***)&ubres) == true) {
					this->AddMotRes((CMoResource**)ubres, this->field_9EC);
				}
				else {
					File::MotionReferenceRead* motread = new File::MotionReferenceRead();
					if (motread != nullptr) {
						if (this == nullptr) {
							motread->ActorIndex = CXiSkeletonActor::skeletonActorIndex;
						}
						else {
							motread->ActorIndex = this->field_9EC;
						}
						motread->field_4.SetActor(this);
						motread->field_10 = 1;
					}
					ResourceContainer::ReferenceRead(&ubres, File::MotionReferenceRead::ReadCallback, motread, this, nullptr);
				}
			}
		}
	}

	if (this->IsOnChocobo() == true
		|| this->IsOnMount() == true) {
		//sub //TODO
		exit(0x100CF712);
	}
}

float FFXI::CYy::CXiSkeletonActor::GetAttackReach()
{
	D3DXVECTOR4 v2{};
	this->GetElemLocal(25, &v2);
	return sqrt(Globals::Vec3Inner(&v2, &v2));
}

unsigned int FFXI::CYy::CXiSkeletonActor::GetWaistDatIndex(int a2, int a3, int a4)
{
	int race = a2;
	if (race >= 9)
		race = 1;

	int waist_type = this->CibCollect.waist_type;
	if (waist_type < 1)
		waist_type = 1;

	if (a3 == 1) {
		int offset = this->CibCollect.amot_wep0;
		if (a4 == 2) {
			offset = this->CibCollect.field_12;
		}

		if (offset < 0) {
			return -1;
		}

		int index = waist_type - 1 + 2 * race;
		if (a4 == 1) {
			return FFXI::Constants::DatIndices::mot_waist_type1[index] + offset;
		}
		else if (a4 == 2) {
			return FFXI::Constants::DatIndices::mot_waist_type2[index] + offset;
		}
		else {
			return FFXI::Constants::DatIndices::mot_waist_typeX[index] + offset;
		}
	}
	else if (waist_type <= 1) {
		if (a3 != 0) {
			return 0;
		}
	}

	return FFXI::Constants::DatIndices::base_skeleton_no_tab[race] + waist_type + 2;
}

unsigned int FFXI::CYy::CXiSkeletonActor::GetUpperBodyDatIndex(int a2, int a3)
{
	int race = a2;
	if (race >= 9)
		race = 1;

	if (this->GetGameStatus() != FFXI::Constants::Enums::GAME_STATUS::B_IDLE) {
		return FFXI::Constants::DatIndices::base_skeleton_no_tab[race] + this->CibCollect.is_shield + 1;
	}

	int offset = this->CibCollect.amot_wep0;
	if (a3 == 2 ) {
		offset = this->CibCollect.field_12;
	}

	if (offset < 0)
		return -1;

	if (a3 == 1) {
		return FFXI::Constants::DatIndices::mot_upperbody_type1[race] + offset;
	}
	else if (a3 == 2) {
		return FFXI::Constants::DatIndices::mot_upperbody_type2[race] + offset;
	}
	else {
		return FFXI::Constants::DatIndices::mot_upperbody_typeX[race] + offset;
	}
}

bool FFXI::CYy::CXiSkeletonActor::IsReadCompleteResList()
{
	if (this->field_8A4 != 0)
		return false;

	CYyResfList* resf = (CYyResfList*)this->field_7E4->GetTail();
	while (resf != nullptr) {
		if (resf->field_1C == nullptr
			|| CYy::CMoResourceMng::IsResourceReady((CYy::CMoResource***) & resf->field_1C) == false) {
			return false;
		}
		resf = (CYyResfList*)resf->field_8;
	}

	ResourceContainer** v5{};
	this->VirtActor109(&v5);
	if (v5 == nullptr)
		return false;

	if (CYy::CMoResourceMng::IsResourceReady((CYy::CMoResource***) &v5) == false)
		return false;

	this->VirtSkeletonActor246(&v5);
	if (v5 != nullptr) {
		if (CYy::CMoResourceMng::IsResourceReady((CYy::CMoResource***) &v5) == false)
			return false;
	}
	else if (this->GetType() == FFXI::Constants::Enums::ActorType::SEVEN) {
		if (this->GetEquipNum(0) != 0)
			return false;
	}
	return true;
}

void FFXI::CYy::CXiSkeletonActor::AddMotRes(CMoResource** a2, int a3)
{
	if (this->field_9EC != a3)
		return;

	if (((*a2)->TypeSizeFlags & 0x7F) == FFXI::Constants::Enums::ResourceType::Rmp) {
		this->CXiActor::StartGenerators(a2);
	}
	
	CYyResfList* resf = this->field_7E4;
	while (resf != nullptr) {
		if (resf->field_1C != nullptr) {
			if (CYy::CMoResourceMng::IsResourceReady((CYy::CMoResource***) &resf->field_1C) == true) {
				if (resf->field_20 == false) {
					resf->field_20 = true;
					(*resf->field_1C)->IncrementReferenceCount();
				}
			}
		}
		resf = (CYyResfList*)resf->field_4;
	}
}

void FFXI::CYy::CXiSkeletonActor::SelectMotionResId(float a2, int* a3, float* a4)
{
	*a4 = 1.0;
	*a3 = this->field_7C8[0];
	switch (this->GetGameStatus()) {
	case FFXI::Constants::Enums::GAME_STATUS::B_IDLE:
		*a3 = this->field_7C8[3];
		break;
	case FFXI::Constants::Enums::GAME_STATUS::N_DEAD:
	case FFXI::Constants::Enums::GAME_STATUS::B_DEAD:
		if (CYyDb::g_pCYyDb->field_9 != 0
			&& this->IsMotionLock() == false) {
			*a3 = this->field_7C8[4];
			return;
		}
		*a3 = ' roc';
		return;
	case FFXI::Constants::Enums::GAME_STATUS::FISHING:
	case FFXI::Constants::Enums::GAME_STATUS::FISHING1:
	case FFXI::Constants::Enums::GAME_STATUS::FISHING2:
	case FFXI::Constants::Enums::GAME_STATUS::FISHING3:
	case FFXI::Constants::Enums::GAME_STATUS::FISHING4:
	case FFXI::Constants::Enums::GAME_STATUS::FISHING5:
	case FFXI::Constants::Enums::GAME_STATUS::FISHING6:
	case FFXI::Constants::Enums::GAME_STATUS::FISH_2:
	case FFXI::Constants::Enums::GAME_STATUS::FISHF:
	case FFXI::Constants::Enums::GAME_STATUS::FISHR:
	case FFXI::Constants::Enums::GAME_STATUS::FISHL:
	case FFXI::Constants::Enums::GAME_STATUS::FISH_3:
	case FFXI::Constants::Enums::GAME_STATUS::FISH_31:
	case FFXI::Constants::Enums::GAME_STATUS::FISH_32:
	case FFXI::Constants::Enums::GAME_STATUS::FISH_33:
	case FFXI::Constants::Enums::GAME_STATUS::FISH_34:
	case FFXI::Constants::Enums::GAME_STATUS::FISH_35:
	case FFXI::Constants::Enums::GAME_STATUS::FISH_36:
		if (CYyDb::g_pCYyDb->field_9 != 0) {
			*a3 = this->field_7C8[4];
		}
		else {
			*a3 = ' 1hf';
		}
		break;
	case FFXI::Constants::Enums::GAME_STATUS::POL:
	case FFXI::Constants::Enums::GAME_STATUS::CAMP:
		if (CYyDb::g_pCYyDb->field_9 != 0
			&& this->IsMotionLock() == false) {
			*a3 = this->field_7C8[4];
			return;
		}
		*a3 = ' 1xr';
		return;
	case FFXI::Constants::Enums::GAME_STATUS::SIT:
		if (CYyDb::g_pCYyDb->field_9 != 0
			&& this->IsMotionLock() == false) {
			*a3 = this->field_7C8[4];
			return;
		}
		*a3 = ' 1is';
		return;
	case FFXI::Constants::Enums::GAME_STATUS::CHAIR00:
	case FFXI::Constants::Enums::GAME_STATUS::CHAIR01:
	case FFXI::Constants::Enums::GAME_STATUS::CHAIR02:
	case FFXI::Constants::Enums::GAME_STATUS::CHAIR03:
	case FFXI::Constants::Enums::GAME_STATUS::CHAIR04:
	case FFXI::Constants::Enums::GAME_STATUS::CHAIR05:
	case FFXI::Constants::Enums::GAME_STATUS::CHAIR06:
	case FFXI::Constants::Enums::GAME_STATUS::CHAIR07:
	case FFXI::Constants::Enums::GAME_STATUS::CHAIR08:
	case FFXI::Constants::Enums::GAME_STATUS::CHAIR09:
	case FFXI::Constants::Enums::GAME_STATUS::CHAIR10:
	case FFXI::Constants::Enums::GAME_STATUS::CHAIR11:
	case FFXI::Constants::Enums::GAME_STATUS::CHAIR12:
	case FFXI::Constants::Enums::GAME_STATUS::CHAIR13:
	case FFXI::Constants::Enums::GAME_STATUS::CHAIR14:
	case FFXI::Constants::Enums::GAME_STATUS::CHAIR15:
	case FFXI::Constants::Enums::GAME_STATUS::CHAIR16:
	case FFXI::Constants::Enums::GAME_STATUS::CHAIR17:
	case FFXI::Constants::Enums::GAME_STATUS::CHAIR18:
	case FFXI::Constants::Enums::GAME_STATUS::CHAIR19:
	case FFXI::Constants::Enums::GAME_STATUS::CHAIR20:
		if (CYyDb::g_pCYyDb->field_9 != 0
			&& this->IsMotionLock() == false) {
			*a3 = this->field_7C8[4];
			return;
		}
		*a3 = ' 1ic';
		return;
	default:
		break;
	}
	
	double v7 = a2;
	if (v7 > 0.33333334) {
		*a4 = a2 * 3.0f;
		if (*a4 <= 1.5) {
			*a3 = this->field_7C8[1];
		}
		else {
			*a4 = a2;
			*a3 = this->field_7C8[2];
		}
	}
	else if (v7 > 0.000099999997) {
		*a4 = a2 * 3.0f;
		*a3 = this->field_7C8[1];
	}

	if (this->IsParallelMove() != 0
		|| this->IsFreeRun() == 0) {
		if (this->GetType() == FFXI::Constants::Enums::ActorType::TWO) {
			if (this->field_598 >= 2 && this->field_598 <= 4)
				*a3 = this->field_7C8[1];
		}
		else if (this->IsOnChocobo() == true
			|| this->IsOnMount() == true) {
			if (this->field_598 == 2
				|| this->field_598 == 4) {
				*a3 = ' klw';
			}
			else if (this->field_598 == 3) {
				*a3 = ' bvm';
				}
		}
		else if (this->field_598 == 2) {
			*a3 = ' rvm';
		}
		else if (this->field_598 == 3) {
			*a3 = ' bvm';
		}
		else if (this->field_598 == 4) {
			*a3 = ' lvm';
		}
	}

	if (this->field_8BC >= 0.0) {
		*a3 = this->GetGameStatus() != FFXI::Constants::Enums::GAME_STATUS::B_IDLE ? ' pmj' : '*pmj';
		*a4 = 1.0;
	}
	*a4 *= this->field_7E0;
	if (this->GetType() == FFXI::Constants::Enums::ActorType::TWO || this->GetType() == FFXI::Constants::Enums::ActorType::SEVEN) {
		if (this->VirtActor94() == 0.0) {
			*a4 = 1.0;
		}
	}
}

CYyResfList* FFXI::CYy::CXiSkeletonActor::FindResfList(int a2)
{
	CYyResfList* list = this->field_7E4;
	while (list != nullptr) {
		if (list->field_14 == a2)
			break;
		list = (CYyResfList*)list->field_4;
	}
	return list;
}

void FFXI::CYy::CXiSkeletonActor::AppendNewResp(ResourceContainer** a2, int a3, int a4, int a5)
{
	char* mem = MemoryPoolManager::globalInstance->Get(sizeof(CYyResfList), MemoryPoolManager::MemoryPoolType::Ex);
	if (mem == nullptr)
		return;

	CYyResfList* newlist = new (mem) CYyResfList();
	if (newlist == nullptr)
		return;

	newlist->field_10 = a3;
	newlist->field_14 = a4;
	newlist->field_1C = a2;
	newlist->field_C = a5;

	newlist->Append((CYyNode**) & this->field_7E4);
}

void FFXI::CYy::CXiSkeletonActor::DefaultMove(D3DXVECTOR3* a2)
{
	bool v3 = this->InOwnActorPointers();
	float MoveVectorRatio = this->GetMoveVectorRatio(a2);
	int v35{};
	float v34{};
	this->SelectMotionResId(MoveVectorRatio, &v35, &v34);
	int v35backup = v35;
	if (this->field_768[0] != nullptr) {
		if (this->IsOnMount() == true) {
			int something = ((CXiSkeletonActor*)this->field_768[0])->CibCollect.GetSomething(this->GetRace());
			v35 ^= (v35 ^ (something + '0'));
		}
	}

	if (this->field_80 != 0)
		return;

	if (this->IsMotionLock() == false
		&& this->field_7C8[4] == v35) {
		if (this->field_7DC != v34) {
			CYyModelBase* base = *this->Model.GetBase();
			if (base != nullptr) {
				for (int i = 0; i < sizeof(base->field_24.motions) / sizeof(base->field_24.motions[0]); ++i) {
					CYyMotionQue* tail = base->field_24.motions[i].GetTail();
					if (tail != nullptr) {
						tail->SetSpeed(v34);
					}
				}
			}

			this->field_7DC = v34;

			CXiSkeletonActor* first = (CXiSkeletonActor*)this->field_768[0];
			if (first != nullptr) {
				if (first->VirtActor201() == true
					|| first->IsFishingRod() == true
					|| first->VirtActor209() == true) {
					CYyModelBase* base = *this->Model.GetBase();
					CYyModelBase* base2 = *first->Model.GetBase();
					if (base != nullptr
						&& base2 != nullptr) {
						for (int i = 0; i < sizeof(base->field_24.motions) / sizeof(base->field_24.motions[0]); ++i) {
							CYyMotionQue* tail = base->field_24.motions[i].GetTail();
							CYyMotionQue* tail2 = base2->field_24.motions[i].GetTail();
							if (tail != nullptr
								&& tail2 != nullptr) {
								float somefloat = tail->GetFrame();
								tail2->Frame = somefloat;
								tail2->SetSpeed(v34);
							}
						}
					}
				}
				else if (this->VirtActor204() == true) {
					float somedouble = first->CibCollect.GetSomeDouble();
					CYyModelBase* base = *first->Model.GetBase();
					if (base != nullptr) {
						for (int i = 0; i < sizeof(base->field_24.motions) / sizeof(base->field_24.motions[0]); ++i) {
							CYyMotionQue* tail = base->field_24.motions[i].GetTail();
							if (tail != nullptr) {
								tail->SetSpeed(v34 * somedouble);
							}
						}
					}
				}
			}
		}
	}
	else {
		float v41 = 16.0;
		bool v24 = false;
		if (this->IsMotionLock() == true) {
			v41 = this->GetRetIdleFrameTailQue();
			v24 = true;
		}

		if ((v35 & 0xFFFFFF) == 'pmj') {
			v24 = false;
		}

		if (v3 == false && this->DefSchedularCall() == true) {
			return;
		}

		if (this->IsReadCompleteResList() == false)
			return;

		if ((this->field_840 & 0x800000) == 0x800000)
			return;

		if (this->SetSystemMotion(v35, 1.0, v34, 0, v41, 1.0, 0, v24, 0, 0, 0) == true) {
			if (v35 == ' 1di') {
				if (this->SetSystemMotion(' ldi', 1.0, v34, 0, v41, 1.0, 0, v24, 0, 0, 0) == false) {
					this->field_7C8[4] = v35;
					this->field_7DC = v34;
				}
			}
			else if (v35 == ' 1fd') {
				if (this->SetSystemMotion('0tfd', 1.0, v34, 0, v41, 1.0, 0, v24, 0, 0, 0) == false) {
					this->field_7C8[4] = v35;
					this->field_7DC = v34;
				}
			}
		}
		else {
			this->field_7C8[4] = v35;
			this->field_7DC = v34;
			if (v3 == false) {
				bool v33{ false }, v42{ false };
				if (this->AtelBuffer != nullptr) {
					if ((this->AtelBuffer->field_12C & 0x400000) != 0)
						v33 = true;
					if ((this->AtelBuffer->field_120 & 0x80) != 0 || (this->AtelBuffer->field_120 & 0x100) != 0)
						v42 = true;
				}

				if (this->IsOnMount() == false) {
					if (this->IsOnChair()) {
						this->HideAllWeapons(0);
					}
					else {
						FFXI::Constants::Enums::ActorType type = this->GetType();
						if (type == FFXI::Constants::Enums::ActorType::ZERO
							|| type == FFXI::Constants::Enums::ActorType::SIX
							|| (type == FFXI::Constants::Enums::ActorType::TWO || type == FFXI::Constants::Enums::ActorType::SEVEN || type == FFXI::Constants::Enums::ActorType::ONE) && this->GetMonsterFlag() == true
							|| v42 == true && type == FFXI::Constants::Enums::ActorType::ONE
							|| (this->field_840 & 0x400000) != 0
							|| v33 == true) {
							this->ShowWeapon(0, 0);
							this->ShowWeapon(1, 0);
							this->HideWeapon(2, 0);
						}
					}
				}
			}
		}

		CXiSkeletonActor* first = (CXiSkeletonActor*)this->field_768[0];
		if (first != nullptr) {
			if (first->VirtActor201() == true
				|| first->VirtActor204() == true
				|| first->IsFishingRod() == true) {
				float v43 = v34;
				if (first->VirtActor204() == true)
					v43 *= first->CibCollect.GetSomeDouble();

				if (first->SetSystemMotion(v35backup, 1.0, v43, 0, v24, 1.0, 0, v24, 0, 0, 0) == true)
					this->field_7C8[4] = '    ';
			}
		}
	}

}

float FFXI::CYy::CXiSkeletonActor::GetMoveVectorRatio(D3DXVECTOR3* a2)
{
	FFXI::Constants::Enums::GAME_STATUS actorstatus = this->GetGameStatus();
	double v10{};
	if (this->AmIControlActor() == false
		|| CXiControlActor::is_auto_running == true
		|| this->GetMoveMode() == 1
		|| this->field_101 != 0
		|| actorstatus == FFXI::Constants::Enums::GAME_STATUS::EVT) {
		float v13 = a2->x * a2->x + a2->z * a2->z;
		if (this->IsOnLift() != 0) {
			v13 += a2->y * a2->y;
		}
		v13 = sqrt(v13);
		float v32 = 0.0;
		if (v13 >= 0.000099999997) {
			double v16 = CYyDb::g_pCYyDb->CheckTick();
			v16 *= this->VirtActor96();
			if (this->IsOnChocobo() == true
				|| this->IsOnMount() == true) {
				v16 *= 0.026666667;
			}
			else {
				v16 *= 0.016666668;
			}

			double v17{ 0.0 };
			if (v16 >= 0.000001) {
				double v18 = 0.0;
				if (v18 > 0.0)
					v17 = v13 / v16;
			}

			v10 = this->field_824 * 0.75 + v17 * 0.25;
		}
	}
	else {
		v10 = this->field_594;
		if (v10 <= 0.0 && this->field_F8 != 0) {
			v10 = 0.33333334;
		}

		double v3 = this->VirtActor94();
		if (v3 > 0.0) {
			v10 *= v3 / this->VirtActor96();
		}

		if (this->IsOnChocobo() == true
			|| this->IsOnMount() == true)
			v10 *= 1.25;
	}

	if (this->VirtActor73() == true) {
		if (v10 > 1.0) {
			v10 = 1.0;
		}
	}
	else {
		if (v10 > 3.0) {
			v10 = 3.0;
		}
	}
	this->field_824 = v10;
	return v10;
}

float FFXI::CYy::CXiSkeletonActor::GetRetIdleFrameTailQue()
{
	CYyModelBase* base = *this->Model.GetBase();
	if (base == nullptr)
	{
		return 1.0f;
	}
	
	CYyMotionQue* tailzero = base->field_24.motions[0].GetTail();
	CYyMotionQue* tailone = base->field_24.motions[1].GetTail();

	if (tailzero != nullptr) {
		if (tailone == nullptr) {
			return tailzero->FadeoutDuration;
		}

		if (tailzero->FadeoutDuration <= tailone->FadeoutDuration) {
			return tailone->FadeoutDuration;
		}
	}
	else if (tailone != nullptr) {
		return tailone->FadeoutDuration;
	}

	return 4.0f;
}

void FFXI::CYy::CXiSkeletonActor::ShowWeapon(int a2, int a3)
{
	CYyModelBase* base = *this->Model.GetBase();
	while (base != nullptr) {
		int index = 0;
		CYyModelDt* dt = *base->GetModelDt();
		while (dt != nullptr) {
			if (this->Model.GetOs2ResId(index) == (((a2 + 48) << 24) | 'pew')) {
				this->Model.IsHideOs2(index, 0, a3);
			}
			index += 1;
			dt = dt->field_4;
		}
		base = base->Previous;
	}

	bool v10{ false };
	if (this->AtelBuffer != nullptr) {
		v10 = (this->AtelBuffer->field_128 & 0x40) != 0;
	}

	if (this->GetGameStatus() != FFXI::Constants::Enums::GAME_STATUS::B_IDLE || v10 == true) {
		this->MaybeHideWepScheduler(a2);
	}
	else {
		this->MaybeShowWepScheduler(a2);
	}
}

void FFXI::CYy::CXiSkeletonActor::ShowAllWeapons(int a2)
{
	for (int i = 0; i < 9; ++i) {
		this->ShowWeapon(i, a2);
	}
}

void FFXI::CYy::CXiSkeletonActor::HideWeapon(int a2, int a3)
{
	CYyModelBase* base = *this->Model.GetBase();
	while (base != nullptr) {
		int index = 0;
		CYyModelDt* dt = *base->GetModelDt();
		while (dt != nullptr) {
			if (this->Model.GetOs2ResId(index) == (((a2 + 48) << 24) | 'pew')) {
				this->Model.IsHideOs2(index, 1, a3);
			}
			index += 1;
			dt = dt->field_4;
		}
		base = base->Previous;
	}

	this->MaybeHideWepScheduler(a2);
}

void FFXI::CYy::CXiSkeletonActor::HideAllWeapons(int a2)
{
	for (int i = 0; i < 9; ++i) {
		this->HideWeapon(i, a2);
	}
}

bool FFXI::CYy::CXiSkeletonActor::SetSystemMotion(int a2, float a3, float a4, int a5, float a6, float a7, int a8, int a9, int a10, int a11, ResourceContainer** a12)
{
	CXiSkeletonActor::is_motion_append = 0;

	CYyModelBase* base = *this->Model.GetBase();
	int v35{ 0 };
	if (base != nullptr) {
		for (int i = 0; i < 3; ++i) {
			if (base->field_24.motions[i].IsExistZombiQue() == true) {
				v35 |= 1 << i;
			}
		}
	}

	bool v18{ false }, v34{ false };
	FFXI::Constants::Enums::ActorType actortype = this->GetType();
	if (actortype == FFXI::Constants::Enums::ActorType::SIX || actortype == FFXI::Constants::Enums::ActorType::SEVEN)
		v34 = true;

	if ( (char)this->field_88 < 0
		|| v34 == true && (a2 & 0xFF000000) == '?\0\0\0') {
		if (a12 != nullptr) {
			v18 = this->SetMotion(a12, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11);
		}
		else {
			ResourceContainer** pfile{ nullptr };
			this->VirtSkeletonActor246(&pfile);
			if (pfile != nullptr) {
				if (CMoResourceMng::IsResourceReady((CMoResource***) &pfile) == true) {
					v18 = this->SetMotion(pfile, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11);
				}
			}
		}
	}

	bool v36{ false };
	if (v18 == false) {
		CYyResfList* nextfree = (CYyResfList*)this->field_7E4->GetTail();
		while (nextfree != nullptr) {
			if (CMoResourceMng::IsResourceReady((CMoResource***)&nextfree->field_1C) == false) {
				v36 = true;
				break;
			}
			this->SetMotion(nextfree->field_1C, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11);
			nextfree = (CYyResfList*)nextfree->field_8;
		}

		if (v36 == false) {
			if (this->AtelBuffer != nullptr) {
				switch (this->GetType()) {
				case FFXI::Constants::Enums::ActorType::ZERO:
				case FFXI::Constants::Enums::ActorType::ONE:
				case FFXI::Constants::Enums::ActorType::SIX:
				case FFXI::Constants::Enums::ActorType::EIGHT:
					if (this->GetRace() < 29
						&& this->field_7E4 == 0
						&& this->VirtActor201() == false
						&& this->VirtActor204() == false
						&& this->IsFishingRod() == false
						&& this->VirtActor209() == false
						&& this->VirtActor212() == false
						&& this->VirtActor215() == false
						&& this->VirtActor219() == false
						&& this->VirtActor223() == false)
					{
						break;
					}
					[[fallthrough]];
				case FFXI::Constants::Enums::ActorType::TWO:
				case FFXI::Constants::Enums::ActorType::SEVEN: {
					ResourceContainer** pfile{ nullptr };
					this->VirtSkeletonActor246(&pfile);
					if (pfile == nullptr
						|| CMoResourceMng::IsResourceReady((CMoResource***)&pfile) == false
						|| this->SetMotion(pfile, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11) == false) {
						this->VirtActor109(&pfile);
						if (pfile != nullptr) {
							if (CMoResourceMng::IsResourceReady((CMoResource***)&pfile) == true) {
								this->SetMotion(pfile, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11);
							}
						}
					}
				}
					  break;
				default:
					break;
				}
			}
			else {
				ResourceContainer** pfile{ nullptr };
				this->VirtActor109(&pfile);
				if (pfile != nullptr) {
					if (CMoResourceMng::IsResourceReady((CMoResource***)&pfile) == true) {
						this->SetMotion(pfile, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11);
					}
				}
			}
		}
	}

	if (v36 == true)
		return true;

	base = *this->Model.GetBase();
	if (base != nullptr) {
		if ((CXiSkeletonActor::is_motion_append & 1) != 0) {
			int v30 = v35 ^ CXiSkeletonActor::is_motion_append;
			for (int i = 0; i < 3; ++i) {
				int a = 1 << i;
				if ((a & v30) != 0 && (a & v35) != 0) {
					base->field_24.motions[i].DeleteAll();
				}
			}
		}
	}

	return false;
}

bool FFXI::CYy::CXiSkeletonActor::SetMotion(ResourceContainer** a2, int a3, float a4, float a5, int a6, float a7, float a8, int a9, int a10, int a11, int a12)
{
	ResourceList v38{};
	CMoResource** v15 = (CMoResource**)a2;
	FFXI::Constants::Enums::ActorType actortype = this->GetType();
	bool returnValue{ false };
	while (true) {
		v38.PrepareFromResource(*v15, FFXI::Constants::Enums::ResourceType::Mo2, a3 & 0xFFFFFF, 0xFFFFFF);
		while (v38.field_14 != nullptr) {
			CMoMo2** rpl = (CMoMo2**)v38.field_14->Metadata.SelfReference;
			CMoMo2* mo2 = *rpl;

			int v12 = a9;
			if (v12 < 5) {
				char c1[2] = { (mo2->ResourceID >> 24) & 0xFF, 0 };
				v12 = atoi(c1);
			}

			if (v12 < 7) {
				if (v12 != 2
					|| this->CibCollect.waist_type != 0
					|| actortype != FFXI::Constants::Enums::ActorType::ZERO && actortype != FFXI::Constants::Enums::ActorType::ONE && actortype != FFXI::Constants::Enums::ActorType::SIX) {
					if (a3 != '*pmj' || v12 != 1) {
						if (((1 << v12) & CXiSkeletonActor::is_motion_append) == 0) {
							CYyModelBase* base = *this->Model.GetBase();
							if (base != nullptr) {
								if (a10 != 0) {
									base->field_24.AppendSync(this, rpl, a4, a5, a6, a7, a8, v12, a11, a12);
								}
								else {
									base->field_24.Append(this, rpl, a4, a5, a6, a7, a8, v12, a11, a12);
								}
							}
							CXiSkeletonActor::is_motion_append |= 1 << v12;
							returnValue = true;
						}
					}
				}
			}
			else {
				//Wrong layer designation
			}
			v38.GetNext(false);
		}
		CMoResource* head = (*a2)->GetParent();
		if (head == nullptr || head == *a2) {
			break;
		}
		v15 = head->Metadata.SelfReference;
		CMoResource::TraversalIdCounter -= 1;
	}

	float v42 = a5 * 0.40000001;
	if (a3 == '*pmj') {
		exit(0x100CB7F2);
	}

	return returnValue;
}

void FFXI::CYy::CXiSkeletonActor::ReverseFindRes(CMoResource*** a2, Constants::Enums::ResourceType a3, int a4, int a5, int a6, bool a7)
{ 
	int counter = 0;
	CYyNode* node = this->field_7E4->GetTail();
	while (node != nullptr) {
		CYyResfList* list = (CYyResfList*)node;
		ResourceContainer* file{};
		if (list->field_1C != nullptr && CMoResourceMng::IsResourceReady((CMoResource***) & list->field_1C) == true) {
			file = *list->field_1C;
		}
		CMoResource** pres = file->Metadata.SelfReference;
		while (true) {
			CMoResource* res{};
			if (pres != nullptr) {
				res = *pres;
			}
			ResourceList v33{};
			v33.PrepareFromResource(res, a3, a4, a5);
			while (v33.field_14 != nullptr) {
				if (counter == a6) {
					if (a3 != Constants::Enums::ResourceType::Scheduler) {
						*a2 = v33.field_14->Metadata.SelfReference;
						return;
					}
					if (a7 == true || v33.field_14->ResourceID != this->field_7C4) {
						*a2 = v33.field_14->Metadata.SelfReference;
						return;
					}
				}
				else {
					counter += 1;
				}
				v33.GetNext(false);
			}

			CMoResource* head = res->GetParent();
			if (head == nullptr || head == res) {
				break;
			}
			pres = head->Metadata.SelfReference;
			CMoResource::TraversalIdCounter -= 1;
		}

		node = node->field_8;
	}

	ResourceContainer** pfile{};
	int v20 = a4;
	if (v20 == 'tini') {
		this->VirtActor109(&pfile);
		if (pfile != nullptr && *pfile != nullptr) {
			v20 = '0ini';
		}
	}

	this->VirtSkeletonActor246(&pfile);
	if (pfile != nullptr && CMoResourceMng::IsResourceReady((CMoResource***) & pfile) == true) {
		ResourceContainer* file{};
		if (pfile != nullptr) {
			file = *pfile;
		}
		CMoResource** pres = file->Metadata.SelfReference;
		while (true) {
			CMoResource* res{};
			if (pres != nullptr) {
				res = *pres;
			}
			ResourceList v33{};
			v33.PrepareFromResource(res, a3, v20, a5);
			while (v33.field_14 != nullptr) {
				if (counter == a6) {
					if (a3 != Constants::Enums::ResourceType::Scheduler) {
						*a2 = v33.field_14->Metadata.SelfReference;
						return;
					}
					if (a7 == true || v33.field_14->ResourceID != this->field_7C4) {
						*a2 = v33.field_14->Metadata.SelfReference;
						return;
					}
				}
				else {
					counter += 1;
				}
				v33.GetNext(false);
			}

			CMoResource* head = res->GetParent();
			if (head == nullptr || head == res) {
				break;
			}
			pres = head->Metadata.SelfReference;
			CMoResource::TraversalIdCounter -= 1;
		}
	}

	this->VirtActor109(&pfile);
	if (pfile != nullptr && CMoResourceMng::IsResourceReady((CMoResource***)&pfile) == true) {
		ResourceContainer* file{};
		if (pfile != nullptr) {
			file = *pfile;
		}
		CMoResource** pres = file->Metadata.SelfReference;
		while (true) {
			CMoResource* res{};
			if (pres != nullptr) {
				res = *pres;
			}
			ResourceList v33{};
			v33.PrepareFromResource(res, a3, v20, a5);
			while (v33.field_14 != nullptr) {
				if (counter == a6) {
					if (a3 != Constants::Enums::ResourceType::Scheduler) {
						*a2 = v33.field_14->Metadata.SelfReference;
						return;
					}
					if (a7 == true || v33.field_14->ResourceID != this->field_7C4) {
						*a2 = v33.field_14->Metadata.SelfReference;
						return;
					}
				}
				else {
					counter += 1;
				}
				v33.GetNext(false);
			}

			CMoResource* head = res->GetParent();
			if (head == nullptr || head == res) {
				break;
			}
			pres = head->Metadata.SelfReference;
			CMoResource::TraversalIdCounter -= 1;
		}
	}

	*a2 = nullptr;
}

void FFXI::CYy::CXiSkeletonActor::MaybeHideWepScheduler(int a2)
{
	if (((1 << a2) & (this->field_840 >> 8) & 0x3FF) != 0) {
		return;
	}

	unsigned int v3{};
	if (a2 == 0) {
		v3 = '20w!';
	}
	else if (a2 == 1) {
		v3 = '21w!';
	}
	else
		return;

	if (this->StartScheduler(v3, nullptr, nullptr) != 0)
		this->field_840 |= ((1 << a2) & 0x3FF) << 8;
}

void FFXI::CYy::CXiSkeletonActor::MaybeShowWepScheduler(int a2)
{
	if (((1 << a2) & (this->field_840 >> 8) & 0x3FF) == 0) {
		return;
	}

	unsigned int v3{};
	if (a2 == 0) {
		v3 = '10w!';
	}
	else if (a2 == 1) {
		v3 = '11w!';
	}
	else
		return;

	if (FFXI::Config::FsConfig::GetConfig(FFXI::Constants::Enums::FsConfigSubjects::Subject190) != 1) {
		if (this->StartScheduler(v3, nullptr, nullptr) == true) {
			v3 = ((~(1 << a2) << 8) | 0xFFFC00FF) & this->field_840;
			this->field_840 = v3;
		}
	}
	if (this->StartScheduler(v3, 0, 0) != 0)
		this->field_840 |= ((1 << a2) & 0x3FF) << 8;
}

void FFXI::CYy::CXiSkeletonActor::Thing1(D3DXVECTOR3* a2)
{
	CXiSkeletonActor* v5 = (CXiSkeletonActor*)this->field_768[1];
	if (v5 == nullptr) {
		return;
	}

	exit(0x100C78C0);
}

void FFXI::CYy::CXiSkeletonActor::Thing2(D3DXVECTOR3* a2)
{
	bool v84 = 0;
	for (int i = 2; i < 10; i++) {
		CXiSkeletonActor* sactor = (CXiSkeletonActor*)this->field_768[i];
		if (sactor != nullptr) {
			v84 = true;
			if (sactor->IsReadComplete() == false) {
				return;
			}
		}
	}

	if (v84 == false) {
		return;
	}

	exit(0x100C7CE5);
}

void FFXI::CYy::CXiSkeletonActor::Thing3(D3DXVECTOR3* a2)
{
	CXiSkeletonActor* v2 = (CXiSkeletonActor*)this->field_768[10];
	if (v2 == nullptr) {
		return;
	}

	exit(0x100C83B1);
}

void FFXI::CYy::CXiSkeletonActor::Thing4(D3DXVECTOR3* a2)
{
	CXiSkeletonActor* v3 = (CXiSkeletonActor*)this->field_768[11];
	if (v3 == nullptr) {
		return;
	}

	exit(0x100C880B);
}

void FFXI::CYy::CXiSkeletonActor::BlinkEyeProc()
{
	this->Blink_Timer -= FFXI::CYyDb::g_pCYyDb->CheckTick();
	
	//Character should blink if Blink_Timer < 0
	if (this->Blink_Timer >= 0.0) {
		return;
	}

	//Set new duration until next blink
	this->Blink_Timer = rand() % 480;

	//8AA locks blinking?
	if (this->field_8AA != 0) {
		return;
	}

	//Make sure models are completely loaded
	if (this->IsReadCompleteResList() == false) {
		return;
	}
	
	CXiSkeletonActor::is_motion_append = 0;
	
	ResourceContainer** model_file = nullptr;
	this->GetModelFile(&model_file);

	//Don't blink if no model file
	if (model_file == nullptr) {
		return;
	}

	if (CMoResourceMng::IsResourceReady((CMoResource***)&model_file) == true) {
		CMoResource** eye_resource = nullptr;
		CMoResourceMng::FindNextUnder(&eye_resource, *model_file, FFXI::Constants::Enums::ResourceType::Mo2, '3eye');
		if (eye_resource != nullptr) {
			CMoMo2** motion_resource = (CMoMo2**)eye_resource;
			this->AppendMotion(motion_resource, 1.0, 1.0, 1, 1.0, 1.0, 3);
		}
	}
}

void FFXI::CYy::CXiSkeletonActor::AppendMotion(CMoMo2** a2, float a3, float a4, int a5, float a6, float a7, int a8)
{
	CYyModelBase** model_base = this->Model.GetBase();
	if (model_base != nullptr && *model_base != nullptr) {
		(*model_base)->field_24.Append(this, a2, a3, a4, a5, a6, a7, a8, 0, 0);
	}
}

void FFXI::CYy::CXiSkeletonActor::WithMotion(CXiSkeletonActor* a2)
{
	if (a2 == nullptr) {
		return;
	}
	CYyModelBase** base = this->Model.GetBase();
	CYyModelBase** with_base = a2->Model.GetBase();

	if (base == nullptr || with_base == nullptr) {
		return;
	}

	const int queue_size = sizeof((*base)->field_24.motions) / sizeof((*base)->field_24.motions[0]);
	for (int i = 0; i < queue_size; ++i) {
		CYyMotionQue* base_tail = (*base)->field_24.motions[i].GetTail();
		if (base_tail != nullptr) {
			CYyMotionQue* with_tail = (*with_base)->field_24.motions[i].GetTail();
			if (with_tail != nullptr) {
				with_tail->Frame = base_tail->GetFrame();
			}
		}
	}
}

void FFXI::CYy::CXiSkeletonActor::WithMotion2(CXiSkeletonActor* a2)
{
	if (a2 == nullptr) {
		return;
	}
	CYyModelBase** base = this->Model.GetBase();
	CYyModelBase** with_base = a2->Model.GetBase();

	if (base == nullptr || with_base == nullptr) {
		return;
	}

	const int queue_size = sizeof((*base)->field_24.motions) / sizeof((*base)->field_24.motions[0]);
	for (int i = 0; i < queue_size; ++i) {
		CYyMotionQue* base_tail = (*base)->field_24.motions[i].GetTail();
		if (base_tail != nullptr) {
			CYyMotionQue* with_tail = (*with_base)->field_24.motions[i].GetTail();
			if (with_tail == nullptr) {
				with_tail = (*with_base)->field_24.motions[0].GetTail();
			}
			if (with_tail != nullptr) {
				float base_frame = base_tail->GetFrame();
				float with_frame = with_tail->GetFrame();
				if (with_frame <= base_frame && with_frame > 0.0) {
					base_tail->Frame = with_frame;
				}
			}
		}
	}
}

void FFXI::CYy::CXiSkeletonActor::PlayFootsteps()
{
	if (this->VirtActor142() == true) {
		return;
	}

	if (this->AtelBuffer != nullptr && (this->AtelBuffer->field_128 & 1) == 0) {
		return;
	}

	//sub //TODO
	//exit(0x100D2B5B);
}

float FFXI::CYy::CXiSkeletonActor::CalcAlphaByDistance()
{
	float v2 = (float)this->VirtActor7();
	float v37 = v2 / 128.0;

	long double v4 = this->GetCollisionSize() * 1.2;

	float some_float = 0.2;
	float v40 = 0.5;
	if (v4 > 0.5 && this->null_maxresource != nullptr) {
		some_float = v4 - 0.4;
		v40 = v4;
	}

	D3DXVECTOR4 va1{}, va2{};
	this->VirtActor103(12, &va2);

	va1.x = va2.x - CYyDb::g_pCYyDb->CameraManager->Position.x;
	va1.y = va2.y - CYyDb::g_pCYyDb->CameraManager->Position.y;
	va1.z = va2.z - CYyDb::g_pCYyDb->CameraManager->Position.z;

	va1.x *= va1.x;
	va1.y *= va1.y;
	va1.z *= va1.z;

	if (va2.y < CYyDb::g_pCYyDb->CameraManager->Position.y) {
		va1.y = 0.0;
	}

	long double v10 = sqrt(va1.z + va1.x);
	if (v10 < v40) {
		if (sqrt(va1.y) > v40) {
			return v37;
		}

		if (v10 < some_float) {
			v37 = 0.0;
		}
		else {
			float v17 = v10;
			if (v17 < 0.0) {
				v17 = 0.0;
			}
			v37 = (v17 - some_float) / (v40 - some_float) * v37;
		}
	}

	if (this->VirtActor142() == true) {
		v37 = 0.0;
	}

	if (this->AtelBuffer != nullptr) {
		if ((this->AtelBuffer->field_130 & 0x40000) != 0) {
			v37 = 0.5;
		} 
		else if ((this->AtelBuffer->field_1CC & 0xFFFFFF1F) != 0) {
			if (CXiControlActor::is_first_person_view == true) {
				CXiActor* searchactor = CXiActor::control_actor.GetSearchActor();
				if (searchactor != nullptr) {
					if (searchactor->AtelBuffer != nullptr) {
						if (this->AtelBuffer == searchactor->AtelBuffer->GetSomeAtelBuff()) {
							v37 = 0.0;
						}
					}
				}
			}

			XiAtelBuff* local = XiAtelBuff::GetLocalPlayer();
			if (local != nullptr && local != this->AtelBuffer) {
				exit(0x100CB2A7);
			}
		}
	}

	this->field_B2 &= 0xFFDF;
	if (this->AtelBuffer != nullptr) {
		if (Placeholder::sub_100861B0() == true) {
			if ((this->AtelBuffer->field_1CC & 0xFFFFFF1F) != 0) {
				XiAtelBuff* local = XiAtelBuff::GetLocalPlayer();
				if (local != nullptr && local != this->AtelBuffer) {
					int flag = local->field_13C & 0x000F0000;
					if (flag != 0) {
						if (flag != (this->AtelBuffer->field_13C & 0x000F0000)) {
							this->field_B2 |= 0x20u;
						}
					}
				}
			}
		}

		if ((this->AtelBuffer->field_13C & 0x1000000) != 0) {
			exit(0x100CB3F3);
		}
	}
	
	if (v37 > 1.0) {
		v37 = 1.0;
	}
	return v37;
}

void FFXI::CYy::CXiSkeletonActor::DeleteResp(int a2)
{
	CYyResfList* list = this->field_7E4;
	while (list != nullptr) {
		CYyNode* next = list->field_4;
		if (list->field_C == a2) {
			list->Delete((CYyNode**) & this->field_7E4);
			delete list;
		}
		list = (CYyResfList*)next;
	}
}

void FFXI::CYy::CXiSkeletonActor::DeleteAllResp()
{
	CYyResfList* list = this->field_7E4;
	while (list != nullptr) {
		CYyNode* next = list->field_4;
		list->Delete((CYyNode**)&this->field_7E4);
		delete list;
		list = (CYyResfList*)next;
	}
}

D3DCOLOR FFXI::CYy::CXiSkeletonActor::VirtActor1()
{
	return this->field_660;
}

void FFXI::CYy::CXiSkeletonActor::SetShadowAlpha(int a2)
{
	this->field_66C = a2;
}

int FFXI::CYy::CXiSkeletonActor::VirtActor6()
{
	return this->field_66C;
}

int FFXI::CYy::CXiSkeletonActor::VirtActor7()
{
	return (this->field_660 >> 24) & 0xFF;
}

void FFXI::CYy::CXiSkeletonActor::StartGenerators()
{
	ResourceContainer** file = nullptr;
	this->GetModelFile(&file);
	if (file != nullptr && *file != nullptr) {
		this->CXiActor::StartGenerators((CMoResource**)file);
	}

	CYyModelBase* pbase = *this->Model.GetBase();

	while (pbase != nullptr) {
		CYyModelDt* pdt = *pbase->GetModelDt();
		while (pdt != nullptr) {
			if (pdt->field_8 != nullptr && *pdt->field_8 != nullptr && CYy::CMoResourceMng::IsResourceReady((CMoResource***) & pdt->field_8) == true) {
				this->CXiActor::StartGenerators((CMoResource**)pdt->field_8);
			}
			pdt = pdt->field_4;
		}
		pbase = pbase->Previous;
	}
}

void FFXI::CYy::CXiSkeletonActor::KillAllGenerator()
{
	if (this->field_764 != nullptr && *this->field_764 != nullptr) {
		(*this->field_764)->DecrementReferenceCount();
		(*this->field_764)->DeactivateClone();
		this->field_764 = nullptr;
	}

	if (this->field_814 != nullptr && *this->field_814 != nullptr) {
		(*this->field_814)->DecrementReferenceCount();
		(*this->field_814)->DeactivateClone();
		this->field_814 = nullptr;
	}

	this->FFXI::CYy::CXiActor::KillAllGenerator();
	this->field_9E8 = 0;
}

void FFXI::CYy::CXiSkeletonActor::VirtActor11()
{
	this->field_734 = 0;
}

char FFXI::CYy::CXiSkeletonActor::MaybeIsEnvmap()
{
	return this->field_734 & 1;
}

char FFXI::CYy::CXiSkeletonActor::MaybeIsDistortion()
{
	return (this->field_734 >> 1) & 1;
}

char FFXI::CYy::CXiSkeletonActor::MaybeIsMasking()
{
	return (this->field_734 >> 2) & 1;
}

char FFXI::CYy::CXiSkeletonActor::VirtActor20()
{
	return (this->field_734 >> 3) & 1;
}

float FFXI::CYy::CXiSkeletonActor::GetWidthScale()
{
	CYyModelBase* base = *this->Model.GetBase();
	if (base == nullptr) {
		return 1.0;
	}

	Math::BoundingBox3D* boundingBox = base->Skeleton.GetSkeletonBoundingBox(0);
	if (boundingBox == nullptr) {
		return 1.0;
	}
	float v5 = boundingBox->Right - boundingBox->Left;
	float v6 = this->VirtActor58();
	if (v6 >= 0.0) {
		return v6 * v5 * 0.58823526;
	}

	if (this->field_760 >= 0.0) {
		return this->field_760 * v5 * 0.58823526;
	}

	return this->field_754 * v5 * 0.58823526;
}

float FFXI::CYy::CXiSkeletonActor::GetHeightScale()
{
	CYyModelBase* base = *this->Model.GetBase();
	if (base == nullptr) {
		return 1.0;
	}

	Math::BoundingBox3D* boundingBox = base->Skeleton.GetSkeletonBoundingBox(0);
	if (boundingBox == nullptr) {
		return 1.0;
	}

	float v5 = boundingBox->Top - boundingBox->Bottom;
	float v6 = this->VirtActor58();
	if (v6 >= 0.0) {
		return v6 * v5 * 0.52631581;
	}

	if (this->field_75C >= 0.0) {
		return this->field_75C * v5 * 0.52631581;
	}

	return this->field_754 * v5 * 0.52631581;
}

float FFXI::CYy::CXiSkeletonActor::GetDepthScale()
{
	CYyModelBase* base = *this->Model.GetBase();
	if (base == nullptr) {
		return 1.0;
	}

	Math::BoundingBox3D* boundingBox = base->Skeleton.GetSkeletonBoundingBox(0);
	if (boundingBox == nullptr) {
		return 1.0;
	}

	float v5 = boundingBox->Back - boundingBox->Front;
	float v6 = this->VirtActor58();
	if (v6 >= 0.0) {
		return v6 * v5 * 0.52631581;
	}

	if (this->field_758 >= 0.0) {
		return this->field_758 * v5 * 0.52631581;
	}

	return this->field_754 * v5 * 0.52631581;
}

unsigned char FFXI::CYy::CXiSkeletonActor::VirtActor31()
{
	return this->field_8A5;
}

void FFXI::CYy::CXiSkeletonActor::VirtActor32(unsigned char a2)
{
	this->field_8A5 = a2;
}

unsigned char FFXI::CYy::CXiSkeletonActor::VirtActor33()
{
	return 0;
}

unsigned char FFXI::CYy::CXiSkeletonActor::VirtActor35()
{
	return 0;
}

void FFXI::CYy::CXiSkeletonActor::VirtActor34(unsigned char a2)
{
	this->field_8A6 = a2;
}

void FFXI::CYy::CXiSkeletonActor::VirtActor36(unsigned char a2)
{
	this->field_8A7 = a2;
}

unsigned char FFXI::CYy::CXiSkeletonActor::VirtActor37()
{
	return this->field_8A8;
}

void FFXI::CYy::CXiSkeletonActor::VirtActor38(unsigned char a2)
{
	this->field_8A8 = a2;
}

unsigned char FFXI::CYy::CXiSkeletonActor::VirtActor39()
{
	return this->field_8A9;
}

void FFXI::CYy::CXiSkeletonActor::VirtActor40(unsigned char a2)
{
	this->field_8A9 = a2;
}

bool FFXI::CYy::CXiSkeletonActor::VirtActor75()
{
	return this->field_8AC & 0x02;
}

void FFXI::CYy::CXiSkeletonActor::VirtActor89()
{
	this->field_9F8 = 1;
}

void FFXI::CYy::CXiSkeletonActor::VirtActor90()
{
	this->field_A04 = 1;
}

D3DXVECTOR4* FFXI::CYy::CXiSkeletonActor::GetPos()
{
	return &this->field_5FC;
}

D3DXVECTOR4* FFXI::CYy::CXiSkeletonActor::VirtActor102()
{
	return &this->field_61C;
}

bool FFXI::CYy::CXiSkeletonActor::VirtActor103(int boneTransformId, D3DXVECTOR4* outPosition)
{
	// Initialize output position with model's base position
	*outPosition = this->Model.field_4;

	// Set up model scale with Y/Z components swapped
	D3DXVECTOR4 modelScale{};
	modelScale.x = this->Model.field_24.x;
	modelScale.y = this->Model.field_24.z;  //Note Y/Z are swapped
	modelScale.z = this->Model.field_24.y;
	modelScale.w = 1.0;

	// Get the bone position from model
	bool result = this->Model.GetBonePositionFromModelBaseIndex(0, boneTransformId, outPosition);

	// Get bone index from the model
	int boneIndex = this->Model.GetBoneIndexFromModelBaseIndex(boneTransformId, 0);

	// Default vertical offset
	float verticalOffset = 0.0;

	// Determine vertical offset based on character state
	if (this->VirtActor204() == true || this->IsOnMount() == false || boneIndex != 0) {
		if (this->VirtActor201() == false && this->IsOnChocobo() == true && boneIndex == 0) {
			verticalOffset = -1.3;
		}
		else if (this->VirtActor209() == false) {
			if (this->IsOnChair() == true && boneIndex == 0)
				verticalOffset = -0.5;
		}
		else if (this->field_768[0] == 0) {
			verticalOffset = -1.3;
		}
		else {
			// Adjust offset based on character race
			int race = this->GetRace();
			int something = this->CibCollect.GetSomething(race);
			if (something == 0 || something == 6)
				verticalOffset = -1.3;
			else
				verticalOffset = -0.5;
		}
	}
	
	// Get vertical scaling factor
	float verticalScale = this->VirtActor58();
	if (verticalScale < 0.0) {
		if (this->field_75C >= 0.0) {
			verticalScale = this->field_75C;
		}
		else {
			verticalScale = this->field_754;
		}
	}

	outPosition->y += verticalScale * verticalOffset;
	return result;
}

void FFXI::CYy::CXiSkeletonActor::GetElemLocal(unsigned int a2, D3DXVECTOR4* a3)
{
	*a3 = this->Model.field_4;
	D3DXVECTOR4 v12{};
	v12.x = this->Model.field_24.x;
	v12.y = this->Model.field_24.z;
	v12.z = this->Model.field_24.y;
	v12.w = 1.0;

	bool a = this->Model.GetBonePositionFromModelBaseIndex(0, a2, a3);
	a3->x -= this->Model.field_4.x;
	a3->y -= this->Model.field_4.y;
	a3->z -= this->Model.field_4.z;

	float a1a = 0.0;
	int sbi = this->Model.GetBoneIndexFromModelBaseIndex(a2, 0);
	if ((this->VirtActor201() == true || this->IsOnChocobo() == false) 
	&& (this->VirtActor204() == true || this->IsOnMount() == false)
	|| sbi != 0) {
			if (this->VirtActor209() == false) {
				if (this->IsOnChair() == true) {
					if (sbi == 0) {
						a1a = -0.5;
					}
			}
		}
	}
	else {
		a1a = -1.3;
	}
	
	float scale{ 0.0 };
	float v58 = this->VirtActor58();
	if (v58 >= 0.0) {
		scale = v58;
	}
	else if (this->field_75C >= 0.0) {
		scale = this->field_75C;
	}
	else {
		scale = this->field_754;
	}

	a3->y += scale * a1a;
}

bool FFXI::CYy::CXiSkeletonActor::IsReadComplete()
{
	return this->IsReadCompleteResList();
}

void FFXI::CYy::CXiSkeletonActor::ActorFindResource(CMoResource*** a2, Constants::Enums::ResourceType a3, int a4)
{
	CYyModelDt** ModelDt{ nullptr }, ** v10{ nullptr };
	CYyModelBase** p_base = this->Model.GetBase();
	if (p_base != nullptr && *p_base != nullptr) {
		CYyModelBase* v8 = *p_base;
		CYyModelDt* v11{ nullptr };
		CYyModelBase** p_Previous = p_base;
		CMoOs2** a1{ nullptr };
		CMoOs2* v13{ nullptr };
		while (1)
		{
			ModelDt = v8->GetModelDt();
			v10 = ModelDt;
			if (ModelDt)
			{
				v11 = *ModelDt;
				if (*v10)
					break;
			}
		LABEL_19:
			p_Previous = &(*p_Previous)->Previous;
			v8 = *p_Previous;
			if (!*p_Previous)
			{
				goto LABEL_21;
			}
		}

		while (1)
		{
			a1 = v11->field_8;
			if (a1)
			{
				v13 = *a1;
				if (v13)
					break;
			}
		LABEL_18:
			v10 = &(*v10)->field_4;
			v11 = *v10;
			if (!*v10)
				goto LABEL_19;
		}
		CMoResource** ParentResourceReference = v13->ParentResourceReference;
		ResourceList v21{};
		while (1)
		{
			CMoResource* v15 = ParentResourceReference ? *ParentResourceReference : nullptr;
			v21.PrepareFromResource(v15, a3, a4, -1);
			if (v21.field_14)
				break;
			
			CMoResource* Head = v15->GetParent();
			if (!Head)
				goto LABEL_18;
			if (Head == v15)
				goto LABEL_18;
			ParentResourceReference = Head->Metadata.SelfReference;
			CMoResource::TraversalIdCounter -= 1;
		}
		*a2 = v21.field_14->Metadata.SelfReference;
	}
	else {
	LABEL_21:
		ResourceContainer** p_modelfile{ nullptr };
		this->GetModelFile(&p_modelfile);
		if (p_modelfile != nullptr && CMoResourceMng::IsResourceReady((CMoResource***) & p_modelfile) == true)
		{
			(*p_modelfile)->AnotherResourceSearcher(a2, a3, a4);
		}
		else
		{
			*a2 = nullptr;
		}
	}
}

void FFXI::CYy::CXiSkeletonActor::SetMotionLock(bool a2)
{
	this->MotionLock = a2;
}

bool FFXI::CYy::CXiSkeletonActor::IsMotionLock()
{
	return this->MotionLock;
}

bool FFXI::CYy::CXiSkeletonActor::VirtActor142()
{
	return this->field_8AC & 1;
}

void FFXI::CYy::CXiSkeletonActor::OnDraw()
{
	if (this->field_8C8.field_C == nullptr) {
		this->Draw();
	}
	else {
		if (this->field_8E8 == 1) {
			this->Draw();
		}
		this->field_8E8 = 0;
	}
}

void FFXI::CYy::CXiSkeletonActor::VirtActor154(ResourceContainer**)
{
	//sub //TODO
	exit(0x103255B0);
}

bool FFXI::CYy::CXiSkeletonActor::SetAction(int a2, CXiActor* a3, void* a4)
{
	//sub //TODO
	exit(0x100CCED0);
	return false;
}

bool FFXI::CYy::CXiSkeletonActor::IsDirectionLock()
{
	return this->field_838;
}

void FFXI::CYy::CXiSkeletonActor::SetConstrain(char a2, int a3)
{
	this->field_83C[a3] = a2 == 1;
}

short FFXI::CYy::CXiSkeletonActor::IsConstrain()
{
	return *(short*)this->field_83C;
}

void FFXI::CYy::CXiSkeletonActor::VirtActor191(char a2, int a3)
{
	this->field_83C[a3 + 2] = a2 == 1;
}

char FFXI::CYy::CXiSkeletonActor::VirtActor192(int a2)
{
	return this->field_83C[a2 + 2];
}

FFXI::Math::BoundingBox3D* FFXI::CYy::CXiSkeletonActor::GetBoundingBoxByState(bool a2) {

	CYyModelBase* base = *this->Model.GetBase();
	if (base != nullptr) {
		Math::BoundingBox3D* boundingBoxTable = base->Skeleton.GetSkeletonBoundingBox(0);
		if (boundingBoxTable != nullptr) {
			int index = 0;
			switch (this->GetGameStatus()) {
			case FFXI::Constants::Enums::GAME_STATUS::N_IDLE:
			case FFXI::Constants::Enums::GAME_STATUS::EVT:
			case FFXI::Constants::Enums::GAME_STATUS::POL:
			case FFXI::Constants::Enums::GAME_STATUS::ES2:
			case FFXI::Constants::Enums::GAME_STATUS::ES5:
				index = 0;
				break;
			case FFXI::Constants::Enums::GAME_STATUS::B_IDLE:
			case FFXI::Constants::Enums::GAME_STATUS::FISHING:
			case FFXI::Constants::Enums::GAME_STATUS::FISHING1:
			case FFXI::Constants::Enums::GAME_STATUS::FISHING2:
			case FFXI::Constants::Enums::GAME_STATUS::FISHING3:
			case FFXI::Constants::Enums::GAME_STATUS::FISHING4:
			case FFXI::Constants::Enums::GAME_STATUS::FISHING5:
			case FFXI::Constants::Enums::GAME_STATUS::FISHING6:
			case FFXI::Constants::Enums::GAME_STATUS::FISH_2:
			case FFXI::Constants::Enums::GAME_STATUS::FISHF:
			case FFXI::Constants::Enums::GAME_STATUS::FISHR:
			case FFXI::Constants::Enums::GAME_STATUS::FISHL:
			case FFXI::Constants::Enums::GAME_STATUS::FISH_3:
			case FFXI::Constants::Enums::GAME_STATUS::FISH_31:
			case FFXI::Constants::Enums::GAME_STATUS::FISH_32:
			case FFXI::Constants::Enums::GAME_STATUS::FISH_33:
			case FFXI::Constants::Enums::GAME_STATUS::FISH_34:
			case FFXI::Constants::Enums::GAME_STATUS::FISH_35:
			case FFXI::Constants::Enums::GAME_STATUS::FISH_36:
				index = 1;
				break;
			case FFXI::Constants::Enums::GAME_STATUS::N_DEAD:
			case FFXI::Constants::Enums::GAME_STATUS::B_DEAD:
				index = 2;
				break;
			default:
				if (this->IsOnChocobo() == true
					|| this->IsOnMount() == true) {
					index = 1;
				}
				break;
			}
			return boundingBoxTable + index;
		}
	}

	if (a2 == true)
		return nullptr;

	return CXiSkeletonActor::skeletonActor_emulate_boundingbox;
}

FFXI::Math::BoundingBox3D* FFXI::CYy::CXiSkeletonActor::GetBoundingBoxDefaultState(bool a2)
{
	CYyModelBase* base = *this->Model.GetBase();
	if (base != nullptr) {
		Math::BoundingBox3D* boundingBox = base->Skeleton.GetSkeletonBoundingBox(0);
		if (boundingBox != nullptr) {
			return boundingBox;
		}
	}

	if (a2 == true) {
		return nullptr;
	}

	return skeletonActor_emulate_boundingbox;
}

KzCibCollect* FFXI::CYy::CXiSkeletonActor::VirtActor236()
{
	return &this->CibCollect;
}

void FFXI::CYy::CXiSkeletonActor::VirtActor237(short a2)
{
	this->field_670 = a2;
}

short FFXI::CYy::CXiSkeletonActor::VirtActor238()
{
	return this->field_670;
}

bool FFXI::CYy::CXiSkeletonActor::IsBlendNormal()
{
	return this->CibCollect.size_foot == 2 && this->CibCollect.legs4 == 2;
}

void FFXI::CYy::CXiSkeletonActor::VirtSkeletonActor246(ResourceContainer*** a2)
{
	*a2 = this->field_864;
}

int FFXI::CYy::CXiSkeletonActor::VirtSkeletonActor248()
{
	return this->field_750;
}

int FFXI::CYy::CXiSkeletonActor::VirtSkeletonActor249()
{
	return this->field_74C;
}

float FFXI::CYy::CXiSkeletonActor::VirtSkeletonActor251()
{
	return this->field_73C;
}

float FFXI::CYy::CXiSkeletonActor::VirtSkeletonActor253()
{
	return this->field_740;
}
