#pragma once
namespace FFXI {
	namespace CYy {
		/*
		*@class BaseGameObject
		* @brief Base class for all runtime - managed objects in the game engine.
		*
		*Provides runtime type information(RTTI), virtual method interfaces,
		* and serves as the foundation for the object memory management system.
		*/
		//Formerly CYyObject
		class BaseGameObject {
		public:
			/**
			 * @struct BaseGameObject::ClassInfo
			 * @brief Runtime type information structure for game objects.
			 *
			 * Provides metadata about classes including their name, size,
			 * and inheritance relationships. Used to implement runtime
			 * type checking and introspection.
			 */
			struct ClassInfo {
				/** Name of the class */
				const char* ClassName;

				/** Size of the class in bytes */
				size_t ClassSize;

				/** Pointer to the parent class info, nullptr for base classes */
				const BaseGameObject::ClassInfo* ClassParent;
			};

			/** Class descriptor for runtime type information */
			const static BaseGameObject::ClassInfo BaseGameObjectClass;

			/**
			 * @brief Get the runtime class information for this object
			 * @return Pointer to the BaseGameObject::ClassInfo containing type information
			 */
			virtual const BaseGameObject::ClassInfo* GetRuntimeClass();

			//Other virtual functions, not sure on these yet
			virtual void VObj2(int*);
			virtual unsigned int VObj3(char);
			virtual int VObj4();
			virtual void VObj5(void*);
			virtual void VObj6();

			BaseGameObject();
			virtual ~BaseGameObject();

			/**
			* @brief Check if this object is of or derived from the specified class
			* @param classType Class type to check against
			* @return true if this is an instance of the specified class or a derived class
			*/
			bool IsKindOf(const BaseGameObject::ClassInfo* classType);
		};
	}
}