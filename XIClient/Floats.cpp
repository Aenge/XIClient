#include "Values.h"

using namespace FFXI::Constants;

const float Values::ANGLE_PI = 3.1415f;
const float Values::ANGLE_2PI = 6.283f;
const float Values::ANGLE_PI_OVER_2 = 1.57075f;
const float Values::ANGLE_3PI_OVER_2 = 4.712388f;
const float Values::ANGLE_7PI_OVER_8 = 2.7488124f;
const float Values::ANGLE_5PI_OVER_8 = 1.9634376f;
const float Values::ANGLE_3PI_OVER_8 = 1.1780624f;
const float Values::ANGLE_PI_OVER_8 = 0.39269906f;
const float Values::ANGLE_PI_OVER_3 = 1.0471976f;
const float Values::ANGLE_PI_OVER_4 = 0.785375f;
const float Values::ANGLE_PI_OVER_6 = 0.5235988f;

const float Values::ANGLE_MINUS_PI = -ANGLE_PI;
const float Values::ANGLE_MINUS_2PI = -ANGLE_2PI;
const float Values::ANGLE_MINUS_PI_OVER_2 = -ANGLE_PI_OVER_2;
const float Values::ANGLE_MINUS_7PI_OVER_8 = -ANGLE_7PI_OVER_8;
const float Values::ANGLE_MINUS_5PI_OVER_8 = -ANGLE_5PI_OVER_8;
const float Values::ANGLE_MINUS_3PI_OVER_8 = -ANGLE_3PI_OVER_8;
const float Values::ANGLE_MINUS_PI_OVER_8 = -ANGLE_PI_OVER_8;
const float Values::ANGLE_MINUS_PI_OVER_4 = -ANGLE_PI_OVER_4;