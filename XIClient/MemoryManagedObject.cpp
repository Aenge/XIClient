#include "MemoryManagedObject.h"
#include "Globals.h"
#include "MemoryPoolManager.h"
using namespace FFXI::CYy;

void FFXI::CYy::MemoryManagedObject::DestroyObject(bool freeMemory)
{
    // Call destructor
    this->~MemoryManagedObject();

    // Free memory if required
    if (freeMemory) {
        MemoryPoolManager::globalInstance->Delete(this);
    }
}
