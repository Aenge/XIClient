#pragma once

namespace FFXI {
	namespace Input {
		class FsWordEntry {
		public:
			virtual ~FsWordEntry();
			FsWordEntry();
			int field_4;
			int field_8;
			int field_C;
			int field_10;
			char field_14;
			char field_15;
			char field_16;
			char field_17;
		};
	}
}