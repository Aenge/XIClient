#pragma once
#define WIN32_LEAN_AND_MEAN
#include "d3dx9math.h"
#include "GAME_STATUS.h"

namespace FFXI {
	namespace Network {
		namespace UDP {
			namespace GP_SERV_PACKETS {
				struct GP_SERV_BASE {
					unsigned short field_0;
					unsigned short field_2;
				};
				struct GP_SERV_LOGIN : public GP_SERV_BASE {
					int field_4;
					short field_8;
					unsigned char field_A;
					unsigned char field_B;
					D3DXVECTOR3 field_C;
					int field_18;
					unsigned char field_1C;
					unsigned char field_1D;
					unsigned char field_1E;
					FFXI::Constants::Enums::GAME_STATUS gameStatus;
					unsigned int field_20;
					int field_24;
					unsigned int field_28;
					int field_2C;
					int field_30;
					unsigned int field_34;
					unsigned int field_38;
					unsigned int field_3C;
					short field_40;
					unsigned short field_42;
					unsigned short ModelIDs[9];
					short field_56[5];
					short field_60;
					short field_62;
					short field_64;
					short field_66;
					short field_68;
					short field_6A;
					int field_6C;
					int field_70;
					short field_74;
					short field_76;
					int field_78;
					unsigned short field_7C;
					unsigned short field_7E;
					int field_80;
					char field_84[16];
					int field_94;
					int field_98;
					short field_9C;
					short field_9E;
					int field_A0;
					unsigned int field_A4;
					short field_A8;
					short field_AA;
					short field_AC;
					unsigned char field_AE;
					unsigned char field_AF;
					int field_B0[17];
					int field_F4;
					int field_F8;
					int field_FC;
					int field_100;
				};
				struct GP_SERV_EQUIP_CLEAR : public GP_SERV_BASE {

				};
			}
		}
	}
}