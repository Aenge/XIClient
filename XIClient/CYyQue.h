#pragma once
#include "MemoryManagedObject.h"
namespace FFXI {
	namespace CYy {
		class CXiSkeletonActor;
		class CYyQue : public MemoryManagedObject {
		public:
			static const BaseGameObject::ClassInfo CYyQueClass;
			virtual const BaseGameObject::ClassInfo* GetRuntimeClass() override;
			CYyQue();
			virtual ~CYyQue();
			void Kill(float);
			CYyQue* Previous;
			int State;
			float RemainingLifetime;
			float BlendWeight;
			float BlendDuration;
			float BlendRate;
			float BlendIntensity;
			float FadeoutDuration;
			virtual void VirtQue1() = 0;
			virtual int GetResID() = 0;
			virtual void* GetMod() = 0;
			virtual void SetMod(void*) = 0;
			virtual void SetSpeed(float) = 0;
			virtual float GetFrame() = 0;
			virtual void AdvanceAnimation(CXiSkeletonActor*, int) = 0;
			virtual void ApplyAnimationToBones(int, int, void*, CYyQue*) = 0;
			virtual bool IsFinished() = 0; //VirtQue9
			virtual void ControlBlendWeight();
		};
	}
}