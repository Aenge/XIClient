#include "TextRenderer.h"
#include "CTkAddMsgHelper.h"
#include "CTkMsgWinData.h"
#include "TextUtils.h"
#include <string>
#include <stdlib.h>

FFXI::CTk::CTkAddMsgHelper::CTkAddMsgHelper()
{
	this->field_0 = 0;
	this->field_C = 0;
	this->field_10 = 0;
	this->field_14 = 0;
	this->field_18 = 0;
	this->field_1A = 0;
	this->field_1C = 0;
	this->field_20 = 0;
	this->field_4 = CTkMsgWinData::fsGetFontWidth(CTkMsgWinData::SomeValues);
	this->field_6 = CTkMsgWinData::fsGetStrWidth(CTkMsgWinData::SomeValues);
	this->field_8[0] = 30;
	this->field_8[1] = 1;
}


FFXI::CTk::CTkAddMsgHelper::~CTkAddMsgHelper()
{
	this->Reset();
}

int FFXI::CTk::CTkAddMsgHelper::IsFinished()
{
	return this->field_20;
}

void FFXI::CTk::CTkAddMsgHelper::Reset()
{
	if (this->field_C != nullptr) {
		delete[] this->field_C;
		this->field_C = nullptr;
	}

	if (this->field_10 != nullptr) {
		delete[] this->field_10;
		this->field_10 = nullptr;
	}

	this->field_14 = 0;
	this->field_18 = 0;
	this->field_1A = 0;
	this->field_1C = 0;
	this->field_20 = 0;
	this->field_8[0] = 30;
	this->field_8[1] = 1;
}

void FFXI::CTk::CTkAddMsgHelper::Init(char* a2, char a3, int a4, void* a5)
{
	this->Reset();
	if (a5 != nullptr) {
		exit(0x10126A64);
	}
	if (a4 == 0) {
		this->field_0 = a3 != 0;
	}
	else {
		this->field_0 = 2;
	}

	if (a2 == nullptr || a2[0] == 0) {
		this->field_20 = 1;
		return;
	}

	unsigned int len{};
	GetLen(&this->field_14, &len, a2);
	this->field_C = new char[len + 1];

	if (this->field_14 != 0) {
		this->field_10 = new s1[this->field_14];
	}

	Fill(this->field_10, this->field_C, len + 1, a2);
}

unsigned short FFXI::CTk::CTkAddMsgHelper::CreateMsg(char* a2, unsigned short a3, unsigned short a4)
{
	if (this->IsFinished() != 0) {
		*a2 = 0;
		return 0;
	}
	
	unsigned short v7{ a4 };
	unsigned short v8{};
	if (this->field_0 == 2 || this->field_0 == 1 && this->field_18 != 0) {
		v7 -= this->field_4;
		strcpy_s(a2, a3, (char*)CTkMsgWinData::SomeValues);
		v8 = strlen(a2);
	}

	strncpy_s(a2 + v8, a3 - v8, this->field_8, 2);
	unsigned short v9 = v8 + 2;
	unsigned int v4{};
	unsigned short v10 = Text::TextRenderer::MsgAddLine(a2 + v9, a3 - v9, this->field_C + this->field_1A, &v4, v7, 0, 0x18u);
	if (this->field_14 != 0) {
		this->field_1C += CTkAddMsgHelper::DoSomething(a2, a3, a2, this->field_10 + this->field_1C);
	}

	if (v10 == 0) {
		*a2 = 0;
		v9 = 0;
	}

	unsigned short total = v10 + v9;
	unsigned short i = 0;
	while (i < total) {
		if (a2[i] == 30) {
			this->field_8[1] = a2[i + 1];
			i += 2;
		}
		else {
			i += Util::Text::GetCodeLen(a2[i]);
		}
	}

	this->field_1A += v4;
	if (this->field_C[this->field_1A] == 0) {
		this->field_20 = 1;
	}
	this->field_18 += 1;
}

void FFXI::CTk::CTkAddMsgHelper::GetLen(unsigned int* a1, unsigned int* a2, char* a3)
{
	*a1 = 0;
	*a2 = 0;
	unsigned int a3pos = 0;
	while (a3[a3pos] != 0) {
		switch (a3[a3pos]) {
		case 0x7Fi8:
			switch (a3[a3pos + 1]) {
			case 0xFBi8:
			case 0xFCi8:
			case 0x31i8:
			case 0x32i8:
			case 0x33i8:
			case 0x37i8:
				a3pos += 2;
				*a2 += 2;
				*a1 += 1;
				break;
			case 0x34i8:
			case 0x35i8:
			case 0x36i8:
				a3pos += 3;
				*a2 += 3;
				*a1 += 1;
				break;
			case 0x38i8:
				a3pos += 4;
				*a2 += 4;
				*a1 += 1;
				break;
			default:
				a3pos += 1;
			}
			break;
		case 0x1E:
			a3pos += 2;
			*a2 += 2;
			*a1 += 1;
			break;
		case 0x1F:
			a3pos += 2;
			*a2 += 2;
			*a1 += 1;
			break;
		default:
		{
			int codelen = Util::Text::GetCodeLen(a3[a3pos]);
			a3pos += codelen;
			*a2 += codelen;
		}
		break;
		}
	}
	
}

void FFXI::CTk::CTkAddMsgHelper::Fill(s1* a1, char* a2, unsigned int a2len, char* a3)
{
	if (a3 == nullptr) {
		*a2 = 0;
		return;
	}

	unsigned int a1pos = 0;
	unsigned int a2pos = 0;
	unsigned int a3pos = 0;
	while (a3[a3pos] != 0) {
		switch (a3[a3pos]) {
		case 0x7Fi8:
			switch (a3[a3pos + 1]) {
			case 0xFBi8:
			case 0xFCi8:
			case 0x31i8:
			case 0x32i8:
			case 0x33i8:
			case 0x37i8:
				a1[a1pos].field_0 = a2pos;
				a1[a1pos].field_4[0] = a3[a3pos];
				a1[a1pos].field_4[1] = a3[a3pos + 1];
				a2[a2pos] = 4;
				a2[a2pos + 1] = 4;
				a1pos += 1;
				a2pos += 2;
				a3pos += 2;
				break;
			case 0x34i8:
			case 0x35i8:
			case 0x36i8:
				a1[a1pos].field_0 = a2pos;
				a1[a1pos].field_4[0] = a3[a3pos];
				a1[a1pos].field_4[1] = a3[a3pos + 1];
				a1[a1pos].field_4[2] = a3[a3pos + 2];
				a2[a2pos] = 4;
				a2[a2pos + 1] = 4;
				a2[a2pos + 2] = 4;
				a1pos += 1;
				a2pos += 3;
				a3pos += 3;
				break;
			case 0x38i8:
				a1[a1pos].field_0 = a2pos;
				a1[a1pos].field_4[0] = a3[a3pos];
				a1[a1pos].field_4[1] = a3[a3pos + 1];
				a1[a1pos].field_4[2] = a3[a3pos + 2];
				a1[a1pos].field_4[3] = a3[a3pos + 3];
				a2[a2pos] = 4;
				a2[a2pos + 1] = 4;
				a2[a2pos + 2] = 4;
				a2[a2pos + 3] = 4;
				a1pos += 1;
				a2pos += 4;
				a3pos += 4;
				break;
			default:
				a3pos += 1;
				break;
			}
			break;
		case 0x1Ei8:
		case 0x1Fi8:
			a1[a1pos].field_0 = a2pos;
			a1[a1pos].field_4[0] = a3[a3pos];
			a1[a1pos].field_4[1] = a3[a3pos + 1];
			a2[a2pos] = 4;
			a2[a2pos + 1] = 4;
			a1pos += 1;
			a2pos += 2;
			a3pos += 2;
			break;
		default:
		{
			int codelen = Util::Text::GetCodeLen(a3[a3pos]);
			strncpy_s(a2 + a2pos, a2len - a2pos, a3 + a3pos, codelen);
			a2pos += codelen;
			a3pos += codelen;
		}
			break;
		}
	}

	a2[a2pos] = 0;
}

int FFXI::CTk::CTkAddMsgHelper::DoSomething(char* output, unsigned int out_len, char* input, s1* a3)
{
	int result = 0;
	unsigned int s1_pos = 0;
	unsigned int out_pos = 0;
	unsigned int in_pos = 0;
	while (input[in_pos] != 0) {
		if (input[in_pos] == 4) {
			s1* v1 = a3 + s1_pos;
			if (v1->field_4[0] == 0x7Fi8) {
				switch (v1->field_4[1]) {
				case 0xFBi8:
				case 0xFCi8:
				case '1':
				case '2':
				case '3':
				case '7':
					strncpy_s(output + out_pos, out_len - out_pos, v1->field_4, 2);
					s1_pos += 1;
					out_pos += 2;
					in_pos += 2;
					result += 1;
					break;
				case '4':
				case '5':
				case '6':
					strncpy_s(output + out_pos, out_len - out_pos, v1->field_4, 3);
					s1_pos += 1;
					out_pos += 3;
					in_pos += 3;
					result += 1;
					break;
				case '8':
					strncpy_s(output + out_pos, out_len - out_pos, v1->field_4, 4);
					s1_pos += 1;
					out_pos += 4;
					in_pos += 4;
					result += 1;
					break;
				default:
					in_pos += 1;
					break;
				}
			}
			else if (input[in_pos] == 30 || input[in_pos] == 31) {
				strncpy_s(output + out_pos, out_len - out_pos, v1->field_4, 2);
				s1_pos += 1;
				out_pos += 2;
				in_pos += 2;
				result += 1;
			}
		}
		else {
			unsigned int codelen = Util::Text::GetCodeLen(input[in_pos]);
			strncpy_s(output + out_pos, out_len - out_pos, input + in_pos, codelen);
			out_pos += codelen;
			in_pos += codelen;
		}
	}

	output[out_pos] = 0;
	return result;
}
