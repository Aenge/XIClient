#define NOMINMAX
#include "CApp.h"
#include "NT_SYS.h"
#include "Globals.h"
#include "GlobalStruct.h"
#include "InputMng.h"
#include "CYyDb.h"
#include "RegistryConfig.h"
#include "PolVars.h"
#include "CDx.h"
#include "XIFileManager.h"
#include "CEnv.h"
#include "RuntimeConfig.h"
#include "MemoryPoolManager.h"
#include <iostream>
#include <commdlg.h>
#include <timeapi.h>
#include <algorithm>

using namespace FFXI::CYy;

const BaseGameObject::ClassInfo CApp::CAppClass{ "CApp", sizeof(CApp), &BaseGameObject::BaseGameObjectClass };

//Global variables
CApp* FFXI::CYy::CApp::g_pCApp{ nullptr };
UINT FFXI::CYy::CApp::period{};
FFXI::Input::InputMng* CApp::g_pInputMng{ nullptr };
int CApp::AppState{ 0 };
FFXI::Network::NT_SYS CApp::g_NT_SYS{};
FFXI::Network::NT_SYS* CApp::g_pNT_SYS{ &CApp::g_NT_SYS };

MMRESULT InitPeriod() 
{
	FFXI::CYy::CApp::period = 0;

	timecaps_tag ptc{};
	MMRESULT result = timeGetDevCaps(&ptc, sizeof(ptc));
	if (result != MMSYSERR_NOERROR) {
		return result;
	}

	// Clamp period between minimum and maximum values
	UINT targetPeriod = std::max(1U, ptc.wPeriodMin);
	targetPeriod = std::min(targetPeriod, ptc.wPeriodMax);

	FFXI::CYy::CApp::period = targetPeriod;
	timeBeginPeriod(targetPeriod);
	return MMSYSERR_NOERROR;
}
bool FFXI::CYy::CApp::Initialize(HWND hwnd)
{
	char* mem = MemoryPoolManager::globalInstance->Get(sizeof(CYy::CApp), MemoryPoolManager::MemoryPoolType::Ex);
	if (mem == nullptr) {
		return false;
	}

	FFXI::CYy::CApp::g_pCApp = new (mem) CYy::CApp(
			FFXI::PolVars::instance.FFXIDirectory,
			FFXI::PolVars::instance.hmod,
			hwnd,
			FFXI::PolVars::instance.DateString,
			FFXI::PolVars::instance.TimeString
		);

	InitPeriod();

	return FFXI::CYy::CApp::g_pCApp != nullptr;
}

void FFXI::CYy::CApp::DestroyApp()
{
	if (FFXI::CYy::CApp::period != 0) {
		timeEndPeriod(FFXI::CYy::CApp::period);
		FFXI::CYy::CApp::period = 0;
	}

	if (FFXI::CYy::CApp::g_pCApp != nullptr) {
		FFXI::CYy::CApp::g_pCApp->DestroyObject();
		FFXI::CYy::CApp::g_pCApp = nullptr;
	}
}

const BaseGameObject::ClassInfo* CApp::GetRuntimeClass()
{
	return &CAppClass;
}

CApp::~CApp() 
{	
	CYyDb::Clean();
	if (CYyDb::g_pCYyDb != nullptr) {
		delete CYyDb::g_pCYyDb;
		CYyDb::g_pCYyDb = nullptr;
	}

	if (GetAppState() != 1 && CApp::g_pNT_SYS)
		CApp::g_pNT_SYS->End();

	if (CApp::g_pInputMng) {
		delete CApp::g_pInputMng;
		CApp::g_pInputMng = nullptr;
	}
	if (this->SomeDateString)
	{
		delete[] this->SomeDateString;
		this->SomeDateString = nullptr;
	}
	if (this->SomeTimeString)
	{
		delete[] this->SomeTimeString;
		this->SomeTimeString = nullptr;
	}
	if (this->VersionString)
	{
		delete[] this->VersionString;
		this->VersionString = nullptr;
	}

	if (Globals::g_pCenv != nullptr) {
		Globals::g_pCenv->DestroyObject();
		Globals::g_pCenv = nullptr;
	}

	if (Globals::g_hhk) {
		UnhookWindowsHookEx(Globals::g_hhk);
		Globals::g_hhk = nullptr;
	}

	if (FFXI::Config::RuntimeConfig::instance.window_mode != FFXI::Config::RuntimeConfig::WindowMode::Fullscreen)
		this->CleanupWindowRefreshThread();
}

LRESULT CALLBACK KeyboardHookCallback(int code, WPARAM wParam, LPARAM lParam) 
{
	// Not a keypress event or not in fullscreen mode
	if ((code != HC_ACTION) || (FFXI::Config::RuntimeConfig::instance.window_mode != FFXI::Config::RuntimeConfig::WindowMode::Fullscreen))
		return CallNextHookEx(Globals::g_hhk, code, wParam, lParam);

	KBDLLHOOKSTRUCT* kbHook = reinterpret_cast<KBDLLHOOKSTRUCT*>(lParam);
	DWORD vkCode = kbHook->vkCode;
	DWORD flags = kbHook->flags;

	// Check for Ctrl key being pressed
	bool ctrlPressed = (GetAsyncKeyState(VK_CONTROL) >> 15) != 0;

	// Block potentially disruptive keys and key combinations:
	
	// Block Windows keys (prevent Start menu)
	if (vkCode == VK_LWIN || vkCode == VK_RWIN)
		return 1;

	// Block Ctrl+Esc (alternative to Windows key)
	if (vkCode == VK_ESCAPE && ctrlPressed)
		return 1;

	// Block Alt+Tab (prevent window switching)
	if (vkCode == VK_TAB && (flags & LLKHF_ALTDOWN))
		return 1;

	// Block Alt+Esc (prevent minimizing)
	if (vkCode == VK_ESCAPE && (flags & LLKHF_ALTDOWN))
		return 1;

	// Block browser keys
	if (vkCode >= VK_BROWSER_BACK && vkCode <= VK_BROWSER_HOME)
		return 1;

	// Block media keys
	if (vkCode >= VK_MEDIA_NEXT_TRACK && vkCode <= VK_MEDIA_PLAY_PAUSE)
		return 1;

	// Block app launch keys
	if (vkCode >= VK_LAUNCH_MAIL && vkCode <= VK_LAUNCH_APP2)
		return 1;

	// Block specific special keys (OEM keys)
	if (vkCode >= 0xB8 && vkCode <= 0xB9) // International keys
		return 1;

	// Block context menu key
	if (vkCode == VK_APPS)
		return 1;

	// Block sleep key
	if (vkCode == VK_SLEEP)
		return 1;

	// Block Japan/Korea specific key
	if (vkCode == 94)
		return 1;

	// Block F5 (why?)
	if (vkCode == VK_F5)
		return 1;

	// Allow all other keys to pass through
	return CallNextHookEx(Globals::g_hhk, code, wParam, lParam);
}

char VersionError[] = "Version Error!!!!\0";
CApp::CApp(char* p_FFXIDir, HINSTANCE p_hMod, HWND p_hWnd, char* p_DateString, char* p_TimeString)
{
	this->isInitialized = 0;
	this->DXdeviceReady = 1;
	
	this->FFXIDirectory = p_FFXIDir;
	this->hmod = p_hMod;
	this->hWnd = p_hWnd;

	// Set keyboard hook in release builds
	#if not defined _DEBUG
	Globals::g_hhk = SetWindowsHookExA(WH_KEYBOARD_LL, KeyboardHookCallback, p_hMod, 0);
	#endif
	
	// Copy date string
	size_t dateLen = strlen(p_DateString) + 1;
	this->SomeDateString = new char[dateLen];
	strcpy_s(this->SomeDateString, dateLen, p_DateString);

	// Copy time string
	size_t timeLen = strlen(p_TimeString) + 1;
	this->SomeTimeString = new char[timeLen];
	strcpy_s(this->SomeTimeString, timeLen, p_TimeString);

	// Get and copy version string
	char* VersionString = FFXI::File::XIFileManager::g_pXIFileManager->GetVersionString();
	if (VersionString == nullptr) 
		VersionString = VersionError;
	
	size_t versionLen = strlen(VersionString) + 1;
	this->VersionString = new char[versionLen];
	strcpy_s(this->VersionString, versionLen, VersionString);
	
	CYyDb::g_pCYyDb = nullptr;

	FFXI::GlobalStruct::g_GlobalStruct.field_0 = 0;
	FFXI::GlobalStruct::g_GlobalStruct.ClientExpansions |= 1u;

	char* mem = MemoryPoolManager::globalInstance->Get(sizeof(CEnv), MemoryPoolManager::MemoryPoolType::Ex);
	if (mem)
		Globals::g_pCenv = new (mem) CEnv();

	if (FFXI::Config::RuntimeConfig::instance.window_mode != FFXI::Config::RuntimeConfig::WindowMode::Fullscreen)
		this->SetupWindowRefreshThread(this->hWnd);

}

bool FFXI::CYy::CApp::InitInput(HINSTANCE a1)
{
	CApp::g_pInputMng = new Input::InputMng();

	if (CApp::g_pInputMng) 
		return CApp::g_pInputMng->Init(a1);
	
	return false;
}

int FFXI::CYy::CApp::GetAppState()
{
	return CApp::AppState;
}

void FFXI::CYy::CApp::SetAppState(int a1)
{
	CApp::AppState = a1;
}

void EnsureFullscreenWindowFocus(HWND windowHandle)
{
	//Skip focus manipulation if not in fullscreen mode	
	if (FFXI::Config::RuntimeConfig::instance.window_mode != FFXI::Config::RuntimeConfig::WindowMode::Fullscreen)
		return;

	// Get the currently focused window
	HWND foregroundWindow = GetForegroundWindow();

	// Get the thread IDs for both the current foreground window and our application window
	DWORD foregroundThreadId = GetWindowThreadProcessId(foregroundWindow, nullptr);
	DWORD appThreadId = GetWindowThreadProcessId(windowHandle, nullptr);

	// Attach our thread's input state to the foreground window's thread
	// This allows us to "steal" focus more reliably
	AttachThreadInput(appThreadId, foregroundThreadId, TRUE);

	// Save the current foreground lock timeout
	DWORD originalTimeout;
	SystemParametersInfoA(SPI_GETFOREGROUNDLOCKTIMEOUT, 0, &originalTimeout, 0);

	// Temporarily disable the foreground lock timeout
    // This bypasses Windows timing restrictions on focus changes
	SystemParametersInfoA(SPI_SETFOREGROUNDLOCKTIMEOUT, 0, nullptr, 0);

	// Force our window to the foreground
	SetForegroundWindow(windowHandle);

	// Restore the original foreground lock timeout
	SystemParametersInfoA(SPI_SETFOREGROUNDLOCKTIMEOUT, 0, &originalTimeout, 0);

	// Detach from the foreground thread's input state
	AttachThreadInput(appThreadId, foregroundThreadId, FALSE);
}
bool FFXI::CYy::CApp::ClientTick()
{
	// Save the current FPU control word state
	unsigned int originalFpuControl = _control87(0, 0);

	// Set specific FPU control flags (precision control, exception masks, etc.)
	// 0x8001F sets single precision and masks all floating-point exceptions
	_control87(0x8001Fu, 0x8001Fu);

	bool continueRunning = CYyDb::g_pCYyDb->MainFlow();

	// Restore the original FPU control state
	_control87(originalFpuControl, 0x8001Fu);

	if (continueRunning == false)
		return false;
	
	// Process network updates
	if (this->g_pNT_SYS != nullptr) {
		this->g_pNT_SYS->Proc();
	}

	return true;
}

int CApp::Execute()
{
	this->TryInitialize();

	if (this->isInitialized == 0) {
		return 0;
	}

	this->RunMainLoop();
}

void FFXI::CYy::CApp::TryInitialize()
{
	this->isInitialized = 0;

	CYyDb::sqTimerInit();
	EnsureFullscreenWindowFocus(this->hWnd);
	FFXI::CYy::CDx::instance->CheckDeviceCaps();

	tagMSG Msg{}, v5{};

	if (InitInput(this->hmod) == false)
		return;

	GetWindowRect(this->hWnd, &this->windowPosition);

	CYyDb::g_pCYyDb = new CYyDb();
	if (CYyDb::g_pCYyDb == nullptr)
		return;

	CYyDb::g_pCYyDb->InitSubState(0);

	char initComplete = 0;
	while (initComplete == 0) {
		if (!CYyDb::g_pCYyDb->Init(&initComplete)) {
			return;
		}
	}

	this->isInitialized = 1;
}

void FFXI::CYy::CApp::RunMainLoop()
{
	while (true)
	{
		EnsureFullscreenWindowFocus(this->hWnd);

		if (FFXI::CYy::CApp::g_pCApp->ClientTick() == false) {
			break;
		}

		//Process Windows messages
		MSG message{};
		while (PeekMessageA(&message, 0, 0, 0, PM_REMOVE) == true) {

			if (message.message == WM_PSD_FULLPAGERECT)
				//Skip message processing and do another client tick
				break;

			// Check for quit message
			if (message.message == WM_QUIT)
				return;

			//Ignore these messages
			if (message.message == WM_IME_SETCONTEXT || message.message == WM_ACTIVATEAPP)
				continue;

			// Handle screenshot key
			if ((message.message == WM_SYSKEYUP || message.message == WM_KEYUP) &&	message.wParam == VK_SNAPSHOT)
			{
				// Take screenshot
				// sub_10001CC0(this, &Read);
				exit(0x10001CC0);
				continue;
			}

			//Handle all other messages
			CApp::WinMsgHandler(&message);
			TranslateMessage(&message);
			DispatchMessageA(&message);
		}
	}
}

DWORD CALLBACK WindowRefreshThreadProc(LPVOID p_param) 
{
	CApp* app = static_cast<CApp*>(p_param);
	while (app->threadShouldExit == 0) {
		PostMessageA(app->threadTargetWindow, WM_PSD_FULLPAGERECT, NULL, NULL);
		Sleep(333u);
	}
	app->threadRunning = 0;
	return 0;
}

void FFXI::CYy::CApp::SetupWindowRefreshThread(HWND windowHandle)
{
	this->threadShouldExit = 0;
	this->threadRunning = 1;
	this->threadTargetWindow = windowHandle;

	// Create a thread to periodically refresh the window
	this->threadHandle = CreateThread(NULL, 0, WindowRefreshThreadProc, this, CREATE_SUSPENDED, NULL);

	if (this->threadHandle)
		ResumeThread(this->threadHandle);

}

void FFXI::CYy::CApp::CleanupWindowRefreshThread()
{
	this->threadShouldExit = 1;

	if (this->threadHandle == nullptr) 
		return;

	while (this->threadRunning == 1)
		Sleep(1u);

	CloseHandle(this->threadHandle);
	this->threadHandle = nullptr;
}
