#pragma once

namespace FFXI {
	namespace Math {
		struct BoundingBox3D {
			float Top;
			float Bottom;
			float Right;
			float Left;
			float Back;
			float Front;
		};
	}
}