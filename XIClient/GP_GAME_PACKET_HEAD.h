#pragma once

namespace FFXI {
	namespace Network {
		namespace UDP {
			class GP_GAME_PACKET_HEAD {
			public:
				unsigned short field_0;
				unsigned short field_2;
				unsigned int field_4;
				unsigned int field_8;
				unsigned int field_C;
				unsigned int field_10;
				unsigned int field_14;
				unsigned int field_18;
			};
		}
	}
}