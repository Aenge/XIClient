#pragma once
#include "PrimTableEntry.h"

namespace FFXI {
	namespace CTk {
		class CTkMenuCtrlData;
		class CTkMenuPrimitive;		
		class CIwPatchSub4;
		class CIwPtcBgWin;
		class CIwTopMenu;
		class CIwLobbyMenu;
		class CIwLicenceMenu;
		class CIwYesNoMenu;
		class CYkWndPartyList;
		class CTkDrawMessageWindowOne;
		class CTkDrawMessageWindowTwo;
		class CTkSpoolMsgType1;
		class CTkSpoolMsgType2;
		class CTkEventMsgType1;
		class CTkEventMsgType2;
		class CTkTarget;
		class CTkQueryWindow;
		class YkWndYesno;
		class CTkGMTellWindow;
		class CIwOkMenu;
		class CFsConf6Win;
		class CFsTitleConfigMenu;
		class CIwNetWin;
		class CIwNetWinSub1;
		class CIwOnePic;
		class CIwRaceMenu;
		class CIwFaceMenu;
		class CIwHairMenu;
		class CIwSizeMenu;
		class CIwJobsMenu;
		class CIwSelectMenu;
		class CIwChfWin;
		class CTkSubWindow;
		class CTkHelp; class CTkTitle;
		class PrimMng {
		public:
			static CTkHelp* g_pTkHelp;
			static CTkTitle* g_pTkTitle;
			static CTkSubWindow* g_pTkSubWindow;
			static CIwPtcBgWin* g_pIwPtcBgWin;
			static CIwTopMenu* g_pIwTopMenu;
			static CIwLobbyMenu* g_pIwLobbyMenu;
			static CIwPatchSub4* g_pIwPatchSub4;
			static CIwNetWin* g_pIwNetWin;
			static CIwNetWinSub1* g_pIwNetWinSub1;
			static CFsTitleConfigMenu* g_pFsTitleConfigMenu;
			static CIwOnePic* g_pIwOnePic;
			static CIwLicenceMenu* g_pIwLicenceMenu;
			static CIwYesNoMenu* g_pIwYesNoMenu;
			static CIwRaceMenu* g_pIwRaceMenu;
			static CIwFaceMenu* g_pIwFaceMenu;
			static CIwHairMenu* g_pIwHairMenu;
			static CIwSizeMenu* g_pIwSizeMenu;
			static CIwJobsMenu* g_pIwJobsMenu;
			static CYkWndPartyList* g_pYkWndPartyList;
			static CTkDrawMessageWindowOne* g_pTkDrawMessageWindow;
			static CTkDrawMessageWindowTwo* g_pTkDrawMessageWindow2;
			static CTkSpoolMsgType1* g_pTkSpoolMsg;
			static CTkSpoolMsgType2* g_pTkSpoolMsg2;
			static CTkEventMsgType1* g_pTkEventMsg;
			static CTkEventMsgType2* g_pTkEventMsg2;
			static CTkTarget* g_pTkTarget;
			static CTkQueryWindow* g_pTkQueryWindow;
			static YkWndYesno* g_pYkWndYesno;
			static CTkGMTellWindow* g_pTkGMTellWindow;
			static CIwOkMenu* g_pIwOkMenu;
			static CFsConf6Win* g_pFsConf6Win;
			static CIwSelectMenu* g_pIwSelectMenu;
			static CIwChfWin* g_pIwChfWin;
			static PrimTableEntry PrimitiveTBL[];
			static CTkMenuPrimitive* FindPrimMenu(char*, bool*);
			static void MenuPrimitiveLink(CTkMenuCtrlData*, char*);
		};
	}
}