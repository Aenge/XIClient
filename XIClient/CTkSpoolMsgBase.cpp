#include "CTkSpoolMsgBase.h"
#include "CTkMenuCtrlData.h"
#include "CTkDrawCtrlFrame.h"
#include "CTkSpoolMsgData.h"
#include "CTkDrawMessageWindowOne.h"
#include "TextRenderer.h"
#include "MojiDraw.h"
#include "CDx.h"
#include "KaWindow.h"
#include "CTkAddMsgHelper.h"
#include "PrimMng.h"

void FFXI::CTk::CTkSpoolMsgBase::StoreMsg(char* a2, TK_LOGDATAHEADER a3)
{
	int v8 = this->field_64->field_18->field_10;
	char v10[512];
	CTkAddMsgHelper v9{};
	v9.Init(a2, 1, 0, nullptr);
	for (int i = 0; i < 64; ++i) {
		if (v9.IsFinished() != 0) {
			break;
		}
		CTkSpoolMsgData* v4 = new CTkSpoolMsgData(this->field_64);
		v4->field_104 = a3;
		v9.CreateMsg(v10, sizeof(v10), (unsigned short)PrimMng::g_pTkDrawMessageWindow->field_32);
		const int array_size = sizeof(v4->field_4) / sizeof(v4->field_4[0]);
		v4->field_104.field_10 = CTkMsgWinData::SplitParse(v10, v4->field_4, array_size, nullptr, 0);
		v4->field_104.field_C = v8 + i;
		this->field_14.AddTail(v4);
		if (a2[0] == 0) {
			break;
		}
	}

	this->field_50 = a3.field_C;
}

void FFXI::CTk::CTkSpoolMsgBase::EnableChatDraw(int a2)
{
	CTkNode* node = this->field_14.GetHeadPosition();
	while (node != nullptr) {
		CTkNode* node_backup = node;
		CTkObject* next = this->field_14.GetNext(&node);
		CTkSpoolMsgData* smd = (CTkSpoolMsgData*)next;
		if (smd->field_14A == 0) {
			if (smd->field_104.field_C > a2) {
				break;
			}
			this->field_6C = 8;
			this->field_68 = node_backup;
			smd->field_14A = 1;
			this->field_4E += 1;
			if (this->field_4E > 4) {
				this->field_4E = 4;
			}
		}
	}

	this->field_5C = this->field_4E;
	if (this->field_5C > 4) {
		this->field_5C = 4;
	}

	this->field_58 = this->field_14.GetHeadPosition();
}

bool FFXI::CTk::CTkSpoolMsgBase::IsActive()
{
	if (this->MenuCtrlData == nullptr) {
		return false;
	}

	CTkDrawCtrlFrame* frame = this->MenuCtrlData->GetDrawCtrlFrame();
	if (frame == nullptr) {
		return false;
	}

	return frame->field_44 != 2 && frame->field_44 != 1;
}

FFXI::CTk::CTkSpoolMsgBase::CTkSpoolMsgBase(CTkDrawMessageWindow* a2)
	: CTkMsgBase(a2)
{
	this->field_4E = 0;
	this->field_68 = 0;
	this->field_6C = 0;
	this->field_58 = 0;
	this->field_5C = 0;
	this->field_5E = 0;
}

void FFXI::CTk::CTkSpoolMsgBase::OnInitialUpdatePrimitive()
{
	this->CTkSpoolMsgBase::OnInitialUpdatePrimitive();
	CTkDrawCtrlFrame* frame = this->MenuCtrlData->GetDrawCtrlFrame();
	if (frame != nullptr) {
		frame->field_44 = 1;
	}
}

void FFXI::CTk::CTkSpoolMsgBase::OnDestroyPrimitive()
{
	this->CTkSpoolMsgBase::OnDestroyPrimitive();
	CTkNode* node = this->field_14.GetHeadPosition();
	while (node != nullptr) {
		CTkObject* obj = this->field_14.GetNext(&node);
		this->field_14.RemoveAt(node);
		if (obj != nullptr) {
			CTkSpoolMsgData* smd = (CTkSpoolMsgData*)obj;
			delete smd;
		}
	}

	this->field_14.RemoveAll();
	this->RemoveAllMsg();
}

void FFXI::CTk::CTkSpoolMsgBase::OnDrawPrimitive()
{
	this->MenuCtrlData->GetWindowLocate(&this->field_30);
	short v4 = this->field_30.Left + 4;
	int v13 = this->field_6C + this->field_30.Bottom - 18;
	CTkDrawCtrlFrame* frame = this->MenuCtrlData->GetDrawCtrlFrame();
	//client does not check frame this for nullptr
	if (frame->field_44 == 2 || frame->field_44 == 1) {
		return;
	}

	TKRECT v10 = this->field_30;
	CTkDrawMessageWindow::LogViewportSetup(v10);
	Text::TextRenderer::PrepareDeviceForText2();
	Text::TextRenderer::g_MojiDraw->SetMojiTexture();

	int v7 = 0;
	CTkNode* node = this->field_68;
	while (node != nullptr) {
		CTkObject* prev = this->field_14.GetPrev(&node);
		CTkSpoolMsgData* smd = (CTkSpoolMsgData*)prev;
		if (smd->field_14A != 0) {
			v10.Right = 0;
			v10.Bottom = 0;
			TK_LOGDATAHEADER v19 = smd->field_104;
			unsigned int color = CTkMsgWinData::GetLineColorData(v19.field_0.field_0);
			CTkDrawMessageWindow::TkDrawStringWithCtrl(smd->field_4, v19.field_10, color, v4, v13, 0, 0, 0);
			v13 -= 16;
			v7 += 1;
			if (v7 > 4) {
				break;
			}
		}
	}

	CYy::CDx::instance->AddSetViewport(&CTkDrawMessageWindow::LogViewport1);
}

void FFXI::CTk::CTkSpoolMsgBase::OnDrawCalc(bool a2)
{
	if (this->MenuCtrlData == nullptr) {
		return;
	}

	CTkObject* v32 = CTkObList::GetAt(this->field_68);
	CTkSpoolMsgData* v28 = (CTkSpoolMsgData*)v32;

	CTkDrawCtrlFrame* frame = this->MenuCtrlData->GetDrawCtrlFrame();
	if (this->field_64 != nullptr) {
		if (this->field_64->MenuCtrlData != nullptr) {
			this->MenuCtrlData->RePosition(this->field_64->MenuCtrlData->field_3A.Left, this->field_64->MenuCtrlData->field_3A.Top);
		}
	}

	if (this->MenuCtrlData->field_6A == 0) {
		this->MenuCtrlData->EnableDrawButton(1);
		this->MenuCtrlData->field_62 = 1;
		this->MenuCtrlData->field_69 = 1;
		this->MenuCtrlData->field_6A = 1;
		this->MenuCtrlData->ResizeWindow(frame->field_1A, frame->field_1C, frame->field_22, 16 * this->field_4E + 4, 1, 0, 0);
	}

	bool v23{ false };
	short v24 = this->field_14.field_10;
	CTkNode* node = this->field_14.GetHeadPosition();
	while (node != nullptr) {
		CTkObject* next = this->field_14.GetNext(&node);
		CTkSpoolMsgData* smd = (CTkSpoolMsgData*)next;
		
		if (smd->field_14A != 1) {
			v24 -= 1;
		}
		smd->NeckCalc();

		if (v28 == smd) {
			v23 = true;
		}
	}

	if (v24 > 4) {
		v24 = 4;
	}

	this->field_4E = v24;
	unsigned int v11 = frame->Window->field_4;
	unsigned int v12 = 16 * this->field_4E + 4;
	if (v11 < v12) {
		v11 += 8;
		if (v11 > v12) {
			v11 = v12;
		}
	}
	else if (v11 > v12) {
		v11 -= 2;
		if (v11 < v12) {
			v11 = v12;
		}
	}

	//second check on menuctrldata
	if (this->MenuCtrlData == nullptr) {
		return;
	}

	this->MenuCtrlData->ResizeWindow(frame->field_1A, frame->field_1C, frame->field_22, v11, 1, 0, 0);
	this->MenuCtrlData->GetWindowLocate(&this->field_30);
	
	
	if (v23 == false) {
		CTkNode* v15{};
		node = this->field_14.GetTailPosition();
		while (node != nullptr) {
			v15 = node;
			CTkObject* prev = this->field_14.GetPrev(&node);
			CTkSpoolMsgData* smd = (CTkSpoolMsgData*)prev;
			if (smd->field_14A == 1) {
				this->field_68 = v15;
				break;
			}
		}
	}

	if (this->field_4E != 0) {
		if (frame->field_44 == 2 || frame->field_44 == 1) {
			frame->field_44 = 3;
			frame->field_48 = 20;
		}
	}
	else if (frame->field_44 == 3 || frame->field_44 == 0) {
		frame->field_44 = 2;
		frame->field_48 = 20;
	}

	if (this->field_6C != 0) {
		this->field_6C -= 4;
	}

	int i = this->field_14.field_10;
	node = this->field_14.GetHeadPosition();
	while (node != nullptr) {
		CTkNode* v20 = node;
		CTkObject* next = this->field_14.GetNext(&node);
		CTkSpoolMsgData* smd = (CTkSpoolMsgData*)next;
		if (smd->field_140 == 0 || i > 8) {
			if (this->field_68 != nullptr && this->field_68 == v20) {
				this->field_14.GetNext(&this->field_68);
			}
			this->field_14.RemoveAt(v20);
			delete smd;
		}
		i -= 1;
	}

	this->field_58 = 0;
}
