#pragma once
#define WIN32_LEAN_AND_MEAN
#include "d3dx9math.h"
namespace FFXI {
	namespace CYy {
		class ActorPosHistory {
		public:
			void Add(D3DXVECTOR3*);
			void GetOldestPos(D3DXVECTOR3*);
			D3DXVECTOR3 stack[4];
			int field_30;
			int field_34{ 0 };
		};
	}
}