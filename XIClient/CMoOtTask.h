#pragma once
#include "CMoTask.h"
#include "OTStruct.h"
namespace FFXI {
	namespace CYy {
		class CMoOtTask : public CMoTask {
		public:
			static const BaseGameObject::ClassInfo CMoOtTaskClass;
			virtual const BaseGameObject::ClassInfo* GetRuntimeClass() override;
			virtual void VirtOt1();
			CMoOtTask();
			virtual ~CMoOtTask() = default;
			OTStruct field_34;
		};
	}
}