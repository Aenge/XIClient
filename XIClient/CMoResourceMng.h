#pragma once
#include "MemoryManagedObject.h"
#include "CMoResource.h"
#include "Values.h"
#include "ISAACRandom.h"
namespace FFXI {
	namespace CTk { class _49SubList; }
	namespace CYy {
		class ResourceContainer;
		class CYyScheduler;
		class CYyOs2VtxBuffer;
		class CXiActor;
		class CMoResourceMng : public MemoryManagedObject {
		public:
			const static BaseGameObject::ClassInfo CMoResourceMngClass;
			static char SomeByte;
			static CMoResource** LastResourceCreatedByType[Constants::Values::COUNT_RESOURCE_TYPES];
			static CMoResource** MostRecentLoadedResourceReference;
			static int ActiveFileLoadTaskCount;
			static int NumFileIndex;
			static ISAACRandom rng;
			static ResourceContainer** file27;
			static CYyScheduler** damv;
			static CYyScheduler** misv;
			static CYyScheduler** curv;
			static CYyOs2VtxBuffer* os2vtxbuffers[16];		
			static int SomeFileMappingFunc(int, int);
			static bool IsResourceReady(CMoResource***);
			static CMoResource*** FindNextUnder(CMoResource***, CMoResource*, Constants::Enums::ResourceType, int);
			static void LoadStartScheduler(int, int, CXiActor*, CXiActor*, unsigned int);
			static void KillScheduler(int, int, CXiActor*, CXiActor*);
			static void SetNumFileIndex(int);
			static int GetNumFileIndex();

			virtual const BaseGameObject::ClassInfo* GetRuntimeClass() override final;
			virtual ~CMoResourceMng();
			CMoResourceMng();
			void RemoveRes(CMoResource**);
			void Unlink(CMoResource**);
			//The only difference between these two is the check on the readfile retval
			ResourceContainer*** LoadNumFile(ResourceContainer***, int);
			ResourceContainer*** LoadNumFile2(ResourceContainer***, int);
			ResourceContainer*** FindFile(ResourceContainer***, int, bool);
			ResourceContainer*** Parse(char*, int, ResourceContainer***, CMoResource**);
			ResourceContainer** GetOrLoadDatByIndex(int);
			CTk::_49SubList*** LoadNumFileGet49(int, ResourceContainer***);
			CMoResource** Link(CMoResource*);
			bool InList(CMoResource**);
			void InitUnk3(int);
			void** ReserveSlot();
			void* NextAvailableSlot;
			void* ResourceSlotPool[0x10000];
			ResourceContainer** Unknown3;
			int ResourceCount;
		};
	}
}