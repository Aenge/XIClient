#pragma once
#include "ReferenceReadBase.h"
namespace FFXI {
	namespace CYy { class CXiActor;	class ResourceContainer; }
	namespace File {
		class MotionReferenceRead : public ReferenceReadBase {
		public:
			MotionReferenceRead() = default;
			~MotionReferenceRead() = default;
			static void ReadCallback(CYy::ResourceContainer**, ReferenceReadBase*);
			int field_10;
		};
	}
}