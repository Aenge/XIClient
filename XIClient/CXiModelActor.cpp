#include "CXiModelActor.h"

const FFXI::CYy::BaseGameObject::ClassInfo FFXI::CYy::CXiModelActor::CXiModelActorClass{
	"CXiModelActor", sizeof(FFXI::CYy::CXiModelActor), &FFXI::CYy::CXiControlActor::CXiControlActorClass
};

const FFXI::CYy::BaseGameObject::ClassInfo* FFXI::CYy::CXiModelActor::GetRuntimeClass()
{
	return &FFXI::CYy::CXiModelActor::CXiModelActorClass;
}
