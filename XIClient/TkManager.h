#pragma once
#include "CTkMenuMng.h"
#include "CTkMsbDataList.h"

namespace FFXI {
	namespace CTk {
		class CTkMouse; 
		class CTkInputCtrl;
		class StDancer;
		class CTkMsgWinData;
		class FsMenuDraw;
		class TkManager {
		public:
			static void TkInit();
			static void TkEnd();
			static void TkZoneIn();
			static CTkMenuMng g_CTkMenuMng;
			static CTkMsbDataList g_CTkMenuDataList;
			static CTkMouse* g_pCTkMouse;
			static CTkInputCtrl* g_pCTkInputCtrl;
			static StDancer* g_pStDancer;
			static CTk::CTkMsgWinData* MsgWinData1;
			static CTk::CTkMsgWinData* MsgWinData2;
			static CTk::FsMenuDraw* g_pFsMenuDraw;
		};
	}
}