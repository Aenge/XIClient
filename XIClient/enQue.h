#pragma once
#include "GP_CLI_COMMAND.h"

namespace FFXI {
	namespace Network {
		class enQueBuff;
		class enQue {
		public:
			static bool enQueInit(enQue*, enQueBuff*, int, int);
			static bool enQueSpecialSet(enQue*, FFXI::Constants::Enums::GP_CLI_COMMAND);
			static enQueBuff* enQueSearch2(enQue*, FFXI::Constants::Enums::GP_CLI_COMMAND, bool, int);
			static void enQueSet(enQueBuff*, int, int);
			enQueBuff* field_0;
			int field_4;
			int field_8;
			int field_C;
			int field_10;
			int field_14;
			FFXI::Constants::Enums::GP_CLI_COMMAND field_18[10];
			short field_2C;
			short field_2E;
		};
	}
}