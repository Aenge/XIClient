#include "tex_struct.h"
#include "CYyDb.h"
#include "CDx.h"

IDirect3DTexture8* FFXI::CYy::tex_struct::g_pSomeTexture1{ nullptr };
IDirect3DTexture8* FFXI::CYy::tex_struct::g_pSomeTexture2{ nullptr };
int FFXI::CYy::tex_struct::some_static{ 0 };

void FFXI::CYy::tex_struct::clean_texs()
{
	if (tex_struct::g_pSomeTexture1 != nullptr) {
		tex_struct::g_pSomeTexture1->Release();
		tex_struct::g_pSomeTexture1 = nullptr;
	}
	if (tex_struct::g_pSomeTexture2 != nullptr) {
		tex_struct::g_pSomeTexture2->Release();
		tex_struct::g_pSomeTexture2 = nullptr;
	}
}

FFXI::CYy::tex_struct::tex_struct()
{
	if (tex_struct::g_pSomeTexture1 == nullptr) {
		unsigned short x_res = FFXI::CYyDb::g_pCYyDb->GetBackgroundXRes();
		unsigned short y_res = FFXI::CYyDb::g_pCYyDb->GetBackgroundYRes();
		tex_struct::g_pSomeTexture1 = FFXI::CYy::CDx::instance->CreateTexture(x_res, y_res, &tex_struct::g_pSomeTexture2, D3DUSAGE_RENDERTARGET, D3DPOOL_DEFAULT);
	}
	if (tex_struct::g_pSomeTexture1 != nullptr) {
		tex_struct::g_pSomeTexture1->AddRef();
	}

	this->field_4 = tex_struct::g_pSomeTexture1;

	if (tex_struct::g_pSomeTexture2 != nullptr) {
		tex_struct::g_pSomeTexture2->AddRef();
	}

	this->field_8 = tex_struct::g_pSomeTexture2;
	tex_struct::some_static = 0;
}

FFXI::CYy::tex_struct::~tex_struct()
{
	if (this->field_4 != nullptr) {
		this->field_4->Release();
		this->field_4 = nullptr;
	}
	if (this->field_8 != nullptr) {
		this->field_8->Release();
		this->field_8 = nullptr;
	}
}
