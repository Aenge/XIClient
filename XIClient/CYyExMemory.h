#pragma once
#include "MemoryBlockDescriptor.h"

namespace FFXI {
	namespace CYy {
		class CYyExMemory : public MemoryBlockDescriptor {
		public:
			const static BaseGameObject::ClassInfo CYyExMemoryClass;
			virtual const BaseGameObject::ClassInfo* GetRuntimeClass() final;
		};
	}
}