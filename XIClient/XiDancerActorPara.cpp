#include "XiDancerActorPara.h"
#include <string>
using namespace FFXI::CYy;

FFXI::CYy::XiDancerActorPara::XiDancerActorPara(char* a2, short* a3)
{
	//Copy & null terminate name
	strncpy_s(this->Name, a2, sizeof(this->Name) - 1);
	this->Name[sizeof(this->Name) - 1] = 0;

	const int array_size = sizeof(this->ModelIDs) / sizeof(ModelIDs[0]);
	for (int i = 0; i < array_size; ++i) {
		this->ModelIDs[i] = a3[i];
	}

	this->RaceGender = a3[array_size];
	this->Size = *((char*)a3 + 20);
}
