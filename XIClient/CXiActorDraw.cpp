#include "CXiActorDraw.h"
#include "CXiActor.h"
#include "FsConfig.h"
using namespace FFXI::CYy;

const BaseGameObject::ClassInfo FFXI::CYy::CXiActorDraw::CXiActorDrawClass{
	"CXiActorDraw", sizeof("CXiActorDraw"), &CMoTask::CMoTaskClass
};

const BaseGameObject::ClassInfo* FFXI::CYy::CXiActorDraw::GetRuntimeClass()
{
	return &CXiActorDraw::CXiActorDrawClass;
}

char FFXI::CYy::CXiActorDraw::OnMove()
{
	CXiActor::maybeActorDrawCount = 0;
	CXiActor::config60 = Config::FsConfig::GetConfig(Constants::Enums::FsConfigSubjects::Subject60);

	CXiActor* actor = CXiActor::top;

	while (actor != nullptr) {
		CXiActor* prev = actor->PreviousActor;
		if (prev == nullptr) {
			if (actor->field_B2 == 0) {
				if (actor->field_8C <= 0.0) {
					actor->field_B0 = 1;
					actor->field_B2 |= 1;
				}
				else {
					actor->OnDraw();
				}
			}
		}
		else if (prev->field_8C < actor->field_8C) {
			if (prev->field_B2 == 0) {
				if (prev->field_8C <= 0.0) {
					prev->field_B0 = 1;
					prev->field_B2 |= 1;
				}
				else {
					prev->OnDraw();
				}

				CXiActor* next = actor->NextActor;
				if (next != nullptr) {
					next->PreviousActor = prev;
				}
				else {
					CXiActor::top = prev;
				}

				CXiActor* v11 = prev->PreviousActor;
				if (v11 != nullptr) {
					v11->NextActor = actor;
				}

				actor->PreviousActor = v11;
				prev->NextActor = actor->NextActor;
				actor->NextActor = prev;

				prev = actor;
			}
		}
		else if (actor->field_B2 == 0) {
			if (actor->field_8C <= 0.0) {
				actor->field_B0 = 1;
				actor->field_B2 |= 1;
			}
			else {
				actor->OnDraw();
			}
		}

		actor = prev;
	}

	return 0;
}
