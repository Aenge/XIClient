#pragma once
#define WIN32_LEAN_AND_MEAN
#include "CMoResource.h"
#include "d3dx9math.h"
namespace FFXI {
	namespace CYy {
		class CMoD3mElem;
		class CMoD3m : public CMoResource {
		public:
			CMoD3m() = default;
			virtual ~CMoD3m() = default;
			virtual void Open() override final;
			virtual void Close() override final;
			unsigned short* GetShortPointer();
			unsigned short* GetAnotherShortPointer();
			float* GetFloatPointer();
			float GetSomeMagnitude();
			void Draw(D3DCOLOR*, CMoD3mElem*);
		};
	}
}