#pragma once
#include "CYyNode.h"
namespace FFXI {
	namespace CYy {
		class ResourceContainer;
		class CYyResfList : public CYyNode {
		public:
			static const BaseGameObject::ClassInfo CYyResfListClass;
			virtual const BaseGameObject::ClassInfo* GetRuntimeClass() override final;
			CYyResfList();
			~CYyResfList();
			int field_C;
			int field_10;
			int field_14;
			int field_18;
			ResourceContainer** field_1C;
			bool field_20;
		};
	}
}