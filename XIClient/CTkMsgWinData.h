#pragma once

namespace FFXI {
	namespace CTk {
		struct ColorTableEntry {
			short ID;
			unsigned int Color;
		};

		struct MsgKindTableEntry {
			short ID;
			char field_2;
			char field_3;
		};
		struct TK_DATAHEADER {
			unsigned char field_0{0};
			unsigned char field_1{0};
			unsigned char field_2{0};
			unsigned char field_3{0};
		};

		struct TK_LOGDATAHEADER {
			TK_DATAHEADER field_0;
			int field_4;
			int field_8;
			int field_C;
			short field_10;
			char field_12;
			char field_13;
			char field_14;
			char field_15;
			char field_16;
			char field_17;
			char field_18[32];
			int field_38;
		};

		struct TK_LOGDATA {
			TK_LOGDATAHEADER header;
			char field_3C[300];
			int field_168[300];
		};

		struct TK_LOGENTRY {
			TK_LOGDATA data;
			char field_0[0x1E8];
		};

		class CTkDrawMessageWindow;

		class CTkMsgWinData {
			static ColorTableEntry colorTable[50];
			static MsgKindTableEntry msgKindTable[194];
			static int lg_currentcolor;
			static char LogStringBuffer[300][2048];
		public:
			static char SomeValues[8];
			static unsigned int SomeColors[18];
			static unsigned int fsGetFontWidth(char*);
			static unsigned int fsGetStrWidth(char*);
			static void ClearCurrentColor();
			static unsigned int GetColorData(short);
			static unsigned char GetTextColorNo(int);
			static unsigned int GetLineColorData(short);
			static MsgKindTableEntry* GetLineKindData(short);
			static bool IsInMsgKindTable(unsigned char);
			static unsigned int SplitParse(char*, short*, unsigned int, char*, unsigned int);
			CTkMsgWinData();
			~CTkMsgWinData() = default;
			void InitAll();
			void Init();
			short* GetPrevString(short*, TK_LOGDATAHEADER**);
			bool AddString(char*, TK_DATAHEADER*, char, char, CTkDrawMessageWindow*);
			void AddLogString(CTkDrawMessageWindow*, char*, TK_LOGDATAHEADER*, char, bool);
			void logDataHeaderCalc(char*, TK_LOGDATAHEADER*, TK_DATAHEADER*);
			short GetTailBufferLineNo();
			short GetNextLineNo(short);
			void RemoveEventFlg(int);
			void AddLogModeCnt(unsigned char, unsigned short);
			TK_LOGENTRY* GetLogEntry(unsigned int);
			int field_0;
			int field_4;
			int field_8;
			int field_C;
			int field_10;
			__int16 field_14;
			__int16 field_16;
			__int16 field_18;
			__int16 field_1A;
			int field_1C;
			TK_LOGENTRY field_20[100];
			char field_32020[0x32000];
			unsigned char field_64020;
			unsigned char field_64021;
			unsigned char field_64022;
			unsigned char field_64023;
			unsigned char field_64024;
			unsigned char field_64025;
			unsigned char field_64026;
			unsigned char field_64027;
			unsigned short field_64028;
			unsigned short field_6402A;
			unsigned short field_6402C;
			unsigned short field_6402E;
			unsigned short field_64030;
			unsigned short field_64032;
			unsigned short field_64034;
			unsigned short field_64036;
			unsigned short field_64038;
			unsigned short field_6403A;
			int field_6403C;
			int field_64040;
			unsigned int field_64044;
			char field_64048;
			char field_64049;
			__int16 field_6404A;
			__int16 field_6404C;
			__int16 field_6404E;
		};
	}
}