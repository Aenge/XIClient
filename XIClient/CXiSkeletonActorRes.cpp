#include "CXiSkeletonActorRes.h"
#include "CXiSkeletonActor.h"
using namespace FFXI::CYy;

FFXI::CYy::CXiSkeletonActorRes* FFXI::CYy::CXiSkeletonActorRes::last_task{ nullptr };

FFXI::CYy::CXiSkeletonActorRes::CXiSkeletonActorRes(CXiSkeletonActor* a2, bool(__cdecl* a3)(CXiSkeletonActor*, CXiSkeletonActorRes*))
{
	this->Actor = a2;
	this->state = 0;
	this->field_40 = 0;
	this->Callback = a3;
}

FFXI::CYy::CXiSkeletonActorRes::~CXiSkeletonActorRes()
{
	if (last_task == this) {
		last_task = nullptr;
	}

	if (this->Actor != nullptr) {
		this->Actor->field_A08 = nullptr;
	}
}

char FFXI::CYy::CXiSkeletonActorRes::OnMove()
{
	if (this->Actor == nullptr) {
		if (this != nullptr) {
			delete this;
		}
		return 1;
	}

	if (this->state == 0) {
		last_task = this;
		this->state = 1;
		return 0;
	}

	if (this->state == 1) {
		this->Callback(this->Actor, this);
		this->state = 2;
		return 0;
	}

	if (this->state == 2) {
		if (this->Actor->IsReadComplete() == true) {
			delete this;
			return 1;
		}
	}

	return 0;
}
