#pragma once
#include "FsWordCompletion.h"
namespace FFXI {
	namespace Input {
		class FsHistory {
		public:
			virtual ~FsHistory();
			FsHistory();
			int field_4;
			int field_8;
			short field_C;
			short field_E;
			int field_10[375];
			int field_5EC[10];
			int field_614;
			int field_618;
		};
	}
}