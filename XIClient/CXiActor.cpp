#include "CXiSkeletonActor.h"
#include "XiAtelBuff.h"
#include "Globals.h"
#include "CYyDb.h"
#include "CXiDollActor.h"
#include "CYyScheduler.h"
#include "ResourceContainer.h"
#include "CMoResourceMng.h"
#include "ResourceList.h"
#include "CYyGenerator.h"
#include "CYyGeneratorClone.h"
#include "CMoSchedulerTask.h"
#include "PlaceHolders.h"
#include "CMoElem.h"
#include "CMoSk2.h"

using namespace FFXI::CYy;

CXiActor* CXiActor::top{ nullptr };
CMoAttachmentsSubStruct CXiActor::control_actor{};
int CXiActor::skeletonActorIndex{ 0 };
int CXiActor::config60{ 0 };
bool CXiActor::somebool{ false };
int CXiActor::maybeActorDrawCount{ 0 };
FFXI::Constants::Enums::GAME_STATUS CXiActor::emulate_game_status{ FFXI::Constants::Enums::GAME_STATUS::N_IDLE };
FFXI::Constants::Enums::ActorType CXiActor::emulate_type{ 0 };
int CXiActor::emulate_race{ 0 };
short CXiActor::emulate_item[16] = { 0 };
KzCibCollect CXiActor::emulate_cib{};
FFXI::Math::BoundingBox3D CXiActor::emulate_skeleton_bounding_boxes[3]{};
int CXiActor::some_count1[4] = { 0 };
int CXiActor::some_count2[4] = { 0 };
bool CXiActor::can_control{ true };

const BaseGameObject::ClassInfo CXiActor::CXiActorClass {
	"CXiActor", sizeof(CXiActor), &CMoTask::CMoTaskClass
};

const BaseGameObject::ClassInfo* FFXI::CYy::CXiActor::GetRuntimeClass()
{
	return &CXiActor::CXiActorClass;
}
int FFXI::CYy::CXiActor::GetActorsNum()
{
	CXiActor* actor = CXiActor::GetHead();
	int count = 0;
	while (actor) {
		++count;
		actor = actor->GetNext();
	}
	return count;
}
CXiActor* FFXI::CYy::CXiActor::GetHead()
{
	CXiActor* retval = CXiActor::top;
	if (retval == nullptr) return nullptr;

	while (retval->IsKindOf(&CXiDollActor::CXiDollActorClass)) {
		retval = retval->PreviousActor;
		if (retval == nullptr)
			return nullptr;
	}
	return retval;
}
void FFXI::CYy::CXiActor::InitActorStatics()
{
	CXiActor::top = nullptr;
	memset(CXiActor::some_count1, 0, sizeof(CXiActor::some_count1));
	memset(CXiActor::some_count2, 0, sizeof(CXiActor::some_count1));
	CXiActor::control_actor.SetActor(nullptr);
	CXiActor::skeletonActorIndex = 0;
	CXiActor::maybeActorDrawCount = 0;
	CXiActor::config60 = 50;
}

bool FFXI::CYy::CXiActor::SomeFunction(CXiActor* a1, CXiActor* a2, int a3)
{
	if (a3 == 0) {
		return true;
	}

	if ((a3 & 1) != 0) {
		Constants::Enums::GAME_STATUS status = CYy::XiAtelBuff::ActorBuffPtr[CYy::XiAtelBuff::LoginActIndex]->gameStatus;
		if (status != Constants::Enums::GAME_STATUS::B_IDLE && status != Constants::Enums::GAME_STATUS::B_DEAD) {
			return true;
		}
	}
	
	int v5 = SomeFunctionSub(a1, a2);

	const int array_size = sizeof(SomeFunctionTable) / sizeof(SomeFunctionTable[0]);
	for (int i = 0; i < array_size; i += 3) {
		int flag1 = (1 << SomeFunctionTable[i]) & a3;
		int flag2 = v5 & SomeFunctionTable[i + 1];
		int flag3 = v5 & SomeFunctionTable[i + 2];
		if (flag1 != 0 && flag2 != 0 && flag3 != 0) {
			return false;
		}
	}

	return true;
}

int FFXI::CYy::CXiActor::SomeFunctionSub(CXiActor* a1, CXiActor* a2)
{
	//sub //TODO
	Placeholder::gcGroupLookStart();
	return 0;
}

char FFXI::CYy::CXiActor::OnMove()
{
	return 0;
}

FFXI::CYy::CXiActor::~CXiActor()
{
	if (this == CXiActor::control_actor.GetSearchActor()) {
		CXiActor::control_actor.SetActor(nullptr);
	}
	if (this == CXiControlActor::user_control_target.TWOGetSearchActor()) {
		CXiControlActor::user_control_target.SetActor(nullptr);
	}

	this->DeleteAttachments();

	if (this == CXiControlActor::follow_actor.GetSearchActor()) {
		CXiActor::control_actor.SetActor(nullptr);
	}
	if (this == CXiActor::control_actor.GetSearchActor()) {
		CXiActor::control_actor.SetActor(nullptr);
	}
	if (this == CXiControlActor::user_control_target.TWOGetSearchActor()) {
		CXiControlActor::user_control_target.SetActor(nullptr);
	}
	if (Placeholder::g_pTkInputCtrl != nullptr) {
		exit(0x100814C3);
	}

	if ((this->field_88 & 2) != 0) {
		if (this->NextActor != nullptr) {
			this->NextActor->PreviousActor = this->PreviousActor;
		}
		else {
			CXiActor::top = this->PreviousActor;
		}

		if (this->PreviousActor != nullptr) {
			this->PreviousActor->NextActor = this->NextActor;
		}

		this->field_88 &= ~0x2;
	}

	this->field_5C.SetActor(nullptr);
	this->AtelBuffer = nullptr;
	this->field_B4 = nullptr;
}
FFXI::CYy::CXiActor::CXiActor()
{
	this->Init();
}

void FFXI::CYy::CXiActor::Init()
{
	D3DXVECTOR4* v2 = this->GetPos();
	D3DXVECTOR4* v3 = this->VirtActor102();

	*v2 = { 0.0, 0.0, 0.0, 1.0 };
	*v3 = { 0.0, 0.0, 0.0, 1.0 };

	this->VirtActor144(nullptr);

	if (CYyDb::g_pCYyDb->field_9 == 0) {
		this->field_B8 = 0;
	}
	else {
		//roro-thomashi
		this->field_B8 = 'ror\0';
	}

	this->field_88 &= 0xFFFFF800;
	this->field_78 = 0x80808080;
	this->field_7C = 0x80808080;
	this->field_B2 = 0;
	this->field_80 = 0;
	this->field_B4 = nullptr;
	this->field_A0 = 0;
	this->field_A8 = 0;
	this->field_84 = 0;
	this->AtelBuffer = 0;
	this->field_C0 = 0;
	this->TargetAttachments = 0;
	this->CasterAttachments = 0;

	this->VirtActor97(5.0);
	this->VirtActor95(5.0);
	this->VirtActor99(1.6666666f);
	this->VirtActor154(nullptr);
	this->Link();
	this->SetSubActorStatus(FFXI::Constants::Enums::SUBACTOR_STATUS::zero);
	//why
	if (CYyDb::g_pCYyDb->field_9 == 0)
		this->ActorsNum = CXiActor::GetActorsNum();

	this->ActorsNum = CXiActor::GetActorsNum();
}

void FFXI::CYy::CXiActor::Link()
{
	if ((this->field_88 & 2) == 0) {
		this->field_88 |= 2;

		if (CXiActor::top != nullptr) 
			CXiActor::top->NextActor = this;

		this->PreviousActor = CXiActor::top;
		this->NextActor = nullptr;
		CXiActor::top = this;

		this->ActorsNum = CXiActor::GetActorsNum();
	}
}

FFXI::Constants::Enums::GAME_STATUS FFXI::CYy::CXiActor::GetGameStatus()
{
	if (this->AtelBuffer)
		return this->AtelBuffer->gameStatus;

	return CXiActor::emulate_game_status;
}

bool FFXI::CYy::CXiActor::IsOnChocobo()
{
	if (this->AtelBuffer == nullptr)
		return false;

	return FFXI::CYy::XiAtelBuff::IsOnChocobo(this->AtelBuffer->gameStatus);
}

bool FFXI::CYy::CXiActor::IsOnMount()
{
	if (this->AtelBuffer == nullptr)
		return false;

	return FFXI::CYy::XiAtelBuff::IsOnMount(this->AtelBuffer->gameStatus);
}

bool FFXI::CYy::CXiActor::SomeEventCheck() {
	if (this->AtelBuffer == nullptr)
		return false;

	if (this->AtelBuffer->field_D4 == 0)
		return false;
	//sub //TODO
	exit(0x10082CD1);
}
FFXI::Constants::Enums::ActorType FFXI::CYy::CXiActor::GetType()
{
	if (this->AtelBuffer)
		return this->AtelBuffer->ActorType;

	return CXiActor::emulate_type;
}

int FFXI::CYy::CXiActor::GetRace()
{
	if (this->VirtActor227())
		return this->VirtActor229();

	if (this->AtelBuffer)
		return this->AtelBuffer->RaceGender;

	return CXiActor::emulate_race;
}

char FFXI::CYy::CXiActor::GetMoveMode()
{
	if (this->AtelBuffer == nullptr)
		return 0;

	return (this->AtelBuffer->field_124 >> 17) & 1;
}

bool FFXI::CYy::CXiActor::GetMonsterFlag()
{
	if (this->AtelBuffer)
		return (this->AtelBuffer->field_120 >> 13) & 1;

	return 1;
}

CXiActor* FFXI::CYy::CXiActor::GetNext()
{
	CXiActor* retval = this->PreviousActor;
	if (retval == nullptr) return nullptr;

	while (retval->IsKindOf(&CXiDollActor::CXiDollActorClass)) {
		retval = retval->PreviousActor;
		if (retval == nullptr)
			return nullptr;
	}
	return retval;

}

bool FFXI::CYy::CXiActor::InOwnActorPointers()
{
	if (this->AtelBuffer == nullptr) return false;

	int actorCount = sizeof(this->AtelBuffer->ActorPointers) / sizeof(this->AtelBuffer->ActorPointers[0]);

	for (int i = 0; i < actorCount; ++i) {
		if (this->AtelBuffer->ActorPointers[i] == this)
			return true;
	}
	return false;
}

bool FFXI::CYy::CXiActor::GetMotStop()
{
	if (this->AtelBuffer)
		return (this->AtelBuffer->field_12C >> 25) & 1;

	return false;
}

unsigned short FFXI::CYy::CXiActor::GetEquipNum(char a2)
{
	if (this->VirtActor227())
		return this->VirtActor231(a2);

	if (this->AtelBuffer)
		return this->AtelBuffer->ModelIDs[a2];

	return (a2 << 12) | CXiActor::emulate_item[a2];
}

bool FFXI::CYy::CXiActor::CheckSomeFlag1()
{
	if (this->AtelBuffer == nullptr)
		return CYyDb::g_pCYyDb->field_9 != 0;

	return this->AtelBuffer->field_128 & 0x08;
}

bool FFXI::CYy::CXiActor::CheckSomeFlag2()
{
	if (this->AtelBuffer == nullptr)
		return true;

	if ((this->AtelBuffer->field_188 & 0x08) != 0)
		return true;

	return this->AtelBuffer->field_128 & 0x10;
}

bool FFXI::CYy::CXiActor::CheckSomeFlag3()
{
	if (this->AtelBuffer == nullptr)
		return false;

	return (this->AtelBuffer->field_120 & 0x100000) != 0
		|| (this->AtelBuffer->field_120 & 0x200000) != 0
		|| (this->AtelBuffer->field_120 & 0x004000) != 0;
}

bool FFXI::CYy::CXiActor::CheckAtel120Bit5()
{
	if (this->AtelBuffer == nullptr)
		return false;

	return (this->AtelBuffer->field_120 >> 5) & 1;
}

bool FFXI::CYy::CXiActor::CheckAtel13CBit20()
{
	if (this->AtelBuffer == nullptr)
		return false;

	return (this->AtelBuffer->field_13C >> 20) & 1;
}

void FFXI::CYy::CXiActor::SetAtel13CBit20(bool a2)
{
	if (this->AtelBuffer == nullptr)
		return;

	unsigned int result = this->AtelBuffer->field_13C & 0xFFEFFFFF;
	this->AtelBuffer->field_13C = result ^ ((a2 & 1) << 20);
}

bool FFXI::CYy::CXiActor::IsOnChair()
{
	if (this->AtelBuffer == nullptr)
		return false;

	return FFXI::CYy::XiAtelBuff::IsOnChair(this->AtelBuffer->gameStatus);
}

bool FFXI::CYy::CXiActor::IsDead()
{
	if (this->AtelBuffer == nullptr)
		return false;

	return FFXI::CYy::XiAtelBuff::IsDead(this->AtelBuffer->gameStatus);
}

CXiActor* FFXI::CYy::CXiActor::Get154Actor()
{
	if (this->AtelBuffer == nullptr)
		return nullptr;

	if (this->AtelBuffer->field_1F4 == 0)
		return nullptr;

	XiAtelBuff* atel = XiAtelBuff::ActorBuffPtr[this->AtelBuffer->field_1F4];
	if (atel == nullptr)
		return nullptr;

	if ((atel->field_120 & 0x200) == 0)
		return nullptr;

	return atel->Actor;
}

bool FFXI::CYy::CXiActor::DefSchedularCall()
{
	if (this->AtelBuffer == nullptr)
		return false;

	if ((this->AtelBuffer->field_120 & 0x200) == 0)
		return false;

	if (this->AtelBuffer->gameStatus == Constants::Enums::GAME_STATUS::EFF0)
		return true;

	if (this->AtelBuffer->GetSomeFlag() != 3)
		return false;

	if ((this->AtelBuffer->field_128 & 0x20000000) != 0)
		this->AtelBuffer->Actor->SetAction('dcs@', this->AtelBuffer->Actor, nullptr);

	return true;
}

bool FFXI::CYy::CXiActor::StartScheduler(int a2, CXiActor* a3, void* a4)
{
	CYyScheduler** sched{ nullptr };
	if (a2 == '?ni!') {
		this->ActorFindResource((CMoResource***) &sched, FFXI::Constants::Enums::ResourceType::Scheduler, '1ni!');
		if (sched == nullptr) {
			this->ActorFindResource((CMoResource***)&sched, FFXI::Constants::Enums::ResourceType::Scheduler, '2ni!');
			if (sched == nullptr) {
				this->ActorFindResource((CMoResource***)&sched, FFXI::Constants::Enums::ResourceType::Scheduler, '3ni!');
			}
		}
	}
	else {
		this->ActorFindResource((CMoResource***)&sched, FFXI::Constants::Enums::ResourceType::Scheduler, a2);
	}

	if (sched != nullptr) {
		(*sched)->Execute(this, a3, 0, a4);
		return true;
	}

	if (a4 != nullptr) {
		//sub //TODO a4 is void*
		exit(0x100CCFD1);
		delete a4;
	}

	return false;
}

void FFXI::CYy::CXiActor::StartGenerators(CMoResource** a2)
{
	if (this->VirtActor63() != 0) {
		return;
	}
	
	if ((this->field_B2 & 2) != 0) {
		return;
	}

	CMoResource* v3 = nullptr;
	if (a2 != nullptr) {
		v3 = *a2;
	}

	ResourceList v10{};
	v10.PrepareFromResource(v3, FFXI::Constants::Enums::ResourceType::Generater, 0, -1);
	CMoResource* next = v10.GetNext(false);
	while (next != nullptr) {
		if ((next->Metadata.StateFlags & CMoResource::ResourceStateFlags::RESOURCE_CLONED) == 0) {
			CYyGeneratorClone* gclone = (CYyGeneratorClone*)next;
			//field_4 here is from the attachments
			int some_value = (gclone->field_4 & 0x10000) >> 12;
			some_value += (gclone->field_4 & 0xF);
			switch (some_value) {
			case 1:
			case 2:
			case 4:
			case 5:
			case 7:
			case 8:
			case 9:
			case 0xA:
			case 0xB:
			case 0xC:
			case 0x10:
			case 0x11:
			case 0x12:
			case 0x13:
				if ((gclone->flags & 0x1000) != 0) {
					gclone->DeleteClone(this, nullptr, 0, nullptr);
				}
				break;
			}
		}
		next = v10.GetNext(false);
	}

	ResourceList v9{};
	v9.PrepareFromResource(v3, FFXI::Constants::Enums::ResourceType::Generater, 0, -1);
	next = v9.GetNext(false);
	CYyGeneratorClone* clone_list[128]{};
	int list_index = 0;
	while (next != nullptr) {
		if ((next->Metadata.StateFlags & CMoResource::ResourceStateFlags::RESOURCE_CLONED) == 0) {
			CYyGeneratorClone* gclone = (CYyGeneratorClone*)next;
			//field_4 here is from the attachments
			int some_value = (gclone->field_4 & 0x10000) >> 12;
			some_value += (gclone->field_4 & 0xF);
			switch (some_value) {
			case 1:
			case 2:
			case 4:
			case 5:
			case 7:
			case 8:
			case 9:
			case 0xA:
			case 0xB:
			case 0xC:
			case 0x10:
			case 0x11:
			case 0x12:
			case 0x13:
				if ((gclone->flags & 0x1000) != 0) {
					clone_list[list_index] = gclone;
					list_index += 1;
					if (list_index >= 128) {
						//Too many generators in the character
						next = nullptr;
						continue;
					}
				}
				break;
			}
		}
		next = v9.GetNext(false);
	}

	while (list_index != 0) {
		list_index -= 1;
		clone_list[list_index]->Clone(this, nullptr, 1);
	}
}

void FFXI::CYy::CXiActor::DeleteAttachments()
{
	while (this->CasterAttachments != nullptr) {
		if (this->CasterAttachments->VirtAttach3() == 0) {
			CXiActor* caster = this->CasterAttachments->GetMasterCaster();
			if (caster != nullptr) {
				caster->VirtActor240(this->CasterAttachments);
			}
		}
	}

	while (this->TargetAttachments != nullptr) {
		if (this->TargetAttachments->VirtAttach4() == 0) {
			CXiActor* target = this->TargetAttachments->GetMasterTarget();
			if (target != nullptr) {
				target->VirtActor241(this->TargetAttachments);
			}
		}
	}
}

void FFXI::CYy::CXiActor::AtelDispOn()
{
	if ((this->field_B2 & 2) != 0) {
		this->field_B2 &= ~2u;
		this->ResumeAllGenerator();
		this->ResumeAllScheduler();
		this->StartGenerators();
	}

	this->field_B2 &= ~2u;
}

void FFXI::CYy::CXiActor::AtelDispOff()
{
	if ((this->field_B2 & 2) == 0) {
		this->KillAllGenerator();
		this->SuspendAllGenerator();
		this->SuspendAllScheduler();
	}

	this->field_B2 |= 2;
}

void FFXI::CYy::CXiActor::ResumeAllGenerator()
{
	CMoAttachments* casterAttach = this->CasterAttachments;
	while (casterAttach != nullptr) {
		BaseGameObject* obj = casterAttach->GetBaseGameObject();
		if (obj != nullptr) {
			if (obj->IsKindOf(&FFXI::CYy::CYyGeneratorClone::CYyGeneratorCloneClass) == true) {
				CYyGeneratorClone* clone = (CYyGeneratorClone*)obj;
				if (clone->IsNever() == true) {
					if (clone->field_B0 == nullptr) {
						clone->Activate();
					}
					else {
						clone->field_B0->field_17A = 0;
					}
				}
			}
		}

		casterAttach = casterAttach->CastAttach;
	}

	CMoAttachments* targetAttach = this->TargetAttachments;
	while (targetAttach != nullptr) {
		BaseGameObject* obj = targetAttach->GetBaseGameObject();
		if (obj != nullptr) {
			if (obj->IsKindOf(&FFXI::CYy::CYyGeneratorClone::CYyGeneratorCloneClass) == true) {
				CYyGeneratorClone* clone = (CYyGeneratorClone*)obj;
				if (clone->IsNever() == true) {
					if (clone->field_B0 == nullptr) {
						clone->Activate();
					}
					else {
						clone->field_B0->field_17A = 0;
					}
				}
			}
		}

		targetAttach = targetAttach->TargAttach;
	}

	if (this->IsKindOf(&CXiSkeletonActor::CXiSkeletonActorClass) == true) {
		CXiSkeletonActor* skeleactor = (CXiSkeletonActor*)this;
		if (skeleactor->field_814 == nullptr) {
			CYyGenerator** pgen = nullptr;
			this->ActorFindResource((CMoResource***) & pgen, FFXI::Constants::Enums::ResourceType::Generater, 'bivf');
			if (pgen != nullptr && *pgen != nullptr) {
				CYyGeneratorClone* clone = (*pgen)->Clone(this, this, 0);
				clone->CMoAttachments::field_34 = this;
				clone->IncrementReferenceCount();
				skeleactor->field_814 = (CYyGeneratorClone**)clone->Metadata.SelfReference;
			}
		}
	}
}

void FFXI::CYy::CXiActor::ResumeAllScheduler()
{
	CMoAttachments* casterAttach = this->CasterAttachments;
	while (casterAttach != nullptr) {
		BaseGameObject* obj = casterAttach->GetBaseGameObject();
		if (obj != nullptr) {
			if (obj->IsKindOf(&CMoSchedulerTask::CMoSchedulerTaskClass) == true) {
				CMoSchedulerTask* schedtask = (CMoSchedulerTask*)obj;
				if (schedtask->field_22 == 0) {
					schedtask->Suspend();
				}
			}
		}

		casterAttach = casterAttach->CastAttach;
	}

	CMoAttachments* targetAttach = this->TargetAttachments;
	while (targetAttach != nullptr) {
		BaseGameObject* obj = targetAttach->GetBaseGameObject();
		if (obj != nullptr) {
			if (obj->IsKindOf(&CMoSchedulerTask::CMoSchedulerTaskClass) == true) {
				CMoSchedulerTask* schedtask = (CMoSchedulerTask*)obj;
				if (schedtask->field_22 == 0) {
					schedtask->Suspend();
				}
			}
		}
		targetAttach = targetAttach->TargAttach;
	}
}

void FFXI::CYy::CXiActor::SuspendAllGenerator()
{
	CMoAttachments* casterAttach = this->CasterAttachments;
	while (casterAttach != nullptr) {
		BaseGameObject* obj = casterAttach->GetBaseGameObject();
		if (obj != nullptr) {
			if (obj->IsKindOf(&FFXI::CYy::CYyGeneratorClone::CYyGeneratorCloneClass) == true) {
				CYyGeneratorClone* clone = (CYyGeneratorClone*)obj;
				if (clone->IsNever() == true) {
					if (clone->field_B0 == nullptr) {
						clone->Unlink(false);
					}
					else {
						clone->field_B0->field_17A = 1;
					}
				}
			}
		}

		casterAttach = casterAttach->CastAttach;
	}

	CMoAttachments* targetAttach = this->TargetAttachments;
	while (targetAttach != nullptr) {
		BaseGameObject* obj = targetAttach->GetBaseGameObject();
		if (obj != nullptr) {
			if (obj->IsKindOf(&FFXI::CYy::CYyGeneratorClone::CYyGeneratorCloneClass) == true) {
				CYyGeneratorClone* clone = (CYyGeneratorClone*)obj;
				if (clone->IsNever() == true) {
					if (clone->field_B0 == nullptr) {
						clone->Unlink(false);
					}
					else {
						clone->field_B0->field_17A = 1;
					}
				}
			}
		}

		targetAttach = targetAttach->TargAttach;
	}
}

void FFXI::CYy::CXiActor::SuspendAllScheduler()
{
	CMoAttachments* casterAttach = this->CasterAttachments;
	while (casterAttach != nullptr) {
		BaseGameObject* obj = casterAttach->GetBaseGameObject();
		if (obj != nullptr) {
			if (obj->IsKindOf(&CMoSchedulerTask::CMoSchedulerTaskClass) == true) {
				CMoSchedulerTask* schedtask = (CMoSchedulerTask*)obj;
				if ((schedtask->field_80[0]->GetRoot()->Metadata.StateFlags & CMoResource::ResourceStateFlags::RESOURCE_SCHEDULED) == 0) {
					if (schedtask->field_22 == 0) {
						schedtask->Suspend();
					}
				}
			}
		}

		casterAttach = casterAttach->CastAttach;
	}

	CMoAttachments* targetAttach = this->TargetAttachments;
	while (targetAttach != nullptr) {
		BaseGameObject* obj = targetAttach->GetBaseGameObject();
		if (obj != nullptr) {
			if (obj->IsKindOf(&CMoSchedulerTask::CMoSchedulerTaskClass) == true) {
				CMoSchedulerTask* schedtask = (CMoSchedulerTask*)obj;
				if ((schedtask->field_80[0]->GetRoot()->Metadata.StateFlags & CMoResource::ResourceStateFlags::RESOURCE_SCHEDULED) == 0) {
					if (schedtask->field_22 == 0) {
						schedtask->Suspend();
					}
				}
			}
		}

		targetAttach = targetAttach->TargAttach;
	}
}

int FFXI::CYy::CXiActor::GetGmLevel()
{
	if (this->AtelBuffer == nullptr) {
		return 0;
	}

	return (this->AtelBuffer->field_128 >> 11) & 7;
}

D3DCOLOR FFXI::CYy::CXiActor::VirtActor1()
{
	return 0x80808080;
}

void FFXI::CYy::CXiActor::SetShadowAlpha(int)
{
	//nullsub
}

int FFXI::CYy::CXiActor::VirtActor6()
{
	return 128;
}

int FFXI::CYy::CXiActor::VirtActor7()
{
	return 128;
}

void FFXI::CYy::CXiActor::KillAllGenerator()
{
	CMoAttachments* casterAttach = this->CasterAttachments;
	while (casterAttach != nullptr) {
		BaseGameObject* obj = casterAttach->GetBaseGameObject();
		if (obj != nullptr) {
			if (obj->IsKindOf(&FFXI::CYy::CYyGeneratorClone::CYyGeneratorCloneClass) == true) {
				CYyGeneratorClone* clone = (CYyGeneratorClone*)obj;
				if (clone->IsNever() == false) {
					clone->DeactivateClone();
					casterAttach = this->CasterAttachments;
					continue;
				}
			}
		}

		casterAttach = casterAttach->CastAttach;
	}

	CMoAttachments* targetAttach = this->TargetAttachments;
	while (targetAttach != nullptr) {
		BaseGameObject* obj = targetAttach->GetBaseGameObject();
		if (obj != nullptr) {
			if (obj->IsKindOf(&FFXI::CYy::CYyGeneratorClone::CYyGeneratorCloneClass) == true) {
				CYyGeneratorClone* clone = (CYyGeneratorClone*)obj;
				if (clone->IsNever() == false) {
					clone->DeactivateClone();
					targetAttach = this->TargetAttachments;
					continue;
				}
			}
		}

		targetAttach = targetAttach->TargAttach;
	}
}

void FFXI::CYy::CXiActor::VirtActor11()
{
	//nullsub
}

char FFXI::CYy::CXiActor::MaybeIsEnvmap()
{
	return 0;
}

char FFXI::CYy::CXiActor::MaybeIsDistortion()
{
	return 0;
}

char FFXI::CYy::CXiActor::MaybeIsMasking()
{
	return 0;
}

char FFXI::CYy::CXiActor::VirtActor20()
{
	return 0;
}

float FFXI::CYy::CXiActor::GetWidthScale()
{
	return 1.0f;
}

float FFXI::CYy::CXiActor::GetHeightScale()
{
	return 1.0f;
}

float FFXI::CYy::CXiActor::GetDepthScale()
{
	return 1.0f;
}

unsigned char FFXI::CYy::CXiActor::VirtActor31()
{
	return 0;
}

void FFXI::CYy::CXiActor::VirtActor32(unsigned char)
{
	//nullsub
}

unsigned char FFXI::CYy::CXiActor::VirtActor33()
{
	return 0;
}

unsigned char FFXI::CYy::CXiActor::VirtActor35()
{
	return 0;
}

void FFXI::CYy::CXiActor::VirtActor34(unsigned char)
{
	//nullsub
}

void FFXI::CYy::CXiActor::VirtActor36(unsigned char)
{
	//nullsub
}

unsigned char FFXI::CYy::CXiActor::VirtActor37()
{
	return 0;
}

void FFXI::CYy::CXiActor::VirtActor38(unsigned char)
{
	//nullsub
}

unsigned char FFXI::CYy::CXiActor::VirtActor39()
{
	return 0;
}

void FFXI::CYy::CXiActor::VirtActor40(unsigned char)
{
	//nullsub
}

float FFXI::CYy::CXiActor::VirtActor58()
{
	if (this->AtelBuffer == nullptr)
		return -1.0f;

	return this->AtelBuffer->field_200;
}

char FFXI::CYy::CXiActor::VirtActor63()
{
	if (this->AtelBuffer == nullptr) {
		return 0;
	}
	
	return (this->AtelBuffer->field_130 >> 1) & 1;
}

char FFXI::CYy::CXiActor::VirtActor64()
{
	if (this->AtelBuffer == nullptr) {
		return 0;
	}

	return (this->AtelBuffer->field_120 >> 6) & 1;
}

unsigned char FFXI::CYy::CXiActor::VirtActor67()
{
	if (this->AtelBuffer == nullptr)
		return 0;

	return this->AtelBuffer->field_1B9;
}

int FFXI::CYy::CXiActor::VirtActor69()
{
	if (this->AtelBuffer == nullptr)
		return 100;

	return this->AtelBuffer->field_DC;
}

char FFXI::CYy::CXiActor::VirtActor72()
{
	if (this->AtelBuffer == nullptr) {
		//need to see where this value gets set in the client. in lobby with one char it's always zero
		exit(0x100834C4);
	}

	return (this->AtelBuffer->field_12C >> 17) & 1;
}

bool FFXI::CYy::CXiActor::VirtActor73()
{
	if (this->AtelBuffer == nullptr)
		return false;

	return this->AtelBuffer->field_20C != 0;
}

bool FFXI::CYy::CXiActor::VirtActor75()
{
	return false;
}

void FFXI::CYy::CXiActor::VirtActor80()
{
	this->field_84 += 1;
}

void FFXI::CYy::CXiActor::VirtActor81()
{
	this->field_84 -= 1;
}

void FFXI::CYy::CXiActor::VirtActor87(int)
{
	//nullsub
}

int FFXI::CYy::CXiActor::VirtActor88()
{
	return 0;
}

void FFXI::CYy::CXiActor::VirtActor89()
{
	//nullsub
}

void FFXI::CYy::CXiActor::VirtActor90()
{
	//nullsub
}

char FFXI::CYy::CXiActor::VirtActor92()
{
	return 0;
}

char FFXI::CYy::CXiActor::IsOnLift()
{
	return 0;
}

float FFXI::CYy::CXiActor::VirtActor94()
{
	return this->field_98;
}

void FFXI::CYy::CXiActor::VirtActor95(float a2)
{
	this->field_98 = a2;
}

float FFXI::CYy::CXiActor::VirtActor96()
{
	return this->field_94;
}

void FFXI::CYy::CXiActor::VirtActor97(float a2)
{
	this->field_94 = a2;
}

float FFXI::CYy::CXiActor::GetWalkSpeed()
{
	return this->field_98 / 3.0f;
}

void FFXI::CYy::CXiActor::VirtActor99(float a2)
{
	this->field_9C = a2;
}


D3DXVECTOR4* FFXI::CYy::CXiActor::GetPos()
{
	return &this->field_34;
}

D3DXVECTOR4* FFXI::CYy::CXiActor::VirtActor102()
{
	return &this->field_44;
}

bool FFXI::CYy::CXiActor::VirtActor103(int a2, D3DXVECTOR4* a3)
{
	*a3 = this->field_34;
	return 1;
}

void FFXI::CYy::CXiActor::GetElemLocal(unsigned int, D3DXVECTOR4* a2)
{
	*a2 = { 0.0, 0.0, 0.0, 1.0 };
}

void FFXI::CYy::CXiActor::VirtActor109(ResourceContainer*** a2)
{
	*a2 = this->field_B4;
}

void FFXI::CYy::CXiActor::GetModelFile(ResourceContainer*** a2)
{
	*a2 = this->field_B4;
}

bool FFXI::CYy::CXiActor::IsReadComplete()
{
	ResourceContainer** model_file = nullptr;
	this->GetModelFile(&model_file);
	if (model_file == nullptr || *model_file == nullptr) {
		return false;
	}

	return CMoResourceMng::IsResourceReady((CMoResource***) &model_file);
}

void FFXI::CYy::CXiActor::ActorFindResource(CMoResource*** a2, Constants::Enums::ResourceType a3, int a4)
{
	ResourceContainer** file{ nullptr };
	this->GetModelFile(&file);

	(*file)->AnotherResourceSearcher(a2, a3, a4);
}

void FFXI::CYy::CXiActor::SetMotionLock(bool)
{
	//nullsub
}

bool FFXI::CYy::CXiActor::IsMotionLock()
{
	return false;
}

void FFXI::CYy::CXiActor::SetGroundNormal(D3DXVECTOR4*)
{
	//nullsub
}

D3DXVECTOR4* FFXI::CYy::CXiActor::GetGroundNormal()
{
	return &this->field_34;
}

void FFXI::CYy::CXiActor::VirtActor123(int)
{
	//nullsub
}

int FFXI::CYy::CXiActor::VirtActor124()
{
	return 0;
}

void FFXI::CYy::CXiActor::VirtActor125(float)
{
	//nullsub
}

void FFXI::CYy::CXiActor::VirtActor127(D3DLIGHT8*)
{
	//nullsub
}

void FFXI::CYy::CXiActor::VirtActor128(D3DLIGHT8**)
{
	//nullsub
}

int FFXI::CYy::CXiActor::VirtActor130()
{
	return this->field_A8;
}

bool FFXI::CYy::CXiActor::VirtActor142()
{
	return false;
}

void FFXI::CYy::CXiActor::VirtActor144(CXiActor* a2)
{
	this->field_5C.SetActor(a2);
}

void FFXI::CYy::CXiActor::OnDraw()
{
	//nullsub
}

void FFXI::CYy::CXiActor::VirtActor154(ResourceContainer** a2)
{
	this->field_B4 = a2;
}

bool FFXI::CYy::CXiActor::SetAction(int a2, CXiActor* a3, void* a4)
{
	ResourceContainer** pfile{ nullptr };
	this->VirtActor109(&pfile);
	if (pfile == nullptr)
		return false;

	if (CMoResourceMng::IsResourceReady((CMoResource***) &pfile) == false)
		return false;

	ResourceContainer* file = *pfile;
	CYyScheduler** scheduler{ nullptr };
	CMoResourceMng::FindNextUnder((CMoResource***)&scheduler, file, FFXI::Constants::Enums::ResourceType::Scheduler, a2);
	if (scheduler == nullptr)
		return false;

	(*scheduler)->Execute(this, a3, 0, a4);
	return true;
}

void FFXI::CYy::CXiActor::KillAction(int a2, CXiActor* a3)
{
	if (this->IsKindOf(&CXiSkeletonActor::CXiSkeletonActorClass) == true) {
		CXiSkeletonActor* ska = (CXiSkeletonActor*)this;
		CYyScheduler** psched{};
		ska->ReverseFindRes((CMoResource***) & psched, Constants::Enums::ResourceType::Scheduler, a2, -1, 0, 0);
		if (psched != nullptr && *psched != nullptr) {
			(*psched)->Kill(this, a3);
		}
	}
	else if (this->field_B4 != nullptr) {
		CMoResource** pres{};
		CMoResourceMng::FindNextUnder(&pres, *this->field_B4, Constants::Enums::ResourceType::Scheduler, a2);
		if (pres != nullptr && *pres != nullptr) {
			CYyScheduler* sched = (CYyScheduler*)(*pres);
			sched->Kill(this, a3);
		}
	}
}

bool FFXI::CYy::CXiActor::IsMovingAction(int a2, CXiActor* a3)
{
	if (this->IsReadComplete() == false) {
		return true;
	}
	
	if (this->field_B4 == nullptr) {
		return false;
	}

	if (CMoResourceMng::IsResourceReady(&(*this->field_B4)->Metadata.SelfReference) == false) {
		return true;
	}

	CYyScheduler** psched{};
	this->ActorFindResource((CMoResource***) & psched, Constants::Enums::ResourceType::Scheduler, a2);
	if (psched != nullptr && *psched != nullptr) {
		return (*psched)->IsMoving(this, a3);
	}

	if (this->IsKindOf(&CXiSkeletonActor::CXiSkeletonActorClass) == true) {
		CXiSkeletonActor* skact = (CXiSkeletonActor*)this;
		skact->ReverseFindRes((CMoResource***) & psched, Constants::Enums::ResourceType::Scheduler, a2, -1, 0, false);
		if (psched != nullptr && *psched != nullptr) {
			return (*psched)->IsMoving(this, a3);
		}
	}

	(*CYyDb::g_pCYyDb->pCMoResourceMng->Unknown3)->AnotherResourceSearcher((CMoResource***)&psched, Constants::Enums::Scheduler, a2);
	if (psched != nullptr && *psched != nullptr) {
		return (*psched)->IsMoving(this, a3);
	}

	return false;
}

bool FFXI::CYy::CXiActor::AmIControlActor()
{
	return false;
}

void FFXI::CYy::CXiActor::StartGenerators()
{
	ResourceContainer** file = nullptr;
	this->GetModelFile(&file);
	if (file == nullptr || *file == nullptr) {
		return;
	}

	if (FFXI::CYy::CMoResourceMng::IsResourceReady((CMoResource***) & file) == true) {
		this->StartGenerators((CMoResource**)file);
	}
}

void FFXI::CYy::CXiActor::IsControlLock(bool)
{
	//nullsub
}

bool FFXI::CYy::CXiActor::IsDirectionLock()
{
	return false;
}

void FFXI::CYy::CXiActor::SetConstrain(char, int)
{
	//nullsub
}

short FFXI::CYy::CXiActor::IsConstrain()
{
	return 0;
}

int FFXI::CYy::CXiActor::IsControlLock()
{
	return 0;
}

void FFXI::CYy::CXiActor::VirtActor191(char, int)
{
	//nullsub
}

char FFXI::CYy::CXiActor::VirtActor192(int)
{
	return 0;
}

char FFXI::CYy::CXiActor::IsFreeRun()
{
	return 1;
}

void FFXI::CYy::CXiActor::IsFreeRun(char)
{
	//nullsub
}

char FFXI::CYy::CXiActor::IsWalkLock()
{
	return 0;
}

void FFXI::CYy::CXiActor::IsWalkLock(char)
{
	//nullsub
}

char FFXI::CYy::CXiActor::IsParallelMove()
{
	return 0;
}

void FFXI::CYy::CXiActor::IsParallelMove(char a2)
{
	//nullsub
}

bool FFXI::CYy::CXiActor::VirtActor201()
{
	return this->SubActorStatus >= FFXI::Constants::Enums::SUBACTOR_STATUS::one && this->SubActorStatus <= FFXI::Constants::Enums::SUBACTOR_STATUS::four;
}

void FFXI::CYy::CXiActor::SetSubActorStatus(FFXI::Constants::Enums::SUBACTOR_STATUS a2)
{
	this->SubActorStatus = a2;
}

bool FFXI::CYy::CXiActor::VirtActor203()
{
	return this->SubActorStatus == FFXI::Constants::Enums::SUBACTOR_STATUS::three || this->SubActorStatus == FFXI::Constants::Enums::SUBACTOR_STATUS::four;
}

bool FFXI::CYy::CXiActor::VirtActor204()
{
	return this->SubActorStatus == FFXI::Constants::Enums::SUBACTOR_STATUS::eightyfour;
}

bool FFXI::CYy::CXiActor::IsFishingRod()
{
	return this->SubActorStatus >= FFXI::Constants::Enums::SUBACTOR_STATUS::five && this->SubActorStatus <= FFXI::Constants::Enums::SUBACTOR_STATUS::thirtysix;
}

bool FFXI::CYy::CXiActor::VirtActor209()
{
	return this->SubActorStatus >= FFXI::Constants::Enums::SUBACTOR_STATUS::sixtyone && this->SubActorStatus <= FFXI::Constants::Enums::SUBACTOR_STATUS::eightyone;
}

bool FFXI::CYy::CXiActor::VirtActor212()
{
	return this->SubActorStatus >= FFXI::Constants::Enums::SUBACTOR_STATUS::thirtyseven && this->SubActorStatus <= FFXI::Constants::Enums::SUBACTOR_STATUS::fiftytwo;
}

bool FFXI::CYy::CXiActor::VirtActor214(int a2)
{
	if (a2 < 37 || a2 > 52)
		return false;

	return true;
}

bool FFXI::CYy::CXiActor::VirtActor215()
{
	return this->SubActorStatus >= FFXI::Constants::Enums::SUBACTOR_STATUS::fiftythree && this->SubActorStatus <= FFXI::Constants::Enums::SUBACTOR_STATUS::sixty;
}

int FFXI::CYy::CXiActor::VirtActor217()
{
	return (int)this->SubActorStatus - 53;
}

bool FFXI::CYy::CXiActor::VirtActor218(int a2)
{
	if (a2 < 53 || a2 > 60)
		return false;

	return true;
}

bool FFXI::CYy::CXiActor::VirtActor219()
{
	return this->SubActorStatus == FFXI::Constants::Enums::SUBACTOR_STATUS::eightytwo;
}

int FFXI::CYy::CXiActor::VirtActor221()
{
	return (int)this->SubActorStatus - 82;
}

bool FFXI::CYy::CXiActor::VirtActor222(int a2)
{
	if (a2 < 82 || a2 > 82)
		return false;

	return true;
}

bool FFXI::CYy::CXiActor::VirtActor223()
{
	return this->SubActorStatus == FFXI::Constants::Enums::SUBACTOR_STATUS::eightythree;
}

int FFXI::CYy::CXiActor::VirtActor225()
{
	return (int)this->SubActorStatus - 83;
}

bool FFXI::CYy::CXiActor::VirtActor226(int a2)
{
	if (a2 < 83 || a2 > 83)
		return false;

	return true;
}

bool FFXI::CYy::CXiActor::VirtActor227()
{
	return false;
}

int FFXI::CYy::CXiActor::VirtActor229()
{
	return 0;
}

unsigned short FFXI::CYy::CXiActor::VirtActor231(char)
{
	return 0;
}

FFXI::Math::BoundingBox3D* FFXI::CYy::CXiActor::GetBoundingBoxByState(bool a2) {
	return CXiActor::emulate_skeleton_bounding_boxes;
}

FFXI::Math::BoundingBox3D* FFXI::CYy::CXiActor::GetBoundingBoxDefaultState(bool)
{
	return nullptr;
}

KzCibCollect* FFXI::CYy::CXiActor::VirtActor236()
{
	return &CXiActor::emulate_cib;
}

void FFXI::CYy::CXiActor::VirtActor237(short)
{
	//nullsub
}

short FFXI::CYy::CXiActor::VirtActor238()
{
	return 0;
}

void FFXI::CYy::CXiActor::VirtActor240(CMoAttachments* a2)
{
	if (a2 == nullptr) {
		return;
	}

	CMoAttachments* attach = this->CasterAttachments;
	if (attach == nullptr) {
		return;
	}
	
	CMoAttachments* prev{ nullptr };
	while (attach != a2) {
		prev = attach;
		attach = attach->CastAttach;
		if (attach == nullptr) {
			return;
		}
	}
	if (prev != nullptr) {
		prev->CastAttach = attach->CastAttach;
	}
	else {
		this->CasterAttachments = attach->CastAttach;
	}

	a2->CastAttach = nullptr;
	a2->ClearCaster(false);
}

void FFXI::CYy::CXiActor::VirtActor241(CMoAttachments* a2)
{
	if (a2 == nullptr)
		return;

	CMoAttachments* attach = this->TargetAttachments;
	if (attach == nullptr) {
		return;
	}

	CMoAttachments* prev{ nullptr };
	while (attach != a2) {
		prev = attach;
		attach = attach->TargAttach;
		if (attach == nullptr) {
			return;
		}
	}

	if (prev != nullptr) {
		prev->TargAttach = attach->TargAttach;
	}
	else {
		this->TargetAttachments = attach->TargAttach;
	}

	a2->TargAttach = nullptr;
	a2->ClearTarget(false);
}

char FFXI::CYy::CXiActor::VirtActor242()
{
	return 0;
}

void FFXI::CYy::CXiActor::VirtActor243(char)
{
	//nullsub
}

int FFXI::CYy::CXiActor::SomeFunctionTable[51] = 
{
	1, 1, 16,
	2, 1, 32,
	3, 1, 64,
	4, 1, 128,
	5, 2, 16,
	6, 2, 32,
	7, 2, 64,
	8, 2, 128,
	9, 4, 16,
	10, 4, 32,
	11, 4, 64,
	12, 4, 128,
	13, 8, 16,
	14, 8, 32,
	15, 8, 64,
	16, 8, 128,
	17, 0, 0	
};