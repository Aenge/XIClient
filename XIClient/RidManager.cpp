#include "RidManager.h"
#include "RidListNode.h"
#include "Zoneline.h"
#include "Submodel.h"
#include "RidStruct.h"
#include "MemoryPoolManager.h"
#include <string>
#include "KO_RectData.h"
using namespace FFXI::CYy;

int RidManager::ListSize{ 0 };
RidListNode* RidManager::ListHead{ nullptr };
RidListNode* RidManager::ListTail{ nullptr };

D3DXVECTOR3 mults[] = {
	{-0.5, -0.5, -0.5},
	{-0.5, -0.5, 0.5},
	{0.5, -0.5, 0.5},
	{0.5, -0.5, -0.5},
	{-0.5, 0.5, -0.5},
	{-0.5, 0.5, 0.5},
	{0.5, 0.5, 0.5},
	{0.5, 0.5, -0.5}
};
RidListNode* CreateNode() {
	RidListNode* node = (RidListNode*)FFXI::MemoryPoolManager::Wrap(sizeof(RidListNode), FFXI::MemoryPoolManager::MemoryPoolType::Ex);
	memset(node, 0, sizeof(RidListNode));

	if (RidManager::ListSize == 0) {
		RidManager::ListHead = node;
		RidManager::ListTail = node;
		RidManager::ListSize = 1;
	}
	else {
		RidManager::ListTail->Next = node;
		RidManager::ListTail = node;
		RidManager::ListSize += 1;
	}

	return node;
}
FFXI::CYy::RidManager::RidManager()
{
	this->Init();
}

FFXI::CYy::RidManager::~RidManager()
{
	this->Clean();
}

void FFXI::CYy::RidManager::Add(char* a2)
{
	RidListNode* node = CreateNode();
	node->data = a2;
	node->field_8 = *((int*)a2 + 12);
	int v3 = 0;
	char* v6 = a2 + 100;
	for (int i = 0; i < node->field_8; ++i) {
		if (*v6 != 'm' && *v6 != 'M' || !*((int*)v6 - 6))
			++v3;
		v6 += 64;
	}

	node->field_10 = v3;
	int wrapsize = node->field_10 * sizeof(RidStruct);
	node->field_C = (RidStruct*)MemoryPoolManager::Wrap(wrapsize, MemoryPoolManager::MemoryPoolType::Ex);
	memset(node->field_C, 0, wrapsize);

	if (node->field_8 <= 0) {
		return;
	}
	
	char* v5 = a2;
	int nodeIndex = 0;
	for (int i = 0; i < node->field_8; ++i) {
		v5 += 64;
		float* floatData = (float*)v5;
		int* intData = (int*)v5;
		RidStruct* rs = node->field_C + nodeIndex;

		char v11 = v5[36];
		if (v11 != 'm' && v11 != 'M' || intData[3] == 0) {
			if (floatData[6] < 0.0) {
				floatData[6] = -floatData[6];
			}

			if (floatData[7] < 0.0) {
				floatData[7] = -floatData[7];
			}

			if (floatData[8] < 0.0) {
				floatData[8] = -floatData[8];
			}

			rs->field_0.Identity();

			rs->Position.x = floatData[0];
			rs->Position.y = floatData[1];
			rs->Position.z = floatData[2];

			FFXI::Math::WMatrix Translater{};
			D3DXMatrixTranslation(&Translater, -rs->Position.x, -rs->Position.y, -rs->Position.z);
			rs->field_0.MatrixMultiply(&Translater);

			FFXI::Math::WMatrix RotateY{};
			D3DXMatrixRotationY(&RotateY, -floatData[4]);
			rs->field_0.MatrixMultiply(&RotateY);
			
			//client does not check for zero denominators here
			float xScale = 1.0 / floatData[6];
			float yScale = 1.0 / floatData[7];
			float zScale = 1.0 / floatData[8];

			FFXI::Math::WMatrix Scaler{};
			D3DXMatrixScaling(&Scaler, xScale, yScale, zScale);
			rs->field_0.MatrixMultiply(&Scaler);

			D3DXMatrixScaling(&Scaler, floatData[6], floatData[7], floatData[8]);
			FFXI::Math::WMatrix a1{};
			a1.Identity();
			a1.MatrixMultiply(&Scaler);

			D3DXMatrixRotationY(&RotateY, floatData[4]);
			a1.MatrixMultiply(&RotateY);

			D3DXMatrixTranslation(&Translater, rs->Position.x, rs->Position.y, rs->Position.z);
			a1.MatrixMultiply(&Translater);

			const int array_size = sizeof(rs->field_4C) / sizeof(rs->field_4C[0]);
			for (int j = 0; j < array_size; ++j) {
				a1.Vec3TransformDrop4(rs->field_4C + j, mults + j);
			}

			rs->fourCC = intData[9];
			rs->field_44 = intData[10];
			rs->field_48 = intData[11];
			rs->field_E0 = 0;
			rs->field_E4 = 0;

			if (v11 == 'z' || v11 == 'Z' || v11 == 'm' || v11 == 'M') {
				for (int j = 0; j < 4; ++j) {
					((char*)&rs->field_E0)[j] = v5[36 + j];
					if (v5[36 + j] == '\0') {
						break;
					}
				}
			}

			rs->field_E8 = 0;
			rs->field_B8 = (float)*((short*)v5 + 26) * 0.00390625f + floatData[1];
			rs->field_BC = (float)*((short*)v5 + 27) * 0.00390625f + floatData[1];
			rs->field_C0 = 0;
			rs->field_C4 = 0;
			rs->field_C8 = 0;
			rs->field_CC = 0;
			rs->field_D0 = 0;
			rs->field_DC = floatData[1];
			rs->field_EC = (intData[12] << 24) >> 28;
			if ((char)rs->fourCC == '_') {
				RidManager::InitUnderscoreRid(rs, v5);
				for (int j = 0; j < rs->field_D4; ++j) {
					rs->field_D8[j].field_0 &= ~0x2000;
				}
				rs->field_F0 = 1;
			}
			else {
				rs->field_D8 = nullptr;
			}
				
			nodeIndex += 1;
		}
	}
}

void FFXI::CYy::RidManager::Clean()
{
	RidListNode* node = RidManager::ListHead;

	while (node != nullptr) {
		RidListNode* next = node->Next;
		if (node->field_10 > 0) {
			for (int i = 0; i < node->field_10; ++i) {
				if (node->field_C[i].field_D8)
					MemoryPoolManager::Unwrap((char*)node->field_C[i].field_D8);
			}
		}
		if (node->field_C)
			MemoryPoolManager::Unwrap((char*)node->field_C);

		MemoryPoolManager::Unwrap((char*)node);

		node = next;
	}

	if (this->Zonelines)
		MemoryPoolManager::Unwrap((char*)this->Zonelines);

	this->Zonelines = nullptr;
	this->ZonelineCount = 0;

	if (this->Submodels)
		MemoryPoolManager::Unwrap((char*)this->Submodels);

	this->Submodels = nullptr;
	this->SubmodelCount = 0;

	RidManager::Init();
}

void FFXI::CYy::RidManager::Init()
{
	this->Submodels = nullptr;
	this->SubmodelCount = 0;
	this->Zonelines = nullptr;
	this->ZonelineCount = 0;

	ListHead = nullptr;
	ListTail = nullptr;
	ListSize = 0;
}

RidListNode* FFXI::CYy::RidManager::GetHead()
{
	return RidManager::ListSize != 0 ? RidManager::ListHead : nullptr;
}

void FFXI::CYy::RidManager::InitUnderscoreRid(RidStruct* a1, char* a2)
{
	int objectcount = 0;
	//96 chunks of int,int,float,float
	int intpairs[192];
	float floatpairs[192];
	float* floatdata = (float*)a2;
	int* int32data = (int*)a2;
	unsigned short* uint16data = (unsigned short*)a2;
	D3DXVECTOR3* scale = (D3DXVECTOR3*)(floatdata + 6);
	D3DXVECTOR3* translation = (D3DXVECTOR3*)floatdata;
	if (scale->x > 7.0 && scale->z > 7.0) {
		FFXI::Math::WMatrix v321{};
		v321.Identity();
		FFXI::Math::WMatrix work{};
		work.Scale3(scale);
		v321.MatrixMultiply(&work);
		v321.RotateY(floatdata[4]);
		work.Identity();
		work.AddTranslation3(translation);
		v321.MatrixMultiply(&work);
		float v332 = v321._31 * -0.5 + v321._11 * -0.5 + v321._41;
		float v39 = v321._33 * -0.5 + v321._13 * -0.5 + v321._43;
		float v330 = v321._31 * 0.5 + v321._11 * 0.5 + v321._41;
		float v331 = v321._33 * 0.5 + v321._13 * 0.5 + v321._43;

		if (v332 > v330) {
			float temp = v332;
			v332 = v330;
			v330 = temp;
		}

		if (v39 > v331) {
			float temp = v39;
			v39 = v331;
			v331 = temp;
		}

		float start = v39;
		float end = v331 - 0.00001 + 4.0;
		while (start < end) {
			if (start > v331) {
				start = v331;
			}
			int v55 = (int)(0.25 * start);
			float start2 = v332;
			float end2 = v330 - 0.00001 + 4.0;
			while (start2 < end2) {
				if (start2 > v330) {
					start2 = v330;
				}
				int v60 = (int)(0.25 * start2);
				if (objectcount <= 0) {
				addobj:
					intpairs[2 * objectcount] = v60;
					intpairs[2 * objectcount + 1] = v55;
					floatpairs[2 * objectcount] = start2;
					floatpairs[2 * objectcount + 1] = start;
					objectcount += 1;
					if (objectcount >= 96) {
						//break nested loop with goto
						goto end;
					}
				}
				else {
					int loopcounter = 0;
					while (intpairs[2 * loopcounter] != v60 || intpairs[2 * loopcounter + 1] != v55) {
						loopcounter += 1;
						if (loopcounter >= objectcount) {
							goto addobj;
						}
					}
				}
				start2 += 4.0;
			}

			start += 4.0;
		}
	}
	
	end:
	if (objectcount == 0) {
		floatpairs[0] = translation->x;
		floatpairs[1] = translation->z;
		objectcount = 1;
	}

	unsigned int wrapsize = sizeof(RidUnderscoreStruct) * objectcount;
	RidUnderscoreStruct* structs = (RidUnderscoreStruct*)MemoryPoolManager::Wrap(wrapsize, MemoryPoolManager::MemoryPoolType::Ex);
	memset(structs, 0, wrapsize);

	a1->field_D4 = objectcount;
	a1->field_D8 = structs;

	structs[0].field_0 = 1;
	structs[0].field_4 = &structs[0].translation;
	structs[0].field_8 = structs[0].field_D8;
	structs[0].field_C = 0;
	structs[0].translation.Identity();

	FFXI::Math::WMatrix work{};
	work.Identity();
	work.AddTranslation3(translation);
	structs[0].translation.MatrixMultiply(&work);

	D3DXVECTOR3 reverse_translate = *translation * -1;
	work.Identity();
	work.AddTranslation3(&reverse_translate);
	structs[0].inverseTranslation.Identity();
	structs[0].inverseTranslation.MatrixMultiply(&work);

	structs[0].field_98 = { 1.0, 0.0, 0.0 };
	structs[0].field_A4 = { 0.0, 1.0, 0.0 };
	structs[0].field_B0 = { 0.0, 0.0, 1.0 };

	int v87 = 2 & 0xFFFFFFFB ^ (4 * ((a2[12] << 16 >> 31) & 1));
	int v88 = (v87 & 0xFFFFFFC7) ^ (8 * ((a2[12] & 0x70000) << 11 >> 27));
	int v90 = (v88 & 0xF3FFFFFF) ^ (((a2[12] << 11 >> 30) & 3) << 26);
	structs[0].field_BC = v90;

	structs[0].field_C0 = 0;
	structs[0].field_C4 = 0;
	structs[0].field_C8 = 0;
	structs[0].field_CC = translation->y - (scale->y * 0.5);
	structs[0].field_D0 = translation->y + (scale->y * 0.5);
	structs[0].field_D4 = 0;
	structs[0].field_DC = structs[0].field_148;
	structs[0].field_D8 = structs[0].field_E8;
	structs[0].field_E0 = structs[0].field_190;
	structs[0].field_E4 = 0x1000C;

	work.RotateY(floatdata[4]);
	FFXI::Math::WMatrix v334{};
	v334.Identity();
	v334.MatrixMultiply(&work);

	work.Identity();
	work.AddTranslation3(translation);
	FFXI::Math::WMatrix v321{};
	v321.Identity();
	v321.MatrixMultiply(&work);

	work.RotateY(floatdata[4]);
	v321.MatrixMultiply(&work);

	const int array_size = sizeof(structs[0].field_E8) / sizeof(structs[0].field_E8[0]);
	const D3DXVECTOR3 v999[array_size] = 
	{
		{ -0.5, -0.5,  0.5 },
		{  0.5, -0.5,  0.5 },
		{ -0.5, -0.5, -0.5 },
		{  0.5, -0.5, -0.5 },
		{ -0.5,  0.5,  0.5 },
		{  0.5,  0.5,  0.5 },
		{ -0.5,  0.5, -0.5 },
		{  0.5,  0.5, -0.5 }
	};

	for (int i = 0; i < array_size; ++i) {
		v321.Vec3TransformDrop4(structs[0].field_E8 + i, v999 + i);
	}

	const int array2_size = sizeof(structs[0].field_148) / sizeof(structs[0].field_148[0]);
	const D3DXVECTOR3 v998[array2_size] =
	{
		{  0.0, -1.0,  0.0 },
		{  0.0,  0.0, -1.0 },
		{ -1.0,  0.0,  0.0 },
		{  0.0,  0.0,  1.0 },
		{  1.0,  0.0,  0.0 },
		{  0.0,  1.0,  0.0 }
	};

	for (int i = 0; i < array2_size; ++i) {
		v334.Vec3TransformDrop4(structs[0].field_148 + i, v998 + i);
	}

	//set 50 uint16 flags
	// many of these seem to be set to the same values
	//0
	unsigned short v139 = (uint16data[24] << 8) >> 12 << 15;
	structs[0].field_190[0] = v139 ^ (v139 ^ 2) & 0x7FFF;
	//1
	v139 = (uint16data[24] << 8) >> 13 << 15;
	structs[0].field_190[1] = v139 ^ (v139 ^ 3) & 0x7FFF;
	//2
	v139 = (uint16data[24] << 8) >> 14 << 15;
	unsigned short v138 = 6 ^ (6 ^ ((uint16data[24] << 7) >> 15 << 14)) & 0x4000;
	structs[0].field_190[2] = v139 ^ (v139 ^ v138) & 0x7FFF;
	//3
	v139 = (uint16data[24] << 8) >> 15 << 15;
	structs[0].field_190[3] = v139 ^ (v139 ^ 1) & 0x7FFF;
	//4
	v139 = (uint16data[24] << 8) >> 12 << 15;
	structs[0].field_190[4] = v139 ^ (v139 ^ 3) & 0x7FFF;
	//5
	v139 = (uint16data[24] << 8) >> 13 << 15;
	structs[0].field_190[5] = v139 ^ (v139 ^ 7) & 0x7FFF;
	//6
	v139 = (uint16data[24] << 8) >> 14 << 15;
	v138 = 6 ^ (6 ^ ((uint16data[24] << 7) >> 15 << 14)) & 0x4000;
	structs[0].field_190[6] = v139 ^ (v139 ^ v138) & 0x7FFF;
	//7
	v139 = (uint16data[24] << 8) >> 15 << 15;
	structs[0].field_190[7] = v139 ^ (v139 ^ 1) & 0x7FFF;
	//8
	v139 = (uint16data[24] << 8) >> 12 << 15;
	structs[0].field_190[8] = v139 ^ (v139 ^ 0) & 0x7FFF;
	//9
	v139 = (uint16data[24] << 8) >> 13 << 15;
	structs[0].field_190[9] = v139 ^ (v139 ^ 2) & 0x7FFF;
	//10
	v139 = (uint16data[24] << 8) >> 14 << 15;
	v138 = 4 ^ (4 ^ ((uint16data[24] << 7) >> 15 << 14)) & 0x4000;
	structs[0].field_190[10] = v139 ^ (v139 ^ v138) & 0x7FFF;
	//11
	v139 = (uint16data[24] << 8) >> 15 << 15;
	structs[0].field_190[11] = v139 ^ (v139 ^ 2) & 0x7FFF;
	//12
	v139 = (uint16data[24] << 8) >> 12 << 15;
	structs[0].field_190[12] = v139 ^ (v139 ^ 2) & 0x7FFF;
	//13
	v139 = (uint16data[24] << 8) >> 13 << 15;
	structs[0].field_190[13] = v139 ^ (v139 ^ 6) & 0x7FFF;
	//14
	v139 = (uint16data[24] << 8) >> 14 << 15;
	v138 = 4 ^ (4 ^ ((uint16data[24] << 7) >> 15 << 14)) & 0x4000;
	structs[0].field_190[14] = v139 ^ (v139 ^ v138) & 0x7FFF;
	//15
	v139 = (uint16data[24] << 8) >> 15 << 15;
	structs[0].field_190[15] = v139 ^ (v139 ^ 2) & 0x7FFF;
	//16
	v139 = (uint16data[24] << 8) >> 12 << 15;
	structs[0].field_190[16] = v139 ^ (v139 ^ 1) & 0x7FFF;
	//17
	v139 = (uint16data[24] << 8) >> 13 << 15;
	structs[0].field_190[17] = v139 ^ (v139 ^ 0) & 0x7FFF;
	//18
	v139 = (uint16data[24] << 8) >> 14 << 15;
	v138 = 5 ^ (5 ^ ((uint16data[24] << 7) >> 15 << 14)) & 0x4000;
	structs[0].field_190[18] = v139 ^ (v139 ^ v138) & 0x7FFF;
	//19
	v139 = (uint16data[24] << 8) >> 15 << 15;
	structs[0].field_190[19] = v139 ^ (v139 ^ 3) & 0x7FFF;
	//20
	v139 = (uint16data[24] << 8) >> 12 << 15;
	structs[0].field_190[20] = v139 ^ (v139 ^ 0) & 0x7FFF;
	//21
	v139 = (uint16data[24] << 8) >> 13 << 15;
	structs[0].field_190[21] = v139 ^ (v139 ^ 4) & 0x7FFF;
	//22
	v139 = (uint16data[24] << 8) >> 14 << 15;
	v138 = 5 ^ (5 ^ ((uint16data[24] << 7) >> 15 << 14)) & 0x4000;
	structs[0].field_190[22] = v139 ^ (v139 ^ v138) & 0x7FFF;
	//23
	v139 = (uint16data[24] << 8) >> 15 << 15;
	structs[0].field_190[23] = v139 ^ (v139 ^ 3) & 0x7FFF;
	//24
	v139 = (uint16data[24] << 8) >> 12 << 15;
	structs[0].field_190[24] = v139 ^ (v139 ^ 3) & 0x7FFF;
	//25
	v139 = (uint16data[24] << 8) >> 13 << 15;
	structs[0].field_190[25] = v139 ^ (v139 ^ 1) & 0x7FFF;
	//26
	v139 = (uint16data[24] << 8) >> 14 << 15;
	v138 = 7 ^ (7 ^ ((uint16data[24] << 7) >> 15 << 14)) & 0x4000;
	structs[0].field_190[26] = v139 ^ (v139 ^ v138) & 0x7FFF;
	//27
	v139 = (uint16data[24] << 8) >> 15 << 15;
	structs[0].field_190[27] = v139 ^ (v139 ^ 4) & 0x7FFF;
	//28
	v139 = (uint16data[24] << 8) >> 12 << 15;
	structs[0].field_190[28] = v139 ^ (v139 ^ 1) & 0x7FFF;
	//29
	v139 = (uint16data[24] << 8) >> 13 << 15;
	structs[0].field_190[29] = v139 ^ (v139 ^ 5) & 0x7FFF;
	//30
	v139 = (uint16data[24] << 8) >> 14 << 15;
	v138 = 7 ^ (7 ^ ((uint16data[24] << 7) >> 15 << 14)) & 0x4000;
	structs[0].field_190[30] = v139 ^ (v139 ^ v138) & 0x7FFF;
	//31
	v139 = (uint16data[24] << 8) >> 15 << 15;
	structs[0].field_190[31] = v139 ^ (v139 ^ 4) & 0x7FFF;
	//32
	v139 = (uint16data[24] << 8) >> 12 << 15;
	structs[0].field_190[32] = v139 ^ (v139 ^ 6) & 0x7FFF;
	//33
	v139 = (uint16data[24] << 8) >> 13 << 15;
	structs[0].field_190[33] = v139 ^ (v139 ^ 7) & 0x7FFF;
	//34
	v139 = (uint16data[24] << 8) >> 14 << 15;
	v138 = 4 ^ (4 ^ ((uint16data[24] << 7) >> 15 << 14)) & 0x4000;
	structs[0].field_190[34] = v139 ^ (v139 ^ v138) & 0x7FFF;
	//35
	v139 = (uint16data[24] << 8) >> 15 << 15;
	structs[0].field_190[35] = v139 ^ (v139 ^ 5) & 0x7FFF;
	//36
	v139 = (uint16data[24] << 8) >> 12 << 15;
	structs[0].field_190[36] = v139 ^ (v139 ^ 7) & 0x7FFF;
	//37
	v139 = (uint16data[24] << 8) >> 13 << 15;
	structs[0].field_190[37] = v139 ^ (v139 ^ 5) & 0x7FFF;
	//38
	v139 = (uint16data[24] << 8) >> 14 << 15;
	v138 = 4 ^ (4 ^ ((uint16data[24] << 7) >> 15 << 14)) & 0x4000;
	structs[0].field_190[38] = v139 ^ (v139 ^ v138) & 0x7FFF;
	//39
	v139 = (uint16data[24] << 8) >> 15 << 15;
	structs[0].field_190[39] = v139 ^ (v139 ^ 5) & 0x7FFF;
	//40
	v139 = (uint16data[24] << 8) >> 12 << 15;
	structs[0].field_190[40] = v139 ^ (v139 ^ 0) & 0x7FFF;
	//41
	v139 = (uint16data[24] << 8) >> 13 << 15;
	structs[0].field_190[41] = v139 ^ (v139 ^ 1) & 0x7FFF;
	//42
	v139 = (uint16data[24] << 8) >> 14 << 15;
	v138 = 2 ^ (2 ^ ((uint16data[24] << 7) >> 15 << 14)) & 0x4000;
	structs[0].field_190[42] = v139 ^ (v139 ^ v138) & 0x7FFF;
	//43
	v139 = (uint16data[24] << 8) >> 15 << 15;
	structs[0].field_190[43] = v139 ^ (v139 ^ 0) & 0x7FFF;
	//44
	v139 = (uint16data[24] << 8) >> 12 << 15;
	structs[0].field_190[44] = v139 ^ (v139 ^ 1) & 0x7FFF;
	//45
	v139 = (uint16data[24] << 8) >> 13 << 15;
	structs[0].field_190[45] = v139 ^ (v139 ^ 3) & 0x7FFF;
	//46
	v139 = (uint16data[24] << 8) >> 14 << 15;
	v138 = 2 ^ (2 ^ ((uint16data[24] << 7) >> 15 << 14)) & 0x4000;
	structs[0].field_190[46] = v139 ^ (v139 ^ v138) & 0x7FFF;
	//47
	v139 = int32data[12] << 24 >> 31 << 15;
	structs[0].field_190[47] = v139 ^ (v139 ^ 0) & 0x7FFF;

	structs[0].field_10 = floatpairs[0];
	structs[0].field_14 = floatpairs[1];

	for (int i = 1; i < objectcount; ++i) {
		memcpy(structs + i, structs, sizeof(structs[0]));
		structs[i].field_10 = floatpairs[2 * i];
		structs[i].field_14 = floatpairs[2 * i + 1];
	}
}

void FFXI::CYy::RidManager::InitZonelines() {
	RidListNode* v1 = RidManager::ListHead;
	int counter = 0;
	while (v1) {
		if (v1->field_8 > 0) {
			for (int i = 0; i < v1->field_8; ++i) {
				char* data = v1->data + 100 + 64 * i;
				if (*data == 'Z' || *data == 'z') {
					if (*(int*)(data + 4)) {
						counter += 1;
					}
				}
			}
		}
		v1 = v1->Next;
	}

	this->Zonelines = nullptr;
	this->ZonelineCount = counter;

	if (counter > 0) {
		this->Zonelines = (Zoneline*)MemoryPoolManager::Wrap(sizeof(Zoneline) * counter, MemoryPoolManager::MemoryPoolType::Ex);
		RidListNode* v7 = RidManager::ListHead;
		int zonelineCounter = 0;
		while (v7) {
			if (v7->field_8 > 0) {
				for (int i = 0; i < v7->field_8; ++i) {
					char* data = v7->data + 64 + 64 * i;
					char prefix = *(data + 36);
					if (prefix == 'Z' || prefix == 'z') {
						if (*(int*)(data + 40)) {
							memcpy_s(this->Zonelines + zonelineCounter, sizeof(Zoneline), data, sizeof(Zoneline));
							zonelineCounter += 1;
						}
					}
				}
			}
			v7 = v7->Next;
		}
	}
}

void FFXI::CYy::RidManager::InitSubModels()
{
	RidListNode* v1 = RidManager::ListHead;
	int counter = 0;
	while (v1) {
		if (v1->field_8 > 0) {
			for (int i = 0; i < v1->field_8; ++i) {
				char* data = v1->data + 100 + 64 * i;
				if (*data == 'M' || *data == 'm') {
					if (*(int*)(data + 4)) {
						counter += 1;
					}
				}
			}
		}
		v1 = v1->Next;
	}

	this->Submodels = nullptr;
	this->SubmodelCount = counter;
	
	if (counter > 0) {
		this->Submodels = (Submodel*)MemoryPoolManager::Wrap(sizeof(Submodel) * counter, MemoryPoolManager::MemoryPoolType::Ex);
		RidListNode* v7 = RidManager::ListHead;
		int submodelCount = 0;
		while (v7) {
			if (v7->field_8 > 0) {
				for (int i = 0; i < v7->field_8; ++i) {
					char* data = v7->data + 64 + 64 * i;
					char prefix = *(data + 36);
					if (prefix == 'M' || prefix == 'm') {
						if (*(int*)(data + 40)) {
							memcpy_s(this->Submodels + submodelCount, sizeof(Submodel), data, sizeof(Submodel));
							submodelCount += 1;
						}
					}
				}
			}
			v7 = v7->Next;
		}
	}
}

RidStruct* FFXI::CYy::RidManager::LiftRectHitCheck(D3DXVECTOR3* a1, D3DXVECTOR3* a2)
{
	if (RidManager::ListSize == 0) {
		return nullptr;
	}

	D3DXVECTOR3 v34 = *a1;
	D3DXVECTOR3 v42 = *a2 - *a1;

	RidListNode* node = RidManager::ListHead;
	while (node != nullptr) {
		for (int i = 0; i < node->field_10; i++) {
			RidStruct* rs = node->field_C + i;
			if ((rs->fourCC & 0xFF) == '@') {
				D3DXVECTOR3 checkone{}, checktwo{};
				rs->field_0.Vec3TransformDrop4(&checkone, &v34);
				rs->field_0.Vec3TransformDrop4(&checktwo, a2);
				if (checktwo.x < -0.5 || checktwo.x > 0.5
					|| checktwo.y < -0.5 || checktwo.y > 0.5
					|| checktwo.z < -0.5 || checktwo.z > 0.5) {
					if (checkone.x < -0.5 || checkone.x > 0.5
						|| checkone.y < -0.5 || checkone.y > 0.5
						|| checkone.z < -0.5 || checkone.z > 0.5) {
						const int array_size = sizeof(FFXI::Math::KO_RectData::DataTbl) / sizeof(FFXI::Math::KO_RectData::DataTbl[0]);
						for (int j = 0; j < array_size; ++j) {
							if (FFXI::Math::KO_RectData::HitCheck(&v34, &v42, FFXI::Math::KO_RectData::DataTbl + j) == true) {
								return rs;
							}
						}
					}
				}
			}
		}
		node = node->Next;
	}

	return nullptr;
}
