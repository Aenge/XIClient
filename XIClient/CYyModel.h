#pragma once
#define WIN32_LEAN_AND_MEAN
#include "MemoryManagedObject.h"
#include "ShadowRenderer.h"
#include "TextureBlender.h"
#include "ModelSubStruct5.h"
#include "tex_struct.h"
#include "model_static_wrapper.h"
#include "WMatrix.h"
#include "d3d8to9/d3d8.hpp"
namespace FFXI {
	namespace CYy {
		class CYyModelBase;
		class CXiSkeletonActor;
		class CXiActor;
		class CYyModel : public MemoryManagedObject {
		public:
			static const BaseGameObject::ClassInfo CYyModelClass;
			static short g_some_short;
			static FFXI::Math::WMatrix FIRST_tex_transform;
			static FFXI::Math::WMatrix SECOND_tex_transform;
			static FFXI::Math::WMatrix view_transform;
			static FFXI::Math::WMatrix temp_proj_mtx;
			static FFXI::Math::WMatrix temp_view_mtx;
			static FFXI::Math::WMatrix temp_wrld_mtx;
			static D3DMATERIAL8 material;
			static D3DCOLOR ambient_color;
			static int maybe_polys_drawn;
			static float some_x_val;
			static float some_y_val;
			virtual const BaseGameObject::ClassInfo* GetRuntimeClass() override final;
			virtual ~CYyModel();
			CYyModel();
			CYyModelBase* CreateBase();
			CYyModelBase** GetBase();
			void Draw(CXiActor*, FFXI::Math::WMatrix*, D3DXVECTOR4*, D3DXVECTOR4*, D3DXVECTOR4*, D3DXVECTOR4*, unsigned int);
			void DrawBaseList(CXiActor*, float);
			bool DoingSomething(CXiActor*, FFXI::Math::WMatrix*, float, D3DXVECTOR4*);
			int GetBoneIndexFromModelBaseIndex(unsigned int, int);
			bool GetBonePositionFromModelBaseIndex(int, unsigned int, D3DXVECTOR4*);
			int GetOs2ResId(int);
			void IsHideOs2(int, int, int);
			void MaybeUpdateAnims(CXiSkeletonActor*);
			void UpdateBaseDTs();
			D3DXVECTOR4 field_4;
			D3DXVECTOR4 field_14;
			D3DXVECTOR3 field_24;
			int field_30;
			D3DXVECTOR3 field_34;
			int field_40;
			CYyModelBase* Base;
			ShadowRenderer shadowRenderer;
			TextureBlender textureBlender;
			model_static_wrapper field_8C;
			tex_struct field_90;
			ModelSubStruct5 SubStruct5;
			char field_A8;
			char field_A9;
			char field_AA;
			char field_AB;
			char field_AC;
			char field_AD;
			char field_AE;
			char field_AF;
			char field_B0;
			char field_B1;
			char field_B2;
			char field_B3;
			int field_B4;
			char field_B8;
			char field_B9;
			char field_BA;
			char field_BB;
		};
	}
}