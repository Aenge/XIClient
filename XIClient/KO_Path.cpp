#include "KO_Path.h"
#include "CYyDb.h"
#include "BaseProcessor.h"

/**
 * @brief Calculate direction and distance from a position to a path segment
 *
 * @param segmentStart Start point of the path segment (x,y,z,radius)
 * @param segmentEnd End point of the path segment (x,y,z,radius)
 * @param outDirection Output vector for direction
 * @param currentPosition Current position
 * @return Distance to the path segment
 */
float RailData_GetDirection(FFXI::Math::KO_Path::SegmentPoint* segmentStart, FFXI::Math::KO_Path::SegmentPoint* segmentEnd, D3DXVECTOR3* outDirection, D3DXVECTOR3* currentPosition)
{
	D3DXVECTOR3 segmentVector{};  // Vector from start to end of segment
	D3DXVECTOR3 normalizedSegment{};
	D3DXVECTOR3 segmentMagnitude{};
	D3DXVECTOR3 positionToStart{}; // Vector from position to segment start
	
	// Calculate segment vector (from start to end)
	segmentVector = segmentStart->Position - segmentEnd->Position;

	// Calculate vector from start point to current position
	positionToStart = segmentStart->Position - *currentPosition;

	segmentMagnitude = segmentVector;
	float segmentLength = FFXI::CYyDb::g_pCYyDb->pMoProcessor->Vec3Magnitude(&segmentMagnitude);

	normalizedSegment = segmentVector;
	FFXI::CYyDb::g_pCYyDb->pMoProcessor->VirtProcessor35(&normalizedSegment);

	// Calculate projection of position onto segment (dot product)
	float projection = -(normalizedSegment.z * positionToStart.z + normalizedSegment.y * positionToStart.y + normalizedSegment.x * positionToStart.x);

	// Clamp projection to segment boundaries
	if (projection > 0.0) {
		projection = 0.0;
	}
	if (projection < -segmentLength) {
		projection = -segmentLength;
	}

	// Calculate the closest point on the segment to the current position
	normalizedSegment *= projection;
	normalizedSegment += segmentStart->Position;

	// Calculate vector from current position to closest point
	*outDirection = normalizedSegment - *currentPosition;

	// Get the distance to the path
	float distance = FFXI::CYyDb::g_pCYyDb->pMoProcessor->Vec3Magnitude(outDirection);

	if (segmentLength > 0.0) {
		// Normalize the direction vector
		outDirection->x *= 1.0f / distance;
		outDirection->y *= 1.0f / distance;
		outDirection->z *= 1.0f / distance;

		// Interpolate radius between start and end points based on projection
		float interpolationFactor = -(projection / segmentLength);
		float interpolatedRadius = (1.0 - interpolationFactor) * segmentStart->Radius + interpolationFactor * segmentEnd->Radius;

		// Adjust distance by radius
		distance -= interpolatedRadius;

		// Clamp distance to zero (inside the path radius)
		if (distance < 0.0) {
			distance = 0.0;
		}

		// Scale direction vector by adjusted distance
		outDirection->x *= distance;
		outDirection->y *= distance;
		outDirection->z *= distance;
	}

	return distance;
}

void FFXI::Math::KO_Path::GetDirection(D3DXVECTOR3* outDirection, D3DXVECTOR3* currentPosition)
{
	// Check if path has valid segments
	unsigned int segmentCount = this->segmentsArray[0];
	if (segmentCount <= 0) {
		return;
	}

	// Start with a large value to find minimum distance
	float minDistance = 1000000000.0f;
	D3DXVECTOR3 tempDirection{};

	for (int i = 0; i < segmentCount; ++i) 
	{
		SegmentDefinition segmentDefinition = this->GetSegmentDefinition(i);

		// Calculate direction to this segment
		// Each segment is defined by two point indices (masked with 0x00FFFFFF)
		float currentDistance = RailData_GetDirection(
			this->GetSegmentPoint(segmentDefinition.StartIndex),
			this->GetSegmentPoint(segmentDefinition.EndIndex),
			&tempDirection,
			currentPosition);

		// Keep track of the closest segment
		if (minDistance > currentDistance) {
			*outDirection = tempDirection;
			minDistance = currentDistance;
		}
	}
}

unsigned int FFXI::Math::KO_Path::GetSegmentCount()
{
	return this->segmentsArray[0];
}

FFXI::Math::KO_Path::SegmentDefinition FFXI::Math::KO_Path::GetSegmentDefinition(unsigned int segmentIndex)
{
	SegmentDefinition* segmentDefinitionTable = (SegmentDefinition*)(this->segmentsArray + 4);

	SegmentDefinition segmentDefinition{};
	segmentDefinition.StartIndex = segmentDefinitionTable[segmentIndex].StartIndex & 0x00FFFFFF;
	segmentDefinition.EndIndex = segmentDefinitionTable[segmentIndex].EndIndex & 0x00FFFFFF;

	return segmentDefinition;
}

FFXI::Math::KO_Path::SegmentPoint* FFXI::Math::KO_Path::GetSegmentPoint(unsigned int pointIndex)
{
	SegmentPoint* segmentPointTable = (SegmentPoint*)(this->pointsArray + 4);
	return segmentPointTable + pointIndex;
}
