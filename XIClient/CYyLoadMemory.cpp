#include "CYyLoadMemory.h"

using namespace FFXI::CYy;

const BaseGameObject::ClassInfo CYyLoadMemory::CYyLoadMemoryClass{
	"CYyLoadMemory", sizeof(CYyLoadMemory), &BaseGameObject::BaseGameObjectClass
};

const BaseGameObject::ClassInfo* CYyLoadMemory::GetRuntimeClass() {
	return &CYyLoadMemoryClass;
}

