#include "enQue.h"
#include "enQueBuff.h"
#include "GP_CLI_COMMAND.h"
#include <cstring>

bool FFXI::Network::enQue::enQueInit(enQue* a1, enQueBuff* a2, int a3, int a4)
{
	memset(a1, 0, sizeof(enQue));
	memset(a2, 0, sizeof(enQueBuff) * a3);

	a1->field_4 = a3;
	a1->field_10 = a3 - 14;
	a1->field_0 = a2;
	a1->field_14 = a4;

	return a3 > a4;
}

bool FFXI::Network::enQue::enQueSpecialSet(enQue* que, FFXI::Constants::Enums::GP_CLI_COMMAND command)
{
	const int array_size = sizeof(que->field_18) / sizeof(que->field_18[0]);
	for (int i = 0; i < array_size; ++i) {
		if (que->field_18[i] == FFXI::Constants::Enums::GP_CLI_COMMAND::NONE) {
			que->field_18[i] = command;
			return true;
		}
	}

	return false;
}

FFXI::Network::enQueBuff* FFXI::Network::enQue::enQueSearch2(enQue* a1, FFXI::Constants::Enums::GP_CLI_COMMAND a2, bool a3, int a4)
{
	if (a1 == nullptr) {
		return nullptr;
	}

	if (a1->field_4 == 0) {
		return nullptr;
	}

	FFXI::Network::enQueBuff* buff = nullptr;
	if (a3 == true) {
		int v6 = a1->field_C % a1->field_4;
		while (v6 != a1->field_8) {
			FFXI::Network::enQueBuff* iter = a1->field_0 + v6;
			if ((iter->field_0 & 0x1FF) == 0 && buff == nullptr) {
				buff = iter;
			}
			if ((iter->field_0 & 0x1FF) == (int)a2 && iter->field_104 == a3) {
				buff = iter;
				a1->field_2C = 1;
				break;
			}
			v6 = (v6 + 1) % a1->field_4;
		}
	}

	if (buff == nullptr) {
		int v10 = a1->field_C - a1->field_8;
		if (v10 <= 0) {
			v10 += a1->field_4;
		}

		if (a1->field_14 > v10
			&& a1->field_18[0] != a2
			&& a1->field_18[1] != a2
			&& a1->field_18[2] != a2
			&& a1->field_18[3] != a2
			&& a1->field_18[4] != a2
			&& a1->field_18[5] != a2
			&& a1->field_18[6] != a2
			&& a1->field_18[7] != a2
			&& a1->field_18[8] != a2
			&& a1->field_18[9] != a2)
		{
			return nullptr;
		}

		int v11 = (a1->field_8 + 1) % a1->field_4;
		if (v11 == a1->field_C) {
			return nullptr;
		}

		buff = a1->field_0 + a1->field_8;
		a1->field_8 = v11;
		
		if (buff == nullptr) {
			return nullptr;
		}
	}
	
	buff->field_10C = a1;
	buff->field_104 = a4;
	return buff;
}

void FFXI::Network::enQue::enQueSet(enQueBuff* a1, int size, int addSize)
{
	int v4 = a1->field_0 & 0x1FF ^ (((size + addSize) / 4 + ((size + addSize) % 4 != 0)) << 9);
	a1->field_0 = v4;
	a1->field_2 = 0;
	if ((a1->field_0 & 0xFE00) != 0 && 4 * ((unsigned short)v4 >> 9) <= 0x104u) {
		if (a1->field_10C->field_2C == 0) {
			a1->field_10C->field_10 -= 1;
		}
		a1->field_10C->field_2C = 0;
	}
	else {
		a1->field_0 = v4 & 0x1FF;
	}
}
