#pragma once
#define WIN32_LEAN_AND_MEAN
#include "MemoryManagedObject.h"
#include "CAcc.h"
#include "d3d8to9/d3d8.hpp"
namespace FFXI {
	namespace CYy {
		class CMoOcclusionMng : public MemoryManagedObject {
		public:
			static const BaseGameObject::ClassInfo CMoOcclusionMngClass;
			virtual const BaseGameObject::ClassInfo* GetRuntimeClass() override final;
			CMoOcclusionMng();
			~CMoOcclusionMng();
			void InitField8();
			int field_4;
			CAcc field_8;
			IDirect3DSurface8* field_18;
		};
	}
}