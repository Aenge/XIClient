#include "MemoryBlockDescriptor.h"

using namespace FFXI::CYy;

int MemoryBlockDescriptor::GetSize() {
	MemoryBlockDescriptor* NextEntry = this->NextOccupied;
    if (NextEntry != nullptr)
        return (int)NextEntry - (int)this - sizeof(MemoryBlockDescriptor);
	else
		return 0;
}

int FFXI::CYy::MemoryBlockDescriptor::GetFreeSize()
{
    int free = 0;
    MemoryBlockDescriptor* tail = this;
    while (tail->PreviousOccupied != nullptr) {
        tail = tail->PreviousFree;
        if (!tail->occupied)
            free += tail->GetSize();
    }
    return free;
}

MemoryBlockDescriptor* FFXI::CYy::MemoryBlockDescriptor::Delete()
{
    MemoryBlockDescriptor* result;
    MemoryBlockDescriptor* PreviousEntry;
    MemoryBlockDescriptor* NextEntry;
    MemoryBlockDescriptor* v4;
    MemoryBlockDescriptor* v5;
    MemoryBlockDescriptor* v6;
    MemoryBlockDescriptor* v7;

    result = this;
    if (!this->occupied)
        return result;
    PreviousEntry = this->PreviousOccupied;
    this->occupied = 0;
    if (PreviousEntry && !PreviousEntry->occupied)
    {
        PreviousEntry->NextOccupied = this->NextOccupied;
        this->NextOccupied->PreviousOccupied = PreviousEntry;
        result = PreviousEntry;
    }
    NextEntry = result->NextOccupied;
    if (!NextEntry->occupied)
    {
        result->NextOccupied = NextEntry->NextOccupied;
        NextEntry->NextOccupied->PreviousOccupied = result;
    }
    v4 = result->PreviousOccupied;
    v5 = result;
    if (v4)
    {
        while (v4->occupied)
        {
            v5 = v4;
            v4 = v4->PreviousOccupied;
            if (!v4)
                goto LABEL_12;
        }
        v5 = v5->PreviousOccupied;
    }
LABEL_12:
    if (v5->PreviousOccupied || result->PreviousOccupied)
    {
        v5->NextFree = result;
        result->PreviousFree = v5;
    }
    v6 = result->NextOccupied;
    v7 = result;
    if (v6)
    {
        while (v6->occupied)
        {
            v7 = v6;
            v6 = v6->NextOccupied;
            if (!v6)
                goto LABEL_20;
        }
        v7 = v7->NextOccupied;
    }
LABEL_20:
    if (!v7->NextOccupied && !result->NextOccupied)
        return result;
    v7->PreviousFree = result;
    result->NextFree = v7;
    return result;
}

