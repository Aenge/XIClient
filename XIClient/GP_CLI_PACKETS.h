#pragma once

namespace FFXI {
	namespace Network {
		namespace UDP {
			namespace GP_CLI_PACKETS {
				struct GP_CLI_BASE {
					unsigned short field_0;
					unsigned short field_2;
				};

				struct GP_CLI_LOGIN : public GP_CLI_BASE {
					char checksum;
					char field_5;
					short client_connection_state;
					int recv_status;
					int field_C;
					int field_10;
					int field_14;
					int field_18;
					int field_1C;
					char field_20;
					char field_21;
					char field_22[15];
					char field_31[15];
					char login_ticket[16];
					int field_50;
					char platform_code[4];
					short field_58;
					short field_5A;
				};

				struct GP_CLI_LOGINOK : public GP_CLI_BASE {
					char field_4;
					char field_5;
				};

				struct GP_CLI_LOCK_CMD : public GP_CLI_BASE {
					enum class LockCMDType : unsigned int {
						DISABLE = 0,
						ENABLE = 1
					};

					int field_4;
					LockCMDType Type;
					int field_C;
					int field_10;
					int field_14;
					int field_18;
					int field_1C;
					int field_20;
					int field_24;
					int field_28;
					int field_2C;
					int field_30;
					int field_34;
					int field_38;
					int field_3C;
					int field_40;
					int field_44;
					int field_48;
					int field_4C;
					int field_50;
					int field_54;
					int field_58;
					int field_5C;
					int field_60;
					int field_64;
					int field_68;
					int field_6C;
					int field_70;
					int field_74;
					int field_78;
					int field_7C;
					int field_80;
					int field_84;
				};

				struct GP_CLI_REQ_STATUS : public GP_CLI_BASE {
					char field_4;
					char field_5;
				};
			}
		}
	}
}
