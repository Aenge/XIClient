#include "CIwSelectMenu.h"
#include "TkManager.h"
#include "CTkMouse.h"
#include "CTkMenuMng.h"
#include "CTkMenuCtrlData.h"
#include "InputMng.h"
#include "SoundMng.h"
#include "Strings.h"
#include "_49SubList.h"
#include "CTkDrawCtrlFrame.h"
#include "KaWindow.h"
#include "Globals.h"
#include "GlobalStruct.h"
#include "CYyDb.h"
#include "CYyCamMng2.h"
#include "TextRenderer.h"
#include "StringTables.h"
#include "PrimMng.h"
#include "CIwChfWin.h"
#include "CXiDancerActor.h"
#include <string>

using namespace FFXI::CTk;

bool CIwSelectMenu::Flag{ false };
int CIwSelectMenu::SelectedCharacter{ 0 };
int CIwSelectMenu::g_userfile{ 0 };

const short ContentIDTextSpacings[] = {
	0x241, 0x241, 0x1F2,
	0x190, 0x190, 0x190,
	0x241, 0x241, 0x18A,
	0x23F, 0x23F, 0x18A
};

const short ContentIDGraphicTable[] = {
	0x90, 0x90, 0, 0, 0xA5, 0xA6, 0xA5, 0xA6
};

const short ContentIDXTable[] = { 0, 0, 0x183, 0x183 };
const short ContentIDYTable[] = { 0, 0, 0x23, 0x23 };

void DrawCharInfo(_49SubList*** a1, IwCharInfo* charinfo) {
	int xPos = TkManager::g_CTkMenuMng.Get84(true) * 
		(TkManager::g_CTkMenuMng.UIXRes - TkManager::g_CTkMenuMng.field_7C) 
		* 0.5;

	int yPos = (1.0 - TkManager::g_CTkMenuMng.Get88(true)) *
		(TkManager::g_CTkMenuMng.UIYRes - TkManager::g_CTkMenuMng.field_7E)
		* 0.82589287 - 144;

	//Draw character name
	FFXI::Text::TextRenderer::DrawScriptString(a1, charinfo->CharName, xPos + 16, yPos);

	//Draw Server name
	yPos += 36;
	const char* string = FFXI::Text::XiStrGet(FFXI::Constants::Enums::StrTables::XIStr_CharSelectText, 0);
	FFXI::Text::TextRenderer::DrawScriptString(a1, string, xPos + 16, yPos);
	FFXI::Text::TextRenderer::DrawScriptString(a1, charinfo->WorldName, xPos + 60, yPos);
	
	//Draw race / gender
	yPos += 18;
	string = FFXI::Text::XiStrGet(FFXI::Constants::Enums::StrTables::XIStr_CharSelectText, 1);
	FFXI::Text::TextRenderer::DrawScriptString(a1, string, xPos + 16, yPos);
	string = FFXI::Text::XiStrGet(FFXI::Constants::Enums::StrTables::XIStr_CharSelectRaceGender, charinfo->RaceGender - 1);
	FFXI::Text::TextRenderer::DrawScriptString(a1, string, xPos + 60, yPos);

	//Draw main job
	yPos += 18;
	string = FFXI::Text::XiStrGet(FFXI::Constants::Enums::StrTables::XIStr_CharSelectText, 2);
	FFXI::Text::TextRenderer::DrawScriptString(a1, string, xPos + 16, yPos);
	string = FFXI::Text::XiStrGet(FFXI::Constants::Enums::StrTables::XIStr_JobFullNames, charinfo->MainJob);
	FFXI::Text::TextRenderer::DrawScriptString(a1, string, xPos + 60, yPos);

	//Draw main job level
	yPos += 18;
	string = FFXI::Text::XiStrGet(FFXI::Constants::Enums::StrTables::XIStr_CharSelectText, 3);
	FFXI::Text::TextRenderer::DrawScriptString(a1, string, xPos + 16, yPos);
	char buf[16];
	sprintf_s(buf, "%d", charinfo->MainJobLevel);
	FFXI::Text::TextRenderer::DrawScriptString(a1, buf, xPos + 60, yPos);

	//Draw area name
	yPos += 18;
	string = FFXI::Text::XiStrGet(FFXI::Constants::Enums::StrTables::XIStr_CharSelectText, 4);
	FFXI::Text::TextRenderer::DrawScriptString(a1, string, xPos + 16, yPos);
	FFXI::Text::TextRenderer::DrawScriptString(a1, charinfo->AreaName, xPos + 60, yPos);
}

void DrawContentIDInfo(_49SubList*** a1, int ContentIDCount, int CharacterCount) {

	double v1 = 1.0 - TkManager::g_CTkMenuMng.Get84(true);
	short diff = TkManager::g_CTkMenuMng.UIXRes - TkManager::g_CTkMenuMng.field_7C;
	double xPos = v1 * (double)diff - 640.0;
	char Message[256];
	int RegionCode = FFXI::GlobalStruct::g_GlobalStruct.GetRegionCode();
	if (RegionCode == FFXI::Constants::Enums::LanguageCode::English) {
		sprintf_s(Message, FFXI::Constants::Strings::ContentIDCount, ContentIDCount);
		FFXI::Text::TextRenderer::YkDrawStringScale(xPos + ContentIDTextSpacings[3], 35, Message, 0x80808080, 1.0, 1.0, false);
		sprintf_s(Message, FFXI::Constants::Strings::CharacterCount, CharacterCount);
		FFXI::Text::TextRenderer::YkDrawStringScale(xPos + ContentIDTextSpacings[4], 51, Message, 0x80808080, 1.0, 1.0, false);
		int canmake = ContentIDCount - CharacterCount;
		if (canmake == 1)
			sprintf_s(Message, FFXI::Constants::Strings::CanMakeChar, 1);
		else
			sprintf_s(Message, FFXI::Constants::Strings::CanMakeChars, canmake);
		FFXI::Text::TextRenderer::YkDrawStringScale(xPos + ContentIDTextSpacings[5], 67, Message, 0x80808080, 1.0, 1.0, false);
	}
	else {
		sprintf_s(Message, "%2d", ContentIDCount);
		FFXI::Text::TextRenderer::YkDrawStringScale(xPos + ContentIDTextSpacings[3 * RegionCode], 35, Message, 0x80808080, 1.0, 1.0, false);
		sprintf_s(Message, "%2d", CharacterCount);
		FFXI::Text::TextRenderer::YkDrawStringScale(xPos + ContentIDTextSpacings[3 * RegionCode + 1], 51, Message, 0x80808080, 1.0, 1.0, false);
		
		int canmake = ContentIDCount - CharacterCount;
		sprintf_s(Message, "%2d", canmake);
		FFXI::Text::TextRenderer::YkDrawStringScale(xPos + ContentIDTextSpacings[3 * RegionCode + 2], 67, Message, 0x80808080, 1.0, 1.0, false);
	
		int v9;
		if (RegionCode == FFXI::Constants::Enums::LanguageCode::French)
			v9 = canmake != 1;
		else
			v9 = RegionCode == FFXI::Constants::Enums::LanguageCode::German && canmake > 1;

		int index = ContentIDGraphicTable[2 * RegionCode + v9];
		(*a1)[index]->ExtraDraw(xPos + ContentIDXTable[RegionCode], ContentIDYTable[RegionCode], 1.0, 1.0, 0x80808080, 0, 0, 0);
	}
}

void DrawCharName(short availableWidth, short x, short y, IwCharInfo* charinfo) {

	D3DCOLOR name_color = 0x80808080;
	if (charinfo->ContentIDInvalid != 0) {
		name_color = 0x80505050;
	}
	else if (charinfo->RenameFlag != 0) {
		name_color = 0x807F7F00;
	}

	FFXI::Text::TextRenderer::YkDrawString(availableWidth, x, y, charinfo->CharName, name_color);
}

FFXI::CTk::CIwSelectMenu::CIwSelectMenu()
{
	memset(this->charInfo, 0, sizeof(this->charInfo));
	this->field_14 = 0;
	this->field_18 = 0;
	this->field_1C = 0;
	this->ContentIDCount = 0;
	this->CharacterCount = 0;
	this->field_2C = 0;
	this->field_32 = 0;
	this->field_30 = -1;
	this->field_31 = -1;
}

void FFXI::CTk::CIwSelectMenu::OnInitialUpdatePrimitive()
{
	this->field_28 = 0;
	if (CIwSelectMenu::Flag == false) {
		CIwSelectMenu::SelectedCharacter = 0;
	}
	this->field_14 = CIwSelectMenu::SelectedCharacter;
	this->field_18 = CIwSelectMenu::SelectedCharacter;
	this->MenuCtrlData->field_5D = 0;
	this->MenuCtrlData->field_85 = 0;
	this->MenuCtrlData->field_86 = 0;
	this->MenuCtrlData->field_84 = 0;
	this->MenuCtrlData->field_D0 = 1;
	float v5 = 1.0 - TkManager::g_CTkMenuMng.Get84(true) * 0.5;
	float v6 = v5 * (double)TkManager::g_CTkMenuMng.UIYRes;

	float v3 = 1.0 - TkManager::g_CTkMenuMng.Get88(true) * 0.5;
	float v4 = v3 * (double)TkManager::g_CTkMenuMng.UIXRes;

	this->MenuCtrlData->ResizeWindow(
		v4 - 640.0,
		v6 - 480,
		640,
		480,
		1,
		0,
		0);
}

void FFXI::CTk::CIwSelectMenu::OnDrawPrimitive()
{
	_49SubList*** v2 = TkManager::g_CTkMenuDataList.FindMenuShapeFile(FFXI::Constants::Strings::MenuLobbyWin);
	
	//Draw FFXI logo
	_49SubList* logo = (*v2)[143];
	tagPOINT pos{};
	TkManager::g_CTkMenuMng.GetSomePosition(&pos, 0, true);
	logo->ExtraDraw(pos.x, pos.y, 1.0, 1.0, 0x80808080, 0, 0, 1);

	//Draw background vertical lines
	CTkDrawCtrlFrame* frame = this->MenuCtrlData->GetDrawCtrlFrame();
	if (frame) {
		KaWindow* window = frame->Window;
		if (window) {
			window->field_0 = TkManager::g_CTkMenuMng.UIXRes;
			window->field_4 = TkManager::g_CTkMenuMng.UIYRes + 32;
			window->Draw(0, -16, 1.0, 1.0, 0x20808080, 0, 0);
		}
	}

	this->field_28 += 1;
	if (this->field_28 >= 150) {
		CYy::CXiDancerActor* something = this->charInfo[SelectedCharacter].field_74;
		if (something != nullptr) {
			this->StartCamMove(true);
		}
		this->field_28 = 0;
	}

	this->CamMove();

	//Draw character names
	for (int i = 0; i < sizeof(this->charInfo) / sizeof(this->charInfo[0]); ++i) {
		IwCharInfo* charinfo = this->charInfo + i;
		if (charinfo->Exists) {
			TKRECT v2{};
			this->MenuCtrlData->SetButtonStatus(0, i + 1);
			this->MenuCtrlData->GetButtonLocate(&v2, i + 1, true, true);
			DrawCharName(v2.Right - v2.Left, v2.Left + 4, v2.Top + 2, charinfo);
		}
		else {
			this->MenuCtrlData->SetButtonStatus(5, i + 1);
		}
	}

	//Draw char info
	int v11 = this->MenuCtrlData->field_4C - 1;
	if (v11 >= 0 && v11 <= 16 && this->charInfo[v11].Exists)
		DrawCharInfo(v2, this->charInfo + v11);

	DrawContentIDInfo(v2, this->ContentIDCount, this->CharacterCount);
}

void FFXI::CTk::CIwSelectMenu::OnUpdatePrimitive()
{
	this->MenuCtrlData->field_4C = SelectedCharacter + 1;
	this->MenuCtrlData->RepositionCursol(this->MenuCtrlData->field_4C);
}

void FFXI::CTk::CIwSelectMenu::OnKeyDown(short a2, short a3)
{
	if (this->field_1C == 1) return;

	this->field_28 = 0;
	this->field_2C = 0;

	switch (a2) {
	case 1:
	case 2:
	case 3:
	case 4:
	case 9:
		SelectedCharacter = a3 - 1;
		if (this->field_18 != SelectedCharacter) {
			SoundMng::CYySePlayCursor();
		}
		this->field_18 = SelectedCharacter;
		if (this->charInfo[SelectedCharacter].field_74 != nullptr) {
			this->StartCamMove(false);
		}
		break;
	case 5:
		SelectedCharacter = a3 - 1;
		if (this->charInfo[SelectedCharacter].Exists == 0) {
			SoundMng::CYySePlayBeep();
		}
		else {
			SoundMng::CYySePlayClick();
			this->field_1C = 1;
			this->MenuCtrlData->field_5D = 1;
		}
		break;
	case 6:
		this->field_1C = 1;
		this->MenuCtrlData->field_5D = 1;
		SelectedCharacter = -1;
		SoundMng::CYySePlayCancel();
		break;
	case 8:
		if (PrimMng::g_pIwChfWin) {
			if (PrimMng::g_pIwChfWin->MenuCtrlData) {
				if (this->charInfo[SelectedCharacter].Exists) {
					TkManager::g_CTkMenuMng.ActiveDrawMenu(PrimMng::g_pIwChfWin->MenuCtrlData, 0);
					SoundMng::CYySePlayWindowSelect();
				}
				else {
					SoundMng::CYySePlayBeep();
				}
			}
		}
	}
}

void FFXI::CTk::CIwSelectMenu::OnDrawCalc(bool a2)
{
	if (!CIwSelectMenu::g_userfile) return;

	//sub //TODO keytype
	if (FFXI::Input::InputMng::GetKey(FFXI::Constants::Enums::KEYTYPE(63), FFXI::Constants::Enums::TRGTYPE::TRGTYPE_4)) {
		if (!a2) return;
		this->field_32 = 1;
		CIwSelectMenu::SelectedCharacter = this->MenuCtrlData->field_4C - 1;
		
		if (this->charInfo[SelectedCharacter].Exists) {
			this->field_1C = 1;
			this->MenuCtrlData->field_5D = 1;
		}
	}
	else
		this->field_32 = 0;

	if (a2 && TkManager::g_pCTkMouse->CheckMouse()) {
		tagPOINT pos{};
		if (TkManager::g_pCTkMouse)
			pos = TkManager::g_pCTkMouse->field_4;
		if (PrimMng::g_pIwChfWin->MenuCtrlData) {
			if (PrimMng::g_pIwChfWin->MenuCtrlData->IsOnWindow(pos)) {
				SelectedCharacter = this->MenuCtrlData->field_4C - 1;
				this->OnKeyDown(8, 0);
			}
		}
	}
}

void FFXI::CTk::CIwSelectMenu::CamMove()
{
	D3DXVECTOR4 v41[6];
	for (int i = 0; i < 3; ++i) {
		v41[i] = this->field_B38[i];
		v41[i + 3] = this->field_B68[i];
	}
	
	for (int i = 0; i < 2; ++i) {
		if (this->field_AF8[i] < 1.0) {
			long double v7 = this->field_AF8[i];
			D3DXVECTOR4* v8 = v41 + 3 * i;
			D3DXVECTOR4* v8_2 = v41 + 3 * i + 1;
			D3DXVECTOR4* v8_3 = v41 + 3 * i + 2;

			D3DXVECTOR4 diffone = *v8 - *v8_2;
			long double v14 = sqrt(diffone.x * diffone.x + diffone.y * diffone.y + diffone.z * diffone.z);
			D3DXVECTOR4 difftwo = *v8_3 - *v8_2;
			long double v19 = sqrt(difftwo.x * difftwo.x + difftwo.y * difftwo.y + difftwo.z * difftwo.z);
			long double v20 = 0.5 * (v19 + v14) - v14;
			float v36 = v20 * v7 + v20 * v7 + v14;
			float v39 = (v19 + v14) * -0.013333334; // -1/75
			long double v21 = v36 * v36 - v39 * v20 * 4.0;
			if (v21 <= 0.0) {
				v21 = 0.0;
			}
			if (fabs(v20) >= 0.009999999776482582) {
				v7 += (sqrt(v21) - v36) / (v20 + v20);
			}
			else if (v36 > 0.0){
				v7 -= v39 / v36;
			}
			if (v7 > 1.0) {
				v7 = 1.0;
			}
			this->field_AF8[i] = v7;
			long double v30 = 1.0 - v7;
			float v40[3];
			v40[0] = v30 * v30 * 0.5;
			v40[1] = v30 * v7 + 0.5;
			v40[2] = v7 * v7 * 0.5;
			if (i == 0) {
				this->field_B28 = { 0.0, 0.0, 0.0, 1.0 };
				for (int j = 0; j < 3; ++j) {
					v8[j].x = this->field_B68[j].x * v40[j];
					v8[j].y = this->field_B68[j].y * v40[j];
					v8[j].z = this->field_B68[j].z * v40[j];
					this->field_B28.x += v8[j].x;
					this->field_B28.y += v8[j].y;
					this->field_B28.z += v8[j].z;
				}
			}
			else {
				this->field_B18 = { 0.0, 0.0, 0.0, 1.0 };
				for (int j = 0; j < 3; ++j) {
					v8[j].x = this->field_B38[j].x * v40[j];
					v8[j].y = this->field_B38[j].y * v40[j];
					v8[j].z = this->field_B38[j].z * v40[j];
					this->field_B18.x += v8[j].x;
					this->field_B18.y += v8[j].y;
					this->field_B18.z += v8[j].z;
				}
			}
		}
	}
	CYyDb::g_pCYyDb->CameraManager->SetAt((D3DXVECTOR3*)&this->field_B18);
	CYyDb::g_pCYyDb->CameraManager->SetPos((D3DXVECTOR3*)&this->field_B28);
}

void FFXI::CTk::CIwSelectMenu::StartCamMove(bool a2)
{
	if (CIwSelectMenu::SelectedCharacter == -1) {
		return;
	}

	FFXI::CTk::IwCharInfo* charinfo1 = PrimMng::g_pIwSelectMenu->charInfo + this->field_14;
	FFXI::CTk::IwCharInfo* charinfo2 = PrimMng::g_pIwSelectMenu->charInfo + CIwSelectMenu::SelectedCharacter;

	if (a2 == true) {
		if (charinfo1->Exists == 0) {
			return;
		}

		if (charinfo1->field_A8 != 0) {
			return;
		}
	}
	else if (this->field_14 == SelectedCharacter) {
		return;
	}

	this->field_AF8[0] = 0;
	this->field_AF8[1] = 0;

	D3DXVECTOR4 v47{ 0.0, -1.0, 0.0, 1.0 };
	D3DXVECTOR4 v48{ 0.0, -1.0, 0.0, 1.0 };

	if (charinfo1->Exists != 0) {
		charinfo1->field_A8 = charinfo1->field_74->VirtActor103(7, &v48) & 0xFF;
		charinfo1->field_A9 = 0;
		charinfo1->field_AA = 0;
		charinfo1->field_AB = 0;

		if (charinfo1->field_A8 != 0) {
			v48.x -= charinfo1->field_78.x;
			v48.y -= charinfo1->field_78.y;
			v48.z -= charinfo1->field_78.z;
		}
	}

	if (charinfo2->Exists != 0) {
		charinfo2->field_A8 = charinfo2->field_74->VirtActor103(7, &v47) & 0xFF;
		charinfo2->field_A9 = 0;
		charinfo2->field_AA = 0;
		charinfo2->field_AB = 0;

		if (charinfo2->field_A8 != 0) {
			v47.x -= charinfo2->field_78.x;
			v47.y -= charinfo2->field_78.y;
			v47.z -= charinfo2->field_78.z;
		}
	}

	D3DXVECTOR4 v45{};
	int index_diff = CIwSelectMenu::SelectedCharacter - this->field_14;
	unsigned int abs_diff = abs(index_diff);
	if (abs_diff == 1 || abs_diff == 15 || abs_diff == 0) {
		v45.x = charinfo1->field_98.x + charinfo2->field_98.x + v47.x + v48.x;
		v45.y = charinfo1->field_98.y + charinfo2->field_98.y + v47.y + v48.y;
		v45.z = charinfo1->field_98.z + charinfo2->field_98.z + v47.z + v48.z;
		v45.x *= 0.5;
		v45.y *= 0.5;
		v45.z *= 0.5;
	}
	else {
		v45 = { 0.0, -2.0, 0.0, 1.0 };
		v45.x += this->field_B08.x;
		v45.y += this->field_B08.y;
		v45.z += this->field_B08.z;
	}

	D3DXVECTOR4 v46{};
	v46.x = FFXI::CYyDb::g_pCYyDb->CameraManager->Position.x;
	v46.y = FFXI::CYyDb::g_pCYyDb->CameraManager->Position.y;
	v46.z = FFXI::CYyDb::g_pCYyDb->CameraManager->Position.z;
	v46.w = FFXI::CYyDb::g_pCYyDb->CameraManager->field_50.x;

	v46.x -= v45.x;
	v46.y -= v45.y;
	v46.z -= v45.z;
	v46.x *= 2.0;
	v46.y *= 2.0;
	v46.z *= 2.0;

	v46.x += v45.x;
	v46.y += v45.y;
	v46.z += v45.z;

	this->field_B68[0].x = v46.x;
	this->field_B68[0].y = v46.y;
	this->field_B68[0].z = v46.z;
	this->field_B68[0].w = v46.w;
	
	this->field_B68[1].x = v45.x;
	this->field_B68[1].y = v45.y;
	this->field_B68[1].z = v45.z;
	this->field_B68[1].w = v45.w;

	v46 = charinfo2->field_98;
	if (charinfo2->Exists != 0) {
		D3DXVECTOR4 dummy{};
		if (charinfo2->field_74->VirtActor103(7, &dummy)) {
			v46 = charinfo2->field_88;
		}
	}

	v46.x += v47.x - v45.x;
	v46.y += v47.y - v45.y;
	v46.z += v47.z - v45.z;
	v46.x *= 2.0;
	v46.y *= 2.0;
	v46.z *= 2.0;

	v46.x += v45.x;
	v46.y += v45.y;
	v46.z += v45.z;

	this->field_B68[2].x = v46.x;
	this->field_B68[2].y = v46.y;
	this->field_B68[2].z = v46.z;

	if (abs_diff == 1 || abs_diff == 15 || abs_diff == 0) {
		v45.x = FFXI::CYyDb::g_pCYyDb->CameraManager->field_50.x;
		v45.y = FFXI::CYyDb::g_pCYyDb->CameraManager->field_50.y;
		v45.z = FFXI::CYyDb::g_pCYyDb->CameraManager->field_50.z;
		v46.w = FFXI::CYyDb::g_pCYyDb->CameraManager->field_5C;
		
		v45.x += charinfo2->field_78.x + v47.x;
		v45.y += charinfo2->field_78.y + v47.y;
		v45.z += charinfo2->field_78.z + v47.z;
		v45.x *= 0.5;
		v45.y *= 0.5;
		v45.z *= 0.5;
	}
	else {
		int some_index{};
		if (index_diff >= 0) {
			some_index = this->field_14 + index_diff / 2;
		}
		else {
			some_index = CIwSelectMenu::SelectedCharacter - index_diff / 2;
		}

		v45 = PrimMng::g_pIwSelectMenu->charInfo[some_index].field_78;
	}

	v45.x -= this->field_B08.x;
	v45.y -= this->field_B08.y;
	v45.z -= this->field_B08.z;
	v45.x *= 0.8;
	v45.y *= 0.8;
	v45.z *= 0.8;
	v45.x += this->field_B08.x;
	v45.y += this->field_B08.y;
	v45.z += this->field_B08.z;

	v46.x = FFXI::CYyDb::g_pCYyDb->CameraManager->field_50.x;
	v46.y = FFXI::CYyDb::g_pCYyDb->CameraManager->field_50.y;
	v46.z = FFXI::CYyDb::g_pCYyDb->CameraManager->field_50.z;
	v46.w = FFXI::CYyDb::g_pCYyDb->CameraManager->field_5C;

	v46.x -= v45.x;
	v46.y -= v45.y;
	v46.z -= v45.z;
	v46.x *= 2.0;
	v46.y *= 2.0;
	v46.z *= 2.0;
	v46.x += v45.x;
	v46.y += v45.y;
	v46.z += v45.z;

	this->field_B38[0].x = v46.x;
	this->field_B38[0].y = v46.y;
	this->field_B38[0].z = v46.z;
	this->field_B38[0].w = v46.w;

	this->field_B38[1].x = v45.x;
	this->field_B38[1].y = v45.y;
	this->field_B38[1].z = v45.z;
	this->field_B38[1].w = v45.w;

	v46 = charinfo2->field_78;
	v46.x += v47.x - v45.x;
	v46.y += v47.y - v45.y;
	v46.z += v47.z - v45.z;
	v46.x *= 2.0;
	v46.y *= 2.0;
	v46.z *= 2.0;
	v46.x += v45.x;
	v46.y += v45.y;
	v46.z += v45.z;
	
	this->field_B38[2].x = v46.x;
	this->field_B38[2].y = v46.y;
	this->field_B38[2].z = v46.z;
	this->field_B38[2].w = v46.w;
	this->field_14 = CIwSelectMenu::SelectedCharacter;
}
