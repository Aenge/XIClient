#pragma once
#include "GP_SERV_PACKETS.h"
namespace FFXI {
	namespace Network { namespace UDP { class GP_GAME_PACKET_HEAD; } }
	namespace Zone {
		class GC_ZONE;
		namespace RecvCallbacks {
			extern void RecvEquipClear(FFXI::Zone::GC_ZONE*, FFXI::Network::UDP::GP_GAME_PACKET_HEAD*, FFXI::Network::UDP::GP_SERV_PACKETS::GP_SERV_EQUIP_CLEAR*);
			extern void RecvLogIn(FFXI::Zone::GC_ZONE*, FFXI::Network::UDP::GP_GAME_PACKET_HEAD*, FFXI::Network::UDP::GP_SERV_PACKETS::GP_SERV_LOGIN*);
		}
	}
}