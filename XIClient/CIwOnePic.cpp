#include "CIwOnePic.h"
#include "CTkMenuCtrlData.h"
#include "TkManager.h"
#include "CTkMenuMng.h"
#include "CYyDb.h"
#include "CMoResourceMng.h"
#include "ResourceContainer.h"
#include "IwManager.h"
#include "CYyMsb.h"
#include "_49SubList.h"
#include "CDx.h"
#include "Strings.h"

using namespace FFXI::CTk;

FFXI::CTk::CIwOnePic::CIwOnePic()
{
	this->init(0);
}

FFXI::CTk::CIwOnePic::~CIwOnePic()
{
	//nullsub
}

void FFXI::CTk::CIwOnePic::OnInitialUpdatePrimitive()
{
	this->MenuCtrlData->field_85 = 0;
	this->MenuCtrlData->field_86 = 0;
	this->MenuCtrlData->field_84 = 0;
	this->field_28 = 0;
	tagPOINT a2{};
	a2.x = 32;
	a2.y = 32;
	FFXI::CTk::TkManager::g_CTkMenuMng.GetSomePosition(&a2, 0, true);
	this->MenuCtrlData->RePosition(a2.x, a2.y);
}

void FFXI::CTk::CIwOnePic::OnDrawPrimitive()
{
	if (this->field_2C == 0) {
		if (this->field_34 == nullptr) {
			return;
		}

		CYy::CMoResource** res{ nullptr };
		(*this->field_34)->FindResourceUnder(&res, FFXI::Constants::Enums::ResourceType::Msb, 0);

		if (res == nullptr) {
			return;
		}

		CYy::CYyMsb* msb = (CYy::CYyMsb*)*res;
		FFXI::CTk::_49SubList* sublist = **FFXI::CTk::TkManager::g_CTkMenuDataList.FindMenuShapeFile((char*)(msb + 1));

		if (sublist == nullptr) {
			return;
		}

		FFXI::CYy::CDx::instance->DXDevice->SetTextureStageState(0, D3DTSS_MINFILTER, D3DTEXF_LINEAR);
		FFXI::CYy::CDx::instance->DXDevice->SetTextureStageState(0, D3DTSS_MAGFILTER, D3DTEXF_LINEAR);

		float xscale = (double)FFXI::CTk::TkManager::g_CTkMenuMng.UIXRes / 512.0;
		float yscale = (double)FFXI::CTk::TkManager::g_CTkMenuMng.UIYRes / 448.0;
		unsigned int color = this->field_24 | ((this->field_24 | ((this->field_24 | (this->field_24 << 8)) << 8)) << 8);
		
		sublist->ExtraDraw(0, 0, xscale, yscale, color, 0, 0, 0);

		FFXI::CYy::CDx::instance->DXDevice->SetTextureStageState(0, D3DTSS_MINFILTER, D3DTEXF_LINEAR);
		FFXI::CYy::CDx::instance->DXDevice->SetTextureStageState(0, D3DTSS_MAGFILTER, D3DTEXF_POINT);
	}
	else if (this->field_2C != 1) {
		this->field_28 += 1;
		
		float yscale = 1.0;
		if (this->field_28 < 4) {
			yscale = (double)this->field_28 / 4.0;
		}

		int v7 = 2 * this->field_28;
		if (v7 > 8) {
			v7 = 8;
		}

		FFXI::CTk::_49SubList** sublists = *FFXI::CTk::TkManager::g_CTkMenuDataList.FindMenuShapeFile(FFXI::Constants::Strings::MenuKeyTops3);
		
		int index = 0xA5 + (((unsigned int)this->field_28 >> 2) % 5);
		FFXI::CTk::_49SubList* sublist = sublists[index];

		short xdiff = FFXI::CTk::TkManager::g_CTkMenuMng.UIXRes - FFXI::CTk::TkManager::g_CTkMenuMng.field_7C;
		short ydiff = FFXI::CTk::TkManager::g_CTkMenuMng.UIYRes - FFXI::CTk::TkManager::g_CTkMenuMng.field_7E;

		float xmod = 1.0 - FFXI::CTk::TkManager::g_CTkMenuMng.Get84(true) * 0.5;
		float ymod = 1.0 - FFXI::CTk::TkManager::g_CTkMenuMng.Get88(true) * 0.5;

		float x = xdiff * xmod - 234.0;
		float y = ydiff * ymod - 48.0;

		sublist->ExtraDraw((int)x, (int)y - v7 + 8, 1.0, yscale, 0x80808080, 0, 0, 0);
	}
}

void FFXI::CTk::CIwOnePic::OnDrawCalc(bool)
{
	if (this->field_18 == 0) {
		if (this->field_1C != this->field_20) {
			this->field_20 = this->field_1C;
			this->field_18 = 1;
		}
	}
	else if (this->field_18 < 10) {
		this->field_18 += 1;
	}
	else if (this->field_18 == 10) {
		int v6 = -(this->field_20 != this->field_1C);
		v6 &= 0xFFFFFFF5;
		this->field_18 = v6 + 11;
	}

	if (this->field_2C == 0) {
		switch (this->field_18) {
		case 11:
			if (this->field_24 == 0) {
				this->field_18 = 12;
			}
			else {
				this->field_24 -= 8;
			}
			break;
		case 12:
			if (this->field_34 != nullptr) {
				(*this->field_34)->DecrementReferenceCount();
				FFXI::CYy::CMoResource::UnlinkFromManager((CYy::CMoResource***) & this->field_34);
				this->field_34 = nullptr;
			}

			FFXI::CYyDb::pCMoResourceMng->LoadNumFile(&this->field_30, 0x9ACF + this->field_20);
			//no check for nullptr...
			(*this->field_30)->IncrementReferenceCount();
			this->field_18 = 13;
			break;
		case 13:
			if (FFXI::CYy::CMoResourceMng::IsResourceReady((CYy::CMoResource***) & this->field_30) == true) {
				this->field_34 = this->field_30;
				this->field_18 = 14;
			}
			break;
		case 14:
			if (this->field_24 == 128) {
				this->field_18 = 15;
			}
			else {
				this->field_24 += 8;
			}
			break;
		case 15:
			this->field_18 = 0;
			break;
		default:
			break;
		}
	}
	else if (this->field_2C == 1) {
		switch (this->field_18) {
		case 11:
			FFXI::CYyDb::pCMoResourceMng->LoadStartScheduler(0x81A9, 'toff', nullptr, nullptr, 0);
			this->field_24 = 31;
			this->field_18 = 12;
			break;
		case 12:
			if (this->field_24 == 0) {
				this->field_18 = 13;
			}
			else {
				this->field_24 -= 1;
			}
			break;
		case 13:
			this->field_18 = 14;
			break;
		case 14:
			FFXI::CTk::IwManager::IwReleaseDemo();
			FFXI::CTk::IwManager::IwBootDemo(this->field_20 + 1);
			//sub //todo volume
			this->field_18 = 15;
			break;
		case 15:
			this->field_18 = 16;
			break;
		case 16:
			FFXI::CYyDb::pCMoResourceMng->LoadStartScheduler(0x81A9, 'niff', nullptr, nullptr, 0);
			this->field_18 = 17;
			break;
		case 17:
			if (this->field_24 == 31) {
				this->field_18 = 18;
			}
			else {
				this->field_24 += 1;
			}
			break;
		case 18:
			this->field_18 = 0;
			break;
		default:
			break;
		}
	}
}

void FFXI::CTk::CIwOnePic::init(int a2)
{
	this->field_24 = 128;
	this->field_1C = -1;
	this->field_20 = -1;
	this->field_18 = 0;
	this->field_30 = 0;
	this->field_34 = 0;
	this->field_2C = a2;
}
