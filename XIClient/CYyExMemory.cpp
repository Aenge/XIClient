#include "CYyExMemory.h"

using namespace FFXI::CYy;

const BaseGameObject::ClassInfo CYyExMemory::CYyExMemoryClass{
	"CYyExMemory", sizeof(CYyExMemory), &BaseGameObject::BaseGameObjectClass
};
const BaseGameObject::ClassInfo* CYyExMemory::GetRuntimeClass() {
	return &CYyExMemoryClass;
}

