#pragma once
namespace FFXI {
	namespace Constants {
		namespace Enums {
			//class instead of enum for implicit conversion
			class PcActorModelSlots {
			public:
				static const int FACE = 0;
				static const int HEAD = 1;
				static const int BODY = 2;
				static const int HAND = 3;
				static const int LEG = 4;
				static const int FOOT = 5;
				static const int MH = 6;
				static const int OH = 7;
				static const int RANGE = 8;
			};
		}
	}
}