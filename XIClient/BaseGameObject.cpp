#include "BaseGameObject.h"

const FFXI::CYy::BaseGameObject::ClassInfo FFXI::CYy::BaseGameObject::BaseGameObjectClass{ "BaseGameObject", sizeof(BaseGameObject), nullptr };
const FFXI::CYy::BaseGameObject::ClassInfo* FFXI::CYy::BaseGameObject::GetRuntimeClass() { return &BaseGameObject::BaseGameObjectClass; }

void FFXI::CYy::BaseGameObject::VObj2(int*)
{
	//nullsub
}

unsigned int FFXI::CYy::BaseGameObject::VObj3(char)
{
	return 1;
}

int FFXI::CYy::BaseGameObject::VObj4()
{
	return 1;
}

void FFXI::CYy::BaseGameObject::VObj5(void*)
{
}

void FFXI::CYy::BaseGameObject::VObj6()
{
}

FFXI::CYy::BaseGameObject::BaseGameObject()
{
	//nullsub
}

FFXI::CYy::BaseGameObject::~BaseGameObject()
{
	//nullsub
}

bool FFXI::CYy::BaseGameObject::IsKindOf(const BaseGameObject::ClassInfo* classType)
{
	const BaseGameObject::ClassInfo* currentClass = this->GetRuntimeClass();

	while (currentClass != nullptr) {
		if (currentClass == classType)
			return true;

		currentClass = currentClass->ClassParent;
	}

	return false;
}
