#pragma once
#include "CTkMsgBase.h"

namespace FFXI {
	namespace CTk {
		class CTkEventMsgBase : public CTkMsgBase {
		public:
			CTkEventMsgBase(CTkDrawMessageWindow*);
			int field_68;
			int field_6C;
		};
	}
}