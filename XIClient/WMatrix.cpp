#include "WMatrix.h"
#include "Globals.h"
#include "BaseProcessor.h"
#include "CYyDb.h"

FFXI::Math::WMatrix::WMatrix(bool)
{
	this->Identity();
}

void FFXI::Math::WMatrix::Identity()
{
	D3DXMatrixIdentity(this);
}

bool FFXI::Math::WMatrix::CheckMatrix()
{
	if (!Globals::CheckFloat(this->_11)) return false;
	if (!Globals::CheckFloat(this->_12)) return false;
	if (!Globals::CheckFloat(this->_13)) return false;
	if (!Globals::CheckFloat(this->_14)) return false;
	if (!Globals::CheckFloat(this->_21)) return false;
	if (!Globals::CheckFloat(this->_22)) return false;
	if (!Globals::CheckFloat(this->_23)) return false;
	if (!Globals::CheckFloat(this->_24)) return false;
	if (!Globals::CheckFloat(this->_31)) return false;
	if (!Globals::CheckFloat(this->_32)) return false;
	if (!Globals::CheckFloat(this->_33)) return false;
	if (!Globals::CheckFloat(this->_34)) return false;
	if (!Globals::CheckFloat(this->_41)) return false;
	if (!Globals::CheckFloat(this->_42)) return false;
	if (!Globals::CheckFloat(this->_43)) return false;
	if (!Globals::CheckFloat(this->_44)) return false;
	return true;
}

void FFXI::Math::WMatrix::Vec3TransformDrop4Self(D3DXVECTOR3* a2)
{
	D3DXVECTOR3 v1 = *a2;
	this->Vec3TransformDrop4(a2, &v1);
}

void FFXI::Math::WMatrix::Vec3TransformDrop4(D3DXVECTOR3* out, const D3DXVECTOR3* in)
{
	out->x = this->_11 * in->x + this->_21 * in->y + this->_31 * in->z + this->_41;
	out->y = this->_12 * in->x + this->_22 * in->y + this->_32 * in->z + this->_42;
	out->z = this->_13 * in->x + this->_23 * in->y + this->_33 * in->z + this->_43;
}

void FFXI::Math::WMatrix::Vec3TransformNormal(D3DXVECTOR3* a2, D3DXVECTOR3* a3)
{
	a2->x = this->_31 * a3->z + this->_21 * a3->y + a3->x * this->_11;
	a2->y = this->_32 * a3->z + this->_22 * a3->y + this->_12 * a3->x;
	a2->z = this->_33 * a3->z + this->_23 * a3->y + this->_13 * a3->x;
}

void FFXI::Math::WMatrix::Vec3TransformNormalSelf(D3DXVECTOR3* a2)
{
	const D3DXVECTOR3 a1 = *a2;
	D3DXVec3TransformNormal(a2, &a1, this);
}

void FFXI::Math::WMatrix::Vec3CharCollisionHelper(D3DXVECTOR3* a2, D3DXVECTOR3* a3, D3DXVECTOR3* a4)
{
	float a3_mag = sqrt(a3->x * a3->x + a3->y * a3->y + a3->z * a3->z);
	float v7 = a4->y * a3->z - a3->y * a4->z;
	float v8 = a3->x * a4->z - a4->x * a3->z;
	float v9 = a3->y * a4->x - a3->x * a4->y;
	float a3a = 1.0f / sqrt(v9 * v9 + v8 * v8 + v7 * v7);

	float v20 = a3a * v7;
	float v10 = a3a * v8;
	float v11 = a3a * v9;

	float v17 = a3->x / a3_mag;
	float v13 = a3->y / a3_mag;
	float v14 = a3->z / a3_mag;

	FFXI::Math::WMatrix new_matrix{};
	new_matrix._11 = v20;
	new_matrix._12 = v10;
	new_matrix._13 = v11;
	new_matrix._14 = 0.0f;
	
	new_matrix._21 = v13 * v11 - v14 * v10;
	new_matrix._22 = v14 * v20 - v17 * v11;
	new_matrix._23 = v17 * v10 - v13 * v20;
	new_matrix._24 = 0.0f;
	
	new_matrix._31 = v17;
	new_matrix._32 = v13;
	new_matrix._33 = v14;
	new_matrix._34 = 0.0f;
	
	new_matrix._41 = a2->x;
	new_matrix._42 = a2->y;
	new_matrix._43 = a2->z;
	new_matrix._44 = 1.0f;

	float t_41 = new_matrix._41;
	float t_42 = new_matrix._42;
	float t_43 = new_matrix._43;

	this->_11 = new_matrix._11;
	this->_12 = new_matrix._21;
	this->_13 = new_matrix._31;
	this->_14 = 0.0f;

	this->_21 = new_matrix._12;
	this->_22 = new_matrix._22;
	this->_23 = new_matrix._32;
	this->_24 = 0.0f;

	this->_31 = new_matrix._13;
	this->_32 = new_matrix._23;
	this->_33 = new_matrix._33;
	this->_34 = 0.0f;

	this->_41 = -(t_43 * this->_31 + t_42 * this->_21 + t_41 * this->_11);
	this->_42 = -(t_43 * this->_32 + t_42 * this->_22 + t_41 * this->_12);
	this->_43 = -(t_43 * this->_33 + t_42 * this->_23 + t_41 * this->_13);
	this->_44 = new_matrix._44;
}

void FFXI::Math::WMatrix::RotateX(float angle)
{
	WMatrix a1{};
	D3DXMatrixRotationX(&a1, angle);
	this->MatrixMultiply(&a1);
}

void FFXI::Math::WMatrix::RotateY(float angle)
{
	WMatrix a1{};
	D3DXMatrixRotationY(&a1, angle);
	this->MatrixMultiply(&a1);
}

void FFXI::Math::WMatrix::RotateZ(float angle)
{
	WMatrix a1{};
	D3DXMatrixRotationZ(&a1, angle);
	this->MatrixMultiply(&a1);
}

void FFXI::Math::WMatrix::Scale3(const D3DXVECTOR3* a2)
{
	this->Identity();
	this->_11 = a2->x;
	this->_22 = a2->y;
	this->_33 = a2->z;
	this->_44 = 1.0f;
}

/**
 * Multiplies two D3DXMATRIX objects: result = this * mat
 *
 * @param this Pointer to the matrix that will store the result
 * @param mat Pointer to the right-hand matrix in the multiplication
 */
void FFXI::Math::WMatrix::MatrixMultiply(const WMatrix* mat)
{
	WMatrix temp{};

	// For each row i
	for (int i = 0; i < 4; i++)
	{
		// For each column j
		for (int j = 0; j < 4; j++)
		{
			// Calculate dot product of row i of 'this' and column j of 'mat'
			float sum = 0.0f;
			for (int k = 0; k < 4; k++)
			{
				sum += this->m[i][k] * mat->m[k][j];
			}
			temp.m[i][j] = sum;
		}
	}

	// Copy result back to 'this'
	*this = temp;
}

/**
 * Multiplies two D3DXMATRIX objects in reverse order: result = mat * this
 *
 * @param this Pointer to the matrix that will store the result
 * @param mat Pointer to the left-hand matrix in the multiplication
 */
void FFXI::Math::WMatrix::MatrixReverseMultiply(const WMatrix* mat)
{
	WMatrix temp{};

	// For each row i
	for (int i = 0; i < 4; i++)
	{
		// For each column j
		for (int j = 0; j < 4; j++)
		{
			// Calculate dot product of row i of 'mat' and column j of 'this'
			float sum = 0.0f;
			for (int k = 0; k < 4; k++)
			{
				sum += mat->m[i][k] * this->m[k][j];
			}
			temp.m[i][j] = sum;
		}
	}

	// Copy result back to 'this'
	*this = temp;
}

void FFXI::Math::WMatrix::MatrixInvert()
{
	for (int i = 0; i < 4; ++i) {
		double factor = this->m[i][i];
		if (factor == 0.0f) {
			factor = 1.0f;
		}
		double invert = 1.0f / factor;
		if (invert == 0.0f) {
			invert = 1.0f;
		}
		for (int j = 0; j < 4; ++j) {
			this->m[i][j] *= invert;
		}
		this->m[i][i] = invert;
		for (int j = 0; j < 4; ++j) {
			if (j != i) {
				double v12 = this->m[j][i];
				for (int k = 0; k < 4; ++k) {
					if (k == i) {
						this->m[j][k] = -(v12 * invert);
					}
					else {
						this->m[j][k] -= v12 * this->m[i][k];
					}
				}
			}
		}
	}
}

void FFXI::Math::WMatrix::Vec4TransformSelf(D3DXVECTOR4* a1)
{
	const D3DXVECTOR4 v4 = *a1;
	D3DXVec4Transform(a1, &v4, this);
}

void FFXI::Math::WMatrix::TransformVectorInPlace(D3DXVECTOR4* a2)
{
	D3DXVECTOR4 a1 = *a2;
	this->TransformVector(a2, &a1);
}

void FFXI::Math::WMatrix::TransformVector(D3DXVECTOR4* out, const D3DXVECTOR4* in)
{
	out->x = this->_41 * in->w + this->_21 * in->y + this->_11 * in->x + this->_31 * in->z;
	out->y = this->_42 * in->w + this->_22 * in->y + this->_12 * in->x + this->_32 * in->z;
	out->z = this->_43 * in->w + this->_23 * in->y + this->_13 * in->x + this->_33 * in->z;
	out->w = this->_44 * in->w + this->_24 * in->y + this->_14 * in->x + this->_34 * in->z;
}

void FFXI::Math::WMatrix::Vec4MultiplyAndScale(D3DXVECTOR4* a2, D3DXVECTOR4* a3)
{
	this->TransformVector(a2, a3);
	double scale = 0.0f;
	if (a2->w != 0.0f) {
		scale = 1.0f / a2->w;
	}
	a2->x *= scale;
	a2->y *= scale;
	a2->z *= scale;
}

void FFXI::Math::WMatrix::Vec4MultiplySelfAndScale(D3DXVECTOR4* a2)
{
	this->TransformVectorInPlace(a2);
	double scale = 0.0f;
	if (a2->w != 0.0f) {
		scale = 1.0f / a2->w;
	}
	a2->x *= scale;
	a2->y *= scale;
	a2->z *= scale;
}

void FFXI::Math::WMatrix::SomeCombo(WMatrix* a2) {
	WMatrix copy = *a2;

	a2->_11 = copy._11 * this->_11 + copy._31 * this->_13 + copy._21 * this->_12;
	a2->_12 = copy._12 * this->_11 + copy._32 * this->_13 + copy._22 * this->_12;
	a2->_13 = copy._13 * this->_11 + copy._33 * this->_13 + copy._23 * this->_12;

	a2->_11 = copy._11 * this->_11 + copy._31 * this->_13 + copy._21 * this->_12;
	a2->_12 = copy._12 * this->_11 + copy._32 * this->_13 + copy._22 * this->_12;
	a2->_23 = copy._13 * this->_21 + copy._33 * this->_23 + copy._23 * this->_22;

	a2->_11 = copy._11 * this->_11 + copy._31 * this->_13 + copy._21 * this->_12;
	a2->_12 = copy._12 * this->_11 + copy._32 * this->_13 + copy._22 * this->_12;
	a2->_33 = copy._13 * this->_31 + copy._33 * this->_33 + copy._23 * this->_32;

	a2->_11 = copy._11 * this->_11 + copy._31 * this->_13 + copy._21 * this->_12;
	a2->_12 = copy._12 * this->_11 + copy._32 * this->_13 + copy._22 * this->_12;
	a2->_43 = copy._13 * this->_41 + copy._33 * this->_43 + copy._23 * this->_42;
}
void FFXI::Math::WMatrix::DoSomething()
{
	D3DXVECTOR4 a1 = this->m[3];
	WMatrix p_src{};

	this->_44 = 0.0f;
	this->_43 = 0.0f;
	this->_42 = 0.0f;
	this->_41 = 0.0f;
	D3DXMatrixTranspose(&p_src, this);
	p_src.Vec4TransformSelf(&a1);

	p_src._41 = -a1.x;
	p_src._42 = -a1.y;
	p_src._43 = -a1.z;
	p_src._44 = 1.0f;

	*this = p_src;
}

/**
 * @brief Determines if a bounding box is visible within the view frustum
 *
 * This function performs frustum culling by transforming the corners of a bounding box
 * by the view-projection matrix and checking if all corners are outside any single frustum plane.
 *
 * @param this The view-projection matrix to transform the bounding box
 * @param boundingBoxCorners Array of 8 points representing the corners of the bounding box
 * @return false if the bounding box may be visible, true if it's definitely not visible
 */
bool FFXI::Math::WMatrix::IsBoxOutsideFrustum(D3DXVECTOR3* boundingBoxCorners)
{
	// Initialize the outsidePlaneMask to all planes (0x3F = 00111111 in binary)
	// This represents that all points may be outside all planes initially
	unsigned char outsidePlaneMask = 0x3F;

	for (int i = 0; i < 8; ++i) {
		D3DXVECTOR4 transformedVertex{};
		D3DXVec3Transform(&transformedVertex, boundingBoxCorners + i, this);

		// Use absolute w for comparison (handles negative w values)
		float absW = abs(transformedVertex.w);

		unsigned char vertexMask = 0;

		// Test right plane (x > w)
		if (transformedVertex.x > absW)
			vertexMask |= 0x01;

		// Test left plane (x < -w)
		if (transformedVertex.x < -absW)
			vertexMask |= 0x02;

		// Test top plane (y > w)
		if (transformedVertex.y > absW)
			vertexMask |= 0x04;

		// Test bottom plane (y < -w)
		if (transformedVertex.y < -absW)
			vertexMask |= 0x08;

		// Test far plane (z > w)
		if (transformedVertex.z > absW)
			vertexMask |= 0x10;

		// Test near plane (z < -w)
		if (transformedVertex.z < -absW)
			vertexMask |= 0x20;

		// Update mask to include only planes where all corners so far are outside
		outsidePlaneMask &= vertexMask;

		// Early exit: If no plane has all points outside, box may be visible
		if (outsidePlaneMask == 0)
			return false;
	}

	return true;
}

/**
 * @brief Determines if a bounding box is visible within the view frustum, excluding far plane checks
 *
 * Unlike full frustum culling, this function will not cull objects that are behind the far plane
 *
 * @param this The view-projection matrix to transform the bounding box
 * @param boundingBoxCorners Array of 8 points representing the corners of the bounding box
 * @return false if the bounding box may be visible when considering all planes except the far plane
 */
bool FFXI::Math::WMatrix::IsBoxOutsideFrustumExcludingFarPlane(D3DXVECTOR3* boundingBoxCorners)
{
	// Initialize the outsidePlaneMask to all planes (0x3F = 00111111 in binary)
	// This represents that all points may be outside all planes initially
	unsigned char outsidePlaneMask = 0x3F;

	for (int i = 0; i < 8; ++i) {
		D3DXVECTOR4 transformedVertex{};
		D3DXVec3Transform(&transformedVertex, boundingBoxCorners + i, this);

		// Use absolute w for comparison (handles negative w values)
		float absW = abs(transformedVertex.w);

		unsigned char vertexMask = 0;

		// Test right plane (x > w)
		if (transformedVertex.x > absW)
			vertexMask |= 0x01;

		// Test left plane (x < -w)
		if (transformedVertex.x < -absW)
			vertexMask |= 0x02;

		// Test top plane (y > w)
		if (transformedVertex.y > absW)
			vertexMask |= 0x04;

		// Test bottom plane (y < -w)
		if (transformedVertex.y < -absW)
			vertexMask |= 0x08;

		// Test near plane (z < -w)
		if (transformedVertex.z < -absW)
			vertexMask |= 0x20;

		// Update mask to include only planes where all corners so far are outside
		outsidePlaneMask &= vertexMask;

		// Early exit: If no plane has all points outside, box may be visible
		if (outsidePlaneMask == 0)
			return false;
	}

	return true;
}

void FFXI::Math::WMatrix::CreateScaling(D3DXVECTOR4* a2)
{
	this->Identity();
	this->_11 = a2->x;
	this->_22 = a2->y;
	this->_33 = a2->z;
	this->_44 = a2->w;
}

void FFXI::Math::WMatrix::AddTranslation4(D3DXVECTOR4* a2)
{
	this->_41 = a2->x;
	this->_42 = a2->y;
	this->_43 = a2->z;
	this->_44 = a2->w;
}

void FFXI::Math::WMatrix::AddTranslation3(const D3DXVECTOR3* a2)
{
	this->_41 = a2->x;
	this->_42 = a2->y;
	this->_43 = a2->z;
}

void __fastcall FFXI::Math::WMatrix::Vec3TransNorm(D3DXVECTOR3* a2, float a3, float a4, float a5)
{
	a2->x = a3 * this->_11 + a4 * this->_21 + a5 * this->_31 + this->_41;
	a2->y = a3 * this->_12 + a4 * this->_22 + a5 * this->_32 + this->_42;
	a2->z = a3 * this->_13 + a4 * this->_23 + a5 * this->_33 + this->_43;
}

void FFXI::Math::WMatrix::RHPerspective(float fovy, float Aspect, float zn, float zf)
{
	float v8 = fovy * 0.5f;
	float v6 = cos(v8) / sin(v8);
	float v7 = zf / (zn - zf);

	this->_34 = -1.0f;
	this->_11 = v6 / Aspect;
	this->_12 = 0.0f;
	this->_13 = 0.0f;
	this->_14 = 0.0f;
	this->_21 = 0.0f;
	this->_22 = v6;
	this->_23 = 0.0f;
	this->_24 = 0.0f;
	this->_31 = 0.0f;
	this->_32 = 0.0f;
	this->_33 = v7;
	this->_41 = 0.0f;
	this->_42 = 0.0f;
	this->_43 = v7 * zn;
	this->_44 = 0.0f;
}
