#include "CXiSkeletonActor.h"
#include "TkManager.h"
#include "CTkInputCtrl.h"
#include "CYyDb.h"
#include "Globals.h"
#include "GlobalStruct.h"
#include "XiZone.h"
#include "CYyCamMng2.h"
#include "FsConfig.h"
#include "PlaceHolders.h"
#include "XiAtelBuff.h"
#include "InputMng.h"
#include "SoundMng.h"
#include "TkManager.h"
#include "CTkMouse.h"
#include "CFsConf6Win.h"
using namespace FFXI::CYy;

const BaseGameObject::ClassInfo CXiControlActor::CXiControlActorClass{
	"CXiControlActor", sizeof(CXiControlActor), &CXiActor::CXiActorClass
};

bool CXiControlActor::enable_user_control_camera{ true };
bool CXiControlActor::is_first_person_view{ false };
bool CXiControlActor::is_auto_running{ false };
CMoAttachmentsSubStruct CXiControlActor::user_control_target{};
CMoAttachmentsSubStruct CXiControlActor::follow_actor{};
D3DXVECTOR4 CXiControlActor::auto_run_vec{};

void FFXI::CYy::CXiControlActor::UserControlCamera(CXiControlActor* a1)
{
	if (GlobalStruct::g_GlobalStruct.field_0 == 64) {
		return;
	}

	if (a1 == nullptr) {
		return;
	}

	if (CXiControlActor::is_first_person_view == true) {
		exit(0x100A457A);
		//FFXI::CYyDb::g_pCYyDb->CameraManager->SetEye();
	}
	else {
		if (a1->IsParallelMove() != 0) {
			if (a1->IsFreeRun() != 0) {
				exit(0x100A45B3);
			}
		}
		FFXI::CYyDb::g_pCYyDb->CameraManager->Follow(a1);
	}
	
	a1->ModeControl();
}

void FFXI::CYy::CXiControlActor::IsFirstPersonView(bool a1)
{
	is_first_person_view = a1;
	CXiActor* actor = CXiActor::control_actor.GetSearchActor();
	if (actor != nullptr && a1 == false) {
		actor->field_B2 &= 0xFFFB;
	}
}

void FFXI::CYy::CXiControlActor::IsAutoRunning(bool a2)
{
	is_auto_running = a2;
	if (a2 == false) {
		CXiControlActor::auto_run_vec = { 0.0f, 0.0f, 0.0f, 1.0f };
		if (CXiControlActor::follow_actor.GetSearchActor() != nullptr) {
			Placeholder::CTkMsgWinData;
		}
		CXiControlActor::follow_actor.SetActor(nullptr);
	}
}

void FFXI::CYy::CXiControlActor::EnableUserControlCamera()
{
	enable_user_control_camera = true;
	CXiActor* actor = CXiActor::control_actor.GetSearchActor();
	if (actor != nullptr) {
		static const D3DXVECTOR4 def_pos = { 0.0, 0.0, 0.0, 1.0 };
		FFXI::CYyDb::g_pCYyDb->CameraManager->SetCameraPos((D3DXVECTOR3*) & def_pos, actor);
		FFXI::CYyDb::g_pCYyDb->CameraManager->field_A4 = 0.0;
		FFXI::CYyDb::g_pCYyDb->CameraManager->field_8A = 1;
	}

	if (is_first_person_view == true) {
		FFXI::CYyDb::g_pCYyDb->SetProjection(280.0);
	}
	else {
		float projection = (float)FFXI::Config::FsConfig::GetConfig(FFXI::Constants::Enums::FsConfigSubjects::Subject73);
		FFXI::CYyDb::g_pCYyDb->SetProjection(projection);
	}
}

const BaseGameObject::ClassInfo* FFXI::CYy::CXiControlActor::GetRuntimeClass()
{
	return &CXiControlActor::CXiControlActorClass;
}

char FFXI::CYy::CXiControlActor::OnMove()
{
	float v47 = CYyDb::g_pCYyDb->CheckTick();
	char retval = this->CXiAtelActor::OnMove();
	if (retval == 0) {
		this->field_5B4.x = this->field_D4.x;
		this->field_5B4.y = this->field_D4.y;
		this->field_5B4.z = this->field_D4.z;

		D3DXVECTOR4 v44{};
		if ((this->field_88 & 1) == 1) {
			v44 = this->field_D4;
		}
		else {
			this->field_E4 = this->field_44;
			if (this->field_103 != 0 || this->field_104 != 0) {
				D3DXVECTOR4* v9 = this->GetPos();
				v44 = *v9;
			}
			else {
				v44 = this->field_C4;
			}
		}

		if (this->AmIControlActor() == true) {
			if (CYyDb::g_pCYyDb->field_8 != 0) {

				switch (this->GetGameStatus()) {
				case Constants::Enums::GAME_STATUS::N_IDLE:
				case Constants::Enums::GAME_STATUS::B_IDLE:
				case Constants::Enums::GAME_STATUS::EVT:
				case Constants::Enums::GAME_STATUS::ES2:
				case Constants::Enums::GAME_STATUS::ES5:
					if (this->field_103 != 0) {
						this->ApproachControl(&v44);
					}
					else if (this->field_104 != 0) {
						this->BackJumpControl(&v44);
					}
					else if (this->field_102 != 0) {
						this->BlowBackControl(&v44);
					}
					else if (this->field_101 != 0) {
						this->ActorMoveControl(&v44);
					}
					else {
						this->UserControl(&v44);
					}
					break;
				case Constants::Enums::GAME_STATUS::N_DEAD:
				case Constants::Enums::GAME_STATUS::B_DEAD:
					break;
				case Constants::Enums::GAME_STATUS::POL:
				case Constants::Enums::GAME_STATUS::CAMP:
				case Constants::Enums::GAME_STATUS::ITEMMAKE:
				case Constants::Enums::GAME_STATUS::SIT:
				case Constants::Enums::GAME_STATUS::ICRYSTALL:
				case Constants::Enums::GAME_STATUS::CHAIR00:
				case Constants::Enums::GAME_STATUS::CHAIR01:
				case Constants::Enums::GAME_STATUS::CHAIR02:
				case Constants::Enums::GAME_STATUS::CHAIR03:
				case Constants::Enums::GAME_STATUS::CHAIR04:
				case Constants::Enums::GAME_STATUS::CHAIR05:
				case Constants::Enums::GAME_STATUS::CHAIR06:
				case Constants::Enums::GAME_STATUS::CHAIR07:
				case Constants::Enums::GAME_STATUS::CHAIR08:
				case Constants::Enums::GAME_STATUS::CHAIR09:
				case Constants::Enums::GAME_STATUS::CHAIR10:
				case Constants::Enums::GAME_STATUS::CHAIR11:
				case Constants::Enums::GAME_STATUS::CHAIR12:
				case Constants::Enums::GAME_STATUS::CHAIR13:
				case Constants::Enums::GAME_STATUS::CHAIR14:
				case Constants::Enums::GAME_STATUS::CHAIR15:
				case Constants::Enums::GAME_STATUS::CHAIR16:
				case Constants::Enums::GAME_STATUS::CHAIR17:
				case Constants::Enums::GAME_STATUS::CHAIR18:
				case Constants::Enums::GAME_STATUS::CHAIR19:
				case Constants::Enums::GAME_STATUS::CHAIR20:
					if (this->field_102 != 0) {
						this->BlowBackControl(&v44);
					}
					break;
				default:
					if (this->IsOnChocobo() == true || this->IsOnMount() == true) {
						if (this->VirtActor201() == false && this->VirtActor204() == false) {
							this->UserControl(&v44);
						}
					}
					break;
				}
			}
			else if (this->field_103 != 0) {
				this->ApproachControl(&v44);
				this->OnKeyDown(&v44);
			}
			else if (this->field_104 != 0) {
				this->BackJumpControl(&v44);
				this->OnKeyDown(&v44);
			}
			else if (this->field_102 != 0) {
				this->BlowBackControl(&v44);
				this->OnKeyDown(&v44);
			}
			else {
				if (this->field_101 != 0) {
					this->ActorMoveControl(&v44);
				}
				this->OnKeyDown(&v44);
			}
		}
		else if (this->field_103 != 0) {
			this->ApproachControl(&v44);
		}
		else if (this->field_104 != 0) {
			this->BackJumpControl(&v44);
		}
		else if (
				this->field_102 == 0
				|| this->IsFishingRod() == true
				|| this->VirtActor209() == true
				|| this->VirtActor201() == true
				|| this->VirtActor204() == true
				|| this->VirtActor212() == true) 
		{
			if (this->field_101 != 0) {
				this->ActorMoveControl(&v44);
			}
		}
		else {
			this->BlowBackControl(&v44);
		}

		D3DXVECTOR3 v45{};
		v45.x = v44.x - this->field_D4.x;
		v45.y = v44.y - this->field_D4.y;
		v45.z = v44.z - this->field_D4.z;
		float magsq = v45.x * v45.x + v45.y * v45.y + v45.z * v45.z;
		float mag = sqrt(magsq);
		this->field_F8 = mag > 0.02f;
		Constants::Enums::GAME_STATUS status = this->GetGameStatus();
		if (this->field_F8 != 0	
			|| status != Constants::Enums::GAME_STATUS::N_IDLE
			&& status != Constants::Enums::GAME_STATUS::CAMP
			&& status != Constants::Enums::GAME_STATUS::SIT
			&& this->IsOnChair() == false) {
			this->field_5AE = 1800;
		}

		if (this->field_5AE >= 0) {
			this->field_5AE -= (int)v47;
		}
		if (this->field_5AE < 0) {
			this->field_5AE = 0;
		}

		this->SetPos(&v44);
		this->field_44 = this->field_E4;
		Globals::RotClamp(&this->field_44);

		int v38 = this->VirtActor130() == 0;
		int v39 = this->VirtActor88();

		D3DLIGHT8 v64{};
		XiZone::zone->GetAreaLightByFourCC(&v64, v38, v39);
		this->VirtActor127(&v64);
	}

	this->field_5B4.x = this->field_D4.x - this->field_5B4.x;
	this->field_5B4.y = this->field_D4.y - this->field_5B4.y;
	this->field_5B4.z = this->field_D4.z - this->field_5B4.z;

	return retval;
}

FFXI::CYy::CXiControlActor::CXiControlActor()
{
	this->field_F4 = 0;
	this->field_D4.w = 1.0;
	this->field_F8 = 0;
	this->field_F9 = 1;
	this->field_FB = 0;
	this->field_103 = 0;
	this->field_104 = 0;
	this->field_102 = 0;
	this->field_FC = 0;
	this->field_100 = 0;
	this->field_594 = 0;
	this->field_598 = 0;
	this->field_174 = 0;
	this->field_178 = 0;
	this->field_17C = 0;
	this->field_160 = { 0.0, -1.0, 0.0, 1.0 };

	if (CTk::TkManager::g_pCTkInputCtrl) {
		//sub //TODO
	}

	this->field_5A0.SetActor(nullptr);

	this->field_5AC = 0;
	this->field_5AE = 0;
	this->field_5B0 = 0;
	this->field_108 = 0;
	this->field_10C = 0;
	this->field_110 = 0;
	this->field_114 = 0;
	this->field_101 = 0;
	this->field_118 = 0;
	this->field_11C = 0;
	this->field_5B1 = 0;

	this->field_120 = { 0.0, 0.0, 0.0, 1.0 };
	this->field_130 = { 0.0, 0.0, 0.0, 1.0 };
	this->field_140 = { 0.125, 0.0, 0.0, 0.0 };
	this->field_150 = { 0.0, 0.0, 0.0, 1.0 };

	for (int i = 0; i < sizeof(this->field_180) / sizeof(this->field_180[0]); ++i) {
		memset(this->field_180 + i, 0, sizeof(this->field_180[0]));
		memset(this->field_388 + i, 0, sizeof(this->field_388[0]));
	}

	if (CXiActor::control_actor.GetSearchActor() == this) {
		//this struct doesn't seem to be used
	}

}

void FFXI::CYy::CXiControlActor::SetPos(D3DXVECTOR4* a2)
{
	this->field_D4 = *a2;
}

void FFXI::CYy::CXiControlActor::ApproachControl(D3DXVECTOR4* a2)
{
	exit(0x100A8640);
	float v20 = CYyDb::g_pCYyDb->CheckTick();
	this->field_108 -= v20;

	if (this->field_108 > 0.0) {
		D3DXVECTOR3 v22{};
		this->GetApproachPoint(&v22);
		D3DXVECTOR3 v21{};
		v21.x = v22.x - a2->x;
		v21.y = v22.y - a2->y;
		v21.z = v22.z - a2->z;
		float magsq = Globals::Vec3Inner(&v21, &v21);
		float mag = sqrt(magsq);
		float attackReach = ((CXiSkeletonActor*)this)->GetAttackReach();
		float diff = mag - attackReach;
		if (diff >= 0.0099999998) {

		}
		else {
			this->field_108 = 0.0;
			this->field_103 = 0;
		}
	}
	else {

	}
}

void FFXI::CYy::CXiControlActor::BackJumpControl(D3DXVECTOR4*)
{
	exit(0x001C4AE0);
}

void FFXI::CYy::CXiControlActor::ActorMoveControl(D3DXVECTOR4*)
{
	exit(0x001C5BC0);
}

void FFXI::CYy::CXiControlActor::BlowBackControl(D3DXVECTOR4*)
{
	exit(0x001C57A0);
}

void FFXI::CYy::CXiControlActor::OnKeyDown(D3DXVECTOR4* a2)
{
	if ((this->field_88 & 1) != 0) {
		return;
	}

	if (this->IsControlLock() != 0) {
		return;
	}

	this->StepControl(a2);
}

void FFXI::CYy::CXiControlActor::StepControl(D3DXVECTOR4* a2)
{
	if (this->AtelBuffer != nullptr) {
		FFXI::CYy::XiAtelBuff* buff = this->AtelBuffer->GetSomeAtelBuff();
		if (buff != nullptr) {
			D3DXVECTOR4* pos = this->GetPos();
			D3DXVECTOR4 diff = buff->field_4 - *pos;

			float csize = this->GetCollisionSize();
			csize = sqrt(diff.x * diff.x + diff.z * diff.z) - csize;

			D3DXVECTOR4 v147 = diff * 0.25f;
			if (csize < 0.0f) {
				v147 = { 0.0f, 0.0f, 0.0f, 1.0f };
			}

			a2->x += v147.x;
			a2->y += v147.y;
			a2->z += v147.z;

			if (CXiControlActor::is_first_person_view == false) {
				if (v147.x != 0.0f || v147.z != 0.0f) {
					this->field_E4.y = -atan2(v147.z, v147.x);
				}
				return;
			}
			//first person stuff
			exit(0x234523);
			return;
		}
	}

	float v30 = CYyDb::g_pCYyDb->field_2C;
	float v150 = v30 * 0.01745278f;
	D3DXVECTOR4 v145 = { 0.0f, 0.0f, 0.0f, 0.0f };

	float fv94 = this->VirtActor94();
	FFXI::Constants::Enums::GAME_STATUS status = this->GetGameStatus();
	if (this->IsOnChocobo() == true || this->IsOnMount() == true) {
		fv94 += fv94;
	}

	if (fv94 > 30.0f) {
		fv94 = 30.0f;
	}

	float v146 = fv94 / 60.0f;
	if (CXiControlActor::is_auto_running == true) {
		exit(0x321422);
	}

	D3DXVECTOR4 v148 = this->field_E4;
	if (CYyDb::g_pCYyDb->field_8 != 0 && this->AtelBuffer != nullptr && XiAtelBuff::CanIMove() == false) {
		this->field_598 = 0;
		this->field_594 = 0.0f;
		return;
	}

	float AnalogKey1 = 0.0f;
	float AnalogKey2 = 0.0f;
	if (CYy::CYyCamMng2::SomeKeyCheck() == false || FFXI::CTk::TkManager::g_pCTkInputCtrl->field_0.field_22 == 0) {
		AnalogKey1 = FFXI::Input::InputMng::GetAnalogKey(63, (Constants::Enums::KEYTYPE)4);
		AnalogKey2 = -FFXI::Input::InputMng::GetAnalogKey(63, (Constants::Enums::KEYTYPE)5);
	}

	CTk::TkManager::g_pCTkMouse->field_A8 = 0.0f;
	CTk::TkManager::g_pCTkMouse->field_AC = 0;
	if (CTk::CFsConf6Win::Check() == true) {
		if ((CTk::TkManager::g_pCTkMouse->field_58 & 4) != 0) {
			int v3{}, v4{};
			long x{}, y{};
			if (CTk::TkManager::g_pCTkMouse != nullptr) {
				v3 = CTk::TkManager::g_pCTkMouse->field_38;
				v4 = CTk::TkManager::g_pCTkMouse->field_3C;
				x = CTk::TkManager::g_pCTkMouse->field_4.x;
				y = CTk::TkManager::g_pCTkMouse->field_4.y;
			}

			float v7 = (float)(x - v3) / ((float)(CTk::TkManager::g_CTkMenuMng.UIXRes - CTk::TkManager::g_CTkMenuMng.field_7C) / 21.0f);
			float v8 = -(float)(y - v4) / ((float)(CTk::TkManager::g_CTkMenuMng.UIYRes - CTk::TkManager::g_CTkMenuMng.field_7E) / 21.0f);

			if (v7 >= 1.0f || v7 < -1.0f || v8 >= 1.0f || v8 < -1.0f) {
				float angle = (float)((__int64)(((atan2(v8, v7) + 3.1415927f) * 0.15915494f + 0.03125f) * 16.0f) & 0xF) * 0.0625f * 6.2831855f - 3.1415927f;
				AnalogKey1 = cos(angle);
				AnalogKey2 = sin(angle);
				CTk::TkManager::g_pCTkMouse->field_A8 = angle;
				CTk::TkManager::g_pCTkMouse->field_AC = 1;
			}
		}
	}

	this->AdjustAnalogKeyLength(&AnalogKey1, &AnalogKey2);
	if (CXiControlActor::is_first_person_view == false) {
		if (CYyDb::g_pCYyDb->field_8 == 0 && (AnalogKey1 != 0.0f || AnalogKey2 != 0.0f)) {
			CYyDb::g_pCYyDb->CameraManager->Follow(this);
		}
		if (CTk::CFsConf6Win::Check() == true) {
			exit(0x100A4E29);
		}

		//label 82
		v145 = { -(AnalogKey2 * v146), 0.0f, AnalogKey1 * v146, 1.0f };
		FFXI::CYy::CXiActor* follow = CXiControlActor::follow_actor.GetSearchActor();
		if (follow != nullptr) {
			exit(0x100A51F5);
		}

		float v78 = sqrt(auto_run_vec.x * auto_run_vec.x + auto_run_vec.y * auto_run_vec.y + auto_run_vec.z * auto_run_vec.z);
		if (v145.x == 0.0 && v145.z == 0.0 && v78 <= 0.001f) {
			if (CXiControlActor::is_auto_running == true) {
				CXiControlActor::IsAutoRunning(false);
			}
			this->field_598 = 0;
		}
		else {
			if (CTk::CFsConf6Win::Check() == true) {
				exit(0x100A5369);
			}

			Constants::Enums::GAME_STATUS status = this->GetGameStatus();
			if (this->IsOnChocobo() == true || this->IsOnMount() == true
				|| status == Constants::Enums::GAME_STATUS::N_IDLE
				|| status == Constants::Enums::GAME_STATUS::B_IDLE
				|| status == Constants::Enums::GAME_STATUS::EVT
				|| status == Constants::Enums::GAME_STATUS::ES2
				|| status == Constants::Enums::GAME_STATUS::ES5) {
				if (this->IsFreeRun() != 0) {
					Math::WMatrix v22{};
					v22.Identity();
					float angle = atan2(-CYyDb::g_pCYyDb->CameraManager->field_4._33, CYyDb::g_pCYyDb->CameraManager->field_4._31);
					v22.RotateY(angle);
					v22.Vec3TransformDrop4Self((D3DXVECTOR3*)&v145);
					if (CXiControlActor::is_auto_running == true) {
						if (CXiControlActor::auto_run_vec.x != 0.0f || CXiControlActor::auto_run_vec.z != 0.0f) {
							float dot = Globals::Vec3Inner(&v145, &CXiControlActor::auto_run_vec);
							if (dot < 0.0f) {
								D3DXVECTOR3 v21{ v145.x, v145.x, v145.z };
								Globals::Vec3Normalize(&v21);

								D3DXVECTOR3 v20{ CXiControlActor::auto_run_vec.x,  CXiControlActor::auto_run_vec.y,  CXiControlActor::auto_run_vec.z };
								Globals::Vec3Normalize(&v20);

								D3DXVECTOR3 v19{};
								Globals::Vec3Outer(&v19, &v21, &v20);

								if (v19.y > -0.4f && v19.y < 0.4f) {
									CXiControlActor::IsAutoRunning(false);
								}
							}

							v145.x = CXiControlActor::auto_run_vec.x;
							v145.y = CXiControlActor::auto_run_vec.y;
							v145.z = CXiControlActor::auto_run_vec.z;

							CXiActor* follow_actor = CXiControlActor::follow_actor.GetSearchActor();
							if (follow_actor != nullptr) {
								exit(0x100A621C);
							}
						}
					}

					if (CXiControlActor::is_auto_running == true) {
						if (CXiControlActor::auto_run_vec.x != 0.0f || CXiControlActor::auto_run_vec.z != 0.0f) {
							if (AnalogKey1 != 0.0f) {
								Math::WMatrix v154{};
								v154.Identity();
								float angle = (AnalogKey1 + AnalogKey2) * v150;
								v154.RotateY(angle);
								v154.Vec3TransformDrop4Self((D3DXVECTOR3*)&auto_run_vec);
							}
						}
						else {
							auto_run_vec.x = v145.x;
							auto_run_vec.y = v145.y;
							auto_run_vec.z = v145.z;
						}
					}
				}
			}

			//label 116
			if (this->VirtActor92() != 0) {
				v150 = sqrt(v145.x * v145.x + v145.y * v145.y + v145.z * v145.z);
				Globals::Vec3Normalize((D3DXVECTOR3*)&v145);
				D3DXVECTOR4* v84 = this->GetGroundNormal();
				D3DXVECTOR3 a1{};
				Globals::Vec3Outer(&a1, (D3DXVECTOR3*)&v145, (D3DXVECTOR3*)v84);
				Globals::Vec3Normalize(&a1);
				Globals::Vec3Outer((D3DXVECTOR3*)&v145, (D3DXVECTOR3*)v84, &a1);
				Globals::Vec3Normalize((D3DXVECTOR3*)&v145);
				if (v145.y > 0.0f) {
					v145.y = 0.0f;
				}
				v145.x *= v150;
				v145.y *= v150;
				v145.z *= v150;
			}
			else if (v145.y < 0.0f) {
				v145.x *= 0.25f;
				v145.y *= 0.25f;
				v145.z *= 0.25f;
			}
		}

		//label 123
		if (this->IsParallelMove() != 0) {
			Math::WMatrix v154{};
			v154.Identity();
			v154.RotateY(this->field_E4.y);
			D3DXVECTOR3 a1{ 1.0f, 0.0f, 0.0f };
			v154.Vec3TransformDrop4Self(&a1);
			D3DXVECTOR4* v90 = this->GetPos();
			a1.x += v90->x;
			a1.y += v90->y;
			a1.z += v90->z;
			this->field_598 = this->GetMoveVecDirId((D3DXVECTOR3*)&v145, &a1);
			this->ChangeVectorLengthByDirection((D3DXVECTOR3*)&v145);
		}
		v145.x *= v30;
		v145.y *= v30;
		v145.z *= v30;
		if (this->CheckContactActor((D3DXVECTOR3*)&v145) == false) {
			a2->x += v145.x;
			a2->y += v145.y;
			a2->z += v145.z;
		}

		if (this->IsFreeRun() != 0 && this->IsParallelMove() == 0) {
			if (v145.x == 0.0f && v145.z == 0.0f) {
				if (AnalogKey1 != 0.0f || AnalogKey2 != 0.0f) {
					D3DXVECTOR3 a1{ -AnalogKey2, 0.0f, AnalogKey1 };
					float angle = atan2(-CYyDb::g_pCYyDb->CameraManager->field_4._33, CYyDb::g_pCYyDb->CameraManager->field_4._31);

					Math::WMatrix v154{};
					v154.Identity();
					v154.RotateY(angle);
					v154.Vec3TransformDrop4Self(&a1);

					angle = atan2(a1.z, a1.x);
					this->field_E4.x = v148.x;
					this->field_E4.y = -angle;
					this->field_E4.z = v148.z;
					this->field_E4.w = v148.w;
				}
			}
			else {
				float angle = atan2(v145.z, v145.x);
				this->field_E4.x = v148.x;
				this->field_E4.y = -angle;
				this->field_E4.z = v148.z;
				this->field_E4.w = v145.w;
			}
		}
	}
	else {
		exit(0x100A57F3);
	}
}

void FFXI::CYy::CXiControlActor::UserControl(D3DXVECTOR4* a2)
{
	if (this->AtelBuffer != nullptr) {
		//sub //TODO
	}
	CXiActor* a = CXiActor::control_actor.GetSearchActor();
	if (a != nullptr) {
		if (a->AtelBuffer == nullptr || 
			(a->AtelBuffer->field_120 & 0x80) == 0 && (a->AtelBuffer->field_12C & 0x2000) == 0) {
			((CXiControlActor*)a)->OnKeyDown(a2);
		}
	}
}

void FFXI::CYy::CXiControlActor::GetApproachPoint(D3DXVECTOR3*)
{
	exit(0x100A8B60);
}

void FFXI::CYy::CXiControlActor::SetIndoorFlag(char a2)
{
	this->field_17C = a2;
}

void FFXI::CYy::CXiControlActor::ModeControl()
{
	if (CYyDb::g_pCYyDb->field_8 != 0) {
		if (this->AtelBuffer != nullptr) {
			if ((this->AtelBuffer->field_120 & 0x80) != 0 || (this->AtelBuffer->field_12C & 0x2000) != 0) {
				return;
			}
		}
	}

	if (FFXI::Input::InputMng::GetKey(63, (FFXI::Constants::Enums::KEYTYPE)28, FFXI::Constants::Enums::TRGTYPE_1, -1)) {
		CXiControlActor::IsAutoRunning(CXiControlActor::is_auto_running == 0);
	}

	switch (this->GetGameStatus()) {
	case Constants::Enums::GAME_STATUS::N_DEAD:
	case Constants::Enums::GAME_STATUS::B_DEAD:
	case Constants::Enums::GAME_STATUS::FISHING:
	case Constants::Enums::GAME_STATUS::POL:
	case Constants::Enums::GAME_STATUS::CAMP:
	case Constants::Enums::GAME_STATUS::FISHING1:
	case Constants::Enums::GAME_STATUS::FISHING2:
	case Constants::Enums::GAME_STATUS::FISHING3:
	case Constants::Enums::GAME_STATUS::FISHING4:
	case Constants::Enums::GAME_STATUS::FISHING5:
	case Constants::Enums::GAME_STATUS::FISHING6:
	case Constants::Enums::GAME_STATUS::ITEMMAKE:
	case Constants::Enums::GAME_STATUS::SIT:
	case Constants::Enums::GAME_STATUS::ICRYSTALL:
	case Constants::Enums::GAME_STATUS::FISH_2:
	case Constants::Enums::GAME_STATUS::FISHF:
	case Constants::Enums::GAME_STATUS::FISHR:
	case Constants::Enums::GAME_STATUS::FISHL:
	case Constants::Enums::GAME_STATUS::FISH_3:
	case Constants::Enums::GAME_STATUS::FISH_31:
	case Constants::Enums::GAME_STATUS::FISH_32:
	case Constants::Enums::GAME_STATUS::FISH_33:
	case Constants::Enums::GAME_STATUS::FISH_34:
	case Constants::Enums::GAME_STATUS::FISH_35:
	case Constants::Enums::GAME_STATUS::FISH_36:
	case Constants::Enums::GAME_STATUS::CHAIR00:
	case Constants::Enums::GAME_STATUS::CHAIR01:
	case Constants::Enums::GAME_STATUS::CHAIR02:
	case Constants::Enums::GAME_STATUS::CHAIR03:
	case Constants::Enums::GAME_STATUS::CHAIR04:
	case Constants::Enums::GAME_STATUS::CHAIR05:
	case Constants::Enums::GAME_STATUS::CHAIR06:
	case Constants::Enums::GAME_STATUS::CHAIR07:
	case Constants::Enums::GAME_STATUS::CHAIR08:
	case Constants::Enums::GAME_STATUS::CHAIR09:
	case Constants::Enums::GAME_STATUS::CHAIR10:
	case Constants::Enums::GAME_STATUS::CHAIR11:
	case Constants::Enums::GAME_STATUS::CHAIR12:
	case Constants::Enums::GAME_STATUS::CHAIR13:
	case Constants::Enums::GAME_STATUS::CHAIR14:
	case Constants::Enums::GAME_STATUS::CHAIR15:
	case Constants::Enums::GAME_STATUS::CHAIR16:
	case Constants::Enums::GAME_STATUS::CHAIR17:
	case Constants::Enums::GAME_STATUS::CHAIR18:
	case Constants::Enums::GAME_STATUS::CHAIR19:
	case Constants::Enums::GAME_STATUS::CHAIR20:
		CXiControlActor::IsAutoRunning(false);
		break;
	default:
		break;
	}

	if (FFXI::Input::InputMng::GetKey(63, (FFXI::Constants::Enums::KEYTYPE)16, FFXI::Constants::Enums::TRGTYPE_1, -1)) {
		CXiControlActor::IsFirstPersonView(CXiControlActor::is_first_person_view == 0);
		if (CXiControlActor::is_first_person_view == true) {
			CYyDb::g_pCYyDb->SetProjection(280.0);
		}
		else {
			float projection = (float)FFXI::Config::FsConfig::GetConfig(FFXI::Constants::Enums::FsConfigSubjects::Subject73);
			CYyDb::g_pCYyDb->SetProjection(projection);
			this->IsParallelMove(0);
		}

		FFXI::SoundMng::CYySePlayOpenMainWindow();
	}

	FFXI::Constants::Enums::GAME_STATUS status = this->GetGameStatus();
	if (status == FFXI::Constants::Enums::GAME_STATUS::N_DEAD || status == FFXI::Constants::Enums::GAME_STATUS::B_DEAD) {
		CXiControlActor::IsFirstPersonView(false);
	}
}

float FFXI::CYy::CXiControlActor::GetCollisionSize()
{
	D3DXVECTOR4 v2{};
	this->GetElemLocal(44, &v2);
	return sqrt(v2.z * v2.z + v2.x * v2.x) * 0.8f;
}

void FFXI::CYy::CXiControlActor::AdjustAnalogKeyLength(float* a2, float* a3)
{
	float v6 = sqrt(*a3 * *a3 + *a2 * *a2);
	float v7{};
	if (v6 >= 0.000001f) {
		v7 = 1.0f / v6;
	}

	*a2 *= v7;
	*a3 *= v7;

	float v20{};
	if (v6 <= 0.05f) {
		//stand still?
		v20 = 0.0f;
	}
	else if (v6 <= 0.9f) {
		//walk?
		v20 = 0.33333334f;
	}
	else {
		//run?
		v20 = 1.0f;
	}

	if (this->IsWalkLock() != 0) {
		if (v20 > 0.33333334f) {
			v20 = 0.33333334f;
		}
	}

	*a2 *= v20;
	*a3 *= v20;
	this->field_594 = v20;
}

int FFXI::CYy::CXiControlActor::GetMoveVecDirId(D3DXVECTOR3* a2, D3DXVECTOR3* a3)
{
	if (sqrt(Globals::Vec3Inner(a2, a2)) < 0.001f) {
		return 0;
	}

	D3DXVECTOR3 v20{ a3->x - this->field_D4.x, a3->y - this->field_D4.y, a3->z - this->field_D4.z };
	Globals::Vec3Normalize(&v20);

	Math::WMatrix v23{};
	v23.Identity();
	v23.RotateY(Constants::Values::ANGLE_PI_OVER_4);

	D3DXVECTOR3 v21{};
	v23.Vec3TransformDrop4(&v21, &v20);

	v23.Identity();
	v23.RotateY(Constants::Values::ANGLE_MINUS_PI_OVER_4);

	D3DXVECTOR3 v22{};
	v23.Vec3TransformDrop4(&v22, &v20);

	float v19 = Globals::Vec3Inner(&v21, a2);
	float v18 = Globals::Vec3Inner(&v22, a2);
	if (v19 >= 0.0f) {
		if (v18 >= 0.0f) {
			return 1;
		}
		else {
			return 2;
		}
	}
	else if (v18 >= 0.0f) {
		return 4;
	}
	else {
		return 3;
	}
}

void FFXI::CYy::CXiControlActor::ChangeVectorLengthByDirection(D3DXVECTOR3* a2)
{
	Globals::Vec3Normalize(a2);

	switch (this->field_598) {
	case 0:
		[[fallthrough]];
	case 1:
	{
		float v10 = sqrt(Globals::Vec3Inner(a2, a2));
		a2->x *= v10;
		a2->y *= v10;
		a2->z *= v10;
	}
		break;
	case 2:
	[[fallthrough]];
	case 4:
		if (this->IsOnChocobo() == true || this->IsOnMount() == true) {
			a2->x /= 8.0f;
			a2->y /= 8.0f;
			a2->z /= 8.0f;
		}
		else {
			a2->x /= 16.0f;
			a2->y /= 16.0f;
			a2->z /= 16.0f;
		}
		break;
	case 3:
	{
		float v10 = this->GetWalkSpeed();
		if (this->IsOnChocobo() == true || this->IsOnMount() == true) {
			a2->x *= v10 / 30.0f;
			a2->y *= v10 / 30.0f;
			a2->z *= v10 / 30.0f;
		}
		else {
			a2->x *= v10 / 60.0f;
			a2->y *= v10 / 60.0f;
			a2->z *= v10 / 60.0f;
		}
	}
		break;
	default:
		break;
	}
}

bool FFXI::CYy::CXiControlActor::CheckContactActor(D3DXVECTOR3* a2)
{
	float v15 = CYyDb::CheckTick();
	if (this->GetGmLevel() >= 3u) {
		return false;
	}

	D3DXVECTOR3 v16{ this->field_D4.x, this->field_D4.y, this->field_D4.z };
	v16.x += a2->x;
	v16.y += a2->y;
	v16.z += a2->z;

	float closestDistSq = 64.0f;
	CXiActor* actor = CXiActor::GetHead();
	CXiControlActor* closestActor{ nullptr };
	while (actor != nullptr) {
		if (actor != this && actor->InOwnActorPointers() == false) {
			if (actor->VirtActor64() == 0) {
				Constants::Enums::ActorType type = actor->GetType();
				if (type != Constants::Enums::ActorType::DOOR && type != Constants::Enums::ActorType::LIFT && type != Constants::Enums::ActorType::MODEL) {
					if (actor->field_B2 == 0) {
						if (actor->VirtActor201() == false) {
							if (actor->VirtActor204() == false) {
								Constants::Enums::GAME_STATUS status = actor->GetGameStatus();
								if (status != Constants::Enums::GAME_STATUS::N_DEAD && status != Constants::Enums::GAME_STATUS::B_DEAD) {
									int v6 = (int)actor->GetEquipNum(0);
									if (v6 < 50 || v6 > 59) {
										CXiControlActor* cact = (CXiControlActor*)actor;
										if (cact->field_59C >= 1.0f) {
											if (cact->AtelBuffer == nullptr
												|| (cact->AtelBuffer->ServerID & 0xFF000000) != 0
												|| cact->field_5AE > 0) {
												D3DXVECTOR3 v17{ cact->field_D4.x - v16.x, cact->field_D4.y - v16.y, cact->field_D4.z - v16.z };
												float v9 = Globals::Vec3Inner(&v17, &v17);
												if (closestDistSq > v9) {
													closestDistSq = v9;
													closestActor = cact;
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		actor = actor->GetNext();
	}

	if (closestActor == nullptr) {
		return false;
	}

	float a2b = sqrt(closestDistSq);
	float a2c = a2b - this->GetCollisionSize();
	if (a2c - closestActor->GetCollisionSize() >= 0.0f) {
		this->field_5A0.SetActor(nullptr);
		this->field_5AC = 0;
		this->field_5B0 = 0;
		return false;
	}

	if (this->field_5A0.GetSearchActor() != closestActor) {
		this->field_5A0.SetActor(closestActor);
		this->field_5AC = 30;
		this->field_5B0 = 1;
		return true;
	}

	if (this->field_5B0 != 0) {
		this->field_5AC -= (int)v15;
		if (this->field_5AC >= 0) {
			return true;
		}

		this->field_5B0 = 0;
	}

	return false;
}

D3DXVECTOR4* FFXI::CYy::CXiControlActor::GetPos()
{
	return &this->field_D4;
}

D3DXVECTOR4* FFXI::CYy::CXiControlActor::VirtActor102()
{
	return &this->field_E4;
}

void FFXI::CYy::CXiControlActor::SetGroundNormal(D3DXVECTOR4* a2)
{
	if (a2->y > 0.7f || a2->y < -0.7f) {
		this->field_160 = *a2;
	}
}

D3DXVECTOR4* FFXI::CYy::CXiControlActor::GetGroundNormal()
{
	return &this->field_160;
}

void FFXI::CYy::CXiControlActor::VirtActor123(int a2)
{
	this->field_174 = a2;
}

int FFXI::CYy::CXiControlActor::VirtActor124()
{
	return this->field_174;
}

void FFXI::CYy::CXiControlActor::VirtActor125(float a2)
{
	this->field_178 = a2;
}

void FFXI::CYy::CXiControlActor::VirtActor127(D3DLIGHT8* a2)
{
	Globals::CopyLight(&this->field_388[0], a2);
}

void FFXI::CYy::CXiControlActor::VirtActor128(D3DLIGHT8** a2)
{
	for (int i = 1; i < sizeof(this->field_388) / sizeof(this->field_388[0]); ++i) {
		D3DLIGHT8* light = nullptr;
		if (a2 != nullptr) {
			light = a2[i - 1];
		}
		Globals::CopyLight(this->field_388 + i, light);
	}
}

void FFXI::CYy::CXiControlActor::VirtActor144(CXiActor* a2)
{
	exit(0x100A85C0);
}

bool FFXI::CYy::CXiControlActor::AmIControlActor()
{
	return CXiActor::control_actor.GetSearchActor() == this;
}

void FFXI::CYy::CXiControlActor::IsControlLock(bool a2)
{
	if (a2 == true) {
		this->field_FC += 1;
	}
	else if (this->field_FC > 0) {
		this->field_FC -= 1;
	}
}

int FFXI::CYy::CXiControlActor::IsControlLock()
{
	return this->field_FC;
}

char FFXI::CYy::CXiControlActor::IsFreeRun()
{
	return this->field_F9;
}

void FFXI::CYy::CXiControlActor::IsFreeRun(char a2)
{
	this->field_F9 = a2;
}

char FFXI::CYy::CXiControlActor::IsWalkLock()
{
	return this->field_FA;
}

void FFXI::CYy::CXiControlActor::IsWalkLock(char a2)
{
	this->field_FA = a2;
}

char FFXI::CYy::CXiControlActor::IsParallelMove()
{
	return this->field_FB;
}

void FFXI::CYy::CXiControlActor::IsParallelMove(char a2)
{
	this->field_FB = a2;
}
