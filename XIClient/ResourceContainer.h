#pragma once
#include "CMoResource.h"

namespace FFXI {
	namespace File { class ReferenceReadBase; }
	namespace CYy {
		class CXiActor;

		/**
	    * @class ResourceContainer
	    * @brief Represents a container file that holds multiple game resources.
	    *
	    * A ResourceContainer is a special type of resource (typically of type Rmp)
	    * that serves as a parent for other resources. It organizes resources in a
	    * hierarchical structure and is typically the root or intermediate node in
	    * the resource tree. Resource containers are loaded from data files and serve
	    * as the primary way to access and manage groups of related game resources.
	    */
		class ResourceContainer : public CMoResource {
		public:
			/**
		   * @brief Asynchronously loads a resource file with callback functionality
		   *
		   * @param outResourceContainer Pointer that will receive the loaded resource container
		   * @param callbackFunction Function to call when the resource is loaded
		   * @param referenceData Additional data to pass to the callback
		   * @param sourceActor Source actor (caster) associated with this operation
		   * @param targetActor Target actor associated with this operation
		   */
			static void ReferenceRead(
				ResourceContainer*** outResourceContainer,
				void(__cdecl* callbackFunction)(ResourceContainer**, File::ReferenceReadBase*),
				File::ReferenceReadBase* referenceData,
				FFXI::CYy::CXiActor* sourceActor,
				FFXI::CYy::CXiActor* targetActor
			);

			ResourceContainer();
			virtual ~ResourceContainer();
			
			/**
			* @brief Stops all generators and schedulers contained in this resource container
			*
			* This method performs two main operations:
			* 1. It finds and stops all generator resources, including handling cloned generators
			*    that reference the generators in this container
			* 2. It stops all scheduler tasks associated with scheduler resources in this container
			*/
			void StopMovers();
			
			int field_30;					//Haven't seen field_30 used yet
			int FileIndex;					//Numerical index of the file this container was found in
			int TraversalMarker;			//Traversal marker used during resource searches
			CMoResource** TerminateNode;	//Points to the end of this container
		};
	}
}