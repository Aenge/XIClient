#pragma once
#include "MemoryBlockDescriptor.h"

namespace FFXI {
	namespace CYy {
		class CYyUpperMemory : public MemoryBlockDescriptor {
		public :
			const static BaseGameObject::ClassInfo CYyUpperMemoryClass;
			virtual const BaseGameObject::ClassInfo* GetRuntimeClass() override final;
		};
	}
}