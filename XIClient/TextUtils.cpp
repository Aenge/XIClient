#include "TextUtils.h"
#include <string.h>

unsigned char FFXI::Util::Text::FsGetLetterByte(char a1, char a2)
{
	switch (a1) {
	case 0xFDi8:
		return 6;
	case 0x7Fi8:
		if (a2 >= 0x34i8) {
			if (a2 <= 0x36i8) {
				return 3;
			}
			else if (a2 == 0x38i8) {
				return 4;
			}
		}
		return 2;
	case 0x1Ei8:
	case 0x1Fi8:
	case 0xEFi8:
		return 2;
	case 0:
		return 0;
	default:
		if (a1 >= 0x20i8 || a1 == 0xAi8 || a1 == 0xDi8) {
			return FFXI::Util::Text::IsTwoByteCode(a1) + 1;
		}
	}
	return 1;
}

unsigned int FFXI::Util::Text::FsTrimStr(char* a1)
{
	unsigned int length = strlen(a1);
	unsigned int out_pos = 0;
	unsigned int in_pos = 0;

	while (in_pos < length) {
		int letterByte = FsGetLetterByte(a1[in_pos], 0);
		int v5 = Util::Text::GetLongCodeLen(a1[in_pos], a1[in_pos + 1]);
		if (v5 == 3 || v5 == 2) {
			break;
		}
		for (int j = 0; j < letterByte; ++j) {
			a1[out_pos] = a1[in_pos + j];
			out_pos += 1;
		}

		in_pos += letterByte;
	}

	a1[out_pos] = 0;
	return out_pos;
}

unsigned int FFXI::Util::Text::SomeCopy(char* output, unsigned int out_len, char* input, unsigned int in_len)
{
	unsigned int out_pos = 0;
	unsigned int in_pos = 0;
	while (in_pos < in_len) {
		if (input[in_pos] == 21) {
			in_pos += 1;
		}
		else {
			int codelen = GetCodeLen(input[in_pos]);
			if (out_len != 0 && codelen + out_pos >= out_len) {
				break;
			}

			memcpy(output + out_pos, input + in_pos, codelen);
			out_pos += codelen;
			in_pos += codelen;
		}
	}

	output[out_pos] = 0;
	return out_pos;
}

bool FFXI::Util::Text::IsOneByteCode(const char a1)
{
	return IsTwoByteCode(a1) == false;
}

bool FFXI::Util::Text::IsTwoByteCode(const char a1)
{
	if (a1 >= 0x81i8 && a1 <= 0x9Fi8) {
		return true;
	}

	if (a1 >= 0xE0i8 && a1 <= 0xEFi8) {
		return true;
	}

	return a1 >= 0xFAi8 && a1 <= 0xFCi8;
}

unsigned int FFXI::Util::Text::GetCodeLen(const char a1)
{
	return (unsigned int)IsTwoByteCode(a1) + 1u;
}

unsigned int FFXI::Util::Text::GetLongCodeLen(const char a1, const char a2)
{
	if (a1 == 0xFDi8) {
		return 4;
	}

	if (a1 == 0x7Fi8) {
		if (a2 >= 0xFBi8 && a2 <= 0xFCi8) {
			return 0;
		}
	}
	else if (a1 != 0x1Ei8 && a1 != 0x1Fi8) {
		if (a1 == 0xEFi8) {
			return 3;
		}
		if (a1 <= 0i8 || a1 >= 0x20i8 || a1 == '\n' || a1 == '\r') {
			return IsTwoByteCode(a1);
		}
	}

	return 2;
}

unsigned int FFXI::Util::Text::GetCode(const char* a1, short* a2)
{

	if (IsTwoByteCode(a1[0]) == true) {
		a2[0] = a1[1];
		a2[0] |= a1[0] << 8;
		return 2;
	}

	a2[0] = a1[0];
	return 1;
}

unsigned int FFXI::Util::Text::GetSomeLen(char* a1, unsigned int a2)
{
	unsigned int result = 0;
	unsigned int pos = 0;
	while (pos < a2) {
		if (a1[pos] == 21) {
			pos += 1;
		}
		else {
			int codelen = GetCodeLen(a1[pos]);
			pos += codelen;
			result += codelen;
		}
	}
	return result;
}

bool FFXI::Util::Text::CharIsNewline(const char a1)
{
	const char v3[3] = { '\a', '\n', '\r' };
	const unsigned int array_size = sizeof(v3) / sizeof(v3[0]);
	for (unsigned int i = 0; i < array_size; ++i) {
		if (a1 == v3[i]) {
			return true;
		}
	}
	return false;
}

bool FFXI::Util::Text::ShouldUnIndent(const char* a2)
{
	if (strlen(a2) < 2) {
		return false;
	}

	if (a2[0] != 0x81) {
		return false;
	}

	//unindentCodes: 0x4181, 0x4281, 0x4981
	if (a2[1] == 0x41 || a2[1] == 0x42 || a2[1] == 0x49) {
		return true;
	}

	return false;
}

bool FFXI::Util::Text::IsHyphenatedWord(const char* a1)
{
	if (a1[0] != '-') {
		return false;
	}

	return a1[1] < '0' || a1[1] > '9';
}


int FFXI::Util::Text::FindToken(const char* a1, unsigned int a2, const char a3[][4], const unsigned char* a4)
{
	int pos = 0;
	while (a3[pos][0] != 0) {
		if (a4[pos] <= a2 && strncmp(a1, a3[pos], a4[pos]) == 0) {
			return a4[pos];
		}
		pos += 1;
	}
	return 0;
}

int FFXI::Util::Text::GetNACKPos(const char* a1, unsigned int a2)
{
	unsigned int pos = 0;
	while (pos < a2) {
		if (a1[pos] == 21) {
			return pos;
		}

		unsigned int codelen = GetCodeLen(a1[pos]);
		pos += codelen;
	}

	return -1;
}