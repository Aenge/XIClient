#pragma once
#include "CYyGenerator.h"
namespace FFXI {
	namespace CYy {
		class CYyGeneratorClone : public CYyGenerator{
		public:
			static const BaseGameObject::ClassInfo CYyGeneratorCloneClass;
			virtual const BaseGameObject::ClassInfo* GetRuntimeClass() override final;
			virtual void VObj2(int*) override final;
			virtual const BaseGameObject::ClassInfo* AttachedTo() override final;
			virtual bool VirtAttach3() override final;
			virtual bool VirtAttach4() override final;
			virtual BaseGameObject* GetBaseGameObject() override final;
			CYyGeneratorClone() = default;
			virtual ~CYyGeneratorClone();
			void DeactivateClone();
			int field_F0;
			CMoResource** field_F4;
			int field_F8;
		};
	}
}