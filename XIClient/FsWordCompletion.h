#pragma once
#include "FsWordCategory.h"
namespace FFXI {
	namespace Input {
		class FsWordCompletion {
		public:
			~FsWordCompletion();
			FsWordCompletion();
			int field_0;
			int field_4;
			int field_8;
			int field_C;
			int field_10;
			int field_14;
			FsWordEntry field_18;
			char field_30[0x1C0];
			int field_1F0;
			int field_1F4;
			int field_1F8;
			int field_1FC;
			int field_200;
			int field_204;
			int field_208;
			int field_20C;
			int field_210;
			int field_214;
			int field_218;
			int field_21C;
			int field_220;
			int field_224;
			int field_228;
			FsWordCategory field_22C[64];
		};
	}
}