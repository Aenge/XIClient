#pragma once
#include "BaseGameObject.h"

namespace FFXI {
	namespace CYy {
		class CMoProcessor : public BaseGameObject {
		protected:
			CMoProcessor() = default;
		public:
			const static BaseGameObject::ClassInfo CMoProcessorClass;
			const BaseGameObject::ClassInfo* GetRuntimeClass() override;
			int field_4;
			int field_8;
			int field_C;
		};
	}
}