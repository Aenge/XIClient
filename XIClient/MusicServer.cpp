#include "MusicServer.h"
#include "CYyMusicLoadTask.h"
#include "CMoTaskMng.h"
#include "Globals.h"
#include "CYyDb.h"
#include "SoundMng.h"
#include "MemoryPoolManager.h"
#include <timeapi.h>

using namespace FFXI;

int MusicServer::last_request{ 0 };
CYy::CYyMusicLoadTask* MusicServer::MusicLoadTask{ nullptr };

void PlayFromTable(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int TableIndex, int TableSubIndex) {
	int v9{}, v10{}, v12{}, v14{};
	char v11{}, v13{};

	if (TableIndex == 0) {
		v10 = 4;
		v11 = 0;
	}
	else if (TableIndex == 1) {
		v10 = TableSubIndex;
		v11 = 1;
	}
	else if (TableIndex == 2) {
		v10 = timeGetTime() % 5;
		v11 = 1;
	}

	for (int i = v10; i >= 0; i--) {
		v13 = SoundMng::GetMusicTable2Index(v12);
		if (v13 >= 0) {
			v9 = v13;
			break;
		}
	}
	
	v14 = SoundMng::MusicTable2[v9].One;
	if (v14 == 0) {
		v14 = a1;
	}
		
	if (MusicServer::last_request == v14) {
		return;
	}

	MusicServer::last_request = v14;

	CYy::CMoTaskMng::DeleteThisTask = true;
	char* mem = MemoryPoolManager::globalInstance->Get(sizeof(CYy::CYyMusicLoadTask), MemoryPoolManager::MemoryPoolType::Ex);
	if (mem == nullptr) {
		return;
	}

	CYy::CYyMusicLoadTask* newTask = new (mem) CYy::CYyMusicLoadTask();
	
	if (MusicServer::MusicLoadTask != nullptr) {
		delete MusicServer::MusicLoadTask;
	}

	MusicServer::MusicLoadTask = newTask;

	newTask->field_35 = 0;
	newTask->field_34 = a7;
	newTask->field_44 = a4;
	newTask->field_3C = v14;
	newTask->field_40 = a3;
	newTask->field_48 = a5;
	newTask->field_50 = v9;
	newTask->field_54 = v11;
	newTask->field_38 = 0;

	if (a6 < 30) {
		newTask->field_4C = 30;
	}
	else {
		newTask->field_4C = a6;
	}

	CYyDb::g_pCYyDb->pCMoTaskMng->SomeShift4(newTask, 5);
}

void FFXI::MusicServer::clear_last_request()
{
	last_request = 0;
}

void FFXI::MusicServer::Play(int index)
{
	PlayFromTable(108, 0, 127, 127, 0, 0, 0, SoundMng::MusicTable[index].Index, SoundMng::MusicTable[index].SubIndex);
}

void FFXI::MusicServer::Play(int a1, int a2, int a3, int a4, int a5, bool a6)
{
	if (last_request == a1) {
		return;
	}

	last_request = a1;
	CYy::CMoTaskMng::DeleteThisTask = true;
	char* mem = MemoryPoolManager::globalInstance->Get(sizeof(CYy::CYyMusicLoadTask), MemoryPoolManager::MemoryPoolType::Ex);
	if (mem == nullptr) {
		return;
	}
		
	CYy::CYyMusicLoadTask* newTask = new (mem) CYy::CYyMusicLoadTask();
	
	if (MusicServer::MusicLoadTask != nullptr) {
		delete MusicServer::MusicLoadTask;
	}
	MusicServer::MusicLoadTask = newTask;

	newTask->field_35 = 0;
	newTask->field_34 = a6;
	newTask->field_44 = a3;
	newTask->field_3C = a1;
	newTask->field_40 = a2;
	newTask->field_48 = a4;
	newTask->field_50 = 0;
	newTask->field_54 = 0;
	newTask->field_38 = 0;

	if (a5 < 30) {
		newTask->field_4C = 30;
	}
	else {
		newTask->field_4C = a5;
	}
	
	CYyDb::g_pCYyDb->pCMoTaskMng->SomeShift4(newTask, 5);
}
