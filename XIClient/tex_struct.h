#pragma once
#define WIN32_LEAN_AND_MEAN
#include "d3d8to9/d3d8.hpp"
namespace FFXI {
	namespace CYy {
		class tex_struct {
		public:
			static IDirect3DTexture8* g_pSomeTexture1;
			static IDirect3DTexture8* g_pSomeTexture2;
			static int some_static;
			static void clean_texs();
			tex_struct();
			virtual ~tex_struct();
			IDirect3DBaseTexture8* field_4;
			IDirect3DBaseTexture8* field_8;
		};
	}
}