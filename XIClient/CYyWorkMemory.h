#pragma once
#include "MemoryBlockDescriptor.h"

namespace FFXI {
	namespace CYy {
		class CYyWorkMemory : public MemoryBlockDescriptor {
		public:
			const static BaseGameObject::ClassInfo CYyWorkMemoryClass;
			virtual const BaseGameObject::ClassInfo* GetRuntimeClass() final;
		};
	}
}