#pragma once

namespace FFXI {
	namespace Network {
		namespace Huffman { struct HuffmanTable; struct HuffmanTree; }
		class enAcv {
		private:
			static const int P_orig[18];
			static const int S_orig[1024];
			void blowfish_encrypt(unsigned int*, unsigned int*);
			void blowfish_decrypt(unsigned int*, unsigned int*);
		public:
			static void enAcvCreate(enAcv*, const char*);
			static int enAcvSet(char*, unsigned int, enAcv*, char*, unsigned int, Huffman::HuffmanTable*);
			static int enAcvGet(char*, unsigned int, enAcv*, Huffman::HuffmanTree*, char*, int);
			static void enAcvChange(enAcv*);
			static void newBlowfishKey(enAcv*, char*, int);
			static void blowfishEncryptString(enAcv*, char*, unsigned int);
			static void blowfishDecryptString(enAcv*, char*, unsigned int);
			unsigned int P[18];
			unsigned int S[4][256];
			char field_1048[80];
			int field_1098;
			int field_109C;
		};
	}
}