#define WIN32_LEAN_AND_MEAN
#include "CMoMmb.h"
#include "CYyDb.h"
#include "CTsZoneMap.h"
#include "d3dx9math.h"
#include "ZoneNames.h"
using namespace FFXI::CYy;

//Client specifically checks for a list of MMBs in the metalworks zone and adjusts their Y position offset
int MetalWorks_MMBsThatAreYAdjusted[] = {
	'nawa', 'carb', '1loc', 'u-hs', 'mius'
};

FFXI::CYy::CMoMmb::CMoMmb()
{
	this->field_30 = 0;
}

void FFXI::CYy::CMoMmb::Open()
{
	D3DXVECTOR4 posAdjust = { 0.0f, 0.0f, 0.0f, 0.0f };
	if (CYyDb::g_pCYyDb->g_pTsZoneMap->field_3C == FFXI::Constants::Enums::ZoneNames::Metalworks) {
		for (int i = 0; i < sizeof(MetalWorks_MMBsThatAreYAdjusted) / sizeof(MetalWorks_MMBsThatAreYAdjusted[0]); ++i) {
			if (this->ResourceID == MetalWorks_MMBsThatAreYAdjusted[i]) {
				posAdjust.y = -0.8f;
				break;
			}
		}
	}

	 *(void**)(&this->field_30) = (void*)CYyDb::g_pCYyDb->g_pTsZoneMap->OpenMmb((ResourceContainer**)this->ParentResourceReference, &this->field_30, this->ResourceID, (this->TypeSizeFlags >> 3) & 0x7FFFF0, &posAdjust);
}
