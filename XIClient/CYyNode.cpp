#include "CYyNode.h"
using namespace FFXI::CYy;

const BaseGameObject::ClassInfo FFXI::CYy::CYyNode::CYyNodeClass = {
	"CYyNode", sizeof(CYyNode), &BaseGameObject::BaseGameObjectClass
};

const BaseGameObject::ClassInfo* FFXI::CYy::CYyNode::GetRuntimeClass()
{
	return &CYyNode::CYyNodeClass;
}

FFXI::CYy::CYyNode::CYyNode()
{
	this->field_4 = nullptr;
	this->field_8 = nullptr;
}

FFXI::CYy::CYyNode::~CYyNode()
{
	//nullsub
}

void FFXI::CYy::CYyNode::Append(CYyNode** a2)
{
	if (*a2 == nullptr) {
		*a2 = this;
	}
	else {
		CYyNode* nextfree = this->GetTail();
		nextfree->field_4 = this;
		this->field_8 = nextfree;
	}
}

void FFXI::CYy::CYyNode::Delete(CYyNode** a2)
{
	if (this->field_8 != nullptr) {
		if (this->field_4 != nullptr) {
			this->field_8->field_4 = this->field_4;
			this->field_4->field_8 = this->field_8;
		}
		else {
			this->field_8->field_4 = nullptr;
		}
	} 
	else {
		if (this->field_4 != nullptr) {
			*a2 = this->field_4;
			this->field_4->field_8 = nullptr;
		}
		else {
			*a2 = nullptr;
		}
	}
}

CYyNode* FFXI::CYy::CYyNode::GetTail()
{
	if (this == nullptr)
		return nullptr;

	CYyNode* last = this;
	while (last->field_4 != nullptr) {
		last = last->field_4;
	}
	return last;
}
