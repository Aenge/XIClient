#include "CYyWorkMemory.h"

using namespace FFXI::CYy;

const BaseGameObject::ClassInfo CYyWorkMemory::CYyWorkMemoryClass{
	"CYyWorkMemory", sizeof(CYyWorkMemory), &BaseGameObject::BaseGameObjectClass
};

const BaseGameObject::ClassInfo* CYyWorkMemory::GetRuntimeClass() {
	return &CYyWorkMemoryClass;
}

