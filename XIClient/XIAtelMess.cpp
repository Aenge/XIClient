#include "XIAtelMess.h"
#include "XIFileManager.h"

char* FFXI::Text::XIAtelMess::StatusMess{ nullptr };
char* FFXI::Text::XIAtelMess::BtlMess{ nullptr };
char* FFXI::Text::XIAtelMess::EmotionMess{ nullptr };
char* FFXI::Text::XIAtelMess::SkillName{ nullptr };
char* FFXI::Text::XIAtelMess::BitName{ nullptr };
char* FFXI::Text::XIAtelMess::SystemMess{ nullptr };
char* FFXI::Text::XIAtelMess::WeatherHead{ nullptr };
char* FFXI::Text::XIAtelMess::WeatherHead2{ nullptr };
char* FFXI::Text::XIAtelMess::MonWazaMess{ nullptr };
char* FFXI::Text::XIAtelMess::UnityMess{ nullptr };
char* FFXI::Text::XIAtelMess::SevMess{ nullptr };

void FFXI::Text::XIAtelMess::ItemNameDataRead()
{
	ReadMess(  &StatusMess, FFXI::File::XIFileManager::GetLanguageDependentDatIndex(FFXI::Constants::Enums::LanguageDependentDats::Status),    true);
	ReadMess(     &BtlMess, FFXI::File::XIFileManager::GetLanguageDependentDatIndex(FFXI::Constants::Enums::LanguageDependentDats::Battle),    true);
	ReadMess( &EmotionMess, FFXI::File::XIFileManager::GetLanguageDependentDatIndex(FFXI::Constants::Enums::LanguageDependentDats::Emotion),   true);
	ReadMess(   &SkillName, FFXI::File::XIFileManager::GetLanguageDependentDatIndex(FFXI::Constants::Enums::LanguageDependentDats::SkillName), true);
	ReadMess(     &BitName, FFXI::File::XIFileManager::GetLanguageDependentDatIndex(FFXI::Constants::Enums::LanguageDependentDats::BitName),   true);
	ReadMess(  &SystemMess, FFXI::File::XIFileManager::GetLanguageDependentDatIndex(FFXI::Constants::Enums::LanguageDependentDats::System),    true);
	ReadMess( &MonWazaMess, FFXI::File::XIFileManager::GetLanguageDependentDatIndex(FFXI::Constants::Enums::LanguageDependentDats::MonWaza),   true);
	ReadMess(   &UnityMess, FFXI::File::XIFileManager::GetLanguageDependentDatIndex(FFXI::Constants::Enums::LanguageDependentDats::Unity),     true);

	ReadMess(&WeatherHead, 7032, false);
	ReadMess(&WeatherHead2, 7036, false);
}

void FFXI::Text::XIAtelMess::SevDataRead(unsigned short a1, unsigned short a2)
{
	int file_index = 0;
	if (a2 < 100 || a2 > 1299) {
		if (a1 < 256) {
			file_index = a1 + FFXI::File::XIFileManager::GetLanguageDependentDatIndex(Constants::Enums::LanguageDependentDats::Sev1);
		}
		else {
			file_index = a1 + FFXI::File::XIFileManager::GetLanguageDependentDatIndex(Constants::Enums::LanguageDependentDats::Sev3) - 256;
		}
	}
	else {
		file_index = a2 + FFXI::File::XIFileManager::GetLanguageDependentDatIndex(Constants::Enums::LanguageDependentDats::Sev2) - 1000;
	}

	int file_size = FFXI::File::XIFileManager::g_pXIFileManager->GetFileSizeByNumfile(file_index);
	if (file_size == -1) {
		return;
	}

	SevMess = new char[file_size + 1500];
	FFXI::File::XIFileManager::g_pXIFileManager->ReadNumfileNow(file_index, SevMess, file_size, 0);
	MesConv((unsigned char*)SevMess, file_size);
}

void FFXI::Text::XIAtelMess::ReadMess(char** a1, unsigned int a2, bool a3)
{
	int file_size = FFXI::File::XIFileManager::g_pXIFileManager->GetFileSizeByNumfile(a2);
	if (file_size == -1) {
		return;
	}

	*a1 = new char[file_size + 1500];
	FFXI::File::XIFileManager::g_pXIFileManager->ReadNumfileNow(a2, *a1, file_size, 0);
	if (a3 == true) {
		MesConv((unsigned char*)*a1, file_size);
	}
}

void FFXI::Text::XIAtelMess::MesConv(unsigned char* a1, int a2)
{
	if (a1[3] != 16) {
		return;
	}

	for (int i = 4; i < a2; ++i) {
		a1[i] ^= 0x80u;
	}

	a1[3] = 0;
}

void FFXI::Text::XIAtelMess::Clean()
{
	delete[] StatusMess;
	StatusMess = nullptr;
	delete[] BtlMess;
	BtlMess = nullptr;
	delete[] EmotionMess;
	EmotionMess = nullptr;
	delete[] SkillName;
	SkillName = nullptr;
	delete[] BitName;
	BitName = nullptr;
	delete[] SystemMess;
	SystemMess = nullptr;
	delete[] WeatherHead;
	WeatherHead = nullptr;
	delete[] WeatherHead2;
	WeatherHead2 = nullptr;
	delete[] MonWazaMess;
	MonWazaMess = nullptr;
	delete[] UnityMess;
	UnityMess = nullptr;
	delete[] SevMess;
	SevMess = nullptr;
}
