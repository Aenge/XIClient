#include "CXiDollActor.h"
using namespace FFXI::CYy;

const BaseGameObject::ClassInfo CXiDollActor::CXiDollActorClass{
	"CXiDollActor", sizeof(CXiDollActor), &CXiActor::CXiActorClass
};
const BaseGameObject::ClassInfo* FFXI::CYy::CXiDollActor::GetRuntimeClass()
{
	return &CXiDollActor::CXiDollActorClass;
}
