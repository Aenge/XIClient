#include "CMoSphRes.h"

const FFXI::CYy::BaseGameObject::ClassInfo FFXI::CYy::CMoSphRes::CMoSphResClass{	"CMoSphRes", sizeof(CMoSphRes), &FFXI::CYy::CMoResource::CMoResourceClass };
const FFXI::CYy::BaseGameObject::ClassInfo* FFXI::CYy::CMoSphRes::GetRuntimeClass() { return &CMoSphRes::CMoSphResClass; }

void FFXI::CYy::CMoSphRes::Open()
{
    // Convert relative pointer addresses to absolute memory addresses
    // This is necessary because the path data is stored with relative offsets in the resource file
    this->pathData.pointsArray = (unsigned int*)((char*)this->pathData.pointsArray + (int)&this->pathData);
    this->pathData.segmentsArray = (unsigned int*)((char*)this->pathData.segmentsArray + (int)&this->pathData);
    this->pathData.field_18 += (int)&this->pathData;
    this->pathData.field_1C += (int)&this->pathData;
}

void FFXI::CYy::CMoSphRes::Close()
{
	//nullsub
}

void FFXI::CYy::CMoSphRes::VObj6()
{
	//nullsub
}
