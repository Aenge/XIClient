#include "CYyMotionQue.h"
#include "CMoResourceMng.h"
#include "CMoMo2.h"
#include "CYyDb.h"
#include "CXiSkeletonActor.h"
using namespace FFXI::CYy;

const BaseGameObject::ClassInfo CYyMotionQue::CYyMotionQueClass{
	"CYyMotionQue", sizeof(CYyMotionQue), &CYyQue::CYyQueClass
};

const BaseGameObject::ClassInfo* FFXI::CYy::CYyMotionQue::GetRuntimeClass()
{
	return &CYyMotionQue::CYyMotionQueClass;
}

void FFXI::CYy::CYyMotionQue::VirtQue1()
{
}

int FFXI::CYy::CYyMotionQue::GetResID()
{
	return this->MotionFourCC;
}

void* FFXI::CYy::CYyMotionQue::GetMod()
{
	return this->MotionResource;
}

void FFXI::CYy::CYyMotionQue::SetMod(void* a2)
{
	this->MotionResource = (CMoMo2**)a2;
}

void FFXI::CYy::CYyMotionQue::SetSpeed(float a2)
{
	this->Speed = a2;
}

float FFXI::CYy::CYyMotionQue::GetFrame()
{
	return this->Frame;
}

void FFXI::CYy::CYyMotionQue::AdvanceAnimation(CXiSkeletonActor* a2, int a3)
{
	if (this->MotionResource == nullptr || CMoResourceMng::IsResourceReady((CMoResource***)&this->MotionResource) == false) {
		return;
	}
	
	if (this->LoopCounter < 0) {
		return;
	}
	
	float frameBackup = this->Frame;
	float frameAdvance{};

	if (this->CustomDuration > 0.0f) {
		frameAdvance = this->CustomDuration;
	}
	else {
		frameAdvance = (float)(*this->MotionResource)->GetFrameCount();
	}
	
	float motionTiming = (*this->MotionResource)->GetTiming();
	if (frameAdvance == 2.0f && motionTiming == 1.0f) {
		this->Frame += 1.0f;
	}
	else {
		float timeScaling = motionTiming * 0.5 * this->Speed;
		this->Frame += CYyDb::g_pCYyDb->CheckTick() * timeScaling;
	}
	
	if (this->LoopCounter == 0 || (this->LoopCounter -= 1) != 0) {
		if (this->Frame > frameAdvance) {
			if (frameAdvance <= 0) {
				this->Frame = 0.0f;
			}
			else {
				while (this->Frame >= frameAdvance) {
					this->Frame -= frameAdvance;
				}
			}
		}
		else {
			if (frameAdvance <= 0) {
				this->Frame = 1.0f;
			}
			else {
				while (this->Frame < 0.0f) {
					this->Frame += frameAdvance;
				}
			}
		}
	}
	else {
		this->LoopCounter = -1;
		if (a3 >= 3) {
			this->BeginFadeOut();
		}
		else if (this->Previous == nullptr) {
			a2->SetMotionLock(true);
		}

		this->Speed = 0.0;
		this->Frame = frameBackup;
	}
}

void FFXI::CYy::CYyMotionQue::ApplyAnimationToBones(int a2, int a3, void* a4, CYyQue* a5)
{
	float v9 = this->BlendWeight;
	if (a5 == nullptr && a2 >= 5)
		v9 = 1.0;

	if (this->MotionResource == nullptr)
		return;

	if (CMoResourceMng::IsResourceReady((CMoResource***) &this->MotionResource) == false)
		return;

	(*this->MotionResource)->CalcMotionData(a2, this->Frame, v9, (BoneTransformState*)a4);
}

bool FFXI::CYy::CYyMotionQue::IsFinished()
{
	return this->LoopCounter < 0;
}

FFXI::CYy::CYyMotionQue::CYyMotionQue()
{
	this->Frame = 0;
	this->Speed = 0;
	this->LoopCounter = 0;
	this->MotionFourCC = 0;
	this->MotionResource = 0;
	this->field_3C = 0;
	this->CustomDuration = 0;
}

FFXI::CYy::CYyMotionQue::~CYyMotionQue()
{
	if (this->MotionResource != nullptr) {
		(*this->MotionResource)->DecrementReferenceCount();
	}
}

bool FFXI::CYy::CYyMotionQue::UpdateBlendWeights()
{
	float engineTick = CYyDb::g_pCYyDb->CheckTick();
	double updatedBlendWeight = engineTick * this->BlendRate + this->BlendWeight;
	switch (this->State) {
	case AnimationBlendState::NORMAL:
		break;
	case AnimationBlendState::FADE_IN:
		this->BlendWeight = (float)updatedBlendWeight;
		this->BlendDuration -= engineTick;
		if (this->BlendDuration <= 0.0f) {
			this->State = AnimationBlendState::NORMAL;
			if (this->BlendWeight > 1.0f) {
				this->BlendWeight = 1.0f;
			}
		}
		break;
	case AnimationBlendState::COMPLETED:
		this->RemainingLifetime -= engineTick;
		if (this->RemainingLifetime <= 0.0f) {
			return false;
		}
		break;
	case AnimationBlendState::FADE_OUT:
		this->BlendWeight = updatedBlendWeight;
		this->BlendDuration -= engineTick;
		this->RemainingLifetime -= engineTick;
		if (this->RemainingLifetime <= 0.0f) {
			this->State = AnimationBlendState::COMPLETED;
			if (this->BlendWeight > 1.0f) {
				this->BlendWeight = 1.0f;
			}
			return false;
		}
		if (this->BlendDuration <= 0.0f) {
			this->State = AnimationBlendState::COMPLETED;
			if (BlendWeight > 1.0f) {
				this->BlendWeight = 1.0f;
			}
		}
		break;
	default:
		break;
	}

	if (this->BlendWeight < 0.0f) {
		this->BlendWeight = 0.0f;
	}
	else if (this->BlendWeight > 1.0f) {
		this->BlendWeight = 1.0f;
	}

	return true;
}

void FFXI::CYy::CYyMotionQue::BeginFadeOut()
{
	if (this->State == AnimationBlendState::NORMAL || this->State == AnimationBlendState::FADE_IN) {
		this->BlendDuration = this->FadeoutDuration;
		this->RemainingLifetime = this->FadeoutDuration;
		this->State = AnimationBlendState::FADE_OUT;
		if (this->FadeoutDuration <= 0.0) {
			this->BlendRate = 0.0;
		}
		else {
			this->BlendRate = -this->BlendWeight / this->FadeoutDuration;
		}
	}
}
