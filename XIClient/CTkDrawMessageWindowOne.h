#pragma once
#include "CTkDrawMessageWindow.h"

namespace FFXI {
	namespace CTk {
		class CTkMsgWinData;
		class CTkDrawMessageWindowOne : public CTkDrawMessageWindow {
		public:
			CTkDrawMessageWindowOne() = delete;
			CTkDrawMessageWindowOne(CTkMsgWinData*);
			virtual ~CTkDrawMessageWindowOne();
			virtual void OnInitialUpdatePrimitive() override final;
			virtual void OnDrawPrimitive() override final;
			virtual void OnDrawCalc(bool) override final;
			virtual bool MsgWinVirt3() override final;
			virtual char MsgWinVirt4() override final;
			virtual const char* MsgWinGetResName() override final;
			virtual int MsgWinVirt6() override final;
			virtual void MsgWinVirt7(short) override final;
		};
	}
}