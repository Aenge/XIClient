#include "CXiAtelActor.h"
#include "Globals.h"
using namespace FFXI::CYy;

const BaseGameObject::ClassInfo CXiAtelActor::CXiAtelActorClass{
	"CXiAtelActor", sizeof(CXiAtelActor), &CXiActor::CXiActorClass
};

const BaseGameObject::ClassInfo* FFXI::CYy::CXiAtelActor::GetRuntimeClass()
{
	return &CXiAtelActor::CXiAtelActorClass;
}

char FFXI::CYy::CXiAtelActor::OnMove()
{
	this->field_C4 = this->field_34;
	Globals::RotClamp(&this->field_44);
	return 0;
}
