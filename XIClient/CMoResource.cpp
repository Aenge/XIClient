#include "CMoResource.h"
#include "CMoResourceMng.h"
#include "MemoryPoolManager.h"
#include "CYyDb.h"
#include "WMatrix.h"
#include "Globals.h"
#include "Enums.h"
#include "Values.h"
#include "XiDateTime.h"
#include <cstring>
#include <iostream>
#include "CYyMeb.h"
#include "ResourceContainer.h"
#include "CMoKeyframe.h"
#include "CYyBmp2.h"
#include "CYyMsb.h"
#include "CMoD3a.h"
#include "CMoD3b.h"
#include "CMoD3m.h"
#include "CMoVtx.h"
#include "CMoSphRes.h"
#include "CMoRid.h"
#include "CMoWeather.h"
#include "CYySepRes.h"
#include <iostream>
#include "CYyScheduler.h"
#include "CYyGenerator.h"
#include "CMoCamera.h"
#include "CMoMmb.h"
#include "CMoMzb.h"
#include "CMoCib.h"
#include "CMoSk2.h"
#include "CMoMo2.h"
#include "CMoOs2.h"
#include "ResourceList.h"
using namespace FFXI::CYy;
using namespace FFXI::Constants;

const BaseGameObject::ClassInfo CMoResource::CMoResourceClass{"CMoResource", sizeof(CMoResource), &BaseGameObject::BaseGameObjectClass};

unsigned int CMoResource::TraversalIdCounter{ 0 };
const BaseGameObject::ClassInfo* FFXI::CYy::CMoResource::GetRuntimeClass()
{
	return &CMoResourceClass;
}

FFXI::CYy::CMoResource::~CMoResource()
{
	this->CMoResource::Close();
	CYyDb::pCMoResourceMng->RemoveRes(this->Metadata.SelfReference);
}

void FFXI::CYy::CMoResource::VObj2(int* param)
{
	if (param[0] == 6) {
		CMoResource::UnlinkFromManager(&this->Metadata.SelfReference);
	}
}

void FFXI::CYy::CMoResource::VObj5(void* param)
{
	exit(0x1002CA60);
	//Some of the resources need to have their vobj5 overridden
	//Haven't seen where VObj5 is called yet
	*(this->Metadata.SelfReference) = (CMoResource*)param;
}

void FFXI::CYy::CMoResource::ConstructFromData(char* p_buf)
{
	unsigned int size = GetSizeFromRawData(p_buf);
	memcpy(&this->ResourceID, p_buf, size);
}

void FFXI::CYy::CMoResource::Open()
{
	//nullsub
}

void FFXI::CYy::CMoResource::Close()
{
	if ((this->Metadata.StateFlags & RESOURCE_CLOSING) != 0)
		return;

	this->Metadata.StateFlags |= RESOURCE_CLOSING;
	this->CleanupResource();

	if ((this->Metadata.StateFlags & RESOURCE_ACTIVE) == 0)
		return;

	this->Metadata.StateFlags &= ~RESOURCE_ACTIVE;

	CYyDb::pCMoResourceMng->RemoveRes(this->Metadata.SelfReference);

	this->DestroyObject();
}

FFXI::Math::WMatrix* FFXI::CYy::CMoResource::VRes4()
{
	return &Globals::emulate_matrix;
}

int FFXI::CYy::CMoResource::GetDependencyCount()
{
	return this->GetRoot()->Metadata.ActiveDependencyCount;
}

CMoResource* FFXI::CYy::CMoResource::OpenResource(char* p_buf, CMoResource* parentResource)
{
	char* mem{ nullptr };
	CMoResource* res{ nullptr };
	
	//Decrease resource size by 16 because we don't need to allocate those 16 bytes
	//They already have a spot in the resource class (ResourceID, TypeSizeFlags, ParentResourceReference, PreviousLoadedResourceReference)
	unsigned int resourceSize = GetSizeFromRawData(p_buf) - 16;

	while (true) {
		int maxsize = MemoryPoolManager::globalInstance->FindLargestOpenSlot(MemoryPoolManager::MemoryPoolType::Lower);
		
		//Check if theres enough room in the memory pool
		if (maxsize < (2 * resourceSize) || CYyDb::pCMoResourceMng->ResourceCount > 0xCCCC) {
			
			//Attempt to free unused resources to make room in the pool
			for (int i = 0; i < 4; ++i) 
			{
				CMoResource* resourceFile = nullptr;

				CMoResource** mostRecentResourceFile = CMoResourceMng::LastResourceCreatedByType[Enums::ResourceType::Rmp];
				if (mostRecentResourceFile != nullptr)
					resourceFile = *mostRecentResourceFile;

				if (resourceFile != nullptr) {
					while (true) {
						CMoResource* root = resourceFile->GetRoot();
						if ((*root->Metadata.SelfReference)->Metadata.MemoryReferenceCount == 0) {
							CMoResource::UnlinkFromManager(&root->Metadata.SelfReference);
							break;
						}

						if (resourceFile->Metadata.PreviousResourceOfSameType != nullptr)
							resourceFile = *resourceFile->Metadata.PreviousResourceOfSameType;
						else
							resourceFile = nullptr;

						if (resourceFile == nullptr)
							break;
					}
				}
			}
		}


		switch (GetTypeFromRawData(p_buf)) {
		case Constants::Enums::ResourceType::Rmp:
			mem = MemoryPoolManager::globalInstance->GetOrUpper(resourceSize + sizeof(ResourceContainer), MemoryPoolManager::MemoryPoolType::Lower);
			CMoResourceMng::SomeByte = 1;
			if (!mem) continue;
			res = new (mem) ResourceContainer();
			break;
		case Constants::Enums::ResourceType::Generater:
			mem = MemoryPoolManager::globalInstance->GetOrUpper(resourceSize + sizeof(CYyGenerator) + 56, MemoryPoolManager::MemoryPoolType::Lower);
			CMoResourceMng::SomeByte = 1;
			if (!mem) continue;
			res = new (mem) CYyGenerator();
			break;
		case Constants::Enums::ResourceType::Camera:
			mem = MemoryPoolManager::globalInstance->GetOrUpper(resourceSize + sizeof(CMoCamera) + 8, MemoryPoolManager::MemoryPoolType::Lower);
			CMoResourceMng::SomeByte = 1;
			if (!mem) continue;
			res = new (mem) CMoCamera();
			break;
		case Constants::Enums::ResourceType::Scheduler:
			mem = MemoryPoolManager::globalInstance->GetOrUpper(resourceSize + sizeof(CYyScheduler), MemoryPoolManager::MemoryPoolType::Lower);
			CMoResourceMng::SomeByte = 1;
			if (!mem) continue;
			res = new (mem) CYyScheduler();
			break;
		case Constants::Enums::ResourceType::Keyframe:
			mem = MemoryPoolManager::globalInstance->GetOrUpper(resourceSize + sizeof(CMoKeyframe), MemoryPoolManager::MemoryPoolType::Lower);
			CMoResourceMng::SomeByte = 1;
			if (!mem) continue;
			res = new (mem) CMoKeyframe();
			break;
		case Constants::Enums::ResourceType::Bmp2:
			mem = MemoryPoolManager::globalInstance->GetOrUpper(resourceSize + sizeof(CYyBmp2), MemoryPoolManager::MemoryPoolType::Lower);
			CMoResourceMng::SomeByte = 1;
			if (!mem) continue;
			res = new (mem) CYyBmp2();
			break;
		case Constants::Enums::ResourceType::Mzb:
			mem = MemoryPoolManager::globalInstance->GetOrUpper(resourceSize + sizeof(CMoMzb), MemoryPoolManager::MemoryPoolType::Lower);
			CMoResourceMng::SomeByte = 1;
			if (!mem) continue;
			res = new (mem) CMoMzb();
			break;
		case Constants::Enums::ResourceType::D3m:
			mem = MemoryPoolManager::globalInstance->GetOrUpper(resourceSize + sizeof(CMoD3m), MemoryPoolManager::MemoryPoolType::Lower);
			CMoResourceMng::SomeByte = 1;
			if (!mem) continue;
			res = new (mem) CMoD3m();
			break;
		case Constants::Enums::ResourceType::D3s:
			mem = MemoryPoolManager::globalInstance->GetOrUpper(resourceSize + sizeof(CYyBmp2), MemoryPoolManager::MemoryPoolType::Lower);
			CMoResourceMng::SomeByte = 1;
			if (!mem) continue;
			res = new (mem) CYyBmp2();
			//This gets overwritten in ConstructFromData
			res->TypeSizeFlags = (res->TypeSizeFlags & 0xFFFFFF80) ^ 0x1B;
			break;
		case Constants::Enums::ResourceType::D3a:
			mem = MemoryPoolManager::globalInstance->GetOrUpper(resourceSize + sizeof(CMoD3a), MemoryPoolManager::MemoryPoolType::Lower);
			CMoResourceMng::SomeByte = 1;
			if (!mem) continue;
			res = new (mem) CMoD3a();
			break;
		case Constants::Enums::ResourceType::D3b:
			mem = MemoryPoolManager::globalInstance->GetOrUpper(resourceSize + sizeof(CMoD3b), MemoryPoolManager::MemoryPoolType::Lower);
			CMoResourceMng::SomeByte = 1;
			if (!mem) continue;
			res = new (mem) CMoD3b();
			break;
		case Constants::Enums::ResourceType::Sk2:
			mem = MemoryPoolManager::globalInstance->GetOrUpper(resourceSize + sizeof(CMoSk2), MemoryPoolManager::MemoryPoolType::Lower);
			CMoResourceMng::SomeByte = 1;
			if (!mem) continue;
			res = new (mem) CMoSk2();
			break;
		case Constants::Enums::ResourceType::Os2:
			mem = MemoryPoolManager::globalInstance->GetOrUpper(resourceSize + sizeof(CMoOs2), MemoryPoolManager::MemoryPoolType::Lower);
			CMoResourceMng::SomeByte = 1;
			if (!mem) continue;
			res = new (mem) CMoOs2();
			break;
			break;
		case Constants::Enums::ResourceType::Mo2:
			mem = MemoryPoolManager::globalInstance->GetOrUpper(resourceSize + sizeof(CMoMo2), MemoryPoolManager::MemoryPoolType::Lower);
			CMoResourceMng::SomeByte = 1;
			if (!mem) continue;
			res = new (mem) CMoMo2();
			break;
		case Constants::Enums::ResourceType::Mmb:
			mem = MemoryPoolManager::globalInstance->GetOrUpper(resourceSize + sizeof(CMoMmb), MemoryPoolManager::MemoryPoolType::Lower);
			CMoResourceMng::SomeByte = 1;
			if (!mem) continue;
			res = new (mem) CMoMmb();
			break;
		case Constants::Enums::ResourceType::Weather:
			mem = MemoryPoolManager::globalInstance->GetOrUpper(resourceSize + sizeof(CMoWeather), MemoryPoolManager::MemoryPoolType::Lower);
			CMoResourceMng::SomeByte = 1;
			if (!mem) continue;
			res = new (mem) CMoWeather();
			break;
		case Constants::Enums::ResourceType::Meb:
			mem = MemoryPoolManager::globalInstance->GetOrUpper(resourceSize + sizeof(CYyMeb) + 1, MemoryPoolManager::MemoryPoolType::Lower);
			CMoResourceMng::SomeByte = 1;
			if (!mem) continue;
			res = new (mem) CYyMeb();
			break;
		case Constants::Enums::ResourceType::Msb:
			mem = MemoryPoolManager::globalInstance->GetOrUpper(resourceSize + sizeof(CYyMsb) + 1, MemoryPoolManager::MemoryPoolType::Lower);
			CMoResourceMng::SomeByte = 1;
			if (!mem) continue;
			res = new (mem) CYyMsb();
			break;
		case Constants::Enums::ResourceType::Rid:
			mem = MemoryPoolManager::globalInstance->GetOrUpper(resourceSize + sizeof(CMoRid), MemoryPoolManager::MemoryPoolType::Lower);
			CMoResourceMng::SomeByte = 1;
			if (!mem) continue;
			res = new (mem) CMoRid();
			break;
		case Constants::Enums::ResourceType::Sep:
			[[fallthrough]];
		case Constants::Enums::ResourceType::Spw:
			mem = MemoryPoolManager::globalInstance->GetOrUpper(resourceSize + sizeof(CYySepRes), MemoryPoolManager::MemoryPoolType::Lower);
			CMoResourceMng::SomeByte = 1;
			if (!mem) continue;
			res = new (mem) CYySepRes();
			break;
		case Constants::Enums::ResourceType::Vtx:
			mem = MemoryPoolManager::globalInstance->GetOrUpper(resourceSize + sizeof(CMoVtx), MemoryPoolManager::MemoryPoolType::Lower);
			CMoResourceMng::SomeByte = 1;
			if (!mem) continue;
			res = new (mem) CMoVtx();
			break;
		case Constants::Enums::ResourceType::Cib:
			mem = MemoryPoolManager::globalInstance->GetOrUpper(resourceSize + sizeof(CMoCib), MemoryPoolManager::MemoryPoolType::Lower);
			CMoResourceMng::SomeByte = 1;
			if (!mem) continue;
			res = new (mem) CMoCib();
			break;
		case Constants::Enums::ResourceType::Sph:
			mem = MemoryPoolManager::globalInstance->GetOrUpper(resourceSize + sizeof(CMoSphRes), MemoryPoolManager::MemoryPoolType::Lower);
			CMoResourceMng::SomeByte = 1;
			if (!mem) continue;
			res = new (mem) CMoSphRes();
			break;
		default:
			//std::cout << "unhandled res type: " << (Type_Size & 0x7F) << std::endl;
			//char ok[256];
			//sprintf_s(ok, 256, "unhandled res type: %d", Type_Size & 0x7F);
			//MessageBoxA(NULL, ok, "", 0);
			mem = MemoryPoolManager::globalInstance->GetOrUpper(resourceSize + 48, MemoryPoolManager::MemoryPoolType::Lower);
			if (!mem) continue;
			CMoResourceMng::SomeByte = 1;
			res = new (mem) CMoResource();
		}
		
		if (!res) continue;
		res->ConstructFromData(p_buf);
		if (parentResource != nullptr) {
			res->ParentResourceReference = parentResource->Metadata.SelfReference;
		}
		res->Metadata.StateFlags = res->Metadata.StateFlags & 0xFFF0 | RESOURCE_ACTIVE;
		return res;
	}
}

FFXI::CYy::CMoResource::CMoResource()
{
	this->Metadata.Initialize();
	this->Metadata.SelfReference = nullptr;

	// Clear the lowest bit, then copy the lowest bit from SomeByte
	this->Metadata.StateFlags &= ~RESOURCE_ACTIVE;
	this->Metadata.StateFlags |= CMoResourceMng::SomeByte & 1;
	
	CMoResourceMng::SomeByte = 0;
	this->ParentResourceReference = nullptr;
}

CMoResource* FFXI::CYy::CMoResource::GetParent()
{
	CMoResource** ParentResourceReference = this->ParentResourceReference;
	if (ParentResourceReference)
		return *ParentResourceReference;
	return nullptr;
}

void FFXI::CYy::CMoResource::RegisterWithManager()
{
	if ((this->TypeSizeFlags & 0x8000000) != 0) return;

	CYyDb::pCMoResourceMng->Link(this);

	unsigned int v3 = this->TypeSizeFlags & 0x7F;
	if (v3 >= Constants::Values::COUNT_RESOURCE_TYPES)
		v3 = 4;
	CMoResource** v4 = CMoResourceMng::LastResourceCreatedByType[v3];
	if (v4)
		(*v4)->Metadata.NextResourceOfSameType = this->Metadata.SelfReference;
	this->Metadata.PreviousResourceOfSameType = CMoResourceMng::LastResourceCreatedByType[v3];
	CMoResourceMng::LastResourceCreatedByType[v3] = this->Metadata.SelfReference;
	this->PreviousLoadedResourceReference = CMoResourceMng::MostRecentLoadedResourceReference;
	CMoResourceMng::MostRecentLoadedResourceReference = this->Metadata.SelfReference;
	this->TypeSizeFlags |= 0x8000000;
}

CMoResource* FFXI::CYy::CMoResource::GetRoot()
{
	CMoResource* current = this;
	
	while (current->ParentResourceReference != nullptr) 
	{
		if (current == *current->ParentResourceReference)
			return current;
	
		current = *current->ParentResourceReference;
	}

	return nullptr;
}

void FFXI::CYy::CMoResource::InitializeByType(char a2)
{
	this->RegisterWithManager();
	Enums::ResourceType v2 = (Enums::ResourceType)(this->TypeSizeFlags & 0x7F);
	if ((a2 & 1) != 0) {
		if (v2 == Enums::ResourceType::Spw ||
			v2 == Enums::ResourceType::Sep ||
			v2 == Enums::ResourceType::D3s ||
			v2 == Enums::ResourceType::Bp ||
			v2 == Enums::ResourceType::Bmp2 ||
			v2 == Enums::ResourceType::Tim2) {
			this->Open();
			this->Metadata.StateFlags |= RESOURCE_OPENED;
			return;
		}
	}
	if ((a2 & 2) != 0) {
		if (v2 == Enums::ResourceType::Texinfo ||
			v2 == Enums::ResourceType::Mmb ||
			v2 == Enums::ResourceType::Rid ||
			v2 == Enums::ResourceType::Os2 ||
			v2 == Enums::ResourceType::Sk2 ||
			v2 == Enums::ResourceType::Mo2 ||
			v2 == Enums::ResourceType::D3m ||
			v2 == Enums::ResourceType::D3a ||
			v2 == Enums::ResourceType::D3b ||
			v2 == Enums::ResourceType::Lfd ||
			v2 == Enums::ResourceType::Msb ||
			v2 == Enums::ResourceType::Meb ||
			v2 == Enums::ResourceType::Mtd ||
			v2 == Enums::ResourceType::Mgb ||
			v2 == Enums::ResourceType::Mtb ||
			v2 == Enums::ResourceType::Vtx) {
			this->Open();
			this->Metadata.StateFlags |= RESOURCE_OPENED;
			return;
		}
	}
	if ((a2 & 4) != 0) {
		if ((this->Metadata.StateFlags & RESOURCE_OPENED) == 0) {
			this->Open();
			this->Metadata.StateFlags |= RESOURCE_OPENED;
		}
	}
}

void FFXI::CYy::CMoResource::CleanupResource()
{
	this->UnlinkFromResourceLists();
	this->Close();
}

void FFXI::CYy::CMoResource::DecrementReferenceCount()
{
	CMoResource* res = this;
	while (CYyDb::pCMoResourceMng->InList(res->Metadata.SelfReference) == true) {
		CMoResource** ParentResourceReference = res->ParentResourceReference;
		CMoResource* v4 = ParentResourceReference ? *ParentResourceReference : nullptr;

		if (v4 == res) {
			if ((res->Metadata.MemoryReferenceCount & 0x7FFF) != 0) {
				res->Metadata.MemoryReferenceCount ^= (res->Metadata.MemoryReferenceCount ^ (res->Metadata.MemoryReferenceCount - 1)) & 0x7FFF;
				if ((res->Metadata.MemoryReferenceCount & 0x7FFF) == 0) {
					res->Metadata.MemoryReferenceCount &= ~0x8000u;
				}
			}

			return;
		}
		else {
			res->Metadata.MemoryReferenceCount ^= (res->Metadata.MemoryReferenceCount ^ (res->Metadata.MemoryReferenceCount - 1)) & 0x7FFF;
			res = v4;
		}
	}
}

void FFXI::CYy::CMoResource::UnlinkFromResourceLists()
{
	if ((this->TypeSizeFlags & 0x8000000) == 0) {
		return;
	}

	CMoResource* SomeRes{ nullptr };
	{//One timer func. Inline it.
		CMoResource* v2{ nullptr };
		if (CMoResourceMng::MostRecentLoadedResourceReference)
			v2 = *CMoResourceMng::MostRecentLoadedResourceReference;
		while (v2 != nullptr && (v2 != this)) {
			SomeRes = v2;
			if (v2->PreviousLoadedResourceReference == nullptr)
				v2 = nullptr;
			else
				v2 = *v2->PreviousLoadedResourceReference;
		}
	}

	if (SomeRes != nullptr) {
		SomeRes->PreviousLoadedResourceReference = this->PreviousLoadedResourceReference;
	}
	else {
		CMoResourceMng::MostRecentLoadedResourceReference = this->PreviousLoadedResourceReference;
	}

	this->PreviousLoadedResourceReference = nullptr;

	if (this->Metadata.NextResourceOfSameType != nullptr) {
		(*this->Metadata.NextResourceOfSameType)->Metadata.PreviousResourceOfSameType = this->Metadata.PreviousResourceOfSameType;
	}

	if (this->Metadata.PreviousResourceOfSameType != nullptr) {
		(*this->Metadata.PreviousResourceOfSameType)->Metadata.NextResourceOfSameType = this->Metadata.NextResourceOfSameType;
	}

	int v7 = this->TypeSizeFlags & 0x7F;
	if (v7 >= Constants::Values::COUNT_RESOURCE_TYPES)
		v7 = 4;

	if (CMoResourceMng::LastResourceCreatedByType[v7] == this->Metadata.SelfReference)
		CMoResourceMng::LastResourceCreatedByType[v7] = this->Metadata.PreviousResourceOfSameType;

	if (this->Metadata.PreviousResource != nullptr) {
		(*this->Metadata.PreviousResource)->Metadata.NextResource = this->Metadata.NextResource;
	}
	
	if (this->Metadata.NextResource != nullptr) {
		(*this->Metadata.NextResource)->Metadata.PreviousResource = this->Metadata.PreviousResource;
	}

	this->TypeSizeFlags &= ~0x8000000u;
}

void FFXI::CYy::CMoResource::IncrementReferenceCount()
{
	CMoResource* ref = this;
	while (true) {
		ref->Metadata.MemoryReferenceCount ^= (ref->Metadata.MemoryReferenceCount ^ (ref->Metadata.MemoryReferenceCount + 1)) & 0x7FFF;
		CMoResource** ParentResourceReference = ref->ParentResourceReference;
		CMoResource* head = ParentResourceReference ? *ParentResourceReference : nullptr;
		if (head == ref)
			break;
		ref = head;
	}
}

void FFXI::CYy::CMoResource::UnlinkFromManager(CMoResource*** a1)
{
	CYyDb::g_pCYyDb->pCMoResourceMng->Unlink(*a1);
}

CMoResource*** FFXI::CYy::CMoResource::FindResourceUnder(CMoResource*** a2, int a3, int a4)
{
	++CMoResource::TraversalIdCounter += 1;
	FindResourceNextUnder(a2, a3, a4, CMoResource::TraversalIdCounter);
	return a2;
}

void FFXI::CYy::CMoResource::FindResourceNextUnder(CMoResource*** a2, int a3, int a4, int a5)
{
	ResourceContainer* resourceFile{ nullptr };
	if ((this->TypeSizeFlags & 0x7F) == Constants::Enums::ResourceType::Rmp)
		resourceFile = (ResourceContainer*)this;
	else
		resourceFile = (ResourceContainer*)this->GetParent();

	ResourceContainer* v7 = resourceFile;
	while (resourceFile != nullptr) {
		CMoResource** TerminateNode = v7->TerminateNode;
		CMoResource* v9{ nullptr };
		if (TerminateNode)
			v9 = *TerminateNode;

		//need to investigate this
		if (resourceFile == v9) {
			if (v7->ParentResourceReference)
				resourceFile = (ResourceContainer*)*v7->ParentResourceReference;
			else
				resourceFile = nullptr;

			if (resourceFile == v7)
				break;
			v7 = resourceFile;
		}
		else {
			if ((resourceFile->TypeSizeFlags & 0x7F) == Constants::Enums::ResourceType::Rmp) {
				if (resourceFile->TraversalMarker == a5) {
					if (resourceFile->TerminateNode)
						resourceFile = (ResourceContainer*)*resourceFile->TerminateNode;
					else
						resourceFile = nullptr;
				}
			}
			if (a3) {
				if ((resourceFile->TypeSizeFlags & 0x7F) == a3) {
					if (!a4) {
						*a2 = resourceFile->Metadata.SelfReference;
						return;
					}
					if (resourceFile->ResourceID == a4) {
						*a2 = resourceFile->Metadata.SelfReference;
						return;
					}
				}
			}
			else if (resourceFile->ResourceID == a4) {
				*a2 = resourceFile->Metadata.SelfReference;
				return;
			}
		}
		if (resourceFile->Metadata.NextResource)
			resourceFile = (ResourceContainer*)*resourceFile->Metadata.NextResource;
		else
			resourceFile = nullptr;;
	}

	if ((this->TypeSizeFlags & 0x7F) == Constants::Enums::Rmp)
		resourceFile = (ResourceContainer*)this;
	else
		resourceFile = (ResourceContainer*)this->GetParent();

	ResourceContainer** RPLhead = (ResourceContainer**)resourceFile->ParentResourceReference;
	resourceFile->TraversalMarker = a5;
	ResourceContainer* v15{ nullptr };
	if (RPLhead)
		v15 = *RPLhead;
	if (resourceFile == v15) {
		if (CYyDb::pCMoResourceMng->Unknown3 == (ResourceContainer**)this->Metadata.SelfReference)
			*a2 = nullptr;
		else
			(*CYyDb::pCMoResourceMng->Unknown3)->FindResourceNextUnder(a2, a3, a4, a5);
	}
	else if (RPLhead)
		(*RPLhead)->FindResourceNextUnder(a2, a3, a4, a5);
	else
		((ResourceContainer*)nullptr)->FindResourceNextUnder(a2, a3, a4, a5);
}

CMoResource*** FFXI::CYy::CMoResource::AnotherResourceSearcher(CMoResource*** a2, Constants::Enums::ResourceType a3, int a4)
{
	CMoResource* head = this;
	int v4 = ((head->TypeSizeFlags & 0x7F) != 1) - 1;
	while (head) {
		int v5 = head->TypeSizeFlags & 0x7F;
		if (v5 == FFXI::Constants::Enums::ResourceType::Rmp)
			++v4;
		if (v5 == 0 && --v4 < 0)
			break;

		if (a3) {
			if (v5 == a3) {
				if (!a4) {
					*a2 = head->Metadata.SelfReference;
					return a2;
				}
				if (head->ResourceID == a4) {
					*a2 = head->Metadata.SelfReference;
					return a2;
				}
			}
		}
		else if (head->ResourceID == a4) {
			*a2 = head->Metadata.SelfReference;
			return a2;
		}
		if (head->Metadata.NextResource)
			head = *(head->Metadata.NextResource);
		else
			head = nullptr;
	}
	
	*a2 = nullptr;
	return a2;
}

bool FFXI::CYy::CMoResource::FindResourceWithCallback(int a2, int a3, bool(__cdecl* a4)(CMoResource**, void*), void* a5)
{
	int v8 = ((this->TypeSizeFlags & 0x7F) != FFXI::Constants::Enums::ResourceType::Rmp) - 1;
	CMoResource* v5 = this;
	while (v5) {
		int resourceType = v5->TypeSizeFlags & 0x7F;
		if (resourceType == FFXI::Constants::Enums::ResourceType::Rmp)
			++v8;
		if (resourceType == FFXI::Constants::Enums::ResourceType::Terminate && --v8 < 0)
			break;
		if (a2) {
			if (resourceType == a2) {
				if (!a3) {
					bool result = a4(v5->Metadata.SelfReference, a5);
					if (result)
						return result;
				}
			}
		}
		if (v5->ResourceID == a3) {
			bool result = a4(v5->Metadata.SelfReference, a5);
			if (result)
				return result;
		}
		
		if (v5->Metadata.NextResource)
			v5 = *v5->Metadata.NextResource;
		else
			v5 = nullptr;
	}

	return false;
}

void FFXI::CYy::CMoResource::GetActivateTime(XiDateTime* a2)
{
	int byte0 = (this->ResourceID >> 0) & 0xFF;
	int byte1 = (this->ResourceID >> 8) & 0xFF;
	int byte2 = (this->ResourceID >> 16) & 0xFF;
	int byte3 = (this->ResourceID >> 24) & 0xFF;

	XiDateTime v4{};
	v4.SetHour(byte1 + 10 * byte0 - 0x210);
	v4.SetMinute(byte3 + 10 * byte2 - 0x210);
	a2->Time = v4.Time;
}

void FFXI::CYy::CMoResource::CheckGenerators()
{
	ResourceList v2{};
	v2.PrepareFromResource(this, FFXI::Constants::Enums::ResourceType::Generater, 0, -1);
	CYyGenerator* gen = (CYyGenerator*)v2.GetNext(false);
	while (gen) {
		gen->Preset();
		gen = (CYyGenerator*)v2.GetNext(false);
	}
}

Enums::ResourceType FFXI::CYy::CMoResource::GetTypeFromRawData(char* data)
{
	return (Enums::ResourceType)(data[4] & 0x7F);
}

unsigned int FFXI::CYy::CMoResource::GetSizeFromRawData(char* data)
{
	unsigned int* intData = (unsigned int*)data + 1;
	return 16 * ((intData[0] >> 7) & 0x7FFFF);
}
