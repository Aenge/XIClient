#pragma once
namespace FFXI {
	namespace Constants {
		namespace Enums {
			enum ActorRace {
				NONE     = 0,
				HUME_M   = 1,
				HUME_F   = 2,
				ELVAAN_M = 3,
				ELVAAN_F = 4,
				TARU_M   = 5,
				TARU_F   = 6,
				MITHRA   = 7,
				GALKA    = 8
			};
		}
	}
}