#pragma once
#include "RidUnderscoreStruct.h"
namespace FFXI {
	namespace CYy {
		class RidStruct {
		public:
			FFXI::Math::WMatrix field_0;
			int fourCC;
			int field_44;
			int field_48;
			D3DXVECTOR3 field_4C[8];
			D3DXVECTOR3 Position;
			float field_B8;
			float field_BC;
			int field_C0;
			int field_C4;
			int field_C8;
			int field_CC;
			int field_D0;
			int field_D4;
			RidUnderscoreStruct* field_D8;
			float field_DC;
			int field_E0;
			int field_E4;
			int field_E8;
			int field_EC;
			char field_F0;
			char field_F1;
			char field_F2;
			char field_F3;
		};
	}
}