#include "ActorPosHistory.h"

void FFXI::CYy::ActorPosHistory::Add(D3DXVECTOR3* a2)
{
	D3DXVECTOR3 diff = *a2 - this->stack[0];
	float diffmag = sqrt(diff.x * diff.x + diff.y * diff.y + diff.z * diff.z);
	if (diffmag < 0.1f) {
		return;
	}

	const int array_size = sizeof(this->stack) / sizeof(this->stack[0]);
	for (int i = array_size - 1; i > 0; --i) {
		this->stack[i] = this->stack[i - 1];
	}

	this->stack[0] = *a2;
	this->field_34 += 1;
}

void FFXI::CYy::ActorPosHistory::GetOldestPos(D3DXVECTOR3* a2)
{
	*a2 = this->stack[3];
}
