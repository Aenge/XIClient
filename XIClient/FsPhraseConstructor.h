#pragma once
#include "FsHistory.h"
namespace FFXI {
	namespace Input {
		class FsPhraseConstructor {
		public:
			~FsPhraseConstructor();
			FsPhraseConstructor();
			int convertPhrase(char*, unsigned int, char*, unsigned int, bool, bool);
			int convertToString(char*, unsigned int, char*, unsigned int, unsigned int*, bool, bool);
			bool isWordIndex(char*, unsigned int);
			char field_0;
			char field_1{};
			char field_2{};
			char field_3{};
			char field_4{};
			char field_5;
			char field_6;
			char field_7;
			char field_8[12][4]{};
			int field_38{};
			int field_3C;
			char field_40[12][4]{};
			int field_70{};
			char field_74[12][4]{};
			int field_A4{};
			FsHistory field_A8;
		};
	}
}