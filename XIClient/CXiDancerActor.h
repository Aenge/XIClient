#pragma once
#define WIN32_LEAN_AND_MEAN
#include "CXiSkeletonActor.h"
#include "XiAtelBuff.h"
#include "d3dx9math.h"
namespace FFXI {
	namespace CYy {
		class XiDancerActorPara;
		class CXiDancerActor : public CXiSkeletonActor {
		public:
			static const BaseGameObject::ClassInfo CXiDancerActorClass;
			virtual const BaseGameObject::ClassInfo* GetRuntimeClass() override;
			virtual ~CXiDancerActor();
			CXiDancerActor(D3DXVECTOR4*, D3DXVECTOR4*, XiDancerActorPara*, int);
			void AtelBuffSet(D3DXVECTOR4*, D3DXVECTOR4*, XiDancerActorPara*, int);
			void AtelBuffInit(XiAtelBuff*, D3DXVECTOR4*, D3DXVECTOR4*, XiDancerActorPara*);
			XiAtelBuff field_A0C;
			virtual void OnDraw() override final;
			virtual char OnMove() override final;
		};
	}
}