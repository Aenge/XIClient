#pragma once

namespace FFXI {
	namespace CYy { class CYyMusicLoadTask;  }
	class MusicServer {
	public:
		static int last_request;
		static void clear_last_request();
		static void Play(int);
		static void Play(int, int, int, int, int, bool);
		static CYy::CYyMusicLoadTask* MusicLoadTask;
	};
}