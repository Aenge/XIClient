#include "CTkSpoolMsgType2.h"
#include "TkManager.h"
#include "CTkMenuMng.h"
#include "PrimMng.h"
#include "Strings.h"

FFXI::CTk::CTkSpoolMsgType2::CTkSpoolMsgType2(CTkDrawMessageWindow* a2)
	: CTkSpoolMsgBase(a2)
{
}

void FFXI::CTk::CTkSpoolMsgType2::CreateSpoolMsg(char* a2, TK_LOGDATAHEADER a3)
{
	if (PrimMng::g_pTkSpoolMsg2->IsDrawWindow() == false) {
		TkManager::g_CTkMenuMng.CreateDrawMenu(Constants::Strings::MenuSpoolMsg2, true, 0);
	}

	PrimMng::g_pTkSpoolMsg2->StoreMsg(a2, a3);
}

void FFXI::CTk::CTkSpoolMsgType2::DestroyWind()
{
	TkManager::g_CTkMenuMng.DestroyDrawMenu(Constants::Strings::MenuSpoolMsg);
}