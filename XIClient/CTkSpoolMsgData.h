#pragma once
#include "CTkObject.h"
#include "CTkMsgWinData.h"

namespace FFXI {
	namespace CTk {
		class CTkDrawMessageWindow;
		class CTkSpoolMsgData : public CTkObject {
		public:
			CTkSpoolMsgData() = delete;
			CTkSpoolMsgData(CTkDrawMessageWindow*);
			virtual ~CTkSpoolMsgData() = default;
			bool NeckCalc();
			short field_4[128];
			TK_LOGDATAHEADER field_104;
			int field_140;
			int field_144;
			char field_148;
			char field_149;
			signed __int16 field_14A;
			CTkDrawMessageWindow* field_14C;
		};
	}
}