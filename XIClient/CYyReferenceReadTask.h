#pragma once
#include "CMoTask.h"

namespace FFXI {
	namespace File { class ReferenceReadBase; }
	namespace CYy {
		class ResourceContainer;
		class CYyReferenceReadTask : public CMoTask {
		public:
			virtual void VObj2(int*) override final;
			virtual char OnMove() override final;
			CYyReferenceReadTask();
			virtual ~CYyReferenceReadTask();
			void Start(ResourceContainer**, void(__cdecl*)(ResourceContainer**, File::ReferenceReadBase*), File::ReferenceReadBase*, char);
			ResourceContainer** ResourceFile;
			void(__cdecl* Callback)(ResourceContainer**, File::ReferenceReadBase*);
			File::ReferenceReadBase* Context;
			char field_40;
			char field_41;
			char field_42;
			char field_43;
		};
	}
}