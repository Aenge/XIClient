#pragma once
#include "CTkMenuPrimitive.h"
#include "TkRect.h"
namespace FFXI {
	namespace CTk {
		class _49SubList;
		class CTkSubWindow : public CTkMenuPrimitive {
			static const int SUB_TYPE_COUNT = 9;
			static short tksubshapeidx[SUB_TYPE_COUNT];
			static short tksubtitleidx[2 * SUB_TYPE_COUNT];
		public:
			static int GetChatMode();
			CTkSubWindow();
			virtual ~CTkSubWindow();
			virtual void OnInitialUpdatePrimitive() override final;
			virtual void OnDrawPrimitive() override final;
			virtual void OnUpdatePrimitive() override final;
			void DrawChat();
			TKRECT field_14{};
			int field_1C;
			int field_20;
			int field_24;
			int field_28;
			int field_2C;
			int field_30;
			int field_34;
			_49SubList* field_38[SUB_TYPE_COUNT];
			_49SubList* field_5C[SUB_TYPE_COUNT];
			_49SubList* field_80[SUB_TYPE_COUNT];
			_49SubList* field_A4;
		};
	}
}