#pragma once

namespace Placeholder {
	static void StartAutoOffline() {}
	static void PauseAutoOffline() {}

	static inline char gcZoneSubMapChangeSet(int a1, int a2) {
		return 0;
	}
	static inline void gcGroupLookStart() {}
	static inline void VulgarFilter(char*) { }
	const static void* g_pTkDelivery{ nullptr };
	const static void* g_pTkPost{ nullptr };
	const static void* pGlobalNowZone{ nullptr };
	const static void* pZoneSys{ nullptr };
	const static void* g_pTkInputCtrl{ nullptr };
	const static void* g_fsTextInput{ nullptr };
	const static void* g_fsTextInputDisplay{ nullptr };
	const static bool enable_change_focus{ false };
	const static void* g_pStDancer{ nullptr };
	const static void* g_pYkMyroom{ nullptr };
	const static void* l_pKaCompass{ nullptr };
	const static bool get_config_153(int) { return false; }
	const static int CTkMsgWinData = 0;
	const static bool CliEventUcFlag{ false };
	const static bool ClientEventUcFlag2{ false };
	const static bool EventExecFlag{ false };
	static inline bool KaComCheckBuffs() { return true; }
	static inline bool KaComCheckBuffStatis(int) { return false; }
	static inline bool CheckSomething() {
		if (pGlobalNowZone || pZoneSys)
			throw "NOT IMPLEMENTED";

		return false;
	}
	static inline bool IsEvent(int) {
		return false;
	}
	static inline int sub_100861B0() {
		return 0;
	}
}