#include "CTkDrawMessageWindowTwo.h"
#include "CTkDrawMessageWindowOne.h"
#include "PrimMng.h"
#include "CTkSubWindow.h"
#include "FsConfig.h"
#include "TkManager.h"
#include "CTkMenuMng.h"
#include "CTkMenuCtrlData.h"
#include "_49SubList.h"
#include "Strings.h"

FFXI::CTk::CTkDrawMessageWindowTwo::CTkDrawMessageWindowTwo(CTkMsgWinData* a2)
	: CTkDrawMessageWindow(a2)
{
	this->field_4D = 0;
}

FFXI::CTk::CTkDrawMessageWindowTwo::~CTkDrawMessageWindowTwo()
{
	//nullsub
}

void FFXI::CTk::CTkDrawMessageWindowTwo::OnInitialUpdatePrimitive()
{
	this->field_5C = 0;
	//sub //todo
	//buffered log here, this->field_18->field_6403C
	//leaving this comment here so when it's changed to buffered log this won't compile
	this->field_18->field_6403C = 0;
	this->CTkDrawMessageWindow::OnInitialUpdatePrimitive();
}

void FFXI::CTk::CTkDrawMessageWindowTwo::OnDrawPrimitive()
{
	this->CTkDrawMessageWindow::OnDrawPrimitive();
	if (this->field_31 <= 0) {
		return;
	}

	TKRECT a2{};
	this->MenuCtrlData->GetWindowLocate(&a2);

	_49SubList* list{};
	int chatmode = CTkSubWindow::GetChatMode();
	int v3 = FFXI::Config::FsConfig::GetConfig(Constants::Enums::FsConfigSubjects::Subject180);
	if (v3 == 0) {
		list = PrimMng::g_pTkSubWindow->field_5C[chatmode];
	}
	else {
		list = PrimMng::g_pTkSubWindow->field_80[chatmode];
	}

	if (list == nullptr) {
		return;
	}

	short diff = TkManager::g_CTkMenuMng.UIXRes - TkManager::g_CTkMenuMng.field_7C;

	float v1 = (1.0f - TkManager::g_CTkMenuMng.Get84(false)) * (float)diff - 512.0f;
	short v2 = diff - this->field_32 - 160;
	v1 -= (float)v2;
	a2.Left += (short)v1;
	list->Draw(a2.Left, a2.Top, 0x80808080, 0, 0);
}

bool FFXI::CTk::CTkDrawMessageWindowTwo::MsgWinVirt3()
{
	return false;
}

char FFXI::CTk::CTkDrawMessageWindowTwo::MsgWinVirt4()
{
	return PrimMng::g_pTkDrawMessageWindow2 != nullptr && PrimMng::g_pTkDrawMessageWindow2->MenuCtrlData != nullptr;
}

const char* FFXI::CTk::CTkDrawMessageWindowTwo::MsgWinGetResName()
{
	return Constants::Strings::MenuLogWindow2;
}

int FFXI::CTk::CTkDrawMessageWindowTwo::MsgWinVirt6()
{
	return 8;
}

void FFXI::CTk::CTkDrawMessageWindowTwo::MsgWinVirt7(short a2)
{
	if (a2 < 0) {
		this->field_52 = PrimMng::g_pTkDrawMessageWindow->field_52;
		return;
	}

	int v2 = (CTk::TkManager::g_CTkMenuMng.UIXRes - CTk::TkManager::g_CTkMenuMng.field_7C - 512) / 100 + 352;
	v2 *= a2;
	int v3 = (CTk::TkManager::g_CTkMenuMng.UIXRes - CTk::TkManager::g_CTkMenuMng.field_7C) - 160;
	if (v2 > v3) {
		v2 = v3;
	}

	if (v2 < 352) {
		v2 = 352;
	}

	this->field_52 = v2;
}
