#include "RuntimeConfig.h"
#define TOML_ENABLE_WINDOWS_COMPAT true
#include "toml++/toml.hpp"

FFXI::Config::RuntimeConfig FFXI::Config::RuntimeConfig::instance{};

void ParseOptionalKeys(const toml::table& tbl)
{
    FFXI::Config::RuntimeConfig* cfg = &FFXI::Config::RuntimeConfig::instance;

    //Input
    cfg->use_hardware_mouse = tbl["Input"]["use_hardware_mouse"].value_or<bool>(true);
    //Process
    cfg->process_priority = tbl["Process"]["priority"].value_or<unsigned int>(1);
    //Sound
    cfg->sound_enable = tbl["Sound"]["enable"].value_or<bool>(true);
    //Video

    int window_mode = tbl["Video"]["window_mode"].value_or<int>((int)FFXI::Config::RuntimeConfig::WindowMode::Windowed);
    if (window_mode < 0 || window_mode > 3) {
        window_mode = 1;
    }
    cfg->window_mode = (FFXI::Config::RuntimeConfig::WindowMode)window_mode;
    cfg->res_window.X = tbl["Video"]["res_window"][0].value_or<unsigned short>(640);
    cfg->res_window.Y = tbl["Video"]["res_window"][1].value_or<unsigned short>(480);
    cfg->res_menu.X = tbl["Video"]["res_menu"][0].value_or<unsigned short>(0);
    cfg->res_menu.Y = tbl["Video"]["res_menu"][1].value_or<unsigned short>(0);
    cfg->res_background.X = tbl["Video"]["res_background"][0].value_or<unsigned short>(512);
    cfg->res_background.Y = tbl["Video"]["res_background"][1].value_or<unsigned short>(512);
    cfg->play_opening_movie = tbl["Video"]["play_opening_movie"].value_or<bool>(false);
}

int FFXI::Config::RuntimeConfig::Initialize(std::filesystem::path config_file)
{
    if (std::filesystem::exists(config_file) == false) {
        return -1;
    }

    toml::table tbl;
    try
    {
        tbl = toml::parse_file(config_file.c_str());
        //Install dir is a required key
         toml::v3::optional<std::wstring> install_dir = tbl["Install"]["dir"].value<std::wstring>();
        if (install_dir.has_value() == false) {
            return -3;
        }

        std::filesystem::path dir = install_dir.value();

        if (dir.is_absolute() == true) {
            FFXI::Config::RuntimeConfig::instance.ffxi_install_path = dir;
        }
        else {
            FFXI::Config::RuntimeConfig::instance.ffxi_install_path = config_file.parent_path() / dir;
        }
    }
    catch (const toml::parse_error& err)
    {
        return -2;
    }
    
    ParseOptionalKeys(tbl);
    return 1;
}
