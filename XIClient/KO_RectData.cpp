#include "KO_RectData.h"

const FFXI::Math::KO_RectData FFXI::Math::KO_RectData::DataTbl[6] = 
{
	{0, 1, 2, 3, { 0.0, -1.0,  0.0}},
	{2, 1, 5, 6, { 1.0,  0.0,  0.0}},
	{3, 2, 6, 7, { 0.0,  0.0,  1.0}},
	{0, 3, 7, 4, {-1.0,  0.0,  0.0}},
	{1, 0, 4, 5, { 0.0,  0.0, -1.0}},
	{5, 4, 7, 6, { 0.0,  1.0,  0.0}},
};
bool FFXI::Math::KO_RectData::HitCheck(const D3DXVECTOR3* a1, const D3DXVECTOR3* a2, const KO_RectData* a3)
{
	D3DXVECTOR3 v67[8];
	v67[0] = { -0.5, -0.5, -0.5 };
	v67[1] = {  0.5, -0.5, -0.5 };
	v67[2] = {  0.5, -0.5,  0.5 };
	v67[3] = { -0.5, -0.5,  0.5 };
	v67[4] = { -0.5,  0.5, -0.5 };
	v67[5] = {  0.5,  0.5, -0.5 };
	v67[6] = {  0.5,  0.5,  0.5 };
	v67[7] = { -0.5,  0.5,  0.5 };

	float scale = a3->field_10.y * a2->y + a3->field_10.x * a2->x + a2->z * a3->field_10.z;
	if (scale == 0.0) {
		return false;
	}

	D3DXVECTOR3* one = v67 + a3->field_0;
	D3DXVECTOR3* two = v67 + a3->field_4;
	D3DXVECTOR3* thr = v67 + a3->field_8;
	D3DXVECTOR3* fou = v67 + a3->field_C;

	float dotone = a1->z * a3->field_10.z + a3->field_10.x * a1->x + a1->y * a3->field_10.y;
	float dottwo = one->y * a3->field_10.y + one->z * a3->field_10.z + a3->field_10.x * one->x;
	float v39 = (dotone - dottwo) * (-1.0 / scale);

	if (v39 < 0.0) {
		return false;
	}

	if (v39 >= 1.0) {
		return false;
	}

	float v58 = v39 * a2->x + a1->x;
	float v59 = v39 * a2->y + a1->y;
	float v60 = v39 * a2->z + a1->z;

	float v18 = two->x - one->x;
	float v19 = two->y - one->y;
	float v20 = two->z - one->z;

	float v40 = v58 - one->x;
	float v63 = v59 - one->y;
	float v22 = v60 - one->z;

	float v50 = (v22 * v19 - v63 * v20) * a3->field_10.x;
	float v51 = (v40 * v20 - v22 * v18) * a3->field_10.y;
	float v23 = (v18 * v63 - v40 * v19) * a3->field_10.z;
	if (v50 < -0.0001) {
		return false;
	}
	if (v51 < -0.0001) {
		return false;
	}
	if (v23 < -0.0001) {
		return false;
	}

	float v41 = thr->x - two->x;
	float v44 = thr->y - two->y;
	float v47 = thr->z - two->z;

	float v33 = v58 - two->x;
	float v35 = v59 - two->y;
	float v26 = v60 - two->z;

	float v99 = (v26 * v44 - v35 * v47) * a3->field_10.x;
	float v52 = (v33 * v47 - v26 * v41) * a3->field_10.y;
	float v55 = (v35 * v41 - v33 * v44) * a3->field_10.z;
	if (v99 < -0.0001) {
		return false;
	}
	if (v52 < -0.0001) {
		return false;
	}
	if (v55 < -0.0001) {
		return false;
	}

	float v42 = fou->x - thr->x;
	float v45 = fou->y - thr->y;
	float v48 = fou->z - thr->z;
	
	float v98 = (v22 * v45 - v63 * v48) * a3->field_10.x;
	float v53 = (v40 * v48 - v22 * v42) * a3->field_10.y;
	float v56 = (v63 * v42 - v40 * v45) * a3->field_10.z;

	if (v98 < -0.0001) {
		return false;
	}
	if (v53 < -0.0001) {
		return false;
	}
	if (v56 < -0.0001) {
		return false;
	}

	float v43 = one->x - fou->x;
	float v46 = one->y - fou->y;
	float v49 = one->z - fou->z;

	float v34 = v58 - fou->x;
	float v36 = v59 - fou->y;
	float v31 = v60 - fou->z;

	float v97 = (v31 * v46 - v36 * v49) * a3->field_10.x;
	float v54 = (v34 * v49 - v31 * v43) * a3->field_10.y;
	float v57 = (v36 * v43 - v34 * v46) * a3->field_10.z;

	if (v97 < -0.0001) {
		return false;
	}
	if (v54 < -0.0001) {
		return false;
	}
	if (v57 < -0.0001) {
		return false;
	}

	return true;
}
