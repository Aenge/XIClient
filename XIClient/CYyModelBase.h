#pragma once
#define WIN32_LEAN_AND_MEAN
#include "MemoryManagedObject.h"
#include "CYySkl.h"
#include "CYyMotionMan.h"
#include "WMatrix.h"
#include "d3dx9math.h"
namespace FFXI {
	namespace CYy {
		class CMoSk2;
		class CMoOs2;
		class CYyModelDt;
		class CXiSkeletonActor;
		class CXiActor;
		class CYyModel;
		class CYyTex;
		class CYyModelBase : public MemoryManagedObject {
		private:
			void AddDt(CYyModelDt*);
		public:
			static const BaseGameObject::ClassInfo CYyModelBaseClass;
			virtual const BaseGameObject::ClassInfo* GetRuntimeClass() override final;
			CYyModelBase();
			virtual ~CYyModelBase();
			void SetSkeleton(CMoSk2**);
			void Init();
			CYyModelDt** GetModelDt();
			int GetStaticBoneTransformBoneIndex(unsigned int);
			void LinkOs2(CMoOs2**);
			bool DoingSomething(CXiActor*, FFXI::Math::WMatrix*, float, D3DXVECTOR4*, int);
			void AddStaticBoneTransformTranslation(int, D3DXVECTOR3*);
			void GetBoneWorldPosition(unsigned int, D3DXVECTOR4*);
			void Draw(CXiActor*, CYyModel*, float);
			void MaybeUpdateAnims(CXiSkeletonActor*);
			void UpdateDTs(CYyModel*, int);
			CYyModelBase* Previous;
			CYySkl Skeleton;
			CYyModelDt* ModelDt;
			CYyMotionMan field_24;
			D3DXVECTOR4 field_B0;
			int field_C0;
			CYyTex* field_C4;
		};
	}
}