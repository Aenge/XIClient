#pragma once
#include "CMoResource.h"
#include "KzFQuat.h"
namespace FFXI {
	namespace CYy {
		class BoneTransformState;
		class CMoMo2 : public CMoResource {
		private:
			unsigned char Data[12];
		public:
			struct BoneTrack;
			CMoMo2();
			virtual ~CMoMo2() = default;
			virtual void Open() override final;
			virtual void Close() override final;
			void CalcMotionData(int, float, float, BoneTransformState*);
			bool ApplyBaseAnimation(int, int, unsigned int, float, BoneTransformState*);
			bool BlendOne(int, int, unsigned int, float, float, BoneTransformState*);

			unsigned char GetVersion();
			bool HasBeenOpened();
			unsigned short GetTrackCount();
			unsigned short GetFrameCount();
			float GetTiming();
			BoneTrack* GetBoneTrack(unsigned short trackIndex);

			#pragma pack(push, 1)
			struct BoneTrack {
				unsigned int BoneIndex;
				unsigned int Something;
				Math::KzFQuat Rotation;
				D3DXVECTOR3 Position;
				D3DXVECTOR3 Scaling;
				char Unused[36];
			};
			#pragma pack(pop)
		};
	}
}