#include "CYyUpperMemory.h"

using namespace FFXI::CYy;

const BaseGameObject::ClassInfo CYyUpperMemory::CYyUpperMemoryClass{
	"CYyUpperMemory", sizeof(CYyUpperMemory), &BaseGameObject::BaseGameObjectClass
};

const BaseGameObject::ClassInfo* CYyUpperMemory::GetRuntimeClass() {
	return &CYyUpperMemoryClass;
}

