#pragma once

namespace FFXI {
	namespace CYy {
		class CMoResource;
		/**
		 * This class manages the reference counting and linked list connections for resources. It maintains
		 * multiple types of linked lists to allow traversal through resources by different criteria:
		 * 1. A main double-linked list of all resources
		 * 2. A type-specific linked list of resources of the same type
		 * 3. Reference counting for memory management and usage tracking
		 */
		class ResourceMetadata {
		public:
			//Sets all pointers to nullptr and zeroes counters.
			void Initialize();

			/**
			* Links this resource to the next resource in the main list.
			* @param nextResource Pointer to the next resource to link to.
			*/
			void LinkToNext(CMoResource* nextResource);

			/**
			* Links this resource to the previous resource in the main list.
			* @param prevResource Pointer to the previous resource to link to.
			*/
			void LinkToPrevious(CMoResource* prevResource);

			// Main resource list
			CMoResource** PreviousResource;
			CMoResource** NextResource;

			// Type-specific list
			CMoResource** NextResourceOfSameType;
			CMoResource** PreviousResourceOfSameType;

			CMoResource** SelfReference;

			//Bits 0-14 are the actual reference count
			//Bit 15 is a flag used to control resource unloading (Just 0 reference count isnt good enough, this bit must also be 0)
			unsigned short MemoryReferenceCount;

			unsigned short StateFlags;
			int ActiveDependencyCount;
		};
	}
}