#pragma once
#include "CTkMenuPrimitive.h"
#include "CTkObList.h"
#include "TkRect.h"

namespace FFXI {
	namespace CTk {
		class CTkDrawMessageWindow;
		class CTkNode;
		class CTkMsgBase : public CTkMenuPrimitive
		{
		public:
			virtual ~CTkMsgBase() = default;
			virtual void OnInitialUpdatePrimitive() override;
			virtual void OnDestroyPrimitive() override;
			virtual void UpdateData();
			CTkMsgBase() = delete;
			CTkMsgBase(CTkDrawMessageWindow*);
			void EnableDraw();
			void DisableDraw();
			void RemoveAllMsg();
			void PresetEventMessageMode(char, short, short);
			void SpoolMsg(char*, char, char);
			CTkObList field_14{};
			TKRECT field_30{};
			TKRECT field_38{};
			int field_40{};
			int field_44{};
			char field_48{};
			char field_49{};
			short field_4A{};
			short field_4C{};
			short field_4E{};
			int field_50{};
			short field_54{};
			char field_56{};
			char field_57{};
			CTkNode* field_58{};
			short field_5C{};
			short field_5E{};
			short field_60{};
			char field_62{};
			char field_63{};
			CTkDrawMessageWindow* field_64{};
		};

	}
}