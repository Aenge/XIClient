#define WIN32_LEAN_AND_MEAN
#include "MemoryPoolManager.h"
#include "CYyLowerMemory.h"
#include "CYyUpperMemory.h"
#include "CYyElemMemory.h"
#include "CYyWorkMemory.h"
#include "CYyMenuMemory.h"
#include "CYyExMemory.h"
#include "CYyLoadMemory.h"
#include <iostream>
#include <Windows.h>

using namespace FFXI;

MemoryPoolManager* MemoryPoolManager::globalInstance{ nullptr };
int MemoryPoolManager::CurrentZoneId{ 0 };

CYy::MemoryBlockDescriptor* MemoryPoolManager::g_pCYyDataHead{ nullptr };
CYy::MemoryBlockDescriptor* MemoryPoolManager::g_pCYyDataTail{ nullptr };
CYy::MemoryBlockDescriptor* MemoryPoolManager::g_pCYyElemHead{ nullptr };
CYy::MemoryBlockDescriptor* MemoryPoolManager::g_pCYyElemTail{ nullptr };
CYy::MemoryBlockDescriptor* MemoryPoolManager::g_pCYyLoadHead{ nullptr };
CYy::MemoryBlockDescriptor* MemoryPoolManager::g_pCYyLoadTail{ nullptr };
CYy::MemoryBlockDescriptor* MemoryPoolManager::g_pCYyMenuHead{ nullptr };
CYy::MemoryBlockDescriptor* MemoryPoolManager::g_pCYyMenuTail{ nullptr };
CYy::MemoryBlockDescriptor* MemoryPoolManager::g_pCYyExHead{ nullptr };
CYy::MemoryBlockDescriptor* MemoryPoolManager::g_pCYyExTail{ nullptr };

//LOCAL FUNCS
int FindLargestSlot(CYy::MemoryBlockDescriptor* p_head, CYy::MemoryBlockDescriptor* p_tail) {
	int size = 0;
	do {
		if (!p_head->occupied && size < p_head->GetSize())
			size = p_head->GetSize();
		p_head = p_head->NextFree;
	} while (p_head != p_tail);
	return size;
}
bool FFXI::MemoryPoolManager::Initialize()
{
	MemoryPoolManager::globalInstance = new MemoryPoolManager();
	if (MemoryPoolManager::globalInstance == nullptr) {
		return false;
	}
	return MemoryPoolManager::globalInstance->Init();
}
void FFXI::MemoryPoolManager::Uninitialize()
{
	if (MemoryPoolManager::globalInstance != nullptr) {
		delete MemoryPoolManager::globalInstance;
		MemoryPoolManager::globalInstance = nullptr;
	}
}
//~LOCAL FUNCS
MemoryPoolManager::~MemoryPoolManager() {
	if (this->AllocatedMemoryBlock) {
		delete[] this->AllocatedMemoryBlock;
		this->AllocatedMemoryBlock = nullptr;
	}
}

CYy::MemoryBlockDescriptor* AllocateLinkedList(char* Buffer, int Size, CYy::MemoryBlockDescriptor** Head, CYy::MemoryBlockDescriptor** Tail) {
	CYy::MemoryBlockDescriptor* head; // eax
	int* v5; // ebx
	CYy::MemoryBlockDescriptor* tail; // edi
	unsigned int v9;

	*Head = (CYy::MemoryBlockDescriptor*)Buffer;
	*Tail = (CYy::MemoryBlockDescriptor*)(Buffer + (Size & 0xFFFFFFF0) - 16);
	head = *Head;
	head->PreviousOccupied = nullptr;
	tail = *Tail;
	head->PreviousFree = nullptr;
	head->NextOccupied = tail;
	head->NextFree = tail;
	head->occupied = false;
	head->MemoryFlags &= 0xFFFFFFFC;
	head->ZoneId = 0;
	tail->PreviousOccupied = head;
	tail->NextOccupied = nullptr;
	tail->NextFree = nullptr;
	tail->PreviousFree = head;
	tail->ZoneId = 0;
	tail->occupied = true;
	tail->MemoryFlags &= 0xFFFFFFFC;
	return tail;
}
bool MemoryPoolManager::Init() {
	unsigned int v2; // ecx
	signed __int32 v3; // eax
	bool v4; // cc
	int v5; // edi
	int v6; // eax
	char* v7; // eax
	char* v8; // eax
	char* v10; // eax
	char* v11;
	MEMORYSTATUS Buffer{};
	memset(&Buffer, 0, sizeof(Buffer));
	Buffer.dwLength = 32;
	GlobalMemoryStatus(&Buffer);
	v2 = 0x5A02000;
	v3 = Buffer.dwTotalPhys >> 20;
	v4 = Buffer.dwTotalPhys >> 20 <= 0x80;
	this->SystemMemoryMB = Buffer.dwTotalPhys >> 20;
	v5 = 0x4000000;
	if (!v4)
	{
		if (v3 > 256)
			this->SystemMemoryMB = 256;
		v6 = (this->SystemMemoryMB - 128) << 20;
		v5 = v6 + 0x4000000;
		v2 = v6 + 0x5A02000;
	}
	v7 = new char[v2];
	this->AllocatedMemoryBlock = v7;
	if (!v7)
	{
		v2 = 0x5A02000u;
		v5 = 0x4000000;
		this->AllocatedMemoryBlock = new char[0x5A02000u];
	}
	v8 = this->AllocatedMemoryBlock;
	if (!v8)
		return false;
	memset(v8, 0, v2);
	v10 = v8 + 15;
	int ok = (int)v10;
	ok &= 0xFFFFFFF0;
	v10 = reinterpret_cast<char*>(ok);
	this->AlignedMemoryStart = v10;
	v11 = v10 + v5;

	AllocateLinkedList(v10, v5 - 128, &g_pCYyDataHead, &g_pCYyDataTail);
	AllocateLinkedList(v11, 0x9FFF80, &g_pCYyElemHead, &g_pCYyElemTail);
	AllocateLinkedList(v11 + 0xA00000, 0xF80, &g_pCYyLoadHead, &g_pCYyLoadTail);
	AllocateLinkedList(v11 + 0xA01000, 0xFFFF80, &g_pCYyExHead, &g_pCYyExTail);
	AllocateLinkedList(v11 + 0x1A01000, 0xF80, &g_pCYyMenuHead, &g_pCYyMenuTail);

	return true;
}

CYy::MemoryBlockDescriptor* GetFromHead(int p_size, CYy::MemoryBlockDescriptor* p_head, CYy::MemoryBlockDescriptor* p_tail) {
	CYy::MemoryBlockDescriptor* head = p_head;
	CYy::MemoryBlockDescriptor* tail = p_tail;
	int size = p_size + 15;
	size &= 0xFFFFFFF0;
	if (size < 32) size = 32;
	while (head->occupied || head->GetSize() < (size + 32)) {
		head = head->NextFree;
		if (head == tail) return 0;
	}
	CYy::MemoryBlockDescriptor* NextEntry = head->NextOccupied;
	CYy::MemoryBlockDescriptor* NextFreeByteLocation = reinterpret_cast<CYy::MemoryBlockDescriptor*>(reinterpret_cast<char*>(head) + size + 32);
	head->ZoneId = FFXI::MemoryPoolManager::CurrentZoneId;
	head->occupied = true;
	head->MemoryFlags &= 0xFFFFFFFC;
	if (NextEntry == NextFreeByteLocation) {
		throw "NEXT ENTRY EQUALS NEXT FREEBYTE";
	} 
	else {
		NextFreeByteLocation->occupied = 0;
		NextFreeByteLocation->NextOccupied = head->NextOccupied;
		NextFreeByteLocation->PreviousOccupied = head;
		NextEntry->PreviousOccupied = NextFreeByteLocation;
		CYy::MemoryBlockDescriptor* v10 = head->NextFree;
		head->NextOccupied = NextFreeByteLocation;
		NextFreeByteLocation->NextFree = v10;
		v10->PreviousFree = NextFreeByteLocation;
		CYy::MemoryBlockDescriptor* v11 = head->PreviousFree;
		if (v11) {
			NextFreeByteLocation->PreviousFree = v11;
			v11->NextFree = NextFreeByteLocation;
		}
		else {
			NextFreeByteLocation->PreviousFree = head;
			head->NextFree = NextFreeByteLocation;
		}
		return head;
	}
}

CYy::MemoryBlockDescriptor* GetFromTail(int p_size, CYy::MemoryBlockDescriptor* p_head, CYy::MemoryBlockDescriptor* p_tail) {
	CYy::MemoryBlockDescriptor* tail = p_tail;

	int size = p_size + 15;
	size &= 0xFFFFFFF0;
	if (size < 32) size = 32;
	do {
		if (!tail->PreviousOccupied) 
			return nullptr;
		tail = tail->PreviousFree;
	} while (tail->occupied || tail->GetSize() < (size + 32));
	
	CYy::MemoryBlockDescriptor* NextEntry = tail->NextOccupied;
	CYy::MemoryBlockDescriptor* FreeSpot = (CYy::MemoryBlockDescriptor*)((char*)NextEntry - size - 32);
	FreeSpot->ZoneId = FFXI::MemoryPoolManager::CurrentZoneId;
	if (FreeSpot == tail) {
		if (tail->PreviousFree) {
			tail->PreviousFree->NextFree = tail->NextFree;
			tail->NextFree->PreviousFree = tail->PreviousFree;
		}

		tail->occupied = true;
		tail->MemoryFlags &= 0xFFFFFFFC;
		return tail;
	}
	else {
		FreeSpot->NextOccupied = NextEntry;
		FreeSpot->PreviousOccupied = tail;
		NextEntry->PreviousOccupied = FreeSpot;
		tail->NextOccupied = FreeSpot;
		FreeSpot->occupied = true;
		FreeSpot->MemoryFlags &= 0xFFFFFFFC;
		return FreeSpot;
	}
}

char* MemoryPoolManager::Get(int p_size, MemoryPoolManager::MemoryPoolType p_mode) {
	CYy::MemoryBlockDescriptor* header{ nullptr };
	//sub //TODO
	//don't copy vtable.. create a new base class with only vtable
	switch (p_mode) {
	case MemoryPoolManager::MemoryPoolType::Upper:
		header = GetFromHead(p_size + sizeof(CYy::MemoryBlockDescriptor), g_pCYyDataHead, g_pCYyDataTail);
		if (header) {
			CYy::CYyUpperMemory v1{};
			memcpy(header, &v1, 4);
			return (char*)(header + 1);
		}
		break;
	case MemoryPoolManager::MemoryPoolType::Lower:
		header = GetFromTail(p_size + sizeof(CYy::MemoryBlockDescriptor), g_pCYyDataHead, g_pCYyDataTail);
		if (header) {
			CYy::CYyLowerMemory v1{};
			memcpy(header, &v1, 4);
			return (char*)(header + 1);
		}
		break;
	case MemoryPoolManager::MemoryPoolType::Elem:
		header = GetFromTail(p_size + sizeof(CYy::MemoryBlockDescriptor), g_pCYyElemHead, g_pCYyElemTail);
		if (header) {
			CYy::CYyElemMemory v1{};
			memcpy(header, &v1, 4);
			return (char*)(header + 1);
		}
		break;
	case MemoryPoolManager::MemoryPoolType::Work:
		header = GetFromTail(p_size + sizeof(CYy::MemoryBlockDescriptor), g_pCYyElemHead, g_pCYyElemTail);
		if (header) {
			CYy::CYyWorkMemory v1{};
			memcpy(header, &v1, 4);
			return (char*)(header + 1);
		}
		break;
	case MemoryPoolManager::MemoryPoolType::Load:
		header = GetFromTail(p_size + sizeof(CYy::MemoryBlockDescriptor), g_pCYyLoadHead, g_pCYyLoadTail);
		if (header) {
			CYy::CYyLoadMemory v1{};
			memcpy(header, &v1, 4);
			return (char*)(header + 1);
		}
		break;
	case MemoryPoolManager::MemoryPoolType::Menu:
		header = GetFromTail(p_size + sizeof(CYy::MemoryBlockDescriptor), g_pCYyMenuHead, g_pCYyMenuTail);
		if (header) {
			CYy::CYyMenuMemory v1{};
			memcpy(header, &v1, 4);
			return (char*)(header + 1);
		}
		break;
	default:
		//std::cout << std::hex << "EX ALLOCATE: " << p_size + 32 << " / " << ((FFXI::Object::MemoryBlockDescriptor*)Globals::g_pCYyExTail)->GetFreeSize() << std::endl;
		header = GetFromTail(p_size + sizeof(CYy::MemoryBlockDescriptor), g_pCYyExHead, g_pCYyExTail);
		if (!header) break;
		CYy::CYyExMemory v1{};
		memcpy(header, &v1, 4);
		return (char*)(header + 1);
	}
	exit(2002);
}

char* FFXI::MemoryPoolManager::GetOrUpper(int p_size, MemoryPoolManager::MemoryPoolType p_mode)
{
	char* result = this->Get(p_size, p_mode);
	if (result || p_mode != MemoryPoolManager::MemoryPoolType::Elem && p_mode != MemoryPoolManager::MemoryPoolType::Work)
		return result;
	result = this->Get(p_size, MemoryPoolManager::MemoryPoolType::Upper);
	if (!result)
		exit(2002);
	return result;
}

int FFXI::MemoryPoolManager::FindLargestOpenSlot(MemoryPoolManager::MemoryPoolType p_mode)
{
	CYy::MemoryBlockDescriptor* head{ nullptr }, * tail{ nullptr };
	switch (p_mode) {
	case MemoryPoolManager::MemoryPoolType::Upper:
	case MemoryPoolManager::MemoryPoolType::Lower:
		head = reinterpret_cast<CYy::MemoryBlockDescriptor*>(g_pCYyDataHead);
		tail = reinterpret_cast<CYy::MemoryBlockDescriptor*>(g_pCYyDataTail);
		break;
	case MemoryPoolManager::MemoryPoolType::Elem:
	case MemoryPoolManager::MemoryPoolType::Work:
		head = reinterpret_cast<CYy::MemoryBlockDescriptor*>(g_pCYyElemHead);
		tail = reinterpret_cast<CYy::MemoryBlockDescriptor*>(g_pCYyElemTail);
		break;
	case MemoryPoolManager::MemoryPoolType::Load:
		head = reinterpret_cast<CYy::MemoryBlockDescriptor*>(g_pCYyLoadHead);
		tail = reinterpret_cast<CYy::MemoryBlockDescriptor*>(g_pCYyLoadTail);
		break;
	case MemoryPoolManager::MemoryPoolType::Menu:
		head = reinterpret_cast<CYy::MemoryBlockDescriptor*>(g_pCYyMenuHead);
		tail = reinterpret_cast<CYy::MemoryBlockDescriptor*>(g_pCYyMenuTail);
		break;
	default:
		head = reinterpret_cast<CYy::MemoryBlockDescriptor*>(g_pCYyExHead);
		tail = reinterpret_cast<CYy::MemoryBlockDescriptor*>(g_pCYyExTail);
	}
	return FindLargestSlot(head, tail);
}

void FFXI::MemoryPoolManager::Delete(CYy::BaseGameObject* p_obj)
{
	CYy::MemoryBlockDescriptor* header = (CYy::MemoryBlockDescriptor*)((char*)p_obj - sizeof(CYy::MemoryBlockDescriptor));
	if (header->occupied == false) return;

	header->occupied = false;

	CYy::MemoryBlockDescriptor* v1 = header;
	CYy::MemoryBlockDescriptor* v2 = header->PreviousOccupied;
	if (v2 && !v2->occupied) {
		v2->NextOccupied = header->NextOccupied;
		header->NextOccupied->PreviousOccupied = v2;
		v1 = v2;
	}
	CYy::MemoryBlockDescriptor* v3 = v1->NextOccupied;
	if (!v3->occupied) {
		v1->NextOccupied = v3->NextOccupied;
		v3->NextOccupied->PreviousOccupied = v1;
	}
	CYy::MemoryBlockDescriptor* v4 = v1->PreviousOccupied;
	CYy::MemoryBlockDescriptor* v5 = v1;
	if (v4) {
		while (v4->occupied) {
			v5 = v4;
			v4 = v4->PreviousOccupied;
			if (!v4)
				goto LABEL_12;
		}
		v5 = v5->PreviousOccupied;
	}
LABEL_12:
	if (v5->PreviousOccupied || v1->PreviousOccupied) {
		v5->NextFree = v1;
		v1->PreviousFree = v5;
	}
	CYy::MemoryBlockDescriptor* v6 = v1->NextOccupied;
	CYy::MemoryBlockDescriptor* v7 = v1;
	if (v6) {
		while (v6->occupied) {
			v7 = v6;
			v6 = v6->NextOccupied;
			if (!v6)
				goto LABEL_20;
		}
		v7 = v7->NextOccupied;
	}
LABEL_20:
	if (v7->NextOccupied || v1->NextOccupied) {
		v7->PreviousFree = v1;
		v1->NextFree = v7;
	}
}
void CleanList(int* a1, FFXI::CYy::MemoryBlockDescriptor* a2, FFXI::CYy::MemoryBlockDescriptor* a3) {
	while (a2 != a3) {
		if (a2->occupied) {
			if ((a2->MemoryFlags & 2) == 0) {
				CYy::BaseGameObject* obj = (CYy::BaseGameObject*)(a2 + 1);
				obj->VObj2(a1);
			}
		}
		a2 = a2->NextOccupied;
	}
}
void FFXI::MemoryPoolManager::RunClean(int* a2)
{
	CleanList(a2, g_pCYyExHead, g_pCYyExTail);
	CleanList(a2, g_pCYyLoadHead, g_pCYyLoadTail);
	CleanList(a2, g_pCYyElemHead, g_pCYyElemTail);
	CleanList(a2, g_pCYyDataHead, g_pCYyDataTail);
}

void FFXI::MemoryPoolManager::Clean123()
{
	if (MemoryPoolManager::globalInstance == nullptr) {
		return;
	}

	for (int i = 1; i < 4; ++i) {
		MemoryPoolManager::globalInstance->RunClean(&i);
	}
}

void FFXI::MemoryPoolManager::Clean456()
{
	if (MemoryPoolManager::globalInstance == nullptr) {
		return;
	}

	for (int i = 4; i < 7; ++i) {
		MemoryPoolManager::globalInstance->RunClean(&i);
	}
}

void* FFXI::MemoryPoolManager::Wrap(int size, MemoryPoolManager::MemoryPoolType a2)
{
	char* mem = MemoryPoolManager::globalInstance->Get(size + sizeof(CYy::BaseGameObject), a2);
	if (!mem)
		return nullptr;

	CYy::BaseGameObject* obj = new (mem) CYy::BaseGameObject();
	return mem + sizeof(CYy::BaseGameObject);
}

void FFXI::MemoryPoolManager::Unwrap(void* a2)
{
	char* obj = (char*)a2 - sizeof(CYy::BaseGameObject);
	MemoryPoolManager::globalInstance->Delete((CYy::BaseGameObject*)obj);
}

void* FFXI::MemoryPoolManager::WrapPlus12(int a1, MemoryPoolManager::MemoryPoolType a2)
{
	return (char*)Wrap(a1 + 12, a2) + 12;
}

void FFXI::MemoryPoolManager::UnwrapPlus12(void* a1)
{
	Unwrap((char*)a1 - 12);
}