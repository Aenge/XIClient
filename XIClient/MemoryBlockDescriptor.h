#pragma once
#include "BaseGameObject.h"
namespace FFXI {
	namespace CYy {
		class MemoryBlockDescriptor : public BaseGameObject{
		public:
			bool occupied { false };
			MemoryBlockDescriptor* NextOccupied{ nullptr };
			MemoryBlockDescriptor* PreviousOccupied{ nullptr };
			MemoryBlockDescriptor* NextFree{ nullptr };
			MemoryBlockDescriptor* PreviousFree{ nullptr };
			int MemoryFlags{ 0 };
			int ZoneId{ 0 };
			int GetSize();
			int GetFreeSize();
			MemoryBlockDescriptor* Delete();
		};
	}
}