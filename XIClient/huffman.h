#pragma once

namespace FFXI {
	namespace Network {
		namespace Huffman {
			struct HuffmanTable {
				int values[256];
				int bitlens[256];
			};
			struct HuffmanTreeNode {
				HuffmanTreeNode* zero;
				HuffmanTreeNode* one;
				HuffmanTreeNode* field_8;
				int field_C;
				int field_10;
			};
			struct HuffmanTree {
				HuffmanTreeNode* head;
				HuffmanTreeNode nodes[511];
			};
			int newHuffmanTable(HuffmanTable*, int);
			void newHuffmanTree(HuffmanTree*);
			int huffman_packet_encoded_length(char*, unsigned int, HuffmanTable*);
			int huffman_packet_encode(char*, unsigned int, char*, unsigned int, HuffmanTable*);
			int huffman_packet_decode(char*, unsigned int, char*, unsigned int, HuffmanTree*);

			struct huff_buff_entry {
				unsigned char token;
				unsigned int value;
				unsigned int bits;
			};

			extern huff_buff_entry Huffman_Buffer[256];
		};
	}
}