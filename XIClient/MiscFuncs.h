#pragma once
#define WIN32_LEAN_AND_MEAN
#include "d3dx9math.h"
namespace FFXI {
	namespace Math {
		bool SomeCollisionTest(D3DXVECTOR3* a1, float* a2, float** a3) {
			//This function seems to take a strange struct rather than split a2 and a3
			float* v2 = a3[1];
			float* v3 = a3[0];
			float* v9 = a3[2];
			double v4 = v2[0] - v3[0];
			double v5 = v2[1] - v3[1];
			double v6 = v2[2] - v3[2];
			float v13 = a1->x - v3[0];
			float v16 = a1->y - v3[1];
			double v7 = a1->z - v3[2];
			float v27 = (v7 * v5 - v16 * v6) * a2[0];
			float v28 = (v13 * v6 - v7 * v4) * a2[1];
			float v8 = (v4 * v16 - v13 * v5) * a2[2];
			if (v27 < -0.000099999997 || v28 < -0.000099999997 || v8 < -0.000099999997) {
				return false;
			}

			float v21 = v9[0] - v2[0];
			float v23 = v9[1] - v2[1];
			float v25 = v9[2] - v2[2];
			float v14 = a1->x - v2[0];
			float v17 = a1->y - v2[1];
			double v10 = a1->z - v2[2];
			v27 = (v10 * v23 - v17 * v25) * a2[0];
			v28 = (v14 * v25 - v10 * v21) * a2[1];
			v8 = (v17 * v21 - v14 * v23) * a2[2];
			if (v27 < -0.000099999997 || v28 < -0.000099999997 || v8 < -0.000099999997) {
				return false;
			}
			float v22 = v3[0] - v9[0];
			float v24 = v3[1] - v9[1];
			float v26 = v3[2] - v9[2];
			float v15 = a1->x - v9[0];
			float v18 = a1->y - v9[1];
			double v11 = a1->z - v9[2];
			v27 = (v11 * v24 - v18 * v26) * a2[0];
			v28 = (v15 * v26 - v11 * v22) * a2[1];
			v8 = (v18 * v22 - v15 * v24) * a2[2];
			if (v27 < -0.000099999997 || v28 < -0.000099999997) {
				return false;
			}
			return v8 >= -0.0001;
		}
	}
}