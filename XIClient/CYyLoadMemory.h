#pragma once
#include "MemoryBlockDescriptor.h"

namespace FFXI {
	namespace CYy {
		class CYyLoadMemory : public MemoryBlockDescriptor {
		public:
			const static BaseGameObject::ClassInfo CYyLoadMemoryClass;
			virtual const BaseGameObject::ClassInfo* GetRuntimeClass() override final;
		};
	}
}