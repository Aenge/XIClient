#include "CAtelIdleTask.h"
#include "CYyDb.h"
#include "PlaceHolders.h"

const FFXI::CYy::BaseGameObject::ClassInfo FFXI::CYy::CAtelIdleTask::CAtelIdleTaskClass{
	"CAtelIdleTask", sizeof(CAtelIdleTask), &FFXI::CYy::CMoTask::CMoTaskClass
};

const FFXI::CYy::BaseGameObject::ClassInfo* FFXI::CYy::CAtelIdleTask::GetRuntimeClass()
{
	return &FFXI::CYy::CAtelIdleTask::CAtelIdleTaskClass;
}

char FFXI::CYy::CAtelIdleTask::OnMove()
{
	if (FFXI::CYyDb::g_pCYyDb->field_9 == 0) {
		return 0;
	}

	FFXI::CYyDb::AtelIdle();

	//TODO
	Placeholder::g_pYkMyroom;
	return 0;
}
