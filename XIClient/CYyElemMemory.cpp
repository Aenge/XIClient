#include "CYyElemMemory.h"

using namespace FFXI::CYy;

const BaseGameObject::ClassInfo CYyElemMemory::CYyElemMemoryClass{
	"CYyElemMemory", sizeof(CYyElemMemory), &BaseGameObject::BaseGameObjectClass
};

const BaseGameObject::ClassInfo* CYyElemMemory::GetRuntimeClass() {
	return &CYyElemMemoryClass;
}

