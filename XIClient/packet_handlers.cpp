#include "packet_handlers.h"
#include "CGcMainSys.h"
#include "CNtTimeSys.h"
#include "Values.h"
#include "GP_CLI_COMMAND.h"
#include "NT_SYS_BASE.h"
#include "ZoneNames.h"
float enDirNetToCli(unsigned char a1) 
{
    return (double)a1 * FFXI::Constants::Values::ANGLE_2PI / 256.0;
}

void FFXI::Zone::RecvCallbacks::RecvLogIn(FFXI::Zone::GC_ZONE* a1, FFXI::Network::UDP::GP_GAME_PACKET_HEAD* a2, FFXI::Network::UDP::GP_SERV_PACKETS::GP_SERV_LOGIN* a3)
{
    int field_42_backup = a3->field_42;
    if (a3->field_42 >= 0x100u) {
        a3->field_42 += 444;
    }

    FFXI::Network::CGcMainSys::set_map_connection_state(4);
    //xiinfo
    if (a1->field_C != 2) {
        FFXI::Network::CGcMainSys::PushRecvStatus(16);
        FFXI::Network::CGcMainSys::PushRecvStatus(16);
        //xiinfo
        return;
    }
    
    //TODO
    switch (a3->field_80) {
    case 1:
        //POLSTAT_OnlineStatGet
        //send packet 0xDC
        //TODO
        FFXI::Network::CGcMainSys::gcZoneFlgOrSet(0x001);
        FFXI::Network::CGcMainSys::gcZoneFlgOrSet(0x100);
        if (field_42_backup == Constants::Enums::ZoneNames::Mog_Garden || field_42_backup == Constants::Enums::ZoneNames::Feretory) {
            FFXI::Network::NT_SYS_BASE::ntSysChatPrintArea(a3);
        } 
        else {
            exit(0x100F80F6);
        }
        break;
    case 3:
        Network::CGcMainSys::gcZoneFlgOrSet(0x200);
        break;
    case 4:
        Network::CGcMainSys::gcZoneFlgOrSet(0x2000);
        Network::CGcMainSys::gcZoneFlgOrSet(0x100);
        break;
    case 5:
        Network::CGcMainSys::gcZoneFlgOrSet(0x200);
        Network::CGcMainSys::gcZoneFlgOrSet(0x100);
        break;
    default:
        //POLSTAT_OnlineStatGet
        //send packet 0xDC
        //TODO
        Network::CGcMainSys::gcZoneFlgOrSet(0x001);
        Network::NT_SYS_BASE::ntSysChatPrintArea(a3);
        break;
    }

    a1->field_C = 3;
    FFXI::Network::enQueBuff* buff = FFXI::Network::CGcMainSys::gcZoneSendQueSearch(FFXI::Constants::Enums::GP_CLI_COMMAND::LOGIN, true, 1);
    if (buff != nullptr) {
        if ((buff->field_0 & 0x1FF) == 10) {
            a3->field_0 &= 0xFE00u;
        }
    }

    FFXI::Network::CNtTimeSys::ntTimeSet(a3->field_38, a3->field_34);
    FFXI::Network::CNtTimeSys::ntGameTimeInit(a3->field_38, a3->field_3C);

    a1->field_3AFF6 = 0;
    a1->field_365DC = a3->field_4;
    a1->field_3AFF4 = a3->field_8;
    a1->field_3AFC8 = a3->field_42;
    a1->field_3AFCC = a3->field_AA;
    a1->field_3AFCE = ((a3->field_A8 & 0xFF) >> 1) & 3;
    a1->field_3659C = a3->field_30;
    a1->field_365A0 = a3->field_100;
    a1->field_365A4 = a3->field_9C;
    a1->field_365A6 = a3->field_9E;
    a1->field_3AFDC = a3->field_62;
    a1->field_3AFDE = a3->field_64;
    a1->field_3AFE0 = a3->field_66;
    a1->field_3AFDA = a3->field_40;
    a1->field_3B008 = a3->field_68;
    a1->field_3B00A = a3->field_6A;
    a1->field_3B014 = a3->field_74;
    a1->field_3B016 = a3->field_76;
    a1->field_3B00C = a3->field_6C;
    a1->field_3B010 = a3->field_70;
    a1->field_3B028 = a3->field_A4 / 60 + FFXI::Network::CNtTimeSys::ntGameTimeGet();
    strncpy_s(a1->field_365F0, a3->field_84, 0x10u);
    a1->field_365F0[15] = 0;
    a1->field_365AC = a3->field_C;
    a1->field_365B8 = 1.0;
    a1->field_365BC = a3->field_C;
    a1->field_365C8 = enDirNetToCli(a3->field_B);
    a1->field_365CC.x = 0;
    a1->field_365CC.y = enDirNetToCli(a3->field_B);
    a1->field_365CC.z = 0;
    //may be a homogoneous coord
    a1->field_365D8 = 1.0;

    unsigned short v11 = a1->field_3AFF8;
    unsigned short v12 = v11 ^ (v11 ^ ((a3->field_20 & 0xFFFF) << 10)) & 0x400;
    unsigned short v13 = v12 ^ (v12 ^ ((unsigned short)(a3->field_20 >> 1) << 11)) & 0x800;
    unsigned short v14 = v13 ^ (v13 ^ ((unsigned short)(a3->field_20 >> 2) << 12)) & 0x1000;
    unsigned short v15 = v14 ^ (v14 ^ ((unsigned short)((unsigned int)a3->field_18 >> 15) << 13)) & 0x2000;
    unsigned short v17 = v15 ^ ((unsigned char)v15 ^ ((a3->field_28 >> 8) & 0xFF)) & 7;
    unsigned short v30 = v17 ^ ((unsigned char)v17 ^ (unsigned char)((a3->field_24 < 0) << 7)) & 0x80;
    unsigned short v39 = v30 ^ ((unsigned char)v30 ^ (unsigned char)(8 * (a3->field_28 >> 1))) & 8;
    unsigned short v41 = ((a3->field_24 >> 29) << 8) & 0xFF00;
    unsigned short v42 = v39 ^ (v39 ^ v41) & 0x100;
    unsigned short v43 = v42 ^ (v42 ^ ((unsigned int)a3->field_24 >> 30 << 9)) & 0x200;
    unsigned short v44 = v43 & 0xFFBF ^ ((a3->field_AF & 1) << 6);
    unsigned short v45 = v44 ^ ((unsigned char)v44 ^ (unsigned char)(32 * (a3->field_28 >> 4))) & 0x20;
    unsigned short v47 = v45 ^ ((unsigned char)v45 ^ (unsigned char)(16 * (a3->field_28 >> 5))) & 0x10;
    a1->field_3AFF8 = v47;

    unsigned short v16 = a1->field_3AFFA;
    unsigned short v18 = v16 ^ ((unsigned char)v16 ^ (unsigned char)(a3->field_20 >> 5)) & 7;
    unsigned short v19 = v18 ^ ((unsigned char)v18 ^ (unsigned char)(8 * ((a3->field_20 >> 8) & 0xFF))) & 8;
    unsigned short v20 = v19 ^ ((unsigned char)v19 ^ (unsigned char)(16 * (a3->field_20 >> 9))) & 0x30;
    a1->field_3AFFA = (a3->field_1C << 6) ^ ((unsigned char)(a3->field_1C << 6) ^ (unsigned char)v20) & 0x3F;

    unsigned short affc = a1->field_3AFFC;
    unsigned short v21 = affc ^ ((unsigned char)affc ^ (unsigned char)((unsigned int)a3->field_18 >> 13)) & 1;
    unsigned short v22 = v21 ^ ((unsigned char)v21 ^ (unsigned char)(2 * ((unsigned int)a3->field_18 >> 14))) & 2;
    unsigned short v23 = v22 ^ ((unsigned char)v22 ^ (unsigned char)(4 * (a3->field_20 >> 11))) & 4;
    unsigned short v24 = v23 ^ ((unsigned char)v23 ^ (unsigned char)(8 * (a3->field_20 >> 12))) & 8;
    unsigned short v25 = v24 ^ ((unsigned char)v24 ^ (unsigned char)(16 * (a3->field_20 >> 14))) & 0x10;
    unsigned short v26 = v25 ^ ((unsigned char)v25 ^ (unsigned char)(32 * (a3->field_20 >> 13))) & 0x20;
    unsigned short v27 = v26 ^ ((unsigned char)v26 ^ (unsigned char)(((a3->field_20 >> 16) & 0xFF) << 7)) & 0x80;
    v26 = ((a3->field_20 >> 17) << 8) & 0xFF00;
    unsigned short v28 = v27 ^ (v27 ^ v26) & 0x100;
    unsigned short v29 = v28 ^ (v28 ^ (a3->field_20 >> 18 << 9)) & 0x200;
    unsigned short v31 = v29 ^ ((unsigned char)v29 ^ (unsigned char)((unsigned char)(a3->field_20 >> 15) << 6)) & 0x40;
    unsigned short v32 = v31 ^ (v31 ^ (a3->field_20 >> 19 << 10)) & 0x400;
    unsigned short v33 = v32 ^ (v32 ^ (a3->field_20 >> 20 << 11)) & 0x800;
    unsigned short v34 = (((a3->field_20 >> 24) & 0xFF) << 13) ^ ((((a3->field_20 >> 24) & 0xFF) << 13) ^ v33) & 0x1FFF;
    a1->field_3AFFC = v34 & 0xEFFF ^ ((a3->field_A8 & 1) << 12);

    unsigned short affe = a1->field_3AFFE;
    unsigned short v35 = affe ^ ((unsigned char)affe ^ (a3->field_20 >> 28)) & 1;
    unsigned short v36 = v35 ^ ((unsigned char)v35 ^ (unsigned char)(2 * (a3->field_20 >> 29))) & 2;
    unsigned short v37 = v36 ^ ((unsigned char)v36 ^ (unsigned char)(8 * ((a3->field_20 & 0x80000000) != 0))) & 8;
    unsigned short v38 = v37 ^ ((unsigned char)v37 ^ (unsigned char)(16 * (a3->field_24 >> 25))) & 0x10;
    unsigned short v40 = v38 ^ ((unsigned char)v38 ^ (unsigned char)(32 * (a3->field_24 >> 26))) & 0x20;
    v40 &= 0x00FF;
    v40 |= (((a3->field_28 >> 19) & 3) << 8) & 0xFF00;
    a1->field_3AFFE = v40 ^ ((unsigned char)v40 ^ (unsigned char)(a3->field_24 >> 28 << 7)) & 0x80;

    a1->field_3B030 = a3->field_1D;
    a1->field_3B020 = a3->field_2C;
    a1->field_3B006 = a3->gameStatus;
    a1->field_3B007 = a3->field_1E;
    a1->field_3B02C = a3->field_24;
    a1->field_3B02D = (a3->field_24 >> 8) & 0xFF;
    a1->field_3B02E = (a3->field_24 >> 16) & 0xFF;
    a1->field_3B02F = (a3->field_20 & 0x8000000) != 0;

    a1->field_3AFCA = a3->field_60;
    a1->field_3B018 = a3->field_78;
    a1->field_3B01C = a3->field_78 + a3->field_7C;
    a1->field_278C8 = a3->field_A0;
    a1->field_278CC = FFXI::Network::CNtTimeSys::ntGameTimeGet();
    a1->field_3B032 = (a3->field_A8 >> 8) & 0xFF;
    a1->field_3B031 = 0;
    a1->field_3B034 = a3->field_AC;
    a1->field_3B000 = a3->field_AE;
    a1->field_3B001 ^= (a1->field_3B001 ^ ((a3->field_A8 & 0xFF) >> 2)) & 0xE;
    a1->field_3B002 = a3->field_7E;

    memcpy(a1->field_3AFD0, a3->field_56, sizeof(a1->field_3AFD0));
    memcpy(&a1->ModelIDs, &a3->ModelIDs, sizeof(a1->ModelIDs));

    a1->field_3B038 = 0;
    a1->field_8 = 0;
    a1->field_A = 0;
    a1->field_4194 = 0;
    a1->field_4196 = 0;
    a1->field_4198 = 0;
    a1->field_419A = 0;
    a1->field_419C = 0;
    a1->field_41A0 = 0;
    a1->field_41A4 = 0;
    a1->field_3B074 = 0;
    a1->field_4164 = a3->field_94;
    a1->field_4168 = a3->field_98;

    memset(a1->myRoomSomething2, 0, sizeof(a1->myRoomSomething2));
    memcpy(a1->myRoomSomething1, a3->field_B0, sizeof(a1->myRoomSomething1));
    a1->myRoomSomething2[0] = a3->field_B0[3];
    a1->myRoomSomething2[1] = a3->field_B0[4];
    a1->myRoomSomething2[2] = a3->field_B0[5];
    a1->myRoomSomething2[3] = a3->field_B0[6];
    a1->field_3B001 &= ~1u;
    a1->field_2728C = a3->field_F4;
    a1->field_27290 = a3->field_F8;
    a1->field_27294 = a3->field_FC;
    //infowrite
    FFXI::Network::CGcMainSys::PushRecvStatus(32);
}

void FFXI::Zone::RecvCallbacks::RecvEquipClear(FFXI::Zone::GC_ZONE* a1, FFXI::Network::UDP::GP_GAME_PACKET_HEAD* a2, FFXI::Network::UDP::GP_SERV_PACKETS::GP_SERV_EQUIP_CLEAR* a3) 
{
    for (int i = 0; i < FFXI::Constants::Enums::EquipmentSlot::MAX; ++i) {
        a1->gcEquip.slots[i].field_0 = i;
        a1->gcEquip.slots[i].field_4 = 0;
    }

    //todo callback
}