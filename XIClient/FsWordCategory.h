#pragma once
#include "FsWordEntry.h"
namespace FFXI {
	namespace Input {
		class FsWordCategory : public FsWordEntry {
		public:
			virtual ~FsWordCategory();
			FsWordCategory();
			int field_18;
			int field_1C;
			int field_20;
			int field_24;
			int field_28;
			char field_2C[0x1C0];
			short field_1EC;
			short field_1EE;
		};
	}
}