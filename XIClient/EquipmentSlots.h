#pragma once
namespace FFXI {
	namespace Constants {
		namespace Enums {
			enum EquipmentSlot {
				MH    = 0,
				OH    = 1,
				RANGE = 2,
				AMMO  = 3,
				HEAD  = 4,
				BODY  = 5,
				HAND  = 6,
				LEG   = 7,
				FOOT  = 8,
				NECK  = 9,
				BELT  = 10,
				EAR1  = 11,
				EAR2  = 12,
				RING1 = 13,
				RING2 = 14,
				BACK  = 15,
				MAX = 16
			};
		}
	}
}