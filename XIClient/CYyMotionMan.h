#pragma once
#include "CYyMotionQueList.h"
namespace FFXI {
	namespace CYy {
		class CXiActor;
		class BoneTransformState;
		class CMoMo2;
		class CYyMotionMan {
		public:
			static BoneTransformState gBoneTransforms[256];
			void AppendSync(CXiActor*, CMoMo2**, float, float, int, float, float, int, float, float);
			void Append(CXiActor*, CMoMo2**, float, float, int, float, float, int, float, float);
			
			bool UpdateAllLayers();
			CYyMotionQueList motions[7];
		};
	}
}