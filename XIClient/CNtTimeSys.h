#pragma once
#define WIN32_LEAN_AND_MEAN
#include <wtypes.h>
namespace FFXI {
	namespace Network {
		class CNtTimeSys {
		public:
			static DWORD gtime;
			static void UpdateTime();
			static unsigned int ntTimeGet();
			static void ntTimeSet(DWORD, DWORD);
			static void ntGameTimeInit(DWORD, DWORD);
			static unsigned int ntTimeNowGet();
			static int ntTimeNowGetSec();
			static int ntTimeGetSec();
			static unsigned int ntTimeNowGetMSec();
			static unsigned int ntTimeGetMSec();
			static DWORD xiGetTickCount();
			static void ntGameTimeSet(int);
			static int ntGameTimeGet();
			static int ntGameMonthGet(int);
			static int ntGameDayGet(int);
			static int ntGameHourGet(int);
			static void ntTimeProc();
			static bool ntTimeSync(unsigned int, unsigned int);
			static void ntTimeSetAbsorb(unsigned int, unsigned int);
			DWORD field_0;
			int field_4;
			int field_8;
			DWORD field_C;
			DWORD field_10;
			DWORD field_14;
			int field_18[10];
			char field_40[10];
			short field_4A;
			DWORD field_4C;
			DWORD field_50;
			int field_54;
		};
	}
}