#include "CYyMotionQueList.h"
#include "CYyMotionQue.h"
#include "Globals.h"
#include "MemoryPoolManager.h"
#include "CMoMo2.h"

using namespace FFXI::CYy;

const BaseGameObject::ClassInfo CYyMotionQueList::CYyMotionQueListClass{
	"CYyMotionQueList", sizeof(CYyMotionQueList), &BaseGameObject::BaseGameObjectClass
};

const BaseGameObject::ClassInfo* FFXI::CYy::CYyMotionQueList::GetRuntimeClass()
{
	return &CYyMotionQueList::CYyMotionQueListClass;
}

FFXI::CYy::CYyMotionQueList::CYyMotionQueList()
{
	this->Head = nullptr;
	this->field_8 = 1.0;
}

FFXI::CYy::CYyMotionQueList::~CYyMotionQueList()
{
	this->DeleteAll();
}

void FFXI::CYy::CYyMotionQueList::AppendSync(CMoMo2** motionResource, float intensity, float playbackSpeed, int loopCount, float blendDuration, float fadeoutDuration, float startFrame, float customDuration)
{
	CYyMotionQue* tail = this->GetTail();
	if (motionResource == nullptr) {
		//The motion resource handle is NULL
		return;
	}

	char* mem = MemoryPoolManager::globalInstance->Get(sizeof(CYyMotionQue), MemoryPoolManager::MemoryPoolType::Ex);
	if (mem != nullptr) {
		CYyMotionQue* mque = new (mem) CYyMotionQue();
		if (tail != nullptr) {
			tail->Previous = mque;
		}
		else {
			this->Head = mque;
		}

		if (blendDuration == 0.0f)
			blendDuration = 1.0f;

		double v14 = 1.0 / blendDuration;
		mque->BlendDuration = blendDuration;
		mque->RemainingLifetime = 0;
		mque->BlendWeight = 0.0f;
		mque->CustomDuration = customDuration;
		mque->Frame = startFrame;
		mque->BlendIntensity = intensity;
		mque->State = 1;
		mque->BlendRate = v14;
		mque->SetSpeed(playbackSpeed);
		mque->LoopCounter = (char)loopCount;
		mque->MotionFourCC = (*motionResource)->ResourceID;
		mque->SetMod(motionResource);
		(*motionResource)->IncrementReferenceCount();

		if (fadeoutDuration < 0.0f)
			fadeoutDuration = 1.0f;

		mque->FadeoutDuration = fadeoutDuration;
		if (tail != nullptr) {
			CMoMo2** p_tail = (CMoMo2**)tail->GetMod();
			CMoMo2** p_que = (CMoMo2**)mque->GetMod();

			if (p_tail != nullptr && p_que != nullptr) {
				CMoMo2* tailmo2 = *p_tail;
				CMoMo2* quemo2 = *p_que;
				float offset = 0.0f;
				float scale = 0.0f;

				float v2 = (float)tailmo2->GetFrameCount() / tailmo2->GetTiming();
				float v3 = (float)quemo2->GetFrameCount() / quemo2->GetTiming();

				if (v2 > 0.001f) {
					v2 = 1.0 / v2;
					scale = tail->GetFrame() * v2;
					offset = v2 * v3 * (quemo2->GetTiming() * 0.5) * playbackSpeed * blendDuration;
				}

				double v31 = scale * v3 + offset;

				if (v3 <= 0.0) {
					v31 = 0.0;
				}
				else {
					while (v31 >= v3) {
						v31 -= v3;
					}
				}
				mque->Frame = quemo2->GetTiming() * v31;
			}

			tail->Kill(blendDuration);
		}

		int nbque = this->GetNbQue();
		CYyMotionQue* head = this->Head;
		while (nbque > 3) {
			head->Kill(1.0f);
			head = (CYyMotionQue*)head->Previous;
			nbque -= 1;
		}

		if (head != nullptr)
			head->BlendWeight = 1.0f;
	}
	else {
		//Failed to allocate motion que memory
	}
}

void FFXI::CYy::CYyMotionQueList::Append(CMoMo2** a2, float a3, float a4, int a5, float a6, float a7, float a8, float a9)
{
	this->AppendSync(a2, a3, a4, a5, a6, a7, a8, a9);
	CYyMotionQue* tail = this->GetTail();
	if (tail != nullptr)
		tail->Frame = a8;
}

void FFXI::CYy::CYyMotionQueList::Remove(CYyMotionQue* a2)
{
	if (this->Head == a2) {
		this->Head = (CYyMotionQue*)a2->Previous;
		delete a2;
	}
	else {
		CYyMotionQue* que = this->GetParent(a2);
		if (que != nullptr) {
			que->Previous = a2->Previous;
			delete a2;
		}
		else {
			//Motion requests may be accumulated. Please contact Yamamoto.
		}
	}
}

void FFXI::CYy::CYyMotionQueList::DeleteAll()
{
	CYyMotionQue* head = this->Head;
	while (head != nullptr) {
		CYyMotionQue* prev = (CYyMotionQue*)head->Previous;
		this->Remove(head);
		head = prev;
	}
	this->Head = nullptr;
}

int FFXI::CYy::CYyMotionQueList::GetNbQue()
{
	int count = 0;

	CYyMotionQue* head = this->Head;
	while (head) {
		count += 1;
		head = (CYyMotionQue*)head->Previous;
	}
	return count;
}

void FFXI::CYy::CYyMotionQueList::UpdateLayer(int layerIndex, BoneTransformState* p_BoneTransforms)
{
	this->blendAccumulator = 0.0;
	CYyMotionQue* v4 = nullptr;
	CYyMotionQue* head = this->Head;
	while (head) {
		CYyQue* prev = head->Previous;
		if (prev == nullptr || prev->BlendWeight != 1.0) {
			head->ApplyAnimationToBones(layerIndex, this->field_10, p_BoneTransforms, v4);
		}
		if (layerIndex != 0) {
			this->blendAccumulator *= 1.0 - head->BlendWeight;
			this->blendAccumulator += head->BlendIntensity * head->BlendWeight;
		}
		v4 = head;
		head = (CYyMotionQue*)prev;
	}
}

bool FFXI::CYy::CYyMotionQueList::IsExistZombiQue()
{
	CYyMotionQue* head = this->Head;
	while (head != nullptr) {
		if (head->IsFinished() == true)
			return true;

		head = (CYyMotionQue*)head->Previous;
	}
	return false;
}

CYyMotionQue* FFXI::CYy::CYyMotionQueList::GetParent(CYyMotionQue* a2)
{
	CYyMotionQue* result = this->Head;

	while (result != nullptr) {
		if (result->Previous == a2)
			return result;
		result = (CYyMotionQue*)result->Previous;
	}

	return nullptr;
}

CYyMotionQue* FFXI::CYy::CYyMotionQueList::GetTail()
{
	CYyMotionQue* tail = this->Head;
	while (tail)
		tail = (CYyMotionQue*)tail->Previous;

	return tail;
}
