#pragma once
#include "CMoResource.h"
#include "KO_Path.h"

namespace FFXI {
	namespace CYy {
		/**
		 * @brief Spherical Path Resource class
		 *
		 * Represents a resource for defining 3D movement paths.
		 */
		class CMoSphRes : public CMoResource {
		public:
			static const BaseGameObject::ClassInfo CMoSphResClass;
			virtual const BaseGameObject::ClassInfo* GetRuntimeClass() override final;

			CMoSphRes() = default;
			virtual ~CMoSphRes() = default;

			virtual void Open() override final;
			virtual void Close() override final;
			virtual void VObj6() override final;

			/** Path data containing points, segments and radii */
			FFXI::Math::KO_Path pathData;
		};
	}
}