#pragma once
#include "MemoryManagedObject.h"
#include "StatusNode.h"
namespace FFXI {
	namespace CYy {
		class CMoDxStatusMng : public MemoryManagedObject {
		public:
			static const BaseGameObject::ClassInfo CMoDxStatusMngClass;
			virtual const BaseGameObject::ClassInfo* GetRuntimeClass() override final;
			CMoDxStatusMng();
			~CMoDxStatusMng();
			StatusNode field_4;
		};
	}
}