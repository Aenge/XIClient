#include "NT_SYS.h"
#include "CommandLineConfig.h"
#include "Globals.h"
#include "CApp.h"
#include "Strings.h"
#include <timeapi.h>
#include "Values.h"
#include "LSBConnector.h"
#include "NT_SYS_BASE.h"
#include "StringTables.h"
#include "GlobalStruct.h"
#include "CTkMsgWinData.h"
#include "TkManager.h"
#include "PlaceHolders.h"
#include "TextUtils.h"

using namespace FFXI::Network;

CNtGame NT_SYS_BASE::g_Game{};
CNtGame* NT_SYS_BASE::pGame{ nullptr };
CNtDebug NT_SYS_BASE::g_Debug{};
CNtDebug* NT_SYS_BASE::pDebug{ nullptr };
CNtTimeSys NT_SYS_BASE::g_TimeSys{};
CNtTimeSys* NT_SYS_BASE::pTimeSys{ nullptr };
CNtThreadSys NT_SYS_BASE::g_ThreadSys{};
CNtThreadSys* NT_SYS_BASE::pThreadSys{ nullptr };
CNtUdpSys NT_SYS_BASE::g_UdpSys{};
CNtUdpSys* NT_SYS_BASE::pUdpSys{};
CGcMainSys NT_SYS_BASE::g_GcMainSys{};
CGcMainSys* NT_SYS_BASE::pGcMainSys{};

FFXI::Network::NT_SYS_BASE::NTGameInitStruct::NTGameInitStruct() {
	memset(this, 0, sizeof(NTGameInitStruct));
	this->field_0 = 21100;
	this->ZoneNumber = 0;
	this->WorldNumber = 0;
	this->CliMode = 1;
	this->field_10 = 0;
	this->field_14 = 0;
	this->field_18 = 0;
	this->WorldLocallyHosted = 0;
	this->WorldPort = htons(18396u);
	this->CenterPort = htons(18333u);
	this->CenterIP = 0;
	this->WorldIP = 0;
	this->MyIP = 0;
	this->pcnt = 400;
	this->ClientPort = htons(54100u);
	this->Polcon = 0;
	this->DnsMode = 0;
}
bool ParseParam(char* a1, char* a2) {
	int v5{};
	bool foundArg{ false };
	char v6{ 0 };
	while (true) {
		v6 = *a2;
		bool isSpec = v6 == ' ' || !v6 || v6 == '\t';
		
		if (foundArg) {
			if (isSpec) {
				*a1 = 0;
				return false;
			}
			else {
				*a1++ = v6;
				++v5;
				++a2;
			}
		}
		else {
			if (isSpec) {
				++v5;
				++a2;
			}
			else {
				*a1++ = v6;
				++v5;
				foundArg = true;
				++a2;
			}
		}
	}

	return foundArg;
}
bool NT_SYS_BASE::Init()
{
	if (this->field_0)
		return false;

	FFXI::CYy::CApp::g_pNT_SYS = &FFXI::CYy::CApp::g_NT_SYS;

	this->field_0 = 1;

	if (!ntGameInit())
		return false;

	++this->field_2;
	return true;
}

bool NT_SYS_BASE::ntGameInit()
{

	char Buffer[128] = { 0 };
	NTGameInitStruct v15{};

	NT_SYS_BASE::pGame = &NT_SYS_BASE::g_Game;
	memset(pGame, 0, sizeof(g_Game));

	NT_SYS_BASE::pDebug = &NT_SYS_BASE::g_Debug;
	memset(pDebug, 0, sizeof(g_Debug));

	pGame->NetTimeout1 = 30000;
	pGame->NetTimeout2 = 40000;
	pGame->NetTimeout3 = 4 * pGame->NetTimeout1;
	pGame->field_18 = 300000;

	v15.CenterIP = StrToIp(Constants::Strings::Default_CenterIP);
	v15.WorldIP = StrToIp(Constants::Strings::Default_WorldIP);

	pGame->field_440 = 3;

	if (FFXI::Config::CommandLineConfig::instance.ArgGetParam(Constants::Strings::CLARG_ver, Buffer, sizeof(Buffer), 0)) {
		//sub //TODO
		MessageBoxA(NULL, "VERSION STRING HERE", "", NULL);
		exit(-1);
	}

	ntTimeInit();
	ntThreadInit();

	char* Param = FFXI::Config::CommandLineConfig::instance.ArgGetParam(Constants::Strings::CLARG_net, nullptr, 0, 0);
	if (Param) {
		ParseParam(Buffer, Param);
		switch (atoi(Buffer)) {
		case 0:
			v15.CliMode = 0;
			memset(&v15.field_10, 0, 12);
			break;
		case 1:
			v15.CliMode = 1;
			memset(&v15.field_10, 0, 12);
			pDebug->field_90 = 1;
			break;
		case 2:
			v15.CliMode = 2;
			memset(&v15.field_10, 0, 12);
			pDebug->field_90 = 1;
			break;
		case 3:
			v15.CliMode = 3;
			memset(&v15.field_10, 0, 12);
			pDebug->field_90 = 1;
			break;
		case 4:
			v15.CliMode = 4;
			memset(&v15.field_10, 0, 12);
			pDebug->field_90 = 1;
			break;
		case 10:
			v15.CliMode = 1;
			v15.field_10 = 1;
			v15.field_14 = 0;
			v15.field_18 = 0;
			v15.WorldLocallyHosted = 1;
			pDebug->field_4 = 1;
			pDebug->field_90 = 1;
			break;
		case 11:
			v15.CliMode = 0;
			v15.field_10 = 1;
			v15.field_14 = 0;
			v15.field_18 = 0;
			v15.WorldLocallyHosted = 1;
			pDebug->field_4 = 1;
			break;
		case 12:
			v15.CliMode = 0;
			v15.field_10 = 2;
			v15.field_14 = 0;
			v15.field_18 = 0;
			pDebug->field_4 = 2;
			break;
		case 20:
			v15.CliMode = 2;
			v15.field_10 = 2;
			v15.field_14 = 1;
			v15.field_18 = 0;
			v15.WorldLocallyHosted = 1;
			pDebug->field_4 = 2;
			pDebug->field_90 = 1;
			break;
		case 21:
			v15.CliMode = 0;
			v15.field_10 = 0;
			v15.field_14 = 1;
			v15.field_18 = 0;
			v15.WorldLocallyHosted = 1;
			break;
		case 22:
			v15.CliMode = 0;
			v15.field_10 = 0;
			v15.field_14 = 2;
			v15.field_18 = 0;
			v15.WorldLocallyHosted = 1;
			break;
		case 30:
			v15.CliMode = 3;
			v15.field_10 = 2;
			v15.field_14 = 2;
			v15.field_18 = 1;
			v15.WorldLocallyHosted = 1;
			pDebug->field_4 = 3;
			pDebug->field_90 = 1;
			break;
		case 31:
			v15.CliMode = 0;
			v15.field_10 = 0;
			v15.field_14 = 0;
			v15.field_18 = 1;
			v15.WorldLocallyHosted = 1;
			break;
		case 32:
			v15.CliMode = 0;
			v15.field_10 = 0;
			v15.field_14 = 0;
			v15.field_18 = 2;
			v15.WorldLocallyHosted = 1;
			break;
		case 40:
			v15.CliMode = 4;
			v15.field_10 = 2;
			v15.field_14 = 2;
			v15.field_18 = 2;
			v15.WorldLocallyHosted = 1;
			pDebug->field_4 = 4;
			pDebug->field_90 = 1;
			break;
		case 255:
			v15.CliMode = FFXI::Constants::Values::CLIENT_NET_OFFLINE;
			memset(&v15.field_10, 0, 12);
			pDebug->field_90 = 1;
			break;
		}

	}

	if (FFXI::Config::CommandLineConfig::instance.ArgGetParam(Constants::Strings::CLARG_zoneno, Buffer, sizeof(Buffer), 0))
		v15.ZoneNumber = atoi(Buffer);

	if (FFXI::Config::CommandLineConfig::instance.ArgGetParam(Constants::Strings::CLARG_worldno, Buffer, sizeof(Buffer), 0))
		v15.WorldNumber = atoi(Buffer);

	if (FFXI::Config::CommandLineConfig::instance.ArgGetParam(Constants::Strings::CLARG_processid, Buffer, sizeof(Buffer), 0)) {
		int v2 = atoi(Buffer);
		v15.field_0 = v2;
		v15.ZoneNumber = v2 % 1000;
		v15.WorldNumber = v2 / 1000;
	}

	if (FFXI::Config::CommandLineConfig::instance.ArgGetParam(Constants::Strings::CLARG_debug, Buffer, sizeof(Buffer), 0))
		pDebug->field_154 = atoi(Buffer);

	if (FFXI::Config::CommandLineConfig::instance.ArgGetParam(Constants::Strings::CLARG_myip, Buffer, sizeof(Buffer), 0))
		v15.MyIP = StrToIp(Buffer);

	if (FFXI::Config::CommandLineConfig::instance.ArgGetParam(Constants::Strings::CLARG_ip, Buffer, sizeof(Buffer), 0))
		v15.WorldIP = StrToIp(Buffer);

	if (FFXI::Config::CommandLineConfig::instance.ArgGetParam(Constants::Strings::CLARG_port, Buffer, sizeof(Buffer), 0)) {
		v15.WorldPort = htons(atoi(Buffer));
	}
	else if (v15.CliMode == 1) {
		v15.WorldPort = v15.ClientPort;
	}

	if (FFXI::Config::CommandLineConfig::instance.ArgGetParam(Constants::Strings::CLARG_name, Buffer, sizeof(Buffer), 0)) {
		strncpy_s(pDebug->Name, Buffer, sizeof(pDebug->Name));
		strncpy_s(v15.Name, Buffer, sizeof(v15.Name));
	}
	else {
		sprintf_s(pDebug->Name, Constants::Strings::SpaceString);
	}

	if (FFXI::Config::CommandLineConfig::instance.ArgGetParam(Constants::Strings::CLARG_accunt, Buffer, sizeof(Buffer), 0))
		strncpy_s(v15.Name, Buffer, sizeof(v15.Name));
	if (FFXI::Config::CommandLineConfig::instance.ArgGetParam(Constants::Strings::CLARG_pass, Buffer, sizeof(Buffer), 0))
		strncpy_s(v15.Password, Buffer, sizeof(v15.Password));
	if (FFXI::Config::CommandLineConfig::instance.ArgGetParam(Constants::Strings::CLARG_pcnt, Buffer, sizeof(Buffer), 0))
		v15.pcnt = atoi(Buffer);
	if (FFXI::Config::CommandLineConfig::instance.ArgGetParam(Constants::Strings::CLARG_cliport, Buffer, sizeof(Buffer), 0))
		v15.ClientPort = htons(atoi(Buffer));
	if (FFXI::Config::CommandLineConfig::instance.ArgGetParam(Constants::Strings::CLARG_worldip, Buffer, sizeof(Buffer), 0))
		v15.WorldIP = StrToIp(Buffer);
	if (FFXI::Config::CommandLineConfig::instance.ArgGetParam(Constants::Strings::CLARG_worldport, Buffer, sizeof(Buffer), 0))
		v15.WorldPort = htons(atoi(Buffer));
	if (FFXI::Config::CommandLineConfig::instance.ArgGetParam(Constants::Strings::CLARG_centerip, Buffer, sizeof(Buffer), 0))
		v15.CenterIP = StrToIp(Buffer);
	if (FFXI::Config::CommandLineConfig::instance.ArgGetParam(Constants::Strings::CLARG_centerport, Buffer, sizeof(Buffer), 0))
		v15.CenterPort = htons(atoi(Buffer));

	pDebug->X = 0;
	pDebug->Y = 0;
	pDebug->Z = 0;
	pDebug->Dir = 0;

	if (FFXI::Config::CommandLineConfig::instance.ArgGetParam(Constants::Strings::CLARG_x, Buffer, sizeof(Buffer), 0))
		pDebug->X = atoi(Buffer);

	if (FFXI::Config::CommandLineConfig::instance.ArgGetParam(Constants::Strings::CLARG_y, Buffer, sizeof(Buffer), 0))
		pDebug->Y = atoi(Buffer);

	if (FFXI::Config::CommandLineConfig::instance.ArgGetParam(Constants::Strings::CLARG_z, Buffer, sizeof(Buffer), 0))
		pDebug->Z = atoi(Buffer);

	if (FFXI::Config::CommandLineConfig::instance.ArgGetParam(Constants::Strings::CLARG_dir, Buffer, sizeof(Buffer), 0))
		pDebug->Dir = atoi(Buffer);

	char* v7 = FFXI::Config::CommandLineConfig::instance.ArgGetParam(Constants::Strings::CLARG_gm, nullptr, sizeof(Buffer), 0);
	if (v7)
		ParseParam(pDebug->Gm, v7);

	if (FFXI::Config::CommandLineConfig::instance.ArgGetParam(Constants::Strings::CLARG_nohitck, nullptr, sizeof(Buffer), 0))
		pDebug->Nohitck = 0;
	else
		pDebug->Nohitck = 1;

	if (FFXI::Config::CommandLineConfig::instance.ArgGetParam(Constants::Strings::CLARG_timeout, Buffer, sizeof(Buffer), 0)) {
		pGame->NetTimeout1 = atoi(Buffer);
		pGame->NetTimeout2 = atoi(Buffer) + 3000;
		pGame->NetTimeout3 = 4 * pGame->NetTimeout1;
		if (pGame->NetTimeout3 > 0x493E0u)
		{
			if (pGame->NetTimeout2 >= 0x4BAF0u)
				pGame->NetTimeout3 = pGame->NetTimeout2 + 10000;
			else
				pGame->NetTimeout3 = 300000;
		}
	}
	else if (v15.field_10 == 1 || v15.CliMode == 1) {
		pGame->NetTimeout1 = 3600000;
		pGame->NetTimeout2 = 3660000;
		pGame->NetTimeout3 = 3720000;
	}

	if (FFXI::Config::CommandLineConfig::instance.ArgGetParam(Constants::Strings::CLARG_rtimeout, Buffer, sizeof(Buffer), 0))
		pGame->NetTimeout2 = atoi(Buffer);

	if (FFXI::Config::CommandLineConfig::instance.ArgGetParam(Constants::Strings::CLARG_uniqueno, Buffer, sizeof(Buffer), 0))
		pDebug->UniqueNo = atoi(Buffer);

	if (FFXI::Config::CommandLineConfig::instance.ArgGetParam(Constants::Strings::CLARG_globaluniqueno, Buffer, sizeof(Buffer), 0))
		pDebug->UniqueNo = atoi(Buffer);

	if (FFXI::Config::CommandLineConfig::instance.ArgGetParam(Constants::Strings::CLARG_mapno, Buffer, sizeof(Buffer), 0))
		pDebug->MapNo = atoi(Buffer);

	if (FFXI::Config::CommandLineConfig::instance.ArgGetParam(Constants::Strings::CLARG_mapblksize, Buffer, sizeof(Buffer), 0))
		pDebug->Mapblksize = atoi(Buffer);

	if (FFXI::Config::CommandLineConfig::instance.ArgGetParam(Constants::Strings::CLARG_stoptimer, Buffer, sizeof(Buffer), 0))
		pDebug->StopTimer = atoi(Buffer);

	if (FFXI::Config::CommandLineConfig::instance.ArgGetParam(Constants::Strings::CLARG_hp, Buffer, sizeof(Buffer), 0))
		pDebug->hp = atoi(Buffer);

	if (FFXI::Config::CommandLineConfig::instance.ArgGetParam(Constants::Strings::CLARG_mp, Buffer, sizeof(Buffer), 0))
		pDebug->mp = atoi(Buffer);

	if (FFXI::Config::CommandLineConfig::instance.ArgGetParam(Constants::Strings::CLARG_clinum, Buffer, sizeof(Buffer), 0))
		pDebug->CliNum = atoi(Buffer);
	else
		pDebug->CliNum = 1;

	if (FFXI::Config::CommandLineConfig::instance.ArgGetParam(Constants::Strings::CLARG_npcnum, Buffer, sizeof(Buffer), 0))
		pGame->NpcNum = atoi(Buffer);
	if (FFXI::Config::CommandLineConfig::instance.ArgGetParam(Constants::Strings::CLARG_pcnum, Buffer, sizeof(Buffer), 0))
		pGame->PcNum = atoi(Buffer);
	if (FFXI::Config::CommandLineConfig::instance.ArgGetParam(Constants::Strings::CLARG_petnum, Buffer, sizeof(Buffer), 0))
		pGame->PetNum = atoi(Buffer);

	if (FFXI::Config::CommandLineConfig::instance.ArgGetParam(Constants::Strings::CLARG_sendt, Buffer, sizeof(Buffer), 0))
		pDebug->Sendt = atoi(Buffer);
	else
		pDebug->Sendt = 0;

	if (FFXI::Config::CommandLineConfig::instance.ArgGetParam(Constants::Strings::CLARG_recvt, Buffer, sizeof(Buffer), 0))
		pDebug->Recvt = atoi(Buffer);
	else
		pDebug->Recvt = 0;

	if (FFXI::Config::CommandLineConfig::instance.ArgGetParam(Constants::Strings::CLARG_prnd, Buffer, sizeof(Buffer), 0))
		pDebug->Prnd = atoi(Buffer);
	else
		pDebug->Prnd = 0;

	if (FFXI::Config::CommandLineConfig::instance.ArgGetParam(Constants::Strings::CLARG_lost, Buffer, sizeof(Buffer), 0))
		pDebug->Lost = atoi(Buffer);
	else
		pDebug->Lost = 0;

	pDebug->field_188 = 0;
	pDebug->field_18A = 0;

	if (FFXI::Config::CommandLineConfig::instance.ArgGetParam(Constants::Strings::CLARG_polcon, Buffer, sizeof(Buffer), 0))
		v15.Polcon = v15.CliMode >= 3;
	if (FFXI::Config::CommandLineConfig::instance.ArgGetParam(Constants::Strings::CLARG_dnsmode, Buffer, sizeof(Buffer), 0))
		v15.DnsMode = atoi(Buffer);

	pGame->CliMode = v15.CliMode;
	pGame->field_0 = v15.field_10;

	if (!pGame->PcNum || pGame->PcNum > 768)
		pGame->PcNum = 768;

	if (!pGame->PetNum) {
		if (pGame->PcNum > 512)
			pGame->PetNum = 512;
		else
			pGame->PetNum = pGame->PcNum;
	}
	else if (pGame->PetNum > 512)
		pGame->PetNum = 512;

	if (pGame->PcNum >= 12) {
		pGame->field_30 = pGame->PcNum - 12;
	}

	pGame->field_2C = pGame->PetNum + 0x700;

	pGame->Polcon = v15.Polcon;
	pGame->DnsMode = v15.DnsMode;

	pTimeSys->ntTimeSet(0, 0);
	pTimeSys->ntGameTimeSet(0);
	if (ntIpInit())
		return false;

	if (NT_SYS_BASE::ntUdpInit() == false)
		return false;

	if (v15.WorldLocallyHosted)
		v15.WorldIP = pUdpSys->MyIp;

	if (gcMainWorkInit(v15.CliMode, v15.WorldIP, v15.WorldPort, pDebug->Name, v15.Name, v15.Password, v15.pcnt) != true)
		return false;

	pDebug->field_79C = pTimeSys->ntGameTimeGet();

	pGame->VersionDateTime = Constants::Strings::VERSION_DateTime;
	pGame->field_46C = 1;

	//LSB check
	char* params = FFXI::Config::CommandLineConfig::instance.ArgGetParam(Constants::Strings::CLARG_lsb, Buffer, sizeof(Buffer), 0);
	if (params != nullptr) {
		//The first param is expected to be the username
		ParseParam(Buffer, params);
		//No param found
		if (Buffer[0] == 0) {
			return false;
		}

		LSBConnector::instance.SetUsername(Buffer);

		//The second param is expected to be the password
		ParseParam(Buffer, params);
		//No param found
		if (Buffer[0] == 0) {
			return false;
		}

		LSBConnector::instance.SetPassword(Buffer);
	}

	if (LSBConnector::instance.Initialize() == true
	&& LSBConnector::instance.Connect(v15.WorldIP) == true) {
		LSBConnector::instance.use_LSB = true;
	}
	else {
		return false;
	}
	
	return true;
}

bool NT_SYS_BASE::ntGameEnd() {
	//sub //TODO
	if (!pGame) return false;
	//sub

	WSACleanup();

	return true;
}

bool NT_SYS_BASE::gcMainWorkInit(int client_mode, int worldIP, unsigned short worldPort, char* debugname, char* name, char* password, int pcnt)
{
	if (pGcMainSys || client_mode < 0)
		return false;
	
	if (client_mode == 0 || client_mode == FFXI::Constants::Values::CLIENT_NET_OFFLINE) {
		return true;
	}

	pGcMainSys = &g_GcMainSys;
	memset(pGcMainSys, 0, sizeof(g_GcMainSys));
	pGcMainSys->gcMainDivisionInit();
	strncpy_s(pGcMainSys->DebugName, debugname, sizeof(pGcMainSys->DebugName));
	strncpy_s(pGcMainSys->Name, name, sizeof(pGcMainSys->Name));
	strncpy_s(pGcMainSys->Password, password, sizeof(pGcMainSys->Password));
	pGcMainSys->field_12 = htons(54090u);
	pGcMainSys->WorldIP = worldIP;
	pGcMainSys->WorldPort = worldPort;
	pGcMainSys->field_8 = worldIP;
	pGcMainSys->field_10 = worldPort;
	pGcMainSys->pcnt = pcnt;
	pGcMainSys->field_120 = 1;
	//PolSys init
	//PolStat init
	//some init
	pGcMainSys->gcFriendInit(60);
	//FDtXiInit
	//gcPolMessageInit
	if (client_mode == 1) {
		pGcMainSys->field_11C = 7;
	}
	else if (client_mode == 2) {
		pGcMainSys->field_11C = 4;
	}
	else {
		pGcMainSys->field_11C = 1;
	}

	return true;
}

void NT_SYS_BASE::End()
{
	if (!this) return;

	this->field_0 = 0;
	if (ntGameEnd())
		FFXI::CYy::CApp::g_pCApp->g_pNT_SYS->ClientMode = 0;
}

unsigned int NT_SYS_BASE::StrToIp(const char* a1)
{
	unsigned int result = inet_addr(a1);
	if (result != INADDR_NONE && result != INADDR_ANY)
		return result;

	hostent* host = gethostbyname(a1);
	if (host)
		return **(unsigned int**)host->h_addr_list;

	return 0;
}

bool FFXI::Network::NT_SYS_BASE::ntIpgethostname(void* a1)
{
	char name[128];
	if (gethostname(name, 128)) return false;

	hostent* v2 = gethostbyname(name);
	if (!v2) return false;

	char* v5 = *v2->h_addr_list;
	int length = v2->h_length;
	int roundlength = 4 * (length >> 2);
	memcpy(a1, v5, roundlength);
	memcpy((char*)a1 + roundlength, v5 + roundlength, length & 3);
	return true;
}

bool FFXI::Network::NT_SYS_BASE::ntUdpInit()
{
	CNtUdpSys* udpsys = FFXI::Network::NT_SYS_BASE::pUdpSys;
	if (udpsys != nullptr) {
		NT_SYS_BASE::ntUdpEnd();
	}

	FFXI::Network::NT_SYS_BASE::pUdpSys = &FFXI::Network::NT_SYS_BASE::g_UdpSys;
	memset(FFXI::Network::NT_SYS_BASE::pUdpSys, 0, sizeof(CNtUdpSys));

	if (FFXI::Network::NT_SYS_BASE::ntIpgethostname(&FFXI::Network::NT_SYS_BASE::pUdpSys->MyIp) == false) {
		NT_SYS_BASE::ntUdpEnd();
		return false;
	}

	return true;
}

void FFXI::Network::NT_SYS_BASE::ntUdpEnd()
{
	CNtUdpSys* udpsys = FFXI::Network::NT_SYS_BASE::pUdpSys;
	if (udpsys == nullptr) {
		return;
	}

	int v2 = (udpsys->field_1AFC4 + 1) % 4;
	while (v2 != udpsys->field_1AFC4) {
		udpContext* context = udpsys->subs + v2;
		if (context->field_4 == 2) {
			closesocket(context->socket);
			context->field_4 = 0;
		}
		v2 = (v2 + 1) % 4;
	}

	pUdpSys = nullptr;
}

udpContext* FFXI::Network::NT_SYS_BASE::ntUdpCreate(unsigned short* a1)
{
	CNtUdpSys* udpsys = FFXI::Network::NT_SYS_BASE::pUdpSys;
	int sub_index = udpsys->field_1AFC4;
	int next_index = (sub_index + 1) % 4;
	
	if (sub_index == next_index) {
		return nullptr;
	}

	while (udpsys->subs[next_index].field_4 != 0) {
		next_index = (next_index + 1) % 4;
		if (next_index == sub_index) {
			return nullptr;
		}
	}

	udpContext* context = udpsys->subs + next_index;
	if (context == nullptr) {
		return nullptr;
	}

	memset(context, 0, sizeof(udpContext));
	context->field_4 = 2;
	context->socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	
	unsigned short port = 0xD34A;
	a1[0] = htons(port);

	context->field_6BE0.sa_family = AF_INET;
	u_long addr = htonl(0);
	memcpy(context->field_6BE0.sa_data, a1, 2);
	memcpy(context->field_6BE0.sa_data + 2, &addr, 4);

	int attempt_count = 0;
	while (bind(context->socket, &context->field_6BE0, sizeof(context->field_6BE0)) == SOCKET_ERROR) {
		attempt_count += 1;
		if (attempt_count >= 100) {
			closesocket(context->socket);
			return nullptr;
		}

		unsigned short new_port = port + 256;
		a1[0] = htons(new_port);
		memcpy(context->field_6BE0.sa_data, a1, 2);
	}

	int optval = 115000;
	setsockopt(context->socket, SOL_SOCKET, SO_RCVBUF, (const char*) & optval, sizeof(optval));
	
	optval = 100000;
	setsockopt(context->socket, SOL_SOCKET, SO_SNDBUF, (const char*) & optval, sizeof(optval));

	u_long l_optval = 1;
	if (ioctlsocket(context->socket, FIONBIO, &l_optval) != SOCKET_ERROR) {
		pUdpSys->field_1AFC4 = (pUdpSys->field_1AFC4 + 1) % 4;
		return context;
	}

	return nullptr;
}

bool FFXI::Network::NT_SYS_BASE::ntSysChatPrintArea(UDP::GP_SERV_PACKETS::GP_SERV_LOGIN* a1)
{
	char* v1 = Text::XiStrGet(Constants::Enums::StrTables::XIStr_20, a1->field_30 & 0xFFFF);
	char* zone_name = Text::XiStrGet(Constants::Enums::StrTables::XIStr_ZoneFullNames, a1->field_30 & 0xFFFF);
	if (zone_name == nullptr) {
		return ntSysChatPrint(41, 0, 0, 0, 0, "=== Area: %d ===", a1->field_30);
	}

	char* v3 = Text::XiStrGet(Constants::Enums::StrTables::XIStr_VariousHelpText, 0x5E);
	if (v3[0] != 0) {
		ntSysChatPrint(41, 0, 0, 0, 0, v3, v1);
	}

	char* v5 = Text::XiStrGet(Constants::Enums::StrTables::XIStr_VariousHelpText, 0x62);
	return ntSysChatPrint(41, 0, 0, 0, 0, v5, zone_name);
}

bool FFXI::Network::NT_SYS_BASE::ntSysChatPrint(int a1, int a2, int a3, int a4, int a5, const char* a6, ...)
{
	va_list va{};
	va_start(va, a6);
	if (GlobalStruct::g_GlobalStruct.field_0 != 96 && GlobalStruct::g_GlobalStruct.field_0 != 112) {
		va_end(va);
		return true;
	}
	CTk::TK_DATAHEADER header{};
	if ((a2 & 1) != 0) {
		header.field_1 = 0x80;
	}
	int v6{ 0 };
	bool new_v6{ true };
	switch (a1) {
	case 0:
		header.field_0 = 9;
		v6 = 1;
		break;
	case 1:
		header.field_0 = 10;
		v6 = 1;
		break;
	case 3:
		header.field_0 = 12;
		v6 = 1;
		break;
	case 4:
		header.field_0 = 13;
		v6 = 1;
		break;
	case 5:
		header.field_0 = 14;
		v6 = 1;
		break;
	case 6:
	case 7:
		header.field_0 = 200;
		header.field_1 |= 0x40u;
		break;
	case 8:
		header.field_0 = 15;
		v6 = 1;
		break;
	case 9:
	case 10:
	case 11:
		header.field_0 = 201;
		header.field_1 |= 0x40u;
		break;
	case 13:
		header.field_0 = 9;
		v6 = 2;
		header.field_1 |= 0x40u;
		break;
	case 14:
		header.field_0 = 10;
		v6 = 2;
		header.field_1 |= 0x40u;
		break;
	case 15:
		header.field_0 = 13;
		v6 = 2;
		header.field_1 |= 0x40u;
		break;
	case 16:
		header.field_0 = 14;
		v6 = 2;
		header.field_1 |= 0x40u;
		break;
	case 26:
		header.field_0 = 11;
		v6 = 1;
		break;
	case 27:
		header.field_0 = 214;
		v6 = 1;
		break;
	case 28:
		header.field_0 = 214;
		v6 = 2;
		header.field_1 |= 0x40u;
		break;
	case 33:
		header.field_0 = 212;
		v6 = 1;
		break;
	case 34:
		header.field_0 = 220;
		v6 = 1;
		new_v6 = false;
		break;
	case 35:
		header.field_0 = 222;
		v6 = 1;
		new_v6 = false;
		break;
	case 41:
		header.field_0 = 0;
		break;
	default:
		header.field_0 = 121;
		header.field_1 |= 0x40u;
		break;
	}

	char msg[500];
	vsprintf_s(msg, a6, va);
	if (new_v6 == true) {
		Util::Text::FsTrimStr(msg);
	}
	else if (a1 >= 0x22u && a1 <= 0x23u) {
		char* v8 = strchr(msg, 239);
		if (v8 != nullptr) {
			if (v8[1] >= 0x2Fi8 && v8[1] < 0x4Di8) {
				header.field_1 |= 0x20u;
			}
		}
	}
	if (a3 == 0) {
		if (v6 == 1) {
			int pos = 0;
			if (msg[0] != 0) {
				while (msg[pos] != ':' && msg[pos] != ' ') {
					pos += 1;
					if (msg[pos] == 0) {
						pos = 0;
						break;
					}
					
				}
			}
			
			Placeholder::VulgarFilter(msg + pos);
		}
		else if (v6 == 2) {
			Placeholder::VulgarFilter(msg);
		}
		
		CTk::TkManager::MsgWinData1->AddString(msg, &header, 0, 1, 0);
	}
	else {
		exit(0x100D7C15);
	}

	va_end(va);
	return true;
}

bool NT_SYS_BASE::IsActive()
{
	return this->field_2 != 0;
}

void NT_SYS_BASE::ntThreadInit()
{
	pThreadSys = &g_ThreadSys;
	memset(pThreadSys, 0, sizeof(g_ThreadSys));
	pThreadSys->field_2EE1 = 2;
}

void NT_SYS_BASE::ntTimeInit()
{
	pTimeSys = &g_TimeSys;
	memset(pTimeSys, 0, sizeof(g_TimeSys));
	pTimeSys->field_C = time(0);
	pTimeSys->field_10 = 0;
	pTimeSys->field_0 = FFXI::Network::CNtTimeSys::xiGetTickCount();
	pTimeSys->field_54 = 0;
	pTimeSys->field_4C = 0;
	pTimeSys->field_50 = FFXI::Network::CNtTimeSys::xiGetTickCount();
}

int NT_SYS_BASE::ntIpInit()
{
	static bool ntIpInitialized{ false };
	if (ntIpInitialized) return 0;

	WSADATA data{};
	if (WSAStartup(0x101u, &data) != 0) return -1;

	//todo //sub
	//Not sure if any of this stuff is used. Will come back to it if it is.

	ntIpInitialized = true;
	return 0;
}

void NT_SYS_BASE::ntGameProc2()
{
	if (pGame == nullptr) {
		return;
	}

	pTimeSys->ntTimeProc();

	if (pThreadSys == nullptr || pUdpSys == nullptr) {
		return;
	}

	//sub //todo 
	//3 funcs
}
