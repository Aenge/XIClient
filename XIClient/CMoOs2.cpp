#include "CMoOs2.h"
#include "Globals.h"
#include "RegistryConfig.h"
#include "CDx.h"
#include "MemoryPoolManager.h"
#include "CYyOs2VtxBuffer.h"
#include "CMoTaskMng.h"
#include "CXiSkeletonActor.h"
#include "CYyModel.h"
#include "CEnv.h"
#include "CYyDb.h"
#include "CYySkl.h"
#include "CYyTexMng.h"
#include "CYyTex.h"

using namespace FFXI::CYy;

FFXI::Math::WMatrix* CMoOs2::mat_matrix{ nullptr };

unsigned short short1{}, short2{};
UINT DrawOneStrides[5]{};
IDirect3DVertexBuffer8* DrawOneBuffs[5]{};
DWORD TempVertexShader{ NULL };
bool CMoOs2::DrawBasicFlagOne{ false };
bool CMoOs2::DrawBasicFlagTwo{ false };
FFXI::Math::WMatrix CMoOs2::DrawBasicMatrixOne{};
FFXI::Math::WMatrix CMoOs2::DrawBasicMatrixTwo{};

FFXI::CYy::CYyTex* drawbasic_local_tex_array[2] = { nullptr };

void SetVertexShaderFromTemp() {
	if (TempVertexShader != NULL) {
		CDx::instance->DXDevice->SetVertexShader(TempVertexShader);
	}
}

void SetTempVertexShader(DWORD a1) {
	TempVertexShader = a1;
	SetVertexShaderFromTemp();
}

void FFXI::CYy::CMoOs2::Open()
{
	if (this->Data[0] != 0 && this->Data[0] != 1) {
		//version error
		return;
	}

	if (this->Data[1] != 0)
		return;

	if ((this->Data[2] & 0x7F) == 0) {
		*(CYyOs2VtxBuffer**)(this->Data + 46) = nullptr;
		this->InitTypeZero();
	}
	else if ((this->Data[2] & 0x7F) == 1) {
		this->InitTypeOne();
	}
	else
		return;

	this->Data[1] = 1;
}

void FFXI::CYy::CMoOs2::Close()
{
	if ((this->Data[2] & 0x7F) == 0) {
		//Type 0 cleanup
		CYyOs2VtxBuffer* buf = *(CYyOs2VtxBuffer**)(this->Data + 46);
		if (buf != nullptr) {
			buf->DestroyObject();
			*(CYyOs2VtxBuffer**)(this->Data + 46) = nullptr;
		}
	}
}

void FFXI::CYy::CMoOs2::InitTypeZero()
{
	*(unsigned short**)(this->Data + 6) = (unsigned short*)(this->Data + 2 * *(int*)(this->Data + 6));
	*(unsigned short**)(this->Data + 12) = (unsigned short*)(this->Data + 2 * *(int*)(this->Data + 12));
	*(unsigned short**)(this->Data + 18) = (unsigned short*)(this->Data + 2 * *(int*)(this->Data + 18));
	*(unsigned short**)(this->Data + 24) = (unsigned short*)(this->Data + 2 * *(int*)(this->Data + 24));
	*(unsigned short**)(this->Data + 30) = (unsigned short*)(this->Data + 2 * *(int*)(this->Data + 30));
	*(unsigned short**)(this->Data + 36) = (unsigned short*)(this->Data + 2 * *(int*)(this->Data + 36));

	if ((this->Data[4] & 2) != 0)
		return;

	char* mem = MemoryPoolManager::globalInstance->Get(sizeof(CYyOs2VtxBuffer), MemoryPoolManager::MemoryPoolType::Ex);
	if (mem == nullptr)
		return;
	
	CMoTaskMng::DeleteThisTask = true;
	CYyOs2VtxBuffer* buf = new (mem) CYyOs2VtxBuffer((CMoOs2**)this->Metadata.SelfReference);
	*(CYyOs2VtxBuffer**)(this->Data + 46) = buf;
	
	this->Data[4] |= 2;
}

void FFXI::CYy::CMoOs2::InitTypeOne()
{
	*(unsigned short**)(this->Data + 6) = (unsigned short*)(this->Data + 2 * *(int*)(this->Data + 6));
	*(unsigned short**)(this->Data + 12) = (unsigned short*)(this->Data + 2 * *(int*)(this->Data + 12));
	*(unsigned short**)(this->Data + 18) = (unsigned short*)(this->Data + 2 * *(int*)(this->Data + 18));
	*(unsigned short**)(this->Data + 24) = (unsigned short*)(this->Data + 2 * *(int*)(this->Data + 24));
	*(unsigned short**)(this->Data + 30) = (unsigned short*)(this->Data + 2 * *(int*)(this->Data + 30));
	*(unsigned short**)(this->Data + 36) = (unsigned short*)(this->Data + 2 * *(int*)(this->Data + 36));
	
	*(unsigned short**)(this->Data + 46) = (unsigned short*)(this->Data + 2 * *(int*)(this->Data + 46));
	*(unsigned short**)(this->Data + 76) = (unsigned short*)(this->Data + 2 * *(int*)(this->Data + 76));
}

void VBSetup(int a1, IDirect3DVertexBuffer8* a2, int a3) {
	DrawOneBuffs[a1] = a2;
	DrawOneStrides[a1] = a3;
	if (a2 != nullptr) {
		FFXI::CYy::CDx::instance->DXDevice->SetStreamSource(a1, a2, a3);
	}
}
//Breakup funcs for DrawBasic
void Draw54Init(D3DPRIMITIVETYPE a1, CMoOs2* _this, CYyOs2VtxBuffer* v10, int v9, int v26) {
	SetTempVertexShader(v10->ShaderHandle);
	
	VBSetup(0, v10->field_44, 12);
	VBSetup(1, v10->field_48, 12);
	VBSetup(2, v10->VertexBuffer, 8);
	VBSetup(3, nullptr, 0);

	FFXI::CYy::CDx::instance->DXDevice->SetIndices(v10->IndexBuffer, 0);
	FFXI::CYy::CDx::instance->DrawStreamIndexed(a1, 0, v10->field_60 + short1 + short2, v9, v26);
	CYyModel::maybe_polys_drawn += v26;

	if ((_this->Data[4] & 1) != 0) {
		VBSetup(0, v10->field_50, 12);
		VBSetup(1, v10->field_54, 12);
		FFXI::CYy::CDx::instance->DrawStreamIndexed(a1, 0, v10->field_60 + short1 + short2, v9, v26);
		CYyModel::maybe_polys_drawn += v26;
	}
}

bool PrepDrawBasicOne(CYyTex** a1, D3DCOLOR a2, FFXI::Math::WMatrix* a3) {
	FFXI::Math::WMatrix* v4{};
	CDx* cdx = FFXI::CYy::CDx::instance;
	if (FFXI::CYy::model_static_wrapper::val != 0) {
		exit(0x10024E37);
		//TODO
		//dword_10445CA8 = *a1;
		//dword_10445CAC = a1[1];
		//dword_10445CA4 = a2;
		if (0 != nullptr) {
			//settexture;
		}

		cdx->DXDevice->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_ALWAYS);
		cdx->DXDevice->SetRenderState(D3DRS_ZWRITEENABLE, false);
		cdx->DXDevice->SetTextureStageState(0, D3DTSS_ADDRESSU, D3DTADDRESS_WRAP);
		cdx->DXDevice->SetTextureStageState(0, D3DTSS_ADDRESSV, D3DTADDRESS_WRAP);

		if (FFXI::Config::RegistryConfig::g_pOptions->Other.field_58 != 0 && FFXI::Config::RegistryConfig::g_pOptions->Main.GraphicsStabilization == 0) {
			cdx->DXDevice->SetRenderState(D3DRS_WRAP0, D3DWRAP_U | D3DWRAP_V | D3DWRAP_W);
		}

		v4->Identity();
		//TODO
		//v4->m[3][0] = ?;
		v4->MatrixMultiply(a3);

		cdx->DXDevice->SetRenderState(D3DRS_LIGHTING, false);
		cdx->DXDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, true);
		cdx->DXDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
		cdx->DXDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLENDOP_SUBTRACT);
		cdx->DXDevice->SetTransform(D3DTS_TEXTURE0, v4);
		cdx->DXDevice->SetTextureStageState(0, D3DTSS_TEXTURETRANSFORMFLAGS, D3DTTFF_COUNT2);
		cdx->DXDevice->SetTextureStageState(0, D3DTSS_TEXCOORDINDEX, 0x10000);
		cdx->DXDevice->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_MODULATE2X);
		cdx->DXDevice->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_TEXTURE);
		cdx->DXDevice->SetTextureStageState(0, D3DTSS_COLORARG2, D3DTA_DIFFUSE);
		cdx->DXDevice->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_MODULATE2X);
		cdx->DXDevice->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
		cdx->DXDevice->SetTextureStageState(0, D3DTSS_ALPHAARG2, D3DTA_DIFFUSE);
		cdx->DXDevice->SetTextureStageState(1, D3DTSS_COLOROP, D3DTOP_MODULATE2X);
		cdx->DXDevice->SetTextureStageState(1, D3DTSS_COLORARG1, D3DTA_CURRENT);
		cdx->DXDevice->SetTextureStageState(1, D3DTSS_COLORARG2, D3DTA_TFACTOR);
		cdx->DXDevice->SetTextureStageState(1, D3DTSS_ALPHAOP, D3DTOP_MODULATE2X);
		cdx->DXDevice->SetTextureStageState(1, D3DTSS_ALPHAARG1, D3DTA_CURRENT);
		cdx->DXDevice->SetTextureStageState(1, D3DTSS_ALPHAARG2, D3DTA_TFACTOR);
		cdx->DXDevice->SetTextureStageState(2, D3DTSS_COLOROP, D3DTA_CURRENT);
		cdx->DXDevice->SetTextureStageState(2, D3DTSS_ALPHAOP, D3DTA_CURRENT);
		//TODO
		//cdx->DXDevice->SetRenderState(D3DRS_TEXTUREFACTOR, dword_10445CB4);
	}
	return false;
}

bool PrepDrawBasicTwo(tex_struct* a0, CYyTex** a1, D3DCOLOR a2, FFXI::Math::WMatrix* a3) {
	FFXI::Math::WMatrix* v6{};
	if (a0->field_4 != nullptr) {
		exit(0x10024528);
		return true;
	}

	return false;
}

void Draw5453(CMoOs2* _this, CYyModel* a3, CYyOs2VtxBuffer* v10, D3DCOLOR color, int polycount) 
{
	if (CMoOs2::DrawBasicFlagOne == true) {
		if (PrepDrawBasicOne(drawbasic_local_tex_array, color , &CMoOs2::DrawBasicMatrixOne) == true) {
			exit(0x1002DF0A);
		}
	}
	
	if (CMoOs2::DrawBasicFlagTwo == true) {
		if (PrepDrawBasicTwo(&a3->field_90, drawbasic_local_tex_array, color, &CMoOs2::DrawBasicMatrixTwo) == true) {
			exit(0x1002E042);
		}
	}

	if (a3->shadowRenderer.PrepareToRender(drawbasic_local_tex_array, color) == true) {
		exit(0x1002E164);
	}
}

void Draw54(D3DPRIMITIVETYPE a2, CMoOs2* _this, CYyModel* a3, CYyOs2VtxBuffer* v10, D3DCOLOR color, int start, int polycount, char type)
{
	CDx* cdx = FFXI::CYy::CDx::instance;
	if (CMoOs2::DrawBasicFlagOne == true) {
		if (PrepDrawBasicOne(drawbasic_local_tex_array, color, &CMoOs2::DrawBasicMatrixOne) == true) {
			exit(0x1002DF0A);
		}
	}

	if (CMoOs2::DrawBasicFlagTwo == true) {
		if (PrepDrawBasicTwo(&a3->field_90, drawbasic_local_tex_array, color, &CMoOs2::DrawBasicMatrixTwo) == true) {
			exit(0x1002E042);
		}
	}

	if (a3->shadowRenderer.PrepareToRender(drawbasic_local_tex_array, color) == true) {
		SetVertexShaderFromTemp();
		if (v10->field_68 != 0) {
			VBSetup(0, v10->field_44, 12);
			VBSetup(1, v10->field_48, 12);
			VBSetup(2, v10->VertexBuffer, 8);
			VBSetup(3, nullptr, 0);
			cdx->DXDevice->SetIndices(v10->IndexBuffer, 0);
			cdx->DrawStreamIndexed(a2, 0, v10->field_60 + short1 + short2, start, polycount);

			if ((_this->Data[4] & 1) != 0) {
				VBSetup(0, v10->field_50, 12);
				VBSetup(1, v10->field_54, 12);
				cdx->DrawStreamIndexed(a2, 0, v10->field_60 + short1 + short2, start, polycount);
			}
		}
		
		a3->shadowRenderer.FinishRender(type);
	}
}

void FFXI::CYy::CMoOs2::DrawBasic(CXiActor* a2, CYyModel* a3, CYyModelBase* a4, float a5, int* a6, int* a7)
{
	SetTempVertexShader(NULL);
	CMoOs2::mat_matrix = FFXI::CYy::CYySkl::gBoneMatrices;
	*a6 = 0;
	*a7 = 0;
	unsigned short* sptr = *(unsigned short**)(this->Data + 18);
	short1 = sptr[0];
	short2 = sptr[1];
	
	CYyOs2VtxBuffer::DataPointer1 = *(unsigned short**)(this->Data + 24);
	CYyOs2VtxBuffer::DataPointer2 = CYyOs2VtxBuffer::DataPointer1 + 2 * short1;
	CYyOs2VtxBuffer* v10 = *(CYyOs2VtxBuffer**)(this->Data + 46);
	if (v10->IndexBuffer == nullptr)
		return;

	if (v10->VertexBuffer == nullptr)
		return;

	if (CXiSkeletonActor::somebool == true
		|| a3->field_B8 != 1) {
		if (v10->field_44 == nullptr)
			return;

		if (v10->field_48 == nullptr)
			return;

		while (v10->field_44->Lock(0, 12 * v10->SomeCount, (BYTE**) &v10->field_8E, D3DLOCK_DISCARD) != D3D_OK)
			Sleep(0);

		v10->field_92 = 0;
		while (v10->field_48->Lock(0, 12 * v10->SomeCount, (BYTE**) &v10->field_92, D3DLOCK_DISCARD) != D3D_OK)
			Sleep(0);

		if ((this->Data[4] & 1) != 0)
		{
			if (v10->field_50 == nullptr || v10->field_54 == nullptr)
			{
				while (v10->field_44->Unlock() != D3D_OK)
					Sleep(0);
				while (v10->field_48->Unlock() != D3D_OK)
					Sleep(0);

				return;
			}

			while (v10->field_50->Lock(0, 12 * v10->SomeCount, (BYTE**)&v10->field_96, D3DLOCK_DISCARD) != D3D_OK)
				Sleep(0);

			v10->field_9A = nullptr;
			while (v10->field_54->Lock(0, 12 * v10->SomeCount, (BYTE**)&v10->field_9A, D3DLOCK_DISCARD) != D3D_OK)
				Sleep(0);
		}

		this->FillVertexBuffers();

		a3->field_B8 = 1;

		while (v10->field_44->Unlock() != D3D_OK)
			Sleep(0);
		while (v10->field_48->Unlock() != D3D_OK)
			Sleep(0);

		if ((this->Data[4] & 1) != 0)
		{
			while (v10->field_50->Unlock() != D3D_OK)
				Sleep(0);
			while (v10->field_54->Unlock() != D3D_OK)
				Sleep(0);
		}
	}
	
	CDx* cdx = FFXI::CYy::CDx::instance;
	char cdx_field_921 = cdx->field_921;

	D3DCOLOR texture_factor = (unsigned __int8)((CYyModel::ambient_color * 0x80808080) >> 7) | (((HIWORD(CYyModel::ambient_color)
		* 0x8080) & 0x7F80 | 0xFFC00000) << 9) | ((unsigned __int16)(CYyModel::ambient_color >> 8) * (((unsigned int)0x80808080 >> 7) & 0xFFFE)) & 0xFF00;
	
	cdx->DXDevice->SetRenderState(D3DRS_TEXTUREFACTOR, texture_factor);
	cdx->DXDevice->SetTexture(0, NULL);
	drawbasic_local_tex_array[0] = nullptr;
	drawbasic_local_tex_array[1] = nullptr;
	cdx->DXDevice->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_DIFFUSE);
	cdx->DXDevice->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_DIFFUSE);
	cdx->DXDevice->SetTextureStageState(1, D3DTSS_COLOROP, D3DTOP_MODULATE2X);
	cdx->DXDevice->SetTextureStageState(1, D3DTSS_COLORARG2, D3DTA_TFACTOR);
	cdx->DXDevice->SetTextureStageState(1, D3DTSS_ALPHAOP, D3DTOP_MODULATE4X);
	cdx->DXDevice->SetTextureStageState(1, D3DTSS_ALPHAARG2, D3DTA_TFACTOR);
	cdx->DXDevice->SetTextureStageState(1, D3DTSS_TEXTURETRANSFORMFLAGS, D3DTTFF_DISABLE);
	cdx->DXDevice->SetTextureStageState(1, D3DTSS_TEXCOORDINDEX, 0);

	switch (this->Data[3]) {
	case 2u:
		a3->field_A9 = 1;
		break;
	case 3u:
		a3->field_A9 = 1;
		a3->field_AA = 1;
		break;
	case 4u:
		a3->field_A9 = 1;
		a3->field_AA = 1;
		a3->field_AB = 1;
		break;
	case 5u:
		a3->field_A9 = 1;
		a3->field_AA = 1;
		a3->field_AB = 1;
		a3->field_B0 = 1;
		a3->field_AC = 1;
		break;
	case 6u:
		a3->field_A9 = 1;
		a3->field_AA = 1;
		a3->field_AB = 1;
		a3->field_B0 = 1;
		break;
	case 0x12u:
		a3->field_AD = 1;
		break;
	case 0x22u:
		a3->field_AF = 1;
		break;
	case 0x32u:
		a3->field_AE = 1;
		break;
	default:
		break;
	}

	unsigned short* v32 = *(unsigned short**)(this->Data + 6);
	bool v29{ false };
	int v9 = 0;
	char v34 = 0;

	while (v32[0] != 0xFFFF) {
		if ((v32[0] & 0x8000) == 0) {
			switch (v32[0]) {
			case 0x5453:
				if (v10->field_68 != 0) {
					if (v29 == false) {
						Draw54Init(D3DPT_TRIANGLESTRIP, this, v10, v9, v32[1]);
					}
				}
				
				if (v29 == false) {
					Draw54(D3DPT_TRIANGLESTRIP, this, a3, v10, texture_factor, v9, v32[1], v34);
				}

				v9 += v32[1] + 2;
				*a7 += v32[1] + 2;
				v32 += 5 * v32[1] + 12;
				break;
			case 0x4353:
				v32 += v32[1] + 6;
				break;
			case 0x43:
				v32 += 5 * v32[1] + 2;
				break;
			case 0x54:
				if (v10->field_68 != 0) {
					if (v29 == false) {
						Draw54Init(D3DPT_TRIANGLELIST, this, v10, v9, v32[1]);
					}
				}
				if (v29 == false) {
					Draw54(D3DPT_TRIANGLELIST, this, a3, v10, texture_factor, v9, v32[1], v34);
				}
				*a6 += 3 * v32[1];
				v9 += 3 * v32[1];
				v32 += 15 * v32[1] + 2;
				break;
			}
		}
		else {
			int code = v32[0] & 0xFF;
			switch (code >> 4) {
			case 0:
			//Sets the texture for the mesh
			{
				CDx::instance->DXDevice->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_TEXTURE);
				CDx::instance->DXDevice->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
				if ((code & 1) == 0) {
					v32[0] |= 1;
					*(CYyTex**)(v32 + 1) = CYyDb::g_pCYyDb->pCYyTexMng->FindD3sTexUnder((char*)v32 + 2, this);
				}
				CYyTex* sometex = *(CYyTex**)(v32 + 1);
				if (sometex != nullptr && sometex != drawbasic_local_tex_array[0]) {
					drawbasic_local_tex_array[0] = sometex;
					if (sometex->RegTex != nullptr) {
						CDx::instance->DXDevice->SetTexture(0, sometex->RegTex);
					}
					else {
						CDx::instance->DXDevice->SetTexture(0, sometex->CubeTex);
					}
				}
				v32 += 9;
				break;
			}
			case 1:
				{	
				int v39[11];
				memcpy(v39, v32 + 1, sizeof(v39));
				int v15 = ((unsigned int)v39[3] >> 8) & 0xF;
				if (v10->field_69 != 0 && (v39[7] & 0xFF) != 0) {
					v15 = 8;
				}

				v29 = *(&a3->field_A8 + v15) != 0;
				const int light_count = sizeof(CXiSkeletonActor::g_light_arr) / sizeof(CXiSkeletonActor::g_light_arr[0]);
				float light_scale = *(float*)(v39 + 4);
				//first should have type 3
				for (int i = 0; i < light_count; ++i) {
					if (CXiSkeletonActor::g_light_arr[i].Type != NULL) {
						static D3DLIGHT8 temp_light = CXiSkeletonActor::g_light_arr[i];
						temp_light.Diffuse.r *= light_scale;
						temp_light.Diffuse.g *= light_scale;
						temp_light.Diffuse.b *= light_scale;
						cdx->SetLight(i, &temp_light);
					}
				}
				long r = (double)((CXiSkeletonActor::ActorAmbientLight >> 0) & 0xFF) * light_scale;
				long g = (double)((CXiSkeletonActor::ActorAmbientLight >> 8) & 0xFF) * light_scale;
				long b = (double)((CXiSkeletonActor::ActorAmbientLight >> 16) & 0xFF) * light_scale;
				long a = (CXiSkeletonActor::ActorAmbientLight >> 24) & 0xFF;
				D3DCOLOR ambient_light{};
				if ((unsigned int)r > 0xFF) {
					ambient_light = 0xFF;
				}
				else {
					ambient_light = (unsigned int)r;
				}
				if ((unsigned int)g > 0xFF) {
					ambient_light |= 0xFF00;
				}
				else {
					ambient_light |= (unsigned int)g << 8;
				}
				if ((unsigned int)b > 0xFF) {
					ambient_light |= 0xFF0000;
				}
				else {
					ambient_light |= (unsigned int)b << 16;
				}
				ambient_light |= (unsigned int)a << 24;
				cdx->DXDevice->SetRenderState(D3DRS_AMBIENT, ambient_light);

				if (((v39[3] >> 8) & 0xFF) != 0 && CXiSkeletonActor::s_veve_texinfo != nullptr
					&& *CXiSkeletonActor::s_veve_texinfo != nullptr && cdx_field_921 == 1) {
					v34 = 2;
					texture_factor = ((unsigned int)(__int64)(*(float*)&v39[9] * 0.5) << 24) | (texture_factor & 0xFFFFFF);
					cdx->DXDevice->SetRenderState(D3DRS_TEXTUREFACTOR, texture_factor);
					cdx->DXDevice->SetTextureStageState(1, D3DTSS_COLOROP, D3DTOP_MODULATEALPHA_ADDCOLOR); 
					cdx->DXDevice->SetTextureStageState(1, D3DTSS_COLORARG2, D3DTA_TEXTURE);
					cdx->DXDevice->SetTextureStageState(1, D3DTSS_ALPHAOP, D3DTOP_ADD);
					cdx->DXDevice->SetTextureStageState(1, D3DTSS_ALPHAARG2, D3DTA_TEXTURE);
					cdx->DXDevice->SetTransform(D3DTS_TEXTURE1, &CYyModel::SECOND_tex_transform);
					cdx->DXDevice->SetTextureStageState(1, D3DTSS_TEXTURETRANSFORMFLAGS, D3DTTFF_COUNT3);
					cdx->DXDevice->SetTextureStageState(1, D3DTSS_TEXCOORDINDEX, 1 | D3DTSS_TCI_CAMERASPACENORMAL);
					drawbasic_local_tex_array[1] = (CYyTex*)((CMoResource*)(*CXiSkeletonActor::s_veve_texinfo) + 1);
					if (drawbasic_local_tex_array[1]->RegTex != nullptr) {
						cdx->DXDevice->SetTexture(1, drawbasic_local_tex_array[1]->RegTex);
					}
					else {
						cdx->DXDevice->SetTexture(1, drawbasic_local_tex_array[1]->CubeTex);
					}
				}
				else if (*(float*)&v39[10] == 0.0 || cdx_field_921 != 1){
					v34 = 0;
					cdx->DXDevice->SetTextureStageState(1, D3DTSS_COLOROP, D3DTOP_MODULATE2X);
					cdx->DXDevice->SetTextureStageState(1, D3DTSS_COLORARG2, D3DTA_TFACTOR);
					cdx->DXDevice->SetTextureStageState(1, D3DTSS_ALPHAOP, D3DTOP_MODULATE4X);
					cdx->DXDevice->SetTextureStageState(1, D3DTSS_ALPHAARG2, D3DTA_TFACTOR);
					cdx->DXDevice->SetTextureStageState(1, D3DTSS_TEXTURETRANSFORMFLAGS, D3DTTFF_DISABLE);
					cdx->DXDevice->SetTextureStageState(1, D3DTSS_TEXCOORDINDEX, 0);
					unsigned int ambientColor = CYyModel::ambient_color;
					unsigned int factor = (unsigned int)v39[0];

					unsigned char red = ((ambientColor * factor) >> 7) & 0xFF;
					unsigned int blue = (((ambientColor >> 16) & 0xFFFF) * ((factor >> 7) & 0xFFFFFE00)) & 0xFF0000;
					unsigned short green = ((ambientColor >> 8) * ((factor >> 7) & 0xFFFE)) & 0xFF00;

					D3DCOLOR texture_factor = 0x80000000 | blue | green | red;
					cdx->DXDevice->SetRenderState(D3DRS_TEXTUREFACTOR, texture_factor);
				}
				else {
					v34 = 1;
					texture_factor = ((unsigned int)(__int64)(*(float*)&v39[9] * 0.5) << 24) | texture_factor & 0xFFFFFF;
					cdx->DXDevice->SetRenderState(D3DRS_TEXTUREFACTOR, texture_factor);
					cdx->DXDevice->SetTextureStageState(1, D3DTSS_COLOROP, D3DTOP_MODULATEALPHA_ADDCOLOR);
					cdx->DXDevice->SetTextureStageState(1, D3DTSS_COLORARG2, D3DTA_TEXTURE);
					cdx->DXDevice->SetTextureStageState(1, D3DTSS_ALPHAOP, D3DTOP_ADD);
					cdx->DXDevice->SetTextureStageState(1, D3DTSS_ALPHAARG2, D3DTA_TEXTURE);
					cdx->DXDevice->SetTransform(D3DTS_TEXTURE1, &CYyModel::FIRST_tex_transform);
					cdx->DXDevice->SetTextureStageState(1, D3DTSS_TEXTURETRANSFORMFLAGS, D3DTTFF_COUNT3);
					cdx->DXDevice->SetTextureStageState(1, D3DTSS_TEXCOORDINDEX, 1 | D3DTSS_TCI_CAMERASPACEREFLECTIONVECTOR);
					drawbasic_local_tex_array[1] = CXiSkeletonActor::s_cubemap_spec;
					if (drawbasic_local_tex_array[1]->RegTex != nullptr) {
						cdx->DXDevice->SetTexture(1, drawbasic_local_tex_array[1]->RegTex);
					}
					else {
						cdx->DXDevice->SetTexture(1, drawbasic_local_tex_array[1]->CubeTex);
					}
				}
				}
				v32 += 23;
				break;
			default:
				exit(0x01010101);
			}
		}
	}
}

//breakup functions for FillVertexBuffers
void FillBuffersSSE(CMoOs2* _this) {
	exit(0x1002E3CD);
}

void FillBuffers(CMoOs2* _this) {
	//Don't see where somestatic is ever initialized
	if (FFXI::CYyDb::g_pCYyDb->SomeStatic != 0) {
		exit(0x1002E421);
		return;
	}

	CYyOs2VtxBuffer* buff = *((CYyOs2VtxBuffer**)(_this->Data + 46));
	unsigned short* sp24 = *(unsigned short**)(_this->Data + 24);
	unsigned short* sp12 = *(unsigned short**)(_this->Data + 12);

	D3DXVECTOR3* vertbuffer_8E = (D3DXVECTOR3*)buff->field_8E;
	D3DXVECTOR3* vertbuffer_92 = (D3DXVECTOR3*)buff->field_92;
	D3DXVECTOR3* vertbuffer_96 = (D3DXVECTOR3*)buff->field_96;
	D3DXVECTOR3* vertbuffer_9A = (D3DXVECTOR3*)buff->field_9A;
	D3DXVECTOR3* buff_6E = (D3DXVECTOR3*)buff->field_6E;
	D3DXVECTOR3* buff_76 = (D3DXVECTOR3*)buff->field_76;

	unsigned char data2_80 = _this->Data[2] & 0x80;
	unsigned char data4_1 = _this->Data[4] & 0x1;

	for (unsigned short i = 0; i < short1; ++i) {
		unsigned short v12 = sp24[2 * i];

		unsigned short v14 = v12 & 0x7F;
		if (data2_80 != 0) { 
			v14 = sp12[v14]; 
		}

		CMoOs2::mat_matrix[v14].Vec3TransformDrop4(vertbuffer_8E, buff_6E + 2 * i);
		CMoOs2::mat_matrix[v14].Vec3TransformNormal(vertbuffer_92, buff_6E + 2 * i + 1);
		vertbuffer_8E += 1;
		vertbuffer_92 += 1;

		if (data4_1 != 0) {
			unsigned short v10 = (v12 >> 7) & 0x7F;
			if (data2_80 != 0) { 
				v10 = sp12[v10];	
			}

			CMoOs2::mat_matrix[v10].Vec3TransformDrop4(vertbuffer_96, buff_76 + 2 * i);
			CMoOs2::mat_matrix[v10].Vec3TransformNormal(vertbuffer_9A, buff_76 + 2 * i + 1);
			vertbuffer_96 += 1;
			vertbuffer_9A += 1;
		}
	}

	unsigned short some_count = **(unsigned short**)(_this->Data + 18);
	unsigned short* v172 = sp24 + 2 * some_count;

	for (int i = 0; i < short2; ++i) {
		unsigned short v17 = v172[2 * i];
		unsigned short v19 = v172[2 * i + 1];
		int v166 = v17 & 0x7F;
		int v21 = v19 & 0x7F;
		int v10 = (v17 >> 7) & 0x7F;
		int v11 = (v19 >> 7) & 0x7F;
		if (data2_80 != 0) {
			v166 = sp12[v166];
			v21 = sp12[v21];
			if (data4_1 != 0) {
				v10 = sp12[v10];
				v11 = sp12[v11];
			}
		}

		FFXI::Math::WMatrix* mone = CMoOs2::mat_matrix + v166;
		FFXI::Math::WMatrix* mtwo = CMoOs2::mat_matrix + v21;
		float* v6 = (float*)(buff_6E + 2 * some_count) + 14 * i;

		//8E calculations
		vertbuffer_8E->x = mone->_11 * v6[0] + mone->_21 * v6[2] + mone->_31 * v6[4] + mone->_41 * v6[6];
		vertbuffer_8E->y = mone->_12 * v6[0] + mone->_22 * v6[2] + mone->_32 * v6[4] + mone->_42 * v6[6];
		vertbuffer_8E->z = mone->_13 * v6[0] + mone->_23 * v6[2] + mone->_33 * v6[4] + mone->_43 * v6[6];

		vertbuffer_8E->x += mtwo->_11 * v6[1] + mtwo->_21 * v6[3] + mtwo->_31 * v6[5] + mtwo->_41 * v6[7];
		vertbuffer_8E->y += mtwo->_12 * v6[1] + mtwo->_22 * v6[3] + mtwo->_32 * v6[5] + mtwo->_42 * v6[7];
		vertbuffer_8E->z += mtwo->_13 * v6[1] + mtwo->_23 * v6[3] + mtwo->_33 * v6[5] + mtwo->_43 * v6[7];

		//92 calculations
		vertbuffer_92->x = mone->_11 * v6[8] + mone->_21 * v6[10] + mone->_31 * v6[12];
		vertbuffer_92->y = mone->_12 * v6[8] + mone->_22 * v6[10] + mone->_32 * v6[12];
		vertbuffer_92->z = mone->_13 * v6[8] + mone->_23 * v6[10] + mone->_33 * v6[12];

		vertbuffer_92->x += mtwo->_11 * v6[9] + mtwo->_21 * v6[11] + mtwo->_31 * v6[13];
		vertbuffer_92->y += mtwo->_12 * v6[9] + mtwo->_22 * v6[11] + mtwo->_32 * v6[13];
		vertbuffer_92->z += mtwo->_13 * v6[9] + mtwo->_23 * v6[11] + mtwo->_33 * v6[13];

		vertbuffer_8E += 1;
		vertbuffer_92 += 1;

		if (data4_1 != 0) {
			float* v32 = (float*)(buff_76 + 2 * short1) + 14 * i;
			mone = CMoOs2::mat_matrix + v10;
			mtwo = CMoOs2::mat_matrix + v11;
			
			//96 calculations
			vertbuffer_96->x = mone->_11 * v32[0] + mone->_21 * v32[2] + mone->_31 * v32[4] + mone->_41 * v32[6];
			vertbuffer_96->y = mone->_12 * v32[0] + mone->_22 * v32[2] + mone->_32 * v32[4] + mone->_42 * v32[6];
			vertbuffer_96->z = mone->_13 * v32[0] + mone->_23 * v32[2] + mone->_33 * v32[4] + mone->_43 * v32[6];
			
			vertbuffer_96->x += mtwo->_11 * v32[1] + mtwo->_21 * v32[3] + mtwo->_31 * v32[5] + mtwo->_41 * v32[7];
			vertbuffer_96->y += mtwo->_12 * v32[1] + mtwo->_22 * v32[3] + mtwo->_32 * v32[5] + mtwo->_42 * v32[7];
			vertbuffer_96->z += mtwo->_13 * v32[1] + mtwo->_23 * v32[3] + mtwo->_33 * v32[5] + mtwo->_43 * v32[7];

			//9A calculations
			vertbuffer_9A->x = mone->_11 * v32[8] + mone->_21 * v32[10] + mone->_31 * v32[12];
			vertbuffer_9A->y = mone->_12 * v32[8] + mone->_22 * v32[10] + mone->_32 * v32[12];
			vertbuffer_9A->z = mone->_13 * v32[8] + mone->_23 * v32[10] + mone->_33 * v32[12];

			vertbuffer_9A->x += mtwo->_11 * v32[9] + mtwo->_21 * v32[11] + mtwo->_31 * v32[13];
			vertbuffer_9A->y += mtwo->_12 * v32[9] + mtwo->_22 * v32[11] + mtwo->_32 * v32[13];
			vertbuffer_9A->z += mtwo->_13 * v32[9] + mtwo->_23 * v32[11] + mtwo->_33 * v32[13];
			
			vertbuffer_96 += 1;
			vertbuffer_9A += 1;
		}
	}

	unsigned short* v45_start = (unsigned short*)buff->field_5C;
	float* v6_start = (float*)(buff_6E + 2 * some_count);
	for (int i = 0; i < buff->field_60; ++i) {
		unsigned short v45 = v45_start[i];
		if (v45 >= short1) {
			int diff = v45 - short1;
			float* v6 = v6_start + 14 * diff;

			int v48 = v172[2 * diff];
			int v49 = v172[2 * diff + 1];
			int v53 = v48 & 0x7F;
			int v54 = v49 & 0x7F;
			int v10 = (v48 >> 7) & 0x7F;
			int v11 = (v49 >> 7) & 0x7F;
			if (data2_80 != 0) {
				v53 = sp12[v53];
				v54 = sp12[v54];
				if (data4_1 != 0) {
					v10 = sp12[v10];
					v11 = sp12[v11];
				}
			}

			FFXI::Math::WMatrix* mone = CMoOs2::mat_matrix + v53;
			FFXI::Math::WMatrix* mtwo = CMoOs2::mat_matrix + v54;

			//8E calculations
			vertbuffer_8E->x = mone->_11 * v6[0] + mone->_21 * v6[2] + mone->_31 * v6[4] + mone->_41 * v6[6];
			vertbuffer_8E->y = mone->_12 * v6[0] + mone->_22 * v6[2] + mone->_32 * v6[4] + mone->_42 * v6[6];
			vertbuffer_8E->z = mone->_13 * v6[0] + mone->_23 * v6[2] + mone->_33 * v6[4] + mone->_43 * v6[6];

			vertbuffer_8E->x += mtwo->_11 * v6[1] + mtwo->_21 * v6[3] + mtwo->_31 * v6[5] + mtwo->_41 * v6[7];
			vertbuffer_8E->y += mtwo->_12 * v6[1] + mtwo->_22 * v6[3] + mtwo->_32 * v6[5] + mtwo->_42 * v6[7];
			vertbuffer_8E->z += mtwo->_13 * v6[1] + mtwo->_23 * v6[3] + mtwo->_33 * v6[5] + mtwo->_43 * v6[7];

			//92 calculations
			vertbuffer_92->x = mone->_11 * v6[8] + mone->_21 * v6[10] + mone->_31 * v6[12];
			vertbuffer_92->y = mone->_12 * v6[8] + mone->_22 * v6[10] + mone->_32 * v6[12];
			vertbuffer_92->z = mone->_13 * v6[8] + mone->_23 * v6[10] + mone->_33 * v6[12];

			vertbuffer_92->x += mtwo->_11 * v6[9] + mtwo->_21 * v6[11] + mtwo->_31 * v6[13];
			vertbuffer_92->y += mtwo->_12 * v6[9] + mtwo->_22 * v6[11] + mtwo->_32 * v6[13];
			vertbuffer_92->z += mtwo->_13 * v6[9] + mtwo->_23 * v6[11] + mtwo->_33 * v6[13];

			vertbuffer_8E += 1;
			vertbuffer_92 += 1;

			if (data4_1 != 0) {
				float* v68 = (float*)(buff_76 + 2 * short1) + 14 * diff;
				mone = CMoOs2::mat_matrix + v10;
				mtwo = CMoOs2::mat_matrix + v11;

				//96 calculations
				vertbuffer_96->x = mone->_11 * v68[0] + mone->_21 * v68[2] + mone->_31 * v68[4] + mone->_41 * v68[6];
				vertbuffer_96->y = mone->_12 * v68[0] + mone->_22 * v68[2] + mone->_32 * v68[4] + mone->_42 * v68[6];
				vertbuffer_96->z = mone->_13 * v68[0] + mone->_23 * v68[2] + mone->_33 * v68[4] + mone->_43 * v68[6];

				vertbuffer_96->x += mtwo->_11 * v68[1] + mtwo->_21 * v68[3] + mtwo->_31 * v68[5] + mtwo->_41 * v68[7];
				vertbuffer_96->y += mtwo->_12 * v68[1] + mtwo->_22 * v68[3] + mtwo->_32 * v68[5] + mtwo->_42 * v68[7];
				vertbuffer_96->z += mtwo->_13 * v68[1] + mtwo->_23 * v68[3] + mtwo->_33 * v68[5] + mtwo->_43 * v68[7];

				//9A calculations
				vertbuffer_9A->x = mone->_11 * v68[8] + mone->_21 * v68[10] + mone->_31 * v68[12];
				vertbuffer_9A->y = mone->_12 * v68[8] + mone->_22 * v68[10] + mone->_32 * v68[12];
				vertbuffer_9A->z = mone->_13 * v68[8] + mone->_23 * v68[10] + mone->_33 * v68[12];

				vertbuffer_9A->x += mtwo->_11 * v68[9] + mtwo->_21 * v68[11] + mtwo->_31 * v68[13];
				vertbuffer_9A->y += mtwo->_12 * v68[9] + mtwo->_22 * v68[11] + mtwo->_32 * v68[13];
				vertbuffer_9A->z += mtwo->_13 * v68[9] + mtwo->_23 * v68[11] + mtwo->_33 * v68[13];

				vertbuffer_96 += 1;
				vertbuffer_9A += 1;
			}
		}
		else {
			unsigned int v46 = sp24[2 * v45];
			int v167 = v46 & 0x7F;
			int v10 = (v46 >> 7) & 0x7F;
			if (data2_80 != 0) {
				v167 = sp12[v167];
				if (data4_1 != 0) {
					v10 = sp12[v10];
				}
			}

			CMoOs2::mat_matrix[v167].Vec3TransformDrop4(vertbuffer_8E, (D3DXVECTOR3*)buff->field_6E + 2 * v45);
			CMoOs2::mat_matrix[v167].Vec3TransformNormal(vertbuffer_92, (D3DXVECTOR3*)buff->field_6E + 2 * v45 + 1);
			
			vertbuffer_8E += 1;
			vertbuffer_92 += 1;

			if (data4_1 != 0) {
				CMoOs2::mat_matrix[v10].Vec3TransformDrop4(vertbuffer_96, (D3DXVECTOR3*)buff->field_76 + 2 * v45);
				CMoOs2::mat_matrix[v10].Vec3TransformNormal(vertbuffer_9A, (D3DXVECTOR3*)buff->field_76 + 2 * v45 + 1);
				vertbuffer_96 += 1;
				vertbuffer_9A += 1;
			}
		}
	}
}

void FFXI::CYy::CMoOs2::FillVertexBuffers()
{
	if (Globals::g_pCenv->CPU_SSE_Support != 0 || Globals::g_pCenv->field_C != 0) {
		FillBuffers(this);
	}
	else {
		FillBuffers(this);
	}
}
