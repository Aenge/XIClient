#pragma once
#include "MemoryManagedObject.h"
namespace FFXI {
	namespace CYy {
		class CYyMotionQue;
		class BoneTransformState;
		class CMoMo2;
		class CYyMotionQueList : public MemoryManagedObject {
		public:
			static const BaseGameObject::ClassInfo CYyMotionQueListClass;
			virtual const BaseGameObject::ClassInfo* GetRuntimeClass() override final;
			CYyMotionQueList();
			~CYyMotionQueList();
			void AppendSync(CMoMo2**, float, float, int, float, float, float, float);
			void Append(CMoMo2**, float, float, int, float, float, float, float);
			void Remove(CYyMotionQue*);
			void DeleteAll();
			int GetNbQue();
			void UpdateLayer(int, BoneTransformState*);
			bool IsExistZombiQue();
			CYyMotionQue* GetParent(CYyMotionQue*);
			CYyMotionQue* GetTail();
			CYyMotionQue* Head;
			float field_8;
			float blendAccumulator;
			float field_10;
		};
	}
}