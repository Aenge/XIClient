#include "CXiCollisionActor.h"
#include "XiAtelBuff.h"
#include "RidStruct.h"
#include "CYyDb.h"
#include "CTsZoneMap.h"
#include "Globals.h"
using namespace FFXI::CYy;

const BaseGameObject::ClassInfo CXiCollisionActor::CXiCollisionActorClass{
	"CXiCollisionActor", sizeof(CXiCollisionActor), &CXiControlActor::CXiControlActorClass
};

const BaseGameObject::ClassInfo* FFXI::CYy::CXiCollisionActor::GetRuntimeClass()
{
	return &CXiCollisionActor::CXiCollisionActorClass;
}

void CheckSomeStructs(FFXI::CYy::MapCollisionData::GroundStatus* a1) {
	a1->field_24[3] = nullptr;
	a1->field_24[2] = nullptr;
	a1->field_24[1] = nullptr;
	a1->field_24[0] = nullptr;
	if (a1->field_20 != 0) {
		FFXI::CYy::SomeStruct* ss = FFXI::CYy::CTsZoneMap::SomeStructs + (a1->field_20 - 1u);
		if ((ss->field_0 & 0xFFFFFF00) != 'bgl\0')
			a1->field_24[0] = &ss->field_8;
	}
	if (a1->field_21 != 0) {
		FFXI::CYy::SomeStruct* ss = FFXI::CYy::CTsZoneMap::SomeStructs + (a1->field_21 - 1u);
		if ((ss->field_0 & 0xFFFFFF00) != 'bgl\0')
			a1->field_24[1] = &ss->field_8;
	}
	if (a1->field_22 != 0) {
		FFXI::CYy::SomeStruct* ss = FFXI::CYy::CTsZoneMap::SomeStructs + (a1->field_22 - 1u);
		if ((ss->field_0 & 0xFFFFFF00) != 'bgl\0')
			a1->field_24[2] = &ss->field_8;
	}
	if (a1->field_23 != 0) {
		FFXI::CYy::SomeStruct* ss = FFXI::CYy::CTsZoneMap::SomeStructs + (a1->field_23 - 1u);
		if ((ss->field_0 & 0xFFFFFF00) != 'bgl\0')
			a1->field_24[3] = &ss->field_8;
	}
}

char FFXI::CYy::CXiCollisionActor::OnMove()
{
	char control_result = this->CXiControlActor::OnMove();
	if (control_result != 0) {
		return control_result;
	}

	bool flag_one{ false };
	bool flag_two{ true };
	if (this->AtelBuffer != nullptr) {
		if ((this->AtelBuffer->field_120 & 0x80) != 0
		|| (this->AtelBuffer->field_120 & 0x100) != 0) {
			flag_one = true;
		}

		if ((this->AtelBuffer->field_120 & 0x100000) != 0
		|| (this->AtelBuffer->field_120 & 0x200000) != 0
		|| (this->AtelBuffer->field_128 & 0x4000) != 0) {
			flag_two = false;
		}
	}

	D3DXVECTOR3 v53{};
	v53.x = this->field_5C4.x;
	v53.y = this->field_5C4.y;
	v53.z = this->field_5C4.z;

	D3DXVECTOR3 v49{};
	v49.x = this->field_D4.x;
	v49.y = this->field_D4.y;
	v49.z = this->field_D4.z;

	bool flag_three{ true };

	if (this->VirtActor201() == true
		|| this->VirtActor204() == true
		|| this->IsFishingRod() == true
		|| this->VirtActor209() == true
		|| this->VirtActor212() == true)
	{
		flag_three = false;
	}

	if (this->VirtActor92() != 0) {
		this->field_F4 = 0.0f;
	}

	D3DXVECTOR3 v52 = v49 - v53;
	D3DXVECTOR3 v61 = v53;

	float mag_sq = v52.z * v52.z + v52.y * v52.y + v52.x * v52.x;
	bool flag_four{ false };
	bool flag_five{ false };
	bool do_35{ false };

	if (flag_three == true) {
		if (this->field_5E0 == 0 || sqrt(mag_sq) > 0.0000001f || this->VirtActor242() == 1) {
			if (this->AmIControlActor() == false && this->field_102 == 0) {
				if (this->field_5E1 == 0
				&& this->field_BC > 10.0
				&& this->VirtActor242() == 0
				&& flag_one == false) {
					this->field_5E0 = 1;
				}
				else {
					do_35 = true;
					this->VirtActor243(0);
					this->field_5E0 = CYyDb::g_pTsZoneMap->CharaCollisionFast(&v49, &v49, 1);
					if (this->field_5E0 == 0) {
						flag_four = true;
					}
					flag_five = true;
				}
			}
			else {
				this->VirtActor243(0);
				D3DXVECTOR3 va2{};
				this->field_5E0 = CYyDb::g_pTsZoneMap->collisionData.CheckCharCollision(&v53, &v49, &va2) != 0;
				if (Globals::CheckFloat(va2.x) == true && Globals::CheckFloat(va2.y) == true && Globals::CheckFloat(va2.z) == true) {
					v49 = va2;
					do_35 = true;
				}
			}
		}
	}
	
	if (do_35 == true) {
		if (this->AmIControlActor() == true) {
			v53 = v49;
			D3DXVECTOR3 v59 = v49;
			v59.y = 0.0;
			D3DXVECTOR3 v62 = v61;
			v62.y = 0.0;
			D3DXVECTOR3 vdiff = v59 - v62;
			float vec3mag = sqrt(vdiff.x * vdiff.x + vdiff.z * vdiff.z);
			float v19 = CYyDb::g_pCYyDb->CheckTick() * 0.040833335f;
			if (vec3mag > 0.01f) {
				if (v19 > vec3mag) {
					v19 = vec3mag;
				}
			}
			float v28 = v19 + this->field_F4;
			this->field_F4 = v28;
			if (v28 > 1.0) {
				this->field_F4 = 1.0;
			}
			if (this->field_5F4 == 0) {
				this->field_5F4 = 1;
				if (this->field_F4 > 0.4f) {
					this->field_F4 = 0.4f;
				}
			}
			v49.y += this->field_F4;
			this->field_5E0 = 0;
			D3DXVECTOR3 va2{};
			if (CYyDb::g_pTsZoneMap->collisionData.CheckCharCollision(&v53, &v49, &va2) == true) {
				this->field_5E0 = 1;
			}
			//It doesn't seem like this stack store function is ever used except for debugging
			//sub(&v49, &va2);
			v49 = va2;
			D3DXVECTOR3 v60{};
			if (CYyDb::g_pTsZoneMap->CharaCollisionFast(&v49, &v60, 0) == false) {
				flag_four = true;
			}
			CYyDb::g_pTsZoneMap->collisionData.GetGroundStatus(&CYyDb::g_pTsZoneMap->groundStatus);
		}
		else if (flag_five == false)
		{
			D3DXVECTOR3 v60{};
			if (CYyDb::g_pTsZoneMap->CharaCollisionFast(&v49, &v60, 0) == false) {
				flag_four = true;
			}
		}
		if (flag_four == false) {
			FFXI::CYy::MapCollisionData::GroundStatus v54{};
			CYyDb::g_pTsZoneMap->GetGroundStatus(&v54);
			CheckSomeStructs(&v54);
			this->SetGroundNormal(&v54.field_8);
			this->VirtActor123(v54.field_0);
			this->VirtActor125(v54.field_4);
			if (this->GetAtelBufferField13CBit12() == 0) {
				if (this->GetAtelBufferField208() != 0) {
					v54.fourcc = this->GetAtelBufferField208();
				}
				this->VirtActor87(v54.fourcc);
			}
			this->VirtActor128(v54.field_24);
			this->SetIndoorFlag(v54.field_18 != 0);
			if (this->IsBlendNormal() == true) {
				this->CorrectNormal();
			}
		}
	}
	else {
		this->field_5F0 += 1;
		if (this->field_5F0 > 120) {
			FFXI::CYy::MapCollisionData::GroundStatus v54{};
			CYyDb::g_pTsZoneMap->GetGroundStatus(&v54);
			CheckSomeStructs(&v54);
			this->SetGroundNormal(&v54.field_8);
			this->VirtActor123(v54.field_0);
			this->VirtActor125(v54.field_4);
			if (this->GetAtelBufferField13CBit12() == 0) {
				if (this->GetAtelBufferField208() != 0) {
					v54.fourcc = this->GetAtelBufferField208();
				}
				this->VirtActor87(v54.fourcc);
			}
			this->VirtActor128(v54.field_24);
			this->SetIndoorFlag(v54.field_18 != 0);
			if (this->IsBlendNormal() == true) {
				this->CorrectNormal();
			}
		}
	}

	//label 70
	if (flag_two == true) {
		this->field_5C4.x = v49.x;
		this->field_5C4.y = v49.y;
		this->field_5C4.z = v49.z;
	}
	else {
		this->field_5C4.x = this->field_D4.x;
		this->field_5C4.y = this->field_D4.y;
		this->field_5C4.z = this->field_D4.z;
	}

	v49.x = this->field_5C4.x;
	v49.y = this->field_5C4.y;
	v49.z = this->field_5C4.z;
	
	RidStruct* rs = CYyDb::g_pTsZoneMap->RidManager.LiftRectHitCheck(&v53, &v49);
	if (rs != nullptr) {
		exit(0x100A3CA0);
	}
	else {
		this->field_5E2 = 0;
		if (flag_two == true && flag_four == true) {
			this->field_5C4.x = v61.x;
			this->field_5C4.y = v61.y;
			this->field_5C4.z = v61.z;
			this->field_F4 = 0.0f;
		}
	}

	if (this->field_103 == 0 && this->field_104 == 0) {
		this->field_34 = this->field_5C4;
		this->field_C4 = this->field_5C4;
		this->SetPos(&this->field_5C4);
	}

	return 0;
}

FFXI::CYy::CXiCollisionActor::CXiCollisionActor()
{
	this->field_5E4 = 0;
	this->field_5E0 = 0;
	this->field_5E1 = 0;
	this->field_5E2 = 0;
	this->field_5E8 = rand() % 120;

	this->field_160 = { 0.0, -1.0, 0.0, 1.0 };

	this->field_174 = 0;
	this->field_178 = this->field_5C4.y;
	this->field_17C = 0;
	this->field_5EC = 1;
	this->field_5F4 = 0;
	this->field_5F0 = rand() % 120;
}

int FFXI::CYy::CXiCollisionActor::GetAtelBufferField208()
{
	if (this->AtelBuffer == nullptr) {
		return 0;
	}

	return this->AtelBuffer->field_208;
}

unsigned int FFXI::CYy::CXiCollisionActor::GetAtelBufferField13CBit12()
{
	if (this->AtelBuffer == nullptr) {
		return 0;
	}

	return (this->AtelBuffer->field_13C >> 12) & 1;
}

void FFXI::CYy::CXiCollisionActor::CorrectNormal()
{
	D3DXVECTOR4 v14{}, v15{}, v16{}, v17{};
	this->VirtActor103(10, &v14);
	this->VirtActor103(11, &v15);
	this->VirtActor103(8, &v16);
	this->VirtActor103(9, &v17);
	D3DXVECTOR3 v11{}, v13{};
	v11.x = v14.x + v15.x;
	v11.y = v14.y + v15.y;
	v11.z = v14.z + v15.z;
	v13.x = v16.x + v17.x;
	v13.y = v16.y + v17.y;
	v13.z = v16.z + v17.z;
	v11 *= 0.5f;
	v13 *= 0.5f;
	D3DXVECTOR3 va3{}, va2{};
	if (FFXI::CYyDb::g_pTsZoneMap->CharaCollisionFast(&v11, &va3, 1) == true
		&& FFXI::CYyDb::g_pTsZoneMap->CharaCollisionFast(&v13, &va2, 1) == true
		&& (va3.x != va2.x || va3.z != va2.z)) 
	{
		D3DXVECTOR3 v12 = va2 - va3;
		D3DXVECTOR3 v7{ 0.0f, -1.0f, 0.0f };
		Globals::Vec3Outer(&v7, &v12, &v7);
		D3DXVECTOR4 v6{};
		Globals::Vec3Outer((D3DXVECTOR3*) & v6, &v12, &v7);
		Globals::Vec3Normalize((D3DXVECTOR3*) & v6);
		if (v6.y > 0.0f) {
			v6.x *= -1.0f;
			v6.y *= -1.0f;
			v6.z *= -1.0f;
		}
		v6.w = 1.0f;
		this->SetGroundNormal(&v6);
	}
		
}

void FFXI::CYy::CXiCollisionActor::VirtActor87(int a2)
{
	this->field_5E4 = a2;
}

int FFXI::CYy::CXiCollisionActor::VirtActor88()
{
	return this->field_5E4;
}

char FFXI::CYy::CXiCollisionActor::VirtActor92()
{
	return this->field_5E0;
}

char FFXI::CYy::CXiCollisionActor::IsOnLift()
{
	return this->field_5E2;
}

D3DXVECTOR4* FFXI::CYy::CXiCollisionActor::GetPos()
{
	return &this->field_5C4;
}

char FFXI::CYy::CXiCollisionActor::VirtActor242()
{
	return this->field_5EC;
}

void FFXI::CYy::CXiCollisionActor::VirtActor243(char a2)
{
	this->field_5EC = a2;
}

bool FFXI::CYy::CXiCollisionActor::IsBlendNormal()
{
	return false;
}
