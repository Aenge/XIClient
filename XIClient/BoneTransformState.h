#pragma once
#include "KzFQuat.h"
namespace FFXI {
	namespace CYy {
		class BoneTransformState {
		public:
			FFXI::Math::KzFQuat Rotation;
			D3DXVECTOR3 Translation;
			D3DXVECTOR3 Scaling;
			D3DXVECTOR3 AccumulatedScaling;
		};
	}
}