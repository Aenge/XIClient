#include "CMoSk2.h"
using namespace FFXI::CYy;

void* FFXI::CYy::CMoSk2::GetTransformTable()
{
	return this->GetBoneData(this->GetBoneCount());
}

FFXI::CYy::CMoSk2::CMoSk2()
{
	//nullsub
}

void FFXI::CYy::CMoSk2::Open()
{
	if (this->GetVersion() != 0) {
		//version error
	}
}

void FFXI::CYy::CMoSk2::Close()
{
	//nullsub
}

unsigned char FFXI::CYy::CMoSk2::GetVersion()
{
	return this->Data[0];
}

unsigned short FFXI::CYy::CMoSk2::GetBoneCount()
{
	return *(unsigned short*)(this->Data + 2);
}

FFXI::CYy::CMoSk2::BoneData* FFXI::CYy::CMoSk2::GetBoneData(unsigned short boneIndex)
{
	return (FFXI::CYy::CMoSk2::BoneData*)(this->Data + 4 + boneIndex * sizeof(BoneData));
}

unsigned short FFXI::CYy::CMoSk2::GetBoneTransformCount()
{
	return *(unsigned short*)this->GetTransformTable();
}

FFXI::CYy::CMoSk2::BoneTransform* FFXI::CYy::CMoSk2::GetBoneTransform(unsigned short transformId)
{
	void* transformTable = (char*)this->GetTransformTable() + 4;
	return (BoneTransform*)transformTable + transformId;
}

FFXI::Math::BoundingBox3D* FFXI::CYy::CMoSk2::GetBoundingBox(unsigned char boxIndex)
{
	void* boxTable = this->GetBoneTransform(this->GetBoneTransformCount());
	return (Math::BoundingBox3D*)boxTable + boxIndex;
}
