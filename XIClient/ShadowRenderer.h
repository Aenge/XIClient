#pragma once
#define WIN32_LEAN_AND_MEAN
#include "d3dx9math.h"
#include "d3d8to9/d3d8.hpp"
namespace FFXI {
	namespace CYy {
		class CYyTex;
		class ShadowRenderer {
		public:
			static float g_mss1_float;
			static D3DCOLOR s_color;
			static CYyTex* s_texture1, *s_texture2;
			static IDirect3DTexture8* g_pSomeTexture1;
			static IDirect3DTexture8* g_pSomeTexture2;
			static void clean_texs();
			ShadowRenderer();
			virtual ~ShadowRenderer();
			void Init(D3DXVECTOR3*);
			void Render(D3DXVECTOR4*);
			bool PrepareToRender(CYyTex**, D3DCOLOR);
			bool FinishRender(char);
			char field_4;
			char field_5;
			char field_6;
			char field_7;
			IDirect3DTexture8* field_8;
			IDirect3DTexture8* field_C;
			int field_10;
			D3DXVECTOR3 field_14;
			D3DXVECTOR3 field_20;
		};
	}
}