#pragma once
#include "d3d8to9/d3d8.hpp"
namespace FFXI {
	namespace CYy {
		class TextureBlender {
		public:
			static bool some_flag;
			static D3DCOLOR some_color;
			static void SetStatics(D3DCOLOR, bool);
			TextureBlender();
			virtual ~TextureBlender();
			void PrepareViewport();
			void CleanViewport();
			void Render();
			bool field_4;
			IDirect3DTexture8* field_8[4];
		};
	}
}