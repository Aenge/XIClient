#include "CMoMzb.h"
#include "Globals.h"
#include "CYyDb.h"
#include "CTsZoneMap.h"

FFXI::CYy::CMoMzb::CMoMzb()
{
}

void FFXI::CYy::CMoMzb::Open()
{
	this->GetRoot();
	if (this->ResourceID == 'enuf') return;

	ResourceContainer* head = (ResourceContainer*)this->GetRoot();
	CYyDb::g_pCYyDb->g_pTsZoneMap->OpenMzb(this + 1, head);
}
