#include "CTkMenuPrimitive.h"
#include "CTkMenuCtrlData.h"
#include "TkManager.h"
#include "CTkMenuMng.h"
using namespace FFXI::CTk;

void FFXI::CTk::CTkMenuPrimitive::OnInitialUpdatePrimitive()
{
}

void FFXI::CTk::CTkMenuPrimitive::OnDestroyPrimitive()
{
}

void FFXI::CTk::CTkMenuPrimitive::OnRemovePrimitive()
{
}

void FFXI::CTk::CTkMenuPrimitive::OnDrawPrimitive()
{
	this->field_10 = 1;
}

void FFXI::CTk::CTkMenuPrimitive::OnUpdatePrimitive()
{
}

void FFXI::CTk::CTkMenuPrimitive::OnKeyDown(short, short)
{
}

void FFXI::CTk::CTkMenuPrimitive::PrimVirt7()
{
}

void FFXI::CTk::CTkMenuPrimitive::PrimVirt8()
{
}

void FFXI::CTk::CTkMenuPrimitive::OnActive(bool)
{
}

void FFXI::CTk::CTkMenuPrimitive::OnDrawCalc(bool)
{
}

void FFXI::CTk::CTkMenuPrimitive::OnZoneChange(char, char)
{
	//nullsub
}

void FFXI::CTk::CTkMenuPrimitive::PrimVirt12()
{
}

void FFXI::CTk::CTkMenuPrimitive::PrimVirt13()
{
}

void FFXI::CTk::CTkMenuPrimitive::PrimVirt14()
{
}

void FFXI::CTk::CTkMenuPrimitive::PrimVirt15()
{
}

void FFXI::CTk::CTkMenuPrimitive::PrimVirt16(int)
{
}

bool FFXI::CTk::CTkMenuPrimitive::IsDrawWindow()
{
	if (this->MenuCtrlData == nullptr) {
		return false;
	}

	if (this->MenuCtrlData->field_10 == 14) {
		return false;
	}

	CTkNode* node = TkManager::g_CTkMenuMng.MCDList.GetHeadPosition();
	while (node != nullptr) {
		CTkMenuCtrlData* mcd = TkManager::g_CTkMenuMng.GetDrawNext(&node);
		if (mcd == this->MenuCtrlData) {
			return true;
		}
	}

	return false;
}
