#include "FsKeyboard.h"
#include "InputMng.h"
#include "CApp.h"
using namespace FFXI::Input;

void FFXI::Input::FsKeyboard::SetFlag1(unsigned char a1)
{
	this->KeyFlags[a1] |= 1u;
}

void FFXI::Input::FsKeyboard::SetFlag2(unsigned char a1)
{
	this->KeyFlags[a1] |= 2u;
}

void FFXI::Input::FsKeyboard::SetFlag4(unsigned char a1)
{
	this->KeyFlags[a1] |= 4u;
	this->field_100 = 1;
}

void FFXI::Input::FsKeyboard::SetFlag8(unsigned char a1)
{
	this->KeyFlags[a1] |= 8u;
}

void FFXI::Input::FsKeyboard::SetFlag10(unsigned char a1)
{
	this->KeyFlags[a1] |= 10u;
}

bool FFXI::Input::FsKeyboard::rept(unsigned char a1)
{
	return (this->KeyFlags[a1] & 1) != 0;
}

bool FFXI::Input::FsKeyboard::just(unsigned char a1)
{
	return (this->KeyFlags[a1] & 2) != 0;
}

bool FFXI::Input::FsKeyboard::CheckFlag4(unsigned char a1)
{
	return (this->KeyFlags[a1] & 4) != 0;
}

bool FFXI::Input::FsKeyboard::CheckFlag8(unsigned char a1)
{
	throw "NOT IMPLEMENTED";
}

bool FFXI::Input::FsKeyboard::CheckFlag10(unsigned char a1)
{
	throw "NOT IMPLEMENTED";
}

bool FFXI::Input::FsKeyboard::reptoff(unsigned char a1)
{
	if ((this->KeyFlags[a1] & 1) == 0)
		return false;

	this->KeyFlags[a1] &= ~1u;
	return true;
}

bool FFXI::Input::FsKeyboard::justoff(unsigned char a1)
{
	if ((this->KeyFlags[a1] & 2) == 0)
		return false;

	this->KeyFlags[a1] &= ~2u;
	return true;
}

void FFXI::Input::FsKeyboard::ClearFlags821(unsigned char a1)
{
	this->KeyFlags[a1] &= 0xF4u;
}

void FFXI::Input::FsKeyboard::ClearFlags(unsigned char a1)
{
	this->KeyFlags[a1] = 0;
}

float FFXI::Input::FsKeyboard::getAnalog(int a2)
{
	FFXI::Input::InputMng* im = FFXI::CYy::CApp::g_pInputMng;
	switch (a2) {
	case 1:
		if (im->CheckThing(77) == true) {
			return 1.0;
		}
		if (im->CheckThing(75) == true) {
			return -1.0;
		}
		break;
	case 2:
		if (im->CheckThing(72) == true) {
			return -1.0;
		}
		if (im->CheckThing(80) == true) {
			return 1.0;
		}
		break;
	case 3:
		if (this->CheckFlag4(203u) == true) {
			return -1.0;
		}
		if (this->CheckFlag4(205u) == true) {
			return 1.0;
		}
		break;
	case 4:
		if (this->CheckFlag4(200u) == true) {
			return -1.0;
		}
		if (this->CheckFlag4(208u) == true) {
			return 1.0;
		}
		break;
	case 5:
		if (this->CheckFlag4(54u) == false && this->CheckFlag4(42u)) {
			return 0.0;
		}
		if (this->CheckFlag4(77u) == true) {
			return 1.0;
		}
		if (this->CheckFlag4(75u) == true) {
			return -1.0;
		}
		break;
	case 6:
		if (this->CheckFlag4(73u) == false && this->CheckFlag4(201u)) {
			return 1.0;
		}
		if (this->CheckFlag4(81u) == true) {
			return -1.0;
		}
		if (this->CheckFlag4(209u) == true) {
			return -1.0;
		}
		break;
	default:
		break;
	}

	return 0.0;
}
