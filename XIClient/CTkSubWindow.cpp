#include "CTkSubWindow.h"
#include "TkManager.h"
#include "CTkMsbDataList.h"
#include "Strings.h"
#include "CTkMenuCtrlData.h"
#include "_49SubList.h"

int FFXI::CTk::CTkSubWindow::GetChatMode()
{
	//TODO
	return 1;
}
FFXI::CTk::CTkSubWindow::CTkSubWindow()
{
	this->field_28 = 0;
	_49SubList*** file = TkManager::g_CTkMenuDataList.FindMenuShapeFile(Constants::Strings::MenuWindowPs);
	if (file == nullptr) {
		return;
	}

	for (int i = 0; i < SUB_TYPE_COUNT; ++i) {
		this->field_38[i] = (*file)[tksubshapeidx[i]];
	}

	for (int i = 0; i < SUB_TYPE_COUNT; ++i) {
		this->field_5C[i] = (*file)[tksubtitleidx[2 * i]];
		this->field_80[i] = (*file)[tksubtitleidx[2 * i + 1]];
	}

	this->field_A4 = (*file)[0x2E4];
}

FFXI::CTk::CTkSubWindow::~CTkSubWindow()
{
	//nullsub
}

void FFXI::CTk::CTkSubWindow::OnInitialUpdatePrimitive()
{
	this->MenuCtrlData->GetWindowLocate(&this->field_14);
}

void FFXI::CTk::CTkSubWindow::OnDrawPrimitive()
{
	switch (this->field_28) {
	case 0:
		this->DrawChat();
		break;
	/*case 1:
		break;
	case 2:
	case 10:
		break;
	case 3:
		break;
	case 4:
		break;
	case 5:
		break;
	case 6:
		break;
	case 7:
		break;
	case 8:
		break;
	case 9:
		break;*/
	default:
		exit(0x101494DB);
		break;
	}
}

void FFXI::CTk::CTkSubWindow::OnUpdatePrimitive()
{
	this->MenuCtrlData->GetWindowLocate(&this->field_14);
}

void FFXI::CTk::CTkSubWindow::DrawChat()
{
	this->field_38[0]->Draw(this->field_14.Left + 5, this->field_14.Top + 8, 0x80808080, 0, 0);
	if (this->field_1C >= 1 && this->field_1C < SUB_TYPE_COUNT) {
		this->field_38[this->field_1C]->Draw(this->field_14.Left + 15, this->field_14.Top + 26, 0x80808080, 0, 0);
	}
}

short FFXI::CTk::CTkSubWindow::tksubshapeidx[SUB_TYPE_COUNT] = {
	0x011B, 0x011C, 0x0121, 0x0122, 0x011D, 0x011E, 0x011F, 0x02C5, 0x02C6
};

short FFXI::CTk::CTkSubWindow::tksubtitleidx[2 * SUB_TYPE_COUNT] = {
	0x011A, 0x011A, 0x0114, 0x02DD, 0x0115, 0x02E3, 0x0116, 0x0116, 0x0117, 0x02DE,
	0x0118, 0x02DF, 0x0119, 0x02E0, 0x02C7, 0x02E1, 0x02C8, 0x02E2
};