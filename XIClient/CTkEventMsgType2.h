#pragma once
#include "CTkEventMsgBase.h"
namespace FFXI {
	namespace CTk {
		class CTkEventMsgType2 : public CTkEventMsgBase {
		public:
			CTkEventMsgType2(CTkDrawMessageWindow*);
		};
	}
}