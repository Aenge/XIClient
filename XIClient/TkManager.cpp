#include "TkManager.h"
#include "Globals.h"
#include "CTkMouse.h"
#include "CYyDb.h"
#include "PrimMng.h"
#include "CTkMenuMng.h"
#include "StDancer.h"
#include "CTkInputCtrl.h"
#include "CIwOnePic.h"
#include "CTkHelp.h"
#include "CTkTitle.h"
#include "CIwOkMenu.h"
#include "CTkDrawMessageWindowOne.h"
#include "CTkDrawMessageWindowTwo.h"
#include "CTkSubWindow.h"
#include "CTkSpoolMsgType1.h"
#include "CTkSpoolMsgType2.h"
#include "CTkEventMsgType1.h"
#include "CTkEventMsgType2.h"
#include "CTkSubWindow.h"
using namespace FFXI::CTk;

CTkMenuMng TkManager::g_CTkMenuMng{};
CTkMsbDataList TkManager::g_CTkMenuDataList{};
CTkMouse* TkManager::g_pCTkMouse{ nullptr };
CTkInputCtrl* TkManager::g_pCTkInputCtrl{ nullptr };
StDancer* TkManager::g_pStDancer{ nullptr };
CTkMsgWinData* TkManager::MsgWinData1{ nullptr };
CTkMsgWinData* TkManager::MsgWinData2{ nullptr };
FsMenuDraw* TkManager::g_pFsMenuDraw{ nullptr };
void FFXI::CTk::TkManager::TkInit()
{
	//sub
	g_CTkMenuMng.Init();
	g_CTkMenuMng.field_7C = 0;
	g_CTkMenuMng.field_7E = 0;
	g_CTkMenuMng.UIXRes = CYyDb::g_pCYyDb->UIXRes;
	g_CTkMenuMng.UIYRes = CYyDb::g_pCYyDb->UIYRes;

	//sub //TODO
	g_pCTkMouse = new CTkMouse();
	//sub //TODO
	
	g_pCTkInputCtrl = new CTkInputCtrl();
	//sub //TODO
	PrimMng::g_pTkDrawMessageWindow = new CTkDrawMessageWindowOne(MsgWinData1);
	PrimMng::g_pTkDrawMessageWindow2 = new CTkDrawMessageWindowTwo(MsgWinData2);

	PrimMng::g_pTkSpoolMsg = new CTkSpoolMsgType1(PrimMng::g_pTkDrawMessageWindow);
	PrimMng::g_pTkDrawMessageWindow->field_1C = PrimMng::g_pTkSpoolMsg;

	PrimMng::g_pTkSpoolMsg2 = new CTkSpoolMsgType2(PrimMng::g_pTkDrawMessageWindow2);
	PrimMng::g_pTkDrawMessageWindow2->field_1C = PrimMng::g_pTkSpoolMsg2;

	PrimMng::g_pTkEventMsg = new CTkEventMsgType1(PrimMng::g_pTkDrawMessageWindow);
	PrimMng::g_pTkDrawMessageWindow->field_20 = PrimMng::g_pTkEventMsg;

	PrimMng::g_pTkEventMsg2 = new CTkEventMsgType2(PrimMng::g_pTkDrawMessageWindow2);
	PrimMng::g_pTkDrawMessageWindow2->field_20 = PrimMng::g_pTkEventMsg2;

	PrimMng::g_pTkSubWindow = new CTkSubWindow();
	PrimMng::g_pIwOnePic = new CIwOnePic();
	TkManager::g_pStDancer = new StDancer();
	TkManager::g_CTkMenuMng.SetMenuPhase(1, 1);
}

void FFXI::CTk::TkManager::TkEnd()
{
	//sub //TODO
	if (PrimMng::g_pTkHelp) {
		delete PrimMng::g_pTkHelp;
		PrimMng::g_pTkHelp = nullptr;
	}

	if (PrimMng::g_pTkTitle) {
		delete PrimMng::g_pTkTitle;
		PrimMng::g_pTkTitle = nullptr;
	}

	if (TkManager::g_pCTkMouse) {
		delete TkManager::g_pCTkMouse;
		TkManager::g_pCTkMouse = nullptr;
	}
	//sub //TODO
	if (TkManager::g_pCTkInputCtrl) {
		delete TkManager::g_pCTkInputCtrl;
		TkManager::g_pCTkInputCtrl = nullptr;
	}
	//sub //TODO
	if (PrimMng::g_pTkDrawMessageWindow != nullptr) {
		delete PrimMng::g_pTkDrawMessageWindow;
		PrimMng::g_pTkDrawMessageWindow = nullptr;
	}

	if (PrimMng::g_pTkDrawMessageWindow2 != nullptr) {
		delete PrimMng::g_pTkDrawMessageWindow2;
		PrimMng::g_pTkDrawMessageWindow2 = nullptr;
	}

	if (PrimMng::g_pTkEventMsg) {
		delete PrimMng::g_pTkEventMsg;
		PrimMng::g_pTkEventMsg = nullptr;
	}

	if (PrimMng::g_pTkSpoolMsg != nullptr) {
		delete PrimMng::g_pTkSpoolMsg;
		PrimMng::g_pTkSpoolMsg = nullptr;
	}

	if (PrimMng::g_pTkSpoolMsg2 != nullptr) {
		delete PrimMng::g_pTkSpoolMsg2;
		PrimMng::g_pTkSpoolMsg2 = nullptr;
	}

	if (PrimMng::g_pTkEventMsg != nullptr) {
		delete PrimMng::g_pTkEventMsg;
		PrimMng::g_pTkEventMsg = nullptr;
	}

	if (PrimMng::g_pTkEventMsg2 != nullptr) {
		delete PrimMng::g_pTkEventMsg2;
		PrimMng::g_pTkEventMsg2 = nullptr;
	}

	if (PrimMng::g_pTkSubWindow != nullptr) {
		delete PrimMng::g_pTkSubWindow;
		PrimMng::g_pTkSubWindow = nullptr;
	}

	if (PrimMng::g_pIwOnePic) {
		delete PrimMng::g_pIwOnePic;
		PrimMng::g_pIwOnePic = nullptr;
	}

	if (TkManager::g_pStDancer) {
		delete TkManager::g_pStDancer;
		TkManager::g_pStDancer = nullptr;
	}

	if (PrimMng::g_pIwOkMenu) {
		delete PrimMng::g_pIwOkMenu;
		PrimMng::g_pIwOkMenu = nullptr;
	}

	TkManager::g_CTkMenuMng.DeleteMenuAll();
}

void FFXI::CTk::TkManager::TkZoneIn()
{
	TkManager::g_CTkMenuMng.ZoneInit();
	TkManager::g_CTkMenuMng.OnZoneChange(0);
}
