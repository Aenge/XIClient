#pragma once
#include "CTkSpoolMsgBase.h"

namespace FFXI {
	namespace CTk {
		class CTkSpoolMsgType2 : public CTkSpoolMsgBase
		{
		public:
			CTkSpoolMsgType2(CTkDrawMessageWindow*);
			virtual void CreateSpoolMsg(char*, TK_LOGDATAHEADER) override final;
			virtual void DestroyWind() override final;
		};
	}
}