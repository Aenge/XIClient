#pragma once
#include "FsWordCompletion.h"
namespace FFXI {
	namespace Input {
		class FsTextInputSetting {
		public:
			virtual ~FsTextInputSetting();
			FsTextInputSetting();
			char field_4;
			char field_5;
			char field_6;
			char field_7;
			int field_8;
			int field_C;
			int field_10[128];
		};
	}
}