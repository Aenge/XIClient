#pragma once
#include "MemoryManagedObject.h"
#include "ResourceMetadata.h"
#include "Enums.h"
namespace FFXI {
	class XiDateTime;
	namespace Math { class WMatrix; }
	namespace CYy {
		/**
		* @class CMoResource
		* @brief Base class for all game resources
		* 
		* It serves as a foundation for the resource management system that handles game assets
		* such as models, textures, animations, and other data files.
		*/
		class CMoResource : public MemoryManagedObject {
		public:
			const static BaseGameObject::ClassInfo CMoResourceClass;
			static unsigned int TraversalIdCounter;
			static CMoResource* OpenResource(char*, CMoResource*);
			static void UnlinkFromManager(CMoResource***);
			static Constants::Enums::ResourceType GetTypeFromRawData(char* data);
			static unsigned int GetSizeFromRawData(char* data);

			virtual const BaseGameObject::ClassInfo* GetRuntimeClass() override;
			virtual void VObj2(int*) override;
			virtual void VObj5(void*) override;
			virtual void ConstructFromData(char*);
			virtual void Open();
			virtual void Close();
			virtual FFXI::Math::WMatrix* VRes4();

			CMoResource();
			virtual ~CMoResource();

			CMoResource* GetParent();

			/**
			* Finds the root parent of this resource by traversing up the parent chain.
			* The root parent is either:
			* 1. A resource that is its own parent
			* 2. The highest ancestor in the resource hierarchy
			*
			* @return The root parent resource, or nullptr if no valid parent chain exists
			*/
			CMoResource* GetRoot();

			CMoResource*** FindResourceUnder(CMoResource***, int, int);
			CMoResource*** AnotherResourceSearcher(CMoResource***, Constants::Enums::ResourceType, int);
			bool FindResourceWithCallback(int, int, bool(__cdecl*)(CMoResource**, void*), void*);

			int GetDependencyCount();

			void RegisterWithManager();
			void InitializeByType(char);
			void CleanupResource();
			void IncrementReferenceCount();
			void DecrementReferenceCount();
			void UnlinkFromResourceLists();
			void FindResourceNextUnder(CMoResource***, int, int, int);
			void GetActivateTime(XiDateTime*);
			void CheckGenerators();

			ResourceMetadata Metadata;

			//A string of four characters that uniquely identifies the resource
			int ResourceID;

			//Type_Size is a bitfield with three sections
			//Bits 0-6 Identify the resource type
			//Bits 7-25 Identify the size of the resource in bytes
			//Bits 26-31 Are flags
			unsigned int TypeSizeFlags;

			//References to previously loaded resources
			CMoResource** ParentResourceReference;
			CMoResource** PreviousLoadedResourceReference;

			/**
			* @enum ResourceStateFlags
			* This enum contains bit flags that are used in ResourceMetadata.StateFlags
			* to track the state and properties of a resource throughout its lifecycle.
			*/
			enum ResourceStateFlags : unsigned short {
				RESOURCE_ACTIVE      = 0x0001,  // Resource is active in the system
				RESOURCE_CLOSING     = 0x0002,  // Resource is in the process of closing
				RESOURCE_CLONED      = 0x0004,  
				RESOURCE_OPENED      = 0x0008,  // Resource has been opened
				RESOURCE_SCHEDULED   = 0x0010,  // Only seen used with schedulers when they are running
				RESOURCE_LOADING     = 0x0020,  // Resource is currently loading
				RESOURCE_ERROR       = 0x0040,  // Resource has encountered an error
				RESOURCE_SPECIAL_USE = 0x0080,  // Special usage flag (context-dependent)
			};
		};
	}
}