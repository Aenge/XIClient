#pragma once
#include "MemoryBlockDescriptor.h"

namespace FFXI {
	namespace CYy {
		class CYyMenuMemory : public MemoryBlockDescriptor {
		public:
			const static BaseGameObject::ClassInfo CYyMenuMemoryClass;
			virtual const BaseGameObject::ClassInfo* GetRuntimeClass() final;
		};
	}
}