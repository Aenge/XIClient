#pragma once
#include "CMoElem.h"

namespace FFXI {
	namespace CYy {
		class CMoD3aElem : public CMoElem {
		public:
			static const BaseGameObject::ClassInfo CMoD3aElemClass;
			const BaseGameObject::ClassInfo* GetRuntimeClass() override final;
			CMoD3aElem() = default;
			virtual ~CMoD3aElem() = default;
			void PrepD3aTSS();
			virtual void OnDraw() override final;
			virtual void VirtElem9() override final;
		};
	}
}