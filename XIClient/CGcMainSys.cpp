#include "CGcMainSys.h"
#include "NT_SYS.h"
#include "CNtTcpDLL.h"
#include "StringTables.h"
#include "GlobalStruct.h"
#include "CApp.h"
#include "ZoneSys.h"
#include "packet_handlers.h"
#include "GP_SERV_COMMAND.h"
#include "GP_CLI_COMMAND.h"
#include "GP_CLI_PACKETS.h"
#include "GP_GAME_PACKET_HEAD.h"
#include "udpBuffer.h"
#include "huffman.h"
#include "enQue.h"
#include "md5.h"
#include <stdio.h>
#include <iostream>

using namespace FFXI::Network;

int FFXI::Network::CGcMainSys::dllsw{ 0 };
char FFXI::Network::CGcMainSys::InfoMationString[0x200] = { "" };
FFXI::Zone::ZoneSys FFXI::Network::CGcMainSys::g_ZoneSys{};
FFXI::Zone::ZoneSys* FFXI::Network::CGcMainSys::pZoneSys{ nullptr };
int FFXI::Network::CGcMainSys::netstate_1318{ 9700 };
int FFXI::Network::CGcMainSys::questate_1330{ 9700 };
FFXI::Network::udpBuffer FFXI::Network::CGcMainSys::sendBuffer{};
FFXI::Network::udpBuffer FFXI::Network::CGcMainSys::recvBuffer{};
short FFXI::Network::CGcMainSys::map_connection_state{ 0 };
int FFXI::Network::CGcMainSys::CharID_1666[2]{ 0 };

FFXI::Zone::GC_ZONE* FFXI::Network::CGcMainSys::pGlobalNowZone{ nullptr };
int* FFXI::Network::CGcMainSys::CliStatusCallPtr{ nullptr };

char recv_status_stack[4]{ 0 };
void createLoginTicket(char* output, char* a2, unsigned int a3, char* a4, unsigned int a5) 
{
    char hash[16];
    FFXI::Util::md5 ctx{};
    ctx.init();
    ctx.update(a2, a3);
    ctx.update(a4, a5);
    ctx.final(hash);

    memcpy(output, hash, sizeof(hash));
}
bool LoginType0(int a1, char* a2, FFXI::Network::enAcv* a3, char a4, const char* a5) 
{
    FFXI::Network::enQueBuff* buff = FFXI::Network::CGcMainSys::gcZoneSendQueSearch(FFXI::Constants::Enums::GP_CLI_COMMAND::LOGIN, true, 1);
    if (buff == nullptr) {
        return false;
    }

    createLoginTicket(buff->data + 60, a2, 15, a3->field_1048, a3->field_1098);
}

void LoginOK(FFXI::Zone::GC_ZONE* a1) {
    FFXI::Network::UDP::GP_CLI_PACKETS::GP_CLI_LOGINOK* buff = (FFXI::Network::UDP::GP_CLI_PACKETS::GP_CLI_LOGINOK*)FFXI::Network::CGcMainSys::gcZoneSendQueSearch(FFXI::Constants::Enums::GP_CLI_COMMAND::LOGINOK, false, 0);
    if (buff == nullptr) {
        return;
    }

    buff->field_4 = 2;
    buff->field_5 = 0;
    FFXI::Network::enQue::enQueSet((FFXI::Network::enQueBuff*)buff, sizeof(*buff), 0);
    a1->field_D = 1;
}

int FFXI::Network::CGcMainSys::gcLoginCliinitProcStart(int* a1)
{
    char value = FFXI::GlobalStruct::g_GlobalStruct.field_18 & 0xFF;
    int rc = FFXI::GlobalStruct::g_GlobalStruct.RegionCode;
    bool flag{ false };
    if (value < 0 && (rc == 2 || rc == 3)) {
        flag = true;
    }

    FFXI::Network::CGcMainSys* mainsys = FFXI::Network::NT_SYS_BASE::pGcMainSys;
    if (gcZoneInitStart(
        mainsys->WorldIP, mainsys->WorldPort,
        mainsys->field_12, mainsys->DebugName,
        (char*)&mainsys->field_64, mainsys->field_B4,
        flag, mainsys->pcnt, FFXI::Network::NT_SYS::pGame->NetTimeout1) == false) {
        return -1;
    }

    a1[0] += 1;
    return 0;
}

int FFXI::Network::CGcMainSys::gcLoginCliinitProcStart2(int* a1)
{
    FFXI::Network::CGcMainSys* mainsys = FFXI::Network::NT_SYS_BASE::pGcMainSys;
    switch (a1[0]) {
    case 0:
        mainsys->field_136AC = 0;
        if (FFXI::Network::CGcMainSys::dllsw == 0) {
            FFXI::Network::CNtTcpDLL::ntTcpDLLRequestCreateObject(&mainsys->ntTcpDLL, &mainsys->DLLInitStruct);
            FFXI::Network::CGcMainSys::dllsw = 1;
            a1[0] = 1;
        }
        else {
            a1[0] = 2;
        }
        return 0;
    case 1:
    case 3:
    case 5:
    case 7:
    case 9:
    case 11:
        FFXI::Network::CNtTcpDLL::ntTcpDLLStatus(mainsys->ntTcpDLL, &mainsys->field_13698);
        if (mainsys->field_13698 == 0) {
            a1[0] += 1;
        }
        else if (mainsys->field_13698 == -1) {
            return -1;
        }
        return 0;
    case 2:
        FFXI::Network::CNtTcpDLL::ntTcpDLLRequestSetWork(mainsys->ntTcpDLL, &mainsys->IwWorkPacket);
        a1[0] = 3;
        return 0;
    case 4:
    case 6:
        FFXI::Network::CNtTcpDLL::ntTcpDLLRequestKeyIncrement(mainsys->ntTcpDLL);
        a1[0] += 1;
        return 0;
    case 8:
        FFXI::Network::CNtTcpDLL::ntTcpDLLRequestGetWork(mainsys->ntTcpDLL, &mainsys->IwWorkPacket);
        a1[0] = 9;
        return 0;
    case 10:
    {
        FFXI::Network::ContentIDInfo* info = mainsys->IwWorkPacket.ContentIDList + mainsys->IwWorkPacket.field_140;
        memcpy(mainsys->Name, info->Name, sizeof(info->Name));
        mainsys->Name[sizeof(mainsys->Name) - 1] = 0;
        mainsys->field_B4 = info->field_4 | ((info->field_A << 8) & 0xFF0000);
        memset(mainsys->field_64, 0, sizeof(mainsys->field_64));
        FFXI::Network::CNtTcpDLL::ntTcpDLLRequestMakeMD5(mainsys->ntTcpDLL, mainsys->field_64);
        a1[0] = 11;
    }
    return 0;
    case 12:
        FFXI::Network::CNtTcpDLL::ntTcpDLLRequestDeleteObject(mainsys->ntTcpDLL);
        a1[0] = 13;
        return 0;
    case 13:
        FFXI::Network::CNtTcpDLL::ntTcpDLLStatus(mainsys->ntTcpDLL, &mainsys->field_13698);
        if (mainsys->field_13698 == 0) {
            mainsys->ntTcpDLL = nullptr;
            a1[0] = 14;
        }
        else if (mainsys->field_13698 == -1) {
            return -1;
        }
        return 0;
    case 14:
        FFXI::Network::CGcMainSys::dllsw = 0;
        a1[0] = 15;
        return 0;
    default:
        return 0;
    }
}

int FFXI::Network::CGcMainSys::gcLoginCliinitProcWaiting(int* a1)
{
    return gcZoneFlag() & 1;
}

void FFXI::Network::CGcMainSys::gcMainDivisionProcSet(char a1)
{
    FFXI::Network::CGcMainSys* mainsys = FFXI::Network::NT_SYS_BASE::pGcMainSys;
    if (mainsys != nullptr) {
        mainsys->field_15110[0] = a1;
    }
}

void FFXI::Network::CGcMainSys::gcMainDivisionNetStateSet(char* a1)
{
    if (FFXI::Network::NT_SYS_BASE::pGcMainSys == nullptr) {
        return;
    }

    FFXI::Network::NT_SYS_BASE::pGcMainSys->field_15110[6] = a1[0];
    FFXI::Network::NT_SYS_BASE::pGcMainSys->field_15110[7] = a1[1];
}

void FFXI::Network::CGcMainSys::gcMainDivisionSubStateSet(char* a1)
{
    if (FFXI::Network::NT_SYS_BASE::pGcMainSys == nullptr) {
        return;
    }

    FFXI::Network::NT_SYS_BASE::pGcMainSys->field_15110[5] = a1[0];
}

void FFXI::Network::CGcMainSys::gcMainDivisionStateSet(char* a1)
{
    if (FFXI::Network::NT_SYS_BASE::pGcMainSys == nullptr) {
        return;
    }

    FFXI::Network::NT_SYS_BASE::pGcMainSys->field_15110[4] = a1[0];
}

int FFXI::Network::CGcMainSys::gcZoneInit(int a1)
{
    set_map_connection_state(0);
    *(int*)recv_status_stack = 0;

    CGcMainSys::gcMainDivisionProcSet('z');
    
    CGcMainSys::pZoneSys = &CGcMainSys::g_ZoneSys;
    memset(pZoneSys, 0, sizeof(*pZoneSys));

    pZoneSys->pNowZone = &CGcMainSys::g_ZoneSys.nowZone;
    CGcMainSys::pGlobalNowZone = pZoneSys->pNowZone;
    
    const int array_size = sizeof(pZoneSys->field_3E07C) / sizeof(pZoneSys->field_3E07C[0]);
    for (int i = 0; i < array_size; ++i) {
        pZoneSys->field_3E07C[i] = 1000;
    }

    pZoneSys->field_3EDEC = a1;
    pZoneSys->field_3B088 = 1;
    
    FFXI::Network::Huffman::newHuffmanTable(&pZoneSys->huffmanTable, sizeof(pZoneSys->huffmanTable));
    FFXI::Network::Huffman::newHuffmanTree(&pZoneSys->huffmanTree);
    //(_DWORD*)&word_104CE650 = 0;
    //(_DWORD*)byte_104CE654 = 0;
    SetZoneCommandFuncs();
    //FDtGmNoticeInit();
    return 1;
}

bool FFXI::Network::CGcMainSys::gcZoneCreate(int a1, int a2, unsigned short a3, short a4, int a5, char* a6, char* a7, bool a8, int a9, int a10)
{
    if (a1 >= CGcMainSys::pZoneSys->field_3B088) {
        return false;
    }

    pGlobalNowZone = *(&pZoneSys->pNowZone + a1);
    memset(pGlobalNowZone, 0, sizeof(*pGlobalNowZone));

    strncpy_s(pGlobalNowZone->field_365E0, a6, sizeof(pGlobalNowZone->field_365E0));
    strncpy_s(pGlobalNowZone->field_36600, a7, sizeof(pGlobalNowZone->field_36600));

    pGlobalNowZone->field_462C = a1;
    pGlobalNowZone->field_417C = 5 * a9;
    pGlobalNowZone->field_365DC = a5;
    pGlobalNowZone->field_416C = a2;
    pGlobalNowZone->field_4170 = a3;
    pGlobalNowZone->field_4172 = a4;
    pGlobalNowZone->field_C = 1;
    pGlobalNowZone->field_D = 0;
    pGlobalNowZone->field_6 = 1;
    pGlobalNowZone->field_36650 = a8;

    pGlobalNowZone->field_41B0 = 3 * a10;

    const int array_size = sizeof(pGlobalNowZone->field_40) / sizeof(pGlobalNowZone->field_40[0]);
    if (FFXI::Network::enQue::enQueInit(&pGlobalNowZone->SendQueSys, pGlobalNowZone->field_40, array_size, 10) == false)
    {
        return false;
    }

    unsigned short v11 = ntohs(pGlobalNowZone->field_4172);
    pGlobalNowZone->field_4 = htons(v11);
    pGlobalNowZone->field_0 = FFXI::Network::NT_SYS_BASE::ntUdpCreate(&pGlobalNowZone->field_4);

    if (pGlobalNowZone->field_0 == nullptr) {
        return false;
    }

    FFXI::Network::enAcv::enAcvCreate(&pGlobalNowZone->field_2020, a7);
    FFXI::Network::enAcv::enAcvCreate(&pGlobalNowZone->field_30C0, a7);

    if (gcZoneSysInit(pGlobalNowZone) == false) {
        return false;
    }

    if (FFXI::Network::enQue::enQueSpecialSet(&pGlobalNowZone->SendQueSys, FFXI::Constants::Enums::GP_CLI_COMMAND::GAMEOK) == false) {
        return false;
    }

    if (FFXI::Network::enQue::enQueSpecialSet(&pGlobalNowZone->SendQueSys, FFXI::Constants::Enums::GP_CLI_COMMAND::SUBMAPCHANGE) == false) {
        return false;
    }

    pGlobalNowZone->field_4174 = FFXI::Network::NT_SYS_BASE::pTimeSys->field_4C;
    pGlobalNowZone->field_4178 = FFXI::Network::NT_SYS_BASE::pTimeSys->field_50;

    pGlobalNowZone->field_4180 = pGlobalNowZone->field_4174;

    unsigned long next = pGlobalNowZone->field_4178 + 1000u;
    pGlobalNowZone->field_4184 = next;

    //check for overflow
    if (next < pGlobalNowZone->field_4178) {
        pGlobalNowZone->field_4180 += 1;
    }

    pGlobalNowZone->field_4630[0] = 0;
    pGlobalNowZone->field_4630[1] = 0;
    pGlobalNowZone->field_4630[2] = 100;
    pGlobalNowZone->field_4630[3] = 0;
    pGlobalNowZone->field_4630[4] = 0;

    return true;
}

void FFXI::Network::CGcMainSys::gcZoneProc()
{
    FFXI::Zone::ZoneSys* zonesys = CGcMainSys::pZoneSys;
    if (zonesys == nullptr) {
        return;
    }

    gcMainDivisionProcSet('Z');
    unsigned int time = FFXI::Network::NT_SYS_BASE::pTimeSys->ntTimeGet();
    unsigned int divtime = time / 1000;
    if (FFXI::Network::NT_SYS::pDebug->field_18C != divtime)
    {
        FFXI::Network::NT_SYS::pDebug->field_190 = FFXI::Network::NT_SYS::pDebug->field_18C;
        FFXI::Network::NT_SYS::pDebug->field_18C = divtime;
        FFXI::Network::NT_SYS::pDebug->field_194 = 0;
        FFXI::Network::NT_SYS::pDebug->field_198 = 0;
    }

    gcZoneSysProc(zonesys->pNowZone);
}

bool FFXI::Network::CGcMainSys::gcZoneSysInit(FFXI::Zone::GC_ZONE*)
{
    if (gcEquipInit() == false) {
        return false;
    }

    return true;
}

bool FFXI::Network::CGcMainSys::gcZoneSysProc(FFXI::Zone::GC_ZONE* a1)
{
    unsigned int v1 = FFXI::Network::CNtTimeSys::ntTimeNowGet();
    if (a1 == nullptr) {
        return false;
    }

    pGlobalNowZone = a1;
    if (a1->field_C == 0) {
        return true;
    }
    else if (a1->field_C == 4) {
        return false;
    }
    else if (a1->field_C == 3) {
        int v6 = a1->field_41B0 + a1->field_4188;
        bool v7{};
        if (v6 <= v1) {
            v7 = v1 - v6 <= 0x7FFFFFFF;
        }
        else {
            v7 = v6 - v1 > 0x7FFFFFFF;
        }
        if (v7 == true) {
            a1->field_C = 4;
            exit(4001);
        }
    }

    gcProcCombine();
    StepCalc(a1);
    //various procs

    if (FFXI::Network::CNtTimeSys::ntTimeSync(0, a1->field_417C) == true) {
        SendProc(a1);
    }

    RecvProc(a1);
    if (a1->field_C == 3) {
        if (a1->field_418C == a1->field_4188) {
            if (FFXI::Network::CNtTimeSys::ntTimeSync(6u, 1000u) == true) {
                if (a1->field_418C <= FFXI::Network::CNtTimeSys::ntTimeGet()) {
                    a1->field_4190 = FFXI::Network::CNtTimeSys::ntTimeGet() - a1->field_418C;
                }
                else {
                    a1->field_4190 = a1->field_418C - FFXI::Network::CNtTimeSys::ntTimeGet();
                }
            }
        }
        else if (a1->field_418C <= FFXI::Network::CNtTimeSys::ntTimeGet()) {
            a1->field_4190 = FFXI::Network::CNtTimeSys::ntTimeGet() - a1->field_418C;
        }
        else {
            a1->field_4190 = a1->field_418C - FFXI::Network::CNtTimeSys::ntTimeGet();
        }
    }

    char v16[256];
    if (a1->field_4190 / 100u < 100) {
        sprintf_s(v16, "%c%d", netstate_1318 / 100, a1->field_4190 / 100u);
        if (a1->field_4190 < 500u) {
            netstate_1318 -= 1;
        }
        if (netstate_1318 < 9700) {
            netstate_1318 = 9700;
        }
    }
    else {
        sprintf_s(v16, "%cA", netstate_1318 / 100);
        netstate_1318 += 1;
        if (netstate_1318 >= 12200) {
            netstate_1318 = 12200;
        }
    }

    gcMainDivisionNetStateSet(v16);

    if (a1->SendQueSys.field_10 < 10) {
        questate_1330 += 1;
    }
    else if (a1->SendQueSys.field_10 > 15) {
        questate_1330 -= 1;
    }

    if (questate_1330 > 12200) {
        questate_1330 = 12200;
    }
    else if (questate_1330 < 9700) {
        questate_1330 = 9700;
    }

    char v14 = questate_1330 / 100;
    gcMainDivisionSubStateSet(&v14);
    pGlobalNowZone = a1;

    if (FFXI::Network::CNtTimeSys::ntTimeSync(1u, 3000u) == true) {
        pGlobalNowZone->field_4630[0] = 1000u * pGlobalNowZone->field_4630[3] / 3000u;
        pGlobalNowZone->field_4630[1] = 1000u * pGlobalNowZone->field_4630[4] / 3000u;
        pGlobalNowZone->field_4630[3] = 0;
        pGlobalNowZone->field_4630[4] = 0;
        if (pGlobalNowZone->field_4190 < 10000) {
            pGlobalNowZone->field_4630[2] += 12;
            if (pGlobalNowZone->field_4630[2] > 100) {
                pGlobalNowZone->field_4630[2] = 100;
            }
        }
        else {
            pGlobalNowZone->field_4630[2] -= 6;
            if (pGlobalNowZone->field_4630[2] < 0) {
                pGlobalNowZone->field_4630[2] = 0;
            }
        }
    }

    pZoneSys->field_3EDF0 += 1;
    return true;
}

void FFXI::Network::CGcMainSys::gcProcCombine()
{
    if (pGlobalNowZone->field_15818 == 4) {
        if ((FFXI::Network::CNtTimeSys::ntTimeGetSec() - pGlobalNowZone->field_15820) > 2) {
            pGlobalNowZone->field_1581C = 0;
            pGlobalNowZone->field_15818 = 0;
        }
    }
    else if (pGlobalNowZone->field_15818 == 2) {
        if ((FFXI::Network::CNtTimeSys::ntTimeGetSec() - pGlobalNowZone->field_15824) > 30) {
            pGlobalNowZone->field_1581C = 0;
            pGlobalNowZone->field_15818 = 0;
        }
    }
}

void FFXI::Network::CGcMainSys::StepCalc(FFXI::Zone::GC_ZONE* a1)
{
    pGlobalNowZone = a1;
    if (a1->field_C == 0) {
        return;
    }

    if (a1->field_C <= 2u) {
        bool v5{};
        if (a1->field_36685 == 0) {
            v5 = LoginType0(a1->field_365DC, a1->field_365E0, &a1->field_30C0, a1->field_36650, "WIN");
        }
        else {
            v5 = LoginType1(a1->field_365DC, a1->field_365E0, &a1->field_30C0, a1->field_36650, a1->field_365F0, "WIN", FFXI::Network::NT_SYS_BASE::pGame->field_438);
        }

        if (v5 == true && a1->field_C == 1) {
            a1->field_C = 2;
            a1->field_41A8 = FFXI::Network::NT_SYS_BASE::pTimeSys->field_4C;
            a1->field_41AC = FFXI::Network::NT_SYS_BASE::pTimeSys->field_50;
        }
    }
    else if (a1->field_C == 3) {
        if (a1->field_D == 0) {
            LoginOK(a1);
        }
    }
}

void FFXI::Network::CGcMainSys::SendProc(FFXI::Zone::GC_ZONE* a1)
{
    if (a1->field_C == 3) {
        int v1 = a1->field_6 - a1->field_A;
        if (v1 < -1 || v1 > 1) {
            a1->field_417C += 50;
            unsigned int v7 = pZoneSys->field_3EDEC + 3000;
            if (v7 < a1->field_417C) {
                a1->field_417C = v7;
            }
        }
        else if (a1->field_417C > pZoneSys->field_3EDEC) {
            a1->field_417C -= pZoneSys->field_3EDEC;
            if (a1->field_417C < pZoneSys->field_3EDEC) {
                a1->field_417C = pZoneSys->field_3EDEC;
            }
        }
    }

    if ((gcZoneFlag() & 0x0004) != 0) {
        gcZoneFlgOrSet(0x0008);
    }

    sendBuffer.remote.sa_family = AF_INET;
    memcpy(sendBuffer.remote.sa_data + 2, &pGlobalNowZone->field_416C, sizeof(pGlobalNowZone->field_416C));
    memcpy(sendBuffer.remote.sa_data, &pGlobalNowZone->field_4170, sizeof(pGlobalNowZone->field_4170));
    
    char v19[10000];
    int v11 = gpGamePacketSet(v19, &a1->SendQueSys, a1->field_6, a1->field_8, a1->field_A, &pZoneSys->huffmanTable);

    if (a1->field_C == 1 || a1->field_C == 2) {
        v19[12] = 0;
        sendBuffer.buffer_length = FFXI::Network::enAcv::enAcvSet(sendBuffer.buffer, sizeof(sendBuffer.buffer), nullptr, v19, v11, &pZoneSys->huffmanTable);
    }
    else {
        v19[12] = 1;
        memcpy(v19 + 16, &a1->field_4164, sizeof(a1->field_4164));
        memcpy(v19 + 20, &a1->field_4168, sizeof(a1->field_4168));
        sendBuffer.buffer_length = FFXI::Network::enAcv::enAcvSet(sendBuffer.buffer, sizeof(sendBuffer.buffer), &a1->field_30C0, v19, v11, &pZoneSys->huffmanTable);
    }

    if (sendBuffer.buffer_length <= 0) {
        return;
    }

    a1->field_4630[4] += sendBuffer.buffer_length;

    FFXI::Network::NT_SYS::pDebug->field_9C += 1;
    FFXI::Network::NT_SYS::pDebug->field_94 += 1;
    FFXI::Network::NT_SYS::pDebug->field_194 += sendBuffer.buffer_length;

    //client does not check the result of sendto ...
    sendto(a1->field_0->socket, sendBuffer.buffer, sendBuffer.buffer_length, NULL, &sendBuffer.remote, sizeof(sendBuffer.remote));

    a1->field_6 += 1;

    //check for overflow
    if (a1->field_6 == 0) {
        a1->field_6 = 1;
    }
    
    unsigned int timenow = FFXI::Network::CNtTimeSys::ntTimeNowGet();

    if (a1->field_4160 == a1->field_4161) {
        FFXI::Network::enAcv::enAcvChange(&a1->field_2020);
        a1->field_4160 += 1;
        a1->field_4196 = 0;
    }
    else {
        if (a1->field_4196 >= 5u) {
            if (timenow > a1->field_41A0) {
                a1->field_419C += timenow - a1->field_41AC;
                if (a1->field_4198 != 0) {
                    if (a1->field_419C > a1->field_4198) {
                        a1->field_4194 = 1;
                    }
                }
            }
        }

        a1->field_4196 += 1;
    }

    a1->field_41A0 = timenow;
}

void FFXI::Network::CGcMainSys::RecvProc(FFXI::Zone::GC_ZONE* a1)
{
    int remote_size = sizeof(recvBuffer.remote);
    recvBuffer.buffer_length = recvfrom(a1->field_0->socket, recvBuffer.buffer, sizeof(recvBuffer.buffer), NULL, &recvBuffer.remote, &remote_size);
    if (recvBuffer.buffer_length <= 0) {
        return;
    }

    set_map_connection_state(3);
    char buffer[10000];
    if (*(int*)(recvBuffer.remote.sa_data + 2) != pGlobalNowZone->field_416C) {
        PushRecvStatus(1);
    }
    else {
        std::cout << "RECV " << recvBuffer.buffer_length << " bytes\n";
        int v3 = FFXI::Network::enAcv::enAcvGet(buffer, sizeof(buffer), &a1->field_2020, &pZoneSys->huffmanTree, recvBuffer.buffer, recvBuffer.buffer_length);
        std::cout << "UNPACKED TO " << v3 << " BYTES\n";
        if (v3 < 0) {
            PushRecvStatus(3);
            TallyRecvErrorType(a1, 1);
            return;
        }
        else if (v3 == 0) {
            PushRecvStatus(2);
            TallyRecvErrorType(a1, 0);
            return;
        }
        else {
            a1->field_4630[3] += recvBuffer.buffer_length;
            FFXI::Network::NT_SYS::pDebug->field_98 += 1;
            FFXI::Network::NT_SYS::pDebug->field_198 += recvBuffer.buffer_length;
            
            //Simulate packet not arriving?
            if (FFXI::Network::NT_SYS::pDebug->Lost >= rand() % 64 + 1) {
                PushRecvStatus(5);
                return;
            }

            FFXI::Network::UDP::GP_GAME_PACKET_HEAD* header = (FFXI::Network::UDP::GP_GAME_PACKET_HEAD*)buffer;
            if (a1->field_C == 3 && (a1->field_8 - header->field_0) <= 0x7FFFu) {
                PushRecvStatus(6);
                TallyRecvErrorType(a1, 2);
                return;
            }

            int pos = sizeof(FFXI::Network::UDP::GP_GAME_PACKET_HEAD);
            while (pos < v3) {
                UDP::GP_SERV_PACKETS::GP_SERV_BASE* base = (UDP::GP_SERV_PACKETS::GP_SERV_BASE*)(buffer + pos);
                int value = base->field_0;
                if ((value & 0xFE00) == 0) {
                    PushRecvStatus(7);
                    TallyRecvErrorType(a1, 3);
                    break;
                }

                int value2 = base->field_2;
                if ((unsigned short)(a1->field_8 - value2) <= 0x7FFFu) {
                    PushRecvStatus(8);
                    TallyRecvErrorType(a1, 4);
                }
                else {
                    bool packet_handler_ran = false;
                    int opcode = value & 0x1FF;
                    if (pZoneSys->IncomingPacketHandlers[opcode] == nullptr) {
                        PushRecvStatus(10);
                    }
                    else {
                        pZoneSys->IncomingPacketHandlers[opcode](a1, header, base);
                        packet_handler_ran = true;
                    }

                    unsigned int flag = gcZoneFlag();
                    bool check1 = (flag & 0x200) == 0 || (flag & 0x400) != 0;
                    bool check2 = (flag & 0x2000) == 0 || (flag & 0x4000) != 0;
                    bool check3 = pZoneSys->field_3E4F4[opcode] != 0;
                    bool check4 = opcode < (int)FFXI::Constants::Enums::GP_SERV_COMMAND::MAX;
                    if (check1 && check2 && check3 && check4) {
                        pZoneSys->field_3E4F4[opcode](a1, header, base);
                    }
                    else if (packet_handler_ran == false) {
                        PushRecvStatus(11);
                        TallyRecvErrorType(a1, 5);
                    }
                }

                pos += 4 * (value >> 9);
            }

            a1->field_8 = header->field_0;
            a1->field_A = header->field_2;
            FFXI::Network::enAcv::enAcvChange(&a1->field_30C0);
            a1->field_4161 += 1;

            if ((a1->field_8 % 30) == 0) {
                gcZoneTimeSet(a1, header->field_4, header->field_8);
            }
        }
    }

    a1->field_4188 = FFXI::Network::CNtTimeSys::ntTimeNowGet();
    if (a1->field_4196 < 5u)
    {
        if (a1->field_4188 > a1->field_41A4)
        {
            unsigned int v16 = a1->field_4188 - a1->field_41A4;
            unsigned int v17 = a1->field_419C;
            if (v17 <= v16) {
                a1->field_419C = 0;
            }
            else {
                a1->field_419C = v17 - v16;
            }

            if (a1->field_419C <= a1->field_419A) {
                a1->field_4194 = 0;
            }
        }
    }

    a1->field_4196 = 0;
    a1->field_41A4 = a1->field_4188;
}

void FFXI::Network::CGcMainSys::PushRecvStatus(unsigned int a1)
{
    if (recv_status_stack[3])
    {
        recv_status_stack[0] = recv_status_stack[1];
        recv_status_stack[1] = recv_status_stack[2];
        recv_status_stack[2] = recv_status_stack[3];
        recv_status_stack[3] = 0;
    }

    if (recv_status_stack[0] == 0) {
        recv_status_stack[0] = a1;
    }
    else {
        int pos = 1;
        while (recv_status_stack[pos] != 0) {
            pos += 1;
            if (pos >= sizeof(recv_status_stack)) {
                return;
            }
        }

        if (recv_status_stack[pos - 1] != a1) {
            recv_status_stack[pos] = a1;
        }
    }
}

bool FFXI::Network::CGcMainSys::ReqCliStatus(int* a1, char a2)
{
    CliStatusCallPtr = a1;
    FFXI::Network::UDP::GP_CLI_PACKETS::GP_CLI_REQ_STATUS* buff = (FFXI::Network::UDP::GP_CLI_PACKETS::GP_CLI_REQ_STATUS*)gcZoneSendQueSearch(FFXI::Constants::Enums::GP_CLI_COMMAND::REQ_STATUS, false, 0);
    if (buff == nullptr) {
        return false;
    }

    gcZoneSendQueSet(buff, sizeof(*buff), 0);
    buff->field_4 = a2;
    return true;
}

bool FFXI::Network::CGcMainSys::SendLockCMD(FFXI::Network::UDP::GP_CLI_PACKETS::GP_CLI_LOCK_CMD::LockCMDType a1, int a2)
{
    FFXI::Network::UDP::GP_CLI_PACKETS::GP_CLI_LOCK_CMD* buff = (FFXI::Network::UDP::GP_CLI_PACKETS::GP_CLI_LOCK_CMD*)gcZoneSendQueSearch(FFXI::Constants::Enums::GP_CLI_COMMAND::LOCK_CMD, false, 0);
    if (buff == nullptr) {
        return false;
    }

    buff->field_4 = 0;
    buff->Type = a1;
    buff->field_C ^= (a2 ^ buff->field_C) & 1;
    gcZoneSendQueSet(buff, sizeof(*buff), 0);
    return true;
}

short FFXI::Network::CGcMainSys::gcZoneActIndex()
{
    if (pGlobalNowZone == nullptr) {
        return 0;
    }

    return pGlobalNowZone->field_3AFF4;
}

int* FFXI::Network::CGcMainSys::gcZoneCharID()
{
    if (pGlobalNowZone == nullptr) {
        return nullptr;
    }

    CharID_1666[0] = pGlobalNowZone->field_3AFF4;
    CharID_1666[1] = pGlobalNowZone->field_365DC;
    return CharID_1666;
}

bool FFXI::Network::CGcMainSys::gcLoginGetIsZoneChange()
{
    if (pGlobalNowZone == nullptr) {
        return false;
    }

    if ((gcZoneFlag() & 0x80u) != 0) {
        return false;
    }

    if ((gcZoneFlag() & 0x44) != 0x44) {
        return false;
    }

    gcZoneFlgOrSet(0x80);
    return true;
}

bool FFXI::Network::CGcMainSys::gcLoginGetIsLogout()
{
    if (pGlobalNowZone == nullptr) {
        return false;
    }

    if ((gcZoneFlag() & 0x80u) != 0) {
        return false;
    }

    if ((gcZoneFlag() & 0x44) != 0x04) {
        return false;
    }

    gcZoneFlgOrSet(0x80);
    return true;
}

void FFXI::Network::CGcMainSys::TallyRecvErrorType(FFXI::Zone::GC_ZONE* a1, unsigned int a2)
{
    const int array_size = sizeof(a1->recvProcErrors) / sizeof(a1->recvProcErrors[0]);
    if (a2 >= array_size) {
        a2 = array_size - 1;
    }

    a1->recvProcErrors[a2] += 1;
}

int FFXI::Network::CGcMainSys::gpGamePacketSet(char* a1, enQue* a2, unsigned short a3, unsigned short a4, unsigned short a5, Huffman::HuffmanTable* a6)
{
    FFXI::Network::UDP::GP_GAME_PACKET_HEAD* head = (FFXI::Network::UDP::GP_GAME_PACKET_HEAD*)a1;
    head->field_0 = a3;
    head->field_2 = a4;
    head->field_4 = FFXI::Network::CNtTimeSys::ntTimeNowGetMSec();
    head->field_8 = FFXI::Network::CNtTimeSys::ntTimeNowGetSec();

    int v8 = a2->field_C % a2->field_4;
    int v23 = sizeof(FFXI::Network::UDP::GP_GAME_PACKET_HEAD);
    int v24 = sizeof(FFXI::Network::UDP::GP_GAME_PACKET_HEAD);
    while (v8 != a2->field_8) {
        enQueBuff* buff = a2->field_0 + v8;
        if ((buff->field_0 & 0x1FFu) != 0) {
            if ((buff->field_0 & 0xFE00u) != 0) {
                int v11 = 4 * (buff->field_0 >> 9);
                if (v11 > 260) {
                    return 0;
                }
                if (buff->field_2 != 0 && (unsigned short)(a5 - buff->field_2) <= 0x7FFFu) {
                    buff->field_2 = 0;
                    buff->field_0 &= 0xFE00;
                    a2->field_10 += 1;
                    a2->field_C = (a2->field_C + 1) % a2->field_4;
                }
                else {
                    int v14 = huffman_packet_encoded_length((char*)&buff->field_0, v11, a6);
                    if (v14 + v23 + 4 >= (unsigned int)(FFXI::Network::NT_SYS_BASE::pGame->field_448 - 49)
                    || v11 + v24 >= 4000)
                    {
                        break;
                    }
                    else if (buff->field_2 == 0) {
                        v14 += 4;
                        buff->field_2 = a3;
                    }
                    memcpy(a1 + v24, buff, v11);
                    v23 += v14;
                    v24 += v11;
                }
            }
        }
        v8 = (v8 + 1) % a2->field_4;
    }

    int count = 0;
    int v21 = (a2->field_8 + 1) % a2->field_4;
    if (v21 != a2->field_C) {
        while (v21 != a2->field_C) {
            count += 1;
            v21 = (v21 + 1) % a2->field_4;
        }
        a2->field_10 = count;
    }

    return v24;
}

void FFXI::Network::CGcMainSys::gcZoneTimeSet(FFXI::Zone::GC_ZONE* a1, unsigned int a2, unsigned int a3)
{
    FFXI::Network::CNtTimeSys::ntTimeSetAbsorb(a3, a2 + (a1->field_4190 >> 1));
}

bool FFXI::Network::CGcMainSys::gcZoneRecvCallBack2(FFXI::Constants::Enums::GP_SERV_COMMAND a1, FFXI::Zone::ZoneSys::COMMAND_FUNC a2)
{
    if (pZoneSys == nullptr) {
        return false;
    }

    if ((int)a1 >= (int)FFXI::Constants::Enums::GP_SERV_COMMAND::MAX) {
        return false;
    }

    pZoneSys->IncomingPacketHandlers[(int)a1] = a2;
    return true;
}

void FFXI::Network::CGcMainSys::SetZoneCommandFuncs()
{
    gcZoneRecvCallBack2(FFXI::Constants::Enums::GP_SERV_COMMAND::LOGIN, (FFXI::Zone::ZoneSys::COMMAND_FUNC)FFXI::Zone::RecvCallbacks::RecvLogIn);
}

bool FFXI::Network::CGcMainSys::gcEquipInit()
{
    gcZoneRecvCallBack2(FFXI::Constants::Enums::GP_SERV_COMMAND::EQUIP_CLEAR, (FFXI::Zone::ZoneSys::COMMAND_FUNC)FFXI::Zone::RecvCallbacks::RecvEquipClear);
    
    memset(&pGlobalNowZone->gcEquip, 0, sizeof(pGlobalNowZone->gcEquip));
    
    for (int i = 0; i < Constants::Enums::EquipmentSlot::MAX; ++i) {
        pGlobalNowZone->gcEquip.slots[i].field_0 = i;
        pGlobalNowZone->gcEquip.slots[i].field_4 = 0;
        pGlobalNowZone->gcEquip.field_80[i].field_8 = 16;
        pGlobalNowZone->gcEquip.field_80[i].field_C = 0;
    }
    return true;
}

bool FFXI::Network::CGcMainSys::LoginType1(int a1, char* a2, FFXI::Network::enAcv* a3, unsigned char a4, char* a5, const char* a6, int a7)
{
    FFXI::Network::UDP::GP_CLI_PACKETS::GP_CLI_LOGIN* packet = (FFXI::Network::UDP::GP_CLI_PACKETS::GP_CLI_LOGIN*)FFXI::Network::CGcMainSys::gcZoneSendQueSearch(FFXI::Constants::Enums::GP_CLI_COMMAND::LOGIN, true, 1);
    if (packet == nullptr) {
        return false;
    }

    createLoginTicket(packet->login_ticket, a2, 15, a3->field_1048, a3->field_1098);
    strncpy_s(packet->field_31, 15, a2, 15);
    packet->field_C = a1;
    packet->field_58 = a4;
    strncpy_s(packet->field_22, 15, a5, 15);
    packet->field_22[14] = 0;
    strncpy_s(packet->platform_code, 4, a6, 4);
    packet->field_50 = a7;
    packet->client_connection_state = FFXI::Network::CGcMainSys::map_connection_state;
    packet->recv_status = *(int*)recv_status_stack;
    //some XiInfo calls
    
    packet->checksum = 0;
    char checksum = 0;
    for (int i = 8; i < 0x5C; ++i) {
        checksum += ((char*)packet)[i];
    }

    packet->checksum = checksum;
    FFXI::Network::enQue::enQueSet((FFXI::Network::enQueBuff*)packet, sizeof(*packet), 0);
    FFXI::Network::CGcMainSys::set_map_connection_state(1);
}

int FFXI::Network::CGcMainSys::gcLoginGetXiError()
{
	if (NT_SYS::pGcMainSys == nullptr)
		return 101;

	int err = CNtTcpDLL::ntTcpDLLGetError();
	if (err)
		return err;

	err = NT_SYS::pGcMainSys->field_136AC;
	if (err)
		return err;

	return 113;
}

const char* FFXI::Network::CGcMainSys::gcLoginGetInfoMationString()
{
    return InfoMationString;
}

void FFXI::Network::CGcMainSys::gcLoginSetInfoMationString(char* a1)
{
    strcpy_s(InfoMationString, a1);
}

int FFXI::Network::CGcMainSys::gcLoginProc(int* a1)
{
    FFXI::Network::CGcMainSys* mainsys = FFXI::Network::NT_SYS_BASE::pGcMainSys;
    if (mainsys == nullptr) {
        return -1;
    }

    if (*a1 == 0) {
        //some pol stuff here
        FFXI::Network::NT_SYS::ResetTimer();
    }

    int result{};
    char* string;
    switch (*a1) {
    case 0:
        string = FFXI::Text::XiStrGet(8, 0);
        FFXI::Network::CGcMainSys::gcLoginSetInfoMationString(string);
        //sub //todo field_136b0 is supposed to be set here to something from pol
        if (FFXI::Network::CGcMainSys::dllsw || mainsys->field_136B0 >= 0) {
            *a1 += 1;
        }
        else {
            mainsys->field_136AC = mainsys->field_136B0;
            return -1;
        }
        [[fallthrough]];
    case 1:
        if (FFXI::Network::CGcMainSys::dllsw) {
            *a1 += 2;
        }
        else {
            int v5 = 1; //this gets something from pol
            if (v5 == 0)
                return 0;
            if (v5 >> 31 < 0) {
                mainsys->field_136AC = v5;
                return -1;
            }

            mainsys->DLLInitStruct.DebugName = mainsys->DebugName;
            mainsys->DLLInitStruct.Password = mainsys->Password;
            in_addr v88{};
            v88.S_un.S_addr = mainsys->WorldIP;
            mainsys->DLLInitStruct.Ip = inet_ntoa(v88);
            mainsys->DLLInitStruct.field_C = htons(mainsys->field_10);
            memcpy(mainsys->DLLInitStruct.VersionString, FFXI::Network::NT_SYS::versionstring, sizeof(FFXI::Network::NT_SYS::versionstring));
            mainsys->DLLInitStruct.VersionString[15] = 0;
            CNtTcpDLL::ntTcpDLLRequestCreateObject(&mainsys->ntTcpDLL, &mainsys->DLLInitStruct);
            FFXI::Network::CGcMainSys::dllsw = 2;
            ++* a1;
        }
        result = 0;
        break;
    case 2:
    case 6:
    case 8:
    case 10:
    case 14:
    case 16:
    case 18:
    case 20:
        FFXI::Network::CNtTcpDLL::ntTcpDLLStatus(mainsys->ntTcpDLL, &mainsys->field_13698);
        if (mainsys->field_13698 == 0)
            ++* a1;
        if (mainsys->field_13698 == -1 || *a1 >= 14 && FFXI::Network::NT_SYS::UpdateTimer())
            return -1;
        return 0;
    case 3:
        string = FFXI::Text::XiStrGet(8, 1);
        FFXI::Network::CGcMainSys::gcLoginSetInfoMationString(string);
        mainsys->ntTcpDLL->ntTcpDLLRequestResetError();
        ++* a1;
        return 0;
    case 4:
        FFXI::Network::CNtTcpDLL::ntTcpDLLStatus(mainsys->ntTcpDLL, &mainsys->field_13698);
        ++* a1;
        return 0;
    case 5:
        if (mainsys->field_136AC == 0 && mainsys->ntTcpDLL) {
            if (mainsys->ntTcpDLL->ntTcpDLLCheckError() == 1) {
                *a1 = 21;
                return 0;
            }
        }
        mainsys->field_136AC = 0;
        mainsys->ntTcpDLL->ntTcpDLLRequestOption(1);
        ++* a1;
        return 0;
    case 7:
        string = FFXI::Text::XiStrGet(8, 2);
        FFXI::Network::CGcMainSys::gcLoginSetInfoMationString(string);
        mainsys->ntTcpDLL->ntTcpDLLRequestShutdown();
        ++* a1;
        return 0;
    case 9:
        string = FFXI::Text::XiStrGet(8, 3);
        FFXI::Network::CGcMainSys::gcLoginSetInfoMationString(string);
        mainsys->ntTcpDLL->ntTcpDLLRequestConnection();
        ++* a1;
        return 0;
    case 11:
        string = FFXI::Text::XiStrGet(8, 4);
        FFXI::Network::CGcMainSys::gcLoginSetInfoMationString(string);
        mainsys->field_136A8 = 0; //something from POL
        if (mainsys->field_136A8 < 0) {
            mainsys->field_136AC = mainsys->field_136A8;
            return -1;
        }
        ++* a1;
        return 0;
    case 12: {
        int v12 = 1; //something from pol
        if (v12 == 0)
            return 0;
        if (v12 >= 0) {
            ++* a1;
            return 0;
        }
        else {
            mainsys->field_136AC = v12;
            return -1;
        }
    }
    case 13:
        string = FFXI::Text::XiStrGet(8, 5);
        FFXI::Network::CGcMainSys::gcLoginSetInfoMationString(string);
        //pol message stuff
        {
            //Some of this is altered in order to work with a modified LSB server
            char v27[0x40];
            int v14 = 0; //get authcode
            if (v14 >= 0) {
                mainsys->ntTcpDLL->ntTcpDLLSetAthCode(v27);
                //get random value from pol 
                //char v26[16];
                //pGcMainSys->ntTcpDLL->ntTcpDLLSetPasswd(v26);
                int v25[] = { 1, 0 };
                mainsys->ntTcpDLL->ntTcpDLLSetClientCode(v25);
                FFXI::CYy::CApp::g_pNT_SYS->ClientExpansions = 1 | FFXI::GlobalStruct::g_GlobalStruct.ClientExpansions;
                mainsys->ntTcpDLL->ntTcpDLLSetExCodeClient(FFXI::CYy::CApp::g_pNT_SYS->ClientExpansions);
                mainsys->ntTcpDLL->SubStruct->field_1A2C = 3;
                mainsys->ntTcpDLL->SubStruct->field_1A30 = FFXI::GlobalStruct::g_GlobalStruct.field_20;
                mainsys->ntTcpDLL->SubStruct->field_1A28 += 2;
                mainsys->ntTcpDLL->ntTcpDLLRequestLobbyLogin();
                ++* a1;
                return 0;
            }
            else {
                mainsys->field_136AC = v14;
                return -1;
            }
        }
        return 0;
    case 15:
        string = FFXI::Text::XiStrGet(8, 6);
        FFXI::Network::CGcMainSys::gcLoginSetInfoMationString(string);
        mainsys->ntTcpDLL->ntTcpDLLGetExCodeServer(&FFXI::CYy::CApp::g_pNT_SYS->ServerExpansions);
        FFXI::GlobalStruct::g_GlobalStruct.ServerExpansions = 1 | FFXI::CYy::CApp::g_pNT_SYS->ServerExpansions;
        mainsys->ntTcpDLL->ntTcpDLLRequestGetChr();
        ++* a1;
        return 0;
    case 17:
        string = FFXI::Text::XiStrGet(8, 7);
        FFXI::Network::CGcMainSys::gcLoginSetInfoMationString(string);
        mainsys->ntTcpDLL->ntTcpDLLRequestQueryWorldList();
        ++* a1;
        return 0;
    case 19:
        FFXI::Network::CNtTcpDLL::ntTcpDLLRequestGetWork(mainsys->ntTcpDLL, &mainsys->IwWorkPacket);
        ++* a1;
        return 0;
    case 21:
        return 1;
    default:
        return 0;
    }

    return 0;
}

void FFXI::Network::CGcMainSys::gcLoginProcBreak()
{
    FFXI::Network::CGcMainSys* mainsys = FFXI::Network::NT_SYS_BASE::pGcMainSys;
    if (CGcMainSys::dllsw == 1) {
        FFXI::Network::CNtTcpDLL::ntTcpDLLStatus(mainsys->ntTcpDLL, &mainsys->field_13698);
        while (mainsys->ntTcpDLL->field_13698 != 0) {
            if (mainsys->ntTcpDLL->field_13698 == -1) {
                break;
            }
            FFXI::Network::CNtTcpDLL::ntTcpDLLStatus(mainsys->ntTcpDLL, &mainsys->field_13698);
        }

        mainsys->ntTcpDLL = nullptr;
        CGcMainSys::dllsw = 0;
    }
    else if (CGcMainSys::dllsw == 2) {
        FFXI::Network::CNtTcpDLL::ntTcpDLLRequestDeleteObject(mainsys->ntTcpDLL);
        CGcMainSys::dllsw = 1;
    }
}

int FFXI::Network::CGcMainSys::gcLoginIsConnect()
{
    if (CGcMainSys::dllsw != 2) {
        return -1;
    }

    FFXI::Network::CGcMainSys* mainsys = FFXI::Network::NT_SYS_BASE::pGcMainSys;
    if (mainsys->field_136AC != 0) {
        return -1;
    }

    if (mainsys->ntTcpDLL == nullptr) {
        return -1;
    }

    if (mainsys->ntTcpDLL->ntTcpDLLCheckError() != 1) {
        return -1;
    }

    return 0;
}

bool FFXI::Network::CGcMainSys::gcChrBindProc()
{
    FFXI::Network::CGcMainSys* mainsys = FFXI::Network::NT_SYS_BASE::pGcMainSys;
    if (mainsys == nullptr) {
        return false;
    }

    mainsys->field_136A4 = 0;
    return true;
}

int FFXI::Network::CGcMainSys::gcLoginChrSelectProc(int* a1, int a2)
{
    FFXI::Network::CGcMainSys* mainsys = FFXI::Network::NT_SYS_BASE::pGcMainSys;
    if (mainsys == nullptr) {
        return -1;
    }

    if (a1[0] == 0) {
        FFXI::Network::NT_SYS::ResetTimer();
    }
    else if (a1[0] <= 9 && FFXI::Network::NT_SYS::UpdateTimer() == true) {
        return -1;
    }

    char* string{};

    switch (a1[0]) {
    case 0:
        string = FFXI::Text::XiStrGet(8, 0xC4);
        FFXI::Network::CGcMainSys::gcLoginSetInfoMationString(string);
        mainsys->field_136AC = 0;
        mainsys->ntTcpDLL->ntTcpDLLRequestSelectChr(a2);
        a1[0] += 1;
        return 0;
    case 1:
    case 3:
    case 5:
        FFXI::Network::CNtTcpDLL::ntTcpDLLStatus(mainsys->ntTcpDLL, &mainsys->field_13698);
        if (mainsys->field_13698 == 0) {
            a1[0] += 1;
        }
        else if (mainsys->field_13698 == -1) {
            return -1;
        }
        return 0;
    case 2:
        string = FFXI::Text::XiStrGet(8, 0xC5);
        FFXI::Network::CGcMainSys::gcLoginSetInfoMationString(string);
        if (mainsys->ntTcpDLL->ntTcpDLLCheckError() == 0) {
            a1[0] += 2;
        }
        return 0;
    case 4:
        FFXI::Network::CNtTcpDLL::ntTcpDLLRequestGetWork(mainsys->ntTcpDLL, &mainsys->IwWorkPacket);
        a1[0] += 1;
        return 0;
    case 6:
        mainsys->WorldIP = mainsys->IwWorkPacket.field_124;
        mainsys->WorldPort = htons(mainsys->IwWorkPacket.field_128);
        if (mainsys->field_1369C != 0) {
            //FFXI::Util::md5 ctx{};
            exit(0x100DEFCA);
        }
        FFXI::Network::CNtTcpDLL::ntTcpDLLRequestDeleteObject(mainsys->ntTcpDLL);
        a1[0] += 1;
        FFXI::Network::CGcMainSys::dllsw = 1;
        return 0;
    case 7:
        FFXI::Network::CNtTcpDLL::ntTcpDLLStatus(mainsys->ntTcpDLL, &mainsys->field_13698);
        if (mainsys->field_13698 == 0) {
            mainsys->ntTcpDLL = nullptr;
            a1[0] += 1;
        }
        else if (mainsys->field_13698 == -1) {
            return -1;
        }
        return 0;
    case 8:
        FFXI::Network::CGcMainSys::dllsw = 0;
        a1[0] += 2;
        return 0;
    case 9:
        a1[0] += 1;
        return 0;
    case 10:
        string = FFXI::Text::XiStrGet(8, 0xC);
        FFXI::Network::CGcMainSys::gcLoginSetInfoMationString(string);
        a1[0] += 1;
        return 0;
    case 11:
        string = FFXI::Text::XiStrGet(8, 0xD);
        FFXI::Network::CGcMainSys::gcLoginSetInfoMationString(string);
        a1[0] += 1;
        return 0;
    case 12:
        string = FFXI::Text::XiStrGet(8, 0xE);
        FFXI::Network::CGcMainSys::gcLoginSetInfoMationString(string);
        a1[0] += 1;
        return 0;
    case 13:
        string = FFXI::Text::XiStrGet(8, 0xF);
        FFXI::Network::CGcMainSys::gcLoginSetInfoMationString(string);
        a1[0] += 1;
        return 0;
    case 14:
        [[fallthrough]];
    case 20:
        string = FFXI::Text::XiStrGet(8, 0x10);
        FFXI::Network::CGcMainSys::gcLoginSetInfoMationString(string);
        a1[0] += 1;

        return 0;
    case 15:
        [[fallthrough]];
    case 21:
        string = FFXI::Text::XiStrGet(8, 0x11);
        FFXI::Network::CGcMainSys::gcLoginSetInfoMationString(string);
        a1[0] += 1;
        return 0;
    case 16:
        a1[0] += 1;
        return 0;
    case 17:
        a1[0] += 1;
        return 0;
    case 18:
        a1[0] += 1;
        return 0;
    case 19:
        a1[0] += 1;
        return 0;
    case 22:
        return 1;
    default:
        return 0;
    }
}

int FFXI::Network::CGcMainSys::gcLoginCliinitProc(int* a1)
{
    FFXI::Network::CGcMainSys* mainsys = FFXI::Network::NT_SYS_BASE::pGcMainSys;
    if (mainsys == nullptr) {
        exit(3001);
    }

    if (a1[0] == 0) {
        FFXI::Network::NT_SYS::ResetTimer();
    }
    else {
        if (FFXI::Network::NT_SYS::UpdateTimer() == true) {
            exit(3001);
        }
    }

    int result = -1;
    if (a1[0] >= 0 && a1[0] <= 14) {
        result = gcLoginCliinitProcStart2(a1);
    }
    else if (a1[0] == 15) {
        result = gcLoginCliinitProcStart(a1);
    }
    else if (a1[0] == 16) {
        result = gcLoginCliinitProcWaiting(a1);
    }

    if (result == -1) {
        exit(3001);
    }

    CGcMainSys::gcZoneProc();

    return result;
}

int FFXI::Network::CGcMainSys::gcDataLoadProc(int* a1)
{
    FFXI::Network::CGcMainSys* mainsys = FFXI::Network::NT_SYS_BASE::pGcMainSys;

    int result = 0;
    switch (a1[0]) {
    case 0:
    {
        mainsys->field_136AC = 0;
        int world = FFXI::CYy::CApp::g_pNT_SYS->getWorld(FFXI::CYy::CApp::g_pNT_SYS->field_80);
        //friend stuff
        //sub TODO
        a1[0] = 1;
    }
        break;
    case 1:
        //friend stuff
        //sub TODO
        a1[0] = 2;
        break;
    case 2:
        //friend stuff
        //sub TODO
        result = 1;
        break;
    default:
        break;
    }

    //now it checks for mainsys?
    if (mainsys == nullptr) {
        return -1;
    }

    CGcMainSys::gcZoneProc();
    return result;
}

int FFXI::Network::CGcMainSys::gcLoginZoneProc()
{
    FFXI::Network::CGcMainSys* mainsys = FFXI::Network::NT_SYS_BASE::pGcMainSys;
    if (mainsys == nullptr) {
        return -1;
    }

    CGcMainSys::gcZoneProc();
    return 1;
}

bool FFXI::Network::CGcMainSys::gcZoneInitStart(int a1, unsigned short a2, short a3, char* a4, char* a5, int a6, bool a7, int a8, int a9)
{
    FFXI::Network::CGcMainSys* mainsys = FFXI::Network::NT_SYS_BASE::pGcMainSys;
    unsigned short v10 = ntohs(a2);

    char v13[256];
    sprintf_s(v13, "%d", v10);

    if (mainsys != nullptr) {
        mainsys->field_15110[1] = v13[2];
        mainsys->field_15110[2] = v13[3];
        mainsys->field_15110[3] = v13[4];
    }

    if (gcZoneInit(a9) == false) {
        return false;
    }

    int UniqueNo = FFXI::Network::NT_SYS::pDebug->UniqueNo;
    if (UniqueNo == 0) {
        UniqueNo = a6;
    }

    if (gcZoneCreate(0, a1, a2, a3, UniqueNo, a4, a5, a7, a8, a9) == false) {
        return false;
    }

    if (FFXI::Network::NT_SYS::pDebug->Name[0] != 0) {
        strncpy_s(pZoneSys->pNowZone->field_365F0, NT_SYS::pDebug->Name, sizeof(NT_SYS::pDebug->Name));
        pZoneSys->pNowZone->field_36685 = 1;
    }

    pGlobalNowZone = pZoneSys->pNowZone;
    return true;
}

unsigned int CGcMainSys::gcZoneFlag()
{
    if (CGcMainSys::pGlobalNowZone == nullptr) {
        return 0;
    }

    return CGcMainSys::pGlobalNowZone->field_365A8;
}

bool FFXI::Network::CGcMainSys::gcZoneFlgOrSet(int a1)
{
    if (pGlobalNowZone == nullptr) {
        return false;
    }

    pGlobalNowZone->field_365A8 |= a1;

    char v4[256];
    unsigned int flag = pGlobalNowZone->field_365A8;
    if ((flag & 0x0001) != 0) {
        sprintf_s(v4, "G");
    }
    else if ((flag & 0x0002) != 0) {
        sprintf_s(v4, "L");
    }
    else if ((flag & 0x0004) != 0) {
        sprintf_s(v4, "O");
    }
    else if ((flag & 0x0008) != 0) {
        sprintf_s(v4, "E");
    }
    else if ((flag & 0x0010) != 0) {
        sprintf_s(v4, "T");
    }
    else if ((flag & 0x0020) != 0) {
        sprintf_s(v4, "S");
    }
    else if ((flag & 0x0040) != 0) {
        sprintf_s(v4, "C");
    }
    else if ((flag & 0x0080) != 0) {
        sprintf_s(v4, "Y");
    }
    else if ((flag & 0x0100) != 0) {
        sprintf_s(v4, "R");
    }
    else if ((flag & 0x0200) != 0) {
        sprintf_s(v4, "1");
    }
    else if ((flag & 0x0400) != 0) {
        sprintf_s(v4, "2");
    }
    else if ((flag & 0x2000) != 0) {
        sprintf_s(v4, "3");
    }
    else if ((flag & 0x4000) != 0) {
        sprintf_s(v4, "4");
    }

    gcMainDivisionStateSet(v4);
    return true;
}

void FFXI::Network::CGcMainSys::gcMainDivisionInit()
{
    FFXI::Network::CGcMainSys* mainsys = FFXI::Network::NT_SYS_BASE::pGcMainSys;
    if (mainsys != nullptr) {
        const int array_size = sizeof(mainsys->field_15110) / sizeof(mainsys->field_15110[0]);
        for (int i = 0; i < array_size; ++i) {
            mainsys->field_15110[i] = 32;
        }
    }
}

enQueBuff* FFXI::Network::CGcMainSys::gcZoneSendQueSearch(FFXI::Constants::Enums::GP_CLI_COMMAND a1, bool a2, int a3)
{
    if (pGlobalNowZone == nullptr) {
        return nullptr;
    }

    if (a1 >= FFXI::Constants::Enums::GP_CLI_COMMAND::MAX) {
        return nullptr;
    }

    if (a2 == false) {
        unsigned int timeget = FFXI::Network::CNtTimeSys::ntTimeGet();
        if (pGlobalNowZone->field_41B4[(int)a1] != 0) {
            unsigned int sum = pGlobalNowZone->field_41B4[(int)a1] + pZoneSys->field_3E07C[(int)a1];
            if (timeget <= sum) {
                if (sum - timeget <= 0x7FFFFFFF) {
                    return nullptr;
                }
            }
            else {
                if (timeget - sum > 0x7FFFFFFF) {
                    return nullptr;
                }
            }
        }

        pGlobalNowZone->field_41B4[(int)a1] = timeget;
    }

    FFXI::Network::enQueBuff* buff = FFXI::Network::enQue::enQueSearch2(&pGlobalNowZone->SendQueSys, a1, a2, a3);
    if (buff == nullptr) {
        return nullptr;
    }
    
    buff->field_0 = (int)a1 & 0x1FF;
    buff->field_2 = 0;
    return buff;
}

void FFXI::Network::CGcMainSys::gcZoneSendQueSet(FFXI::Network::UDP::GP_CLI_PACKETS::GP_CLI_BASE* a1, int a2, int a3)
{
    FFXI::Network::enQue::enQueSet((FFXI::Network::enQueBuff*)a1, a2, a3);
}

void CGcMainSys::gcFriendInit(int a1)
{
	this->FriendUpdateCycle = a1;
	this->field_130 = -1;
	this->field_128 = 0;
	this->field_808 = 0;
	this->field_5F0 = 0;
	this->field_5EC = 0;
	this->field_3D4 = 0;
	this->field_5E8 = 0;
	this->field_5DC = 0;
	this->field_804 = 0;
	this->field_7F8 = 0;
	this->field_80C = 0;
	this->gcFriendSearchClear();
}

void FFXI::Network::CGcMainSys::set_map_connection_state(short a1)
{
    FFXI::Network::CGcMainSys::map_connection_state = a1;
}

void CGcMainSys::gcFriendSearchClear()
{
	this->field_1C4 = 0;
	this->field_3C8 = 0;
}
