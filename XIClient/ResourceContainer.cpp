#include "ResourceContainer.h"
#include "ResourceList.h"
#include "CYyGenerator.h"
#include "CYyScheduler.h"
#include "Globals.h"
#include "CYyDb.h"
#include "CMoResourceMng.h"
#include "CMoTaskMng.h"
#include "CMoSchedulerTask.h"
#include "CYyReferenceReadWithAttachmentTask.h"
#include "MemoryPoolManager.h"

using namespace FFXI::CYy;

void FFXI::CYy::ResourceContainer::ReferenceRead(ResourceContainer*** outResourceContainer, void(__cdecl* callbackFunction)(ResourceContainer**, File::ReferenceReadBase*), File::ReferenceReadBase* referenceData, FFXI::CYy::CXiActor* sourceActor, FFXI::CYy::CXiActor* targetActor)
{
	// Allocate memory for the reference read task
	char* memory = MemoryPoolManager::globalInstance->Get(sizeof(CYyReferenceReadWithAttachmentTask), MemoryPoolManager::MemoryPoolType::Ex);
	
	// Create the task to handle the asynchronous resource loading
	CYyReferenceReadWithAttachmentTask* referenceReadTask{ nullptr };

	// Indicate that this task should be deleted when complete
	CMoTaskMng::DeleteThisTask = true;

	if (memory != nullptr)
		referenceReadTask = new (memory) CYyReferenceReadWithAttachmentTask();

	//refreadtask could be null as per client
	referenceReadTask->Start(*outResourceContainer,	callbackFunction, referenceData, 1);
	referenceReadTask->SetCaster(sourceActor);
	referenceReadTask->SetTarget(targetActor);
}

FFXI::CYy::ResourceContainer::ResourceContainer()
{
	//nullsub in client
	this->field_30 = 0;
	this->FileIndex = 0;
	this->TraversalMarker = 0;
	this->TerminateNode = nullptr;
}

FFXI::CYy::ResourceContainer::~ResourceContainer()
{
	//nullsub
}

void FFXI::CYy::ResourceContainer::StopMovers()
{
	if (this == nullptr)
		return;

	ResourceList v8{}, v9{};
	v8.Clear();
	v8.PrepareFromResource(this, FFXI::Constants::Enums::ResourceType::Generater, 0, -1);

	CYyGenerator* gen = (CYyGenerator*)v8.GetNext(false);
	while (gen) {
		v9.PrepareFromLastCreated(CYyDb::g_pCYyDb->pCMoResourceMng, FFXI::Constants::Enums::ResourceType::Generater, 0, -1);
		CYyGenerator* gen2 = (CYyGenerator*)v9.GetNext(false);
		while (gen2) {
			if ((gen2->Metadata.StateFlags & CMoResource::ResourceStateFlags::RESOURCE_CLONED) != 0 &&
				gen2->ParentResourceReference == gen->Metadata.SelfReference) {
				gen2->IncrementReferenceCount();
				gen2->KillAll(nullptr);
				gen2->DecrementReferenceCount();
				if ((gen2->Metadata.MemoryReferenceCount & 0x7FFF) == 0) {
					CYyDb::g_pCYyDb->pCMoResourceMng->Unlink(gen2->Metadata.SelfReference);
					v9.PrepareFromLastCreated(CYyDb::g_pCYyDb->pCMoResourceMng, FFXI::Constants::Enums::ResourceType::Generater, 0, -1);
				}
			}
			gen2 = (CYyGenerator*)v9.GetNext(false);
		}
		gen->KillAll(nullptr);
		gen->Unlink(true);
		gen = (CYyGenerator*)v8.GetNext(false);
	}

	v8.PrepareFromResource(this, FFXI::Constants::Enums::ResourceType::Scheduler, 0, -1);
	CYyScheduler* sched = (CYyScheduler*)v8.field_14;
	while (sched != nullptr) {
		CMoTask* task = CYyDb::g_pCYyDb->pCMoTaskMng->field_D50;
		while (task != nullptr) {
			if (task->Param2 == 'Ms\0\0') {
				CMoSchedulerTask* stask = (CMoSchedulerTask*)task;
				CYyScheduler* runner = stask->field_80 ? *stask->field_80 : nullptr;
				if (runner == sched) {
					stask->DestroyTask();
					sched = (CYyScheduler*)v8.field_14;
					task = CYyDb::g_pCYyDb->pCMoTaskMng->field_D50;
					continue;
				}
			}
			task = task->field_C;
		}
		v8.GetNext(false);
		sched = (CYyScheduler*)v8.field_14;
	}
}
