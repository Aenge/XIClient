#include "ResourceMetadata.h"
#include "CMoResource.h"

using namespace FFXI::CYy;

void ResourceMetadata::Initialize() {
	this->MemoryReferenceCount = 0x8000;
	this->PreviousResource = nullptr;
	this->NextResource = nullptr;
	this->NextResourceOfSameType = nullptr;
	this->PreviousResourceOfSameType = nullptr;
	this->StateFlags = 0;
	this->ActiveDependencyCount = 0;
}

void ResourceMetadata::LinkToNext(CMoResource* nextResource) {
	if (nextResource == nullptr)
		return;

	this->NextResource = nextResource->Metadata.SelfReference;
}

void FFXI::CYy::ResourceMetadata::LinkToPrevious(CMoResource* previouseResource)
{
	if (previouseResource == nullptr)
		return;

	this->PreviousResource = previouseResource->Metadata.SelfReference;
}
