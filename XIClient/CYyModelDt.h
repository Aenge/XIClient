#pragma once
#include "MemoryManagedObject.h"
#include "VbList.h"

namespace FFXI {
	namespace CYy {
		class CMoOs2;
		class CYyOs2VtxBuffer;
		class CXiActor;
		class CYyModel;
		class CYyModelBase;
		class CYyModelDt : public MemoryManagedObject {
		public:
			static const BaseGameObject::ClassInfo CYyModelDtClass;
			virtual const BaseGameObject::ClassInfo* GetRuntimeClass() override final;
			CYyModelDt();
			virtual ~CYyModelDt();
			virtual void VirtModelDt1(CXiActor*, CYyModel*, CYyModelBase*, float, int*, int*) = 0;
			virtual void VirtModelDt2() = 0;
			virtual unsigned short VirtModelDt3() = 0;
			virtual void VirtModelDt4(unsigned char*) = 0;
			virtual CYyOs2VtxBuffer* VirtModelDt5() = 0;
			int GetResId();
			CYyModelDt* field_4;
			CMoOs2** field_8;
			int field_C;
			VbList field_10;
		};
	}
 }