#pragma once
#include "MemoryManagedObject.h"
#include "WMatrix.h"
#include "CMoSk2.h"

namespace FFXI {
	namespace CYy {
		class CYySkl : public MemoryManagedObject {
		public:
			static const BaseGameObject::ClassInfo CYySklClass;
			static CMoSk2* gSkeletonResource;
			static FFXI::Math::WMatrix* gBoneMatrices;
			static unsigned char* gBoneFlags;
			static int gBoneCount;
			static int g_arr[256];
			virtual const BaseGameObject::ClassInfo* GetRuntimeClass() override final;
			CYySkl();
			~CYySkl();
			void Init();

			/**
			* Sets the flags for every bone in the skeleton to zero
			*/
			bool ResetBoneFlags();

			CMoSk2::BoneTransform* GetStaticBoneTransform(unsigned short transformIndex);
			Math::BoundingBox3D* GetSkeletonBoundingBox(unsigned char boxIndex);

			/**
			* Recursively marks a bone and all its ancestors as active
			*
			* This function traverses the bone hierarchy upward from the specified bone,
			* marking each bone in the chain with the BONE_ACTIVE flag (0x80). This ensures
			* that when a bone is animated, all of its parent bones are also processed,
			* maintaining the proper skeletal hierarchy during animation.
			*
			* The function continues recursively until it reaches the root bone
			* (identified when a bone's parent index equals its own index).
			*
			* @param boneIndex Current bone to mark and recurse from
			*/
			void MarkBoneChainActive(int boneIndex);

			void ComputeBoneMatrices(FFXI::Math::WMatrix*, int, int, char*);

			//Called when the skeleton begins processing for animation
			bool BeginAnimating();

			void SetStatics2();

			/**
			 * Applies a global translation to all bone matrices in the skeleton
			 *
			 * This function shifts the position of the entire skeleton by adding the provided
			 * translation vector to the position component (_41, _42, _43) of each bone's
			 * transformation matrix. This effectively moves the entire skeleton in world space
			 * without changing the relative positions of bones to each other.
			 *
			 * The function uses the number of bones specified in the skeleton resource if
			 * zero is provided via the boneCount parameter.
			 *
			 * @param translationVector Pointer to a D3DXVECTOR4 containing the x, y, z translation values
			 * @param boneCount Optional count of bones to update (uses all bones in skeleton if set to 0)
			 */
			void ApplyGlobalTranslation(D3DXVECTOR4* translationVector, unsigned short boneCount);

			CMoSk2** Resource;

			// Raw allocated memory pointer for bone matrices (unaligned)
			void* pMatrixMemoryBlock;

			// 16-byte aligned pointer to the actual bone matrices
			FFXI::Math::WMatrix* pBoneMatrices;

			char field_10;
			char field_11;
			__int16 field_12;

			// Each bone in the skeleton has its own flags
			unsigned char* pBoneFlags;

			// Flags that control bone animation processing and blending
			enum BoneProcessingFlags : unsigned char {
				// Processing states (lower 6 bits)
				BONE_STATE_INITIAL = 0x00,
				BONE_STATE_PROCESSED = 0x01,
				BONE_STATE_MASK = 0x3F,

				// Control flags (upper 2 bits)
				BONE_SKIP_PROCESSING = 0x40,  // Skip further processing for this bone
				BONE_ACTIVE = 0x80,  // Bone is active in the current animation chain
			};
		};
	}
}