#include "CYyMenuMemory.h"

using namespace FFXI::CYy;

const BaseGameObject::ClassInfo CYyMenuMemory::CYyMenuMemoryClass{
	"CYyMenuMemory", sizeof(CYyMenuMemory), &BaseGameObject::BaseGameObjectClass
};

const BaseGameObject::ClassInfo* CYyMenuMemory::GetRuntimeClass() {
	return &CYyMenuMemoryClass;
}

