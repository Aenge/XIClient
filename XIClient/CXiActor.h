#pragma once
#define WIN32_LEAN_AND_MEAN
#include "CMoTask.h"
#include "CMoAttachmentsSubStruct.h"
#include "d3dx9math.h"
#include "d3d8to9/d3d8types.hpp"
#include "KzCibCollect.h"
#include "GAME_STATUS.h"
#include "SUBACTOR_STATUS.h"
#include "ActorType.h"
#include "BoundingBox3D.h"
#include "Enums.h"

namespace FFXI {
	namespace CYy {
		class XiAtelBuff;
		class CMoResource;
		class ResourceContainer;
		class CMoAttachments;
		class CXiActor : public CMoTask {
			static int SomeFunctionTable[51];
			static int SomeFunctionSub(CXiActor*, CXiActor*);
		public:
			static const BaseGameObject::ClassInfo CXiActorClass;
			virtual const BaseGameObject::ClassInfo* GetRuntimeClass() override;
			static CXiActor* top;
			static CMoAttachmentsSubStruct control_actor;
			static int skeletonActorIndex;
			static int config60;
			static bool somebool;
			static int maybeActorDrawCount;
			static Constants::Enums::GAME_STATUS emulate_game_status;
			static FFXI::Constants::Enums::ActorType emulate_type;
			static int emulate_race;
			static short emulate_item[16];
			static KzCibCollect emulate_cib;
			static Math::BoundingBox3D emulate_skeleton_bounding_boxes[3];
			static int some_count1[4];
			static int some_count2[4];
			static bool can_control;
			static int GetActorsNum();
			static CXiActor* GetHead();
			static void InitActorStatics();
			static bool SomeFunction(CXiActor*, CXiActor*, int);
			virtual char OnMove() override;
			virtual ~CXiActor();
			CXiActor();
			void Init();
			void Link();
			FFXI::Constants::Enums::GAME_STATUS GetGameStatus();
			bool IsOnChocobo();
			bool IsOnMount();
			bool SomeEventCheck();
			FFXI::Constants::Enums::ActorType GetType();
			int GetRace();
			char GetMoveMode();
			bool GetMonsterFlag();
			CXiActor* GetNext();
			bool InOwnActorPointers();
			bool GetMotStop();
			unsigned short GetEquipNum(char);
			bool CheckSomeFlag1();
			bool CheckSomeFlag2();
			bool CheckSomeFlag3();
			bool CheckAtel120Bit5();
			bool CheckAtel13CBit20();
			void SetAtel13CBit20(bool);
			bool IsOnChair();
			bool IsDead();
			CXiActor* Get154Actor();
			bool DefSchedularCall();
			bool StartScheduler(int, CXiActor*, void*);
			void StartGenerators(CMoResource**);
			void DeleteAttachments();
			void AtelDispOn();
			void AtelDispOff();
			void ResumeAllGenerator();
			void ResumeAllScheduler();
			void SuspendAllGenerator();
			void SuspendAllScheduler();
			int GetGmLevel();
			D3DXVECTOR4 field_34;
			D3DXVECTOR4 field_44;
			CXiActor* PreviousActor;
			CXiActor* NextActor;
			CMoAttachmentsSubStruct field_5C;
			CMoAttachments* CasterAttachments;
			CMoAttachments* TargetAttachments;
			XiAtelBuff* AtelBuffer;
			int ActorsNum;
			int field_78;
			int field_7C;
			int field_80;
			int field_84;
			unsigned int field_88;
			float field_8C;
			int field_90;
			float field_94;
			float field_98;
			float field_9C;
			int field_A0;
			Constants::Enums::SUBACTOR_STATUS SubActorStatus;
			int field_A8;
			int field_AC;
			char field_B0;
			char field_B1;
			unsigned short field_B2;
			ResourceContainer** field_B4;
			int field_B8;
			float field_BC;
			char field_C0;
			char field_C1;
			char field_C2;
			char field_C3;
			virtual D3DCOLOR VirtActor1();
			virtual void SetShadowAlpha(int);//VirtActor5
			virtual int VirtActor6();
			virtual int VirtActor7();
			virtual void KillAllGenerator(); //VirtActor8
			virtual void VirtActor11();
			virtual char MaybeIsEnvmap();
			virtual char MaybeIsDistortion();
			virtual char MaybeIsMasking();
			virtual char VirtActor20();
			virtual float GetWidthScale();
			virtual float GetHeightScale();
			virtual float GetDepthScale();
			virtual unsigned char VirtActor31();
			virtual void VirtActor32(unsigned char);
			virtual unsigned char VirtActor33();
			virtual void VirtActor34(unsigned char);
			virtual unsigned char VirtActor35();
			virtual void VirtActor36(unsigned char);
			virtual unsigned char VirtActor37();
			virtual void VirtActor38(unsigned char);
			virtual unsigned char VirtActor39();
			virtual void VirtActor40(unsigned char);
			virtual float VirtActor58();
			virtual char VirtActor63() final;
			virtual char VirtActor64() final;
			virtual unsigned char VirtActor67() final;
			virtual int VirtActor69() final;
			virtual char VirtActor72() final;
			virtual bool VirtActor73() final;
			virtual bool VirtActor75();
			virtual void VirtActor80() final;
			virtual void VirtActor81() final;
			virtual void VirtActor87(int);
			virtual int VirtActor88();
			virtual void VirtActor89();
			virtual void VirtActor90();
			virtual char VirtActor92();
			virtual char IsOnLift();//93
			virtual float VirtActor94() final;
			virtual void VirtActor95(float) final;
			virtual float VirtActor96() final;
			virtual void VirtActor97(float) final;
			virtual float GetWalkSpeed() final; //98
			virtual void VirtActor99(float) final;
			virtual D3DXVECTOR4* GetPos();
			virtual D3DXVECTOR4* VirtActor102();
			virtual bool VirtActor103(int, D3DXVECTOR4*);
			virtual void GetElemLocal(unsigned int, D3DXVECTOR4*); //VirtActor104
			virtual void VirtActor109(ResourceContainer***) final;
			virtual void GetModelFile(ResourceContainer***) final; //(VirtActor110)
			virtual bool IsReadComplete(); //(VirtActor111)
			virtual void ActorFindResource(CMoResource***, Constants::Enums::ResourceType, int); //(VirtActor113)
			virtual void SetMotionLock(bool);
			virtual bool IsMotionLock();
			virtual void SetGroundNormal(D3DXVECTOR4*);
			virtual D3DXVECTOR4* GetGroundNormal(); //virt 122
			virtual void VirtActor123(int);
			virtual int VirtActor124();
			virtual void VirtActor125(float);
			virtual void VirtActor127(D3DLIGHT8*);
			virtual void VirtActor128(D3DLIGHT8**);
			virtual int VirtActor130() final;
			virtual bool VirtActor142();
			virtual void VirtActor144(CXiActor*);
			virtual void OnDraw();
			virtual void VirtActor154(ResourceContainer**);
			virtual bool SetAction(int, CXiActor*, void*); //(VirtActor156)
			virtual void KillAction(int, CXiActor*) final;//157
			virtual bool IsMovingAction(int, CXiActor*) final;//VirtActor158
			virtual bool AmIControlActor(); //VirtActor183
			virtual void StartGenerators(); //VirtActor184
			virtual void IsControlLock(bool); //185
			virtual int IsControlLock(); //186
			virtual bool IsDirectionLock(); //188
			virtual void SetConstrain(char, int); //189
			virtual short IsConstrain(); //190
			virtual void VirtActor191(char, int);
			virtual char VirtActor192(int);
			virtual char IsFreeRun();//194
			virtual void IsFreeRun(char);//195
			virtual char IsWalkLock();//196
			virtual void IsWalkLock(char);//197
			virtual char IsParallelMove();
			virtual void IsParallelMove(char);
			virtual bool VirtActor201() final;
			virtual void SetSubActorStatus(FFXI::Constants::Enums::SUBACTOR_STATUS) final;
			virtual bool VirtActor203() final;
			virtual bool VirtActor204() final;
			virtual bool IsFishingRod() final;
			virtual bool VirtActor209() final;
			virtual bool VirtActor212() final;
			virtual bool VirtActor214(int) final;
			virtual bool VirtActor215() final;
			virtual int VirtActor217() final;
			virtual bool VirtActor218(int) final;
			virtual bool VirtActor219() final;
			virtual int VirtActor221() final;
			virtual bool VirtActor222(int) final;
			virtual bool VirtActor223() final;
			virtual int VirtActor225() final;
			virtual bool VirtActor226(int) final;
			virtual bool VirtActor227();
			virtual int VirtActor229();
			virtual unsigned short VirtActor231(char) final;
			virtual FFXI::Math::BoundingBox3D* GetBoundingBoxByState(bool);
			virtual FFXI::Math::BoundingBox3D* GetBoundingBoxDefaultState(bool);
			virtual KzCibCollect* VirtActor236();
			virtual void VirtActor237(short);
			virtual short VirtActor238();
			virtual void VirtActor240(CMoAttachments*) final;
			virtual void VirtActor241(CMoAttachments*) final;
			virtual char VirtActor242();
			virtual void VirtActor243(char);
		};
	}
}