#pragma once
#include "WeatherResource.h"
namespace FFXI {
	namespace CYy {
		class XiZone;
		class WeatherResources {
		public:
			void LoadAll(XiZone*);
			void Load(int, XiZone*);
			WeatherResources();
			virtual ~WeatherResources();
			CMoKeyframe** GetRandKeyframe(int);
			int Index;
			WeatherResource Resources[32];
		};
	}
}